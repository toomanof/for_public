from rest_framework import serializers
from billing.models import ReestrAvailableEvents
from ..models import VideoCatalog, AccessVideoCourse, AccessVideoTariff


class VideoCatalogPreviewSerializer(serializers.ModelSerializer):
    class Meta:
        model = VideoCatalog
        fields = ('id', 'title', 'thumbnail_url',)


class VideoCatalogSerializer(serializers.ModelSerializer):
    is_access = serializers.SerializerMethodField()

    class Meta:
        model = VideoCatalog
        fields = '__all__'

    def is_access_by_courses(self, element):
        request = self.context.get("request")
        courses = set(AccessVideoCourse.objects.filter(
            video=element,
            video__is_deleted=False,
            course__is_deleted=False,
            restricted=False

        ).values_list('course', flat=True))
        if not courses:
            return self.is_access_by_tariff(element)

        return ReestrAvailableEvents.objects.filter(
            event__in=courses,
            student=request.current_user,
            suspended=False
        ).exists()

    def is_access_by_tariff(self, element):
        request = self.context.get("request")
        tariffs = set(AccessVideoTariff.objects.filter(
            video=element,
            video__is_deleted=False,
            tariff__is_deleted=False,
        ).values_list('tariff', flat=True))
        if not tariffs:
            return False

        return ReestrAvailableEvents.objects.filter(
            tariff__in=tariffs,
            student=request.current_user,
            suspended=False
        ).exists()

    def get_is_access(self, element):
        return element.is_public or self.is_access_by_courses(element)
