from rest_framework import serializers
from ..models import AccessVideoCourse, AccessVideoTariff


class AccessVideoCourseSerializer(serializers.ModelSerializer):

    class Meta:
        model = AccessVideoCourse
        fields = ('course', 'video', 'restricted')


class AccessVideoTariffSerializer(serializers.ModelSerializer):

    class Meta:
        model = AccessVideoTariff
        fields = ('tariff', 'video', )
