from django.apps import AppConfig


class VideoCatalogConfig(AppConfig):
    name = 'video_catalog'
