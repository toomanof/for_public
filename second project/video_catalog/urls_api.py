from django.urls import path
from rest_framework.routers import DefaultRouter

from video_catalog.views import VideoCatalogApiView,\
    AccessVideoCourseApiView, AccessVideoTariffApiView

router = DefaultRouter()

router.register('', VideoCatalogApiView, basename='video_catalog')


urlpatterns = [
    path('<id_video>/access/courses/', AccessVideoCourseApiView.as_view(),
         name='access_video_courses'),
    path('<id_video>/access/tariffs/', AccessVideoTariffApiView.as_view(),
         name='access_video_tariffs'),
]

urlpatterns += router.urls
