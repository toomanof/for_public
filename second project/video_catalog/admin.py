from django.contrib import admin

from .models import *


@admin.register(VideoCatalog)
class VideoCatalogAdmin(admin.ModelAdmin):
    model = VideoCatalog
    list_display = ('title', 'url', 'user', 'order', 'is_public', 'is_deleted')


@admin.register(AccessVideoCourse)
class AvailableVideoCourseAdmin(admin.ModelAdmin):
    model = AccessVideoCourse
    list_display = ('course', 'video', 'date_create', 'date_update')


@admin.register(AccessVideoTariff)
class AvailableVideoTariffAdmin(admin.ModelAdmin):
    model = AccessVideoCourse
    list_display = ('tariff', 'video', 'date_create', 'date_update')
