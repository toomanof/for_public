from django.db import models
from django.contrib.auth.models import User
from core.models import AbstractNotRemoveModel, AbstractModelWithTitle, VideoMixin
from events.models import Event
from payments.models import Tariffication


class VideoCatalog(AbstractNotRemoveModel, AbstractModelWithTitle, VideoMixin):
    user = models.ForeignKey(
        User, on_delete=models.CASCADE,
        related_name='video_catalog', verbose_name='Создатель')
    is_public = models.BooleanField('Флаг общедоступности', default=False)
    thumbnail_url = models.URLField('Url превью', blank=True, null=True)
    description = models.TextField('Описание', blank=True, null=True)
    published = models.BooleanField('Опубликовано', default=False)


class AccessVideoCourse(models.Model):

    video = models.ForeignKey(
        VideoCatalog, on_delete=models.CASCADE,
        related_name='available_video_course_recordings', verbose_name='Видео')
    course = models.ForeignKey(Event,
                               on_delete=models.CASCADE,
                               verbose_name='Курс',
                               related_name='available_video_records')
    restricted = models.BooleanField('Ограничено тарифами', default=False)
    date_create = models.DateTimeField('Дата создание записи',
                                       auto_now_add=True)
    date_update = models.DateTimeField('Дата обновление записи',
                                       auto_now=True)

    class Meta:
        ordering = ['-date_update', ]


class AccessVideoTariff(models.Model):

    video = models.ForeignKey(
        VideoCatalog, on_delete=models.CASCADE,
        related_name='available_video_tariff_recordings', verbose_name='Видео')
    tariff = models.ForeignKey(Tariffication,
                               on_delete=models.CASCADE,
                               verbose_name='Тариф',
                               related_name='available_video_records')
    date_create = models.DateTimeField('Дата создание записи',
                                       auto_now_add=True)
    date_update = models.DateTimeField('Дата обновление записи',
                                       auto_now=True)

    class Meta:
        ordering = ['-date_update', ]
