from django.shortcuts import get_object_or_404
from rest_framework.permissions import IsAuthenticated

from core.errors import wrapper_error
from core.views import AbstractModelViewSetWithCurrentUser
from ..models import VideoCatalog
from ..serializers import VideoCatalogSerializer


class VideoCatalogApiView(AbstractModelViewSetWithCurrentUser):
    model = VideoCatalog
    serializer_class = VideoCatalogSerializer
    permission_classes = [IsAuthenticated]
