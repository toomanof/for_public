from django.db import transaction
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView

from core.errors import wrapper_error
from ..models import AccessVideoCourse, AccessVideoTariff
from ..serializers import AccessVideoCourseSerializer, \
    AccessVideoTariffSerializer


class AccessVideoApiView(APIView):
    model = None
    serializer_class = None
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return self.model.objects.filter(
            video_id=self.kwargs['video_id'],
            video__is_deleted=False,
            video__published=True
        )

    @wrapper_error
    @transaction.atomic
    def post(self, request, id_video, *args, **kwargs):
        self.kwargs['video_id'] = id_video
        self.model.objects.filter(video_id=id_video).delete()
        self.save_data_list_to_db(request, id_video)
        return self.get(request, id_video, *args, **kwargs)

    def put(self, request, id_video, *args, **kwargs):
        return self.post(request, id_video, *args, **kwargs)

    def patch(self, request, id_video, *args, **kwargs):
        return self.post(request, id_video, *args, **kwargs)

    @wrapper_error
    def get(self, request, id_video, *args, **kwargs):
        self.kwargs['video_id'] = id_video
        return self.serializer_class(self.get_queryset(), many=True).data


class AccessVideoCourseApiView(AccessVideoApiView):
    model = AccessVideoCourse
    serializer_class = AccessVideoCourseSerializer
    permission_classes = [IsAuthenticated]

    def save_data_list_to_db(self, request, id_video):
        list_objects = []
        for item in request.data:
            list_objects.append(
                self.model(video_id=id_video,
                           course_id=item['course'],
                           restricted=item['restricted']))
        self.model.objects.bulk_create(list_objects)

    def post(self, request, id_video, *args, **kwargs):
        """
        Метод создания доступных курсов для видое с каталога
        Входной параметр:
           - [
	            {
		         "course": 4,
		         "restricted": true
	            },
	           {
		        "course": 1,
		        "restricted": true
	           }
            ] list with objects: course - id курса, restricted -ограничение курса тарифаим
        """
        return super().post(request, id_video, *args, **kwargs)

    def put(self, request, id_video, *args, **kwargs):
        """
        Метод обновления доступных курсов для видое с каталога
        Входной параметр:
           - [
	            {
		         "course": 4,
		         "restricted": true
	            },
	           {
		        "course": 1,
		        "restricted": true
	           }
            ] list with objects: course - id курса, restricted -ограничение курса тарифаим
        """
        return super().put(request, id_video, *args, **kwargs)


class AccessVideoTariffApiView(AccessVideoApiView):
    model = AccessVideoTariff
    serializer_class = AccessVideoTariffSerializer
    permission_classes = [IsAuthenticated]

    def save_data_list_to_db(self, request, id_video):
        list_objects = []
        for item in request.data:
            list_objects.append(self.model(video_id=id_video, tariff_id=item))
        self.model.objects.bulk_create(list_objects)

    def post(self, request, id_video, *args, **kwargs):
        """
        Метод создания доступных тарифов для видое с каталога
        Входной параметр:
           - [1,2,4] list with id tariffs
        """
        return super().post(request, id_video, *args, **kwargs)

    def put(self, request, id_video, *args, **kwargs):
        """
        Метод обновления доступных тарифов для видое с каталога
        Входной параметр:
           - [1,2,4] list with id tariffs
        """
        return super().put(request, id_video, *args, **kwargs)

    @wrapper_error
    def get(self, request, id_video, *args, **kwargs):
        self.kwargs['video_id'] = id_video
        return [item.tariff.id for item in self.get_queryset()]
