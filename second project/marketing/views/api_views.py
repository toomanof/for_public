from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated


from core.errors import wrapper_error
from billing.models import ReestUserViewCourse, ReestrStudentInSchool
from marketing.services import MarketingService


class ListUserInSchools(APIView):
    permission_classes = [IsAuthenticated]

    @wrapper_error
    def get(self, request, pk):
        """
        Список школ в которых зарегистрирован пользователь
        """
        return MarketingService(request).list_schools_user(pk)


class ListUserCourses(APIView):
    permission_classes = [IsAuthenticated]

    @wrapper_error
    def get(self, request, pk):
        """
        Список школ в которых зарегистрирован пользователь
        """
        return MarketingService(request).list_courses_user(pk)
