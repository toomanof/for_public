from django.urls import path

from .views import \
    ListUserInSchools, ListUserCourses

app_name = 'api_marketing'

urlpatterns = [
    path('track/student/<pk>/schools/',
         ListUserInSchools.as_view(),
         name='list_user_in_schools'),
    path('track/student/<pk>/courses/',
         ListUserCourses.as_view(),
         name='list_user_in_courses'),
    ]