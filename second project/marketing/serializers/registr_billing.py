from rest_framework import serializers
from billing.models import ReestUserViewCourse, ReestrStudentInSchool


from events.models import CurrentElementEventForStudent


class ReestUserViewCourseSerializer(serializers.ModelSerializer):
    current_lesson_element = serializers.SerializerMethodField()

    class Meta:
        model = ReestUserViewCourse
        fields = '__all__'

    def get_current_lesson_element(self, element):
        request = self.context.get("request")
        if not request:
            return False

        return CurrentElementEventForStudent.objects\
            .values_list('id', flat=True)\
            .filter(student=request.current_user,
                    event=element.event)\
            .first()


class ReestrStudentInSchoolSerializer(serializers.ModelSerializer):


    class Meta:
        model = ReestrStudentInSchool
        fields = '__all__'
