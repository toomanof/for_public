import logging

from django.db import IntegrityError
from django.shortcuts import get_object_or_404

from core.errors import timer_function
from core.models import School
from core.services import MixinServiceRoleBilling
from billing.models import \
    ReestUserViewCourse, ReestrStudentInSchool

from marketing.serializers import \
    ReestUserViewCourseSerializer, ReestrStudentInSchoolSerializer
logger = logging.getLogger('info')
logger_err = logging.getLogger('warning')


class MarketingService(MixinServiceRoleBilling):

    def __init__(self, request):
        self.request = request

    @staticmethod
    @timer_function
    def add_user_to_school(request, school_id):
        school = get_object_or_404(School, id=school_id)

        if request.current_user:
            logger.info(f'add_user_to_school {request.current_user}, {school_id}')
            ReestrStudentInSchool.objects.get_or_create(
                student=request.current_user,
                school_id=school_id)

    @staticmethod
    def _update_status_in_reest_user_view_course(
            request, event_id, old_status, new_status):
        try:
            if request.current_user:
                ReestUserViewCourse.objects.update_or_create(
                    student=request.current_user,
                    event_id=event_id,
                    defaults={'status': new_status})
        except IntegrityError:
            logger_err.error(f'Event with {event_id} not found')

    @staticmethod
    def _update_status_in_reest_student_in_school(
            request, school, old_status, new_status):
        if request.current_user:
            logger.info(f'_update_status_in_reest_student_in_school {request.current_user}, {school}, {old_status}, {new_status}')
            ReestrStudentInSchool.objects.update_or_create(
                student=request.current_user,
                school=school,
                defaults={'status': new_status}
            )

    @staticmethod
    def update_status_in_reest_student_in_school_to_not_have_course(
            request, school):

        MarketingService._update_status_in_reest_student_in_school(
            request, school,
            old_status=ReestrStudentInSchool.ST_USER_NOT_HAVE_COURSE,
            new_status=ReestrStudentInSchool.ST_USER_NOT_PAID_COURSE
        )

    @staticmethod
    def update_status_in_reest_student_in_school_to_processing_paid(
            request, school):

        MarketingService._update_status_in_reest_student_in_school(
            request, school,
            old_status=ReestrStudentInSchool.ST_USER_NOT_PAID_COURSE,
            new_status=ReestrStudentInSchool.ST_USER_PROCESSING_PAID_COURSE
        )

    @staticmethod
    def update_status_in_reest_student_in_school_to_have_course(
            student, school):

        ReestrStudentInSchool.objects.get_or_create(
                student=student,
                school=school,
                defaults={'status': ReestrStudentInSchool.ST_USER_HAVE_COURSE}
        )

    @staticmethod
    def student_interested_event(user, event_id):
        try:
            record, created = ReestUserViewCourse.objects.get_or_create(
                event_id=event_id,
                student=user,
                defaults={'status': ReestUserViewCourse.ST_VIEW})
            if not created:
                record.status = ReestUserViewCourse.ST_VIEW
                record.save()
        except IntegrityError:
            logger_err.error(f'Event with {event_id} not found')

    @staticmethod
    def student_redirect_to_payment(request, event_id):
        MarketingService._update_status_in_reest_user_view_course(
            request,
            event_id,
            ReestUserViewCourse.ST_VIEW,
            ReestUserViewCourse.ST_REDIRECT_TO_PAYMENT)

    @staticmethod
    def student_paid_course(student, course):
        ReestUserViewCourse.objects\
            .filter(student=student,
                    event=course)\
            .update(status=ReestUserViewCourse.ST_PAID)

    @staticmethod
    def student_begin_course(request, event_id):
        MarketingService._update_status_in_reest_user_view_course(
            request,
            event_id,
            ReestUserViewCourse.ST_PAID,
            ReestUserViewCourse.ST_BEGIN)

    @staticmethod
    def student_end_course(request, event_id):
        MarketingService._update_status_in_reest_user_view_course(
            request,
            event_id,
            ReestUserViewCourse.ST_BEGIN,
            ReestUserViewCourse.ST_END)

    def list_schools_user(self, user_id):
        return ReestrStudentInSchoolSerializer(
            ReestrStudentInSchool.objects.filter(
                student_id=user_id),
            many=True).data

    def list_courses_user(self, user_id):
        return ReestUserViewCourseSerializer(
            ReestUserViewCourse.objects.filter(
                student_id=user_id),
            context={"request": self.request},
            many=True).data
