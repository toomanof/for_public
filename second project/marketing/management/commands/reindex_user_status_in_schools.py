from django.core.management.base import BaseCommand
from django_extensions.management.utils import signalcommand

from billing.models import Order, ReestrStudentInSchool


class Command(BaseCommand):

    @signalcommand
    def handle(self, *args, **options):
        qs = Order.objects\
            .filter(status=Order.COMPLETED)\
            .values_list('event__owner__school', 'student')

        schools = {}
        for school_id, student_id in qs:
            if school_id not in schools:
                schools[school_id] = []
            schools[school_id].append(student_id)
        for key, val in schools.items():
            ReestrStudentInSchool.objects\
                .filter(school_id=key, student__in=val)\
                .update(status=ReestrStudentInSchool.ST_USER_HAVE_COURSE)
