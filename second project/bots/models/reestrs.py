import uuid
from django.db import models

from .algorithms import BotDialogAlgorithm
from .settings import Bot


class ReestrChatTelegram(models.Model):
    INCOMING, OUTGOING = range(2)
    CHOICES_DIRECTION = (
        (INCOMING, 'входящий'),
        (OUTGOING, 'исходящий')
    )
    dialog = models.UUIDField('uuid диалога', editable=False, null=True, blank=True)
    id_bot = models.PositiveIntegerField('ID telefram bot')
    username = models.CharField('username in teleram bot', max_length=255)
    bot = models.ForeignKey(
        Bot, on_delete=models.CASCADE, related_name='records_rct')
    bot_algorithm_step = models.ForeignKey(
        BotDialogAlgorithm, on_delete=models.CASCADE,
        related_name='records_rct', null=True, blank=True)
    direction = models.SmallIntegerField(
        'Направление', choices=CHOICES_DIRECTION, default=INCOMING)
    text = models.TextField('Сообщение', null=True, blank=True)
    date_create = models.DateTimeField('Дата создание записи', auto_now_add=True)
    date_update = models.DateTimeField('Дата обновление записи', auto_now=True)

    class Meta:
        verbose_name = "Запись диалога чата"
        verbose_name_plural = "Записи диалогов чатов"
        ordering = ['bot', 'username', '-date_create']


class CurrentAlgorithmStep(models.Model):
    dialog = models.UUIDField('uuid диалога', default=uuid.uuid4(),
                              editable=False, null=True, blank=True)
    id_bot = models.PositiveIntegerField('ID telegram bot')
    bot = models.ForeignKey(
        Bot, on_delete=models.CASCADE,
        related_name='current_algorithm_steps')
    bot_algorithm_step = models.ForeignKey(
        BotDialogAlgorithm, on_delete=models.CASCADE,
        related_name='current_algorithm_steps')
    date_create = models.DateTimeField('Дата создание записи', auto_now_add=True)
    date_update = models.DateTimeField('Дата обновление записи', auto_now=True)

    class Meta:
        verbose_name = 'Текущий шаг бота'
        verbose_name_plural = 'Текущие шаги ботов'


class VariableInBot(models.Model):
    bot = models.ForeignKey(
        Bot, on_delete=models.CASCADE,
        related_name='variables')
    name = models.CharField('Название переменной', max_length=255)
    date_create = models.DateTimeField('Дата создание записи', auto_now_add=True)
    date_update = models.DateTimeField('Дата обновление записи', auto_now=True)

    class Meta:
        verbose_name = 'Переменная бота'
        verbose_name_plural = 'Переменные ботов'
        ordering = ['-date_create', ]


class ValueVariableInBot(models.Model):
    id_bot = models.PositiveIntegerField('ID telegram bot', null=True, blank=True)
    bot = models.ForeignKey(
        Bot, on_delete=models.CASCADE,
        related_name='values_variable')
    name = models.CharField('Название переменной', max_length=255)
    value = models.CharField('Значение переменной', max_length=255)
    date_create = models.DateTimeField('Дата создание записи', auto_now_add=True)
    date_update = models.DateTimeField('Дата обновление записи', auto_now=True)

    class Meta:
        verbose_name = 'Значение переменной бота'
        verbose_name_plural = 'Значения переменных ботов'
        ordering = ['-date_create', ]
