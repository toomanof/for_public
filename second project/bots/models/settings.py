from django.db import models
from django.contrib.auth.models import User

from core.models import AbstractNotRemoveModel


class Bot(AbstractNotRemoveModel):
    TB_TELEGRAM, TB_VIBER, TB_VK, TB_FB = range(4)
    CHOICE_TYPE_BOT = (
        (TB_TELEGRAM, 'Telegram'),
        (TB_VIBER, 'viber'),
        (TB_VK, 'VK'),
        (TB_FB, 'facebook')
    )
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='bot_settings',
        verbose_name='Создатель бота')
    name = models.CharField('Название', max_length=255)
    token = models.CharField('Токен', max_length=255, blank=True, null=True)
    scope_vk = models.CharField('Запрашиваемые права', max_length=255,
                                help_text='Запрашиваемые права бота VK,'
                                          'можно передать строкой или числом',
                                blank=True, null=True)
    login = models.CharField('Логин', max_length=255, blank=True, null=True)
    password = models.CharField('Пароль', max_length=255,
                                blank=True, null=True)
    type = models.SmallIntegerField('Тип бота',
                                    default=TB_TELEGRAM,
                                    choices=CHOICE_TYPE_BOT)
    activate = models.BooleanField('Флаг активности бота', default=False)

    def __str__(self):
        return f'Бот {self.name} создатель {self.user}'

    def get_service_work_with_bot(self):
        from bots.services import TelegramBot, ViberBot, VKBot, FBBot
        if self.type == Bot.TB_TELEGRAM:
            return TelegramBot(self.token)
        if self.type == Bot.TB_VIBER:
            return ViberBot()
        if self.type == Bot.TB_VK:
            return VKBot()
        if self.type == Bot.TB_VK:
            return FBBot()
