import logging
import uuid
import json

from django.db import models
from pydantic import ValidationError
from django_jsonfield_backport.models import JSONField
from .settings import Bot
from core.models import AbstractNotRemoveModel
from core.errors import ConflictError

logger = logging.getLogger('warning')


def get_bot_component_class(index):
    """
    Возвращает класс компонента бота хранящегося в записи
    @return: BotButton, BotResponse, BotQuestion, BotGoToStep, BotSaveAnswer
    """
    from ..services import (
        BotStart, BotUserInput, BotResponse, BotGoTo, BotFilter, BotStop,
        BotFallBack, BotSetVariable)
    list_classes_bot = (BotStart, BotUserInput, BotResponse, BotGoTo,
                        BotFilter, BotStop, BotFallBack, BotSetVariable)
    return list_classes_bot[index]


class BotDialogAlgorithm(AbstractNotRemoveModel):
    (CMD_START, CMD_USER_INPUT, CMD_RESPONSE, CMD_GOTO,
     CMD_FILTER, CMD_STOP, CMD_FALLBACK, CMD_SET_VAR) = range(8)
    CHOICE_COMMANDS = (
        (CMD_START, 'Точка входа'),
        (CMD_USER_INPUT, 'Пользовательский ввод'),
        (CMD_RESPONSE, 'Вывод сообщения'),
        (CMD_GOTO, 'Переход'),
        (CMD_FILTER, 'Фильтр'),
        (CMD_STOP, 'Точка выхода'),
        (CMD_FALLBACK, 'Тригер на нероспознаное сообщение'),
        (CMD_SET_VAR, 'Присвоение значения переменной'),
    )

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    title = models.CharField('Название', max_length=255, null=True, blank=True)
    description = models.TextField('Описание', null=True, blank=True)
    bot = models.ForeignKey(
        Bot, on_delete=models.CASCADE, related_name='algorithm_commands',
        verbose_name='Бот', null=True, blank=True)
    command = models.SmallIntegerField('Команда', choices=CHOICE_COMMANDS,
                                       default=CMD_START)
    data = JSONField('Данные шага', null=True, blank=True)
    parent = models.ForeignKey('self', on_delete=models.CASCADE,
                               null=True, blank=True, related_name='childs')
    childrens_class = models.TextField('Классы потомков', blank=True, null=True)
    order = models.PositiveIntegerField('индекс сортировки', default=0)

    def __str__(self):
        return self.title

    @property
    def count_childrens(self):
        return self.childs.all_active().count()

    @property
    def command_object(self):
        """
        Возвращает объект класса компонента бота хранящегося в записи
        @return:
        """
        try:
            if isinstance(self.data, str):
                self.data = json.loads(self.data)
            result = self.component_class(**self.data)
        except ValidationError as e:
            logger.error(f'command_object error: {str(e)}')
        else:
            return result

    @property
    def component_class(self):
        """
        Возвращает класс компонента бота хранящегося в записи
        @return: BotButton, BotResponse, BotQuestion, BotGoToStep, BotSaveAnswer
        """
        return get_bot_component_class(self.command)

    @property
    def is_start(self):
        return self.command == self.CMD_START

    @property
    def is_stop(self):
        return self.command == self.CMD_STOP

    @property
    def is_response(self):
        return self.command == self.CMD_RESPONSE

    @property
    def is_set_variable(self):
        return self.command == self.CMD_SET_VAR

    @property
    def is_user_input(self):
        return self.command == self.CMD_USER_INPUT

    @property
    def is_goto(self):
        return self.command == self.CMD_GOTO

    @property
    def is_filter(self):
        return self.command == self.CMD_FILTER

    @property
    def is_fallback(self):
        return self.command == self.CMD_FALLBACK

    @staticmethod
    def list_commands():
        from core.services import find_sub_str

        regex = r"'([a-z]*)'"
        result = []
        for key, val in BotDialogAlgorithm.CHOICE_COMMANDS:
            fields = [{'name': key,
                       'type': find_sub_str(str(field.type_), regex),
                       'required': field.required,
                       'default': field.default}
                      for key, field in
                      get_bot_component_class(key).__fields__.items()]
            result.append({
                'id': key,
                'command': key,
                'title': val,
                'data': fields
            })
        return result

    def save(self, *args, **kwargs):
        # TODO Создать проверку на правильность заполнения данных data

        set_order = kwargs.pop('set_order', False)
        try:
            if set_order:
                self.order = self.last_order()
                print('BotDialogAlgorithm save last_order', self.order)
            # if self.data is not None:
            #     self.data = self.command_object.json()
        except Exception as e:
            logger.error(str(e))
        super().save(*args, **kwargs)

    def last_order(self):
        obj = BotDialogAlgorithm.objects.filter(bot=self.bot).order_by('order').last()
        return obj.order + 1 if obj else 0

    def add_child(self, class_child, **kwargs):
        """
        Добавление потомка.
        В начале производится проверка на разрешения добавления этого потомка.
        А также производится проверка на совместимость этого потомка с уже
        существующими потомками
        @param class_child: класс добавляемого потомка
        @param kwargs: параметра добавляемого потомка
        @return: добавленный потомок
        """
        if class_child.__name__ not in self.command_object.allowed_children_class:
            raise ConflictError(f'{class_child.__name__}'
                                f' cannot be child of a {self.command_object.__class__.__name__}!')

        if (self.count_childrens > 0
                and class_child.__name__ not in self.command_object.allowed_many_children_class):
            raise ConflictError(
                f'{class_child.__name__} is not compatible with existing descendants!')

        self.childrens_class = '' if self.childrens_class is None else self.childrens_class
        self.childrens_class += f', {class_child.__name__}' if self.childrens_class else class_child.__name__
        self.save()

        assert kwargs.get('command'), 'Not found parameter index_command!'

        kwargs_component = kwargs.pop('data', {})
        kwargs['parent'] = self
        kwargs['data'] = class_child(**kwargs_component).dict()

        children = BotDialogAlgorithm(**kwargs)
        children.save(set_order=True)
        return children

    def add_child_by_index_command(self, **kwargs):
        kwargs['bot_id'] = self.bot_id
        if kwargs['command'] == self.CMD_RESPONSE:
            return self.add_child_bot_response(**kwargs)
        elif kwargs['command'] == self.CMD_FALLBACK:
            return self.add_child_bot_fallback(**kwargs)
        elif kwargs['command'] == self.CMD_USER_INPUT:
            return self.add_child_bot_user_input(**kwargs)
        elif kwargs['command'] == self.CMD_GOTO:
            return self.add_child_bot_go_to(**kwargs)
        elif kwargs['command'] == self.CMD_FILTER:
            return self.add_child_bot_filter(**kwargs)
        elif kwargs['command'] == self.CMD_SET_VAR:
            return self.add_child_bot_set_variable(**kwargs)
        elif kwargs['command'] == self.CMD_STOP:
            return self.add_child_bot_stop(**kwargs)

    def add_child_bot_response(self, **kwargs):
        from ..services import BotResponse
        kwargs['command'] = self.CMD_RESPONSE
        kwargs['title'] = kwargs.get('title', "Response")
        return self.add_child(BotResponse, **kwargs)

    def add_child_bot_fallback(self, **kwargs):
        from ..services import BotFallBack
        kwargs['command'] = self.CMD_FALLBACK
        kwargs['title'] = kwargs.get('title', "FallBack")
        return self.add_child(BotFallBack, **kwargs)

    def add_child_bot_user_input(self, **kwargs):
        from ..services import BotUserInput
        kwargs['command'] = self.CMD_USER_INPUT
        kwargs['title'] = kwargs.get('title', "User input")
        return self.add_child(BotUserInput, **kwargs)

    def add_child_bot_go_to(self, **kwargs):
        from ..services import BotGoTo
        kwargs['command'] = self.CMD_GOTO
        kwargs['title'] = kwargs.get('title', "Goto")
        return self.add_child(BotGoTo, **kwargs)

    def add_child_bot_filter(self, **kwargs):
        from ..services import BotFilter
        kwargs['command'] = self.CMD_FILTER
        kwargs['title'] = kwargs.get('title', "Filter")
        return self.add_child(BotFilter, **kwargs)

    def add_child_bot_set_variable(self, **kwargs):
        from ..services import BotSetVariable
        kwargs['command'] = self.CMD_SET_VAR
        kwargs['title'] = kwargs.get('title', "Set variable")
        return self.add_child(BotSetVariable, **kwargs)

    def add_child_bot_stop(self, **kwargs):
        from ..services import BotStop
        kwargs['command'] = self.CMD_STOP
        kwargs['title'] = kwargs.get('title', "Stop")
        return self.add_child(BotStop, **kwargs)
