from django.contrib import admin

from bots.models import Bot, BotDialogAlgorithm, CurrentAlgorithmStep,\
    ReestrChatTelegram, VariableInBot, ValueVariableInBot


@admin.register(Bot)
class BotAdmin(admin.ModelAdmin):
    model = Bot
    list_display = ('name', 'user', 'type', 'activate')


@admin.register(BotDialogAlgorithm)
class BotDialogAlgorithmAdmin(admin.ModelAdmin):
    model = BotDialogAlgorithm
    list_display = ('title', 'bot', 'command', 'order')


@admin.register(CurrentAlgorithmStep)
class CurrentAlgorithmStepAdmin(admin.ModelAdmin):
    model = CurrentAlgorithmStep
    list_display = ('dialog', 'bot', 'bot_algorithm_step', 'date_create', 'date_update')


@admin.register(ReestrChatTelegram)
class ReestrChatTelegramAdmin(admin.ModelAdmin):
    model = ReestrChatTelegram
    list_display = ('dialog', 'username', 'bot', 'bot_algorithm_step', 'direction',
                    'text', 'date_create', 'date_update')


@admin.register(VariableInBot)
class VariableInBotAdmin(admin.ModelAdmin):
    model = VariableInBot
    list_display = ('bot', 'name',  'date_create', 'date_update')


@admin.register(ValueVariableInBot)
class ValueVariableInBotAdmin(admin.ModelAdmin):
    model = ValueVariableInBot
    list_display = ('bot', 'id_bot', 'name', 'value', 'date_create', 'date_update')

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False
