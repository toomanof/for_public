from rest_framework import serializers
from bots.models.algorithms import BotDialogAlgorithm


class BotDialogAlgorithmSerializer(serializers.ModelSerializer):
    count_childrens = serializers.SerializerMethodField()
    childrens = serializers.SerializerMethodField()

    class Meta:
        model = BotDialogAlgorithm
        fields = ('id', 'bot', 'command', 'title', 'description',
                  'data', 'order', 'parent', 'childrens', 'count_childrens')

    def get_count_childrens(self, element):
        return element.count_childrens

    def get_childrens(self, element):
        return BotDialogAlgorithmSerializer(element.childs.all_active(), many=True).data
