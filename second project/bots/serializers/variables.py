from rest_framework import serializers
from bots.models import VariableInBot


class VariableInBotSerializer(serializers.ModelSerializer):

    class Meta:
        model = VariableInBot
        fields = ('id', 'name', )
