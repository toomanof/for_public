from rest_framework import serializers
from bots.models import ReestrChatTelegram


class ReestrChatTelegramSerializer(serializers.ModelSerializer):

    class Meta:
        model = ReestrChatTelegram
        fields = ('dialog', 'username')


class RetrieveReestrChatTelegramSerializer(serializers.ModelSerializer):

    class Meta:
        model = ReestrChatTelegram
        fields = ('dialog', 'direction', 'username', 'text', 'date_update', 'bot_algorithm_step')
