from rest_framework import serializers

from bots.models import Bot
from .algorithms import BotDialogAlgorithmSerializer


class BotSerializer(serializers.ModelSerializer):

    class Meta:
        model = Bot
        fields = ('id', 'name', 'type', 'token', 'scope_vk',
                  'login', 'password', 'activate')


class BotWithAlgorithmSerializer(serializers.ModelSerializer):
    algorithm = serializers.SerializerMethodField()
    #webhook_info = serializers.SerializerMethodField()

    class Meta:
        model = Bot
        fields = ('id', 'name', 'type', 'token', 'scope_vk',
                  'login', 'password', 'algorithm', 'activate')

    def get_algorithm(self, element):
        from bots.services import ProxyBotService
        return BotDialogAlgorithmSerializer(
            ProxyBotService(element.id).entry_point).data

    def get_webhook_info(self, element):
        from bots.services import ProxyBotService
        return ProxyBotService(element.id).get_webhook_info()
