from django.urls import path
from rest_framework.routers import DefaultRouter

from bots.views.api_views import send_message_to_bots
from bots.views import (
    BotCommandsAPIView, BotApiView, BotDialogAlgorithmApiView,
    BotVariablesAPIView, BotDialogAPIView, RetrieveBotDialogAPIView)

app_name = 'api_bots'


router = DefaultRouter()

router.register('',
                BotApiView, basename='bot_settings')
router.register(r'(?P<bot_id>\d+)/algorithm',
                BotDialogAlgorithmApiView, basename='bot_algorithm')

urlpatterns = [
    path('send_message_to_bots/', send_message_to_bots),
    path('commands/', BotCommandsAPIView.as_view(), name='bot_commands'),
    path('<bot_id>/variables/', BotVariablesAPIView.as_view(), name='bot_variables'),
    path('<bot_id>/dialogs/', BotDialogAPIView.as_view(), name='bot_dialog'),
    path('dialogs/<uuid>', RetrieveBotDialogAPIView.as_view(), name='bot_dialog_records'),

] + router.urls
