# Generated by Django 3.0.2 on 2021-05-28 12:30

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('bots', '0062_auto_20210528_1148'),
    ]

    operations = [
        migrations.AlterField(
            model_name='currentalgorithmstep',
            name='dialog',
            field=models.UUIDField(blank=True, default=uuid.UUID('583a3877-cc40-4536-99d6-deb432973011'), editable=False, null=True, verbose_name='uuid диалога'),
        ),
    ]
