# Generated by Django 3.0.2 on 2021-05-05 12:29

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bots', '0034_auto_20210503_1231'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='reestrchattelegram',
            options={'ordering': ['bot', 'username', '-date_create'], 'verbose_name': 'Запись диалога чата', 'verbose_name_plural': 'Записи диалогов чатов'},
        ),
    ]
