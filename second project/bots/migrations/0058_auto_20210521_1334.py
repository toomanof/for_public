# Generated by Django 3.0.2 on 2021-05-21 10:34

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('bots', '0057_auto_20210521_1302'),
    ]

    operations = [
        migrations.AlterField(
            model_name='currentalgorithmstep',
            name='dialog',
            field=models.UUIDField(blank=True, default=uuid.UUID('adbb4ee4-2127-427f-ac43-35bdaca6cd06'), editable=False, null=True, verbose_name='uuid диалога'),
        ),
    ]
