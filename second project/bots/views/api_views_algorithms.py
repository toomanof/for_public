import json
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ModelViewSet
from django.shortcuts import get_object_or_404

from bots.models import BotDialogAlgorithm, VariableInBot
from bots.serializers import BotDialogAlgorithmSerializer
from bots.services import ProxyBotService

from core.errors import wrapper_error
from core.services import camel_case_to_under_case


def save_variables_in_base(bot_id, variable_name):
    VariableInBot.objects.update_or_create(
        bot_id=bot_id,
        name=variable_name.strip()
    )


class BotDialogAlgorithmApiView(ModelViewSet):
    permission_classes = [IsAuthenticated]
    serializer_class = BotDialogAlgorithmSerializer
    queryset = BotDialogAlgorithm.objects.all_active()

    @wrapper_error
    def create(self, request, bot_id):
        data = camel_case_to_under_case(json.loads(request.body))
        data['bot_id'] = bot_id
        data['parent_id'] = data.pop('parent', None)
        parent = get_object_or_404(BotDialogAlgorithm, id=data['parent_id'])
        result = parent.add_child_by_index_command(**data)

        if result and 'data' in data and 'var_name' in data['data'] and data['data']['var_name']:
            save_variables_in_base(bot_id, data['data']['var_name'])

        return BotDialogAlgorithmSerializer(result).data

    @wrapper_error
    def update(self, request, bot_id, **kwargs):
        data = camel_case_to_under_case(json.loads(request.body))
        if 'data' in data and 'var_name' in data['data']:
            save_variables_in_base(bot_id, data['data']['var_name'])

        return super(BotDialogAlgorithmApiView, self).update(request, bot_id, **kwargs)

    @wrapper_error
    def partial_update(self, request, bot_id, **kwargs):
        return self.update(request, bot_id, **kwargs)

    @wrapper_error
    def list(self, request, bot_id):
        return Response(
            BotDialogAlgorithmSerializer(
                ProxyBotService(bot_id).entry_point).data)

    @wrapper_error
    def retrieve(self, request, bot_id, pk=None):
        return BotDialogAlgorithmSerializer(
            get_object_or_404(BotDialogAlgorithm,
                              id=pk,
                              is_deleted=False)).data

    @wrapper_error
    def destroy(self, request, bot_id, pk=None):
        get_object_or_404(BotDialogAlgorithm, id=pk, is_deleted=False).delete()
