import json
import logging

from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.generics import ListAPIView

from bots.models import BotDialogAlgorithm, VariableInBot, ReestrChatTelegram
from core.services import AccountService
from bots.services import (
    AccountServiceWorkWithTelegram, AccountServiceWorkWithViber)
from bots.serializers import VariableInBotSerializer, ReestrChatTelegramSerializer, RetrieveReestrChatTelegramSerializer
from core.errors import wrapper_error

logger_debug = logging.getLogger('debug')

@api_view(['POST'])
def send_message_to_bots(request):
    """
        Отправляет сообщение на все подключенные боты пользователю
        определённому по токену.

        Parameters
        ----------
        message - текст сообщения;

    """
    try:
        AccountService.get_user_by_knox_token(request)
    except Exception:
        return Response('Not found token',
                        status=status.HTTP_401_UNAUTHORIZED)
    try:
        data = json.loads(request.body)
    except Exception:
        return Response('not message', status.HTTP_400_BAD_REQUEST)

    if not 'message' in data:
        return Response('not message', status.HTTP_400_BAD_REQUEST)

    AccountServiceWorkWithTelegram(request).send_messange_all_subscribers(
        data['message'])
    AccountServiceWorkWithViber(request).send_messange_all_subscribers(
        data['message'])
    return Response('Ok. Message send.')


class BotCommandsAPIView(APIView):

    def get(self, request):
        """
        Список команд(компонетов) бота
        @param request:
        @return: list
        """
        return Response(BotDialogAlgorithm.list_commands())


class BotVariablesAPIView(ListAPIView):
    permission_classes = [IsAuthenticated]
    queryset = VariableInBot.objects.all()
    serializer_class = VariableInBotSerializer

    def get_queryset(self):
        return VariableInBot.objects\
            .filter(bot_id=self.kwargs['bot_id']).distinct()


class BotDialogAPIView(ListAPIView):
    permission_classes = [IsAuthenticated]
    queryset = ReestrChatTelegram.objects.all()
    serializer_class = ReestrChatTelegramSerializer

    def get_queryset(self):
        qs = ReestrChatTelegram.objects.filter(
            bot__user=self.request.current_user
        )
        result = list()
        dialogs = list()

        for item in qs:
            if item.dialog not in dialogs:
                dialogs.append(item.dialog)
                result.append(item)

        return result


class RetrieveBotDialogAPIView(ListAPIView):
    permission_classes = [IsAuthenticated]
    queryset = ReestrChatTelegram.objects.all()
    serializer_class = RetrieveReestrChatTelegramSerializer

    def get_queryset(self):
        return ReestrChatTelegram.objects.filter(
            dialog=self.kwargs['uuid']
        ).order_by('date_update')
