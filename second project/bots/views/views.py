from django.views.generic import View
from django.http import HttpResponse

from core.services import camel_case_to_under_case
from bots.services import (
    TelegramBotIncomingRequestService, AccountServiceWorkWithViber,
    AccountServiceWorkWithVK, AccountServiceWorkWithFB,
    TelegramBotChat
)
from bots.services.telegrambot import TelegramBot
from bots.services.viberbot import ViberBot
from bots.services.vkbot import VKBot
from bots.services.fbbot import FBBot

import json


def bot_setwebhook(request):
    bot = TelegramBot()
    status = bot.set_webhook()
    bot = ViberBot()
    status = bot.set_webhook()
    return HttpResponse(status=status)


def bot_unsetwebhook(request):
    bot = TelegramBot()
    status = bot.unset_webhook()

    bot = ViberBot()
    status = bot.unset_webhook()
    return HttpResponse(status=status)


class AbstractViewBot(View):
    class Meta:
        abstract = True

    def get(self, request, *args, **kwargs):
        return HttpResponse(status=200)

    def post(self, request, *args, **kwargs):
        return self.bot.incoming(request)


class ViewTelegramBot(AbstractViewBot):

    def post(self, request, *args, **kwargs):
        return TelegramBotChat(request, *args, **kwargs).listen()


class ViewViberBot(AbstractViewBot):

    def post(self, request, *args, **kwargs):
        self.bot = AccountServiceWorkWithViber(request).bot
        return super().post(request, *args, **kwargs)


class ViewVKBot(AbstractViewBot):
    return_code = 'c040f32e'  # Строка, которую должен вернуть сервер

    def post(self, request, *args, **kwargs):
        if request.is_ajax:
            # если тип входящих данных 'confirmation', то это проверка
            # регистрации сервера в VK API.
            # Возращаем return_code  в которм указан код регистрации VK API
            data = camel_case_to_under_case(json.loads(request.body))
            if (data['type'] == 'confirmation'):
                return HttpResponse({self.return_code})
        self.bot = AccountServiceWorkWithVK(request).bot
        return super().post(request, *args, **kwargs)


class ViewFBBot(AbstractViewBot):
    return_code = 'c040f32e'  # Строка, которую должен вернуть сервер

    def get(self, request, *args, **kwargs):
        verify_token = request.GET.get('hub.verify_token', '')
        challenge = request.GET.get('hub.challenge', '')
        if verify_token:
            return HttpResponse({challenge})
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.bot = AccountServiceWorkWithFB(request).bot
        return super().post(request, *args, **kwargs)
