import json
from django.db import transaction
from django.shortcuts import get_object_or_404
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.response import Response


from bots.models import Bot
from bots.serializers import BotSerializer, BotWithAlgorithmSerializer
from bots.services import ProxyBotService

from core.errors import wrapper_error, ConflictError
from core.services import camel_case_to_under_case
from core.views import AbstractModelViewSetWithCurrentUser


class BotApiView(AbstractModelViewSetWithCurrentUser):
    model = Bot
    serializer_class = BotSerializer
    queryset = Bot.objects.all_active()

    def create_entry_point(self):
        return ProxyBotService(self.get_object()).create_entry_point()

    def get_entry_point(self):
        return ProxyBotService(self.get_object()).entry_point

    @wrapper_error
    @transaction.atomic
    def create(self, request):
        data = camel_case_to_under_case(json.loads(request.body))
        assert 'token' in data, 'Not set parameter token'

        if self.queryset.filter(token=data['token'], is_deleted=False).exists():
            raise ConflictError('Bot with this token is exists!')

        super().create(request)
        if not self.get_entry_point():
            self.create_entry_point()
        return self.get_serializer_class()(self.object).data

    @wrapper_error
    def update(self, request, pk=None):
        data = camel_case_to_under_case(json.loads(request.body))
        data['user'] = request.current_user
        self.get_model().objects.filter(id=pk).update(**data)
        return BotWithAlgorithmSerializer(
            self.get_model().objects.get(id=pk)
        ).data

    def partial_update(self, request, pk=None):
        return self.update(request, pk)

    @wrapper_error
    def retrieve(self, request, pk=None):
        return BotWithAlgorithmSerializer(
            get_object_or_404(self.get_model(), id=pk, is_deleted=False)
        ).data

    @wrapper_error
    def destroy(self, request, pk):
        ProxyBotService(pk).deactivate()
        object = get_object_or_404(self.get_model(), id=pk)
        object.activate = False
        object.save()
        return super().destroy(request, pk)

    @action(detail=True, methods=['post'])
    def activate(self, request, pk):
        """
        Активация бота
        """
        try:
            service = ProxyBotService(pk)
            service.activate()
        except Exception as e:
            json_ret = {
                'message': str(e),
                'code': status.HTTP_400_BAD_REQUEST,
                'status': 'ERROR'}
            return Response(json_ret, status=status.HTTP_400_BAD_REQUEST)

        return Response({'status': 'activate',
                         'webhook_info': service.get_webhook_info()})

    @action(detail=True, methods=['post'])
    def deactivate(self, request, pk):
        """
        Деактивация бота
        """
        try:
            ProxyBotService(pk).deactivate()
        except Exception as e:
            json_ret = {
                'message': str(e),
                'code': status.HTTP_400_BAD_REQUEST,
                'status': 'ERROR'}
            return Response(json_ret, status=status.HTTP_400_BAD_REQUEST)
        return Response({'status': 'deactivate'})

    @action(detail=True, methods=['post'])
    def start(self, request, pk):
        ProxyBotService(pk).start()

    @action(detail=True, methods=['get'])
    def info(self, request, pk):
        return Response({'info': ProxyBotService(pk).get_webhook_info()})
