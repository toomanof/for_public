from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.urls import path

from .views import *

app_name = 'bots'

urlpatterns = [
    path('setwebhook/', bot_setwebhook, name='bot_setwebhook'),
    path('unsetwebhook/', bot_unsetwebhook, name='bot_unsetwebhook'),
    path('incoming/<token>/telegram', csrf_exempt(ViewTelegramBot.as_view()), name="incoming_telegram"),
    path('incoming/viber', csrf_exempt(ViewViberBot.as_view()), name="incoming_viber"),
    path('incoming/vk', csrf_exempt(ViewVKBot.as_view()), name="incoming_vk"),
    path('incoming/fb', csrf_exempt(ViewFBBot.as_view()), name="incoming_fb"),
]
