from django.contrib.auth import login
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.urls import reverse_lazy
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode

from bots.services.constants import (
    MESSENGERS, LINKS_BOT_START,
    COMMAND_START, COMMAND_ACTIVE_ACCOUNT, COMMAND_AUTH,
    COMMAND_START_RU,
    TEXT_REGISTRATION, TEXT_OK_REGISTRATION, TEXT_GO_SITE, TEXT_ACTIVATE,
    KB_VIBER_ACTIVE_ACCOUNT, KB_VIBER_LINK_TO_SITE, KB_VK_LINK_TO_SITE,
    TB_TELEGRAM, TB_VIBER, TB_VK, TB_FB)

from bots.services.telegrambot import TelegramBot
from bots.services.viberbot import ViberBot
from bots.services.vkbot import VKBot
from bots.services.fbbot import FBBot

from core.services.constants import get_common_context
from core.services import AccountService

import binascii


def get_field_profile_id_chat(type_messanger):
    return f'id_bot_{MESSENGERS[type_messanger][1].lower()}'


def set_value_to_custom_field(_object, field_name, value):
    '''
    Метод присвоение объекту _object
    нового значения value, полю field_name.
    И запись объекта
    '''
    setattr(_object, field_name, value)
    print('set_value_to_custom_field', _object, field_name, value)
    _object.save()


class TokenActivationGenerator(PasswordResetTokenGenerator):
    def _make_hash_value(self, user, timestamp):
        return f'{user.pk}{timestamp}{user.is_active}'


class TokenGenerator(PasswordResetTokenGenerator):
    def _make_hash_value(self, user, timestamp):
        return f'{user.pk}{timestamp}'


account_activation_token = TokenActivationGenerator()
account_token = TokenGenerator()


class AccountServiceWorkWithMessengers:
    comand_start = COMMAND_START
    command_active_account = COMMAND_ACTIVE_ACCOUNT
    command_auth = COMMAND_AUTH

    '''
    Класс реализующий кастомную работу с ботами.
    '''
    def __init__(self, request, *args, **kwargs):
        self.request = request
        print('----------------- AccountServiceWorkWithMessangers', kwargs)
        if 'bot' in kwargs and 'type_bot' in kwargs:
            self.bot = kwargs.pop('bot')
            self.type_bot = kwargs.pop('type_bot')
            self.bot.set_command(self.comand_start,
                                 self.command_start_registration)
            self.bot.set_command(self.command_active_account,
                                 self.active_account)
            self.bot.set_command(self.command_auth,
                                 self.command_authentication)

    def get_user_and_chat_id(self):
        '''
        Метод возвращает ссылку автризованного входа на сайт
        '''
        chat_id = self.bot.get_chat_id()
        user = AccountService.get_user_by_id_chat(self.type_bot, chat_id)
        return user, chat_id

    def get_link_authentication_to_site(self, user, token):
        '''
        Метод возвращает ссылку автризованного входа на сайт
        '''
        common_context = get_common_context(self.request)
        uid = urlsafe_base64_encode(force_bytes(user.pk))
        path = reverse_lazy(
            'core:login_token',
            kwargs={'uidb64': uid, 'token': token})
        return f"{common_context['protocol']}://{common_context['domain']}{path}"

    def get_link_authentication_to_messenger(self):
        '''
        Метод возвращает ссылку автризованного входа на сайт
        в формате понятном ботам.
        '''
        user, errors = AccountService.get_user_by_login_or_email(self.request)
        if not user:
            return
        try:
            type_messanger = int(self.request.POST.get('messenger'))
        except (KeyError, TypeError):
            return

        token = account_token.make_token(user)
        uid = urlsafe_base64_encode(force_bytes(user.pk))
        return f'{LINKS_BOT_START[type_messanger]}={uid}_{token}'

    def get_link_registration_to_messenger(self):
        '''
        Метод отправки в выбранный месенджер ссылки на инициализацию старта
        регистрации пользователя через мессенджер
        В случае передачи параметра messenger выдается ссылка на регистрацию
        через выбранный месенджеер
        '''
        email = self.request.POST.get('email')
        try:
            type_messanger = int(self.request.POST.get('messenger'))
        except (KeyError, TypeError):
            return
        if not email and not type_messanger:
            return
        base64email = AccountService.get_base64email(email)
        return f'{LINKS_BOT_START[type_messanger]}={base64email}'

    def send_messange_all_subscribers(self, message):
        """
        Отсылает сообщение всем пользователям у которых если подписка
        на бот
        """
        chats_id_subscribers = AccountService.get_chats_id_all_subscriber_bots(
            self.type_bot)
        for chat_id in chats_id_subscribers:
            self.send_messange_subscriber(chat_id, message)

    def send_messange_subscriber(self, chat_id, message):
        self.bot.send_message_subscriber(chat_id, message)

    def _command_start_registration(self):
        '''
        Процедура регистрации клиента по переданному в формате base64
        email-у
        В случаее не соответсвия email-а стандартному формату производится
        попытка авторизации пользователя. Входной параметр подразумевается
        как переданный токен.
        '''
        paramentrs = self.bot.get_parametrs()
        print('132 ---- AccountServiceWorkWithMessangers _command_start_registration', paramentrs)
        if not paramentrs:
            return
        try:
            print('136 registration_with_params', paramentrs[0])
            email = AccountService.decode_email(paramentrs[0])
            print(' 138 email', email)
        except binascii.Error:
            return self.command_authentication()
        if not email:
            return
        chat_id = self.bot.get_chat_id()
        user = AccountService.get_or_create_user(email)
        user_name = self.bot.get_username()
        AccountService.set_username_first_name(user, user_name)
        AccountService.set_mesanger_id_chat(user, self.type_bot, chat_id)
        return True

    def _command_authentication(self):
        '''
        Процедура проверки переданных данных uidb64, token
        Поиск по этим данным пользователя и его авторизация
        В случае успеха передаётся ссылка на авторизацию на сайте
        '''
        paramentrs = self.bot.get_parametrs()
        if not paramentrs or len(paramentrs) < 2:
            return
        uidb64 = paramentrs[0]
        token = paramentrs[1]
        user = AccountService.get_user_by_token(uidb64=uidb64, token=token)
        if not user:
            return
        print('_command_authentication -----------------------', user)
        if not user.is_authenticated:
            login(self.request, user)
        if not user.username:
            user_name = self.bot.get_username()
            AccountService.set_username_first_name(user, user_name)
        chat_id = self.bot.get_chat_id()
        AccountService.set_mesanger_id_chat(user, self.type_bot, chat_id)
        return self.get_link_authentication_to_site(user, token)

    def _active_account(self):
        '''Метод активации пользователя на основании id чата'''
        user, id_chat = self.get_user_and_chat_id()
        print('_active_account', user, id_chat)
        if not user:
            return
        AccountService.set_active_user(user, True)
        login(self.request, user)
        return self.get_link_authentication_to_site(
            user, account_token.make_token(user))


class AccountServiceWorkWithTelegram(AccountServiceWorkWithMessengers):
    '''
    Класс реализующий кастомную работу с телеграм ботом.
    К методам родителя добавляются возратные кнопки
    и сообщения для пользователя
    '''

    def __init__(self, request, *args, **kwargs):
        kwargs['bot'] = TelegramBot()
        kwargs['type_bot'] = TB_TELEGRAM
        super().__init__(request, *args, **kwargs)

    def command_start_registration(self):
        '''Метод регистрации пользователя'''
        if not self._command_start_registration():
            return

        self.bot.set_keyboard([{'text': TEXT_ACTIVATE,
                                'callback_data': COMMAND_ACTIVE_ACCOUNT}])
        self.bot.text_messange =\
            TEXT_REGISTRATION.format(self.bot.get_username())

    def command_authentication(self):
        '''Метод авторизации пользователя'''
        link = self._command_authentication()
        if not link:
            return
        self.bot.set_keyboard([{
            'text': TEXT_GO_SITE,
            'url': link}])
        self.bot.text_messange = TEXT_OK_REGISTRATION

    def active_account(self):
        '''Метод активации пользователя'''
        link = self._active_account()
        if not link:
            return
        self.bot.set_keyboard([{'text': TEXT_GO_SITE,
                                'url': link}])
        self.bot.text_messange = TEXT_OK_REGISTRATION


class AccountServiceWorkWithViber(AccountServiceWorkWithMessengers):
    '''
    Класс реализующий кастомную работу с телеграм ботом.
    К методам родителя добавляются возратные кнопки
    и сообщения для пользователя
    '''

    def __init__(self, request, *args, **kwargs):
        kwargs['bot'] = ViberBot()
        kwargs['type_bot'] = TB_VIBER
        super().__init__(request, *args, **kwargs)

    def set_keyboard_with_link_to_site(self, link):
        if not link:
            return

        key_link_to_syte = KB_VIBER_LINK_TO_SITE.copy()
        key_link_to_syte['ActionBody'] = link
        self.bot.set_keyboard(
            {"Type": "keyboard",
                "Buttons": [key_link_to_syte]}
        )
        return self.bot.get_keyboard()

    def command_start_registration(self):
        '''Метод регистрации пользователя'''
        if not self._command_start_registration():
            return
        self.bot.set_keyboard(
            {"Type": "keyboard",
                "Buttons": [KB_VIBER_ACTIVE_ACCOUNT]}
        )
        self.bot.text_messange =\
            TEXT_REGISTRATION.format(self.bot.get_username())

    def command_authentication(self):
        '''Метод авторизации пользователя'''
        if not self.set_keyboard_with_link_to_site(
           self._command_authentication()):
            return
        self.bot.text_messange = TEXT_OK_REGISTRATION

    def active_account(self):
        '''Метод активации пользователя'''
        if not self.set_keyboard_with_link_to_site(self._active_account()):
            return
        self.bot.text_messange = TEXT_OK_REGISTRATION


class AccountServiceWorkWithVK(AccountServiceWorkWithMessengers):
    '''
    Класс реализующий кастомную работу с vk ботом.
    К методам родителя добавляются возратные кнопки
    и сообщения для пользователя
    '''
    comand_start = COMMAND_START_RU

    def __init__(self, request, *args, **kwargs):
        kwargs['bot'] = VKBot()
        kwargs['type_bot'] = TB_VK
        super().__init__(request, *args, **kwargs)

    def command_start_registration(self):
        '''Метод регистрации пользователя'''
        print('270 AccountServiceWorkWithVK command_start_registration')
        if not self._command_start_registration():
            return
        self.bot.set_keyboard([{'text': TEXT_ACTIVATE,
                                'callback_data': COMMAND_ACTIVE_ACCOUNT}])
        self.bot.text_messange =\
            TEXT_REGISTRATION.format(self.bot.get_username())

    def command_authentication(self):
        '''Метод авторизации пользователя'''
        link = self._command_authentication()
        if not link:
            return
        self.bot.set_keyboard([{
            'text': TEXT_GO_SITE,
            'url': link}])
        self.bot.text_messange = TEXT_OK_REGISTRATION

    def active_account(self):
        '''Метод активации пользователя'''
        link = self._active_account()
        if not link:
            return
        self.bot.set_keyboard([{'text': TEXT_GO_SITE,
                                'url': link}])
        self.bot.text_messange = TEXT_OK_REGISTRATION


class AccountServiceWorkWithFB(AccountServiceWorkWithMessengers):
    '''
    Класс реализующий кастомную работу с vk ботом.
    К методам родителя добавляются возратные кнопки
    и сообщения для пользователя
    '''

    def __init__(self, request, *args, **kwargs):
        kwargs['bot'] = FBBot()
        kwargs['type_bot'] = TB_FB
        super().__init__(request, *args, **kwargs)

    def command_start_registration(self):
        '''Метод регистрации пользователя'''
        if not self._command_start_registration():
            return
        self.bot.set_keyboard([{'text': TEXT_ACTIVATE,
                                'callback_data': COMMAND_ACTIVE_ACCOUNT}])
        self.bot.text_messange =\
            TEXT_REGISTRATION.format(self.bot.get_username())

    def command_authentication(self):
        '''Метод авторизации пользователя'''
        if not self.set_keyboard_with_link_to_site(
           self._command_authentication()):
            return
        self.bot.text_messange = TEXT_OK_REGISTRATION

    def active_account(self):
        '''Метод активации пользователя'''
        if not self.set_keyboard_with_link_to_site(self._active_account()):
            return
        self.bot.text_messange = TEXT_OK_REGISTRATION
