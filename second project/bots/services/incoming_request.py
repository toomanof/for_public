import logging
import json
from django.http import HttpResponse
from telebot import TeleBot
from telebot import types as telegram_types

from bots.services.constants import TB_TELEGRAM, MESSENGERS
from core.services import BaseService


logger_error = logging.getLogger('warning')
logger_debug = logging.getLogger('debug')


class BotIncomingRequestService(BaseService):
    keyboard = None
    dict_message = None

    def incoming(self):
        logger_debug.debug('----BotIncomingRequestService bot incoming -----')
        self.dict_message = self.get_dict_message()
        logger_debug.debug(f'----BotIncomingRequestService dict_message: {self.dict_message}')
        self.set_type_query()
        logger_debug.debug("HttpResponse('ok', status=200)")
        return HttpResponse('ok', content_type="text/plain", status=200)

    def add_button_to_keyboard(self, button):
        pass

    def clear_keyboard(self):
        self.keyboard = None

    def process_input_data(self):
        pass

    def set_keyboard(self, keys_params):
        self.clear_keyboard()
        for key_params in keys_params:
            self.add_button_to_keyboard(**key_params)

    def find_step_in_algorithm(self,):
        pass

    def get_call_data(self):
        pass

    def get_chat_id(self):
        pass

    def get_dict_message(self):
        pass

    def get_field_command_bot_component(self):
        return f'command_{MESSENGERS[self.type_bot][1].lower()}'

    def get_keyboard(self):
        pass

    def get_text_message(self):
        pass

    def next_step(self):
        pass

    def set_type_query(self):
        pass

    def send_message(self):
        pass


class TelegramBotIncomingRequestService(BotIncomingRequestService):
    Q_BOT_COMMAND, Q_MESSAGE, Q_CALLBACK = range(3)
    type_query = None
    dict_message = {}
    type_bot = TB_TELEGRAM

    def add_button_to_keyboard(self, text,
                               callback_data=None, url=None):
        self.add_button_to_inline_keyboard(text, callback_data, url)

    def add_button_to_inline_keyboard(self, text,
                                      callback_data=None, url=None):
        self.keyboard = telegram_types.InlineKeyboardMarkup()
        self.keyboard.add(telegram_types.InlineKeyboardButton(
            text=text, callback_data=callback_data, url=url))

    def get_chat_id(self):
        if self.type_query == self.Q_MESSAGE:
            return self.dict_message['message']['chat']['id']
        elif self.type_query == self.Q_CALLBACK:
            return self.dict_message['Q_CALLBACK']['message']['chat']['id']

    def get_chat_text(self):
        if self.type_query == self.Q_MESSAGE:
            return self.dict_message['message']['text']
        elif self.type_query == self.Q_CALLBACK:
            return self.dict_message['Q_CALLBACK']['message']['text']

    def get_chat_username(self):
        if self.type_query == self.Q_MESSAGE:
            return self.dict_message['message']['chat']['username']
        elif self.type_query == self.Q_CALLBACK:
            return self.dict_message['Q_CALLBACK']['message']['chat']['username']

    def get_sdk_bot(self,):
        logger_debug.debug('MixinTelegramBotIncomingRequestService add_sdk_bot ---')
        return TeleBot(self.token_bot)

    def get_dict_message(self):
        return json.loads(self.request.body.decode('utf-8'))

    def get_text_message(self):
        return self.dict_message['message'].get('text', None) if self.dict_message else ''

    def set_type_query(self):
        if 'callback_query' in self.dict_message:
            self.type_query = self.Q_CALLBACK
        elif 'entities' in self.dict_message:
            self.type_query = self.Q_BOT_COMMAND
        else:
            self.type_query = self.Q_MESSAGE

    def send_message(self, text, save_msg=True):
        chat_id = self.get_chat_id()
        if not chat_id:
            return
        try:
            logger_debug.debug(f'MixinTelegramBotIncomingRequestService send_message {text}')
            self.get_sdk_bot().send_message(
                chat_id,
                text,
                reply_markup=self.get_keyboard())
            if save_msg:
                self.save_incoming(text)
        except Exception as e:
            logger_error.error(str(e))

