import json
#import vk

from django.conf import settings
from django.http import JsonResponse
from django.http import HttpResponse
from vk_api.vk_api import VkApi
from vk_api.keyboard import VkKeyboard, VkKeyboardColor
from vk_api.utils import get_random_id

from .constants import (
    PATH_INCOMING_BOT,
    TB_TELEGRAM, TB_VIBER, TB_VK, TB_FB,
    TOKEN_VIBER, TOKEN_TELEGRAM, TEXT_ERROR,
    COMMAND_START)

from core.services import camel_case_to_under_case
from telebot import TeleBot, apihelper
from telebot import types as telegram_types
from viberbot import Api
from viberbot.api.bot_configuration import BotConfiguration
from viberbot.api.messages.text_message import TextMessage
from viberbot.api.viber_requests import (
    ViberMessageRequest, ViberSubscribedRequest,
    ViberConversationStartedRequest)

from fbchat import Client as FBClient
from fbchat.models import Message as FBMessage
from fbchat.models import ThreadType as FBThreadType


class AbstractBot:
    """
    Абстрактный класс ботов.
    По умолчанию тип бота телеграм
    """
    HTTP_Response, Json_Response = range(2)
    type_bot = TB_TELEGRAM
    type_query = None
    default_text_messange = TEXT_ERROR
    commands = {}
    dict_message = {}
    parametrs = {}
    keyboard = None
    type_response = HTTP_Response

    def add_button_to_keyboard(self, button):
        pass

    def callback(self):
        self.clear_keyboard()
        self.text_messange = self.default_text_messange
        command = self.split_command_and_params()
        print('52 AbstractBot callback', command, self.commands)
        if command in self.commands and self.commands[command]:
            self.commands[command]()

    def clear_keyboard(self):
        self.keyboard = None

    def incoming(self, request):
        print('------------ bot incoming -----')
        self.request = request
        self.dict_message = self.get_dict_message()
        print(self.dict_message)
        self.set_type_query()
        self.callback()
        self.send_message()
        if self.type_response == self.HTTP_Response:
            print("HttpResponse('ok', status=200)")
            return HttpResponse('ok', content_type="text/plain", status=200)
        else:
            print("JsonResponse('ok', status=200)")
            return JsonResponse({}, status=200)

    def get_call_data(self):
        pass

    def get_command(self):
        pass

    def get_chat_id(self):
        pass

    def get_dict_message(self):
        pass

    def get_keyboard(self):
        return self.keyboard

    def get_parametrs(self):
        return self.parametrs

    def get_text_messange(self):
        return self.text_messange

    def get_username(self):
        pass

    def get_webhook_info(self):
        pass

    def send_message(self):
        pass

    def send_message_subscriber(self, chat_id, message):
        self.chat_id = chat_id
        self.text_messange = message
        self.send_message()

    def set_command(self, command, callback):
        self.commands[command.lower()] = callback

    def set_keyboard(self, keys_params):
        self.clear_keyboard()
        for key_params in keys_params:
            self.add_button_to_keyboard(**key_params)

    def set_type_query(self):
        pass

    def set_webhook(self):
        pass

    def split_command_and_params(self):
        command_with_paramentrs = self.get_command()
        if command_with_paramentrs:
            print('---------split_command_and_params', command_with_paramentrs)
            if isinstance(command_with_paramentrs, list):
                if len(command_with_paramentrs) == 2:
                    self.parametrs = command_with_paramentrs[1]
                return command_with_paramentrs[0].lower()
            else:
                return command_with_paramentrs.lower()

    def unset_webhook(self):
        pass


class AbstractTelegramBot(AbstractBot):
    Q_MESSAGE, Q_CALLBACK = range(2)
    type_bot = TB_TELEGRAM
    bot = None

    def __init__(self, token):
        self.bot = TeleBot(token)

    def add_button_to_keyboard(self, text,
                               callback_data=None, url=None):
        self.add_button_to_inline_keyboard(text, callback_data, url)

    def add_button_to_inline_keyboard(self, text,
                                      callback_data=None, url=None):
        self.keyboard = telegram_types.InlineKeyboardMarkup()
        self.keyboard.add(telegram_types.InlineKeyboardButton(
            text=text, callback_data=callback_data, url=url))

    def get_call_data(self):
        if self.type_query == self.Q_CALLBACK:
            return self.dict_message['Q_CALLBACK']['data']

    def get_command(self):
        if self.type_query == self.Q_CALLBACK:
            return self.get_call_data()
        return self.dict_message['message'].get('text').split(" ")

    def get_chat_id(self):
        if hasattr(self, 'chat_id'):
            return self.chat_id
        if self.type_query == self.Q_MESSAGE:
            return self.dict_message['message']['chat']['id']
        elif self.type_query == self.Q_CALLBACK:
            return self.dict_message['Q_CALLBACK']['message']['chat']['id']

    def get_dict_message(self):
        raw = self.request.body.decode('utf-8')
        return json.loads(raw)

    def get_parametrs(self):
        try:
            result = self.parametrs.split('_')
        except Exception:
            result = None
        return result

    def get_username(self):
        return self.get_dict_message()['message']['from']['username']

    def get_webhook_info(self):
        return apihelper.get_webhook_info(self.bot.token)

    def send_message(self):
        chat_id = self.get_chat_id()
        if not chat_id:
            return
        try:
            self.bot.send_message(
                chat_id,
                self.text_messange,
                reply_markup=self.get_keyboard())
        except:
            pass

    def set_type_query(self):
        if 'callback_query' in self.dict_message:
            self.type_query = self.Q_CALLBACK
        self.type_query = self.Q_MESSAGE

    def set_webhook(self, domain_name):
        self.bot.set_webhook(
            f'https://{settings.BACKEND_SITE_DOMAIN}{PATH_INCOMING_BOT}{self.bot.token}/telegram')

    def unset_webhook(self):
        self.bot.delete_webhook()


class AbstractViberBot(AbstractBot):
    type_bot = TB_VIBER
    bot = Api(BotConfiguration(
            name='toomanof',
            avatar='https://grain.club/static/img/logo_light.png',
            auth_token=TOKEN_VIBER))

    def get_command(self):
        if (self.type_query == ViberMessageRequest
           and isinstance(self.dict_message, ViberMessageRequest)):
            return self.dict_message.message.text.split(" ")
        if isinstance(self.dict_message, ViberConversationStartedRequest):
            return COMMAND_START

    def get_chat_id(self):
        if self.chat_id:
            return self.chat_id
        if isinstance(self.dict_message, ViberMessageRequest):
            return self.dict_message.sender.id
        elif isinstance(self.dict_message, ViberConversationStartedRequest):
            return self.dict_message.user.id

    def get_dict_message(self):
        return self.bot.parse_request(self.request.body)

    def get_keyboard(self):
        return self.keyboard

    def get_parametrs(self):
        if hasattr(self.dict_message, 'context'):
            return self.dict_message.context.split('_')

    def get_username(self):
        print(' 225 get_username', self.dict_message)
        if isinstance(self.dict_message, ViberMessageRequest):
            return self.dict_message.sender.name
        elif isinstance(self.dict_message, ViberConversationStartedRequest):
            return self.dict_message.user.name

    def set_keyboard(self, keyboard):
        self.keyboard = keyboard

    def send_message(self):
        chat_id = self.get_chat_id()
        if not chat_id:
            return
        answer_messages = [TextMessage(text=self.text_messange,
                                       keyboard=self.get_keyboard())]
        try:
            self.bot.send_messages(chat_id, answer_messages)
        except:
            pass

    def set_type_query(self):
        if (isinstance(self.dict_message, ViberMessageRequest)
           or isinstance(self.dict_message, ViberSubscribedRequest)):
            self.type_query = ViberMessageRequest
        elif isinstance(self.dict_message, ViberSubscribedRequest):
            self.type_query = ViberSubscribedRequest
        elif isinstance(self.dict_message, ViberConversationStartedRequest):
            self.type_query = ViberConversationStartedRequest

    def set_webhook(self):
        self.bot.set_webhook(
            f'https://{settings.SITE_DOMAIN}{PATH_INCOMING_BOT}viber')

    def unset_webhook(self):
        self.bot.unset_webhook()

    def split_command_and_params(self):
        self.parametrs = self.get_parametrs()
        if self.parametrs:
            return COMMAND_START
        return super().split_command_and_params()


class AbstractVKBot(AbstractBot):
    Q_TYPE_STATE, Q_MESSAGE, Q_MESSAGE_REPLY = range(3)
    type_bot = TB_VK
    vk_session = VkApi(#login=settings.VK_TEL_NUMBER,
#                              password=settings.VK_PASSWORD,
                              token=settings.TOKEN_VK,
                              scope='4096|8192')

#    vk_session = vk.Session(access_token=settings.TOKEN_VK)
#    bot = vk.API(vk_session, v=5.103)
    bot = vk_session.get_api()

    def __init__(self):
        self.type_response = self.HTTP_Response
        super().__init__()

    def add_button_to_keyboard(self, text,
                               callback_data=None, url=None):
        if callback_data:
            self.keyboard.add_button(
                text, color=VkKeyboardColor.DEFAULT, payload=callback_data)
        elif url:
            self.keyboard.add_openlink_button(text, url)

    def get_command(self):
        if self.type_query == self.Q_MESSAGE:
            return self.dict_message['object']['message']['text']

    def get_chat_id(self):
        if self.chat_id:
            return self.chat_id
        if self.type_query == self.Q_MESSAGE:
            return self.dict_message['object']['message']['from_id']
        if self.type_query == self.Q_MESSAGE_REPLY:
            return self.dict_message['object']['peer_id']
        if self.type_query == self.Q_TYPE_STATE:
            return self.dict_message['object']['from_id']

    def get_dict_message(self):
        raw = self.request.body.decode('utf-8')
        raw = camel_case_to_under_case(json.loads(raw))
        if not raw or 'type' not in raw:
            return
        return raw

    def get_keyboard(self):
        return self.keyboard.get_keyboard()

    def set_keyboard(self, keys_params):
        self.clear_keyboard()
        self.keyboard = VkKeyboard(one_time=False)
        for key_params in keys_params:
            self.add_button_to_keyboard(**key_params)

    def get_user_id(self):
        if self.type_query == self.Q_MESSAGE:
            return self.dict_message['object']['message']['from_id']

    def get_parametrs(self):
        print('320 AbstractVKBot get_parametrs')
        if self.type_query == self.Q_MESSAGE:
            try:
                return self.dict_message['object']['message']['ref'].split('_')
            except Exception:
                return

    def set_type_query(self):
        self.type_query = None
        if self.dict_message['type'] == 'message_typing_state':
            self.type_query = self.Q_TYPE_STATE
        if self.dict_message['type'] == 'message_new':
            self.type_query = self.Q_MESSAGE
        if self.dict_message['type'] == 'message_reply':
            self.type_query = self.Q_MESSAGE_REPLY

    def send_message(self):
        chat_id = self.get_chat_id()
        print(chat_id, self.type_query)
        if not chat_id or self.type_query != self.Q_MESSAGE:
            return
        print('send_message', self.text_messange, self.get_keyboard())
        try:
            self.bot.messages.send(
                message=self.text_messange,
                random_id=get_random_id(),
                keyboard=self.get_keyboard(),
                user_id=chat_id)
        except:
            pass


class AbstractFBBot(AbstractBot):
    Q_TYPE_STATE, Q_MESSAGE, Q_MESSAGE_REPLY = range(3)
    type_bot = TB_FB
    bot = None
# Закоменчено до запуска на тестовом сервере - логинится при запуске приложения
#    bot = FBClient(settings.FB_LOGIN,
#                   settings.FB_PASSWORD)

    def add_button_to_keyboard(self, text,
                               callback_data=None, url=None):
        if callback_data:
            self.keyboard.add_button(
                text, color=VkKeyboardColor.DEFAULT, payload=callback_data)
        elif url:
            self.keyboard.add_openlink_button(text, link=url)

    def get_command(self):
        if self.type_query == self.Q_MESSAGE:
            return self.dict_message['object']['message']['text']
        if self.type_query == self.Q_MESSAGE_REPLY:
            return self.dict_message['object']['text']

    def get_chat_id(self):
        if self.chat_id:
            return self.chat_id
        if self.type_query == self.Q_MESSAGE:
            return self.dict_message['object']['message']['from_id']
        if self.type_query == self.Q_MESSAGE_REPLY:
            return self.dict_message['object']['from_id']

    def get_dict_message(self):
        raw = self.request.body.decode('utf-8')
        raw = camel_case_to_under_case(json.loads(raw))
        print('------------AbstractVKBot-------------', raw)
        if not raw or 'type' not in raw:
            return
        return raw

    def get_keyboard(self):
        return self.keyboard

    def set_keyboard(self, keys_params):
        self.clear_keyboard()
        self.keyboard = VkKeyboard(one_time=False)
        for key_params in keys_params:
            self.add_button_to_keyboard(**key_params)

    def set_type_query(self):
        if self.dict_message['type'] == 'message_typing_state':
            self.type_query = self.Q_TYPE_STATE
        if self.dict_message['type'] == 'message_new':
            self.type_query = self.Q_MESSAGE
        if self.dict_message['type'] == 'message_reply':
            self.type_query = self.Q_MESSAGE_REPLY

    def send_message(self):
        chat_id = self.get_chat_id()
        if not chat_id:
            return
        print('send_message', self.text_messange)
        try:
            self.bot.send(
                FBMessage(text=self.text_messange),
                thread_id=settings.ID_FB,
                thread_type=FBThreadType.USER)
        except:
            pass
