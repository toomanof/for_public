import logging
from bots.models import CurrentAlgorithmStep, ReestrChatTelegram, ValueVariableInBot


logger_debug = logging.getLogger('debug')


class MixinWorkingWithChatRecords:

    def get_dict_current_bot(self):
        logger_debug.debug('get_dict_current_bot')
        return {
            'bot': self.current_bot,
            'bot_algorithm_step': self.current_bot_algorithm_step
        }

    def get_dict_chat(self, text=None):
        logger_debug.debug('get_dict_chat')
        if self.current_bot_algorithm_step.is_user_input\
           or self.current_bot_algorithm_step.is_start:
            direction = ReestrChatTelegram.INCOMING
            text = self.get_chat_text()
        else:
            direction = ReestrChatTelegram.OUTGOING
            text = text
            self.get_current_algorithm_step()
        current_algorithm_step = self.get_record_current_algorithm_step()
        return {
            'dialog': current_algorithm_step.dialog if current_algorithm_step else None,
            'id_bot': self.get_chat_id(),
            'username': self.get_chat_username(),
            'direction': direction,
            'text': text
        }

    def get_records(self):
        return ReestrChatTelegram.objects.filter(**self.get_dict_current_bot())

    def get_record_current_algorithm_step(self):
        return CurrentAlgorithmStep.objects.filter(
            bot=self.current_bot,
            id_bot=self.get_chat_id()
        ).first()

    def get_current_algorithm_step(self):
        logger_debug.debug(f'MixinWorkingWithChatRecords get_current_algorithm_step'
                           f'{self.current_bot}, {self.get_chat_id()} ')

        result = self.get_record_current_algorithm_step()
        logger_debug.debug(
            f'MixinWorkingWithChatRecords get_current_algorithm_step result {result}')
        return result.bot_algorithm_step if result else None

    def delete_record_current_algorithm_step(self):
        CurrentAlgorithmStep.objects.filter(
            bot=self.current_bot,
            id_bot=self.get_chat_id()
        ).delete()

    def save_current_algorithm_step(self):
        logger_debug.debug(f'MixinWorkingWithChatRecords save_current_algorithm_step: '
                           f'{self.current_bot}, {self.current_bot_algorithm_step}')
        if not self.current_bot_algorithm_step:
            return

        CurrentAlgorithmStep.objects.update_or_create(
            id_bot=self.get_chat_id(),
            bot=self.current_bot,
            defaults={'bot_algorithm_step': self.current_bot_algorithm_step}
        )

    def save_fallback(self, step, text):
        record_current_algorithm_step = self.get_record_current_algorithm_step()

        kwargs = {
            'dialog': record_current_algorithm_step.dialog if record_current_algorithm_step else None,
            'id_bot': self.get_chat_id(),
            'username': self.get_chat_username(),
            'direction': ReestrChatTelegram.OUTGOING,
            'text': text
        }
        self.save_record_chat(**kwargs)

    def save_incoming(self, text=None):
        if not self.dict_message:
            return
        self.save_record_chat(**self.get_dict_chat(text))

    def save_record_chat(self, **kwargs):
        if kwargs:
            kwargs.update(self.get_dict_current_bot())
        logger_debug.debug(f'MixinWorkingWithChatRecords save_record_chat {kwargs}')
        ReestrChatTelegram.objects.update_or_create(**kwargs)

    def save_values_variables_in_base(self, variables):
        for key, val in variables.items():
            ValueVariableInBot.objects.update_or_create(
                id_bot=self.get_chat_id(),
                bot=self.current_bot,
                name=key,
                defaults={'value': val}
            )

    def get_variables(self):
        result = ValueVariableInBot.objects.filter(
            id_bot=self.get_chat_id(),
            bot=self.current_bot,
        ).values_list('name', 'value')
        return {'variables': {item[0]: item[1] for item in result}}
