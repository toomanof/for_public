import re
import logging
from django.http import HttpResponse

from bots.models import Bot, BotDialogAlgorithm, ValueVariableInBot
from .incoming_request import TelegramBotIncomingRequestService
from .working_chat_records import MixinWorkingWithChatRecords

logger_error = logging.getLogger('warning')
logger_debug = logging.getLogger('debug')


def get_variables_in_template_response_body(template):
    regex = r"{{(\w*)}}"
    result = re.findall(regex, template)
    logger_debug.debug(f"get_variables_in_template_response_body {result}")
    return result


def sub_values_to_template_response_body(variables, template):
    logger_debug.debug(f"sub_values_to_template_response_body before template: {template} {variables}")
    for key, value in variables.items():
        template = re.sub(f"{{{{{key}}}}}", value, template)
    logger_debug.debug(f"sub_values_to_template_response_body after template: {template}")
    return template


class CoreBotChat:
    current_bot = None
    current_bot_algorithm_step = None
    token_bot = None
    flag_stop_execute: bool = False
    request = None
    tmpInputValue = None
    childs_filter = None

    def __init__(self, request, *args, **kwargs):
        logger_debug.debug('CoreBotChat __init__')
        self.request = request
        self.token_bot = kwargs.get('token', None)
        if self.token_bot is None:
            logger_error.error('Init CoreBotChat not possible with None token!')
        try:
            self.current_bot = Bot.objects.get(token=self.token_bot,
                                               is_deleted=False)
            logger_debug.debug(f'current_bot_algorithm_step {self.current_bot_algorithm_step}')
        except Exception as e:
            logger_error.error(f'Not found bot with token {self.token_bot}! {e}')

    def listen(self):
        self.incoming()
        self.set_current_step_bot_algorithm()
        self.save_current_algorithm_step()
        self.parser_input_text()
        self.save_incoming(self.tmpInputValue)
        self.execute()
        return HttpResponse(status=200)

    @property
    def current_step_component(self):
        return self.current_bot_algorithm_step.command_object

    @property
    def current_component_is_wait_input(self):
        return self.current_step_component.await_input

    def execute(self):
        """
        Основной цикл алгоритма
        """
        self.flag_stop_execute = False
        try:
            while not self.flag_stop_execute:
                logger_debug.debug(f'start while execute current_bot_algorithm_step {self.current_bot_algorithm_step}')
                self.checking_type_current_bot_algorithm_step()
                self.save_current_algorithm_step()

            logger_debug.debug('Exiting the loop! Awaiting user input.')
            if self.current_bot_algorithm_step.is_stop:
                self.delete_record_current_algorithm_step()
        except Exception as e:
            logger_error.error(f'Wrong error from execute algorithm chat bot: {e}')

    def execute_previous_fallback(self):
        logger_debug.debug(' - before execute_previous_fallback')
        fallback_step = self.current_bot_algorithm_step
        parent = self.current_bot_algorithm_step.parent
        logger_debug.debug(f' first result {parent}')
        while not parent.is_fallback:
            tmp_result = parent.childs.filter(
                is_deleted=False, command=BotDialogAlgorithm.CMD_FALLBACK).first()
            logger_debug.debug(f' tmp_result  in loop {tmp_result}')
            if tmp_result is None:
                parent = parent.parent
                if parent is None:
                    break
            else:
                response_fallback = tmp_result.childs.all_active().first()
                text_msg = response_fallback.command_object.body if response_fallback else tmp_result.command_object.default_msg
                self.send_message(text_msg, False)
                self.save_fallback(fallback_step, text_msg)
                break
        self.stop_execution(False)

    def execute_current_step(self):
        logger_debug.debug(f'execute_current_step {self.current_bot_algorithm_step}')
        if (self.current_bot_algorithm_step.is_response and
                not self.current_bot_algorithm_step.is_deleted):
            body = self.render_response_body(self.current_step_component.body)
            self.send_message(body)
        elif (self.current_bot_algorithm_step.is_stop and
                not self.current_bot_algorithm_step.is_deleted):
            body = self.render_response_body(self.current_step_component.text)
            self.send_message(body)

        else:
            logger_debug.debug('current_step_component.execute()')
            self.current_step_component.execute()

    def checking_type_current_bot_algorithm_step(self):
        """
        С алгоритма диалога выбирается текущий шаг
        и выполняются шаги следующие после него
        """
        logger_debug.debug(f'before checking_type_current_bot_algorithm_step {self.current_bot_algorithm_step}')
        if self.current_bot_algorithm_step.is_start:
            logger_debug.debug('current_bot_algorithm_step is_start')
            self.execute_current_step()

        elif self.current_bot_algorithm_step.is_stop:
            logger_debug.debug('current_bot_algorithm_step is_stop')
            self.stop_execution()
            return

        elif self.current_bot_algorithm_step is None:
            logger_debug.debug('current_bot_algorithm_step is None got to start')
            self.go_to_previous_user_input()
            return

        elif self.current_bot_algorithm_step.is_response:
            logger_debug.debug('current_bot_algorithm_step is_response')
            self.execute_current_step()

        elif self.current_bot_algorithm_step.is_set_variable:
            logger_debug.debug('current_bot_algorithm_step is_set_variable')
            variables = self.current_step_component.execute(self.tmpInputValue)
            self.save_values_variables_in_base(variables)
            logger_debug.debug(f' - set_variable: {variables}')

        elif self.current_bot_algorithm_step.is_user_input:
            logger_debug.debug('current_bot_algorithm_step is_user_input')
            self.execute_current_step()
            self.parser_input_text()
            if self.tmpInputValue is None:
                logger_debug.debug('Not parsersed! tmpInputValue is None')
                self.execute_previous_fallback()
                return
            else:
                logger_debug.debug('tmpInputValue is parsed')
                self.childs_filter = self.get_childrens_filter()
                logger_debug.debug(f'childs_filter {self.childs_filter}')

                is_find_in_filter = False

                for child_filter in self.childs_filter:
                    logger_debug.debug(
                        f'Сверяется {child_filter} '
                        f'{child_filter.command_object.value} in '
                        f'{self.tmpInputValue}')

                    is_find_in_filter = child_filter.command_object.execute(expression=self.tmpInputValue)
                    logger_debug.debug(f'is_find_in_filter {is_find_in_filter}')

                    if is_find_in_filter:
                        logger_debug.debug(f'Шаг {child_filter} подошел переходим на него')
                        self.set_current_step_bot_algorithm_by_id(child_filter.id)
                        break

                childrens_response = self.get_childrens_response()
                childrens_set_v = self.get_childrens_set_variable()
                childrens_goto = self.get_childrens_goto()
                childrens_stop = self.get_childrens_stop()
                if not childrens_stop and\
                        not childrens_goto and\
                        not childrens_set_v and\
                        not childrens_response and\
                        not is_find_in_filter:
                    self.execute_previous_fallback()
                    return

        elif self.current_bot_algorithm_step.is_goto:
            logger_debug.debug('current_bot_algorithm_step is_goto')
            self.goto_step()
            return

        elif self.current_bot_algorithm_step.is_filter:
            logger_debug.debug('current_bot_algorithm_step is_filter')
            logger_debug.debug(f'filtering self.tmpInputValue {self.tmpInputValue}')
            if not self.current_step_component.execute(expression=self.tmpInputValue):
                self.execute_previous_fallback()

        logger_debug.debug(f'after checking_type_current_bot_algorithm_step {self.current_bot_algorithm_step}')
        self.next_current_bot_algorithm_step()

    def set_current_step_bot_algorithm_by_id(self, id):
        """
        Установка текущего шага диалога по его id
        """
        self.current_bot_algorithm_step =\
            self.get_record_bot_dialog_algorithm(id=id).first()
        logger_debug.debug(f'set_current_bot_algorithm_step_by_id {self.current_bot_algorithm_step}')

    def set_current_step_bot_algorithm(self):
        self.current_bot_algorithm_step = self.get_current_algorithm_step()
        logger_debug.debug(f'set_current_step_bot_algorithm in database {self.current_bot_algorithm_step}')
        if not self.current_bot_algorithm_step:
            self.set_start_step_bot_algorithm()
        logger_debug.debug(f'set_current_step_bot_algorithm start {self.current_bot_algorithm_step}')

    def set_start_step_bot_algorithm(self):
        self.current_bot_algorithm_step = \
            self.get_record_bot_dialog_algorithm(
                command=BotDialogAlgorithm.CMD_START).first()
        logger_debug.debug(f'set_start_step_bot_algorithm start {self.current_bot_algorithm_step}')

    def get_record_bot_dialog_algorithm(self, **filter_params):
        filter_params['bot_id'] = self.current_bot.id
        filter_params['is_deleted'] = False
        logger_debug.debug(f'get_record_bot_dialog_algorithm {self.current_bot_algorithm_step}')
        return BotDialogAlgorithm.objects.filter(**filter_params)

    def next_current_bot_algorithm_step(self):
        logger_debug.debug(f' - before next_current_bot_algorithm_step {self.current_bot_algorithm_step}')

        tmp_bot_algorithm_step = self.current_bot_algorithm_step.childs.all_active().first()

        if tmp_bot_algorithm_step is None:
            logger_debug.error(f'Step {self.current_bot_algorithm_step} not have childs!')
            self.go_to_previous_user_input()
            return

        self.current_bot_algorithm_step = tmp_bot_algorithm_step
        if self.current_bot_algorithm_step.is_user_input:
            logger_debug.debug(f'Step {self.current_bot_algorithm_step} is input! Waiting')
            self.stop_execution(False)

        logger_debug.debug(f' - after next_current_bot_algorithm_step {self.current_bot_algorithm_step}')

    def goto_step(self):
        id_step = getattr(self.current_step_component, 'id_goto_step', '')
        logger_debug.debug(f'goto_step {id_step}')
        self.set_current_step_bot_algorithm_by_id(id_step)

    def go_to_previous_user_input(self):
        tmp_step = self.current_bot_algorithm_step
        logger_debug.debug(f'before go_to_previous_user_input current_bot_algorithm_step {tmp_step}')
        while tmp_step:
            tmp_step = tmp_step.parent
            logger_debug.debug(
                f'while tmp_step {tmp_step}')
            if tmp_step.is_user_input or tmp_step is None:
                break

        if tmp_step:
            self.current_bot_algorithm_step = tmp_step
            self.save_current_algorithm_step()
        logger_debug.debug(
            f'after go_to_previous_user_input current_bot_algorithm_step {tmp_step}')
        self.stop_execution()

    def select_component_from_current_step(self):
        pass

    def stop_execution(self, execute_step=True):
        self.flag_stop_execute = True
        if execute_step:
            self.execute_current_step()
        logger_debug.debug(f' - stop_execution {self.current_bot_algorithm_step} , {self.flag_stop_execute}')

    def parser_input_text(self):
        self.tmpInputValue = self.get_text_message()
        logger_debug.debug(f' - parser_input_text {self.tmpInputValue}')

    def get_childrens_filter(self):
        childs = self.current_bot_algorithm_step.childs.filter(command=BotDialogAlgorithm.CMD_FILTER, is_deleted=False)
        logger_debug.debug(f'get_childrens_filter current_bot_algorithm_step {self.current_bot_algorithm_step} childs {childs}')
        return childs

    def get_childrens_response(self):
        childs = self.current_bot_algorithm_step.childs.filter(command=BotDialogAlgorithm.CMD_RESPONSE, is_deleted=False)
        logger_debug.debug(f'get_childrens_response current_bot_algorithm_step {self.current_bot_algorithm_step} childs {childs}')
        return childs

    def get_childrens_set_variable(self):
        childs = self.current_bot_algorithm_step.childs.filter(command=BotDialogAlgorithm.CMD_SET_VAR, is_deleted=False)
        logger_debug.debug(f'get_childrens_set_variable current_bot_algorithm_step {self.current_bot_algorithm_step} childs {childs}')
        return childs

    def get_childrens_goto(self):
        childs = self.current_bot_algorithm_step.childs.filter(command=BotDialogAlgorithm.CMD_GOTO, is_deleted=False)
        logger_debug.debug(f'get_childrens_goto current_bot_algorithm_step {self.current_bot_algorithm_step} childs {childs}')
        return childs

    def get_childrens_stop(self):
        childs = self.current_bot_algorithm_step.childs.filter(command=BotDialogAlgorithm.CMD_STOP, is_deleted=False)
        logger_debug.debug(f'get_childrens_stop current_bot_algorithm_step {self.current_bot_algorithm_step} childs {childs}')
        return childs

    def render_response_body(self, template):
        logger_debug.debug(f"render_response_body before {template}")
        variables = get_variables_in_template_response_body(template)
        values_variables = self.get_values_variables(variables)
        return sub_values_to_template_response_body(values_variables, template)

    def get_values_variables(self, variables):
        result = ValueVariableInBot.objects.filter(
            id_bot=self.get_chat_id(),
            bot=self.current_bot,
            name__in=variables
        ).values_list('name', 'value')
        logger_debug.debug(f"get_values_variables queryset: {result}")
        result = {item[0]: item[1] for item in result}
        logger_debug.debug(f"get_values_variables dict: {result}")
        return result


class TelegramBotChat(MixinWorkingWithChatRecords,
                      TelegramBotIncomingRequestService, CoreBotChat):
    pass
