from django.conf import settings
from django.db.models import Model
from django.shortcuts import get_object_or_404
from django.utils.functional import cached_property

from bots.models import Bot, BotDialogAlgorithm
from bots.serializers import BotSerializer

from core.services import BaseService


class ProxyBotService(BaseService):
    model = Bot
    serializers = BotSerializer
    object = None

    def __init__(self, id_bot):
        if isinstance(id_bot, Model):
            self.object = id_bot
        else:
            self.object = get_object_or_404(self.get_model(), id=id_bot)
        self.service_work_with_bot = self.object.get_service_work_with_bot()

    @cached_property
    def entry_point(self):
        result = BotDialogAlgorithm.objects.filter(
            command=BotDialogAlgorithm.CMD_START,
            bot=self.object,
            is_deleted=False).first()
        return result

    def activate(self):
        """
        Активация бота
        """
        domain_name = (
            f'{self.object.user.school.domain_name}.{settings.SITE_DOMAIN}'
            if not self.object.user.school.custom_domain else
            self.object.user.school.domain_name)
        print('----------Активация бота domain_name', domain_name)
        try:
            self.service_work_with_bot.set_webhook(domain_name)
        except Exception as e:
            raise Exception(str(e))
        else:
            print('----------Активация бота', self.object)
            self.object.activate = True
            self.object.save()

    def create_entry_point(self):
        from bots.services import BotStart

        result = BotDialogAlgorithm.objects.create(
            command=BotDialogAlgorithm.CMD_START,
            title="Старт",
            data=BotStart().dict(),
            bot_id=self.object.id)
        first_response = result.add_child_bot_response(
            bot_id=self.object.id,
            data={'body': 'Aloha! \n Как вас зовут?'}
        )
        user_input = first_response.add_child_bot_user_input(
            bot_id=self.object.id,
        )
        bot_set_varialbe = user_input.add_child_bot_set_variable(
            bot_id=self.object.id,
            data={'var_name': 'first_name'}
        )

        bot_set_varialbe.add_child_bot_response(
            bot_id=self.object.id,
            data={'body': 'Спасибо!'}
        )

        fallback_obj = result.add_child_bot_fallback(bot_id=self.object.id)
        fallback_obj.add_child_bot_response(
            bot_id=self.object.id,
            data={'body': 'I missed what you said. Say it again?'}
        )

        return result

    def deactivate(self):
        """
        Деактивация бота
        """
        try:
            self.service_work_with_bot.unset_webhook()
        except Exception as e:
            pass
        else:
            self.object.activate = False
            self.object.save()

    def get_info(self):
        webhook_info = {'webhook_info': self.get_webhook_info()}
        return {**self.serializers(self.object).data, **webhook_info}

    def get_webhook_info(self):
        return self.service_work_with_bot.get_webhook_info()
