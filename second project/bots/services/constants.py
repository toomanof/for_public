from django.conf import settings

PATH_INCOMING_BOT = '/bots/incoming/'

TOKEN_TELEGRAM = settings.TOKEN_TELEGRAM # временно указываю свои токен
TOKEN_VIBER = settings.TOKEN_VIBER

TB_TELEGRAM, TB_VIBER, TB_VK, TB_FB = range(4)

MESSENGERS = (
    (TB_TELEGRAM, 'Telegram'),
    (TB_VIBER, 'Viber'),
    (TB_VK, 'vk'),
    (TB_FB, 'fb'),
)

LINK_TELEGRAM = f'tg://resolve?domain={settings.NAME_BOT_TELEGRAM}'
LINK_VIBER = f'viber://pa?chatURI={settings.NAME_BOT_VIBER}'
LINK_VK = f'https://vk.com/write-{settings.NAME_BOT_VK}'

LINKS_BOT_START = {
    TB_TELEGRAM: f'{LINK_TELEGRAM}&start',
    TB_VIBER: f'{LINK_VIBER}&context=',
    TB_VK: f'{LINK_VK}?ref',
}


COMMAND_START = '/start'
COMMAND_ACTIVE_ACCOUNT = '/active_account'
COMMAND_AUTH = '/authentication'

COMMAND_START_RU = 'начать'
COMMAND_ACTIVE_ACCOUNT = '/active_account'
COMMAND_AUTH = '/authentication'

TEXT_REGISTRATION = '''Привет, {}, меня зовут Лорри
            Я - твой персональный помощник в Grain Club.
            Для начала работы необходимо активировать акаунт'''
TEXT_OK_REGISTRATION = 'Теперь можно начинать работать в системе'
TEXT_ERROR = 'Чаво-то не то! Проверьте правильность действий.'
TEXT_GO_SITE = 'Перейти в систему'
TEXT_ACTIVATE = 'Активировать'


KB_VIBER_ACTIVE_ACCOUNT = {
    "Columns": 6,
    "Rows": 2,
    "BgColor": "#4BB34B",
    "ActionType": "reply",
    "ActionBody": COMMAND_ACTIVE_ACCOUNT,
    "OpenURLType": "external",
    "Text": TEXT_ACTIVATE
}

KB_VIBER_LINK_TO_SITE = {
    "Columns": 6,
    "Rows": 2,
    "BgColor": "#4BB34B",
    "ActionType": "open-url",
    "ActionBody": '{}',
    "OpenURLType": "external",
    "Text": TEXT_GO_SITE
}

KB_VK_LINK_TO_SITE = {}
