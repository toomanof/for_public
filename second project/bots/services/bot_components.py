from typing import List
from pydantic import BaseModel


class BaseBotComponent(BaseModel):
    allowed_children_class: List[str] = []  # список разрешенных классов потомком
    allowed_many_children_class: List[str] = []  # список совместимых классов потомков
    await_input = False
    name: str

    def execute(self, **kwargs):
        pass


class BotStart(BaseBotComponent):
    def __init__(self, *args, **kwargs):
        kwargs.update({
            'allowed_children_class': ['BotResponse', 'BotFallBack'],
            'allowed_many_children_class': ['BotResponse', 'BotFallBack'],
            'name': 'Start',
        })
        super().__init__(*args, **kwargs)

    def execute(self, **kwargs):
        print('BotStart execute')


class BotFallBack(BaseBotComponent):
    default_msg: str = 'I missed what you said. Say it again?'

    def __init__(self, *args, **kwargs):
        kwargs.update({
            'allowed_children_class': ['BotFallBack', 'BotResponse',
                                       'BotSetVariable', 'BotFilter',
                                       'BotGoTo'],
            'allowed_many_children_class': ['BotFilter'],
            'name': 'FallBack',
        })
        super().__init__(*args, **kwargs)


class BotUserInput(BaseBotComponent):

    def __init__(self, *args, **kwargs):
        kwargs.update({
            'allowed_children_class': ['BotFallBack', 'BotResponse',
                                       'BotSetVariable', 'BotFilter',
                                       'BotGoTo, BotStop'],
            'allowed_many_children_class': ['BotFilter', 'BotFallBack'],
            'await_input': True,
            'name': 'UserInput',
        })
        super().__init__(*args, **kwargs)


class BotResponse(BaseBotComponent):
    body: str

    def __init__(self, *args, **kwargs):
        kwargs.update({
            'allowed_children_class': ['BotFallBack', 'BotResponse',
                                       'BotSetVariable', 'BotUserInput',
                                       'BotGoTo', 'BotStop'],
            'name': 'Response',
        })
        super().__init__(*args, **kwargs)

    def execute(self, callback_response):
        print('BotResponse execute before callback_response')
        callback_response(self.body)


class BotGoTo(BaseBotComponent):
    command: str
    id_goto_step: str

    def __init__(self, *args, **kwargs):
        kwargs.update({
            'name': 'GoTo',
        })
        super().__init__(*args, **kwargs)


class BotFilter(BaseBotComponent):
    body: str
    value: str = '1'
    comparison: str = 'in'
    cases = []

    def __init__(self, *args, **kwargs):
        kwargs.update({
            'allowed_children_class': ['BotResponse', 'BotSetVariable',
                                       'BotGoTo', 'BotStop'],
            'name': 'Filter',
        })
        super().__init__(*args, **kwargs)

    def execute(self, expression):
        return expression is not None and self.value in expression


class BotSetVariable(BaseBotComponent):
    var_name: str

    def __init__(self, *args, **kwargs):
        kwargs.update({
            'allowed_children_class': ['BotFilter', 'BotFallBack',
                                       'BotUserInput', 'BotResponse',
                                       'BotGoTo'],
            'allowed_many_children_class': ['BotFilter', 'BotFallBack'],
            'name': 'SetVariable',
        })
        super().__init__(*args, **kwargs)

    def execute(self, value):
        return {self.var_name: value}


class BotStop(BaseBotComponent):
    text: str

    def __init__(self, *args, **kwargs):
        kwargs.update({
            'name': 'Stop',
        })
        super().__init__(*args, **kwargs)
