from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls.i18n import i18n_patterns
from core.views import HomeView, DashboardView

#from rest_framework_swagger.views import get_swagger_view

from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

#schema_view = get_swagger_view(title='API Grain Club')

schema_view = get_schema_view(
   openapi.Info(
      title="API Grain Club",
      default_version='v1',
      description="Test description",
      terms_of_service="https://www.google.com/policies/terms/",
      contact=openapi.Contact(email="contact@snippets.local"),
      license=openapi.License(name="BSD License"),
   ),
   public=True,
   permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/bots/', include('bots.urls_api')),
    path('api/', include('billing.urls_api')),
    path('api/o/', include('core.urls_api_oauth2')),
    path('api/', include('core.urls_api')),
    path('api/hooks/', include('hooks.urls_api_hooks')),
    path('api/', include('payments.urls_api_tariffs')),
    path('api/orders/', include('billing.urls_api_orders')),
    path('api/payments/', include('payments.urls_api')),
    path('api/catalog/video/', include('video_catalog.urls_api')),
#    path('api/v1/', schema_view),

    path('swagger.json', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    path('api/v1/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('api/redoc/', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),

    path('bots/', include('bots.urls')),
    path('oauth2/',
         include('oauth2_provider.urls', namespace='oauth2_provider')),


]

urlpatterns += i18n_patterns(

    path('', HomeView.as_view(), name="home"),
    path('', include('core.urls_auth')),
    path('chat/', include('chat.urls')),
    path('dashboard', DashboardView.as_view(), name="dashboard"),
    path('events/', include('events.urls')),
    path('api/', include('events.urls_api')),
    path('api/', include('marketing.urls_api')),
    path('api/', include('core.urls_api_i18n')),
)

urlpatterns += staticfiles_urlpatterns()

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
