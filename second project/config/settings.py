import os
from corsheaders.defaults import default_headers
from celery.schedules import crontab


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
SECRET_KEY = 'v@e@61eodl(0ja&wl0*lxxyuucwiev_g_tht7a9um1pza)d-&i'
DEBUG = False
ALLOWED_HOSTS = []

AWS_ACCESS_KEY_ID = ''
AWS_SECRET_ACCESS_KEY = ''

AWS_STORAGE_BUCKET_NAME = 'graine'
AWS_S3_ENDPOINT_URL = 'https://ams3.digitaloceanspaces.com'
AWS_S3_CUSTOM_DOMAIN = 'spaceone.grain.club'
AWS_S3_OBJECT_PARAMETERS = {
    'CacheControl': 'max-age=86400',
}
AWS_LOCATION = 'static'
AWS_DEFAULT_ACL = 'public-read'
AWS_REGION_PROD = 'eu-central-1'
AWS_STORAGE_BUCKET_NAME_PROD = 'spaceone'


# Application definition

INSTALLED_APPS = [
    'modeltranslation',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'storages',
    'channels',
    'widget_tweaks',
    'sorl.thumbnail',
    'rest_framework_swagger',
    'embed_video',
    'ckeditor',
    'rest_framework',
    'oauth2_provider',
    'knox',
    'corsheaders',
    'mptt',
    'imagekit',
    'phonenumber_field',
    'drf_yasg',
    'colorfield',
    'django_jsonfield_backport',
    'core',
    'events',
    'bots',
    'chat',
    'billing',
    'payments',
    'marketing',
    'hooks',
    'video_catalog',
]

MIDDLEWARE = [
    'django_cookies_samesite.middleware.CookiesSameSite',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'oauth2_provider.middleware.OAuth2TokenMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    #'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'core.middleware.CurrentUserMiddleware',
    'billing.middleware.AccessToEventMiddleware',
]

ROOT_URLCONF = 'config.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'core.context_processors.facade_logging',
                'django.template.context_processors.i18n'
            ],
        },
    },
]

WSGI_APPLICATION = 'config.wsgi.application'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static')

DEFAULT_FILE_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'

#STATICFILES_DIRS = (os.path.join(BASE_DIR, 'static'),)

MEDIA_URL = '/'

MEDIA_ROOT = os.path.join(BASE_DIR, 'public')


LOGIN_REDIRECT_URL = 'api_events:home'
LOGOUT_REDIRECT_URL = 'home'

LOGIN_URL = 'api_core:login'
LOGOUT_URL = 'api_core:logout'

CORS_ORIGIN_ALLOW_ALL = True

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'core.auth.CookieTokenAuthentication',
    ),
    'DATETIME_FORMAT': "%m/%d/%Y %H:%M:%S",
    'DEFAULT_SCHEMA_CLASS': 'rest_framework.schemas.coreapi.AutoSchema',

#  Расскоментировать при переводе фронта на формат параметров в camelCase
#    'DEFAULT_RENDERER_CLASSES': (
#        'django_rest_framework_camel_case.render.CamelCaseJSONRenderer',
#    ),

#    'DEFAULT_PARSER_CLASSES': (
#        'django_rest_framework_camel_case.parser.CamelCaseJSONParser',
#    ),
#    'JSON_UNDERSCOREIZE': {
#        'no_underscore_before_number': True,
#    },
}

TIME_ZONE = 'Europe/Moscow'

LANGUAGE_CODE = 'ru'
USE_I18N = True
USE_L10N = True
USE_TZ = True

LANGUAGES = [
    ('ru', 'Russian'),
    ('en', 'English'),
]

LOCALE_PATHS = (
    os.path.join(BASE_DIR, 'locale'),
)

MODELTRANSLATION_LANGUAGES = ('ru', 'en')
MODELTRANSLATION_DEFAULT_LANGUAGE = 'en'
MODELTRANSLATION_AUTO_POPULATE = True

APPEND_SLASH = True


AUTHENTICATION_BACKENDS = (
    'oauth2_provider.backends.OAuth2Backend',
    # and maybe some others ...
    'django.contrib.auth.backends.ModelBackend',
)


REST_SOCIAL_OAUTH_REDIRECT_URI = '/'
REST_SOCIAL_DOMAIN_FROM_ORIGIN = True


CSRF_COOKIE_SECURE = True

ASGI_APPLICATION = 'config.routing.application'

CHANNEL_LAYERS = {
    'default': {
        'BACKEND': 'channels_redis.core.RedisChannelLayer',
        'CONFIG': {
            "hosts": [('127.0.0.1', 6379)],
        },
    },
}


YANDEX_REDIRECT_URL = '/payments/yandex/redirect/'
YANDEX_NOTIFICATIONS_URL = 'payments/yandex/notifications/'

LIFETIME_AUTH_PHONE_CODE = 300  # в секундах
LIFETIME_CHECK_AUTH_PHONE_CODE = 60 * 20  # по умолчанию 20мин

DEFAULT_SIZE_FONT = 16

DEFAULT_COLOR_BG_BASE = '#FFFFFF'  # Цвет основного фона
DEFAULT_COLOR_TEXT_BASE = '#727171'  # Цвет основного текста
DEFAULT_COLOR_BG_MENU ='#С4С4С4'  # Цвет фона меню
DEFAULT_COLOR_TEXT_MENU = '#000000'  # Цвет текста меню
DEFAULT_COLOR_HEADERS ='#000000'  # Цвет заголовков
DEFAULT_COLOR_BG_BUTTON = '#000000'  #Цвет фона кнопки
DEFAULT_TEXT_BUTTON ='#FFFFFF'  # Цвет текста кнопки
DEFAULT_COLOR_LOGO = '#727171'  # Цвет текста логотипа

CREATOR_SITE_DOMAIN = ''
STUDENT_SITE_DOMAIN = ''

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

CORS_ALLOW_HEADERS = list(default_headers) + [
    'Cookie', 'cookie',
]

#SESSION_COOKIE_SAMESITE_KEYS = {'tokenDlyaDrevnyh'}
#SESSION_COOKIE_SAMESITE="lax"

CELERY_BROKER_URL = 'redis://127.0.0.1:6379'
CELERY_RESULT_BACKEND = 'redis://127.0.0.1:6379'
CELERY_ACCEPT_CONTENT = ['application/json']
CELERY_RESULT_SERIALIZER = 'json'
CELERY_TASK_SERIALIZER = 'json'
CELERY_TIMEZONE = 'Europe/Moscow'

CELERY_BEAT_SCHEDULE = {
    'get_status_domains_record_a': {
        'task': 'core.tasks.get_status_domains_record_a',
        'schedule': crontab(minute='*/10'),
        'args': (),
    },
    'create_config_nginx': {
        'task': 'core.tasks.create_config_nginx',
        'schedule': crontab(minute='*/15'),
        'args': (),
    },
    'generation_certbot_certificates': {
        'task': 'core.tasks.generation_certbot_certificates',
        'schedule': crontab(minute='*/30'),
        'args': (),
    },
#    'remove_declined_orders': {
#        'task': 'billing.tasks.remove_declined_orders',
#        'schedule': crontab(minute=0, hour=1),
#        'args': (),
#    },
#    'remove_processing_orders': {
#        'task': 'billing.tasks.remove_processing_orders',
#        'schedule': crontab(minute=0, hour=0),
#        'args': (),
#    },
    'set_first_status_reestr__user_view_course': {
        'task': 'hooks.tasks.set_first_status_reestr__user_view_course',
        'schedule': crontab(minute=0, hour="*/3"),
        'args': (),
    },
    'send_request_hooks': {
        'task': 'hooks.tasks.send_request_hooks',
        'schedule': crontab(minute='*/5'),
        'args': (),
    },
    'canceled_request_hooks': {
        'task': 'hooks.tasks.canceled_request_hooks',
        'schedule': crontab(minute=0, hour=0),
        'args': (),
    },

}


DCS_SESSION_COOKIE_SAMESITE = 'Lax'
DCS_SESSION_COOKIE_SAMESITE_KEYS = {'my-custom-cookies'}
DCS_SESSION_COOKIE_SAMESITE_FORCE_ALL = True


PATH_LOG_FILES = '/var/log/grain.club/'
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'console': {
            'format': '%(name)-12s %(levelname)-8s %(message)s'
        },
        'file': {
            'format': '%(asctime)s %(name)-12s %(levelname)-8s %(message)s'
        }
    },
    'handlers': {
        'file_warning': {
            'level': 'WARNING',
            'class': 'logging.FileHandler',
            'formatter': 'file',
            'filename': f'{PATH_LOG_FILES}errors.log'
        },
        'file_info': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'formatter': 'file',
            'filename': f'{PATH_LOG_FILES}info.log'
        },
        'file_timer': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'formatter': 'file',
            'filename': f'{PATH_LOG_FILES}timer.log'
        },
        'file_debug': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'formatter': 'file',
            'filename': f'{PATH_LOG_FILES}debug.log'
        },
        'celery': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'formatter': 'file',
            'filename': f'{PATH_LOG_FILES}celery.log'
        },
    },
    'loggers': {
        'debug': {
            'level': 'DEBUG',
            'handlers': ['file_debug']
        },
        'warning': {
            'level': 'WARNING',
            'handlers': ['file_warning']
        },
        'info': {
            'level': 'INFO',
            'handlers': ['file_info']
        },
        'timer': {
            'level': 'INFO',
            'handlers': ['file_timer']
        },
        'django.request': {
            'level': 'INFO',
            'handlers': ['file_info']
        },
        'celery': {
            'level': 'INFO',
            'handlers': ['celery']
        },
    }
}

EMAIL_FROM_SENDGRID = 'mail@grain.club'

HOOKS_LIST = [
    'hooks.services.hooks.CreatePaymentHook',
    'hooks.services.hooks.RegistrationUserHook',
    'hooks.services.hooks.SuccessfulPaymentHook',
    'hooks.services.hooks.StartStudiesHook',
    'hooks.services.hooks.EndStudiesHook',
]

OAUTH2_PROVIDER_APPLICATION_MODEL = 'oauth2_provider.Application'

ATOMIC_REQUESTS = True
