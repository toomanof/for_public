from config.settings import *

DEBUG = True
SECRET_KEY = '9ia^2j(6z)sgrain9__!)%0s2m_hdl4bQre+o&a(t&v%jalu01%'

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

ALLOWED_HOSTS = ['localhost',]
SITE_URL = 'http://localhost:8000/'


# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.mysql',
#         'NAME': 'awesome',
#         'USER': 'awesome',
#         'PASSWORD': 'j33s8JiGKQ8CAhQ3867Y',
#         'HOST': 'localhost',
#         'PORT': '',
#         'OPTIONS': {
#                     'charset': 'utf8mb4',
#                     'use_unicode': True, },
#     }
# }

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

DEFAULT_PROTOCOL = 'http'
DEFAULT_SITE = '127.0.0.1'

MEDIA_URL = 'http://localhost:8000/media/'
MEDIA_ROOT = '/Users/olegkohl/Documents/Work/opt/worki/toki/media/'

TOKEN_TELEGRAM = 'werere234234:AAFlJ_I4paomOQwmo1gNukdrX1VJR_hLFVU' # временно указываю свой токен
TOKEN_VIBER = '4b208c6d14e7d32a4-71a405be3c29da30-6fb6442eef494676'
TOKEN_VK = '2a3e6e0635ee30adfadasd2341a35a7f5ce3b7122e7027fb480ffbb930b4e9d84c3e8ce75f8e83b21934340d'
VK_TEL_NUMBER = '+79381234567'
VK_PASSWORD = 'Qwerty123456'


TOKEN_FB = 'EAAo234asd0cUA0gMBAMobILU5vXy0m5TwN1zsADvHfk3fADCU7ULiC4kGI0CZBTMD9fm0hkZCWZCeSvlk9ZCyIBNXk3uxK6ErizLDw17gDL4RcCyGPPlS9nUJxUNrjr4nT1ioZBWwjG9bWcETDmvh11J4V1XLZBzjwqwerezkJtJcN0UGtLZAgZDZD'
ID_FB = '2837640234522342259'

SITE_DOMAIN = 'test.toomanof.ru'
CREATOR_SITE_DOMAIN = "creator.grain.club"
STUDENT_SITE_DOMAIN = "student.grain.club"
LOGIN_SITE_DOMAIN = "login.grain.club"

NAME_BOT_TELEGRAM = 'ToomanofBot'
NAME_BOT_VIBER = 'testtoomanofbot'
NAME_BOT_VK = '192688413'

CORS_ORIGIN_ALLOW_ALL = True
CORS_ALLOW_CREDENTIALS = True

CORS_ORIGIN_WHITELIST = (oauth2_provider
       'http://127.0.0.1:8000',
)

CORS_ORIGIN_REGEX_WHITELIST = [
    'http://127.0.0.1:8000',
]


LOGIN_USER_FOR_TEST = ''
EMAIL_USER_FOR_TEST = ''
PASSWORD_USER_FOR_TEST = ''

TWILIO_ACCOUNT_SID = ''
TWILIO_TOKEN = ''
TWILIO_NUMBER = ''

MIN_VALUE_PHONE_CODE = 111111
MAX_VALUE_PHONE_CODE = 999999

API_KEY_VIMEO = ''
