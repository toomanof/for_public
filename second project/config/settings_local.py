from config.settings import *

DEBUG = True
SECRET_KEY = '9ia^2j(6z)sgrain9__!)%0s2m_hdl4bQre+o&a(t&v%jalu01%'

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

ALLOWED_HOSTS = ['*']
SITE_URL = 'http://edu.local:5660'

# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.mysql',
#         'NAME': 'awesome',
#         'USER': 'awesome',
#         'PASSWORD': 'j33s8JiGKQ8CAhQ3867Y',
#         'HOST': 'localhost',
#         'PORT': '',
#         'OPTIONS': {
#                     'charset': 'utf8mb4',
#                     'use_unicode': True, },
#     }
# }

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

DEFAULT_PROTOCOL = 'http'
DEFAULT_SITE = '127.0.0.1'
DEFAULT_DOMAIN = "tt.df"

#MEDIA_URL = 'http://localhost:8000/media/'
#MEDIA_ROOT = '/Users/olegkohl/Documents/Work/opt/worki/toki/media/'


TOKEN_TELEGRAM = ''
TOKEN_VIBER = ''
TOKEN_VK = ''
VK_TEL_NUMBER = ''
VK_PASSWORD = ''

TOKEN_FB = ''
MARKER_FB = ''
ID_FB = ''

FB_LOGIN = ''
FB_PASSWORD = ''

SITE_DOMAIN = 'test.toomanof.ru'

NAME_BOT_TELEGRAM = 'ToomanofBot'
NAME_BOT_VIBER = 'testtoomanofbot'
NAME_BOT_VK = ''

INSTALLED_APPS += ('django_extensions', )

LOGIN_USER_FOR_TEST = 'toomanof'
EMAIL_USER_FOR_TEST = 'toomanfo@gmail.com'
PASSWORD_USER_FOR_TEST = ''

TWILIO_ACCOUNT_SID = ''
TWILIO_TOKEN = ''
TWILIO_NUMBER = ''

MIN_VALUE_PHONE_CODE = 111111
MAX_VALUE_PHONE_CODE = 999999

KEY_SENDGRID = ''

DEFAULT_FROM_EMAIL = 'toomanof@gmail.com'
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_PASSWORD = ''
EMAIL_HOST_USER = 'toomanof@gmail.com'
EMAIL_USE_TLS = True
#EMAIL_USE_SSL = True
EMAIL_PORT = 587
#EMAIL_SUBJECT_PREFIX = ''
