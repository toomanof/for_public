from django.urls import path

from billing.views import AddUserToSchool, UserViewCourse

app_name = 'api_billing'

urlpatterns = [
    path('track/school/<pk>/',
         AddUserToSchool.as_view(),
         name='add_user_to_school'),
    path('track/course/<pk>/',
         UserViewCourse.as_view(),
         name='user_view_course'),
    ]