"""
API представления для работы c биллингом системы
"""

import json

from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response

from core.errors import timer_function, wrapper_error
from core.services import camel_case_to_under_case
from billing.services import BillingService
from marketing.services import MarketingService


class CreateOrder(APIView):
    permission_classes = [IsAuthenticated]

    @wrapper_error
    def post(self, request):
        """
        Создание заказа на оплату выбраного тарифа

        Parameters
        ----------
        link: uuid выбраного тарифа

        @param tariff: ID выбраного тарифа
        @return: созданный заказ
        """
        data = camel_case_to_under_case(json.loads(request.body))
        link = data['link'] if 'link' in data else 0
        return BillingService(request).create_order(link)


class ApplyPromoCodeInOrder(APIView):
    permission_classes = [IsAuthenticated]

    @wrapper_error
    def post(self, request, pk):
        """
        Активация промокода и применения его к заказу на оплату
        выбраного тарифа.

        Parameters
        ----------
        pk: ID заказа
        promocode: ID выбраного тарифа

        @param request: request
        @param pk: ID выбраного заказа
        @param promocode: Промокод
        @return: обнавленный заказ
        """
        data = camel_case_to_under_case(json.loads(request.body))
        promocode = data['promocode'] if 'promocode' in data else 'None'
        return BillingService(request).apply_promocode_in_order(
            pk, promocode)


class CreatePaymentOrder(APIView):
    permission_classes = [IsAuthenticated]

    @wrapper_error
    def post(self, request, pk):
        """
        Создание платежа заказа на оплату выбраного тарифа

        @param request: request
        @param pk: ID выбраного заказа
        @param returnurl: url страницы возврата после совершения платежа
        @return: созданный платёж
        """
        data = camel_case_to_under_case(json.loads(request.body))
        if 'first_name' in data:
            request.current_user.first_name = data['first_name']
            request.current_user.save()
        if 'last_name' in data:
            request.current_user.last_name = data['last_name']
            request.current_user.save()
        if 'phone' in data:
            request.current_user.profile.phone = data['phone']
            request.current_user.profile.save()

        returnurl = data['returnurl'] if 'returnurl' in data else None
        installments = data['installments'] if 'installments' in data else False
        return BillingService(request).create_payment_in_paysystem(
            pk, returnurl, installments)


class CancelPaymentOrder(APIView):
    permission_classes = [IsAuthenticated]

    @wrapper_error
    def post(self, request, pk):
        return BillingService(request).cancel_order(pk)


class VerifyStatusPaymentOrder(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, pk):
        """
        Проверка статуса платежа заказа на оплату выбраного тарифа
        Возвращаемые статусы:
         0 - 'в обработке'
         1 - 'отклонён'
         2 - 'выполнен'
         3 - 'ожидание оплаты'
        @param request: request
        @param pk: ID выбраного заказа
        @return: {'order_id', 'event_id', 'status'}
        """
        return BillingService(request).verify_status_payment_in_paysystem(pk)


class AddUserToSchool(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request, pk):
        MarketingService.add_user_to_school(request, pk)
        return Response('', status=204)


class UserViewCourse(APIView):
    permission_classes = [IsAuthenticated]

    @wrapper_error
    def post(self, request, pk):
        return MarketingService.student_interested_event(request.current_user, pk)
