from django.contrib.auth.models import User
from django.db import models

from billing.models import Order
from core.models import School
from events.models import Event, LessonElement, Flow
from payments.models import Tariffication, PromoCode


class ReestrBillingPayment(models.Model):
    """
    Реестр платёжных счетов вставленных студентам за выбранные тарифы
    """
    PS_NONE, PS_PAYPAL, PS_YMONEY, PS_YKASSA = range(4)
    PROCESSING, DECLINED, ACCEPTED = range(3)
    CHOICES_PAY_SYSTEM = (
        (PS_NONE, 'none'),
        (PS_PAYPAL, 'PayPal'),
        (PS_YMONEY, 'yandex money'),
        (PS_YKASSA, 'yandex kassa'),
    )
    CHOICES_PAY_STATUS = (
        (PROCESSING, 'в обработке'),
        (DECLINED, 'отклонён'),
        (ACCEPTED, 'принят'),
    )

    student = models.ForeignKey(User, on_delete=models.PROTECT,
                                verbose_name='Студент',
                                related_name='billing_payments')
    order = models.ForeignKey(Order, on_delete=models.CASCADE,
                              verbose_name='Заказ',
                              related_name='billing_payments')
    tariff = models.ForeignKey(Tariffication, on_delete=models.CASCADE,
                               verbose_name='Тариф',
                               related_name='billing_payments')
    pay_system = models.PositiveSmallIntegerField(
        'Платежная система', choices=CHOICES_PAY_SYSTEM, default=PS_NONE)
    amount = models.DecimalField('Стоимостть',
                                 max_digits=15,
                                 decimal_places=2,
                                 default=0)
    status = models.PositiveSmallIntegerField('Статус',
                                              choices=CHOICES_PAY_STATUS,
                                              null=True, blank=True)
    installments = models.BooleanField('Рассрочка', default=False)
    date_create = models.DateTimeField('Дата создание записи', auto_now_add=True)
    date_update = models.DateTimeField('Дата обновление записи', auto_now=True)


class ReestrAvailableEvents(models.Model):
    """
    Реестр доступных студентами курсов по оплаченным тарифам
    """
    student = models.ForeignKey(User,
                                on_delete=models.PROTECT,
                                verbose_name='Студент',
                                related_name='available_event_records')
    event = models.ForeignKey(Event,
                              on_delete=models.PROTECT,
                              verbose_name='Курс',
                              related_name='available_event_records')
    tariff = models.ForeignKey(Tariffication,
                               on_delete=models.PROTECT,
                               verbose_name='Тариф',
                               related_name='available_event_records')
    date_create = models.DateTimeField('Дата создание записи',
                                       auto_now_add=True)
    date_update = models.DateTimeField('Дата обновление записи',
                                       auto_now=True)
    suspended = models.BooleanField('Доступ приостановлен', default=False)

    class Meta:
        ordering = ['-date_update', ]


class ReestrStudentInFlow(models.Model):
    """
    Реестр студентов включенных в потоки курсов
    """
    student = models.ForeignKey(User,
                                on_delete=models.PROTECT,
                                verbose_name='Студент',
                                related_name='flows_in_reestr')
    event = models.ForeignKey(Event,
                             on_delete=models.PROTECT,
                             verbose_name='Курс',
                             related_name='students_in_reestr')
    flow = models.ForeignKey(Flow,
                             on_delete=models.PROTECT,
                             verbose_name='Поток',
                             related_name='students_in_reestr')
    tariff = models.ForeignKey(Tariffication,
                               on_delete=models.CASCADE,
                               verbose_name='Тариф',
                               related_name='students_in_reestr',
                               blank=True, null=True)
    promocode = models.ForeignKey(PromoCode,
                                  on_delete=models.PROTECT,
                                  verbose_name='Промокод',
                                  related_name='students_in_reestr',
                                  null=True, blank=True)
    date_create = models.DateTimeField('Дата создание записи',
                                       auto_now_add=True)
    date_update = models.DateTimeField('Дата обновление записи',
                                       auto_now=True)
    term_limits = models.BooleanField('Ограничение срока', default=True)
    date_expiration = models.DateTimeField('Дата окончания действия тарифа',
                                           blank=True, null=True)

    class Meta:
        ordering = ['-date_update', ]


class ReestrStudentInSchool(models.Model):
    (ST_USER_NOT_HAVE_COURSE, ST_USER_NOT_PAID_COURSE, ST_USER_PROCESSING_PAID_COURSE,
     ST_USER_HAVE_COURSE) = range(4)

    CHOICES_STAT_STUDENT_IN_SCHOLL = (
        (ST_USER_NOT_HAVE_COURSE, 'студент без курсов'),
        (ST_USER_NOT_PAID_COURSE, 'студент зарегистрировался по ссылке на курс (не оплачен)'),
        (ST_USER_PROCESSING_PAID_COURSE, 'студент с курсом и пытался оплатить (оплата в процессе)'),
        (ST_USER_HAVE_COURSE, 'студент с курсом и оплачен'),
    )

    student = models.ForeignKey(User,
                                on_delete=models.PROTECT,
                                verbose_name='Студент',
                                related_name='records_in_school')
    school = models.ForeignKey(School,
                               on_delete=models.PROTECT,
                               verbose_name='Школа',
                               related_name='records_in_school')
    status = models.PositiveSmallIntegerField(
        'Платежная система', choices=CHOICES_STAT_STUDENT_IN_SCHOLL,
        default=ST_USER_NOT_HAVE_COURSE)
    date_create = models.DateTimeField('Дата создание записи',
                                       auto_now_add=True)
    date_update = models.DateTimeField('Дата обновление записи',
                                       auto_now=True)


class ReestUserViewCourse(models.Model):
    ST_VIEW, ST_REDIRECT_TO_PAYMENT, ST_PAID, ST_BEGIN, ST_END = range(5)
    CHOICES_STAT_STUDENT_IN_EVENT = (
        (ST_VIEW, 'Смотрел'),
        (ST_REDIRECT_TO_PAYMENT, 'Перешел на страницу оплаты'),
        (ST_PAID, 'Оплатил'),
        (ST_BEGIN, 'Начал обучение'),
        (ST_END, 'Окончил обучение')
    )
    student = models.ForeignKey(User,
                                on_delete=models.PROTECT,
                                verbose_name='Студент',
                                related_name='records_user_views_courses')
    event = models.ForeignKey(Event,
                              on_delete=models.PROTECT,
                              verbose_name='Курс',
                              related_name='records_user_views_courses',
                              blank=True,
                              null=True)
    status = models.PositiveSmallIntegerField(
        'Статус', choices=CHOICES_STAT_STUDENT_IN_EVENT,
        default=ST_VIEW)
    date_create = models.DateTimeField('Дата создание записи',
                                       auto_now_add=True)
    date_update = models.DateTimeField('Дата обновление записи',
                                       auto_now=True)
