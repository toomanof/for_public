import decimal
import uuid
from django.db import models
from django.contrib.auth.models import User

from core.models import AbstractNotRemoveModel
from events.models import Event
from payments.models.tariffications import Tariffication
from payments.models.promocodes import PromoCode


class Order(AbstractNotRemoveModel):
    """
    Заказы студентов выбранных тарифы
    """
    PROCESSING, DECLINED, COMPLETED, WAIT_PAYMENT = range(4)
    CHOICES_STATUS = (
        (PROCESSING, 'в обработке'),
        (DECLINED, 'отклонён'),
        (COMPLETED, 'выполнен'),
        (WAIT_PAYMENT, 'ожидание оплаты'),
    )
    uuid = models.UUIDField('Ссылка', editable=False, default=uuid.uuid4)
    student = models.ForeignKey(User, on_delete=models.CASCADE,
                                verbose_name='Студент',
                                related_name='orders')
    tariff = models.ForeignKey(Tariffication,
                               on_delete=models.CASCADE,
                               verbose_name='Тариф',
                               related_name='orders')
    event = models.ForeignKey(Event,
                              on_delete=models.CASCADE,
                              verbose_name='Курс',
                              related_name='orders')
    promocode = models.ForeignKey(PromoCode,
                                  on_delete=models.PROTECT,
                                  verbose_name='Промокод',
                                  related_name='orders',
                                  null=True,
                                  blank=True)
    amount = models.DecimalField('Цена', max_digits=15, decimal_places=2,
                                 default=0)
    discount = models.FloatField('Процент скидки', default=0)
    status = models.PositiveSmallIntegerField('Статус',
                                              choices=CHOICES_STATUS,
                                              default=PROCESSING)
    installments = models.BooleanField('Рассрочка', default=False)
    date_create = models.DateTimeField('Дата создание записи',
                                       auto_now_add=True)
    date_update = models.DateTimeField('Дата обновление записи', auto_now=True)

    def __str__(self):
        return self.title

    @property
    def title(self):
        return f'Заказ №{self.id}'

    @property
    def full_title(self):
        return f'{self.title} на оплату тарифа {self.tariff.title} курса {self.event.title}'

    @property
    def discount_amount(self):
        return round(self.amount - self.amount * decimal.Decimal(self.discount / 100))
