from django.urls import path

from billing import views


app_name = 'api_orders'

urlpatterns = [
    path('', views.CreateOrder.as_view(), name='create_order'),
    path('<pk>/checkout/', views.CreatePaymentOrder.as_view(),
         name='create_payment_order'),
    path('<pk>/cancel/', views.CancelPaymentOrder.as_view(),
         name='cancel_payment_order'),
    path('<pk>/status/', views.VerifyStatusPaymentOrder.as_view(),
         name='order_status'),
    path('<pk>/promocode/', views.ApplyPromoCodeInOrder.as_view(),
         name='apply_promocode_in_order'),
]
