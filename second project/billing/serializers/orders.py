from rest_framework import serializers

from billing.models import Order
from events.serializers import CourserInfoSerializer
from payments.serializers import TarifficationSerializer


class OrderSerializer(serializers.ModelSerializer):
    discount_amount = serializers.SerializerMethodField()
    amount = serializers.SerializerMethodField()
    pay_system_have_installments = serializers.SerializerMethodField()
    tariff = serializers.SerializerMethodField()
    event = serializers.SerializerMethodField()
    promocode = serializers.SerializerMethodField()

    class Meta:
        model = Order
        fields = '__all__'

    def get_discount_amount(self, element):
        return element.discount_amount

    def get_amount(self, element):
        return float(element.amount)

    def get_event(self, element):
        request = self.context.get("request")
        return CourserInfoSerializer(element.event,
                                     context={"request": request}).data

    def get_pay_system_have_installments(self, element):
        return element.event.owner.payment_setting.installments

    def get_promocode(self, element):
        return element.promocode.promocode if element.promocode else ""

    def get_tariff(self, element):
        request = self.context.get("request")
        return TarifficationSerializer(element.tariff,
                                       context={"request": request}).data


class OrderSmallSerializer(serializers.ModelSerializer):
    discount_amount = serializers.SerializerMethodField()
    amount = serializers.SerializerMethodField()
    promocode = serializers.SerializerMethodField()
    event = serializers.SerializerMethodField()
    tariff = serializers.SerializerMethodField()

    class Meta:
        model = Order
        fields = '__all__'

    def get_discount_amount(self, element):
        return element.discount_amount

    def get_amount(self, element):
        return float(element.amount)

    def get_promocode(self, element):
        return str(element.promocode) if element.promocode else ""

    def get_event(self, element):
        return {'id': element.event.id,
                'title': element.event.title}

    def get_tariff(self, element):
        return {'id': element.tariff.id,
                'title': element.tariff.title}