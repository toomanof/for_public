import logging
from datetime import timedelta

from django.utils import timezone

from core import celery_app
from billing.models import Order, ReestrBillingPayment, ReestUserViewCourse
from payments.models import ReestrPayment


app = celery_app
logger = logging.getLogger('celery')

@app.task
def set_first_status_reestr__user_view_course():
    three_hours_before = timezone.now() - timedelta(hours=3)
    ReestUserViewCourse.objects.filter(
        status=ReestUserViewCourse.ST_REDIRECT_TO_PAYMENT,
        date_update__lt=three_hours_before
    ).update(status=ReestUserViewCourse.ST_VIEW)


@app.task
def remove_declined_orders():
    two_days_before = timezone.now() - timedelta(days=1)

    payments = ReestrBillingPayment.objects.filter(
        status=ReestrPayment.PROCESSING,
        date_update__lt=two_days_before)
    logger.info(f'ReestrBillingPayment processing removed: {payments}')
    payments.delete()

    payments = ReestrPayment.objects.filter(
        status=ReestrPayment.DECLINED,
        date_update__lt=two_days_before)
    logger.info(f'Payments declined removed: {payments}')
 #   payments.delete()

    orders = Order.objects.filter(
        status=Order.DECLINED,
        date_update__lt=two_days_before)
    logger.info(f'Orders declined removed: {orders}')
    for order in orders:
        try:
            order.student.records_user_views_courses\
                .filter(
                    event=order.event,
                    status=ReestUserViewCourse.ST_REDIRECT_TO_PAYMENT)\
                .update(status=ReestUserViewCourse.ST_VIEW)
            order.delete()
        except Exception as e:
            logger.info(type(e), str(e))


@app.task
def remove_processing_orders():
    own_day_before = timezone.now() - timedelta(days=1)

    payments = ReestrBillingPayment.objects.filter(
        status=ReestrPayment.PROCESSING,
        date_update__lt=own_day_before)
    logger.info(f'Payments processing removed: {payments}')
    payments.delete()

    payments = ReestrPayment.objects.filter(
        status=ReestrPayment.PROCESSING,
        date_update__lt=own_day_before)
    logger.info(f'Payments processing removed: {payments}')
#    payments.delete()

    orders = Order.objects.filter(
        status=Order.PROCESSING,
        date_update__lt=own_day_before,
        is_deleted=False)
    logger.info(f'Orders processing removed: {orders}')
    for order in orders:
        try:
            order.student.records_user_views_courses\
                .filter(
                    event=order.event,
                    status=ReestUserViewCourse.ST_REDIRECT_TO_PAYMENT)\
                .update(status=ReestUserViewCourse.ST_VIEW)
            order.delete()
        except Exception as e:
            logger.info(type(e), str(e))
