from django.db.models import Sum
from django.shortcuts import get_object_or_404
from operator import attrgetter

from billing.models import ReestrAvailableEvents
from billing.services import BillingService
from core.errors import timer_function
from core.services import RoleUser
from core.services import MixinServiceRoleEvent
from events.models import Event, ReestrExecution, Unit
from payments.models import TarifficationElement


class ServiceReestrAvailableLessonElements(MixinServiceRoleEvent):
    """
    Сервис по дополнительной работе с данными реестра доступных элементов
    """

    def __init__(self, request, event):
        self.request = request
        self.event = event
        if isinstance(event, (int, str)):
            self.event = get_object_or_404(Event, pk=int(event))
        self.object = self.event

    @timer_function
    def sorted_tariff_elements(self, tariff_elements):
        list_units = Unit.objects.filter(
            is_deleted=False,
            lessons__is_deleted=False,
            lessons__elements__is_deleted=False,
            lessons__elements__tarrification_elements__in=tariff_elements

        ).prefetch_related('lessons', 'lessons__elements').distinct().all()

        list_units = sorted([unit for unit in list_units], key=attrgetter('order'))
        result = []

        for unit in list_units:
            list_lessons = sorted([lesson for lesson in unit.lessons.all_active()], key=attrgetter('order'))
            for lesson in list_lessons:
                result.extend(sorted([lesson_element for lesson_element in lesson.elements.all_active()],
                                     key=attrgetter('order')))
        return result

    @property
    def count_available_elements(self):
        return self.list_available_elements_lessons().count()

    def list_available_elements_lessons(self):
        """
        Возвращает список всех доступных,текущему пользователю, элементы уроков
        @return: список всех доступных элементов уроков
        """
        return TarifficationElement.objects.filter(
            parent=BillingService.get_current_tariff(self.request.current_user,
                                                     self.event),
            lesson_element__is_deleted=False,
            lesson_element__lesson__is_deleted=False,
            lesson_element__lesson__unit__is_deleted=False).distinct()

    def list_available_lessons(self):
        """
        Возвращает список всех доступных,текущему пользователю, уроков
        @return: список всех доступных элементов уроков
        """
        available_elements_lessons = self.list_available_elements_lessons()
        result = []
        for element in available_elements_lessons:
            if element.lesson_element.lesson not in result:
                result.append(element.lesson_element.lesson)
        return sorted(result, key=attrgetter('order'))

    def list_available_units(self):
        """
        Возвращает список всех доступных,текущему пользователю, блоков
        @return:  список всех доступных,текущему пользователю, блоков
        """
        available_lesson = self.list_available_lessons()
        result = []
        for element in available_lesson:
            if element.unit not in result:
                result.append(element.unit)
        return sorted(result, key=attrgetter('order'))

    def nodes_lesson_elements(self, lesson):
        from events.services import ServiceCurrentElementStudent
        result = []
        service_current_element_student = ServiceCurrentElementStudent(
            self.request, self.event)
        current_index = service_current_element_student.current_index
        elements = self.list_available_elements_lessons()
        for element in elements:
            if element.lesson_element.lesson == lesson:
                is_access = True
                if not self.event.open_all_unit:
                    is_access = element.order <= current_index
                result.append({
                    'id': element.lesson_element.pk,
                    'name': element.lesson_element.title,
                    '_type': element.lesson_element._type,
                    'is_access': is_access,
                    'is_current': element.order <= current_index,
                    'is_exec': ReestrExecution.objects.filter(
                        student=self.request.current_user,
                        lesson_element=element.lesson_element
                    ).exists()
                })
        return result

    def nodes_lessons(self, unit):
        from events.services import ServiceCurrentElementStudent
        result = []
        service_current_element_student = ServiceCurrentElementStudent(
            self.request, self.event)
        current_index = service_current_element_student.current_index
        lessons = self.list_available_lessons()
        for lesson in lessons:
            if lesson.unit == unit:
                range_index_in_lesson = service_current_element_student.get_range_indexes_by_lesson(lesson)

                is_access = True
                if not self.event.open_all_unit:
                    is_access = current_index in range_index_in_lesson
                elements = self.nodes_lesson_elements(lesson)
                elements_id = [element['id'] for element in elements]
                count_exec_elements = ReestrExecution.objects.filter(
                    student=self.request.current_user,
                    lesson_element_id__in=elements_id
                ).count()
                record_sum_scores = ReestrExecution.objects.filter(
                    student=self.request.current_user,
                    lesson=lesson
                ).aggregate(sum=Sum('score'))
                result.append(
                    {'id': lesson.pk,
                     'name': lesson.title,
                     'is_access': is_access,
                     'count_elements': len(elements),
                     'count_exec_elements': count_exec_elements,
                     'sum_score': record_sum_scores['sum'],
                     'elements': elements}
                )
        return result

    def get_tree(self):
        self.access()
        if self.event.owner == self.request.current_user:
            return self.get_tree_event_elements()
        else:
            return self.get_tree_available_elements()

    def get_tree_event_elements(self):
        tree = {
            'event_id': self.event.pk,
            'owner': self.event.owner.id,
            'title': self.event.title,
            'is_paid': False,
            'is_access': True,
            'units': []
        }
        units = []

        for unit in self.event.units.all_active():
            lesson_list = []
            lessons = unit.lessons.all_active()

            for lesson in lessons:
                count_elements = 0
                elements = lesson.elements.all_active()
                elements_list = []
                for element in elements:
                    count_elements += 1
                    elements_list.append({
                        'id': element.pk,
                        'name': element.title,
                        '_type': element._type,
                        'is_access': True,
                        'is_current': True,
                        'is_exec': True
                    })
                count_exec_lessons = ReestrExecution.objects.filter(
                    student=self.request.current_user,
                    lesson=lesson
                ).count()
                lesson_list.append(
                    {'id': lesson.pk,
                     'name': lesson.title,
                     'is_access': True,
                     'count_elements': count_elements,
                     'count_exec_elements': count_exec_lessons,
                     'sum_score': 0,
                     'elements': elements_list,
                     'score_system_is_used': lesson.score_system_is_used}
                )

            tree['units'].append(
                {'id': unit.pk,
                 'name': unit.title,
                 'is_access': True ,
                 'count_elements': len(lesson_list),
                 'count_exec_elements': len(lesson_list),
                 'sum_score': 0,
                 'lessons': lesson_list}
            )
        return tree

    def get_tree_available_elements(self):
        from events.services import ServiceCurrentElementStudent
        is_paid = ReestrAvailableEvents.objects.filter(
            student=self.request.current_user,
            event=self.event,
            suspended=False
        ).exists()
        is_access = RoleUser(self.request).access_event(self.event)
        service_current_element_student = ServiceCurrentElementStudent(
            self.request, self.event)
        current_index = service_current_element_student.current_index
        tree = {
            'event_id': self.event.pk,
            'owner': self.event.owner.id,
            'title': self.event.title,
            'is_paid': is_paid,
            'is_access': is_access,
            'units': []
        }
        units = self.list_available_units()
        for unit in units:
            range_index_in_unit = service_current_element_student.get_range_indexes_by_unit(unit)

            is_access = True
            if not self.event.open_all_unit:
                is_access = current_index in range_index_in_unit

            lessons = self.nodes_lessons(unit)
            count_exec_lessons = ReestrExecution.objects.filter(
                student=self.request.current_user,
                lesson_id__in=[lesson['id'] for lesson in lessons]
            ).count()
            record_sum_scores = ReestrExecution.objects.filter(
                student=self.request.current_user,
                unit=unit).aggregate(sum=Sum('score'))

            tree['units'].append(
                {'id': unit.pk,
                 'name': unit.title,
                 'is_access': is_access,
                 'count_elements': sum(lesson['count_elements'] for lesson in lessons),
                 'count_exec_elements': count_exec_lessons,
                 'sum_score': record_sum_scores['sum'],
                 'lessons': lessons}
            )
        return tree
