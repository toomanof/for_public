from functools import lru_cache, reduce

from django.db.models import Sum, Count
from django.shortcuts import get_object_or_404
from operator import attrgetter
from billing.models import ReestrAvailableEvents
from billing.services import BillingService
from core.errors import timer_function
from core.services import MixinServiceRoleEvent
from events.models import Event, Unit, ReestrExecution
from payments.models import TarifficationElement


class ServiceAvailableLessonElements(MixinServiceRoleEvent):
    """
    Сервис по дополнительной работе с данными реестра доступных элементов
    """

    def __init__(self, request, event):
        self.request = request
        self.event = event

        if isinstance(event, (int, str)):
            self.event = get_object_or_404(Event, pk=int(event))
        self.object = self.event
        self._list_available_elements_lessons = set()
        self._list_available_lessons = set()
        self._list_available_units = set()

    def sorted_tariff_elements(self, tariff_elements):
        list_elements = sorted([record.lesson_element for record in tariff_elements], key=attrgetter('order'))
        list_lessons = sorted([element.lesson for element in list_elements], key=attrgetter('order'))
        list_units = sorted([lesson.unit for lesson in list_lessons], key=attrgetter('order'))
        result = []
        index_unit = 0
        for unit in list_units:
            for lesson in unit.lessons.all_active():
                if lesson in list_lessons:
                    for element in lesson.elements.all_active():
                        if element in list_elements:
                            result.append(element)
                            list_elements.remove(element)
        return result

    @property
    def count_available_elements(self):
        return len(self.list_available_elements_lessons())

    def list_available_elements_lessons(self):
        """
        Возвращает список всех доступных,текущему пользователю, элементы уроков
        @return: список всех доступных элементов уроков
        """
        return self._list_available_elements_lessons

    def list_available_lessons(self):
        """
        Возвращает список всех доступных,текущему пользователю, уроков
        @return: список всех доступных элементов уроков
        """
        return self._list_available_lessons

    def list_available_units(self):
        """
        Возвращает список всех доступных,текущему пользователю, блоков
        @return:  список всех доступных,текущему пользователю, блоков
        """
        return self._list_available_units

    def nodes_lesson_elements(self, lesson):
        from events.services import ServiceCurrentElementStudent
        result = []
        service_current_element_student = ServiceCurrentElementStudent(
            self.request, self.event)
        current_index = service_current_element_student.current_index
        elements = self.list_available_elements_lessons()
        for element in elements:
            if element.lesson == lesson:
                is_access = True
                if not self.event.open_all_unit:
                    is_access = element.order <= current_index
                result.append({
                    'id': element.pk,
                    'name': element.title,
                    '_type': element._type,
                    'is_access': is_access,
                    'is_current': element.order <= current_index,
                    'is_exec': ReestrExecution.objects.filter(
                        student=self.request.current_user,
                        lesson_element=element
                    ).exists()
                })
        return result

    def nodes_lessons(self, unit):
        from events.services import ServiceCurrentElementStudent
        result = []
        service_current_element_student = ServiceCurrentElementStudent(
            self.request, self.event)
        current_index = service_current_element_student.current_index
        lessons = self.list_available_lessons()
        for lesson in lessons:
            if lesson.unit == unit:
                range_index_in_lesson = service_current_element_student.get_range_indexes_by_lesson(lesson)

                is_access = True
                if not self.event.open_all_unit:
                    is_access = current_index in range_index_in_lesson
                elements = self.nodes_lesson_elements(lesson)
                elements_id = [element['id'] for element in elements]
                count_exec_elements = ReestrExecution.objects.filter(
                    student=self.request.current_user,
                    lesson_element_id__in=elements_id
                ).count()
                record_sum_scores = ReestrExecution.objects.filter(
                    student=self.request.current_user,
                    lesson=lesson
                ).aggregate(sum=Sum('score'))
                result.append(
                    {'id': lesson.pk,
                     'name': lesson.title,
                     'is_access': is_access,
                     'count_elements': len(elements),
                     'count_exec_elements': count_exec_elements,
                     'sum_score': record_sum_scores['sum'],
                     'elements': elements}
                )
        return result

    def get_tree(self):

        if self.event.owner == self.request.current_user:
            return self.get_tree_event_elements()
        else:
            return self.get_tree_available_elements()

    @timer_function
    def get_tree_event_elements(self):
        tree = {
            'event_id': self.event.pk,
            'owner': self.event.owner.id,
            'title': self.event.title,
            'is_paid': False,
            'is_access': True,
            'has_units': self.event.has_units,
            'units': []
        }
        list_units = Unit.objects.filter(
            event=self.event,
            is_deleted=False,
            lessons__is_deleted=False,
        ).prefetch_related('lessons', 'lessons__elements').distinct().all()

        list_lessons = []
        for unit in list_units:
            list_lessons.extend(unit.lessons.all_active())

        count_exec_lessons = ReestrExecution.objects.filter(
            student=self.request.current_user,
            lesson__in=[lesson for lesson in list_lessons]
        ).values('lesson__id').annotate(count=Count('id'))

        for unit in list_units:
            lesson_list = []
            count_exec_lesson_in_unit = 0
            count_elements_in_unit = 0

            for lesson in unit.lessons.all_active():
                count_elements = 0
                elements_list = []

                for element in lesson.elements.all_active():
                    count_elements += 1
                    elements_list.append({
                        'id': element.pk,
                        'name': element.title,
                        '_type': element._type,
                        'is_access': True,
                        'is_current': True,
                        'is_exec': True
                    })

                count_exec_lesson = list(filter(lambda i: i['lesson__id'] == lesson.pk, count_exec_lessons))
                count_exec_lesson = count_exec_lesson[0]['count'] if count_exec_lesson else 0
                count_elements_in_unit += count_elements
                count_exec_lesson_in_unit += count_exec_lesson

                lesson_list.append(
                    {'id': lesson.pk,
                     'name': lesson.title,
                     'is_access': True,
                     'count_elements': count_elements,
                     'count_exec_elements': count_exec_lesson,
                     'sum_score': 0,
                     'elements': elements_list,
                     'score_system_is_used': lesson.score_system_is_used}
                )

            tree['units'].append(
                {'id': unit.pk,
                 'name': unit.title,
                 'is_access': True,
                 'count_elements': count_elements_in_unit,
                 'count_exec_elements': count_exec_lesson_in_unit,
                 'sum_score': 0,
                 'lessons': lesson_list}
            )
        return tree

    @timer_function
    def get_tree_available_elements(self):
        from events.services import ServiceCurrentElementStudent
        record_available_events = ReestrAvailableEvents.objects.filter(
            student=self.request.current_user,
            event=self.event,
            suspended=False,
        ).first()
        is_paid = isinstance(record_available_events, ReestrAvailableEvents)  # 1 запрос

        is_access = self.access()

        service_current_element_student = ServiceCurrentElementStudent(
            self.request, self.event)  # 1 запрос

        current_element = service_current_element_student.current_element

        tree = {
            'event_id': self.event.pk,
            'owner': self.event.owner.id,
            'title': self.event.title,
            'is_paid': is_paid,
            'is_access': is_access,
            'has_units': self.event.has_units,
            'units': []
        }

        # Получаем прогресс и сохраняем оценки - 1 запрос
        exec_registry = ReestrExecution.objects.filter(event=self.event, student=self.request.current_user)
        executed = {}
        for e in exec_registry:
            executed[e.lesson_element_id] = e.score

        # Получаем элементы согласно купленноме тарифу - 1 запрос

        available = TarifficationElement.objects \
            .filter(parent=BillingService.get_current_tariff(self.request.current_user, self.event),
                    lesson_element__is_deleted=False,
                    lesson_element__lesson__is_deleted=False,
                    lesson_element__lesson__unit__is_deleted=False) \
            .select_related('lesson_element', 'lesson_element__lesson', 'lesson_element__lesson__unit')

        # Строим дерево, без обращения к БД
        units = {}

        for e in available:
            lesson = e.lesson_element.lesson
            unit = lesson.unit
            if unit.pk not in units.keys():
                units[unit.pk] = {
                    'id': unit.pk,
                    'name': unit.title,
                    'order': unit.order,
                    'is_access': False,
                    'count_elements': 0,
                    'count_exec_elements': 0,
                    'sum_score': 0,
                    'lessons': {}
                }

            if lesson.pk not in units[unit.pk]['lessons'].keys():
                units[unit.pk]['lessons'][lesson.pk] = {
                    'id': lesson.pk,
                    'name': lesson.title,
                    'order': lesson.order,
                    'is_access': False,
                    'count_elements': 0,
                    'count_exec_elements': 0,
                    'sum_score': 0,
                    'score_system_is_used': lesson.score_system_is_used,
                    'elements': {}
                }
            if e.lesson_element.pk not in units[unit.pk]['lessons'][lesson.pk]['elements'].keys():
                if current_element.lesson.unit.order == unit.order:
                    if current_element.lesson.order == lesson.order:
                        is_access = current_element.order >= e.lesson_element.order
                    elif current_element.lesson.order > lesson.order:
                        is_access = True
                    else:
                        is_access = False
                elif current_element.lesson.unit.order > unit.order:
                    is_access = True
                else:
                    is_access = False

                units[unit.pk]['lessons'][lesson.pk]['elements'][e.lesson_element.pk] = {
                    'id': e.lesson_element.pk,
                    'name': e.lesson_element.title,
                    '_type': e.lesson_element._type,
                    'order': e.lesson_element.order,
                    'is_access': is_access,
                    'is_current': current_element.order == e.lesson_element.order,
                    'is_exec': (e.lesson_element.pk in executed.keys()),
                    'score': executed.get(e.lesson_element.pk, 0)
                }

        # service_current_element_student.get_range_indexes_by_lesson генерирует сотни запросов,
        # поэтому ручками :)
        def get_lesson_elements_indexes(dict_lesson):
            return [e['order'] for e in dict_lesson['elements']]

        # service_current_element_student.get_range_indexes_by_unit генерирует сотни запросов,
        # поэтому ручками :)
        def get_unit_elements_indexes(dict_unit):
            result = []
            for l in dict_unit['lessons']:
                result += get_lesson_elements_indexes(l)
            return result

        # Сортируем и пересчитываем без дополнительных запросов
        for uk in units.keys():
            lessons_completed = 0

            for lk in units[uk]['lessons'].keys():
                elements_completed = 0

                for e in units[uk]['lessons'][lk]['elements'].values():
                    if e['is_exec']:
                        elements_completed += 1

                units[uk]['lessons'][lk]['count_exec_elements'] = elements_completed
                units[uk]['lessons'][lk]['sum_score'] = sum(
                    i['score'] for i in units[uk]['lessons'][lk]['elements'].values())
                units[uk]['lessons'][lk]['count_elements'] = len(units[uk]['lessons'][lk]['elements'])
                units[uk]['lessons'][lk]['elements'] = sorted(units[uk]['lessons'][lk]['elements'].values(),
                                                              key=lambda k: k['order'])
                units[uk]['lessons'][lk]['is_access'] = current_element.order >= min(
                    get_lesson_elements_indexes(units[uk]['lessons'][lk]))

                if elements_completed == len(units[uk]['lessons'][lk]['elements']):
                    lessons_completed += 1

            units[uk]['count_exec_elements'] = lessons_completed
            units[uk]['count_elements'] = len(units[uk]['lessons'])
            units[uk]['sum_score'] = sum(i['sum_score'] for i in units[uk]['lessons'].values())
            units[uk]['lessons'] = sorted(list(units[uk]['lessons'].values()), key=lambda k: k['order'])
            units[uk]['is_access'] = current_element.order >= min(
                get_unit_elements_indexes(units[uk])) if not self.event.open_all_unit else True

        units = sorted(list(units.values()), key=lambda k: k['order'])

        tree['units'] = units

        # total queries: 10, database time: 0.1301
        return tree

    @lru_cache(maxsize=128)
    def tree_available_elements_course(self):
        """
        Возвращает список всех доступных,текущему пользователю, элементы уроков
        @return: списки всех доступных элементов курса (список уроков, список элементов урока, список блоков)
        """
        params = {'student': self.request.current_user,
                  'event': self.event,
                  'event__is_deleted': False,
                  'lesson_element__lesson__is_deleted': False,
                  'lesson_element__lesson__unit__is_deleted': False,
                  'lesson_element__is_deleted': False}
        queryset = TarifficationElement.objects\
            .filter(
            parent=BillingService.get_current_tariff(self.request.current_user, self.event),
            lesson_element__is_deleted=False,
            lesson_element__lesson__is_deleted=False,
            lesson_element__lesson__unit__is_deleted=False).distinct()

        list_available_elements_lessons = set()
        list_available_lessons = set()
        list_available_units = set()
        for record in queryset:
            list_available_elements_lessons.add(record.lesson_element)
            list_available_lessons.add(record.lesson_element.lesson)
            list_available_units.add(record.lesson_element.lesson.unit)
        self._list_available_elements_lessons = sorted(list_available_elements_lessons, key=attrgetter('order'))
        self._list_available_lessons = sorted(list_available_lessons, key=attrgetter('order'))
        self._list_available_units = sorted(list_available_units, key=attrgetter('order'))
