from django.db import transaction
from django.shortcuts import get_object_or_404
from django.core.validators import validate_email

from core.errors import timer_function
from core.services import MixinServiceRoleBilling
from events.serializers import EventsSerializer, CoursersSerializer
from billing.models import Order
from billing.serializers import OrderSerializer
from payments.models import SettingsPayment
from payments.services import ServicePaymentPayPal, ServicePaymentYandex, ServicePaymentYooMoney


class PaymentOrderService(MixinServiceRoleBilling):
    """
    Класс реализующий оплату пользователем выбранного тарифа
    """
    tariff = None
    order = None
    index_payment_system = SettingsPayment.PS_NONE

    def __init__(self, request, order_pk):
        """
        Инициализация сервиса.
        выбраного тарифа через выбраную платёжную систему
        @param order_pk: int, id payments.models.Order id заказа

        """
        self.request = request
        self.order = get_object_or_404(Order,
                                       pk=order_pk,
                                       student=self.request.current_user,
                                       is_deleted=False)
        self.tariff = self.order.tariff
        if self.order.tariff.price > 0:
            self.index_payment_system = self.order.event.owner.payment_setting.pay_system

    def get_payment_system(self):
        """
        По полученному индексу платёжной системы возвращает сервис платёжной системы
        @return: Сервис платёжной системы
        """
        assert SettingsPayment.PS_NONE < self.index_payment_system < SettingsPayment.PS_YKASSA + 1, \
            'Not selected payment system'
        if self.index_payment_system == SettingsPayment.PS_PAYPAL:
            return ServicePaymentPayPal(self.request, self.order.event.owner.payment_setting)
        elif self.index_payment_system == SettingsPayment.PS_YMONEY:
            return ServicePaymentYooMoney(self.request, self.order.event.owner.payment_setting)
        elif self.index_payment_system == SettingsPayment.PS_YKASSA:
            return ServicePaymentYandex(self.request, self.order.event.owner.payment_setting)

    @timer_function
    @transaction.atomic
    def create_payment(self, return_url=None, installments=None):
        """
        Передает выбранный тариф на оплату через выбраную платёжную систему
        """
        # Выставление оплаты в выбранной платёжной системе
        if self.order.discount_amount > 0:
            return self.payment_in_payment_system(return_url, installments)

        return self.create_payment_free()

    def create_payment_free(self):
        """
        Создается оплата бесплатного тарифа.
        В реестр оплаты выбранного тарифа заносится запись с
        данными о заказе и тарифе платежная система оставляется PS_NONE
        сумма проствляется 0 и статус сразу в  ACCEPTED.
        В реестр доступных курсов (элементов курса) заносится запись
        о доступности выбраного курса
        @return: Response ответ
        """
        return {
            'order': OrderSerializer(self.order,
                                     context={"request": self.request}).data,
            'event': CoursersSerializer(self.order.event,
                                        context={"request": self.request}).data,
            'status': Order.COMPLETED
        }

    def create_receipt(self, installments=None):
        customer = {}
        if self.request.current_user.profile.phone:
            customer['phone'] = str(self.request.current_user.profile.phone)
        if self.request.current_user.email:
            try:
                validate_email(self.request.current_user.email)
            except Exception as e:
                pass
            else:
                customer['email'] = self.request.current_user.email

        return {
                "customer": customer,
                "items": [
                    {
                        "description": (self.order.event.owner.payment_setting.service_name
                                        if self.order.event.owner.payment_setting.service_name else str(self.order)),
                        "quantity": "1.00",
                        "amount": {
                            "value": str(self.order.discount_amount),
                            "currency": "RUB"
                        },
                        "vat_code": "4" if self.order.event.owner.payment_setting.vat else "1",
                        "payment_mode": "full_payment",
                        "payment_subject": "service"
                    },
                ]
        }

    def payment_in_payment_system(self, return_url=None, installments=None):
        """
        Оплата в выбранной платёжной системе
        """
        data = {'amount': self.order.discount_amount,
                'description': str(self.order),
                'return_url': return_url,
                'installments': installments,
                'receipt': self.create_receipt(installments)
                }
        result = {}
        payment_system_service = self.get_payment_system()

        if not isinstance(payment_system_service, ServicePaymentYooMoney):
            result = payment_system_service.create_payment(**data)
            if isinstance(result, dict):
                data['type_redirect'] = 'to_payment'
                if ('payment_method' in result and
                        'type' in result['payment_method'] and
                        'id' in result['payment_method']):
                    result['installments_id'] = result['payment_method']['id']
        else:
            data.update({
                'type_redirect': 'to_form',
                'account_id': self.order.event.owner.payment_setting.account_id,
                'wallet': self.order.event.owner.payment_setting.wallet,
                'secret': self.order.event.owner.payment_setting.secret,
                'status': Order.WAIT_PAYMENT
            })

        if isinstance(result, dict):
            result.update(data)
            result.update({
                'order': OrderSerializer(self.order,
                                         context={"request": self.request}).data,
                'event': EventsSerializer(self.order.event,
                                          context={"request": self.request}).data,
            })
        return result

    def verify_payment_status(self):
        """
        Проверка статуса оплаты
        """
        return {
            'event_id': self.order.event.pk,
            'order_id': self.order.pk,
            'status': self.order.status
        }

    def send_message_success(self):
        """
        Отправка сообщения пользователю о доступности курса и его элементов
        """
        pass

    def send_message_fail(self):
        """
        Отправка сообщение пользователю об отклоненной оплате
        """
        pass
