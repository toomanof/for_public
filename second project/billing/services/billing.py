import logging
from datetime import timedelta

from django.db import models, transaction
from django.shortcuts import get_object_or_404
from django.utils import timezone

from core.errors import timer_function
from core.services import MixinServiceRoleBilling

from billing.models import (
    Order, ReestrBillingPayment, ReestrAvailableEvents, ReestrStudentInFlow,
    ReestrStudentInSchool, ReestUserViewCourse)

from billing.serializers import OrderSerializer
from .payment_orders import PaymentOrderService
from core.errors import wrapper_error
from hooks.services import HooksProcessing, H_CREATE_PAYMENT
from marketing.services import MarketingService
from payments.models import Tariffication, PromoCode, ReestrPayment
from payments.services import PromocodeService

logger = logging.getLogger('info')


class BillingService(MixinServiceRoleBilling):
    def __init__(self, request):
        self.request = request
        self.current_user = request.current_user
        self.records_student_in_events_flow = []

    def _activated_promocode(self, order, promocode):
        logger.info(f'Activated promocode: {promocode}')
        return PromocodeService(self.request,
                                promocode.pk,
                                order.tariff.pk).activated()

    @transaction.atomic
    def apply_promocode_in_order(self, order_pk, promocode):
        order = get_object_or_404(Order,
                                  pk=order_pk,
                                  student=self.request.current_user,
                                  is_deleted=False)
        promocode_obj = get_object_or_404(PromoCode,
                                          promocode=promocode,
                                          event=order.event,
                                          is_deleted=False)
        promocode_tariffs = promocode_obj.tariffs.all()
        if promocode_tariffs.exists():
            print(f'Tariff {order.tariff.id} in {promocode_tariffs}')
            assert order.tariff in promocode_tariffs, 'This promo code is not valid in this tariff'

        logger.info(f'Apply promocode in_order: {order.title}, {promocode}, {order.tariff}')
        order.promocode = promocode_obj
        order.discount = promocode_obj.discount
        order.save()

        self._activated_promocode(order, promocode_obj)

        return OrderSerializer(Order.objects.get(pk=order_pk),
                               context={"request": self.request}).data

    def add_student_to_last_enabled_item(self, order):
        """
        Добавляет студента с предоставленого счета в последний активный поток
        @param order: object model Order
        @return: object model ReestrStudentInFlow
        """
        from events.services import ServicesFlows
        return ServicesFlows(
            self.request, order.event
        ).add_student_to_last_enabled_item(order)

    @timer_function
    def create_record_reestr_billing_payment(self, order, status=None,
                                             pay_system=None,
                                             installments=False):
        """
        Запись в реестр оплаты выбранного тарифа.
        Данные в записи:
            1. Пользователь;
            2. Тариф;
            3. Платёжная система;
            4. Сумма;
            5. Дата и время создания записи;
            6. Дата и время редактирования записи;
            7. Статус результата оплаты (При создании статус ожидания).
        """
        pay_system = order.event.owner.payment_setting.pay_system \
            if not pay_system else pay_system
        obj, _ = ReestrBillingPayment.objects.get_or_create(
            student=order.student,
            order=order,
            tariff=order.tariff,
            pay_system=pay_system,
            amount=order.discount_amount,
            status=ReestrBillingPayment.PROCESSING if not status else status,
            installments=installments
        )
        return obj

    def _create_order(self, student_id, tariff):
        return Order.objects.get_or_create(
            student_id=student_id,
            tariff=tariff,
            event=tariff.event,
            amount=tariff.price,
            status=Order.PROCESSING,
            is_deleted=False)

    def create_order(self, tariff_link):
        self.access()
        tariff = get_object_or_404(Tariffication,
                                   link=tariff_link, is_deleted=False)
        order, _ = self._create_order(self.current_user.id, tariff)

        MarketingService \
            .update_status_in_reest_student_in_school_to_not_have_course(
            self.request, tariff.event.owner.school)

        logger.info(f'{self.request.current_user} create order: {order}')

        return OrderSerializer(order,
                               context={"request": self.request}).data

    def create_record_reestr_payment(self, order, payment_obj_paysystem):
        """
        Запись в реестр отправленных платежей в полатёжных системах
        """

        ReestrPayment.objects.create(
            lector=order.event.owner,
            student=order.student,
            order=order,
            payment_id=payment_obj_paysystem['id'] if 'id' in payment_obj_paysystem else '',
            pay_system=order.event.owner.payment_setting.pay_system,
            amount=payment_obj_paysystem['amount'] if 'amount' in payment_obj_paysystem else 0,
            status=payment_obj_paysystem['status'] if 'status' in payment_obj_paysystem else 0,
            status_txt=payment_obj_paysystem['status_txt'] if 'status_txt' in payment_obj_paysystem else '',
            href=payment_obj_paysystem['link'] if 'link' in payment_obj_paysystem else '',
            installments_id=payment_obj_paysystem['installments_id'] \
                if 'installments_id' in payment_obj_paysystem else None
        )

    @timer_function
    def create_record_reestr_available_events(self, order):
        """
        Запись в реестр доступных курсов (элементов курса).
        Данные в записи:
            1. Пользователь;
            2. Тариф(тариф больше удалить нельзя!);
            3. Дата и время создания записи;
            4. Дата и время обновления записи;
        """
        ReestrAvailableEvents.objects.get_or_create(
            student=order.student,
            tariff=order.tariff,
            event=order.event,
        )

    def change_status_record_reestr_payment(self,
                                            id_payment_obj_paysystem,
                                            new_status):
        """
        Обновление статуса в реестр отправленных платежей в полатёжных системах
        """
        ReestrPayment.objects.filter(
            lector=self.order.event.owner,
            student=self.order.student,
            payment_id=id_payment_obj_paysystem,
        ).update(status=new_status)

    def create_records_open_access_event_to_student(self, student, order):
        from events.services import ServiceExecutionEvent

        self.create_record_reestr_billing_payment(
            order, status=ReestrBillingPayment.ACCEPTED)
        self.create_record_reestr_available_events(order)
        self.add_student_to_last_enabled_item(order)

        ServiceExecutionEvent(self.request,
                              order.event).start_event_for_student()
        order.status = Order.COMPLETED
        order.save()
        order.event.participants.add(student)

        MarketingService \
            .update_status_in_reest_student_in_school_to_have_course(
                student,
                order.event.owner.school
            )

    def open_access_event_to_student(self, student_id, tariff_link):
        """
        Предоставлется доступ к курсу по предоставленному тарифу
        """
        from django.contrib.auth.models import User
        student = get_object_or_404(User, id=student_id)
        tariff = get_object_or_404(Tariffication,
                                   link=tariff_link, is_deleted=False)

        order, _ = self._create_order(student_id, tariff)
        order.amount = 0
        order.discount = 100
        order.save()

        self.create_records_open_access_event_to_student(student, order)
        return {
            'order': order.id,
            'tariff': tariff_link,
            'student': student_id,
            'access': True
        }

    @transaction.atomic
    def create_payment_in_paysystem(self, order_pk,
                                    return_url=None, installments=False):
        """
        Создает платеж в подключенной платежной системе.
        Создатеся запись в реестре оплаты выбранного тарифа.
        Если тариф бесплатный:
          - создается запись в реестре доступных курсов (элементов курса).
          - студент добавлятся в активный последний поток курса
        Если тариф платный
        @param order_pk: int id счета
        @param return_url: str урл перенаправления после создания платежа
        @param installments: bool наличие рассрочки
        @return: dict структура ответа платежа
        """
        from events.services import ServiceExecutionEvent
        order = Order.objects.get(pk=order_pk)
        if order.status == Order.COMPLETED:
            raise Exception('The order has already been paid.')

        order.installments = installments
        order.save()

        MarketingService.student_redirect_to_payment(self.request,
                                                     order.event.id)
        result = PaymentOrderService(
            self.request, order_pk).create_payment(return_url, installments)

        if not isinstance(result, dict):
            return result
        assert 'status' in result, \
            "Response method create_payment not have key 'status'!"

        if order.discount_amount > 0:
            self.create_record_reestr_billing_payment(
                order,
                status=ReestrBillingPayment.PROCESSING,
                installments=installments)
            MarketingService \
                .update_status_in_reest_student_in_school_to_processing_paid(
                self.request, order.event.owner.school)
        else:
            self.create_records_open_access_event_to_student(self.current_user, order)
        self.create_record_reestr_payment(order, result)


        HooksProcessing.execute(self.request, H_CREATE_PAYMENT, course=order.event)
        return result

    @transaction.atomic
    def cancel_order(self, order_pk):
        order = Order.objects.filter(id=order_pk).first()
        if not order:
            return {'canceled': False}

        Order.objects.filter(id=order_pk).update(status=Order.DECLINED)

        ReestrStudentInFlow.objects.filter(
            student=self.current_user,
            tariff=order.tariff
        ).delete()

        ReestrAvailableEvents.objects.filter(
            student=self.current_user,
            tariff=order.tariff,
            suspended=False

        ).delete()

        logger.info(f'{self.request.current_user} canceled order: {order}')

        return {'canceled': True}

    @staticmethod
    def list_records_reest_students_in_flows(**params):
        """
        Список записей рееста студентов включенных в потоки курсов
        @param params: named arguments
        @return: queryset ReestrStudentInFlow
        """
        status = params.pop('status', None)
        students = []

        if status in (ReestUserViewCourse.ST_VIEW,
                      ReestUserViewCourse.ST_REDIRECT_TO_PAYMENT):
            return ReestUserViewCourse.objects \
                .filter(status=status, event=params['event'], is_deleted=False) \
                .distinct() \
                .only('student')
        elif status == ReestUserViewCourse.ST_PAID:
            students = Order.objects\
                .filter(event=params['event'], status=Order.COMPLETED, is_deleted=False)\
                .distinct()\
                .values_list('student', flat=True)
            print('-------- students', students)
        else:
            students = ReestUserViewCourse.objects\
                .filter(status=status, event=params['event']) \
                .distinct()\
                .values_list('student', flat=True)

        if status:
            params['student__in'] = students
        if params:
            return ReestrStudentInFlow.objects.filter(**params).only('student')
        else:
            return ReestrStudentInFlow.objects.all().only('student')

    @wrapper_error
    @transaction.atomic
    def verify_status_payment_in_paysystem(self, order_pk):
        """
        Проверка статуса оплаты
        @param order_pk: int id object model Order
        @return: dict
        """
        return PaymentOrderService(self.request,
                                   order_pk).verify_payment_status()

    def update_status_in_reestr_billing_payment(self, status):
        """
        Обновление статуса оплаты в реестре платёжных счетов по выбранному
        тарифу.
        """
        ReestrBillingPayment.objects.filter(
            student=self.order.student,
            tariff=self.order.tariff,
            pay_system=self.index_payment_system,
            amount=self.order.tariff.price
        ).update(status=status)

    def list_payments_student(self, filter_params):
        """
        Список платежей студента
        """
        from payments.serializers import ReestrPaymentForStudentSerializer
        filter = {
            'student': self.request.current_user
        }

        if filter_params and 'school' in filter_params:
            filter['lector__school__id'] = filter_params['school']

        return ReestrPaymentForStudentSerializer(
            ReestrPayment.objects.filter(**filter),
            many=True).data

    @staticmethod
    def list_record_reestr_student_in_flows(**params):
        """
        Список записей реестра вхождаемости студентов в потоки курсов.
        @param params: dict Параметры фильтрации списка.
        @return:
        """
        if params:
            return ReestrStudentInFlow.objects.filter(**params)
        else:
            return ReestrStudentInFlow.objects.all()

    @staticmethod
    def list_record_reestr_student_in_school(school, **params):
        """
        Список записей реестра вхождаемости студентов в реестр школы.
        @param params: dict Параметры фильтрации списка.
        @return:
        """
        return ReestrStudentInSchool.objects.filter(school=school)

    @staticmethod
    def exists_student_in_flows_event_by_id(student_id, events):
        """
        Флаг вхождаемости студента в потоки курсов.
        @param self:
        @param student_id: int id object model User
        @param events: list objects models Event
        @return: bool
        """
        params = {
            "event__in": events,
            "student_id": student_id,
            "flow__is_enabled": True,
            "flow__date_begin__lte": timezone.now()
        }
        params.update({"term_limits": False})
        params_not_with_term_limits = params
        params.update({
            "term_limits": True,
            'date_expiration__lte': timezone.now()
        })
        params_with_term_limits = params

        result = BillingService.list_record_reestr_student_in_flows(**params_not_with_term_limits).exists()
        return result or BillingService.list_record_reestr_student_in_flows(**params_with_term_limits).exists()

    def exists_student_in_flows_event(self, event):
        """
        Флаг вхождаемости студента в поток курса.
        @param event: object model Event
        @return: bool
        """
        params = {
            "event": event,
            "student": self.request.current_user,
            "flow__is_enabled": True,
            "flow__date_begin__lte": timezone.now(),
            "term_limits": False
        }
        params_not_with_term_limits = params.copy()
        params.update({
            "term_limits": True,
            'date_expiration__gte': timezone.now()
        })

        result = BillingService.list_record_reestr_student_in_flows(**params_not_with_term_limits).exists()
        result2 = BillingService.list_record_reestr_student_in_flows(**params).exists()
        return result or result2

    def get_or_create_reestr_student_in_flows(self, event,
                                              order, last_enabled_flow):
        """
        Создает или возвращает запись реестр вхождаемости
        студента в поток курса.
        @param event: object model Event
        @param order: object model Order
        @param last_enabled_flow: object model Flow
        @return: object model ReestrStudentInFlow
        """
        date_expiration = None
        if order.tariff.term_limits:
            date_expiration = timezone.now() + timedelta(seconds=order.tariff.duration)

        filter_params = {
            'student': order.student,
            'event': event,
            'tariff': order.tariff,
            'flow': last_enabled_flow
        }
        update_params = {
            'promocode': order.promocode,
            'term_limits': order.tariff.term_limits,
            'date_expiration': date_expiration
        }

        record = ReestrStudentInFlow.objects.filter(**filter_params).first()
        if record:
            if order.tariff.term_limits:
                update_params['date_expiration'] = record.date_expiration + timedelta(seconds=order.tariff.duration)
            ReestrStudentInFlow.objects.update(**update_params)
            return ReestrStudentInFlow.objects.filter(**filter_params)
        else:
            return ReestrStudentInFlow.objects.create(**{**filter_params, **update_params})

    @timer_function
    def get_records_student_in_events_flow(self, events):
        params = {
            "event__in": events,
            "student": self.request.current_user,
            "flow__is_enabled": True,
            "flow__date_begin__lte": timezone.now()
        }
        result = ReestrStudentInFlow.objects.filter(**params)
        self.records_student_in_events_flow = result
        return result

    @timer_function
    def get_record_student_in_event_flow(self, event, cache=True):
        if cache:
            result = list(filter(lambda record: record == event,
                                 self.records_student_in_events_flow))
            return result[0] if result else None

        params = {
            "event": event,
            "student": self.request.current_user,
            "flow__is_enabled": True,
            "flow__date_begin__lte": timezone.now()
        }
        result = ReestrStudentInFlow.objects.filter(**params)
        return result.first()

    @staticmethod
    def get_current_tariff(student, event):
        record = ReestrStudentInFlow.objects \
            .filter(student=student, event=event) \
            .first()
        return record.tariff if record else None

    @staticmethod
    def get_current_tariff_to_student_pk(student_id, event):
        record = ReestrStudentInFlow.objects \
            .filter(student_id=student_id, event=event) \
            .first()
        return record.tariff if record else None