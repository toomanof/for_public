from django.contrib import admin

from .models import *


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    model = Order
    list_display = ('title', 'student', 'tariff', 'event', 'promocode', 'amount',
                    'discount', 'discount_amount', 'status', 'installments',
                    'date_create', 'date_update', 'is_deleted')


@admin.register(ReestrAvailableEvents)
class ReestrAvailableEventsAdmin(admin.ModelAdmin):
    model = ReestrAvailableEvents
    list_display = ('student', 'tariff', 'event', 'suspended', 'date_create', 'date_update')
    list_filter = ('student', 'event', 'suspended')


@admin.register(ReestrBillingPayment)
class ReestrBillingPaymentAdmin(admin.ModelAdmin):
    model = ReestrBillingPayment
    list_display = ('student', 'order', 'tariff', 'pay_system', 'amount',
                    'status', 'installments', 'date_create', 'date_update')
    list_filter = ('student', 'order', 'pay_system', 'installments')


@admin.register(ReestrStudentInFlow)
class ReestrStudentInFlowAdmin(admin.ModelAdmin):
    model = ReestrStudentInFlow
    list_display = ('student', 'event', 'flow', 'tariff', 'promocode',
                    'date_create', 'date_update')
    list_filter = ('student', 'event')


@admin.register(ReestrStudentInSchool)
class ReestrStudentInSchoolAdmin(admin.ModelAdmin):
    model = ReestrStudentInSchool
    list_display = ('student', 'school', 'status',  'date_create', 'date_update')
    list_filter = ('student', 'school')


@admin.register(ReestUserViewCourse)
class ReestUserViewCourseAdmin(admin.ModelAdmin):
    model = ReestUserViewCourse
    list_display = ('student', 'event', 'status', 'date_create', 'date_update')
    list_filter = ('student', 'event', 'status')
