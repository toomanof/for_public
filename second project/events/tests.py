import requests
from django.conf import settings
from django.contrib.auth.models import User
from django.db.utils import IntegrityError
from django.test import Client

from datetime import time
from django.test import TestCase

# Create your tests here.
from .models import (VideoMixin, IntervalLesson, IntervalLessonElement)
from .services import (ServicesIntervalLesson, ServicesSchedulePlan)


class LessonTestCase(TestCase):

    def setUp(self):
        self.duration = time(0, 0, 0)
        self.title = 'Тестовый урок'
        self.serv_lesson = ServicesIntervalLesson(
            title=self.title, duration=self.duration)
        self.lesson = self.serv_lesson.lesson

        self.first_element = {
            'task': 'Первая задача в элементе урока',
            'video_source': VideoMixin.SOURCE_YOUTUBE,
            'video_code': 'https://www.youtube.com/watch?v=RCevytJ8Ips',
            'order': 1}

        self.second_element = {
            'task': 'Первая задача в элементе урока',
            'duration': time(0, 0, 30),
            'video_source': VideoMixin.SOURCE_YOUTUBE,
            'video_code': 'https://www.youtube.com/watch?v=RCevytJ8Ips',
            'order': 2}

    def test_create_lesson(self):
        print('Содание нового урока')
        newlesson = self.lesson
        print('Новый урок:', newlesson)
        self.assertIsInstance(newlesson, IntervalLesson)

    def test_add_element_to_lesson(self):
        print('Содание и добавление нового элемента в новом уроке')
        self.serv_lesson.add_element(**self.first_element)
        self.serv_lesson.add_element(**self.second_element)
        print('Список элементов в новом уроке:', self.lesson.elements)
        self.assertEqual(len(self.lesson.elements.all()), 2)

    def test_get_null_current_lesson_item(self):
        print('Тест на возврат none при отсутсвии элементов урока')
        element = self.serv_lesson.current_element
        print('Текущий элемент:', element)
        self.assertIsNone(element)

    def test_get_current_lesson_item(self):
        print('Тест на возврат текущего элементата урока')
        self.serv_lesson.add_element(**self.first_element)
        element = self.serv_lesson.current_element
        print('Текущий элемент:', element)
        self.assertIsInstance(element, IntervalLessonElement)

    def test_full_runing_current_element_lesson(self):
        print('Тест выполнения элемента на весь период в 5 секунд')
        self.second_element['duration'] = time(0, 0, 5)
        self.serv_lesson.add_element(**self.second_element)
        self.serv_lesson.run_timer_element()

    def test_stopping_runing_current_element_lesson(self):
        print('''Тест эмуляции события остановки выполнения
                 элемента через 10 сек''')
        self.serv_lesson.add_element(**self.second_element)
        self.serv_lesson.run_timer_element()
        self.serv_lesson.start_test_stop(10)


class APIEventsTestCase(TestCase):

    def setUp(self):
        self.prefix_location = 'ru'
        self.url = f'{settings.SITE_URL}/api'
        self.url_prefix = f'{settings.SITE_URL}/{self.prefix_location}/api'
        self.client = requests

    def login(self):
        url = f'{self.url}/login/'
        print('url login', url)
        response = self.client.post(
            url,
            data={'username': settings.LOGIN_USER_FOR_TEST,
                  'password': settings.PASSWORD_USER_FOR_TEST})
        self.paramentrs_login = response.json()

    def test_get_events(self):
        print('Тест GET списка курсов без авторизации', f'{self.url_prefix}/events/')
        response = self.client.get(f'{self.url_prefix}/events/')
        self.assertEqual(response.status_code, 200)

    def test_create_even_not_auth(self):
        print('Тест POST создание курса без авторизации')
        response = self.client.post(
            f'{self.url_prefix}/events/',
            data={
                'title': 'test event'
            })
        self.assertEqual(response.status_code, 401)

    def test_create_event_with_auth(self):
        self.login()
        print('Тест POST создание курса с авторизацией')
        token = self.paramentrs_login["token"]
        headers = {"Authorization": f'Token {token}'}
        response = self.client.post(
            f'{self.url_prefix}/events/',
            headers=headers,
            json={'title': 'test event'})
        self.assertEqual(response.status_code, 200)

    def test_get_event_not_auth(self):
        print('Тест GET курса')
        response = self.client.get(
            f'{self.url_prefix}/events/10/')
        self.assertEqual(response.status_code, 200)

    def test_get_event_items_not_auth(self):
        response = self.client.get(
            f'{self.url_prefix}/events/10/units/21/items/')
        self.assertEqual(response.status_code, 401)

    def test_get_event_items_with_auth(self):
        self.login()
        token = self.paramentrs_login["token"]
        headers = {"Authorization": f'Token {token}'}
        print('Тест POST создание курса с авторизацией', self.paramentrs_login)

        response = self.client.get(
            f'{self.url_prefix}/events/10/units/21/items/',
            headers=headers)
        self.assertEqual(response.status_code, 200)

    def test_update_event_with_auth(self):
        self.login()
        print('Тест PUT редактирование курса с авторизацией')
        token = self.paramentrs_login["token"]
        headers = {"Authorization": f'Token {token}'}
        response = self.client.put(
            f'{self.url_prefix}/events/10/',
            headers=headers,
            json={'title': 'test event',
                  'order': 1,
                  'description': 'asasdfsdfsf dfdsfdf  sdf sd',
                  'summary': 'asasdfsdfsf dfdsfdf  sdf sd',
                  'published': True})
        self.assertEqual(response.status_code, 200)

    def test_update_event_not_with_auth(self):
        print('Тест PUT редактирование курса с авторизацией')
        response = self.client.put(
            f'{self.url_prefix}/events/10/',
            json={'title': 'test event',
                  'order': 1,
                  'description': 'asasdfsdfsf dfdsfdf  sdf sd',
                  'summary': 'asasdfsdfsf dfdsfdf  sdf sd',
                  'published': True})
        self.assertEqual(response.status_code, 401)