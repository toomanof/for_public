from django import forms

from modeltranslation.forms import TranslationModelForm

from .models import Event, Unit, Lesson, Webinar


class EventForm(TranslationModelForm):

    def __init__(self, *args, **kwargs):
        if 'request' in kwargs:
            self.request = kwargs.pop('request')
            self.user = self.request.user
        super().__init__(*args, **kwargs)
        if self.user.is_authenticated:
            self.fields['owner'].initial = self.user.pk
        self.fields['owner'].widget = forms.HiddenInput()

    class Meta:
        model = Event
        fields = ('title', 'owner', 'order')


class UnitForm(TranslationModelForm):

    def __init__(self, *args, **kwargs):
        if 'event' in kwargs:
            self.event = kwargs.pop('event')
        super().__init__(*args, **kwargs)
        self.fields['event'].initial = self.event
        self.fields['event'].widget = forms.HiddenInput()

    class Meta:
        model = Unit
        fields = ('title', 'event', 'order')


class LessonForm(TranslationModelForm):

    def __init__(self, *args, **kwargs):
        if 'unit' in kwargs:
            self.unit = kwargs.pop('unit')
        super().__init__(*args, **kwargs)
        if hasattr(self, 'unit'):
            self.fields['unit'].initial = self.unit
        self.fields['unit'].widget = forms.HiddenInput()

    class Meta:
        model = Lesson
        fields = ('title', 'unit', 'order')


class WebinarForm(TranslationModelForm):
    def __init__(self, *args, **kwargs):
        if 'event' in kwargs:
            self.event = kwargs.pop('event')
        super().__init__(*args, **kwargs)
        if hasattr(self, 'event'):
            self.fields['event'].initial = self.unit
        self.fields['event'].widget = forms.HiddenInput()

    class Meta:
        model = Webinar
        fields = ('title', 'event', 'url',)
