from django.shortcuts import redirect
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.http import require_POST
from django.views.generic import TemplateView
from django.views.generic.detail import DetailView
from django.views.generic.edit import (CreateView, UpdateView, DeleteView)
from django.http import JsonResponse

from events.models import Event, Lesson, Unit, Webinar
from events.services import ServicesEvents, ServicesEvent, ServicesChat

from events.views.view_mixins import (
    MixinEventTemplateView, MixinEventView, MixinUnitView, MixinLessonView,
    MixinWebinarView)


def sure_body(method):
    """
    Декоратор в случае совершения ошибки в обёрнутой функции
    возрашает msg ошибки.
    При удачном выполненнии функции возращает её ответ
    """
    def wrapper(self, *args, **kwargs):
        try:
            new_obj = method(self, *args, **kwargs)
        except Exception as e:
            json_ret = {
                'message': str(e),
                'code': 400001,
                'status': 'ERROR'}
            return Response(
                json_ret, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(new_obj)
    return wrapper


class ListEvents(MixinEventTemplateView, TemplateView):
    template_name = 'events/event_list.html'

    def get(self, request, *args, **kwargs):
        first_event = self.get_first_active_event()
        if first_event:
            return redirect('events:event-detail',
                            pk=first_event.pk)
        return super().get(request, *args, **kwargs)


class DetailEvent(MixinEventTemplateView, DetailView):
    model = Event

    def get_context_data(self, *args, **kwargs):
        contex = super().get_context_data(*args, **kwargs)
        #contex.update(
        #    ServicesEvent(self.object.pk).get_structure_units_lessons())
        return contex


class DetailUnit(MixinEventTemplateView, DetailView):
    model = Unit
    template_name = 'events/unit_detail.html'

    def get_context_data(self, *args, **kwargs):
        contex = super().get_context_data(*args, **kwargs)
        contex['active_unit_pk'] = self.object.pk
        contex['lessons'] = ServicesEvents.all_active_lesons(self.object.pk)
        contex['webinars'] = ServicesEvents.all_active_webinars(self.object.pk)
        return contex


class DetailLesson(MixinEventTemplateView, DetailView):
    model = Lesson
    template_name = 'events/lesson_detail.html'


class DetailWebinar(MixinEventTemplateView, DetailView):
    model = Webinar
    template_name = 'events/webinar_detail.html'

    def get_context_data(self, *args, **kwargs):
        cxt = super().get_context_data(*args, **kwargs)
        services_chat = ServicesChat(self.object)
        cxt['chat_members'] = services_chat.get_members()
        cxt['chat_messages'] = services_chat.get_messages()
        return cxt


class CreateEvent(MixinEventView, CreateView):
    pass


class UpdateEvent(MixinEventView, UpdateView):
    pass


class DeleteEvent(MixinEventView, DeleteView):
    pass


class CreateUnit(MixinUnitView, CreateView):
    pass


class UpdateUnit(MixinUnitView, UpdateView):
    pass


class DeleteUnit(MixinUnitView, DeleteView):
    pass


class CreateLesson(MixinLessonView, CreateView):
    pass


class UpdateLesson(MixinLessonView, UpdateView):
    pass


class DeleteLesson(MixinLessonView, DeleteView):
    pass


class CreateWebinar(MixinWebinarView, CreateView):
    pass


class UpdateWebinar(MixinWebinarView, UpdateView):
    pass


class DeleteWebinar(MixinWebinarView, DeleteView):
    pass


@csrf_protect
@require_POST
def add_all_user_to_event(request, event):
    ServicesEvent(request, event).add_all_user_to_event()
    return JsonResponse({'status': 'OK'})
