"""
API представления для работы с вопросами элементов урока типа тест

"""
import json

from django.shortcuts import get_object_or_404

from rest_framework import permissions
from rest_framework.decorators import action
from rest_framework.viewsets import ModelViewSet

from core.services import camel_case_to_under_case
from events.serializers import AnswerTestSerializer
from events.services import  ServicesTestAnswer, ServicesTestAnswers
from events.models import AnswerTest, QuestionTest


class AnswersTestApiView(ModelViewSet):
    permission_classes = [
        permissions.IsAuthenticated,
    ]
    serializer_class = AnswerTestSerializer
    queryset = AnswerTest.objects.all_active()

    def create(self, request, question):
        """
        Создается новый вопрос элемента урока

        Parameters
        ----------
            body - текст вопроса
            randomize - Порядок следования ответов(случайный или последовательный
            order - вес сортировки)
        """
        question = get_object_or_404(
            QuestionTest,
            pk=question,
            is_deleted=False)
        return ServicesTestAnswer(
            request,
            question,
            question.answers,
            **camel_case_to_under_case(json.loads(request.body))).get_serializer_object()

    def update(self, request, question, pk):
        """
        Обновляет вопрос элемента урока

        Parameters
        ----------
            body - текст вопроса
            correct - флаг достоверности ответа
        """
        question = get_object_or_404(
            QuestionTest,
            pk=question,
            is_deleted=False)
        service =  ServicesTestAnswer(request, question, question.answers, pk)
        service.update(**camel_case_to_under_case(json.loads(request.body)))
        return service.get_serializer_object()

    def partial_update(self, request,question, pk):
        return self.update(request, question, pk)

    def list(self, request, question):
        """
        Получает список элементов урока
        """
        question = get_object_or_404(
            QuestionTest,
            pk=question,
            is_deleted=False)
        return ServicesTestAnswers(request, question, question.answers).list()

    @action(detail=True, methods=['post'])
    def set_order(self, request, question, pk):
        return self.update(request, question, pk)
