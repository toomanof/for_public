"""
API представления для работы с блоками курсами

/ru/api/events/{}/design/settings/      - GET      - список блоков курса

/ru/api/events/{}/design/settings/      - POST     - создание блока курса

/ru/api/events/{}/design/settings/{}/   - PUT      - редактирование блока курса

/ru/api/events/{}/design/settings/{}/   - PATCH    - редактирование блока курса

/ru/api/events/{}/design/settings/{}/   - DELETE   - удаление блока курса
"""
import json

from rest_framework import permissions
from rest_framework.views import APIView

from core.errors import wrapper_error
from events.services import ServicesEvent


class DesignEventApiView(APIView):
    permission_classes = [permissions.IsAuthenticated, ]

    @wrapper_error
    def get(self, request, event):
        """
        Возвращает настроки дизайна страницы курса
        """
        return ServicesEvent(
            request, int(event)).get_design_settings_serializer()
