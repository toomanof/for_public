"""
API представления для работы с курсами

-----------
"""

import json
from django.shortcuts import get_object_or_404

from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from rest_framework.decorators import action

from core.errors import wrapper_error
from core.views import MixinPermissionCustomActions
from core.services import camel_case_to_under_case
from events.serializers import (
    CategoryEventsSerializer, EventsSerializer)
from events.services import (ServicesEvent, ServicesEvents)
from events.models import Event, VideoMixin

from .view_mixins import MixinCategoryEventFilterView


class CategoryEventsApiView(MixinCategoryEventFilterView,
                            MixinPermissionCustomActions, ModelViewSet):
    serializer_class = CategoryEventsSerializer
    permission_classes = [IsAuthenticated]
    permission_classes_by_action = {'list': [AllowAny],
                                    'retrieve': [AllowAny]}


class EventsApiView(MixinPermissionCustomActions, ModelViewSet):

    serializer_class = EventsSerializer
    queryset = Event.objects.all_active().order_by('-order')

    permission_classes = [IsAuthenticated]
    permission_classes_by_action = {'list': [AllowAny],
                                    'retrieve': [AllowAny]}

    @wrapper_error
    def create(self, request):
        """
        Создание курса

        Parameters
        ----------
            title - наименование;
            order - вес;
            description - описание;
            summary - краткое описание;
            image_storage - id хранилище изображение;
            fixedDateBegin - флаг проставления даты начала потока курса.
                  Если True, то устанавливается дата указанная в dateBegin
                иначе текущая дата
            dateBegin - дата начала потока курса
        """
        return ServicesEvents.create_event(
            request, **camel_case_to_under_case(json.loads(request.body)))

    @action(detail=True, methods=['post'])
    def create_unit(self, request, pk=None):
        """
        Создается новый блок и добавляется к курсу

        Parameters
        ----------
            title - наименование;
            order - вес;
            summary - краткое описание;

        """
        return ServicesEvent(request, pk).create_unit(
            **camel_case_to_under_case(json.loads(request.body)))

    @wrapper_error
    def destroy(self, request, pk):
        """
        Удаление курса
        """
        return ServicesEvent(request, pk).destroy()

    @wrapper_error
    def list(self, request):
        """
        Получает список курсов
        """
        return ServicesEvents(request).list()


    @action(detail=False, methods=['get'])
    def my(self, request):
        """
        Получает список всех своих курсов.
        """
        return ServicesEvents.my_events(request)

    @action(detail=False, methods=['get'])
    def involved(self, request):
        """
        Получает курсы на которые подписан пользователь
        """
        events = []
        if request.current_user:
            events = ServicesEvents.active_events_participant(
                request, request.current_user)
        return Response(events)

    @wrapper_error
    def update(self, request, pk=None):
        """
        Обновляет курс

        Parameters
        ----------
            title - наименование;
            order - вес;
            description - описание;
            summary - краткое описание;
            image_storage - id хранилище изображение;

        """
        return ServicesEvent(request, pk).update(
            **camel_case_to_under_case(json.loads(request.body)))

    def partial_update(self, request, pk=None):
        return self.update(request, pk)

    @wrapper_error
    def retrieve(self, request, pk=None):
        """
        Получает курс
        """
        return ServicesEvent(request, pk, verify_access=False).get_data_event_serializer()

    @wrapper_error
    @action(detail=True, methods=['post'])
    def add_unit(self, request, pk=None):
        """
        Добавляет блок к курсу

        Parameters
        ----------
            unit_id - id добавляемого блока
        """
        data = camel_case_to_under_case(json.loads(request.body))
        if 'unit_id' in data:
            return ServicesEvent(request, pk).add_unit(data['unit_id'])
        else:
            return Response("Field 'unit_id' not found!",
                            status=status.HTTP_400_BAD_REQUEST)

    @action(detail=True, methods=['post'])
    def set_units(self, request, pk):
        """
        Cоздания и изменения блоков из списка
        Метод принимает массивом новых блоков или измененных.
        Если в элементе массива указан id и он не пуст, обновляет блок,
        если id не указан или он пуст - создает новый блок

        Parameters
        ----------
            массив - с параметрами:
                id    - id блока;
                title - наименование;
                order - вес;
                summary - краткое описание;
        """
        data = camel_case_to_under_case(json.loads(request.body))
        result, status = ServicesEvent(request, pk).create_or_update_units(data['units'])
        return Response(result, status=status)

    @action(detail=True, methods=['post'])
    def attach_student(self, request, pk=None):
        """
        Присоединение пользователя к курсу.
        Пользователь идентифицируется по переданому токену.
        Курс получаем по id.
        """
        return ServicesEvent(request, pk).attach_student()

    @action(detail=True, methods=['get'])
    def current_unit(self, request, pk=None):
        """
        Возвращает текущий
        (первый по установленному порядку не законченнный) блок
        """
        return Response(ServicesEvent(
            request, pk).get_current_unit_for_student())


    @action(detail=True, methods=['post'])
    def set_order(self, request, pk):
        """
        Задаёт новый вес курса

        Parameters
        ----------
            order - вес курса
        """
        data = camel_case_to_under_case(json.loads(request.body))
        return ServicesEvent(request, pk).set_order_event(data['order'])

    @action(detail=True, methods=['get'])
    def gradebook(self, request, pk):
        """
        Журнал оценивания уроков курса
        """
        return ServicesEvent(request, pk).get_gradebook_event()        


class TarificationLinkAPIView(APIView):

    @wrapper_error
    def get(self, request, event):
        """
        Возвращает ссылки курса и тарифов

         @param event: id курса
         @return:{"link_event": ссылка курса,
                  "link_tariffs": список ссылок тарифов курса}
        """
        return ServicesEvent(request, event, False).get_link_tariffs()


class VideoSourcesView(APIView):

    def get(self, request, format=None):
        """
        Список видео ресурсов
        """
        result = dict((x, y) for x, y in VideoMixin.CHOICES_SOURCE_VIDEO)
        return Response(result)


class SetFlagUnitsView(APIView):

    @wrapper_error
    def post(self, request, event):
        """
        Установка флага блочности курса.
        При установке флага в Ложь удаляются все блоки кроме первого,
        а все уроки переводятся в первый блок

        Parameters
        ----------
        hasUnits - флаг блочности
        """
        data = camel_case_to_under_case(json.loads(request.body))
        return ServicesEvent(request, event).set_flag_units(data['has_units'])


class SetFlagPaidView(APIView):

    @wrapper_error
    def post(self, request, event):
        """
        Установка флага платность/бесплатность курса.
        При установке флага в Ложь удаляются все тарифы,
        и создается тариф с нулевой ценой

        Parameters
        ----------
        paid - флаг платность/бесплатность
        """
        data = camel_case_to_under_case(json.loads(request.body))
        assert 'paid' in data, 'Parameter "paid" required!'
        return ServicesEvent(request, event).set_flag_paid(data['paid'])


class StudentCurrentElement(APIView):

    @wrapper_error
    def get(self, request, event):
        """
        Возвращает текущий элемент в списке доступных элементов курса.
        """
        from events.services import ServiceExecutionEvent
        return ServiceExecutionEvent(request, event).get_current_element()


class StudentNextElement(APIView):

    @wrapper_error
    def get(self, request, event):
        """
        Возвращает следующий элемент в списке доступных элементов курса.
        """
        from events.services import ServiceExecutionEvent
        return ServiceExecutionEvent(request, event).get_next_element()


class StudentEventTreeElements(APIView):

    @wrapper_error
    def get(self, request, event):
        """
        Возвращает дерево элементов курса доступных(оплаченных) пользователем
        @param request:
        @param event:
        @return:
        """
        from billing.services import ServiceReestrAvailableLessonElements
        return ServiceReestrAvailableLessonElements(request, event).get_tree()


class ListUnverifiedHomework(APIView):

    @wrapper_error
    def get(self, request, event):
        """
        Возвращает список непроверенных домашних заданий курса
        @param request:
        @param event:
        @return:
        """
        from events.services import ServiceReestrRatingLesson
        return ServiceReestrRatingLesson(request).list_unverified_homework(event)


class UnverifiedHomework(APIView):

    @wrapper_error
    def get(self, request, event, lesson_element):
        """
        Возвращает непроверенное домашнее задание
        @param request:
        @param event:
        @param lesson_element:
        @return:
        """
        from events.services import ServiceReestrRatingLesson
        return ServiceReestrRatingLesson(request).unverified_homework(
            event, lesson_element)

class CheckHomeworkTask(APIView):
    @wrapper_error
    def post(self, request, event, homework_task_id):
        """
        Проверка домашнего задания лектором
        @param request:
        @param event:
        @param lesson_element:
        @return:

        Parameters
        ----------
        comment_lector - Коментарий лектора к ответу студента
        score - Оцена домашнего задания
        """
        from events.services import ServiceReestrRatingLesson

        data = camel_case_to_under_case(json.loads(request.body))
        assert 'comment_lector' in data, 'Parameter "comment_lector" required!'
        assert 'score' in data, 'Parameter "score" required!'

        return ServiceReestrRatingLesson(request).check_homework_tasks(
            event, homework_task_id, **data)


class EventStudentsList(APIView):
    """
    Список студентов школы
    GET параметры фильтрации:
        - flow - id потока
        - tariff - id тарифа
        - promocode - id промокода
        - sorted - сортировка по
          - score - балам
          - progress - прогресу выполнения
    """

    @wrapper_error
    def get(self, request, event):
        flow = request.GET.get('flow', None)
        tariff = request.GET.get('tariff', None)
        promocode = request.GET.get('promocode', None)
        status = request.GET.get('status', None)
        return ServicesEvent(request, event).list_students(
            event=event,
            flow_id=int(flow) if flow else flow,
            tariff_id=int(tariff) if tariff else tariff,
            promocode_id=int(promocode) if promocode else promocode,
            sorted=request.GET.get('sorted', None),
            status=int(status) if status else status
        )


class EventStudentStepForward(APIView):

    @wrapper_error
    def post(self, request, event, student_id):
        from events.services import ServiceExecutionEvent
        return ServiceExecutionEvent(request, event).completed_current_and_step_forward(student_id)


class EventStudentAccessClose(APIView):

    @wrapper_error
    def post(self, request, event, student_id):
        from billing.models import ReestrAvailableEvents

        ReestrAvailableEvents.objects.\
            filter(event_id=event, student_id=student_id)\
            .update(suspended=True)
        return {'event': event,
                'student': student_id,
                'access': False}


class EventStudentAccessOpen(APIView):

    @wrapper_error
    def post(self, request, event, student_id):
        from billing.models import ReestrAvailableEvents
        from billing.services import BillingService

        data = camel_case_to_under_case(json.loads(request.body))
        assert 'link' in data, 'Parameter "link" is required!'

        record = ReestrAvailableEvents.objects.filter(
            student_id=student_id,
            tariff__link=data['link']).first()

        if record:
            record.suspended = False
            record.save()
            result = {'event': event,
                      'student': student_id,
                      'tariff': data['link'],
                      'access': True}
        else:
            result = BillingService(request).open_access_event_to_student(student_id, data['link'])
        return result
