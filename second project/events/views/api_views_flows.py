"""
API представления для работы с потоками
"""

import json
from django.shortcuts import get_object_or_404

from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from rest_framework.views import APIView

from core.errors import wrapper_error as obj_or_error
from core.services import AccountService
from core.services import camel_case_to_under_case
from core.errors import wrapper_error
from events.models import Event, Flow
from events.serializers import FlowSerializer
from payments.services import ServicePaymentYandex


class FlowAPIView(ModelViewSet):
    serializer_class = FlowSerializer
    queryset = Flow.objects.all_active()
    permission_classes = [IsAuthenticated]

    def list(self, request, event):
        event = get_object_or_404(Event, pk=event)
        return Response(FlowSerializer(
            event.flows.all_active(),
            many=True).data)

    def create(self, request, event):
        data = camel_case_to_under_case(json.loads(request.body))
        data['event'] = get_object_or_404(Event, pk=event)
        flow = Flow.objects.create(**data)
        return Response(FlowSerializer(flow).data)

    def update(self, request, event, pk):
        data = camel_case_to_under_case(json.loads(request.body))
        data['event'] = get_object_or_404(Event, pk=event)
        Flow.objects.filter(pk=pk).update(**data)
        flow = Flow.objects.get(pk=pk)
        return Response(FlowSerializer(flow).data)

    def partial_update(self, request, event, pk):
        return self.update(request, event, pk)
