"""
API представления для работы с элементами уроков

"""
import json
from django.shortcuts import get_object_or_404
from django.utils import timezone
from rest_framework import permissions
from rest_framework.decorators import action
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet

from core.errors import wrapper_error
from core.services import camel_case_to_under_case

from events.serializers import LessonElementSerializer, StudentCertificateSerializer
from events.services import ServicesLesson, ServiceExecutionEvent, ServiceExecutionTest
from events.models import LessonElement, StudentCertificate


class LessonElementsApiView(ModelViewSet):
    permission_classes = [
        permissions.IsAuthenticated,
    ]
    serializer_class = LessonElementSerializer
    queryset = LessonElement.objects.all_active()

    def create(self, request, event, unit, lesson):
        """
        Создается новый элемент урока и добавляется к уроку

        Parameters
        ----------
            title - наименование;
            order - вес;
            _type - тип урока
                0 - текст;
                1 - видео;
                2 - тест;
                3 - вебинар;
                4 - домашнее задание;
                5 - пауза;
                6 - интервальная тренировка;
                7 - чек-лист;
                8 - zoom;
                9 - сылка;
                10 - сертификат;
            task  - текстовое описание задания
            success_score - бал за впоненное задание;
            failure_score - бал за невыполненное задание;
            В зависимости от типа элемента меняются входящие параметры
        """
        return ServicesLesson(request, unit, lesson).create_element(
            **camel_case_to_under_case(json.loads(request.body)))

    def update(self, request, event, unit, lesson, pk):
        """
        Обновляет элемент урока

        Parameters
        ----------
            title - наименование;
            order - вес;
            _type - тип урока
                0 - текст;
                1 - видео;
                2 - тест;
                3 - вебинар;
                4 - домашнее задание;
                5 - пауза;
                6 - интервальная тренировка;
                7 - чек-лист;
                8 - zoom;
                9 - сылка;
                10 - сертификат;
            task  - текстовое описание задания
            В зависимости от типа элемента меняются входящие параметры
        """
        return ServicesLesson(request, unit, lesson).update_element(
            pk, **camel_case_to_under_case(json.loads(request.body)))

    def partial_update(self, request, event, unit, lesson, pk):
        return self.update(request, event, unit, lesson, pk)

    def list(self, request, event, unit, lesson):
        """
        Получает список элементов урока
        """
        return ServicesLesson(request, unit, lesson).elements()

    @action(detail=True, methods=['post'])
    def set_order(self, request, event, unit, lesson, pk):
        data = camel_case_to_under_case(json.loads(request.body))
        return ServicesLesson(request, unit, lesson).set_order_element(
            pk, data['order'])


class LessonElementsForStudents(APIView):

    @wrapper_error
    def get(self, request, element):
        lesson_element = get_object_or_404(LessonElement, pk=element)
        return ServiceExecutionEvent(
            request,
            lesson_element.lesson.unit.event).get_element_lesson(element)


class CompleteLessonElementsForStudents(APIView):

    @wrapper_error
    def post(self, request, element):
        """
        API метод завершения элемента урока
        """
        lesson_element = get_object_or_404(LessonElement, pk=element)
        return ServiceExecutionEvent(
            request,
            lesson_element.lesson.unit.event).complete_element_lesson(element)


class CreateCertificateForStudents(APIView):
    @wrapper_error
    def post(self, request, element):
        """
        API метод выпуска сертификата для студента на основании
        данных элемента урока.
        """
        lesson_element = get_object_or_404(LessonElement, pk=element)
        assert lesson_element.is_certificate, 'Lesson element is not certificate!'

        obj, _ = StudentCertificate.objects.get_or_create(
            student=request.current_user,
            title=lesson_element.certificate.title,
            signature=lesson_element.certificate.signature,
            defaults={'date_expiration': timezone.now().date()}
        )

        return StudentCertificateSerializer(obj).data


class StudentStartTest(APIView):

    @wrapper_error
    def post(self, request, element):
        """
        API метод начала теста.
        Обнуляет записи по результатам текущего теста.
        """
        return ServiceExecutionTest(request, element).start_test()


class StudentAnswerToQuestionInTest(APIView):

    @wrapper_error
    def post(self, request, element, question):
        """
        API метод получения ответов на вопрос.
        Записывает в реестр переданные ответы на вопрос.
        В начале проверяет наличие не отвеченных вопросов и если таких нет, то
        возращает ответ с результатом прохождения теста
        Проверяет наличие элемента урока и вопроса по переданным id.
        """
        data = camel_case_to_under_case(json.loads(request.body))
        return ServiceExecutionTest(request, element).answer_to_question(question, **data)
