from django.urls import reverse_lazy

from core.views import AbstractFilterView

from events.forms import EventForm, LessonForm, WebinarForm, UnitForm
from events.models import CategoryEvent, Event, Unit, Lesson, Webinar
from events.services import AccountService, ServicesEvents


class MixinEventTemplateView:
    change_filter = False

    def active_user(self):
        return self.request.user or self.user if hasattr(self, 'user')\
            else None

    def get_session_event_filter(self):
        if 'events-filter' in self.request.session:
            return self.request.session['events-filter']
        return ServicesEvents.EVENTS_ALL

    def get_events_filter(self):
        if 'filter' in self.request.GET:
            self.change_filter = self.get_session_event_filter()\
                != self.request.GET['filter']
            self.request.session['events-filter']\
                = int(self.request.GET['filter'])
        return self.get_session_event_filter()

    def get_first_active_event(self):
        return self.get_list_events().first()

    def get_list_events(self):
        events = ServicesEvents().filter_active_events(
            self.get_events_filter(), self.active_user())
        return events

    def get_active_event_pk(self):
        event = self.kwargs['event'] if 'event' in self.kwargs else None
        pk = self.kwargs['pk'] if 'pk' in self.kwargs else None
        return event or pk

    def get_active_unit_pk(self):
        return self.kwargs['unit'] if 'unit' in self.kwargs else None

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['active_event_pk'] = self.get_active_event_pk()
        if context['active_event_pk']:
            context['active_event'] = ServicesEvents.get_event_by_pk(
                context['active_event_pk'])
        context['events'] = self.get_list_events()
        context['active_filter'] = self.get_events_filter()
        context['filter_events'] = (
            ServicesEvents.EVENTS_ALL,
            ServicesEvents.EVENTS_MY,
            ServicesEvents.EVENTS_PARTICIPATE
        )
        context['has_active_events_owner']\
            = ServicesEvents.has_active_events_owner(self.request.user)
        context['has_active_events_participant']\
            = ServicesEvents.has_active_events_participant(self.request.user)
        return context


class MixinEventView(MixinEventTemplateView):
    model = Event
    form_class = EventForm
    success_url = reverse_lazy('events:home')

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def get_object(self):
        return self.get_first_active_event() if self.change_filter\
            else super().get_object()


class MixinUnitView(MixinEventTemplateView):
    model = Unit
    form_class = UnitForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['event'] = self.get_active_event_pk()
        return kwargs

    def get_success_url(self):
        return reverse_lazy('events:event-detail',
                            kwargs={'pk': self.get_active_event_pk()})


class MixinLessonView(MixinEventTemplateView):
    model = Lesson
    form_class = LessonForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['unit'] = self.get_active_unit_pk()
        return kwargs

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['active_unit_pk'] = self.get_active_unit_pk()
        return context

    def get_success_url(self):
        return reverse_lazy(
            'events:detail-unit',
            kwargs={'event': self.get_active_event_pk(),
                    'pk': self.get_active_unit_pk()})


class MixinWebinarView(MixinLessonView):
    model = Webinar
    form_class = WebinarForm


class MixinEventFilterView(AbstractFilterView):

    model = Event
    foreign_fields = ('category', )


class MixinCategoryEventFilterView(AbstractFilterView):

    model = CategoryEvent
    foreign_fields = ('parent', )
