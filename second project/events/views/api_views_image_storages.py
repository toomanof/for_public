"""
/ru/api/image_storages/     - GET     - список хранилища изображений
/ru/api/image_storages/     - POST    - создание хранилища изображения
            image - изображение;
/ru/api/image_storages/{}/  - PUT     - редактирование хранилища изображения
/ru/api/image_storages/{}/  - PATCH   - редактирование хранилища изображения
/ru/api/image_storages/{}/  - DELETE  - удаление хранилища изображения

"""
import json

from rest_framework import permissions
from rest_framework.viewsets import ModelViewSet

from events.models import ImageStorage
from events.serializers import ImageStorageSerializer
from events.services import ServicesImageStorage, ServicesImageStorages


class ImageStorageApiViews(ModelViewSet):
    permission_classes = [
        permissions.IsAuthenticated,
    ]
    serializer_class = ImageStorageSerializer
    queryset = ImageStorage.objects.all()

    def create(self, request):
        """
        Создание хранилища изображений

        Parameters
        ----------
            image - изображение;

        """
        return ServicesImageStorage(request, file=request.FILES['file']).get_serializered_data()

    def list(self, request):
        """
        Список хранилища изображений
        """
        return ServicesImageStorages(request).list()

    def update(self, request, pk):
        """
        Редактирование хранилища изображения

        Parameters
        ----------
            image - изображение;
        """
        return ServicesImageStorage(request, pk).update(request.FILES['file'])

    def partial_update(self, request, pk):
        """
        Редактирование хранилища изображения

        Parameters
        ----------
            image - изображение;
        """
        return self.update(request, pk)

    def retrieve(self, request, pk): 
        """
        Возвращает хранилище изображения
        """       
        return ServicesImageStorage(request, pk).get_serializered_data()
