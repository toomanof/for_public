"""
/ru/api/events/{}/webinars/     - GET     - список вебинаров курса
/ru/api/events/{}/webinars/     - POST    - создание вебинара курса
/ru/api/events/{}/webinars/{}/  - PUT     - редактирование вебинара курса
/ru/api/events/{}//webinars/{}/  - PATCH   - редактирование вебинара курса
/ru/api/events/{}//webinars/{}/  - DELETE  - удаление вебинара курса

/ru/api/events/{}/webinars/{}/attach_file  - POST - Присоединение объекта
                                                 хранилища файлов к вебинару
/ru/api/events/{}/webinars/{}/remove_file  - POST - Отключение объекта хранилища
                                                 файлов от вебинара

/ru/api/messages/     - GET     - список сообщений
/ru/api/messages/     - POST    - создание сообщения
/ru/api/messages/{}/  - PUT     - редактирование сообщения
/ru/api/messages/{}/  - PATCH   - редактирование сообщения
/ru/api/messages/{}/  - DELETE  - удаление сообщения

"""
import json

from rest_framework import permissions
from rest_framework.viewsets import ModelViewSet
from rest_framework.decorators import action

from core.services import camel_case_to_under_case
from events.serializers import WebinarSerializer, MessageSerializer
from events.services import ServicesWebinar, ServicesWebinars
from events.models import Webinar, Message



class WebinarApiView(ModelViewSet):
    permission_classes = [
        permissions.IsAuthenticated,
    ]
    serializer_class = WebinarSerializer
    queryset = Webinar.objects.all_active()

    def create(self, request, event):
        """
        Создается новый вебинар и добавляется к курсу

        Parameters
        ----------
        title - Название вебинара
        start_date - Дата и время начала вебинара
        video_source - Видео источник
        video_code - Url video
        file_storage - id хранилище файлов;
        """
        return ServicesWebinar(request=request, event=event,
            **camel_case_to_under_case(json.loads(request.body))).serializer_data()

    def destroy(self, request, event, pk):
        return ServicesWebinar(request, event, pk).destroy()


    def update(self, request, event, pk):
        """
        Обновляет вебинар

        Parameters
        ----------
        title - Название вебинара
        start_date - Дата и время начала вебинара
        video_source - Видео источник
        video_code - Url video
        """
        return ServicesWebinar(request, event, pk).update(
            **camel_case_to_under_case(json.loads(request.body)))

    def partial_update(self, request, event, pk):
        return self.update(request, event, pk)


    def retrieve(self, request, event, pk):
        """
        Получает вебинар
        """
        return ServicesWebinar(request, event, pk).serializer_data()

    def list(self, request, event):
        """
        Получает список вебинаров.
        """
        return ServicesWebinars(
            request, int(event)).get_webinars_in_event()

    @action(detail=True, methods=['post'])
    def attach_file(self, request, event, pk):
        """
        Присоединение объекта хранилища файлов к вебинару
        Parameters
        ----------
        file_storage - id прикрепляемого объекта хранилища файлов
        """

        params = camel_case_to_under_case(json.loads(request.body))
        if 'file_storage' not in params:
            raise Exception('file_storage not in params!')
        return ServicesWebinar(request, event, pk).attach_file(params['file_storage'])

    @action(detail=True, methods=['post'])
    def remove_file(self, request, event, pk):
        """
        Отключение объекта хранилища файлов от вебинара
        Parameters
        ----------
        file_storage - id отключаемого объекта хранилища файлов
        """
        params = camel_case_to_under_case(json.loads(request.body))
        if 'file_storage' not in params:
            raise Exception('file_storage not in params!')
        return ServicesWebinar(request, event, pk).remove_file(params['file_storage'])

class MessageApiView(ModelViewSet):
    permission_classes = [
        permissions.IsAuthenticated,
    ]
    serializer_class = MessageSerializer
    queryset = Message.objects.all()