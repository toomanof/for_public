"""
API представления для работы элементов урока типа чек лист
"""
from events.serializers import QuestionTestSerializer
from events.services import ServicesTestQuestion, ServicesTestQuestions
from events.models import QuestionTest
from .abstract_views import SubordinateToLessonApiView


class QuestionTestApiView(SubordinateToLessonApiView):

    serializer_class = QuestionTestSerializer
    queryset = QuestionTest.objects.all_active()
    service_object = ServicesTestQuestion
    service_items = ServicesTestQuestions

    def get_queryset_items(self):
        return self.lesson.test.questions
