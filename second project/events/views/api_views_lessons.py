"""
API представления для работы с
"""
import json

from rest_framework import permissions
from rest_framework.decorators import action
from rest_framework.viewsets import ModelViewSet

from core.errors import wrapper_error
from core.services import camel_case_to_under_case
from events.serializers import LessonSerializer
from events.services import ServicesEvent, ServicesLesson
from events.models import Lesson


class LessonApiView(ModelViewSet):
    permission_classes = [
        permissions.IsAuthenticated,
    ]
    serializer_class = LessonSerializer
    queryset = Lesson.objects.all_active()

    @wrapper_error
    def create(self, request, event, unit):
        """
        Создается новый урок и добавляется к блоку

        Parameters
        ----------
            title - наименование;
            order - вес;
            lesson_type - тип урока
            (LT_RELAX, LT_TEXT, LT_VIDEO, LT_TEST, LT_HOME_WORK)
        """
        return ServicesEvent(request, event).create_lesson(
            unit, **camel_case_to_under_case(json.loads(request.body)))

    @wrapper_error
    def update(self, request, event=None, unit=None, pk=None):
        """
        Обновляет урок

        Parameters
        ----------
            title - наименование;
            order - вес;
            lesson_type - тип урока
            (LT_RELAX, LT_TEXT, LT_VIDEO, LT_TEST, LT_HOME_WORK)
        """
        return ServicesEvent(request, event).update_lesson(
            unit, pk, **camel_case_to_under_case(json.loads(request.body)))

    def partial_update(self, request, event=None, unit=None, pk=None):
        return self.update(request, event, unit, pk)

    @action(detail=True, methods=['post'])
    def close(self, request, event=None, unit=None, pk=None):
        """
        Закрывается урок текущего студента.
        Студент выявляется по переданному токену.
        """
        if request.body:
            return ServicesLesson(request, unit, pk).\
                close_lesson_for_student(
                    **camel_case_to_under_case(json.loads(request.body)))
        else:
            return ServicesLesson(request,  unit, pk).\
                close_lesson_for_student()

    @wrapper_error
    def list(self, request, event, unit):
        """
        Получает список уроков
        """
        return ServicesEvent(
            request, int(event)).get_lessons_in_unit(unit)

    @action(detail=True, methods=['post'])
    def set_order(self, request, event, unit, pk):
        data = camel_case_to_under_case(json.loads(request.body))
        return ServicesEvent(request, event).set_order_lesson(
            unit, pk, data['order'])
