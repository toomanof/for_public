"""
API представления для работы элементов урока типа чек лист
"""
from events.serializers import CheckListElementSerializer
from events.services import (
    ServicesCheckListElement, ServicesCheckListElements)
from events.models import CheckListElement
from .abstract_views import SubordinateToLessonApiView


class CheckListElementApiView(SubordinateToLessonApiView):

    serializer_class = CheckListElementSerializer
    queryset = CheckListElement.objects.all_active()
    service_object = ServicesCheckListElement
    service_items = ServicesCheckListElements

    def get_queryset_items(self):
        return self.lesson.check_list.elements
