"""
API представления для работы элементов урока типа интервальные задания
"""
from events.serializers import IntervalTraningElementSerializer
from events.services import  (
    ServicesIntervalTraningElement, ServicesIntervalTraningElements)
from events.models import IntervalTraningElement
from .abstract_views import SubordinateToLessonApiView


class IntervalTrainingElementApiView(SubordinateToLessonApiView):

    serializer_class = IntervalTraningElementSerializer
    queryset = IntervalTraningElement.objects.all_active()
    service_object = ServicesIntervalTraningElement
    service_items = ServicesIntervalTraningElements

    def get_queryset_items(self):
        return self.lesson.interval_traning.elements
