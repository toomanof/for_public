"""
API представления для работы с блоками курсами

/ru/api/events/{}/units/      - GET      - список блоков курса

/ru/api/events/{}/units/      - POST     - создание блока курса

/ru/api/events/{}/units/{}/   - PUT      - редактирование блока курса

/ru/api/events/{}/units/{}/   - PATCH    - редактирование блока курса

/ru/api/events/{}/units/{}/   - DELETE   - удаление блока курса

/ru/api/events/{}/units/{}/create_lesson/  - POST  - Создается новый урок и
                                                 добавляется к блоку

/ru/api/events/{}/units/{}/current_lesson/  - GET
    - Возвращает текущий (первый по установленному порядку не законченнный)
      урок текущего студента

/ru/api/events/{}/units/{}/close_current_lesson/  - POST
    - Закрывается текущий урок

/ru/api/events/{}/units/{}/items/   - GET  - Возвращаются все элементы для
                                         заданного блока.
/ru/api/events/{}/units/{}/set_order   - POST     - задаёт новый вес блока

"""
import json

from rest_framework import permissions
from rest_framework.decorators import action
from rest_framework.viewsets import ModelViewSet

from core.errors import wrapper_error
from core.services import camel_case_to_under_case
from events.serializers import UnitsSerializer
from events.services import ServicesEvent
from events.models import Unit


class UnitsApiView(ModelViewSet):
    permission_classes = [permissions.IsAuthenticated, ]
    serializer_class = UnitsSerializer
    queryset = Unit.objects.all_active()

    @wrapper_error
    def create(self, request, event):
        """
        Создается новый блок и добавляется к курсу

        Parameters
        ----------
            title - наименование;
            order - вес;
            summary - краткое описание;

        """
        return ServicesEvent(request, event).create_unit(
            **camel_case_to_under_case(json.loads(request.body)))

    @wrapper_error
    def destroy(self, request, event, pk):
        """
        Удаление блока курса
        """
        return ServicesEvent(request, event).destroy_unit(pk)

    @action(detail=True, methods=['post'])
    def create_lesson(self, request, event=None, pk=None):
        """
        Создается новый урок и добавляется к блоку

        Parameters
        ----------
            title - наименование;
            order - вес;
            summary - краткое описание;
            lesson_type - тип урока
            (LT_RELAX, LT_TEXT, LT_VIDEO, LT_TEST, LT_HOME_WORK)
        """
        return ServicesEvent(request, event).create_lesson(
            pk, **camel_case_to_under_case(json.loads(request.body)))

    @action(detail=True, methods=['get'])
    def current_lesson(self, request, event=None, pk=None):
        """
        Возвращает текущий
        (первый по установленному порядку не законченнный)урок
        текущего студента. Студент выявляется по переданному токену.
        """
        return ServicesEvent(
            request, event).get_current_lesson_for_student(pk)

    @action(detail=True, methods=['post'])
    def close_current_lesson(self, request, event=None, pk=None):
        """
        Закрывается текущий
        (первый по установленному порядку не законченнный) урок
        текущего студента. Студент выявляется по переданному токену.
        """
        return ServicesEvent(
            request, event).close_current_lesson_for_student(pk)

    @wrapper_error
    def list(self, request, event):
        """
        Получает список блоков урока
        """
        return ServicesEvent(
            request, int(event)).get_units_serializer()

    @wrapper_error
    def retrieve(self, request, event=None, pk=None):
        """
        Получает блок
        """
        return ServicesEvent(
            request, int(event)).get_unit_serializer(pk)

    @action(detail=True, methods=['get'])
    def items(self, request, event=None, pk=None):
        """
        Метод  возвращает все элементы для заданного блока.

        Parameters
        ----------
            event - id курса
            pk - id блока
        """
        return ServicesEvent(request, event).get_unit_items(pk)

    @action(detail=True, methods=['post'])
    def set_order(self, request, event, pk):
        data = camel_case_to_under_case(json.loads(request.body))
        return ServicesEvent(request, event).set_order_unit(pk, data['order'])
