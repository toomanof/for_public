"""
API представления для работы элементов урока типа домашнее задание
"""
import json
from django.shortcuts import get_object_or_404
from rest_framework.views import APIView


from core.errors import wrapper_error
from core.services import camel_case_to_under_case
from events.serializers import HomeWorkTaskSerializer
from events.services import ServicesHomeWorkTask, ServicesHomeWorkTasks, ServiceExecutionHomeWork
from events.models import HomeWorkTask, LessonElement
from .abstract_views import SubordinateToLessonApiView



class HomeWorkTaskApiView(SubordinateToLessonApiView):

    serializer_class = HomeWorkTaskSerializer
    queryset = HomeWorkTask.objects.all_active()
    service_object = ServicesHomeWorkTask
    service_items = ServicesHomeWorkTasks

    def get_queryset_items(self):
        return self.lesson.home_work.tasks


class StudentDeliverHomeWork(APIView):

    @wrapper_error
    def post(self, request, element, homework_task):
        """
        Метод API руки получения ответов по доашнему заданию.
        Записывает в реестр переданный ответ по доашнему заданию.
        """
        data = camel_case_to_under_case(json.loads(request.body))
        return ServiceExecutionHomeWork(
            request, element).deliver_homework(homework_task, **data)
