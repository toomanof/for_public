"""
Multipart - форма передачи данных

/ru/api/file_storages/     - GET     - список хранилища файлов
/ru/api/file_storages/     - POST    - создание хранилища файла
            file - изображение;
            url_third_party_res - url на сторонний ресурс;
/ru/api/file_storages/{}/  - PUT     - редактирование хранилища файла
/ru/api/file_storages/{}/  - PATCH   - редактирование хранилища файла
/ru/api/file_storages/{}/  - DELETE  - удаление хранилища файла

"""
import json

from rest_framework import permissions
from rest_framework.viewsets import ModelViewSet

from core.services import camel_case_to_under_case
from events.models import FileStorage
from events.serializers import FileStorageSerializer
from events.services import ServicesFileStorage, ServicesFileStorages


class FileStorageApiViews(ModelViewSet):
    permission_classes = [
        permissions.IsAuthenticated,
    ]
    serializer_class = FileStorageSerializer
    queryset = FileStorage.objects.all()


    def create(self, request):
        """
        Создание хранилища файла

        Parameters
        ----------
            file - изображение;
            url_third_party_res - url на сторонний ресурс;

        """

        kwargs = camel_case_to_under_case(request.POST)
        if 'file' in request.FILES:
            kwargs['file'] = request.FILES['file']
        return ServicesFileStorage(request, **kwargs).get_serializered_data()

    def list(self, request):
        """
        Список хранилища файлов
        """
        return ServicesFileStorages(request).list()

    def update(self, request, pk=None):
        """
        Редактирование хранилища файла

        Parameters
        ----------
            file - изображение;
            url_third_party_res - url на сторонний ресурс;

        """
        return ServicesFileStorage(request, pk).update(request.FILES['file'])

    def partial_update(self, request, pk=None):
        return self.update(request, pk)

    def retrieve(self, request, pk=None):        
        return ServicesFileStorage(request, pk).get_serializered_data()