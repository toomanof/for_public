"""
Abstract API views
"""
import json

from django.shortcuts import get_object_or_404

from rest_framework import permissions
from rest_framework.decorators import action
from rest_framework.viewsets import ModelViewSet

from core.errors import wrapper_error
from core.services import obj_or_error
from core.services import camel_case_to_under_case
from events.models import LessonElement


class SubordinateToLessonApiView(ModelViewSet):
    permission_classes = [
        permissions.IsAuthenticated,
    ]
    # object service
    service_object = None
    # object items service
    service_items = None
    # queryset items
    queryset_items = None
    lesson = None

    def get_queryset_items(self):
        return self.queryset_items

    def get_lesson(self, lesson_pk):
        self.lesson = get_object_or_404(
            LessonElement,
            pk=lesson_pk,
            is_deleted=False)
        return self.lesson

    @wrapper_error
    def create(self, request, lesson_element_pk):
        """
        Create a new lesson item element.
        Initialization to the parameters in children views.
        """

        return self.service_object(
            request,
            self.get_lesson(lesson_element_pk),
            self.get_queryset_items(),
            **camel_case_to_under_case(json.loads(request.body))).get_serializer_object()

    @wrapper_error
    def update(self, request, lesson_element_pk, pk):
        """
        Update a lesson item element.
        Initialization to the parameters in children views.
        """
        service = self.service_object(
            request,
            self.get_lesson(lesson_element_pk),
            self.get_queryset_items(),
            pk
        )
        service.update(**camel_case_to_under_case(json.loads(request.body)))
        return service.get_serializer_object()


    def partial_update(self, request, lesson_element_pk, pk):
        return self.update(request, lesson_element_pk, pk)

    @obj_or_error
    def list(self, request, lesson_element_pk):
        """
        Получает список элементов урока
        """
        try:
            return self.service_items(
                request,
                self.get_lesson(lesson_element_pk),
                self.get_queryset_items()
            ).list()
        except Exception:
            raise Exception('Lesson item type does not match request')

    @action(detail=True, methods=['post'])
    def set_order(self, request, lesson_element_pk, pk):
        return self.update(request, lesson_element_pk, pk)
