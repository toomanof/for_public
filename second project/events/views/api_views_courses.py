import json

from rest_framework.views import APIView

from core.errors import wrapper_error
from core.services import camel_case_to_under_case
from events.services import (
    ServicesEvent, ServicesEvents, ServicesCourse, ServicesCourses, ServicesCourseBlock,
    ServicesCourseLesson)
from marketing.services import MarketingService


class StudentEventTreeElements(APIView):

    @wrapper_error
    def get(self, request, event):
        """
        Возвращает дерево элементов курса доступных(оплаченных) пользователем
        @param request:
        @param event:
        @return:
        """
        from billing.services import ServiceAvailableLessonElements
        MarketingService.student_begin_course(request, event)
        return ServiceAvailableLessonElements(request, event).get_tree()


class CourseView(APIView):

    @wrapper_error
    def get(self, request, id):
        MarketingService.student_interested_event(request.current_user, id)
        return ServicesCourse(request, id, verify_access=False).get_course()

    @wrapper_error
    def patch(self, request, id):
        data = camel_case_to_under_case(json.loads(request.body))
        return ServicesCourse(request, id).update_course(**data)

    @wrapper_error
    def put(self, request, id):
        return self.patch(request, id)


class StudentCourserInfo(APIView):

    @wrapper_error
    def get(self, request, id):
        return ServicesCourse(
            request, id, verify_access=False).course_info_for_student()


class BlocksView(APIView):

    @wrapper_error
    def get(self, request, id):
        return ServicesCourse(request, id).get_blocks()


class BlockView(APIView):

    @wrapper_error
    def get(self, request, id):
        return ServicesCourseBlock(request, id).get_block()

    @wrapper_error
    def patch(self, request, id):
        data = camel_case_to_under_case(json.loads(request.body))
        return ServicesCourseBlock(request, id).update_block(**data)

    @wrapper_error
    def put(self, request, id):
        return self.patch(request, id)


class BlockLessonsView(APIView):

    @wrapper_error
    def get(self, request, id):
        return ServicesCourseBlock(request, id).get_lessons()


class LessonView(APIView):

    @wrapper_error
    def get(self, request, id):
        return ServicesCourseLesson(request, id).get_lesson()


class CourseContentTree(APIView):

    @wrapper_error
    def get(self, request, id):
        return ServicesCourse(request, id, verify_access=False).get_content_tree()


class CourseEditorsView(APIView):
    @wrapper_error
    def get(self, request, id):
        """
        Список редакторов курса
        @param request:
        @param id: ID курса
        @return: [
                      {
                        "id": int,
                        "first_name": string,
                        "last_name": string,
                        "email": string,
                        "phone": string,
                        "joined": date,
                        "permissions": {
                          "settings": bool,
                          "tariffs": bool,
                          "content": bool,
                          "students": bool,
                          "homeworks": bool
                        }
                      },
                ]
        """
        service = ServicesEvent(request, id, False)
        return service.list_editors()


class ActivationInviteCourseEditorView(APIView):

    @wrapper_error
    def get(self, request, uuid):
       return ServicesEvents.activation_invitation_course_editor(request, uuid)


class DeclineInviteCourseEditorView(APIView):

    @wrapper_error
    def post(self, request, uuid):
       return ServicesEvents.decline_invitation_course_editor(request, uuid)


class InviteCourseEditorView(APIView):

    @wrapper_error
    def post(self, request, id):
        """
        Создает запись в базе с приглашением стать редактором курса и
        отправляет письмо с ссылкой подтверждения.
        @param {
            email: string,
            permissions: {
                settings: bool,
                tariffs: bool,
                content: bool,
                students: bool,
                homeworks: bool
            }
        }
        @return: invite_id: ID приглашения
        """
        service = ServicesEvent(request, id)
        data = camel_case_to_under_case(json.loads(request.body))
        return service.invitation_become_course_editor(id, **data)


class UpdateCourseEditorView(APIView):

    @wrapper_error
    def patch(self, request, course_id, editor_id):
        """
        Удаляет пользователя с редакторов курса
        @param course_id: Id курса
        @param editor_id: Id пользователя(редактора курса)
        @return: id пользователя
        """
        service = ServicesEvent(request, course_id)
        data = camel_case_to_under_case(json.loads(request.body))
        return service.update_course_editor(int(editor_id), **data)

    @wrapper_error
    def put(self, request, course_id, editor_id):
        return self.patch(request, course_id, editor_id)


class RevokeCourseEditorView(APIView):

    @wrapper_error
    def delete(self, request, course_id, editor_id):
        """
        Удаляет пользователя с редакторов курса
        @param course_id: Id курса
        @param editor_id: Id пользователя(редактора курса)
        @return: id пользователя
        """
        return ServicesEvent(request, course_id).revoke_course_editor(int(editor_id))


class InviteCourseEditorListView(APIView):

    @wrapper_error
    def get(self, request, course_id):
        """
        Список приглашений стать редактором курса
        @param request:
        @param id: ID курса
        @return: [
                      {
                        "id": int,
                        "first_name": string,
                        "last_name": string,
                        "email": string,
                        "phone": string,
                        "joined": date,
                        "permissions": {
                          "settings": bool,
                          "tariffs": bool,
                          "content": bool,
                          "students": bool,
                          "homeworks": bool
                        }
                      },
                ]
        """
        return ServicesEvent(request, course_id).list_invitations_editors_course()


class RevokeInviteCourseEditorView(APIView):

    @wrapper_error
    def delete(self, request, course_id, invite_id):
        """
        Удаляет приглашение стать редактором курса
        @param course_id: Id курса
        @param invite_id: Id приглашения
        @return: id приглашения
        """
        return ServicesEvent(
            request, course_id).revoke_invitation_editors_course(invite_id)


class ManagedCoursesListView(APIView):

    @wrapper_error
    def get(self, request):
        return ServicesCourses(request).get_managed_courses()


class InvitationsListView(APIView):

    @wrapper_error
    def get(self, request):
        return ServicesCourses(request).get_invitations()
