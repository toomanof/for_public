from django.urls import path
from rest_framework.routers import DefaultRouter
from events.views import (
    CategoryEventsApiView,
    EventsApiView, UnitsApiView, LessonApiView,
    LessonElementsApiView,
    QuestionTestApiView,
    AnswersTestApiView,
    WebinarApiView, MessageApiView,
    VideoSourcesView,
    HomeWorkTaskApiView,
    IntervalTrainingElementApiView,
    CheckListElementApiView,
    ImageStorageApiViews,
    FileStorageApiViews,
    DesignEventApiView,
    SetFlagUnitsView,
    SetFlagPaidView,
    TarificationLinkAPIView,
    FlowAPIView,
    LessonElementsForStudents,
    CompleteLessonElementsForStudents,
    StudentStartTest,
    StudentAnswerToQuestionInTest,
    StudentDeliverHomeWork,
    StudentEventTreeElements,
    StudentCurrentElement,
    StudentNextElement,
    ListUnverifiedHomework,
    UnverifiedHomework,
    CheckHomeworkTask,
    EventStudentsList,
    CourseView,
    BlocksView,
    BlockView,
    BlockLessonsView,
    LessonView,
    StudentCourserInfo,
    CourseContentTree,
    CourseEditorsView,
    InviteCourseEditorListView,
    RevokeInviteCourseEditorView,
    InviteCourseEditorView,
    ActivationInviteCourseEditorView,
    DeclineInviteCourseEditorView,
    UpdateCourseEditorView,
    RevokeCourseEditorView,
    ManagedCoursesListView,
    InvitationsListView,
    CreateCertificateForStudents,
    EventStudentStepForward,
    EventStudentAccessClose,
    EventStudentAccessOpen,
)

from payments.views import TarificationAPIView, PromoCodeAPIView

app_name = 'api_events'

urlpatterns = [
    path('video-sources/', VideoSourcesView.as_view(), name='video_sources'),

    path('events/<event>/design/settings/', DesignEventApiView.as_view(),
         name='design_settings'),
    path('events/<event>/block-system/', SetFlagUnitsView.as_view(),
         name='set_flag_units'),
    path('events/<event>/paid/', SetFlagPaidView.as_view(),
         name='set_flag_paid'),
    path('events/<event>/tariffication/tariffs/link/',
         TarificationLinkAPIView.as_view(), name='tariffs_link'),
    path('events/<event>/homeworks/',
         ListUnverifiedHomework.as_view(),
         name='list_unchecked_homework'),
    path('events/<event>/homeworks/<lesson_element>/',
         UnverifiedHomework.as_view(),
         name='unchecked_homework'),
    path('events/<event>/homework_task/<homework_task_id>/',
         CheckHomeworkTask.as_view(),
         name='checked_homework'),
    path('events/<event>/students/',
         EventStudentsList.as_view(),
         name='list_students_in_event'),
    path('events/<event>/students/<student_id>/step-forward/',
         EventStudentStepForward.as_view(),
         name='student_step_forward_in_event'),
    path('events/<event>/students/<student_id>/access/close/',
         EventStudentAccessClose.as_view(),
         name='close_access_close_to_course'),
    path('events/<event>/students/<student_id>/access/open/',
         EventStudentAccessOpen.as_view(),
         name='open_access_close_to_course'),


    path('learning/elements/<element>/',
         LessonElementsForStudents.as_view(),
         name='lesson_elements_for_student'),
    path('learning/elements/<element>/complete/',
         CompleteLessonElementsForStudents.as_view(),
         name='complete_lesson_element_student'),

    path('learning/elements/<element>/certificate/',
         CreateCertificateForStudents.as_view(),
         name='create_certificate_for_student'),

    path('learning/elements/<element>/test/start/',
         StudentStartTest.as_view(),
         name='student_test_start'),
    path('learning/elements/<element>/test/questions/<question>/',
         StudentAnswerToQuestionInTest.as_view(),
         name='student_answer_to_question'),
    path('learning/elements/<element>/homeworks/<homework_task>/',
         StudentDeliverHomeWork.as_view(),
         name='student_deliver_homework'),

    path('learning/events/<event>/',
         StudentEventTreeElements.as_view(), name='student_event_tree_elements'),
    path('learning/events/<event>/current/',
         StudentCurrentElement.as_view(), name='student_current_element'),
    path('learning/events/<event>/next/',
         StudentNextElement.as_view(), name='student_next_element'),
]

urlpatterns += [
    path('courses/managed/',
         ManagedCoursesListView.as_view(),
         name='managed_courses_list'),
    path('courses/invited/',
         InvitationsListView.as_view(),
         name='managed_courses_list'),

    path('courses/<id>/',
         CourseView.as_view(), name='courser'),
    path('courses/<id>/blocks/',
         BlocksView.as_view(), name='blocks'),
    path('blocks/<id>/',
         BlockView.as_view(), name='block'),
    path('blocks/<id>/lessons/',
         BlockLessonsView.as_view(), name='block_lessons'),
    path('lessons/<id>/',
         LessonView.as_view(), name='lesson_new'),

    path('learning/courses/<event>/',
         StudentEventTreeElements.as_view(), name='student_course_tree_elements'),
    path('learning/courses/<id>/info/',
         StudentCourserInfo.as_view(), name='student_course_tree_elements'),
    path('learning/courses/<id>/tree/',
         CourseContentTree.as_view(), name='course_content_tree'),

    path('courses/<id>/editors/',
         CourseEditorsView.as_view(), name='course_editors'),

    path('courses/<id>/editors/invite/',
         InviteCourseEditorView.as_view(), name='invite_course_editor'),

    path('courses/<course_id>/editors/<editor_id>/',
         UpdateCourseEditorView.as_view(), name='update_course_editor'),

    path('courses/<course_id>/editors/<editor_id>/revoke/',
         RevokeCourseEditorView.as_view(), name='revoke_course_editor'),

    path('courses/<course_id>/invitations/',
         InviteCourseEditorListView.as_view(), name='invitations_course_editor'),

    path('invitations/active/<uuid>',
         ActivationInviteCourseEditorView.as_view(),
         name='activation_invitation_course_editor'),

    path('invitations/decline/<uuid>',
         DeclineInviteCourseEditorView.as_view(),
         name='decline_invitation_course_editor'),


    path('courses/<course_id>/invitations/<invite_id>/revoke/',
         RevokeInviteCourseEditorView.as_view(),
         name='revoke_invitation_course_editor'),

]

router = DefaultRouter()

router.register('category',
                CategoryEventsApiView, basename='categories_events')

router.register('events',
                EventsApiView, basename='events')

router.register(r'events/(?P<event>\d+)/flows',
                FlowAPIView, basename='flows')

router.register(r'events/(?P<event>\d+)/webinars',
                WebinarApiView, basename='webinars')

router.register(r'events/(?P<event>\d+)/units',
                UnitsApiView, basename='units')

router.register(r'events/(?P<event>\d+)/units/(?P<unit>\d+)/lessons',
                LessonApiView, basename='lessons')

router.register(r'events/(?P<event>\d+)/units/(?P<unit>\d+)/lessons/(?P<lesson>\d+)/elements',
                LessonElementsApiView, basename='lesson_elements')

router.register(r'lesson_elements/(?P<lesson_element_pk>\d+)/questions',
                QuestionTestApiView, basename='lesson_test_questions')

router.register(r'test_questions/(?P<question>\d+)/answers',
                AnswersTestApiView, basename='lesson_test_answers')

router.register(r'lesson_elements/(?P<lesson_element_pk>\d+)/homework_tasks',
                HomeWorkTaskApiView, basename='lesson_homework_tasks')

router.register(r'lesson_elements/(?P<lesson_element_pk>\d+)/interval_tranings',
                IntervalTrainingElementApiView, basename='lesson_interval_tranings')


router.register(r'lesson_elements/(?P<lesson_element_pk>\d+)/check_lists',
                CheckListElementApiView, basename='lesson_')

router.register('messages', MessageApiView, basename='messages')

# Закомментиовано до выяснения новой логики интервальных заданий
#router.register(
#    'schedules', SchedulePlanApiView, basename='schedule_plans')
#router.register(
#    'schedule-elements', ScheduleElementApiView, basename='schedule_elements')

router.register('image_storages', ImageStorageApiViews, basename='image_storages')

router.register('file_storages', FileStorageApiViews, basename='file_storages')

router.register(r'events/(?P<event>\d+)/tariffication/tariffs',
                TarificationAPIView, basename='tariffs')

router.register(r'events/(?P<event>\d+)/tariffication/promocodes',
                PromoCodeAPIView, basename='promocode')


urlpatterns += router.urls
