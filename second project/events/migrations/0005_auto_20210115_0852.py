# Generated by Django 3.0.2 on 2021-01-15 08:52

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('events', '0004_permissionevent'),
    ]

    operations = [
        migrations.CreateModel(
            name='EditorsEvent',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('settings', models.BooleanField(default=False, verbose_name='Доступ к основным настройкам')),
                ('tariffs', models.BooleanField(default=False, verbose_name='Доступ к тарифам')),
                ('content', models.BooleanField(default=False, verbose_name='Доступ к содержимому')),
                ('students', models.BooleanField(default=False, verbose_name='Доступ к студентам')),
                ('homeworks', models.BooleanField(default=False, verbose_name='Доступ к домашним задания')),
                ('date_create', models.DateTimeField(auto_now_add=True, verbose_name='Дата создание записи')),
                ('date_update', models.DateTimeField(auto_now=True, verbose_name='Дата обновление записи')),
            ],
        ),
        migrations.RemoveField(
            model_name='event',
            name='editors',
        ),
        migrations.DeleteModel(
            name='PermissionEvent',
        ),
        migrations.AddField(
            model_name='editorsevent',
            name='event',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='editors', to='events.Event', verbose_name='Курс'),
        ),
        migrations.AddField(
            model_name='editorsevent',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='editors_event', to=settings.AUTH_USER_MODEL, verbose_name='Пользователь'),
        ),
    ]
