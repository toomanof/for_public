# Generated by Django 3.0.2 on 2021-05-21 11:23

import django.core.validators
from django.db import migrations, models
import re


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0025_merge_20210521_1421'),
    ]

    operations = [
        migrations.AlterField(
            model_name='homeworktask',
            name='_scores',
            field=models.CharField(max_length=200, validators=[django.core.validators.RegexValidator(re.compile('^\\d+(?:,\\d+)*\\Z'), code='invalid', message='Enter only digits separated by commas.')], verbose_name='Баллы для оценивания'),
        ),
    ]
