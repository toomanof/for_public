# Generated by Django 3.0.2 on 2021-03-01 17:27

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0013_auto_20210228_1414'),
    ]

    operations = [
        migrations.AddField(
            model_name='currentelementeventforstudent',
            name='lesson_element',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='record_current_elements_for_student', to='events.LessonElement', verbose_name='Текущий элемент в списке'),
        ),
        migrations.AlterUniqueTogether(
            name='currentelementeventforstudent',
            unique_together=set(),
        ),
        migrations.RemoveField(
            model_name='currentelementeventforstudent',
            name='index',
        ),
    ]
