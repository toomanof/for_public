from django.core.validators import validate_comma_separated_integer_list
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver


from .abstract_models import AbstractNotRemoveModel
from .lesson_elements import LessonElement


class HomeWork(AbstractNotRemoveModel):
    """
    Модель содержащая домашнее задание к соответсвующиему типу урока
    """
    CREDIT_FORM, EVALUATION_FORM = range(2)
    CHOICES_FORM_TYPE = (
        (CREDIT_FORM, 'Зачётная форма'),
        (EVALUATION_FORM, 'Оценочная форма'),
    )
    lesson_element = models.OneToOneField(
        LessonElement, on_delete=models.CASCADE,
        related_name='home_work', verbose_name='Урок',
        blank=True, null=True)
    type_form = models.PositiveSmallIntegerField(
        'Тип блока', choices=CHOICES_FORM_TYPE,
        default=CREDIT_FORM)

    class Meta:
        verbose_name = 'Домашнее задание'
        verbose_name_plural = 'Домашние задания'


class HomeWorkTask(AbstractNotRemoveModel):
    home_work = models.ForeignKey(
        HomeWork, on_delete=models.CASCADE,
        related_name='tasks', blank=True, null=True)
    body = models.TextField('Задание', blank=True, null=True)
    answer = models.TextField('Ответ', blank=True, null=True)
    success_score = models.PositiveSmallIntegerField(
        'Бал за выполнение урока', default=2)
    failure_score = models.SmallIntegerField(
        'Бал за не выполнение урока', default=0)
    _scores = models.CharField(
        'Баллы для оценивания', max_length=200,
        validators=[validate_comma_separated_integer_list])
    order = models.PositiveSmallIntegerField(
    default=0, blank=False, null=False, verbose_name='Вес', unique=True)


@receiver(post_save, sender=LessonElement)
def lesson_post_save(sender, instance, **kwargs):
    if instance.is_home_work:
        HomeWork.objects.get_or_create(lesson_element=instance)
