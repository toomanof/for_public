from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver


from .abstract_models import AbstractNotRemoveModel
from .lesson_elements import LessonElement


class TestTypeLessonElement(AbstractNotRemoveModel):
    """
    Модель содержащая тестовые задания к соответсвующиему типу урока
    """

    lesson_element = models.OneToOneField(
        LessonElement, on_delete=models.CASCADE,
        related_name='test', verbose_name='Урок',
        blank=True, null=True)
    randomize = models.BooleanField(
        'Порядок следования вопросов', default=True)
    count_positive_answers = models.PositiveSmallIntegerField(
        'Условия сдачи теста', default=0, blank=False, null=False)

    class Meta:
        verbose_name = 'Тип тест - элемента урока'
        verbose_name_plural = 'Тип тест - элементов уроков'


@receiver(post_save, sender=LessonElement)
def lesson_post_save(sender, instance, **kwargs):
    if instance.is_test:
        TestTypeLessonElement.objects.get_or_create(lesson_element=instance)
