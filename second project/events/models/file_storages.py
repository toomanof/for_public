from django.contrib.auth.models import User
from django.db import models



class FileStorage(models.Model):
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='file_storages',
        verbose_name='')
    file = models.FileField(
        upload_to='uploads', blank=True, null=True, verbose_name='Файл',
        max_length=255)
    url_third_party_res = models.URLField(
        'Ссылка на сторонний ресурс', max_length=255, blank=True, null=True)
