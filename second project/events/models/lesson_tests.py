from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver


from .abstract_models import AbstractNotRemoveModel
from .lesson_elements import LessonElement
from .test_type_lesson_elements import TestTypeLessonElement


class QuestionTest(AbstractNotRemoveModel):
    """
    Модель содержащая тесты к соответсвующиему типу элемента урока
    """
    lesson_test_element = models.ForeignKey(
        TestTypeLessonElement, on_delete=models.CASCADE,
        related_name='questions', verbose_name='Элемент урока',
        blank=True, null=True)
    body = models.TextField('Вопрос', blank=True, null=True)
    randomize = models.BooleanField(
        'Порядок следования ответов', default=True)
    order = models.PositiveSmallIntegerField(
    default=0, blank=False, null=False, verbose_name='Вес', unique=True)

    class Meta:
        verbose_name = 'Тестовое занятие'
        verbose_name_plural = 'Тестовые занятия'
        ordering = ['-order']


class AnswerTest(AbstractNotRemoveModel):
    """
    Модель вариантов ответов на вопросы тестов
    """
    body = models.TextField('Ответ')
    correct = models.BooleanField('Флаг правильности ответа', default=False)
    question = models.ForeignKey(
        QuestionTest, on_delete=models.CASCADE,
        related_name='answers', verbose_name='тест', blank=True, null=True)

    def __str__(self):
        return f"{self.body} {self.correct}"
