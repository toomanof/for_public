from django.db import models

from .abstract_models import (
    AbstractNotRemoveModel, AbstractModelWithTitle,
    AbstractWithDescription)
from .lessons import Lesson


class LessonElement(AbstractModelWithTitle, AbstractWithDescription,
                    AbstractNotRemoveModel):
    """
    Модель елементов урока. Явлеется подчененным урок объектом.
    Может быть различного типа.
    Для типов: (ET_TEST, ET_HOME_WORK, ET_INTERVAL) существуют свои модели,
    с отношением один к одному, в которых указаны соответсвующие настройки
    елемента.
    """
    (ET_TEXT, ET_VIDEO, ET_TEST, ET_WEBINAR, ET_HOME_WORK,
     ET_PAUSE, ET_INTERVAL, ET_CHECK_LIST, ET_ZOOM,
     ET_LINK, ET_CERTIFICATE) = range(11)
    CHOICES_ET_TYPES = (
        (ET_TEXT, 'Элемент текст',),
        (ET_VIDEO, 'Элемент видео',),
        (ET_TEST, 'Элемент тест',),
        (ET_WEBINAR, 'Элемент вебинар',),
        (ET_HOME_WORK, 'Элемент домашнее задание',),
        (ET_PAUSE, 'Элемент пауза',),
        (ET_INTERVAL, 'Элемент интервальная тренировка'),
        (ET_CHECK_LIST, 'Элемент чек-лист'),
        (ET_ZOOM, 'Элемент zoom'),
        (ET_LINK, 'Элемент ссылка'),
        (ET_CERTIFICATE, 'Элемент сертификат')
    )
    lesson = models.ForeignKey(
        Lesson, on_delete=models.CASCADE,
        related_name='elements', verbose_name='Урок')
    _type = models.PositiveSmallIntegerField(
        'Тип урока', choices=CHOICES_ET_TYPES,
        default=ET_TEXT)
    task = models.TextField(
        blank=True, null=True, verbose_name='Задание')
    order = models.PositiveSmallIntegerField(
        default=0, blank=False, null=False, verbose_name='Вес', unique=False)
    success_score = models.PositiveSmallIntegerField(
        'Бал за выполнение урока', default=2)
    failure_score = models.SmallIntegerField(
        'Бал за не выполнение урока', default=0)

    class Meta:
        verbose_name = 'Элемент урока'
        verbose_name_plural = 'Элементы уроков'


    @property
    def is_text(self):
        return self._type == self.ET_TEXT

    @property
    def is_video(self):
        return self._type == self.ET_VIDEO

    @property
    def is_test(self):
        return self._type == self.ET_TEST

    @property
    def is_webinar(self):
        return self._type == self.ET_WEBINAR

    @property
    def is_home_work(self):
        return self._type == self.ET_HOME_WORK

    @property
    def is_pause(self):
        return self._type == self.ET_PAUSE

    @property
    def is_interval(self):
        return self._type == self.ET_INTERVAL

    @property
    def is_check_list(self):
        return self._type == self.ET_CHECK_LIST

    @property
    def is_zoom(self):
        return self._type == self.ET_ZOOM

    @property
    def is_link(self):
        return self._type == self.ET_LINK

    @property
    def is_certificate(self):
        return self._type == self.ET_CERTIFICATE
