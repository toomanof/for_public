from django.db import models

from .abstract_models import (
    AbstractNotRemoveModel, AbstractModelWithTitle,
    AbstractWithDescription, VideoMixin)
from .units import Unit


class Lesson(AbstractModelWithTitle,
             AbstractWithDescription, AbstractNotRemoveModel):
    EA_STEP_BY_STEP, EA_SAME_TIME = range(2)
    CHOICES_ACCESSIBILITY_OF_ELEMENTS = (
        (EA_STEP_BY_STEP, 'Поэтапно'),
        (EA_SAME_TIME, 'Сразу')
    )
    unit = models.ForeignKey(
        Unit, on_delete=models.CASCADE,
        related_name='lessons', verbose_name='Блок')
    pass_count = models.PositiveSmallIntegerField(
        'Процент прохождения', default=0)
    order = models.PositiveSmallIntegerField(
        default=0, blank=False, null=False, verbose_name='Вес', unique=False)
    fixing_time = models.BooleanField(
        'Фиксировное время прохождения урока', default=False)
    lesson_duration = models.PositiveSmallIntegerField(
        'Продолжительность урока', default=1)
    success_score = models.PositiveSmallIntegerField(
        'Бал за выполнение урока', default=2)
    failure_score = models.SmallIntegerField(
        'Бал за не выполнение урока', default=0)
    score_system_is_used = models.BooleanField(
        'Флаг пользования системы оценивания', default=False)
    accessibity_elements = models.PositiveSmallIntegerField(
        'Доступность элементов', choices=CHOICES_ACCESSIBILITY_OF_ELEMENTS,
        default=EA_STEP_BY_STEP)

    class Meta:
        verbose_name = 'Урок'
        verbose_name_plural = 'Уроки'

    def get_previos_lessons(self):
        """
        Возрат списка предшествующих уроков от представленного в __init__.
        """
        return self.unit.lessons.all_active().filter(order__lt=self.order)

    def pervios_lessons_is_execution(self, student):
        """
        Возращает флаг завершенности предшествующих уроков от
        представленного в __init__.
        True если список предшествующих уроков пуст или
        список записей в 'реестре выполнения' предшествующих уроков без
        отметки выполнения пуст
        """
        from .reestrs import ReestrExecution
        
        list_previos_lessons_pk = self.get_previos_lessons().values_list(
            'pk', flat=True)
        if not list_previos_lessons_pk:
            return True
        return not ReestrExecution.objects.filter(
            event=self.unit.event,
            lesson__pk__in=list_previos_lessons_pk,
            student=student
        ).exists()

    def is_accessible_for_student(self, student):
        """
        Возращает флаг доступности для студента.
        Производится проверка на закрытие предыдущих уроков
        """
        return self.pervios_lessons_is_execution(student)

    def rating(self, student):
        """
        Возвращает оценку студента за урок.
        """
        rating = 0
        record_reestr = self.records_rating.filter(student=student).first()
        if record_reestr:
            rating = record_reestr.rating
        return rating

    def test_passed(self, student):
        """
        Возвращает флаг прохождения теста урока студентом.
        """
        pass

    def lesson_is_execution(self, student):
        """
        Возвращает флаг прохождения урока урока студентом.
        """
        return self.records_execution.filter(
            lesson=self,
            student=student
        ).exists()
