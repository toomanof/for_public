from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver


from .abstract_models import AbstractNotRemoveModel
from .lesson_elements import LessonElement


class TextTypeLessonElement(AbstractNotRemoveModel):
    """
    Модель содержащая текстовое задание к соответсвующиему типу урока
    """

    lesson_element = models.OneToOneField(
        LessonElement, on_delete=models.CASCADE,
        related_name='text', verbose_name='Урок',
        blank=True, null=True)
    body = models.TextField('Содержимое', blank=True, null=True)

    class Meta:
        verbose_name = 'Тип текст - элемента урока'
        verbose_name_plural = 'Тип текст - элементов уроков'


@receiver(post_save, sender=LessonElement)
def lesson_post_save(sender, instance, **kwargs):
    if instance.is_text:
        TextTypeLessonElement.objects.get_or_create(lesson_element=instance)
