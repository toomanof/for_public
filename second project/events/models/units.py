from django.db import models
from django.db.models import Max, Min

from .abstract_models import (
    AbstractNotRemoveModel, AbstractModelWithTitle)
from .events import Event


class Unit(AbstractModelWithTitle, AbstractNotRemoveModel):
    UT_RELAX, UT_LESSONS = range(2)
    CHOICES_UNIT_TYPES = (
        (UT_RELAX, 'блок отдыха', ),
        (UT_LESSONS, 'блок уроков', ),
    )

    event = models.ForeignKey(
        Event, on_delete=models.CASCADE,
        related_name='units', verbose_name='Курс')
    summary = models.TextField(
        blank=True, null=True, verbose_name='Краткое описание')
    open_only_previous_execution = models.BooleanField(
        'Флаг открытия только по завершению предыдущего', default=True)
    unit_type = models.PositiveSmallIntegerField(
        'Тип блока', choices=CHOICES_UNIT_TYPES,
        default=UT_LESSONS)
    days_duration_relax = models.PositiveSmallIntegerField(
        'Продолжительность дней отдыха', blank=True, null=True)
    hours_duration_relax = models.PositiveSmallIntegerField(
        'Продолжительность часов отдыха', blank=True, null=True)
    order = models.PositiveSmallIntegerField(
        default=0, blank=False, null=False, verbose_name='Вес', unique=False)

    class Meta:
        verbose_name = 'Блок курса'
        verbose_name_plural = 'Блоки курса'
#        unique_together =(
#            'order',
#            'event'
#        )

    def delete(self, *args, **kwargs):
        if self.event.units.count() <= 1:
            return
        super().delete(*args, **kwargs)

    def get_previos_units(self):
        """
        Возрат списка предшествующих блоков от представленного в __init__.
        """
        return self.event.units.filter(order__lt=self.order)

    def pervios_units_is_execution(self, student):
        """
        Возращает флаг завершенности предшествующих блоков от
        представленного в __init__.
        True если список предшествующих блоков пуст или
        список записей в 'реестре выполнения' предшествующих блоков без
        отметки выполнения пуст
        """
        from .reestrs import ReestrExecution

        list_previos_block_pk = self.get_previos_units().values_list(
            'pk', flat=True)
        if not list_previos_block_pk:
            return True
        return not ReestrExecution.objects.filter(
            event=self.event,
            unit__pk__in=list_previos_block_pk,
            student=student
        ).exists()

    def is_accessible_for_student(self, student):
        """
        Возращает флаг доступности для студента.
        Если у блока не установлен флаг
        'открыт только по завершению предыдущего', то блок доступен.
        Иначе идет проверка на закрытие предыдущих блоков
        """
        if not self.open_only_previous_execution:
            return True
        return self.pervios_units_is_execution(student)

    @property
    def is_relax(self):
        return self.unit_type == self.UT_RELAX

    def get_max_order(self):
        result = self.event.units.aggregate(Max('order'))['order__max']
        return result if result else 0

    def get_min_order(self):
        result = self.event.units.aggregate(Min('order'))['order__min']
        return result if result else 0

    def get_max_order_element(self):
        return self.event.units.filter(
            order=self.get_max_order()).first()

    def get_min_order_element(self):
        return self.event.units.filter(
            order=self.get_min_order()).first()

    def next_order(self):
        max_item = self.get_max_order()
        if isinstance(max_item, int) and max_item > 0:
            return max_item + 1
        else:
            return 0

    def set_order(self, value):
        """
        Устанавливает вес объекту.
        При этом вес элементов у которых он равен или больше значения value
        устанавливается на единицу больше.
        """
        if value == self.order:
            return
        items = self.event.units.filter(order__gte=value).order_by('-order')
        for item in items:
            item.order += 1
            item.save()
        self.order = value
        self.save()