from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

from .abstract_models import AbstractNotRemoveModel
from .lesson_elements import LessonElement


class CertificateTypeLessonElement(AbstractNotRemoveModel):
    """
    Модель типа сертификат элементов урока.
    """
    lesson_element = models.OneToOneField(
        LessonElement, on_delete=models.CASCADE,
        related_name='certificate', verbose_name='Сертификат',
        blank=True, null=True)
    title = models.CharField('Название', max_length=254)
    signature = models.CharField('Подпись', max_length=254)
    date_expiration = models.DateField('Дата окончания курса',
                                       blank=True, null=True)

    class Meta:
        verbose_name = 'Тип сертификат - элемента урока'
        verbose_name_plural = 'Тип сертификат - элементов уроков'

    def is_expiration(self):
        return self.date_expiration is not None


@receiver(post_save, sender=LessonElement)
def lesson_post_save(sender, instance, **kwargs):
    if instance.is_certificate:
        CertificateTypeLessonElement\
            .objects.\
            get_or_create(
                lesson_element=instance,
                defaults={
                    'title': instance.lesson.unit.event.title,
                    'signature': instance.lesson.unit.event.owner.school.title
                }
            )
