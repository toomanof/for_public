from django.contrib.auth.models import User
from django.db import models

from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFill


class ImageStorage(models.Model):
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='image_storages',
        verbose_name='')
    image = models.ImageField(
        upload_to='uploads', blank=True, null=True, verbose_name='Изображение',
        max_length=255)
    cover = ImageSpecField(source='image',
                           processors=[ResizeToFill(640, 640)],
                           format='JPEG',
                           options={'quality': 80})

    @property
    def filename(self):
        return self.image.name
