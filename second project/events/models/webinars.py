from django.db import models
from django.utils import timezone

from .abstract_models import (
    AbstractNotRemoveModel, VideoMixin
)
from .events import Event
from .file_storages import FileStorage

class Webinar(VideoMixin, AbstractNotRemoveModel):
    title = models.CharField(
        max_length=100, blank=False, null=False, verbose_name='Наименование')
    event = models.ForeignKey(
        Event, on_delete=models.CASCADE,
        related_name='webinars', verbose_name='Курс')
    start_date = models.DateTimeField(
        'Время начала занятия', default=timezone.now)
    pdf_file = models.FileField(
        'PDF file', upload_to='uploads/pdf_webinars', blank=True, null=True)
    published = models.BooleanField('Флаг публикации', default=False)
    file_storages = models.ManyToManyField(
        FileStorage, verbose_name='Хранилище файлов')

    class Meta:
        verbose_name = 'Вебинар'
        verbose_name_plural = 'Вебинары'

    def __str__(self):
        return f"{self.title}"
