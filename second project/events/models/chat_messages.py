from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone

from .webinars import Webinar


class Message(models.Model):
    webinar = models.ForeignKey(
        Webinar, on_delete=models.CASCADE,
        related_name='messages', verbose_name='Сообщение в чате')
    author = models.ForeignKey(
        User, on_delete=models.CASCADE,
        related_name='chat_messages', verbose_name='Автор')
    body = models.CharField(
        max_length=254, blank=False, null=False, verbose_name='Тело сообщения')
    pub_date = models.DateTimeField('Дата сообщения', default=timezone.now)