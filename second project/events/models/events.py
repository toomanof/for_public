import decimal
from django.contrib.auth.models import User
from django.db import models

from mptt.models import MPTTModel, TreeForeignKey
from jsonfield import JSONField
from .abstract_models import (
    AbstractNotRemoveModel, AbstractModelWithTitle, AbstractWithDescription)


class CategoryEvent(AbstractNotRemoveModel, MPTTModel,
                    AbstractModelWithTitle, AbstractWithDescription):
    """Категории курсов"""
    parent = TreeForeignKey('self', on_delete=models.CASCADE,
                            null=True, blank=True, related_name='children')

    class Meta:
        verbose_name = 'Категория курса'
        verbose_name_plural = 'Категория курсов'

    class MPTTMeta:
        order_insertion_by = ['title']


class Event(AbstractModelWithTitle,
            AbstractWithDescription, AbstractNotRemoveModel):
    GS_YES_NO, GS_POINT = range(2)
    CHOICES_GRADING_SYSTEM_TYPES =(
        (GS_YES_NO, 'сдал/не сдал'),
        (GS_POINT, 'балльная система оценивания')
    )
    owner = models.ForeignKey(
        User, on_delete=models.CASCADE,
        related_name='owner_events', verbose_name='Владелец курса')
    participants = models.ManyToManyField(
        User, related_name='events', verbose_name='Участники курса')
    category = models.ForeignKey(
        CategoryEvent, on_delete=models.PROTECT, null=True, blank=True,
        related_name='events', verbose_name='категория')
    published = models.BooleanField('Флаг публикации', default=False)
    open_all_unit = models.BooleanField(
        'Флаг открытости блоков', default=False)
    grading_system = models.PositiveSmallIntegerField(
        '', choices=CHOICES_GRADING_SYSTEM_TYPES,
        default=GS_POINT)
    default_grade_completed_lesson = models.PositiveSmallIntegerField(
        'Оценка по умолчанию пройденого урока', default=0)
    fixed_date_begin = models.BooleanField(
        'Флаг фиксированной даты начала', default=False)
    date_begin = models.DateTimeField(
        'Дата начала', blank=True, null=True)
    domain_name = models.CharField(
        max_length=255, blank=True, verbose_name='Доменное имя', null=True)
    has_units = models.BooleanField('Флаг блочности курса', default=False)
    meta = JSONField('Meta', blank=True, null=True)
    paid = models.BooleanField('Платный/Бесплатный', default=True)

    class Meta:
        verbose_name = 'Курс'
        verbose_name_plural = 'Курсы'

    def student_is_attach(self, student):
        return student in self.participants.all()

    @property
    def payment(self):
        return False

    @property
    def percent_completion(self):
        return 0

    def __init__(self, *args, **kwargs):
        super(Event, self).__init__(*args, **kwargs)
        self.old_value_paid = self.paid

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        if self.old_value_paid != self.paid and not self.paid:
            self.create_free_tariff()

    @property
    def lessons_elements(self):
        elements = []
        for unit in self.units.all_active():
            for lesson in unit.lessons.all_active():
                for element in lesson.elements.all_active():
                    elements.append(element)
        return elements

    def create_free_tariff(self):
        try:
            self.tariffications.all().delete()
        except Exception:
            raise Exception('Not was deleted tariffication! They is written in registres.')

        tariff = self.tariffications.create(
            title=f"Бесплатный тариф курса: {self.title}",
            paid=False,
            price=decimal.Decimal(0.00)
        )

        for lesson_element in self.lessons_elements:
            tariff.elements.create(lesson_element=lesson_element)

    def is_started(self, student):
        return self.records_execution.filter(student=student).exists()

    def is_finished(self, student):
        from billing.services import BillingService
        from payments.models import TarifficationElement

        count_avalaibe_elements = TarifficationElement.objects.filter(
            parent=BillingService.get_current_tariff(student, self),
            lesson_element__is_deleted=False,
            lesson_element__lesson__is_deleted=False,
            lesson_element__lesson__unit__is_deleted=False).distinct().count()

        count_completed_lesson_elements =\
            self.records_execution.filter(student=student).count()

        return count_avalaibe_elements == count_completed_lesson_elements


class EditorsEvent(models.Model):
    event = models.ForeignKey(
        Event, on_delete=models.CASCADE,
        related_name='editors', verbose_name='Курс')
    user = models.ForeignKey(
        User, on_delete=models.CASCADE,
        related_name='editors_event', verbose_name='Пользователь')
    settings = models.BooleanField('Доступ к основным настройкам', default=False)
    tariffs = models.BooleanField('Доступ к тарифам', default=False)
    content = models.BooleanField('Доступ к содержимому', default=False)
    students = models.BooleanField('Доступ к студентам', default=False)
    homeworks = models.BooleanField('Доступ к домашним задания', default=False)
    date_create = models.DateTimeField('Дата создание записи',
                                       auto_now_add=True)
    date_update = models.DateTimeField('Дата обновление записи',
                                       auto_now=True)

    class Meta:
        unique_together = (('event', 'user'),)
