from django.contrib.auth.models import User
from django.db import models

from .events import Event
from .lessons import Lesson
from .lesson_elements import LessonElement
from .lesson_tests import QuestionTest, AnswerTest
from .home_work_type_lesson_elements import HomeWorkTask
from .units import Unit


class CurrentElementEventForStudent(models.Model):
    lesson_element = models.ForeignKey(
        LessonElement,
        on_delete=models.PROTECT,
        related_name='record_current_elements_for_student',
        verbose_name='Текущий элемент в списке',
        blank=True, null=True)
    student = models.ForeignKey(
        User, on_delete=models.PROTECT,
        related_name='record_current_elements_for_student',
        verbose_name='Студент курса')
    event = models.ForeignKey(
        Event, on_delete=models.PROTECT,
        related_name='record_current_elements_for_student',
        verbose_name='Курс',
        blank=True, null=True)
    date_create = models.DateTimeField(
        'Дата записи', auto_now_add=True)
    date_update = models.DateTimeField('Дата обновление записи',
                                       auto_now=True)

    class Meta:
        verbose_name = 'Запись реестра текущих элементов курса для студентов'
        verbose_name_plural = 'Реестр текущих элементов курса для студентов'


class ReestrExecution(models.Model):
    event = models.ForeignKey(
        Event, on_delete=models.PROTECT,
        related_name='records_execution',
        verbose_name='Курс',
        blank=True, null=True)
    unit = models.ForeignKey(
        Unit, on_delete=models.PROTECT,
        related_name='records_execution',
        verbose_name='Блок',
        blank=True, null=True)
    lesson = models.ForeignKey(
        Lesson, on_delete=models.CASCADE,
        related_name='records_execution',
        verbose_name='Урок',
        blank=True, null=True)
    lesson_element = models.ForeignKey(
        LessonElement, on_delete=models.PROTECT,
        related_name='records_execution',
        verbose_name='Элемент урока',
        blank=True, null=True)
    home_work_task = models.ForeignKey(
        HomeWorkTask, on_delete=models.PROTECT,
        related_name='records_execution',
        verbose_name='элемент домашнего задания',
        blank=True, null=True)
    student = models.ForeignKey(
        User, on_delete=models.PROTECT,
        related_name='records_execution',
        verbose_name='Студент курса')
    score = models.SmallIntegerField(
        'Бал за выполнение урока',
        blank=True, null=True)
    date_completion = models.DateTimeField(
        'Дата выполнения', blank=True, null=True)
    date_update = models.DateTimeField('Дата обновление записи',
                                       auto_now=True)

    class Meta:
        verbose_name = 'Элемент реестра выполнения уроков'
        verbose_name_plural = 'Реестр выполнения уроков'


class ReestrDeliveryHomeWorkTask(models.Model):
    lesson_element = models.ForeignKey(
        LessonElement, on_delete=models.PROTECT,
        related_name='records_delivery_homework_task',
        verbose_name='Элемент урока',
        blank=True, null=True)
    home_work_task = models.ForeignKey(
        HomeWorkTask, on_delete=models.PROTECT,
        related_name='records_delivery_homework_task',
        verbose_name='элемент домашнего задания')
    student = models.ForeignKey(
        User, on_delete=models.PROTECT,
        related_name='records_delivery_homework_task',
        verbose_name='Студент курса')
    answer = models.TextField('Ответ', blank=True, null=True)
    date_create = models.DateTimeField('Дата создание записи',
                                       auto_now_add=True)
    date_update = models.DateTimeField('Дата обновление записи',
                                       auto_now=True)
    score = models.SmallIntegerField(
        'Бал за выполнение урока',
        blank=True, null=True)
    lector = models.ForeignKey(
        User, on_delete=models.PROTECT,
        related_name='records_homework_task_as_lector',
        verbose_name='Лектор курса',
        blank=True, null=True)
    comment_lector = models.TextField(
        'Коментарий к ответу', blank=True, null=True)
    date_of_check = models.DateTimeField(
        'Дата проверки', blank=True, null=True)


    class Meta:
        verbose_name = 'Запись реестра здачи элементов домашнего задания'
        verbose_name_plural = 'Реестр здачи элементов домашнего задания'


class ReestrAnswerInTest(models.Model):
    lesson_element = models.ForeignKey(
        LessonElement, on_delete=models.PROTECT,
        related_name='records_answers_in_test',
        verbose_name='Элемент урока',
        blank=True, null=True)
    question = models.ForeignKey(
        QuestionTest, on_delete=models.PROTECT,
        related_name='records_answers_in_test',
        verbose_name='элемент домашнего задания')
    answer = models.ForeignKey(
        AnswerTest, on_delete=models.PROTECT,
        related_name='records_answers_in_test',
        verbose_name='элемент домашнего задания')
    student = models.ForeignKey(
        User, on_delete=models.PROTECT,
        related_name='records_answers_in_test',
        verbose_name='Студент курса')
    correct = models.BooleanField('Флаг правильности ответа', default=False)
    date_create = models.DateTimeField('Дата создание записи',
                                       auto_now_add=True)
    date_update = models.DateTimeField('Дата обновление записи',
                                       auto_now=True)

    class Meta:
        verbose_name = 'Запись реестра ответов на вопросы тестов'
        verbose_name_plural = 'Реестр ответов на вопросы тестов'


class ReestrRating(models.Model):
    """
    Реестр отметок проверки домашних заданий
    """
    lesson = models.ForeignKey(
        Lesson, on_delete=models.CASCADE,
        related_name='records_rating',
        verbose_name='Урок',
        blank=True, null=True)
    lesson_element = models.ForeignKey(
        LessonElement, on_delete=models.CASCADE,
        related_name='records_rating',
        verbose_name='Элемент урока')
    home_work_task = models.ForeignKey(
        HomeWorkTask, on_delete=models.CASCADE,
        related_name='records_rating',
        verbose_name='элемент домашнего задания')
    score = models.SmallIntegerField('Бал за выполнение урока')
    student = models.ForeignKey(
        User, on_delete=models.CASCADE,
        related_name='records_rating',
        verbose_name='Студент курса')
    lector = models.ForeignKey(
        User, on_delete=models.CASCADE,
        related_name='records_rating_as_lector',
        verbose_name='Лектор курса',
        blank=True, null=True)
    date_create = models.DateTimeField('Дата создание записи',
                                       auto_now_add=True)
    date_update = models.DateTimeField('Дата обновление записи',
                                       auto_now=True)

    class Meta:
        verbose_name = 'Элемент реестра оценки уроков'
        verbose_name_plural = 'Реестр оценки уроков'


class StudentCertificate(models.Model):
    student = models.ForeignKey(
        User, on_delete=models.PROTECT,
        related_name='certificates',
        verbose_name='Студент курса')
    title = models.CharField('Название', max_length=254)
    signature = models.CharField('Подпись', max_length=254)
    date_expiration = models.DateField('Дата окончания курса',
                                       blank=True, null=True)
    date_create = models.DateTimeField('Дата создания', auto_now_add=True)
    date_update = models.DateTimeField('Дата обновление записи',
                                       auto_now=True)

    class Meta:
        verbose_name = 'Сертификат студента'
        verbose_name_plural = 'Сертификаты студентов'


class PauseForStudent(models.Model):
    lesson_element = models.ForeignKey(
        LessonElement,
        on_delete=models.PROTECT,
        related_name='pauses_for_student',
        verbose_name='Элемент паузы',
        blank=True, null=True)

    student = models.ForeignKey(
        User, on_delete=models.PROTECT,
        related_name='pauses',
        verbose_name='Студент курса')
    date_create = models.DateTimeField(
        'Дата записи', auto_now_add=True)
    date_update = models.DateTimeField('Дата обновление записи',
                                       auto_now=True)
    date_end = models.DateTimeField('Дата и время окончания паузы', blank=True, null=True)

    class Meta:
        verbose_name = 'Запись паузы для студента'
        verbose_name_plural = 'Реестр пауз для студенов'
