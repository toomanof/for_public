from django.db import models

from .abstract_models import AbstractNotRemoveModel
from .events import Event


class Flow(AbstractNotRemoveModel):
    event = models.ForeignKey(
        Event, on_delete=models.CASCADE,
        related_name='flows', verbose_name='Курс')
    title = models.CharField('Наименование',
                             max_length=250, blank=False,
                             null=False, default='Поток')
    date_begin = models.DateTimeField(
        'Дата начала', blank=True, null=True)
    is_enabled = models.BooleanField('Включенный', default=True)

    class Meta:
        verbose_name = 'Поток курса'
        verbose_name_plural = 'Потоки курсов'
