from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver


from .abstract_models import AbstractNotRemoveModel
from .lesson_elements import LessonElement


class LinkTypeLessonElement(AbstractNotRemoveModel):
    """
    Модель содержащая текстовое задание к соответсвующиему типу урока
    """

    lesson_element = models.OneToOneField(
        LessonElement, on_delete=models.CASCADE,
        related_name='link', verbose_name='Урок',
        blank=True, null=True)
    title = models.CharField('Название', max_length=254)
    description = models.TextField('Описание', blank=True, null=True)
    url = models.URLField('Ссылка')

    class Meta:
        verbose_name = 'Тип ссылка - элемента урока'
        verbose_name_plural = 'Тип ссылка - элементов уроков'


@receiver(post_save, sender=LessonElement)
def lesson_post_save(sender, instance, **kwargs):
    if instance.is_link:
        LinkTypeLessonElement.objects.get_or_create(lesson_element=instance)
