from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver


from .abstract_models import AbstractNotRemoveModel
from .lesson_elements import LessonElement


ONE_DAY = 60 * 60 * 24


class PauseTypeLessonElement(AbstractNotRemoveModel):
    """
    Модель пауза типа елементов урока.
    """
    lesson_element = models.OneToOneField(
        LessonElement, on_delete=models.CASCADE,
        related_name='pause', verbose_name='Урок',
        blank=True, null=True)
    until_next_day = models.BooleanField("До следующего дня", default=True)
    seconds_count = models.PositiveIntegerField("Количество секунд паузы",
                                               default=ONE_DAY)

    class Meta:
        verbose_name = 'Тип пауза - элемента урока'
        verbose_name_plural = 'Тип пауза - элементов уроков'


@receiver(post_save, sender=LessonElement)
def lesson_post_save(sender, instance, **kwargs):
    if instance.is_pause:
        obj = PauseTypeLessonElement.objects.get_or_create(lesson_element=instance)
