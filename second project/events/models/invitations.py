import uuid
from django.db import models

from events.models import Event


class InvitationEditorCourse(models.Model):

    event = models.ForeignKey(
        Event, on_delete=models.CASCADE,
        related_name='invitations_to_editors_courses', verbose_name='Курс')
    email = models.CharField(
        'Email приглашаемого пользователя',
        max_length=255)
    link = models.UUIDField('Ссылка', editable=False, default=uuid.uuid4)
    settings = models.BooleanField('Доступ к основным настройкам', default=False)
    tariffs = models.BooleanField('Доступ к тарифам', default=False)
    content = models.BooleanField('Доступ к содержимому', default=False)
    students = models.BooleanField('Доступ к студентам', default=False)
    homeworks = models.BooleanField('Доступ к домашним задания', default=False)
    date_create = models.DateTimeField(
        'Дата создание записи',
        auto_now_add=True)
    date_update = models.DateTimeField(
        'Дата обновление записи',
        auto_now=True)
