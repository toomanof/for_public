from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver


from core.models import AbstractNotRemoveModel, VideoMixin
from .lesson_elements import LessonElement


class VideoTypeLessonElement(VideoMixin, AbstractNotRemoveModel):
    """
    Модель видео типа елементов урока.
    """
    lesson_element = models.OneToOneField(
        LessonElement, on_delete=models.CASCADE,
        related_name='video', verbose_name='Урок',
        blank=True, null=True)

    class Meta:
        verbose_name = 'Тип видео - элемента урока'
        verbose_name_plural = 'Тип видео - элементов уроков'


@receiver(post_save, sender=LessonElement)
def lesson_post_save(sender, instance, **kwargs):
    if instance.is_video:
        obj = VideoTypeLessonElement.objects.get_or_create(lesson_element=instance)
