from datetime import time

from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver


from .abstract_models import AbstractNotRemoveModel
from .lesson_elements import LessonElement


class CheckListTypeLessonElement(AbstractNotRemoveModel):
    """
    Модель содержащая домашнее задание к соответсвующиему типу урока
    """
    lesson_element = models.OneToOneField(
        LessonElement, on_delete=models.CASCADE,
        related_name='check_list', verbose_name='элемент урока',
        blank=True, null=True)
    class Meta:
        verbose_name = 'Тип чек лист- элемента урока'
        verbose_name_plural = 'Тип чек лист - элементов уроков'


class CheckListElement(AbstractNotRemoveModel):
    parent = models.ForeignKey(
        CheckListTypeLessonElement, on_delete=models.CASCADE,
        related_name='elements', blank=True, null=True)
    title = models.CharField('Заголовок', max_length=200,
        blank=True, null=True)
    url = models.URLField('Ссылка на видео')
    body = models.TextField('Задание', blank=True, null=True)
    order = models.PositiveSmallIntegerField(
        default=0, blank=False, null=False, verbose_name='Вес', unique=True)


@receiver(post_save, sender=LessonElement)
def lesson_post_save(sender, instance, **kwargs):
    if instance.is_check_list:
        CheckListTypeLessonElement.objects.get_or_create(lesson_element=instance)
