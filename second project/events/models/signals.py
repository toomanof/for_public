from django.db.models.signals import pre_save
from django.dispatch import receiver
from events.models import Lesson, LessonTest


@receiver(post_save, sender=Lesson)
def lesson_post_save(sender, **kwargs):
    LessonsTest.filter(lesson=sender).update(
        pass_count=sender.pass_count)