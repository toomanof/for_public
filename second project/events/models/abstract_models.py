from django.db import models

from core.models import AbstractNotRemoveModel, AbstractModelWithTitle, VideoMixin
from .image_storages import ImageStorage


class AbstractWithDescription(models.Model):
    description = models.TextField(
        blank=True, null=True, verbose_name='Описание')
    summary = models.TextField(
        blank=True, null=True, verbose_name='Краткое описание')
    image_storage = models.ForeignKey(
        ImageStorage, on_delete=models.CASCADE, verbose_name='Хранилище изображения',
         blank=True, null=True,)

    class Meta:
        abstract = True

    @property
    def image(self):
        return self.image_storage.image

    def attach_image(self, value, user):
        image_storage, _ = ImageStorage.objects.get_or_create(
            user=user, image=value)
        self.image_storage = image_storage

    @property
    def cover(self):
        return self.image_storage.cover
