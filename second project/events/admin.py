from django.contrib import admin
from mptt.admin import DraggableMPTTAdmin

from .models import *


admin.site.register(
    CategoryEvent,
    DraggableMPTTAdmin,
    list_display=(
        'tree_actions',
        'indented_title',
        # ...more fields if you feel like it...
    ),
    list_display_links=(
        'indented_title',
    ),
)

class EditorsEventInLine(admin.TabularInline):
    model = EditorsEvent
    list_display = ('user', 'settings', 'tariffs', 'content', 'students',
                    'homeworks')


class UnitInLine(admin.TabularInline):
    model = Unit
    list_display = ('title', 'event', 'order', 'is_deleted')


@admin.register(Event)
class EventAdmin(admin.ModelAdmin):
    model = Event
    list_display = ('title', 'owner', 'order', 'is_deleted')
    list_display_links = ('title', 'owner')
    inlines = (UnitInLine, EditorsEventInLine)
    list_filter = ('title', 'owner', 'order', 'is_deleted')


@admin.register(Unit)
class UnitAdmin(admin.ModelAdmin):
    model = Unit
    list_display = ('title', 'event', 'order', 'is_deleted')
    list_filter = ('title', 'event', 'order', 'is_deleted')


class PauseTypeLessonElementInLine(admin.TabularInline):
    model = PauseTypeLessonElement
    fields = ('until_next_day', 'seconds_count')


@admin.register(LessonElement)
class IntervalLessonAdmin(admin.ModelAdmin):
    model = LessonElement
    list_display = ('title', 'course', 'block', 'lesson', '_type', 'task', 'order',
                    'success_score', 'failure_score')
    list_filter = ('_type',)
    inlines = [PauseTypeLessonElementInLine, ]

    def course(self, obj):
        return obj.lesson.unit.event if obj.lesson else ''

    def block(self, obj):
        return obj.lesson.unit if obj.lesson else ''


class IntervalTraningElementInLine(admin.TabularInline):
    model = IntervalTraningElement
    list_display = (
        'parent', 'title', 'body', 'duration',
        'url', 'order', 'is_deleted')


@admin.register(IntervalTraningTypeLessonElement)
class IntervalLessonAdmin(admin.ModelAdmin):
    model = IntervalTraningTypeLessonElement
    inlines =[IntervalTraningElementInLine, ]


class LessonElementInLine(admin.TabularInline):
    model = LessonElement
    list_display = (
        'order', 'title', 'task', 'order', 'method_stoped', 'is_deleted')


@admin.register(Lesson)
class LessonAdmin(admin.ModelAdmin):
    model = Lesson
    list_display = ('title', 'unit', 'order', 'is_deleted')
    inlines = [LessonElementInLine, ]


@admin.register(Webinar)
class WebinarAdmin(admin.ModelAdmin):
    model = Webinar
    list_display = ('title', 'event',
                    'url', 'start_date', 'is_deleted')


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    model = Message
    list_display = ('webinar', 'author', 'body',)


@admin.register(CurrentElementEventForStudent)
class CurrentElementEventForStudentAdmin(admin.ModelAdmin):
    model = CurrentElementEventForStudent
    list_display = ('event', 'block', 'lesson', 'lesson_element', 'student', 'date_create', 'date_update')

    def block(self, obj):
        return obj.lesson_element.lesson.unit if obj.lesson_element else ''

    def lesson(self, obj):
        return obj.lesson_element.lesson if obj.lesson_element else ''

    block.short_description = 'Блок'
    lesson.short_description = 'Урок'


@admin.register(ReestrExecution)
class ReestrExecutionAdmin(admin.ModelAdmin):
    model = ReestrExecution
    list_display = ('event', 'unit', 'lesson', 'lesson_element',
                    'student', 'score', 'date_completion', 'date_update',)


@admin.register(ReestrRating)
class ReestrRaitingLessonAdmin(admin.ModelAdmin):
    model = ReestrRating
    list_display = ('lesson', 'lesson_element', 'student', 'lector',
                    'score', 'date_create', 'date_update',)


@admin.register(ReestrAnswerInTest)
class ReestrExecutionAdmin(admin.ModelAdmin):
    model = ReestrAnswerInTest


@admin.register(ReestrDeliveryHomeWorkTask)
class ReestrDeliveryHomeWorkTaskAdmin(admin.ModelAdmin):
    model = ReestrDeliveryHomeWorkTask
    list_display = ('student', 'lesson_element', 'home_work_task',
                    'answer', 'date_create',)


@admin.register(ImageStorage)
class ImageStorageAdmin(admin.ModelAdmin):
    model = ImageStorage
    list_display = ('id', 'image', 'user', )


@admin.register(Flow)
class FlowAdmin(admin.ModelAdmin):
    model = Flow
    list_display = ('event', 'title', 'date_begin', 'is_enabled',)

@admin.register(EditorsEvent)
class EditorsEventAdmin(admin.ModelAdmin):
    model = EditorsEvent
    list_display = ('event', 'user', 'settings', 'tariffs',
                    'content', 'students', 'homeworks',
                    'date_create', 'date_update',)


@admin.register(InvitationEditorCourse)
class InvitationEditorCourseAdmin(admin.ModelAdmin):
    model = InvitationEditorCourse
    fields = ('link', 'event', 'email', 'settings', 'tariffs',
              'content', 'students', 'homeworks', 'date_create', 'date_update')
    readonly_fields = ('link', 'date_create', 'date_update',)


@admin.register(StudentCertificate)
class StudentCertificateAdmin(admin.ModelAdmin):
    model = StudentCertificate
    list_display = ('student', 'title', 'signature',
                    'date_expiration', 'date_create', 'date_update',)
    readonly_fields = ('date_create', 'date_update',)


@admin.register(PauseForStudent)
class PauseForStudentAdmin(admin.ModelAdmin):
    model = PauseForStudent
    list_display = ('student', 'course', 'block', 'lesson', 'lesson_element', 'date_create', 'date_end', 'date_update')
    readonly_fields = ('date_create', 'date_update',)

    def course(self, obj):
        return obj.lesson_element.lesson.unit.event

    def block(self, obj):
        return obj.lesson_element.lesson.unit

    def lesson(self, obj):
        return obj.lesson_element.lesson

    block.short_description = 'Блок'
    course.short_description = 'Курс'
    lesson.short_description = 'Урок'
