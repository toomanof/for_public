from rest_framework import serializers

from .lessons import LessonSerializer, LessonNewSerializer
from events.models import Unit


class UnitsSerializer(serializers.ModelSerializer):
    is_accessible = serializers.SerializerMethodField('get_is_accessible')
    is_relax = serializers.SerializerMethodField('get_is_relax')
    lessons = serializers.SerializerMethodField('get_lessons')

    def get_is_accessible(self, element):
        request = self.context.get("request")
        if not request:
            return False
        student = request.current_user
        return element.is_accessible_for_student(student)

    def get_is_relax(self, element):
        return element.is_relax

    def get_lessons(self, element):
        return LessonSerializer(
            element.lessons.all_active(),
            many=True).data

    class Meta:
        model = Unit
        fields = '__all__'


class BlocksSerializer(serializers.ModelSerializer):

    class Meta:
        model = Unit
        fields = '__all__'
