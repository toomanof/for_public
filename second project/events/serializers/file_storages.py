from rest_framework import serializers

from events.models import FileStorage


class FileStorageSerializer(serializers.ModelSerializer):

    class Meta:
        model = FileStorage
        fields = '__all__'
