from datetime import timedelta

from django.utils import timezone

from django.conf import settings
from rest_framework import serializers
from rest_framework.fields import empty

from core.serializers import SchoolSmallSerializer
from core.errors import timer_function
from core.services.roles import RoleUser
from billing.models import ReestrAvailableEvents
from .lessons import HomeWorkTaskSerializer
from events.models import (CategoryEvent, Event,
                           Message, Webinar,
                           ReestrExecution, ReestrAnswerInTest,
                           ReestrDeliveryHomeWorkTask, ReestrRating,
                           EditorsEvent, InvitationEditorCourse)
from events.services.units import get_event_content_tree
from payments.serializers import TarifficationSerializerSmall


class CategoryEventsSerializer(serializers.ModelSerializer):
    image = serializers.ImageField(read_only=True)
    cover = serializers.ImageField(read_only=True)

    class Meta:
        model = CategoryEvent
        fields = '__all__'


class EventRecordStudentInEventFlowSerializerMixin(serializers.ModelSerializer):
    class Meta:
        model = Event

    @timer_function
    def __init__(self, instance=None, data=empty, **kwargs):
        from billing.services import BillingService
        super().__init__(instance, data, **kwargs)

        self.record_student_in_event_flow = None
        self.records_student_in_event_flow = None

        self.request = self.context.get("request")
        self.billing_service = BillingService(self.request)
        if isinstance(instance, Event):
            self.record_student_in_event_flow = \
                self.billing_service.get_record_student_in_event_flow(instance, False)
        else:
            self.records_student_in_event_flow = \
                self.billing_service.get_records_student_in_events_flow(instance)


class EventIsAccessSerializerMixin(serializers.ModelSerializer):
    is_access = serializers.SerializerMethodField()

    class Meta:
        model = Event
        fields = ('is_access',)

    def get_is_access(self, element):
        request = self.context.get("request")
        if not request:
            return False
        return RoleUser(request).access_event(element)


class EventIsPaidSerializerMixin(serializers.ModelSerializer):
    is_paid = serializers.SerializerMethodField()

    class Meta:
        model = Event
        fields = ('is_paid',)

    def get_is_paid(self, element):
        request = self.context.get("request")
        if not request:
            return False

        return ReestrAvailableEvents.objects.filter(
            student=request.current_user,
            event=element,
            suspended=False
        ).exists()


class EventDateExpirationSerializerMixin(EventRecordStudentInEventFlowSerializerMixin):
    date_expiration = serializers.SerializerMethodField()

    class Meta:
        model = Event
        fields = ('date_expiration',)

    @timer_function
    def get_date_expiration(self, element):
        if not self.request or element.owner == self.request.current_user:
            return None
        return self.record_student_in_event_flow.date_expiration \
            if self.record_student_in_event_flow else None


class EventsDateExpirationSerializerMixin(EventRecordStudentInEventFlowSerializerMixin):
    date_expiration = serializers.SerializerMethodField()

    class Meta:
        model = Event
        fields = ('date_expiration',)

    @timer_function
    def get_date_expiration(self, element):
        if (not self.request or
                element.owner == self.request.current_user or
                not self.records_student_in_event_flow):
            return None
        record = self.records_student_in_event_flow.filter(event=element).first()
        return record.date_expiration if record else None


class EventsListSerializer(EventsDateExpirationSerializerMixin,
                           EventIsPaidSerializerMixin):
    tariff = serializers.SerializerMethodField()
    term_limits = serializers.SerializerMethodField()

    class Meta:
        model = Event
        fields = ['pk', 'title', 'summary', 'owner',
                  'date_expiration', 'is_paid', 'tariff', 'term_limits']

    def get_tariff(self, element):
        request = self.context.get("request")
        record = ReestrAvailableEvents.objects.filter(
            student=request.current_user,
            event=element,
            suspended=False
        ).only('tariff').first()
        return TarifficationSerializerSmall(record.tariff).data if record else None

    @timer_function
    def get_term_limits(self, element):
        if (not self.request or
                element.owner == self.request.current_user or
                not self.records_student_in_event_flow):
            return False
        record = self.records_student_in_event_flow.filter(event=element).first()
        return record.term_limits if record else False


class EventsBaseSerializer(EventIsAccessSerializerMixin, EventIsPaidSerializerMixin):
    image = serializers.ImageField(read_only=True)
    cover = serializers.ImageField(read_only=True)
    student_is_attach = serializers.SerializerMethodField('get_student_is_attach')
    flows = serializers.SerializerMethodField('get_flows')

    def get_student_is_attach(self, element):
        request = self.context.get("request")
        student = request.current_user
        return element.student_is_attach(student)

    def get_flows(self, element):
        from events.services import ServicesFlows
        return ServicesFlows(self.context.get("request"), element).list()

    class Meta:
        model = Event
        fields = '__all__'


class EventsSerializer(serializers.ModelSerializer):
    units = serializers.SerializerMethodField('get_units')

    def get_units(self, element):
        return get_event_content_tree(element.id)

    class Meta:
        model = Event
        fields = '__all__'


class WebinarSerializer(serializers.ModelSerializer):
    class Meta:
        model = Webinar
        fields = '__all__'
        depth = 1


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = '__all__'


class ReestrExecutionSerializer(serializers.ModelSerializer):
    class Meta:
        model = ReestrExecution
        fields = '__all__'
        depth = 1


class ReestrDeliveryHomeWorkTaskSerializer(serializers.ModelSerializer):
    type_form = serializers.SerializerMethodField('get_type_form')
    home_work_task = serializers.SerializerMethodField('get_home_work_task')

    class Meta:
        model = ReestrDeliveryHomeWorkTask
        fields = '__all__'
        depth = 1

    def get_type_form(self, element):
        return element.home_work_task.home_work.type_form

    def get_home_work_task(self, element):
        return HomeWorkTaskSerializer(
            element.home_work_task,
            context={"request": self.context.get("request")}).data


class ReestrRaitingSerializer(serializers.ModelSerializer):
    class Meta:
        model = ReestrRating
        fields = '__all__'


class ReestrAnswerInTestSerializer(serializers.ModelSerializer):
    class Meta:
        model = ReestrAnswerInTest
        fields = '__all__'


class CoursersBaseSerializer(EventDateExpirationSerializerMixin,
                             EventIsAccessSerializerMixin,
                             EventIsPaidSerializerMixin):
    term_limits = serializers.SerializerMethodField()
    tariff = serializers.SerializerMethodField()

    class Meta:
        model = Event
        fields = '__all__'

    @timer_function
    def get_term_limits(self, element):
        if not self.request or element.owner == self.request.current_user:
            return False
        if self.record_student_in_event_flow:
            return self.record_student_in_event_flow.term_limits \
                if self.record_student_in_event_flow else False
        if self.records_student_in_event_flow:
            record = self.records_student_in_event_flow.filter(event=element).first()
            return record.term_limits if record else False

    @timer_function
    def get_tariff(self, element):
        if not self.request or element.owner == self.request.current_user:
            return None
        return TarifficationSerializerSmall(self.record_student_in_event_flow.tariff).data \
            if self.record_student_in_event_flow else None


class CoursersSerializer(CoursersBaseSerializer):
    permissions = serializers.SerializerMethodField()
    school = serializers.SerializerMethodField()
    tariff_price = serializers.SerializerMethodField()
    waiting_payment = serializers.SerializerMethodField()

    class Meta(CoursersBaseSerializer.Meta):
        model = Event
        fields = '__all__'

    @timer_function
    def get_permissions(self, element):
        request = self.context.get("request")
        editor_element = EditorsEvent.objects.filter(
            event=element, user=request.current_user).first()
        return {
            'settings': editor_element.settings if editor_element else False,
            'tariffs': editor_element.tariffs if editor_element else False,
            'content': editor_element.content if editor_element else False,
            'students': editor_element.students if editor_element else False,
            'homeworks': editor_element.homeworks if editor_element else False,
        }

    @timer_function
    def get_school(self, element):
        return SchoolSmallSerializer(element.owner.school).data

    @timer_function
    def get_tariff_price(self, element):
        if not self.request or element.owner == self.request.current_user:
            return None
        return self.record_student_in_event_flow.tariff.price \
            if self.record_student_in_event_flow else None

    def get_waiting_payment(self, element):
        from billing.models import ReestUserViewCourse
        request = self.context.get("request")

        if not request:
            return False
        return element.records_user_views_courses.filter(
            student=request.current_user,
            status=ReestUserViewCourse.ST_REDIRECT_TO_PAYMENT).exists()


class CourserInfoSerializer(CoursersBaseSerializer):
    waiting_payment = serializers.SerializerMethodField()

    class Meta(CoursersBaseSerializer.Meta):
        model = Event
        fields = ('id', 'title', 'summary', 'description', 'is_access',
                  'is_paid', 'owner', 'paid', 'has_units',
                  'term_limits', 'date_expiration', 'tariff', 'waiting_payment',
                  )

    def get_waiting_payment(self, element):
        from billing.models import Order, ReestUserViewCourse
        request = self.context.get("request")
        if not request:
            return False
        ordeer_is_processing = element.orders.filter(
            student=request.current_user,
            event=element,
            status=Order.PROCESSING,
            is_deleted=False).exists()
        return ordeer_is_processing and ReestUserViewCourse.objects.filter(
            student=request.current_user,
            event=element,
            status=ReestUserViewCourse.ST_REDIRECT_TO_PAYMENT
        ).exists()


class CourserInfoWithSchoolSerializer(CourserInfoSerializer):
    school = serializers.SerializerMethodField()

    class Meta(CourserInfoSerializer.Meta):
        model = Event
        fields = ('id', 'title', 'summary', 'description', 'is_access',
                  'is_paid', 'owner', 'paid', 'published', 'has_units',
                  'term_limits', 'date_expiration', 'tariff',
                  'school',
                  )

    def get_school(self, element):
        return SchoolSmallSerializer(element.owner.school).data


class EditorsEventSerializer(serializers.ModelSerializer):
    permissions = serializers.SerializerMethodField()
    first_name = serializers.SerializerMethodField()
    last_name = serializers.SerializerMethodField()
    email = serializers.SerializerMethodField()
    phone = serializers.SerializerMethodField()
    joined = serializers.SerializerMethodField()

    class Meta:
        model = EditorsEvent
        fields = ('id', 'first_name', 'last_name', 'email', 'phone', 'joined',
                  'permissions', 'event')

    def get_permissions(self, element):
        return {
            'settings': element.settings,
            'tariffs': element.tariffs,
            'content': element.content,
            'students': element.students,
            'homeworks': element.homeworks,
        }

    def get_first_name(self, element):
        return element.user.first_name

    def get_last_name(self, element):
        return element.user.last_name

    def get_email(self, element):
        return element.user.email

    def get_phone(self, element):
        return str(element.user.profile.phone)

    def get_joined(self, element):
        return element.date_create


class InvitationEditorCourseSerializer(serializers.ModelSerializer):
    permissions = serializers.SerializerMethodField()
    link = serializers.SerializerMethodField()

    class Meta:
        model = InvitationEditorCourse
        fields = ('id', 'event', 'email', 'link', 'permissions', 'date_create')

    def get_link(self, element):
        return f'https://api/{element.event.owner.school.domain_name}.{settings.SITE_DOMAIN}/invitations/active/{element.link}'

    def get_permissions(self, element):
        return {
            'settings': element.settings,
            'tariffs': element.tariffs,
            'content': element.content,
            'students': element.students,
            'homeworks': element.homeworks,
        }


class InvitationEditorCourseSmallSerializer(serializers.ModelSerializer):
    course = serializers.SerializerMethodField()
    permissions = serializers.SerializerMethodField()
    school = serializers.SerializerMethodField()
    code = serializers.SerializerMethodField()

    class Meta:
        model = InvitationEditorCourse
        fields = ('id', 'code', 'course', 'permissions', 'school')

    def get_code(self, element):
        return element.link

    def get_course(self, element):
        return {
            'id': element.event.id,
            'title': element.event.title,
            'summary': element.event.summary,
            'published': element.event.published
        }

    def get_permissions(self, element):
        return {
            'settings': element.settings,
            'tariffs': element.tariffs,
            'content': element.content,
            'students': element.students,
            'homeworks': element.homeworks,
        }

    def get_school(self, element):
        return SchoolSmallSerializer(element.event.owner.school).data
