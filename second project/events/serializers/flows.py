from rest_framework import serializers

from events.models import Flow


class FlowSerializer(serializers.ModelSerializer):

    class Meta:
        model = Flow
        fields = '__all__'
