from rest_framework import serializers

from events.models import ImageStorage


class ImageStorageSerializer(serializers.ModelSerializer):
    image = serializers.SerializerMethodField('get_image')
    cover = serializers.SerializerMethodField('get_cover')

    class Meta:
        model = ImageStorage
        fields = '__all__'

    def get_image(self, element):
        return element.image.url

    def get_cover(self, element):
        return element.cover.url
