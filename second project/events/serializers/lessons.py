from rest_framework import serializers

from events.models import (
    Lesson, LessonElement, HomeWork, HomeWorkTask, VideoTypeLessonElement,
    QuestionTest, AnswerTest, TestTypeLessonElement, TextTypeLessonElement,
    PauseTypeLessonElement, IntervalTraningTypeLessonElement,
    IntervalTraningElement, CheckListTypeLessonElement, CheckListElement,
    LinkTypeLessonElement, CertificateTypeLessonElement)

from events.models import ReestrExecution


class AnswerTestSerializer(serializers.ModelSerializer):
    class Meta:
        model = AnswerTest
        fields = '__all__'


class CheckListElementSerializer(serializers.ModelSerializer):
    class Meta:
        model = CheckListElement
        fields = '__all__'


class CheckListTypeLessonElementSerializer(serializers.ModelSerializer):
    elements = serializers.SerializerMethodField('get_elements')
    class Meta:
        model = CheckListTypeLessonElement
        fields = '__all__'

    def get_elements(self, element):
        return CheckListElementSerializer(
            element.elements.all_active(),
            many=True, context=self.context).data


class CertificateTypeLessonElementSerializer(serializers.ModelSerializer):
    class Meta:
        model = CertificateTypeLessonElement
        fields = '__all__'


class IntervalTraningElementSerializer(serializers.ModelSerializer):
    class Meta:
        model = IntervalTraningElement
        fields = '__all__'


class IntervalTraningTypeLessonElementSerializer(serializers.ModelSerializer):
    elements = serializers.SerializerMethodField('get_elements')
    class Meta:
        model = IntervalTraningTypeLessonElement
        fields = '__all__'

    def get_elements(self, element):
        return IntervalTraningElementSerializer(
            element.elements.all_active(),
            many=True, context=self.context).data


class QuestionTestSerializer(serializers.ModelSerializer):
    answers = serializers.SerializerMethodField('get_answers')
    class Meta:
        model = QuestionTest
        fields = '__all__'

    def get_answers(self, element):
        return AnswerTestSerializer(element.answers.all_active(),
                                    many=True).data


class HomeWorkTaskSerializer(serializers.ModelSerializer):
    scores = serializers.SerializerMethodField('get_scores')
    class Meta:
        model = HomeWorkTask
        fields = ('id', 'body', 'answer', 'success_score',
                  'failure_score', 'order', 'scores')

    def get_scores(self, element):
        r = element._scores.split(',') if element._scores else []
        r = [int(x) for x in r]
        return r

    def create(self, validated_data):
        return super().update(validated_data)

    def update(self, instance, validated_data):
        return super().update(validated_data)


class HomeWorkSerializer(serializers.ModelSerializer):
    tasks = serializers.SerializerMethodField('get_tasks')
    class Meta:
        model = HomeWork
        fields = '__all__'

    def get_tasks(self, element):
        return HomeWorkTaskSerializer(element.tasks.all_active(),
                                      many=True).data


class PauseTypeLessonElementSerializer(serializers.ModelSerializer):
    class Meta:
        model = PauseTypeLessonElement
        fields = '__all__'


class TestTypeLessonElementSerializer(serializers.ModelSerializer):
    questions = serializers.SerializerMethodField('get_questions')
    class Meta:
        model = TestTypeLessonElement
        fields = '__all__'

    def get_questions(self, element):
        return QuestionTestSerializer(element.questions.all_active(),
                                      many=True).data


class TextTypeLessonElementSerializer(serializers.ModelSerializer):
    class Meta:
        model = TextTypeLessonElement
        fields = '__all__'


class VideoTypeLessonElementSerializer(serializers.ModelSerializer):
    class Meta:
        model = VideoTypeLessonElement
        fields = '__all__'


class LinkTypeLessonElementSerializer(serializers.ModelSerializer):
    class Meta:
        model = LinkTypeLessonElement
        fields = '__all__'


class LessonElementSerializer(serializers.ModelSerializer):
    certificate = serializers.SerializerMethodField('get_certificate')
    home_work = serializers.SerializerMethodField('get_home_work')
    video = serializers.SerializerMethodField('get_video')
    test = serializers.SerializerMethodField('get_test')
    text = serializers.SerializerMethodField('get_text')
    pause = serializers.SerializerMethodField('get_pause')
    interval_traning = serializers.SerializerMethodField('get_interval_traning')
    check_list = serializers.SerializerMethodField('get_check_list')
    executed = serializers.SerializerMethodField('get_executed')
    link = serializers.SerializerMethodField()

    class Meta:
        model = LessonElement
        fields = '__all__'

    def get_student(self):
        request = self.context.get("request")
        if not request:
            return False
        return request.current_user

    def get_related_object(self, element, name_related_object,
                           class_serializer_related_object):
        try:
            return class_serializer_related_object(
                getattr(element, name_related_object)).data
        except Exception as err:
            return

    def get_certificate(self, element):
        return self.get_related_object(
            element, 'certificate', CertificateTypeLessonElementSerializer)

    def get_interval_traning(self, element):
        return self.get_related_object(
            element, 'interval_traning', IntervalTraningTypeLessonElementSerializer)

    def get_pause(self, element):
        return self.get_related_object(
            element, 'pause', PauseTypeLessonElementSerializer)

    def get_video(self, element):
        return self.get_related_object(
            element, 'video', VideoTypeLessonElementSerializer)

    def get_home_work(self, element):
        return self.get_related_object(
            element, 'home_work', HomeWorkSerializer)

    def get_test(self, element):
        return self.get_related_object(
            element, 'test', TestTypeLessonElementSerializer)

    def get_text(self, element):
        return self.get_related_object(
            element, 'text', TextTypeLessonElementSerializer)

    def get_check_list(self, element):
        return self.get_related_object(
            element, 'check_list', CheckListTypeLessonElementSerializer)

    def get_link(self, element):
        return self.get_related_object(
            element, 'link', LinkTypeLessonElementSerializer)

    def get_executed(self, element):
        student = self.get_student()
        return ReestrExecution.objects.filter(
            student=student,
            lesson_element=element
        ).exists()


class LessonSerializer(serializers.ModelSerializer):
    is_accessible = serializers.SerializerMethodField('get_is_accessible')
#    rating = serializers.SerializerMethodField('get_rating')
#    test_passed = serializers.SerializerMethodField('get_test_passed')
    lesson_is_execution = serializers.SerializerMethodField('get_lesson_is_execution')
    elements = serializers.SerializerMethodField('get_elements')

    def get_student(self):
        request = self.context.get("request")
        if not request:
            return False
        return request.current_user

    def get_is_accessible(self, element):
        student = self.get_student()
        return element.is_accessible_for_student(student)\
            if student else student

    def get_elements(self, element):
        return LessonElementSerializer(
            element.elements.all_active(),
            many=True, context=self.context).data

#    def get_rating(self, element):
#        if not element.is_lesson_home_work:
#            return
#        student = self.get_student()
#        return element.rating(student) if student else 0

#    def get_test_passed(self, element):
#        if not element.is_lesson_test:
#            return
#        student = self.get_student()
#        return element.test_passed(student) if student else False

    def get_lesson_is_execution(self, element):
        student = self.get_student()
        return element.lesson_is_execution(student) if student else False

    class Meta:
        model = Lesson
        fields = '__all__'


class LessonNewSerializer(serializers.ModelSerializer):
    class Meta:
        model = Lesson
        fields = '__all__'
