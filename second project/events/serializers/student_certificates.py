from rest_framework import serializers

from events.models import StudentCertificate


class StudentCertificateSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentCertificate
        fields = ('id', 'student', 'title', 'signature',
                  'date_expiration', 'date_create',)
