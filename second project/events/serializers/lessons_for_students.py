import datetime
import logging

from rest_framework import serializers

from events.models import (
    Lesson, LessonElement, HomeWork, HomeWorkTask, VideoTypeLessonElement,
    QuestionTest, AnswerTest, TestTypeLessonElement, TextTypeLessonElement,
    PauseTypeLessonElement, IntervalTraningTypeLessonElement,
    IntervalTraningElement, CheckListTypeLessonElement, CheckListElement,
    CertificateTypeLessonElement,
    ReestrDeliveryHomeWorkTask, ReestrRating, CurrentElementEventForStudent,
    ONE_DAY, LinkTypeLessonElement, PauseForStudent
)

from events.models import ReestrExecution


logger_error = logging.getLogger('warning')


class AnswerTestForStudentSerializer(serializers.ModelSerializer):
    class Meta:
        model = AnswerTest
        fields = ('id', 'body',)


class CertificateTypeLessonElementSerializer(serializers.ModelSerializer):
    class Meta:
        model = CertificateTypeLessonElement
        fields = '__all__'


class CheckListElementForStudentSerializer(serializers.ModelSerializer):
    class Meta:
        model = CheckListElement
        fields = '__all__'


class CheckListTypeLessonElementForStudentSerializer(serializers.ModelSerializer):
    elements = serializers.SerializerMethodField('get_elements')
    class Meta:
        model = CheckListTypeLessonElement
        fields = '__all__'

    def get_elements(self, element):
        return CheckListElementForStudentSerializer(
            element.elements.all_active(),
            many=True, context=self.context).data


class IntervalTraningElementForStudentSerializer(serializers.ModelSerializer):
    class Meta:
        model = IntervalTraningElement
        fields = '__all__'


class IntervalTraningTypeLessonElementForStudentSerializer(serializers.ModelSerializer):
    elements = serializers.SerializerMethodField('get_elements')
    class Meta:
        model = IntervalTraningTypeLessonElement
        fields = '__all__'

    def get_elements(self, element):
        return IntervalTraningElementForStudentSerializer(
            element.elements.all_active(),
            many=True, context=self.context).data


class QuestionTestForStudentSerializer(serializers.ModelSerializer):
    answers = serializers.SerializerMethodField('get_answers')
    class Meta:
        model = QuestionTest
        fields = '__all__'

    def get_answers(self, element):
        return AnswerTestForStudentSerializer(
            element.answers.all_active(), many=True).data


class LinkTypeLessonElementSerializer(serializers.ModelSerializer):
    class Meta:
        model = LinkTypeLessonElement
        fields = '__all__'


class HomeWorkTaskForStudentSerializer(serializers.ModelSerializer):
    scores = serializers.SerializerMethodField('get_scores')
    delivered = serializers.SerializerMethodField('get_delivered')
    verified = serializers.SerializerMethodField('get_verified')
    pref_answer =serializers.SerializerMethodField('get_pref_answer')
    comment_lector = serializers.SerializerMethodField('get_comment_lector')
    score = serializers.SerializerMethodField('get_score')

    class Meta:
        model = HomeWorkTask
        fields = ('id', 'body', 'delivered', 'verified', 'order',
                  'scores', 'pref_answer', 'comment_lector', 'score')

    def get_student(self):
        request = self.context.get("request")
        if not request:
            return False
        return request.current_user

    def get_scores(self, element):
        r = element._scores.split(',') if element._scores else []
        r = [int(x) for x in r]
        return r

    def get_delivered(self, element):
        student = self.get_student()
        return ReestrDeliveryHomeWorkTask.objects.filter(
            home_work_task=element,
            student=student
        ).exists()

    def get_verified(self, element):
        student = self.get_student()
        return ReestrRating.objects.filter(
            home_work_task=element,
            student=student
        ).exists()

    def get_pref_answer(self, element):
        student = self.get_student()
        record = ReestrDeliveryHomeWorkTask.objects.filter(
            home_work_task=element,
            student=student
        ).first()
        return record.answer if record else ''

    def get_comment_lector(self, element):
        student = self.get_student()
        record = ReestrDeliveryHomeWorkTask.objects.filter(
            home_work_task=element,
            student=student
        ).first()
        if record:
            return record.comment_lector

    def get_score(self, element):
        student = self.get_student()
        record = ReestrDeliveryHomeWorkTask.objects.filter(
            home_work_task=element,
            student=student
        ).first()
        if record:
            return record.score


class HomeWorkForStudentSerializer(serializers.ModelSerializer):
    tasks = serializers.SerializerMethodField('get_tasks')

    class Meta:
        model = HomeWork
        fields = '__all__'

    def get_tasks(self, element):
        request = self.context.get("request")
        return HomeWorkTaskForStudentSerializer(
            element.tasks.all_active(), many=True, context={"request": request}).data


class PauseTypeLessonElementForStudentSerializer(serializers.ModelSerializer):
    begin = serializers.SerializerMethodField('get_begin')
    end = serializers.SerializerMethodField('get_end')

    class Meta:
        model = PauseTypeLessonElement
        fields = '__all__'

    def get_student(self):
        request = self.context.get("request")
        if not request:
            return False
        return request.current_user

    def record_current_element(self, element):
        return PauseForStudent.objects.filter(
            student=self.get_student(),
            lesson_element=element.lesson_element
        ).first()

    def get_begin(self, element):
        record = self.record_current_element(element)
        if record:
            return record.date_create

    def get_end(self, element):
        record = self.record_current_element(element)
        if record:
            return record.date_end


class TestTypeLessonElementForStudentSerializer(serializers.ModelSerializer):
    questions = serializers.SerializerMethodField('get_questions')

    class Meta:
        model = TestTypeLessonElement
        fields = '__all__'

    def get_questions(self, element):
        return QuestionTestForStudentSerializer(
            element.questions.all_active(), many=True).data


class TextTypeLessonElementForStudentSerializer(serializers.ModelSerializer):
    class Meta:
        model = TextTypeLessonElement
        fields = '__all__'


class VideoTypeLessonElementForStudentSerializer(serializers.ModelSerializer):
    class Meta:
        model = VideoTypeLessonElement
        fields = '__all__'


class LessonElementForStudentSerializer(serializers.ModelSerializer):
    home_work = serializers.SerializerMethodField('get_home_work')
    video = serializers.SerializerMethodField('get_video')
    certificate = serializers.SerializerMethodField('get_certificate')
    test = serializers.SerializerMethodField('get_test')
    text = serializers.SerializerMethodField('get_text')
    pause = serializers.SerializerMethodField('get_pause')
    interval_traning = serializers.SerializerMethodField('get_interval_traning')
    check_list = serializers.SerializerMethodField('get_check_list')
    executed = serializers.SerializerMethodField('get_executed')
    link = serializers.SerializerMethodField()

    class Meta:
        model = LessonElement
        fields = '__all__'

    def get_student(self):
        request = self.context.get("request")
        if not request:
            return False
        return request.current_user

    def get_related_object(self, element, name_related_object,
                           class_serializer_related_object):
        try:
            request = self.context.get("request")
            return class_serializer_related_object(
                getattr(element, name_related_object),
                context={"request": request}
            ).data
        except Exception as err:
            logger_error.error(err)

    def get_certificate(self, element):
        return self.get_related_object(
            element, 'certificate', CertificateTypeLessonElementSerializer)

    def get_interval_traning(self, element):
        return self.get_related_object(
            element, 'interval_traning',
            IntervalTraningTypeLessonElementForStudentSerializer)

    def get_pause(self, element):
        return self.get_related_object(
            element, 'pause', PauseTypeLessonElementForStudentSerializer)

    def get_video(self, element):
        return self.get_related_object(
            element, 'video', VideoTypeLessonElementForStudentSerializer)

    def get_home_work(self, element):
        return self.get_related_object(
            element, 'home_work', HomeWorkForStudentSerializer)

    def get_test(self, element):
        return self.get_related_object(
            element, 'test', TestTypeLessonElementForStudentSerializer)

    def get_text(self, element):
        return self.get_related_object(
            element, 'text', TextTypeLessonElementForStudentSerializer)

    def get_check_list(self, element):
        return self.get_related_object(
            element, 'check_list',
            CheckListTypeLessonElementForStudentSerializer)

    def get_executed(self, element):
        student = self.get_student()
        return ReestrExecution.objects.filter(
            student=student,
            lesson_element=element
        ).exists()

    def get_link(self, element):
        return self.get_related_object(
            element, 'link', LinkTypeLessonElementSerializer)


class LessonForStudentSerializer(serializers.ModelSerializer):
    is_accessible = serializers.SerializerMethodField('get_is_accessible')
#    rating = serializers.SerializerMethodField('get_rating')
#    test_passed = serializers.SerializerMethodField('get_test_passed')
    lesson_is_execution = serializers.SerializerMethodField(
        'get_lesson_is_execution')
    elements = serializers.SerializerMethodField('get_elements')

    def get_student(self):
        request = self.context.get("request")
        if not request:
            return False
        return request.current_user

    def get_is_accessible(self, element):
        student = self.get_student()
        return element.is_accessible_for_student(student)\
            if student else student

    def get_elements(self, element):
        return LessonElementForStudentSerializer(
            element.elements.all_active(),
            many=True, context=self.context).data

#    def get_rating(self, element):
#        if not element.is_lesson_home_work:
#            return
#        student = self.get_student()
#        return element.rating(student) if student else 0

#    def get_test_passed(self, element):
#        if not element.is_lesson_test:
#            return
#        student = self.get_student()
#        return element.test_passed(student) if student else False

    def get_lesson_is_execution(self, element):
        student = self.get_student()
        return element.lesson_is_execution(student) if student else False

    class Meta:
        model = Lesson
        fields = '__all__'
