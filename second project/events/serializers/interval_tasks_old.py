from rest_framework import serializers

from events.models import (
#    SchedulePlan, ScheduleElement,
    IntervalLessonElement)

# Закомментиовано до выяснения новой логики интервальных заданий
#class SchedulePlanSerializer(serializers.ModelSerializer):
#    class Meta:
#        model = SchedulePlan
#        fields = '__all__'


#class ScheduleElementSerializer(serializers.ModelSerializer):
#    finish_date = serializers.SerializerMethodField('get_finish_date')
#    lesson_id = serializers.IntegerField(write_only=True)
#    plan_id = serializers.IntegerField(write_only=True)

#    def get_finish_date(self, element):
#        return element.finish_date

#    class Meta:
#        model = ScheduleElement
#        fields = '__all__'
#        depth = 1


class IntervalLessonElementSerializer(serializers.ModelSerializer):
    #video_source = serializers.CharField(source='get_video_source_display')
    class Meta:
        model = IntervalLessonElement
        fields = '__all__'
