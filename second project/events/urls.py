from django.urls import path
from django.contrib.auth.decorators import login_required

from .views import *

app_name = 'events'

urlpatterns = [
    path('', login_required(ListEvents.as_view()),
         name="home"),
    path('create/', login_required(CreateEvent.as_view()),
         name='create'),
    path('<pk>/update', login_required(UpdateEvent.as_view()),
         name='update'),
    path('<pk>/remove', login_required(DeleteEvent.as_view()),
         name='remove'),

    path('<event>/units/create/', login_required(CreateUnit.as_view()),
         name='create-unit'),
    path('<event>/units/<pk>', login_required(DetailUnit.as_view()),
         name='detail-unit'),
    path('<event>/units/<pk>/update', login_required(UpdateUnit.as_view()),
         name='update-unit'),
    path('units/<pk>/remove', login_required(DeleteUnit.as_view()),
         name='remove-unit'),

    path('<event>/units/<unit>/lessons/create/',
         login_required(CreateLesson.as_view()), name='create-lesson'),
    path('<event>/units/<unit>/lessons/<pk>',
         login_required(DetailLesson.as_view()), name='detail-lesson'),
    path('<event>/units/<unit>/lessons/<pk>/update',
         login_required(UpdateLesson.as_view()), name='update-lesson'),
    path('lessons/<pk>/remove',
         login_required(DeleteLesson.as_view()), name='remove-lesson'),

    path('<event>/units/<unit>/webinars/create/',
         login_required(CreateWebinar.as_view()), name='create-webinar'),
    path('<event>/units/<unit>/webinars/<pk>/update',
         login_required(UpdateWebinar.as_view()), name='update-webinar'),
    path('<event>/units/<unit>/webinars/<pk>',
         login_required(DetailWebinar.as_view()), name='detail-webinar'),
    path('webinars/<pk>/remove',
         login_required(DeleteWebinar.as_view()), name='remove-webinar'),

    path('<event>/add_all_users', login_required(add_all_user_to_event),
         name="add_all_user_to_event"),

    path('<pk>', login_required(DetailEvent.as_view()),
         name="event-detail"),
    #path('subscribe', SignupView.as_view(), name="signup"),
]
