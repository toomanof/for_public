from modeltranslation.translator import register, TranslationOptions
from .models import (
    Event, Unit, Lesson, Webinar, Message,)


@register(Event)
class EventTranslationOptions(TranslationOptions):
    fields =('title', )


@register(Unit)
class UnitTranslationOptions(TranslationOptions):
    fields =('title', )


@register(Lesson)
class LessonTranslationOptions(TranslationOptions):
    fields =('title', )


@register(Webinar)
class WebinarTranslationOptions(TranslationOptions):
    fields =('title', )


@register(Message)
class MessageTranslationOptions(TranslationOptions):
    fields =('body', )


# Закомментиовано до выяснения новой логики интервальных заданий
#@register(IntervalLessonElement)
#class IntervalLessonElementTranslationOptions(TranslationOptions):
#    fields =('task', )

#@register(SchedulePlan)
#class SchedulePlanTranslationOptions(TranslationOptions):
#    fields =('title', )
