from django.db import models
from django.shortcuts import get_object_or_404

from events.models import SchedulePlan
from events.serializers import (
    SchedulePlanSerializer, ScheduleElementSerializer)
from .abstract_services import AbstractService


class ServicesSchedulePlan(AbstractService):
    '''
    Бизнес-логика расписания интервальных занятия
    '''
    serializer_class = SchedulePlanSerializer
    title_field_active_item = 'active_schedule'
    title_atribute_active_item = 'plan'

    def __init__(self, pk):
        if isinstance(pk, int):
            self.plan = SchedulePlan.objects.get(pk=pk)
        elif isinstance(pk, models.Model):
            self.plan = pk

    def add_lesson_to_element(self, element, lesson):
        element.lesson = lesson
        element.save()

    def create_element(self, lesson, start_date):
        '''
        Создание нового элемента расписания.
        Вначале проверяется не проходят уже в это время занятия
        '''
        if not self.check_start_time(start_date):
            return
            return self.plan.elements.create(
                lesson=lesson, start_date=start_date)

    def get_elements(self):
        return self.get_data(
            elements=ScheduleElementSerializer(
                self.plan.elements.order_by('start_date'), many=True).data
        )

    def check_start_time(self, start_date):
        '''
        Проверка время начала заниятия. Время занято уже или нет.
        '''
        return (self.plan.elements.count() == 0 or
                len(self.get_active_element_in_date(start_date)) == 0)

    def get_active_element_in_date(self, curent_date):
        '''
        Возвращает элементы расписания проходящие в это время
        '''
        tmp_elements = self.plan.elements.filter(start_date__lte=curent_date)
        return ScheduleElementSerializer(
            filter(lambda x: x.finish_date >= curent_date, tmp_elements),
            many=True).data

    @staticmethod
    def get_plan_by_pk(pk):
        return get_object_or_404(SchedulePlan, pk=pk)
