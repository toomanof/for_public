from core.services import SubordinateObjectService, SubordinateObjectsService

from events.serializers import QuestionTestSerializer
from events.models import QuestionTest


class ServicesTestQuestion(SubordinateObjectService):
    """
    Бизнес-логика для одного тестового вопроса
    """
    model = QuestionTest
    serializer_class = QuestionTestSerializer
    queryset = QuestionTest.objects


class ServicesTestQuestions(SubordinateObjectsService):
    """
    Бизнес-логика для одного тестового вопроса
    """
    model = QuestionTest
    serializer_class = QuestionTestSerializer
    queryset = QuestionTest.objects
