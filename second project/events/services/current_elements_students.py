import logging
import datetime
from django.db import transaction
from django.shortcuts import get_object_or_404
from django.utils import timezone

from billing.services import ServiceReestrAvailableLessonElements
from events.models import CurrentElementEventForStudent,\
    Event, ONE_DAY, Lesson, LessonElement, Unit, Event, PauseForStudent


logger_debug = logging.getLogger('debug')


class MixinPauseElementStudent:
    def get_pause_record(self, lesson_element):
        obj = PauseForStudent.objects.filter(
            lesson_element=lesson_element,
            student=self.request.current_user
        ).first()
        logger_debug.debug(f'get or create PauseForStudent {obj}')
        return obj

    def create_pause(self, lesson_element):
        """
        Метод создает запись паузы в базе
        """
        logger_debug.debug('create PauseForStudent')
        obj = self.get_pause_record(lesson_element)
        if not obj:
            pause_element = lesson_element.pause
            delta = ONE_DAY if pause_element.until_next_day \
                else pause_element.seconds_count
            date_end = timezone.now() + datetime.timedelta(seconds=delta)
            PauseForStudent.objects.create(
                lesson_element=lesson_element,
                student=self.request.current_user,
                date_end=date_end
            )

    def delete_pause(self, record_pause):
        """
        Метод удаляет запись паузы в базе
        """
        record_pause.delete()

    def is_passed(self, record_pause, lesson_element):
        return timezone.now() > record_pause.date_end


class ServiceCurrentElementStudent(MixinPauseElementStudent):

    def __init__(self, request, event):
        self.request = request
        if isinstance(event, (int, str,)):
            self.event = get_object_or_404(Event, pk=event)
        else:
            self.event = event
        self.service_available = ServiceReestrAvailableLessonElements(
            request, event)
        self.list_available_elements =\
            self.service_available.list_available_elements_lessons()

    def record(self):
        record = CurrentElementEventForStudent.objects.filter(
            student=self.request.current_user,
            event=self.event
        ).first()
        if not record:
            record = self.initialization()
        return record

    @property
    def current_element(self):
        record = self.record()
        logger_debug.debug(f'Current_element {record}')
        return record.lesson_element

    def current_element_for_student(self, student_id):
        record = CurrentElementEventForStudent.objects.filter(
            student_id=student_id,
            event=self.event
        ).first()
        if not record:
            record = self.initialization_for_student()
        return record

    def initialization(self):
        lesson_element = self.event\
            .units.all_active()\
            .first()\
            .lessons.all_active()\
            .first()\
            .elements.all_active()\
            .first()

        logger_debug.debug(f'ServiceCurrentElementStudent initialization : {self.request.current_user}')
        obj, _ = CurrentElementEventForStudent.objects.update_or_create(
            student=self.request.current_user,
            event=self.event,
            defaults={'lesson_element': lesson_element}
        )
        return obj

    def initialization_for_student(self, student_id):
        lesson_element = self.event\
            .units.all_active()\
            .first()\
            .lessons.all_active()\
            .first()\
            .elements.all_active()\
            .first()

        logger_debug.debug(f'ServiceCurrentElementStudent initialization for student : {student_id}')
        obj, _ = CurrentElementEventForStudent.objects.update_or_create(
            student_id=student_id,
            event=self.event,
            defaults={'lesson_element': lesson_element}
        )
        return obj

    def next_element_in_lesson(self, element):
        return LessonElement.objects.filter(
            lesson=element.lesson,
            order__gt=element.order,
            is_deleted=False,
            lesson__is_deleted=False,
            lesson__unit__is_deleted=False
        ).order_by('order').first()

    def next_lesson_in_unit(self, lesson):
        return Lesson.objects.filter(
            unit=lesson.unit,
            order__gt=lesson.order,
            is_deleted=False,
            unit__is_deleted=False,
            elements__isnull=False,
        ).order_by('order').first()

    def next_unit_in_event(self, unit):
        return Unit.objects.filter(
            event=unit.event,
            order__gt=unit.order,
            is_deleted=False,
            lessons__isnull=False
        ).order_by('order').first()

    def get_next_element(self, element):
        next_element = self.next_element_in_lesson(element)

        while not next_element:
            lesson = element.lesson
            next_lesson = self.next_lesson_in_unit(lesson)
            while not next_lesson:
                unit = element.lesson.unit
                next_unit = self.next_unit_in_event(unit)
                if not next_unit:
                    return False
                next_lesson = next_unit.lessons.all_active().first()

            next_element = next_lesson.elements.all_active().first()
        print('get_next_element', element, next_element)
        return next_element

    @transaction.atomic
    def step_forward_to_student(self, element, pk_student):
        """
        Перевод запись в реестр следующего доступного элемента урока
        @return:
        """
        CurrentElementEventForStudent.objects.update_or_create(
            student_id=pk_student,
            event=self.event,
            defaults={'lesson_element': self.get_next_element(element)}
        )
        return True

    @transaction.atomic
    def step_forward(self, element):
        """
        Перевод запись в реестр следующего доступного элемента урока
        @return:
        """
        assert element,\
            f'Not record available elements with element={element}'

        logger_debug.debug(f'step_forward before {element} element.is_pause =={element.is_pause}')
        if element.is_pause:
            logger_debug.debug('element.is_pause')
            pause_record = self.get_pause_record(element)
            if pause_record:
                if not self.is_passed(pause_record, element):
                    logger_debug.debug('element.is_pause not is_passed')
                    return False
            else:
                return False

        CurrentElementEventForStudent.objects.update_or_create(
            student=self.request.current_user,
            event=self.event,
            defaults={'lesson_element': self.get_next_element(element)}
        )
        return True

    @property
    def count_available_elements(self):
        return len(self.list_available_elements)

    def get_current_record_available_elements(self):
        return (self.current_element,
                self.service_available.count_available_elements)

    def get_prev_record_available_elements(self):
        return (self.get_record_available_elements_to_index(self.current_element - 1),
                self.current_element + 1,
                self.service_available.count_available_elements)

    def get_next_record_available_elements(self):
        return (self.get_record_available_elements_to_index(self.current_element + 1),
                self.current_element + 1,
                self.service_available.count_available_elements)
