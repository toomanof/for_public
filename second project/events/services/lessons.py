from django.core.exceptions import ObjectDoesNotExist, EmptyResultSet
from django.db import models
from django.shortcuts import get_object_or_404

from core.services import MixinServiceRoleLesson
from .abstract_services import AbstractService, obj_or_error, CourseContentService
from events.serializers import (LessonSerializer, LessonNewSerializer,
                                LessonElementSerializer)
from events.models import Unit, Lesson
from .lesson_elements import ServicesLessonElement, ServicesLessonElements


class ServicesLesson(AbstractService):
    """
    Бизнес-логика для одного урока
    """
    model = Lesson
    serializer_class = LessonSerializer
    queryset = Lesson.objects

    def __init__(self, request, unit, pk=None, **kwargs):
        """
        Инициализация сервиса.
        """
        super().__init__(request)

        if not unit:
            raise EmptyResultSet("parameter 'unit' not set")

        if isinstance(unit, int) or isinstance(unit, str):
            self.unit = get_object_or_404(
                Unit,
                pk=int(unit),
                is_deleted=False)
        elif isinstance(unit, models.Model):
            self.unit = unit

        if pk:
            self.object = get_object_or_404(
                self.model,
                pk=pk,
                is_deleted=False)
        else:
            self.object = self.create_object_with_order_offset(**kwargs)

        self.student = self.request.current_user

    def create_object(self, **kwargs):
        """
        Создается новый урок и добавляется к блоку с id = unit_id
        """
        return self.unit.lessons.create(**kwargs)

    @obj_or_error
    def create_element(self, **kwargs):
        """
        Создание объекта с смещением веса остальных объектов в queryset.
        В начале сохраняется вес с параметров kwargs, далее выполняется
        предопределенный метод create_object и устанавливается выбранный вес
        А также этот элемент заносится во все бесплатные тарифы текущего курса
        """
        service_lesson_elements = ServicesLessonElement(
            self.request, self.object, **kwargs)

        new_lesson_element = service_lesson_elements.object
        for tariff in self.unit.event.tariffications.all_active():
            if tariff.price == 0:
                tariff.elements.create(lesson_element=new_lesson_element)

        return service_lesson_elements.serializer_data()

    @obj_or_error
    def elements(self):
        return ServicesLessonElements(self.request, self.object).list()

    @obj_or_error
    def update_element(self, pk, **kwargs):
        service = ServicesLessonElement(self.request, self.object, pk)
        service.update(**kwargs)
        return service.serializer_data()

    @obj_or_error
    def set_order_element(self, pk_element, value):
        service = ServicesLessonElement(self.request, self.object, pk_element)
        return service._set_order(
            value, service.object, self.object.elements,
            LessonElementSerializer)

    def set_order(self, value):
        return self._set_order(
            value, self.object, self.unit.lessons, self.serializer_class)


class ServicesLessons(AbstractService):
    ''' Бизнес-логика для уроков'''
    model = Lesson
    serializer_class = LessonSerializer
    queryset = Lesson.objects

    def __init__(self, request, unit):
        super().__init__(request)
        if not unit:
            raise EmptyResultSet("parameter 'unit' not set")
        self.unit = unit

    def close_current_lesson_for_student(self):
        """
        Закрывается текущий урок студента.
        Студент выявляется по переданому токену
        """
        return self.reestr_execution.close_current_lesson_for_student()

    def get_lesson(self, pk):
        """
        Возвращает урок по id=pk
        """
        lesson = self.unit.lessons.get(pk=pk).first()
        if not lesson:
            raise ObjectDoesNotExist('Lesson not found')

    def get_lessons(self):
        """
        Возвращает урок по id=pk
        """
        return self.unit.lessons.all_active()

    def get_current_lesson_for_student(self):
        """
        Возвращает текущий урок студента.
        Студент выявляется по переданому токену
        """
        return self.reestr_execution.get_curent_lesson_for_student()


class ServicesCourseLesson(MixinServiceRoleLesson, CourseContentService):
    """ Бизнес-логика для одного блока"""

    model = Lesson
    queryset = Unit.objects.all_active()
    serializer_class = LessonNewSerializer

    def get_lesson(self):
        return LessonNewSerializer(self.object,
                                   context={"request": self.request}).data
