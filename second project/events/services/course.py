from django.core.exceptions import ObjectDoesNotExist
from core.errors import  timer_function
from events.models import Event, EditorsEvent, InvitationEditorCourse, ImageStorage
from events.serializers import (
    CoursersSerializer, CourserInfoSerializer, BlocksSerializer,
    CourserInfoWithSchoolSerializer, InvitationEditorCourseSmallSerializer)
from events.services import ServicesEvent, ServicesEvents
from .units import get_event_content_tree


class ServicesCourse(ServicesEvent):

    @timer_function
    def get_course(self):
        return CoursersSerializer(self.event,
                                  context={"request": self.request}).data

    def update_course(self, **kwargs):

        if 'published' in kwargs and kwargs['published']:
            count_content = 0
            for unit in self.event.units.all_active():
                for lesson in unit.lessons.all_active():
                    count_content += lesson.elements.all_active().count()
            if count_content == 0:
                raise Exception('Course not saved! Source contains no content.')

        if 'image_storage' in kwargs:
            try:
                kwargs['image_storage'] = ImageStorage.objects.get(
                    pk=kwargs['image_storage'])
            except ObjectDoesNotExist:
                raise ObjectDoesNotExist('Not found image storage!')

        if 'paid' in kwargs and not kwargs['paid']:
            self.event.create_free_tariff()

        if 'has_units' in kwargs and not kwargs['has_units']:
            self.removeAllUnits()

        Event.objects.filter(pk=self.event.id).update(**kwargs)
        self.event = Event.objects.filter(pk=self.event.id).first()

        return self.get_course()

    def course_info_for_student(self):
        return CourserInfoSerializer(self.event,
                                     context={"request": self.request}).data

    def get_blocks(self):
        return BlocksSerializer(self.event.units.all_active(),
                                context={"request": self.request},
                                many=True).data

    def get_content_tree(self):
        return get_event_content_tree(self.event.id)


class ServicesCourses(ServicesEvents):

    def get_managed_courses(self):
        data = [element.event for element in EditorsEvent.objects.filter(user=self.request.current_user)]
        return CourserInfoWithSchoolSerializer(data, context={"request": self.request}, many=True).data

    def get_invitations(self):
        return InvitationEditorCourseSmallSerializer(
            InvitationEditorCourse.objects.filter(email=self.request.current_user.email),
            many=True).data
