from core.services import SubordinateObjectService, SubordinateObjectsService

from events.serializers import IntervalTraningElementSerializer
from events.models import IntervalTraningElement


class ServicesIntervalTraningElement(SubordinateObjectService):
    """
    Бизнес-логика для одного элемента интервальной тренировки
    """
    model = IntervalTraningElement
    serializer_class = IntervalTraningElementSerializer
    queryset = IntervalTraningElement.objects


class ServicesIntervalTraningElements(SubordinateObjectsService):
    """
    Бизнес-логика для элементов интервальной тренировки
    """
    model = IntervalTraningElement
    serializer_class = IntervalTraningElementSerializer
    queryset = IntervalTraningElement.objects
