from operator import attrgetter

from django.core.exceptions import ObjectDoesNotExist, EmptyResultSet
from django.conf import settings
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from django.db import transaction
from django.db.models import Count
from rest_framework import status
from rest_framework.response import Response

from core.serializers import UserSerializer
from events.serializers import (
    BlocksSerializer,
    CoursersSerializer, CourserInfoSerializer, EventsBaseSerializer,
    EventsSerializer, EventsListSerializer, LessonSerializer, UnitsSerializer,
    EditorsEventSerializer, InvitationEditorCourseSerializer)
from events.models import (
    Event, Unit, Lesson, Webinar, CategoryEvent, ImageStorage,
    ReestrExecution, InvitationEditorCourse, EditorsEvent)

from .abstract_services import (
    CourseContentService, obj_or_error, permissions_as_owner)
from .lessons import ServicesLesson, ServicesLessons
from .reestr_execution import ServiceExecutionEvent
from .flows import ServicesFlow, ServicesFlows
from core.errors import ConflictError, timer_function
from core.services import (
    BaseItemsService, MixinServiceRoleEvents, MixinServiceRoleEvent,
    camel_case_to_under_case, get_fields, AccountService)
from core.serializers import DesignSerializer
from core.models import School
from payments.serializers import TarifficationSerializer


class ServicesEvent(MixinServiceRoleEvent, CourseContentService):
    """ Бизнес-логика для одного курса"""
    model = Event
    queryset = Event.objects.all_active()
    serializer_class = EventsSerializer

    title_field_active_item = 'active_event'
    title_atribute_active_item = 'event'

    def __init__(self, request, pk_event, verify_access=True):
        super().__init__(request, pk_event, verify_access)
        self.event = self.object
        self.current_user = self.request.current_user

    @property
    def count_units(self):
        return self.event.units.count()

    @permissions_as_owner
    def add_unit(self, unit_id):
        """
        Добавляет блок к курсу
        """
        unit = self.add_child_obj(self.event, 'units', unit_id, Unit)
        if isinstance(unit, str):
            return unit
        return UnitsSerializer(unit, context={"request": self.request}).data

    @permissions_as_owner
    def destroy(self):
        self.event.delete()
        self.event = None
        return {'success': True, 'message': 'Курс удален!'}

    def get_editors(self):
        return [element.user for element in self.object.editors.all()]

    def get_object(self):
        return self.object

    def get_current_unit_for_student(self):
        """
        Возвращает текущий блок
        """
        return ServiceExecutionEvent(
            self.request, self.event).get_current_element()

    def get_unit(self, pk):
        """
        Возвращает блок по id=pk
        или None при отсутсвии блока с id=pk
        """
        return get_object_or_404(Unit, pk=pk)

    def get_design_settings(self):
        """
        Возвращает настроки дизайна страницы курса по id=pk
        или None при отсутсвии его отсутсвии
        """
        try:
            design_settings = self.event.owner.school.design
        except User.school.RelatedObjectDoesNotExist:
            school, _ = School.objects.get_or_create(user=self.event.owner)
            design_settings = school.design
        if not design_settings:
            raise ObjectDoesNotExist("Designs not found!")
        return design_settings

    @permissions_as_owner
    @obj_or_error
    def get_design_settings_serializer(self):
        """
        Возвращает сериализованные настройки дизайна страницы курса
        """
        return DesignSerializer(
            self.get_design_settings(),
            context={"request": self.request}).data

    def get_units(self):
        return self.event.units.all_active()

    def get_unit_serializer(self, pk):
        unit = self.get_unit(pk)

        # проверка на флаг открытия только после завершения предыдущих блоков
        if not unit.open_only_previous_execution:
            return UnitsSerializer(
                unit, context={"request": self.request}).data
        # берем текущий блок
        current_unit = self.get_current_unit_for_student()
        if 'unit' not in current_unit:
            raise EmptyResultSet('Unit closed')
        # если курс завершён, то смотреть можно все блоки
        if current_unit['is_finished']:
            return UnitsSerializer(
                unit, context={"request": self.request}).data

        if self.current_user == self.event.owner\
           or unit.pk <= int(current_unit['unit']['id']):
            return UnitsSerializer(
                unit, context={"request": self.request}).data
        # если id выбраного блока меньше и имеет установленный закрытости то:
        raise EmptyResultSet('Unit closed')

    def get_data_event_serializer(self):
        return self.get_units_serializer()

    @timer_function
    def get_units_serializer(self):
        units = self.get_units()
        if 'authorization' in self.request.headers:
            list_units = UnitsSerializer(
                units, context={"request": self.request}, many=True).data
        else:
            list_units = UnitsSerializer(units, many=True).data
        data_event = EventsBaseSerializer(self.event,
                                          context={"request": self.request}).data
        if self.current_user:
            reestr = ServiceExecutionEvent(self.request, self.event.pk)
            data_event['is_finished'] =\
                reestr.event_is_finished_for_student()
        return {
            'units': list_units,
            self.title_field_active_item: data_event}

    def get_structure_unit_lessons(self, unit):
        return LessonSerializer(unit.lessons.all_active(),
                                many=True).data

    def get_structure_units_lessons(self):
        '''
        Возвращает массив всех активных блоков, уроков, и вебинаров
        '''
        result = []
        for unit in self.event.units.all_active():
            tmp = self.get_structure_unit_lessons(unit)
            tmp['unit'] = UnitsSerializer(
                unit, context={"request": self.request}).data
            result.append(tmp)

        return self.get_data(structure_units_lessons=result)


    def add_all_user_to_event(self):
        '''
        Добавляет всех пользователей в участники курса кроме его организатора
        '''
        user_to_event = User.objects.exclude(pk=self.event.owner.pk)
        self.event.participants.set(user_to_event)
        return 200


    def create_lesson(self, unit_id, **kwargs):
        """
        Создается новый урок и добавляется к блоку с id = unit_id
        Прописываются в реестр сведения по всем студента о
        курсе, блоке и уроку с выставление флага не прохождения
        """
        return ServicesLesson(self.request, unit_id, **kwargs).serializer_data()

    def create_unit_not_serializer(self, **kwargs):
        """
        Создается новый блок и добавляется к курсу
        Прописываются в реестр сведения по всем студента о
        курсе, блоке с выставление флага не прохождения
        """
        kwargs['order'] = Unit.objects.next_order()
        new_unit = self.create_child_obj(self.event, 'units', **kwargs)
        return new_unit

    @permissions_as_owner
    def create_unit(self, **kwargs):
        """
        Создается новый блок и добавляется к курсу
        Прописываются в реестр сведения по всем студента о
        курсе, блоке с выставление флага не прохождения
        Возвращается сериализованные данные блока
        """
        return UnitsSerializer(
            self.create_unit_not_serializer(**kwargs),
            context={"request": self.request}).data

    @permissions_as_owner
    def destroy_unit(self, pk_unit):
        if self.event.units.count() <=1:
            return Response(
                {'success': False,
                 'message': 'Единственный блок нельзя удалить!'},
                status=status.HTTP_423_LOCKED)
        else:
            Unit.objects.get(pk=pk_unit).delete()
        return {'success': True, 'message': 'Блок удален!'}

    @obj_or_error
    def close_lesson_for_student(self, unit_id, lesson_id, **kwargs):
        """
        Закрывается урок (lesson_id) студента.
        Студент выявляется по переданому токену
        """
        unit = self.get_unit(unit_id)
        serv_lessons = ServicesLesson(
            self.request, unit, lesson_id=lesson_id)
        return serv_lessons.close_lesson_for_student(**kwargs)

    @obj_or_error
    def close_current_lesson_for_student(self, unit_id):
        """
        Закрывается текущий урок студента.
        Студент выявляется по переданому токену
        """
        unit = self.get_unit(unit_id)
        serv_lessons = ServicesLessons(self.request, unit)
        return serv_lessons.close_current_lesson_for_student()

    @obj_or_error
    def get_lessons_in_unit(self, unit_id):
        """
        Возвращает текущий урок студента.
        Студент выявляется по переданому токену
        """
        serv_lessons = ServicesLessons(self.request, self.get_unit(unit_id))
        ret = serv_lessons.get_lessons()
        return LessonSerializer(ret, many=True,
                                context={"request": self.request}).data

    @obj_or_error
    def get_current_lesson_for_student(self, unit_id):
        """
        Возвращает текущий урок студента.
        Студент выявляется по переданому токену
        """
        serv_lessons = ServicesLessons(self.request, self.get_unit(unit_id))
        return serv_lessons.get_current_lesson_for_student()

    @obj_or_error
    def get_first_lesson(self):
        """
        Возвращает первый блок и первый урок в первом блоке
        """
        if not self.event:
            raise ObjectDoesNotExist('Not found event!')

        first_unit = self.event.units.all_active().first()
        first_lesson = first_unit.lessons.all_active().first() if first_unit else None
        return self.get_data(unit=first_unit, lesson=first_lesson)

    @permissions_as_owner
    @obj_or_error
    def update_lesson(self, unit_id, lesson_id, **kwargs):
        serv_lesson = ServicesLesson(
            self.request, self.get_unit(unit_id), lesson_id)
        serv_lesson.update(**kwargs)
        return serv_lesson.serializer_data()

    @obj_or_error
    def attach_student(self):
        """
        Присоединение пользователя к курсу.
        Пользователь идентифицируется по переданому токену.
        """
        if self.current_user == self.event.owner:
            raise EmptyResultSet("Lector can't be student!")
        if not self.current_user:
            raise ObjectDoesNotExist('Not found student!')
        self.event.participants.add(self.current_user)
        self.event.save()
        self.set_initial_all_lessons_for_student(self.current_user)

        return self._get_serializered_data(self.event)

    @permissions_as_owner
    @obj_or_error
    def join_image(self, file):
        self.event.attach_image(file, self.current_user)
        self.event.save()
        return self._get_serializered_data(self.event)

    @permissions_as_owner
    @obj_or_error
    def update(self, **kwargs):
        kwargs = camel_case_to_under_case(kwargs)
        if 'published' in kwargs and kwargs['published']:
            count_content = 0
            for unit in self.event.units.all_active():
                for lesson in unit.lessons.all_active():
                    count_content += lesson.elements.all_active().count()
            if count_content == 0:
                raise Exception('Course not saved! Source contains no content.')

        if 'image_storage' in kwargs:
            try:
                kwargs['image_storage'] = ImageStorage.objects.get(
                    pk=kwargs['image_storage'])
            except ObjectDoesNotExist:
                raise ObjectDoesNotExist('Not found image storage!')

        Event.objects.filter(pk=self.event.pk).update(**kwargs)

        if 'paid' in kwargs and not kwargs['paid']:
            self.event.create_free_tariff()

        if 'has_units' in kwargs and not kwargs['has_units']:
            self.removeAllUnits()

        self.event = Event.objects.get(pk=self.event.pk)
        return self._get_serializered_data(self.event)

    @obj_or_error
    def get_unit_items(self, unit_id):
        unit = self.event.units.get(pk=unit_id)
        return self.get_structure_unit_lessons(unit)

    def set_initial_all_lessons_for_student(self, student):
        pass

    @obj_or_error
    def set_order_event(self, value):
        return self._set_order(value, self.event, Event.objects,
                               CourserInfoSerializer)

    @obj_or_error
    def set_order_unit(self, unit_id, value):
        unit = self.get_unit(unit_id)
        return self._set_order(value, unit, Unit.objects, BlocksSerializer)

    @obj_or_error
    def set_order_lesson(self, unit_id, lesson_id, value):
        unit = self.get_unit(unit_id)
        return ServicesLesson(self.request, unit, lesson_id).set_order(value)

    @permissions_as_owner
    @obj_or_error
    def get_gradebook_event(self):
        pass

    @obj_or_error
    def get_flows(self):
        return ServicesFlows(self.request, self.event).list()

    def set_flag_units(self, value):
        """
        Устанавливает флаг блочности.
        При установке в False все уроки пере
        """
        self.event.has_units = value
        self.event.save()

        if not value:
            self.removeAllUnits()

        return Response(
            {'event':self.event.pk,
             'has_units': self.event.has_units}
        )

    def set_flag_paid(self, value):
        """
        Устанавливает флаг платности/бесплатности.
        При установке в False все тарифы удаляются
        """
        self.event.paid = value
        self.event.save()
        if not value:
            self.event.create_free_tariff()
        return Response(
            {'event': self.event.pk,
             'paid': self.event.paid,
             'tariff': TarifficationSerializer(
                 self.event.tariffications.all_active().first()).data}
        )

    def transfer_lessons_to_first_unit(self):
        """
        Пееревод уроков в первый блок
        """
        units = self.get_units().values_list('pk', flat=True)
        first_unit = self.event.units.all_active().first()
        Lesson.objects.filter(unit__in=list(units)).update(unit=first_unit)

    def removeAllUnits(self):
        """
        Переводит все кроме первого блока в статус удаленных
        """
        self.transfer_lessons_to_first_unit()
        units_pk = self.get_units().values('pk')[1:]
        Unit.objects.filter(pk__in=units_pk).update(is_deleted=True)

    @transaction.atomic
    def create_or_update_units(self, units):
        """
        Cоздания и изменения блоков из списка
        Метод принимает массивом новых блоков или измененных.
        Если в элементе массива указан id и он не пуст, обновляет блок,
        если id не указан или он пуст - создает новый блок
        """
        result = {'success':True}
        errors = []
        units_pk = []
        for index, unit in enumerate(units):
            obj = None
            if 'remove' in unit and 'id' in unit:
                Unit.objects.get(pk=unit['id']).delete()
            try:
                unit['event'] = self.event
                # Сохраняем отдельно вес и убираем с параметров
                unit_order, unit = self.pop_order(unit, Unit.objects)

                if 'id' not in unit:
                    obj = self.create_unit_not_serializer(**unit)
                else:
                    obj, _ = Unit.objects.update_or_create(pk=unit['id'], defaults=unit)

                # Устанавливаем вес и делаем смещение блоков
                self._set_order(unit_order, obj, self.event.units, UnitsSerializer)

            except Exception as e:
                result['success'] = False
                errors.append(
                    {'items': index,
                     'error': type(e).__name__,
                     'message': str(e)})
            if obj:
                units_pk.append(obj.pk)

        if result['success']:
            result['units'] = UnitsSerializer(
                Unit.objects.all_active().filter(pk__in=units_pk),
                context={"request": self.request},
                many=True).data
            return result, status.HTTP_200_OK
        else:
            result['errors'] = errors
            return result, status.HTTP_400_BAD_REQUEST

    @obj_or_error
    def get_link_tariffs(self):
        url = (f'{self.event.owner.school.domain_name}.{settings.SITE_DOMAIN}'
               if not self.event.owner.school.custom_domain else self.event.owner.school.domain_name)
        result = {
            'link_event': f'https://{url}/course/{self.event.pk}',
            'tariffs': []}
        for tariff in self.event.tariffications.all_active():

            result['tariffs'].append({
                'link': f'https://{url}/buy/{tariff.link}',
                'title': tariff.title
            })
        return result

    @permissions_as_owner
    def list_students(self, **params):
        """
        Список студентов оплативших курс
        """
        from billing.services import BillingService

        self.access_to_students()
        status = params.get('status', None)

        del_key = [key for key, val in params.items() if not val]
        for key in del_key:
            params.pop(key)

        filter_params = {'event': self.event} if not params else params
        sorted = filter_params.pop('sorted', None)

        queryset = BillingService.list_records_reest_students_in_flows(**filter_params)
        result = []
        list_users = set(record.student for record in queryset)

        if status in (0, 1):
            sorted = None
        if sorted:
            if sorted == 'score':
                queryset = ReestrExecution.objects\
                    .filter(student__in=list_users) \
                    .distinct() \
                    .select_related('student')\
                    .only('student')\
                    .order_by('-score')
                list_users = []
                for record in queryset:
                    if record.student not in list_users:
                        list_users.append(record.student)

            if sorted == 'progress':
                queryset = ReestrExecution.objects\
                    .filter(student__in=list_users) \
                    .values('student') \
                    .annotate(count=Count('id', distinct=True))\
                    .order_by('-count')
                list_users = [User.objects.get(id=record['student']) for record in queryset]

        return UserSerializer(list_users, many=True).data

    @permissions_as_owner
    def list_editors(self):
        return EditorsEventSerializer(self.event.editors, many=True).data

    @permissions_as_owner
    def list_invitations_editors_course(self):
        return InvitationEditorCourseSerializer(
            self.event.invitations_to_editors_courses,
            many=True).data

    @permissions_as_owner
    def revoke_invitation_editors_course(self, invite_id):
        invite = get_object_or_404(InvitationEditorCourse, id=invite_id)
        invite.delete()
        return {'id': invite_id}

    @permissions_as_owner
    def invitation_become_course_editor(self, course_id, **data):
        """
        Создает запись в базе с приглашением стать редактором курса и
        отправляет письмо с ссылкой подтверждения.
        @param course_id: ID курса
        @return: id приглашения
        """
        has_invite = InvitationEditorCourse.objects.filter(event=self.event, email=data['email']).exists()
        if has_invite:
            raise ConflictError('The invitation has already been created!')

        data['event'] = self.event
        data.update(data['permissions'])
        data.pop('permissions')

        invite = InvitationEditorCourse.objects.create(**data)
        AccountService.send_message_invite(email_invited=data['email'],
                                           course_id=self.event.title,
                                           link=invite.link)
        return InvitationEditorCourseSerializer(invite).data

    @permissions_as_owner
    def update_course_editor(self, editor_id, **data):
        """
        Обнавляет данные по разрешениям редактора курса
        @param editor_id: Id пользователя(редактора курса)
        @param data: {
                        permissions: {
                            settings: bool,
                            tariffs: bool,
                            content: bool,
                            stdents: bool,
                            homeworks: bool
                        }
                    }
        @return:
        """
        data.update(data['permissions'])
        data.pop('permissions')

        EditorsEvent.objects.filter(
            event=self.event, id=editor_id).update(**data)
        course_editor = get_object_or_404(EditorsEvent,
                                          event=self.event,
                                          id=editor_id)
        return EditorsEventSerializer(course_editor).data

    @permissions_as_owner
    def revoke_course_editor(self, editor_id):
        """
        Удаляет пользователя с редакторов курса
        @param editor_id: Id пользователя(редактора курса)
        @return: id пользователя
        """
        course_editor = get_object_or_404(EditorsEvent,
                                          event=self.event,
                                          id=editor_id)
        course_editor.delete()
        return {'id': editor_id}


class ServicesEvents(MixinServiceRoleEvents, BaseItemsService):
    """" Класс для работы с данными курсов
    """
    EVENTS_ALL, EVENTS_MY, EVENTS_PARTICIPATE = range(3)
    model = Event
    queryset = Event.objects.all_active()
    serializer_class = EventsSerializer
    field_owner_events = 'owner'  # Наименование поля которому производиться
    # фильтрация

    def __init__(self, request):
        self.request = request

    def filter_active_events(self, filter_events, user):
        """
        Фильтрация курсов по EVENTS_ALL, EVENTS_MY, EVENTS_PARTICIPATE
        """
        if user and user.is_authenticated:
            if int(filter_events) == self.EVENTS_MY:
                return ServicesEvents.active_events_owner(user)
            elif int(filter_events) == self.EVENTS_PARTICIPATE:
                return ServicesEvents.active_events_participant(user)
        return self.list()

    @staticmethod
    def create_event(request, **kwargs):
        kwargs = camel_case_to_under_case(kwargs)
        if 'image_storage' in kwargs:
            try:
                kwargs['image_storage'] = ImageStorage.objects.get(
                    pk=kwargs['image_storage'])
            except ObjectDoesNotExist:
                raise ObjectDoesNotExist('Not found image storage!')

        user = request.current_user
        if not user:
            raise ObjectDoesNotExist('Not found student!')

        kwargs['owner'] = user
        if 'category' in kwargs:
            try:
                category_obj = CategoryEvent.objects.get(id=kwargs['category'])
            except CategoryEvent.DoesNotExist as e:
                return Response(str(e), status=status.HTTP_400_BAD_REQUEST)
            else:
                kwargs['category'] = category_obj

        kwargs['order'] = Event.objects.next_order()

        fixed_date_begin = kwargs.get('fixed_date_begin', False)
        kwargs['date_begin'] = kwargs['date_begin'] if fixed_date_begin else None

        new_event = Event.objects.create(**kwargs)
        new_unit = ServicesEvent(request, new_event).create_unit_not_serializer(
            title="Первый блок курса",
            order=0
        )
        # Создается поток курса
        event_new_flow = ServicesFlow(request).create(event=new_event, date_begin=kwargs['date_begin'])

        return EventsSerializer(new_event, context={"request": request}).data

    @staticmethod
    def get_event_by_pk(event_pk):
        return get_object_or_404(Event, pk=event_pk, is_deleted=False)

    def list(self):
        return Response(
            EventsListSerializer(
                self.get_queryset(),
                many=True,
                context={"request": self.request}).data)

    @staticmethod
    @obj_or_error
    def my_events(request):
        index_sort = request.GET['sort'] if 'sort' in request.GET else 'id'
        Event._meta.get_field(index_sort)
        if 'dir' in request.GET:
            if request.GET['dir'] == 'DESC':
                index_sort = '-' + index_sort
        current_user = request.current_user
        my_events = Event.objects.all_not_delete().\
            filter(owner=current_user).order_by(index_sort)
        return Response(
            CoursersSerializer(
                my_events,
                many=True,
                context={"request": request}).data)

    @staticmethod
    def active_events_owner(owner):
        return Event.objects.filter(owner=owner)

    @staticmethod
    def active_events_participant(request, participant):
        current_user = request.current_user
        list_events = Event.objects.all_active().filter(participants=participant)

        data = EventsSerializer(
            list_events,
            many=True,
            context={"request": request}            
        ).data
        for event in data:
            reestr = ServiceExecutionEvent(request, event['id'])
            event['is_finished'] = reestr.event_is_finished_for_student()
        return data


    @staticmethod
    def all_active_units(event):
        return Unit.objects.all_active().filter(event=event)

    @staticmethod
    def all_active_lesons(unit_pk):
        return Lesson.objects.all_active().filter(unit__pk=unit_pk)

    @staticmethod
    def all_active_webinars(unit_pk):
        return Webinar.objects.all_active().filter(unit__pk=unit_pk)

    @staticmethod
    def first_event():
        _first_event = Event.objects.first()
        if _first_event:
            return ServicesEvent(_first_event.pk).get_first_lesson()
        else:
            return {}

    @staticmethod
    def has_active_events_owner(owner):
        return ServicesEvents.active_events_owner(owner).exists()

    @staticmethod
    def has_active_events_participant(owner):
        return ServicesEvents.active_events_participant(owner).exists()

    @staticmethod
    @transaction.atomic
    def activation_invitation_course_editor(request, uuid):
        invite = get_object_or_404(InvitationEditorCourse, link=uuid)

        user_is_editor = EditorsEvent.objects.filter(
            user=request.current_user, event=invite.event).exists()
        if user_is_editor:
            raise ConflictError('User already editor this course!')

        kwargs = get_fields(InvitationEditorCourse, invite)
        kwargs['user'] = request.current_user
        kwargs.pop('id')
        kwargs.pop('link')
        kwargs.pop('email')
        editor, _ = EditorsEvent.objects.get_or_create(**kwargs)
        invite.delete()
        return EditorsEventSerializer(editor).data

    @staticmethod
    @transaction.atomic
    def decline_invitation_course_editor(request, uuid):
        invite = get_object_or_404(InvitationEditorCourse, link=uuid)
        invite.delete()
        return {'success': True}
