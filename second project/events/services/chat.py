from django.db import models

from events.models import Webinar


class ServicesChat:
    def __init__(self, webinar):
        if isinstance(webinar, int):
            self.webinar = Webinar.objects.get(pk=webinar)
        elif isinstance(webinar, models.Model):
            self.webinar = webinar

    def get_members(self):
        return self.webinar.unit.event.participants.all()

    def get_messages(self):
        return self.webinar.messages.all()
