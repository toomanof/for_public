from django.db import transaction
from django.shortcuts import get_object_or_404

from core.errors import AccessError
from core.services import RoleUser
from events.models import Event, ReestrRating, ReestrDeliveryHomeWorkTask
from events.serializers import ReestrRaitingSerializer, ReestrDeliveryHomeWorkTaskSerializer


class ServiceReestrRatingLesson:
    """
    Журнал успеваемости
    Бизнес логика с реестром выставления оценок по урокам
    заданого курса и блока.
    """

    def __init__(self, request):
        self.request = request
        self.lector = request.current_user

    def list_unverified_homework(self, event):
        event = get_object_or_404(Event, pk=event)
        if not RoleUser(self.request).current_user_is_access_homeworks(event):
            raise AccessError('Access denied')

        result = ReestrDeliveryHomeWorkTask.objects.filter(
            lesson_element__lesson__unit__event=event,
            score__isnull=True,
            lector__isnull=True

        ).all()
        return ReestrDeliveryHomeWorkTaskSerializer(
            result,
            context={"request": self.request}, many=True).data

    def unverified_homework(self, event, lesson_element):
        event = get_object_or_404(Event, pk=event)
        if not RoleUser(self.request).current_user_is_access_homeworks(event):
            raise AccessError('Access denied')

        result = get_object_or_404(
            ReestrDeliveryHomeWorkTask,
            lesson_element_id=lesson_element)
        return ReestrDeliveryHomeWorkTaskSerializer(
            result,
            context={"request": self.request}).data

    @transaction.atomic
    def check_homework_tasks(self, event, homework_task_id, **data):
        event = get_object_or_404(Event, pk=event)
        if not RoleUser(self.request).current_user_is_access_homeworks(event):
            raise AccessError('Access denied')

        data['lector'] = self.request.current_user

        ReestrDeliveryHomeWorkTask.objects.filter(
            home_work_task_id=homework_task_id).update(**data)

        record_delivery_hw = get_object_or_404(
            ReestrDeliveryHomeWorkTask,
            home_work_task_id=homework_task_id)

        data.pop('comment_lector')
        ReestrRating.objects.get_or_create(
            lesson=record_delivery_hw.home_work_task.home_work.lesson_element.lesson,
            lesson_element=record_delivery_hw.home_work_task.home_work.lesson_element,
            home_work_task=record_delivery_hw.home_work_task,
            student=record_delivery_hw.student,
            defaults=data
        )

        return ReestrDeliveryHomeWorkTaskSerializer(
            record_delivery_hw,
            context={"request": self.request}).data
