import logging
from django.shortcuts import get_object_or_404

from billing.services import \
    ServiceReestrAvailableLessonElements
from core.services import MixinServiceRoleExecutionEvent
from marketing.services import MarketingService

from events.models import (
    Event, LessonElement, ReestrExecution)
from events.serializers import ReestrExecutionSerializer,\
    LessonElementForStudentSerializer
from .abstract_services import AbstractReestrService
from .current_elements_students import ServiceCurrentElementStudent, MixinPauseElementStudent
from hooks.services import HooksProcessing, H_START_STUDIES, H_END_STUDIES


logger_debug = logging.getLogger('debug')


class ServiceExecutionEvent(MixinPauseElementStudent, MixinServiceRoleExecutionEvent,
                            AbstractReestrService):
    """
    Бизнес логика с реестром проходжения урока
    заданого курса и блока.
    --------
    """
    serializer_class = ReestrExecutionSerializer
    model = ReestrExecution

    def __init__(self, request, event):
        super().__init__(request, event)
        self.event = event
        if isinstance(event, (int, str)):
            self.event = get_object_or_404(Event, pk=int(event))

        self.service_current_element = ServiceCurrentElementStudent(
            request, event)
        self.service_available_elements = ServiceReestrAvailableLessonElements(
            request, event)

    def _record_in_reestrs(self, element):
        params = {
            'event': element.lesson.unit.event,
            'unit': element.lesson.unit,
            'lesson': element.lesson,
            'lesson_element': element,
            'student': self.student,
            'score': element.success_score if element.lesson.score_system_is_used else 0
        }

        if element.lesson.unit.event.is_started(student=self.student):
            HooksProcessing.execute(self.request, H_START_STUDIES, course=element.lesson.unit.event)

        self.create_record(**params)
        return True

    def _record_in_reestrs_to_student(self, element, pk_student):
        params = {
            'event': element.lesson.unit.event,
            'unit': element.lesson.unit,
            'lesson': element.lesson,
            'lesson_element': element,
            'student_id': pk_student,
            'score': element.success_score if element.lesson.score_system_is_used else 0
        }

        if element.lesson.unit.event.is_started(student=pk_student):
            HooksProcessing.execute(self.request, H_START_STUDIES, course=element.lesson.unit.event)

        self.create_record(**params)
        return True

    def _complete_video_element_lesson(self, element):
        return self._record_in_reestrs(element)

    def _complete_text_element_lesson(self, element):
        return self._record_in_reestrs(element)

    def _complete_test_element_lesson(self, element):
        return self._record_in_reestrs(element)

    def _complete_homework_element_lesson(self, element):
        return self._record_in_reestrs(element)

    def _complete_check_list_element_lesson(self, element):
        return self._record_in_reestrs(element)

    def complete_element_lesson(self, pk_element):
        """
        API метод по завершению элемента урока
        @param pk_element: id элемента урока
        @return:
        """
        element = get_object_or_404(LessonElement, pk=pk_element)
        self.access_lesson_element(element)

        completed_element = self._record_in_reestrs(element)

        logger_debug.debug(f'Complete_element_lesson : {element}, {completed_element}')
        event_finished = self.event_is_finished_for_student()

        if event_finished:
            HooksProcessing.execute(self.request, H_END_STUDIES, course=self.event)
        else:
            self.service_current_element.step_forward(element)

        return {'complete': True,
                'event_finished': event_finished}

    def complete_element_lesson_to_student(self, pk_element, pk_student):
        """
        API метод по завершению элемента урока
        @param pk_element: id элемента урока
        @return:
        """
        element = get_object_or_404(LessonElement, pk=pk_element)
        #self.access_lesson_element(element)

        completed_element = self._record_in_reestrs_to_student(element, pk_student)
        print('completed_element', completed_element)
        logger_debug.debug(f'Complete_element_lesson : {element}, {completed_element}')
        event_finished = self.event_is_finished_for_student_with_pk(pk_student)

        if event_finished:
            print('event finished')
            HooksProcessing.execute(self.request, H_END_STUDIES, course=self.event)
        else:
            print('event not finished')
            self.service_current_element.step_forward_to_student(element, pk_student)

        return {'complete': True,
                'completed_element': pk_element,
                'current_element': self.service_current_element.current_element_for_student(pk_student).lesson_element.id,
                'event_finished': event_finished}

    def event_is_finished_for_student(self):
        count_avalaibe_elements = self.service_available_elements.count_available_elements
        count_completed_lesson_elements = ReestrExecution.objects.filter(
            student=self.student,
            event=self.event
        ).count()
        result = count_completed_lesson_elements >= count_avalaibe_elements

        if result:
            MarketingService.student_end_course(self.request, self.event.id)

        return result

    def event_is_finished_for_student_with_pk(self, pk_student):
        from billing.services import BillingService
        from payments.models import TarifficationElement

        count_avalaibe_elements = TarifficationElement.objects.filter(
            parent=BillingService.get_current_tariff_to_student_pk(
                pk_student, self.event),
            lesson_element__is_deleted=False,
            lesson_element__lesson__is_deleted=False,
            lesson_element__lesson__unit__is_deleted=False).distinct().count()

        count_completed_lesson_elements = ReestrExecution.objects.filter(
            student_id=pk_student,
            event=self.event
        ).count()
        result = count_completed_lesson_elements >= count_avalaibe_elements

        if result:
            MarketingService.student_end_course(self.request, self.event.id)

        return result

    def get_element_lesson(self, pk_element):
        lesson_element = get_object_or_404(LessonElement, pk=pk_element)
        self.access_lesson_element(lesson_element)

        if hasattr(lesson_element, 'pause') and lesson_element.pause:
            self.create_pause(lesson_element)

        return LessonElementForStudentSerializer(
            lesson_element, context={"request": self.request}).data

    def start_event_for_student(self):
        self.service_current_element.initialization()

    def get_current_element(self):
        result = {'finished': True, 'started': False}
        record = self.service_current_element.current_element

        if record:
            result = {
                'index': record.id,
                'finished': self.event_is_finished_for_student(),
                'unit': record.lesson.unit_id,
                'lesson': record.lesson_id,
                'element': record.id,
                'paused': record.is_pause,
                'started': True
            }
        return result

    def get_next_element(self):
        result = {'finished': True}
        record, index, len_list_available_elements =\
            self.service_current_element.get_next_record_available_elements()
        if record:
            result = {
                'index': index,
                'finished': index == len_list_available_elements - 1,
                'unit': record.lesson_element.lesson.unit_id,
                'lesson': record.lesson_element.lesson_id,
                'element': record.lesson_element_id,
                'paused': record.lesson_elemen.is_pause,
            }
        return result

    def completed_current_and_step_forward(self, student_id):
        current_element = self.service_current_element.current_element_for_student(student_id).lesson_element
        return self.complete_element_lesson_to_student(current_element.pk, student_id)
