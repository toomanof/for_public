from core.services import MixinServiceRoleBlock
from events.services.abstract_services import CourseContentService
from events.models import Unit
from events.serializers.units import BlocksSerializer
from events.serializers.lessons import LessonNewSerializer


def get_event_content_tree(event_id):
    units = Unit.objects.filter(
        event_id=event_id,
        is_deleted=False,
        lessons__is_deleted=False,
        lessons__elements__is_deleted=False
    ).prefetch_related('lessons', 'lessons__elements').distinct().all()

    tree = []

    for u in units:
        _unit = {
            'id': u.id,
            'order': u.order,
            'title': u.title,
            'summary': u.summary,
            'event': u.event_id,
            'lessons': []
        }

        _lessons = []
        for l in u.lessons.all_active():
            _lesson = {
                'id': l.id,
                'name': l.title,
                'order': l.order,
                'summary': l.summary,
                'unit': l.unit_id,
                'elements': []
            }

            _elements = []
            for e in l.elements.all_active():
                _element = {
                    'id': e.id,
                    'order': e.order,
                    '_type': e._type,
                    'title': e.title,
                    'lesson': e.lesson_id,
                }
                _elements.append(_element)

            _lesson['elements'] = sorted(_elements, key=lambda e: e['order'])
            _lessons.append(_lesson)

        _unit['lessons'] = sorted(_lessons, key=lambda e: e['order'])

        tree.append(_unit)

    return tree


class ServicesCourseBlock(MixinServiceRoleBlock, CourseContentService):
    """ Бизнес-логика для одного блока"""

    model = Unit
    queryset = Unit.objects.all_active()
    serializer_class = BlocksSerializer

    def __init__(self, request, pk):
        super().__init__(request, pk, False)

    def get_block(self):

        return BlocksSerializer(self.object,
                                context={"request": self.request}).data

    def update_block(self, **data):
        if not Unit.objects.filter(
                pk=self.object.id,
                event__owner=self.request.current_user).exists():
            self.access()

        Unit.objects.filter(pk=self.object.id).update(**data)
        return BlocksSerializer(Unit.objects.get(pk=self.object.id),
                                context={"request": self.request}).data

    def get_lessons(self):
        data = self.object.lessons.all_active()
        return LessonNewSerializer(data, context={"request": self.request},
                                   many=True).data
