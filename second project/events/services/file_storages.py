from events.serializers import FileStorageSerializer
from events.models import FileStorage
from .abstract_services import AbstractServicesStorage, AbstractServicesStorages

class ServicesFileStorage(AbstractServicesStorage):
    """
    Бизнес логика работы с хранилищем файлов.
    """

    model = FileStorage
    serializer_class = FileStorageSerializer
    name_field_object = 'file'


class ServicesFileStorages(AbstractServicesStorages):
    model = FileStorage
    serializer_class = FileStorageSerializer