from django.core.exceptions import ObjectDoesNotExist, EmptyResultSet
from django.db import models
from django.shortcuts import get_object_or_404
from django.utils import timezone

from events.models import Event, Flow
from events.serializers import FlowSerializer
from .abstract_services import AbstractService


class ServicesFlow(AbstractService):
    """Бизнес-логика для одного потока курса"""

    serializer_class = FlowSerializer

    def __init__(self, request, pk=None):
        super().__init__(request)
        if pk:
            try:
                pk = int(pk) if isinstance(pk, str) else pk
            except ValueError:
                raise ObjectDoesNotExist('parameter pk  must be integer!')
            if isinstance(pk, int):
                self.flow = get_object_or_404(Flow, pk=pk, is_deleted=False)
            elif isinstance(pk, models.Model):
                self.flow = pk

    def create(self, event, date_begin=None):
        if not date_begin:
            date_begin = timezone.now()

        return self.get_serializered_data(Flow.objects.create(
            event=event,
            date_begin=date_begin
        ))


class ServicesFlows(AbstractService):
    """ Бизнес-логика для потоков курса"""

    serializer_class = FlowSerializer

    def __init__(self, request, event):
        super().__init__(request)
        if not event:
            raise EmptyResultSet("parameter 'event' not set")
        self.event = event
        self.student = self.request.current_user

    def list(self):
        """
        Список действующих потоков курса
        @return: QuerySet Flows
        """
        if self.event.owner == self.student:
            return self._get_serializered_data(
                self.event.flows.all_not_delete(), many=True)
        else:
            return self._get_serializered_data(
                self.event.flows.all_active(), many=True)

    def list_enabled_items(self):
        """
        Формирует список активных потоков курса
        @return: QuerySet
        """
        return self.event.flows.filter(is_enabled=True)

    def last_enabled_item(self):
        """
        Возвращает последний активный поток курса
        @return: object model Flow
        """
        return self.list_enabled_items().order_by('-date_begin').last()

    def add_student_to_last_enabled_item(self, order):
        """
        Добавляет студента с предоставленого счета в последний активный поток
        @param order: object model Order
        @return: object model ReestrStudentInFlow
        """
        from billing.services import BillingService

        last_enabled_item = self.last_enabled_item()

        assert last_enabled_item, 'Not enabled flow!'

        record = BillingService(
            self.request).get_or_create_reestr_student_in_flows(
            event=self.event, order=order, last_enabled_flow=last_enabled_item)

        return record
