from core.services import SubordinateObjectService, SubordinateObjectsService

from events.serializers import CheckListElementSerializer
from events.models import CheckListElement


class ServicesCheckListElement(SubordinateObjectService):
    """
    Бизнес-логика для одного элемента интервальной тренировки
    """
    model = CheckListElement
    serializer_class = CheckListElementSerializer
    queryset = CheckListElement.objects


class ServicesCheckListElements(SubordinateObjectsService):
    """
    Бизнес-логика для элементов интервальной тренировки
    """
    model = CheckListElement
    serializer_class = CheckListElementSerializer
    queryset = CheckListElement.objects
