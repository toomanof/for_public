from core.services import SubordinateObjectService, SubordinateObjectsService

from events.serializers import HomeWorkTaskSerializer
from events.models import HomeWorkTask


class ServicesHomeWorkTask(SubordinateObjectService):
    """
    Бизнес-логика для одного тестового вопроса
    """
    model = HomeWorkTask
    serializer_class = HomeWorkTaskSerializer
    queryset = HomeWorkTask.objects

    def create_object(self, **kwargs):
        """
        Создается новое задание ДЗ
        """
        scores = kwargs.pop('scores', [])
        if scores:
            kwargs['_scores'] = ','.join(str(x) for x in scores)
        return super().create_object(**kwargs)

    def update(self, **kwargs):
        scores = kwargs.pop('scores', [])
        if scores:
            kwargs['_scores'] = ','.join(str(x) for x in scores)
        super().update(**kwargs)


class ServicesHomeWorkTasks(SubordinateObjectsService):
    """
    Бизнес-логика для одного тестового вопроса
    """
    model = HomeWorkTask
    serializer_class = HomeWorkTaskSerializer
    queryset = HomeWorkTask.objects
