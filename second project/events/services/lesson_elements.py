from django.core.exceptions import EmptyResultSet
from django.db import models, transaction
from django.shortcuts import get_object_or_404

from core.services import AccountService
from .abstract_services import AbstractService, obj_or_error

from events.serializers import (LessonElementSerializer)
from events.models import (
    Lesson, LessonElement, HomeWork, VideoTypeLessonElement,
    TextTypeLessonElement, TestTypeLessonElement, PauseTypeLessonElement,
    IntervalTraningTypeLessonElement, CheckListElement, LinkTypeLessonElement,
    CertificateTypeLessonElement)


class ServicesLessonElement(AbstractService):
    """
    Бизнес-логика для одного элемента урока
    """

    model = LessonElement
    serializer_class = LessonElementSerializer
    queryset = LessonElement.objects

    types_element = {
        'home_work': HomeWork,
        'video': VideoTypeLessonElement,
        'text_work': TextTypeLessonElement,
        'test': TestTypeLessonElement,
        'pause': PauseTypeLessonElement,
        'interval_traning': IntervalTraningTypeLessonElement,
        'check_list': CheckListElement,
        'link': LinkTypeLessonElement,
        'certificate': CertificateTypeLessonElement,
    }

    def __init__(self, request, lesson, pk=None, **kwargs):
        """
        Инициализация сервиса.
        """
        super().__init__(request)

        if not lesson:
            raise EmptyResultSet("parameter 'lesson' not set")
        if isinstance(lesson, int) or isinstance(lesson, str):
            self.lesson = get_object_or_404(Lesson, pk=int(lesson), is_deleted=False)
        elif isinstance(lesson, models.Model):
            self.lesson = lesson

        self.object = get_object_or_404(self.model, pk=pk, is_deleted=False) if pk\
            else self.create_object_with_order_offset(**kwargs)

        self.student = self.request.current_user

    def update_type_element(self, obj_lesson_element, kwargs):
        """
        Обновляет данные записей типов элемента урока.
        """
        for key, type_queryset in self.types_element.items():
            if key in kwargs:
                type_queryset.objects.filter(lesson_element=obj_lesson_element.pk).update(**kwargs[key])

    @transaction.atomic
    def create_object(self, **kwargs):
        """
        Создается новый элемент урока и добавляется к уроку с id = unit_id
        """
        new_element = self.lesson.elements.create(**kwargs)
        self.update_type_element(new_element, kwargs)
        return new_element

    def set_order(self, value):
        return self._set_order(value, self.object, self.lesson.elements, self.serializer_class)

    @transaction.atomic
    def update(self, **kwargs):
        copy_kwargs = kwargs.copy()
        for key, type_queryset in self.types_element.items():
            if key in kwargs:
                kwargs.pop(key)

        obj = super().update(**kwargs)
        self.update_type_element(obj, copy_kwargs)
        return obj


class ServicesLessonElements(AbstractService):
    """
    Бизнес-логика для одного элемента урока
    """

    model = LessonElement
    serializer_class = LessonElementSerializer
    queryset = LessonElement.objects

    def __init__(self, request, lesson):
        """
        Инициализация сервиса.
        """
        super().__init__(request)

        if not lesson:
            raise EmptyResultSet("parameter 'lesson' not set")
        if isinstance(lesson, int):
            self.lesson = get_object_or_404(Lesson, pk=lesson, is_deleted=False)
        elif isinstance(lesson, models.Model):
            self.lesson = lesson

    @obj_or_error
    def list(self):
        return self.get_serializered_data(
            self.lesson.elements.all_active(),
            True)
