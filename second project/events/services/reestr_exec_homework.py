from django.shortcuts import get_object_or_404

from core.services import MixinServiceRoleExecutionEvent
from events.models import (
    LessonElement, ReestrDeliveryHomeWorkTask, HomeWorkTask)

from .abstract_services import AbstractReestrService


class ServiceExecutionHomeWork(MixinServiceRoleExecutionEvent,
                               AbstractReestrService):

    def __init__(self, request, pk_element):
        """
        При инициализации необходимо указывать для какого курса и какого блока,
        будут производится события
        """
        self.element = get_object_or_404(LessonElement, pk=pk_element)
        super().__init__(request, self.element.lesson.unit.event)
        self.access_lesson_element(self.element)

    def deliver_homework(self, pk_homework_task, **kwargs):
        """
        Метод API руки получения ответов по доашнему заданию.
        Записывает в реестр переданный ответ по доашнему заданию.
        @param pk_element: id элемента урока
        @param kwargs: в ключе 'answer' принимается ответ
        @return: словари с результатами
        """
        assert 'answer' in kwargs, 'Parameter answer is required!'
        homework_task = get_object_or_404(HomeWorkTask, pk=pk_homework_task)
        record, _ = ReestrDeliveryHomeWorkTask.objects.get_or_create(
            lesson_element=self.element,
            home_work_task=homework_task,
            student=self.student
        )
        record.answer = kwargs['answer']
        record.save()
        return {
            "count_delivered_tasks": self.count_delivered_tasks(),
            "count_not_delivered_tasks": self.count_not_delivered_tasks()
        }

    def count_delivered_tasks(self):
        return ReestrDeliveryHomeWorkTask.objects.filter(
            lesson_element=self.element,
            student=self.student,
        ).count()

    def count_not_delivered_tasks(self):
        return self.element.home_work.tasks.count() - self.count_delivered_tasks()

