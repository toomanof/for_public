from django.core.exceptions import ObjectDoesNotExist, EmptyResultSet
from django.shortcuts import get_object_or_404

from core.services import AccountService
from .abstract_services import (
    AbstractService, obj_or_error, permissions_as_owner)


from events.serializers import WebinarSerializer
from events.models import Event, FileStorage, Webinar


class ServicesWebinar(AbstractService):
    """
    Бизнес-логика для одного вебинара
    """

    model = Webinar
    serializer_class = WebinarSerializer

    def __init__(self, request, event, webinar_id=None, **kwargs):
        super().__init__(request)

        if not event:
            raise EmptyResultSet("parameter 'event' not set")
        self.event = get_object_or_404(Event, pk=event, is_deleted=False)

        if webinar_id:
            self.webinar = get_object_or_404(Webinar, pk=webinar_id, is_deleted=False)
        else:
            self.webinar = self._create_webinar(**kwargs)

        self.student = self.request.current_user

    def _checked_file_storage_in_kwargs(self, kwargs):
        if 'file_storage' in kwargs:
            kwargs['file_storage'] = get_object_or_404(
                FileStorage, pk=kwargs['file_storage'])
        return kwargs


    def _create_webinar(self, **kwargs):
        if 'file_storage' in kwargs:
            kwargs.pop('file_storage')
        return self.create_child_obj(
            self.event, 'webinars', **kwargs)


    @permissions_as_owner
    @obj_or_error
    def destroy(self):
        self.webinar.delete()
        self.webinar = None
        return {'Вебинар удален!'}

    def get_object(self):
        return self.webinar

    @permissions_as_owner
    @obj_or_error
    def serializer_data(self):
        return self._get_serializered_data(self.get_object())

    @obj_or_error
    def update(self, **kwargs):
        kwargs.pop('file_storage')
        super().update(**kwargs)
        return self.serializer_data()

    @obj_or_error
    def attach_file(self, id_file_storage):
        file_storage = get_object_or_404(FileStorage, pk=id_file_storage)
        self.webinar.file_storages.add(file_storage)
        return self.serializer_data()

    @obj_or_error
    def remove_file(self, id_file_storage):
        file_storage = get_object_or_404(FileStorage, pk=id_file_storage)
        self.webinar.file_storages.remove(file_storage)
        return self.serializer_data()

class ServicesWebinars(AbstractService):
    ''' Бизнес-логика для вебинаров'''

    serializer_class = WebinarSerializer

    def __init__(self, request, event):
        super().__init__(request)
        if not event:
            raise EmptyResultSet("parameter 'event' not set")
        self.event = get_object_or_404(Event, pk=event)
        self.student = self.request.current_user

    @obj_or_error
    def get_webinars_in_event(self):
        if self.event.owner == self.student:
            return self._get_serializered_data(self.event.webinars.all_not_delete(), many=True)
        else:
            return self._get_serializered_data(self.event.webinars.all_active(), many=True)
