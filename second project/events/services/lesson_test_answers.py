from core.services import SubordinateObjectService, SubordinateObjectsService

from events.serializers import AnswerTestSerializer
from events.models import AnswerTest


class ServicesTestAnswer(SubordinateObjectService):
    """
    Бизнес-логика для одного тестового вопроса
    """
    model = AnswerTest
    serializer_class = AnswerTestSerializer
    queryset = AnswerTest.objects


class ServicesTestAnswers(SubordinateObjectsService):
    """
    Бизнес-логика для одного тестового вопроса
    """
    model = AnswerTest
    serializer_class = AnswerTestSerializer
    queryset = AnswerTest.objects
