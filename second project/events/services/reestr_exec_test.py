from django.db import transaction
from django.shortcuts import get_object_or_404
from django.http.response import Http404

from core.services import MixinServiceRoleExecutionEvent
from events.models import (
    LessonElement, ReestrAnswerInTest, QuestionTest, AnswerTest)
from events.serializers import ReestrAnswerInTestSerializer

from .abstract_services import AbstractReestrService


class ServiceExecutionTest(MixinServiceRoleExecutionEvent,
                           AbstractReestrService):
    serializer_class = ReestrAnswerInTestSerializer
    model = ReestrAnswerInTest

    def __init__(self, request, pk_element):
        """
        При инициализации необходимо указывать для какого курса и какого блока,
        будут производится события
        """
        self.element = get_object_or_404(LessonElement, pk=pk_element)
        super().__init__(request, self.element.lesson.unit.event)
        self.access_lesson_element(self.element)


    def start_test(self):
        """
        API метод начала теста.
        Обнуляет записи по результатам текущего теста.
        """
        assert self.element.is_test, \
            'This method is not designed for this type of element!'
        ReestrAnswerInTest.objects.filter(
            student=self.student,
            lesson_element=self.element
        ).delete()
        return {'message': 'Test started'}

    @transaction.atomic
    def answer_to_question(self, pk_question, **kwargs):
        """
        Метод API руки получения ответов на вопрос.
        Записывает в реестр переданные ответы на вопрос.
        В начале проверяет наличие не отвеченных вопросов и если таких нет, то
        возращает ответ с результатом прохождения теста
        Проверяет наличие элемента урока и вопроса по переданным id.

        @param pk_element: id элемента урока
        @param pk_question: id вопроса
        @param kwargs: в ключе 'answers' принимаются id отвветов
        @return: словари с результатами
        """
        assert self.element.is_test, \
            'This method is not designed for this type of element!'
        question = get_object_or_404(QuestionTest, pk=pk_question)

        assert 'answers' in kwargs, 'Parameter answers is requried!'
        if ReestrAnswerInTest.objects.filter(
                student=self.student,
                lesson_element=self.element,
                question=question
        ).exists():
            raise Exception('The answer to the question has already been accepted')

        for pk_answer in kwargs['answers']:
            try:
                answer = get_object_or_404(AnswerTest, pk=pk_answer, question=question)
            except Http404:
                raise Exception(f'The answer (id={pk_answer}) is not to this question!')

            ReestrAnswerInTest.objects.get_or_create(
                student=self.student,
                lesson_element=self.element,
                question=question,
                answer=answer,
                defaults={'correct': answer.correct}
            )
        if self.is_all_questions_answered(self.element):
            return self.test_completed(self.element)
        return {'message': 'Answer accepted'}

    def get_count_answers(self, element):
        result = {
            'count_success_answers': 0,
            'count_failure_answers': 0
        }
        for question in element.test.questions.all_active():
            count_accepted_answers_to_question = ReestrAnswerInTest.objects.filter(
                lesson_element=element,
                question=question,
                student=self.student,
                correct=True).count()
            count_must_answers_to_question = question.answers.filter(correct=True).count()
            if count_must_answers_to_question == count_accepted_answers_to_question:
                result['count_success_answers'] += 1
            else:
                result['count_failure_answers'] += 1
        return result

    def test_completed(self, element):
        count_answers = self.get_count_answers(element)
        count_answers['test_is_passed'] =\
            count_answers['count_success_answers'] >= count_answers['count_failure_answers']
        return count_answers

    def is_all_questions_answered(self, element):
        all_questions = element.test.questions.all_active().count()
        count_accepted_answers = 0
        accepted_answers = ReestrAnswerInTest.objects\
            .filter(lesson_element=element, student=self.student)\
            .values('question')
        if accepted_answers:
            tmp_accepted_answer = 0
            for accepted_answer in accepted_answers:
                if accepted_answer['question'] != tmp_accepted_answer:
                    count_accepted_answers +=1
                tmp_accepted_answer = accepted_answer['question']

        return count_accepted_answers == all_questions
