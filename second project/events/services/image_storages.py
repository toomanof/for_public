from events.serializers import ImageStorageSerializer
from events.models import ImageStorage
from .abstract_services import AbstractServicesStorage, AbstractServicesStorages

class ServicesImageStorage(AbstractServicesStorage):
    """
    Бизнес логика работы с хранилищем изображений.
    """

    model = ImageStorage
    serializer_class = ImageStorageSerializer
    name_field_object = 'image'


class ServicesImageStorages(AbstractServicesStorages):

    model = ImageStorage
    serializer_class = ImageStorageSerializer
