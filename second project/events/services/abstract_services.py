from django.core.exceptions import ObjectDoesNotExist, EmptyResultSet
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404

from core.services import obj_or_error, permissions_as_owner
from core.services import AbstractService, CourseContentService


class AbstractReestrService(AbstractService):

    def __init__(self, request, event, unit=None):
        """
        При инициализации необходимо указывать для какого курса и какого блока,
        будут производится события
        """
        super().__init__(request)
        self.event = event
        if unit:
            self.unit = unit
        self.student = self.request.current_user

    def create_record(self, **kwargs):
        if not self.get_model().objects.filter(**kwargs).exists():
            self.get_model().objects.create(**kwargs)
            return True


class AbstractServiceLessonWork(AbstractService):
    """
    Абстрактный класс бизнес логики работы с заданиями.
    """

    def __init__(self, request, lesson, work_id=None, student_id=None, check_task=None, **kwargs):
        """
        При нициализации необходимо указывать для какого курса и какого блока,
        будут производится события
        """
        super().__init__(request)
        self.event = lesson.unit.event
        self.unit = lesson.unit
        self.lesson = lesson
        if not student_id:
            self.student = self.request.current_user
        else:
            self.student = get_object_or_404(User, pk=student_id)

        self.reest_rating = self.get_class_reest()(
            self.request, self.event, self.lesson)

        if work_id:
            self.work = get_object_or_404(
                self.get_model(),
                pk=work_id,
                is_deleted=False)
        else:
            self.work = self.create_work(check_task, **kwargs)

    def add_work_to_lesson(self):
        """
        Добавляет задание к уроку и
        производится установка первоначальных данных для
        каждого студента прикрепленного к курсу
        """
        self.work.lesson = self.lesson
        self.work.save()
        self.set_initial_data(self.work)
        return self.serializer_data()

    def create_work_with_params(self, **kwargs):
        pass

    def create_work(self, check_task=None, **kwargs):
        """
        Создает задание для каждого подписчика курса
        В kwargs передаються поля модели
        """
        kwargs['lesson'] = self.lesson
        work = None
        if check_task:
           work = self.get_model().objects.filter(lesson=self.lesson,
                                   question=self.lesson.task).first()
        if not work:
            work = self.create_work_with_params(**kwargs)
        self.set_initial_data(work)
        return work

    def set_initial_data(self, work):
        """
        Производится установка первоначальных данных для
        каждого студента прикрепленного к курсу
        """
        self.reest_rating.set_initial_data(work)

    def get_class_reest(self):
        pass

    def get_filter_reest_raiting_for_update(self):
        """
        Возращает фильтр реестра оценок для его обновления
        """
        return {'student': self.student}

    def get_object(self):
        return self.get_model().objects.get(pk=self.work.id)

    def get_positive_rating(self):
        pass

    def items(self):
        pass



class AbstractServicesStorage(AbstractService):
    """
    Бизнес логика работы с хранилищем.
    """
    name_field_object = 'file'

    def __init__(self, request, storage_id=None, **kwargs):
        super().__init__(request)
        self.user = self.request.current_user
        if storage_id:
            self.object = get_object_or_404(
                self.get_model(),
                pk=storage_id)
        else:
            self.object = self.create(**kwargs)
            self.object.save()

    def __get__(self, instance, owner):
        return self.object

    def _create_object(self, **kwargs):
        paramentrs_new_obj = kwargs.copy()
        paramentrs_new_obj['user'] = self.user
        if 'file' in kwargs:
            paramentrs_new_obj.pop('file')
            paramentrs_new_obj.update({
                self.name_field_object: kwargs['file']
            })
        return self.get_model()(**paramentrs_new_obj)

    def create(self, **kwargs):
        return self._create_object(**kwargs)

    @obj_or_error
    def get_serializered_data(self):
        return super().get_serializered_data(self.object)

    @obj_or_error
    def update(self, file):
        field_object = getattr(self.object, self.name_field_object)
        field_object = file
        self.object.save()
        return super().get_serializered_data(self.object)


class AbstractServicesStorages(AbstractService):

    def __init__(self, request):
        super().__init__(request)
        self.user = self.request.current_user
    
    @obj_or_error
    def list(self):
        return super().get_serializered_data(
            self.get_model().objects.all().filter(user=self.user),
            many=True)
