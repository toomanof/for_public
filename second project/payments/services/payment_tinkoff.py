import requests
import hashlib

from .abstract_services import AbstractPaymentService


class PaymentTinkoff(AbstractPaymentService):
    """
    Класс реализации оплаты через tinkoff эквайринг
    Основной алгоритм проводки:
        Шаг 1. Создайние платежа
        Шаг 2. Отправка пользователя на страницу оплаты
        Шаг 3.

    Сумма указывается в копейках
    https://oplata.tinkoff.ru/develop/api/payments/
    """
    URL_POINT_ENTRY = 'https://securepay.tinkoff.ru/v2/'
    URL_INIT = 'init'
    URL_FINISH_AUTH = 'FinishAuthorize'
    URL_СONFIRM = 'Сonfirm'
    URL_CANCEl ='Cancel'
    URL_GET_STATE = 'GetState'
    URL_RESEND = 'Resend'
    URL_SUBMIT_3D_AUTH = 'Submit3DSAuthorization'

    STATUS_PAYMENT_NEW = 'NEW'  # В случае успешного сценария
    STATUS_PAYMENT_REJECTED ='REJECTED'  # В случае неуспешного сценария

    def __init__(self, request):
        super().__init__(request)

        # Настройки платёжной системы
        self.terminal_key = self.lector.settings_payment.secret_key
        self.password_terminal = self.lector.settings_payment.secret_key
        self.status = None
        self.error = False
        self.paymentUrl = None
        self.PaymentId = None

    def _get_token(self, **kwargs):
        token = ''
        args['Password'] = self.password_terminal
        for key, val in sorted(kwargs.items()):
            if key not in ('Receipt', 'DATA', ):
                tokem += val

        hash_object = hashlib.sha256(token.encode())
        return hash_object.hexdigest()

    def _send_request(self, url, data=None):
        response = requests.post(url, data=data)
        response_data = respone.json()
        if not {'ErrorCode', 'Success'} <= response_data.keys():
            self.error = True
            raise Exception ('json response not valid')
        self.error = response_data['ErrorCode'] != 0
        self.status = response_data['Success']
        self.paymentUrl = response_data['PaymentURL']
        self.PaymentId = response_data['PaymentId']

        return response_data

    def _request(self, url_action, data=None):
        data.update({
            'TerminalKey': self.terminal_key,
            'OrderId': OrderId,
        })
        if 'Token' not in data:
            data['Token'] = self._get_token(**data)

        self._send_request(f'{URL_POINT_ENTRY}'{url_action}, data=data)

    def create_payment(self, OrderId, **kwargs):
        """
        Метод создает платеж: продавец получает ссылку на платежную форму и
        должен перенаправить по ней покупателя.

        OrderId - string(20) - идентификатор заказа в системе продавца

        Параметры запроса описаны по адресу:
        https://oplata.tinkoff.ru/develop/api/payments/init-request/
        Amount - сумма указывается в копейках
        """
        kwargs['OrderId'] = OrderId
        return self._request(URL_INIT, data=kwargs)

    def finish_authorize(self, **kwargs):
        """
        Метод подтверждает платеж передачей реквизитов, а также списывает
        средства с карты покупателя при одностадийной оплате и блокирует
        указанную сумму при двухстадийной.

        Используется, если у площадки есть сертификация PCI DSS
        и собственная платежная форма.
        """
        return self._request(URL_FINISH_AUTH, data=kwargs)

    def get_state(self, **kwargs):
        """
        Метод возвращает текущий статус платежа.
        """
        return self._request(URL_GET_STATE, data=kwargs)

    def confirm(self, **kwargs):
        """
        Метод подтверждает платеж и списывает ранее заблокированные средства.

        Используется при двухстадийной оплате. При одностадийной оплате
        вызывается автоматически. Применим к платежу только в статусе
        AUTHORIZED и только один раз.

        Сумма подтверждения не может быть больше заблокированной.
        Если сумма подтверждения меньше заблокированной, будет выполнено
        частичное подтверждение.
        """
        return self._request(URL_СONFIRM, data=kwargs)

    def cancel(self, **kwargs):
        """
        Метод отменяет платеж.
        """
        return self._request(URL_CANCEl, data=kwargs)

    def resend(self, **kwargs):
        """
        Метод отправляет все неотправленные уведомления.
        """
        return self._request(URL_RESEND, data=kwargs)

    def submit_3d_auth(self, **kwargs):
        """
        Осуществляет проверку результатов прохождения 3-D Secure и
        при успешном результате прохождения 3-D Secure подтверждает
        инициированный платеж.

        При использовании одностадийной оплаты осуществляет списание
        денежных средств с карты покупателя. При двухстадийной оплате
        осуществляет блокировку указанной суммы на карте покупателя.
        """
        pass
