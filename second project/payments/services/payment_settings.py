from django.shortcuts import get_object_or_404

from core.services import AbstractService
from payments.models import SettingsPayment
from payments.serializers import SettingsPaymentSerializer


class ServicesLesson(AbstractService):
    """
    Бизнес-логика
    """
    model = SettingsPayment
    serializer_class = SettingsPaymentSerializer
    queryset = SettingsPayment.objects

    def __init__(self, request, pk=None, **kwargs):
        super().__init__(request)
        if pk:
            self.object = get_object_or_404(
                self.model,
                pk=pk,
                is_deleted=False)
        else:
            self.object = self.create_object_with_order_offset(**kwargs)

        self.student = self.request.current_user