"""
Service payment with PayPal

https://developer.paypal.com/docs/api/overview/
Process PayPal Payments:
    1. Define payment with details about the PayPal payment.
    2. Create payment.
       To create the payment, send the payment object to PayPal.
       This action provides a redirect URL to which to redirect the customer.
       After the customer accepts the payment, PayPal redirects the customer
       to the return or cancel URL that you specified in the payment object.
    3. Execute payment.
       Use the payer ID and payment ID that the create payment call returns.

Response create payment:
    200 : {
             'intent': 'sale',
             'payer': {'payment_method': 'paypal'},
             'redirect_urls': {'return_url': 'https://toomanof.ru',
             'cancel_url': 'https://toomanof.ru'},
             'transactions': [
                                {'amount': {'total': '12.12', 'currency': 'RUB'},
                                 'description': 'Заказ №10 от 2020-11-10 13:37:34.539205+00:00на оплату тарифа SdsdSDsd курса 1 курс',
                                 'related_resources': []
                                }
                            ],
             'id': 'PAYID-L6V4JMI89C55437LR049582A',
             'state': 'created',
             'create_time': '2020-11-11T11:02:09Z',
             'links': [
                        {'href': 'https://api.sandbox.paypal.com/v1/payments/payment/PAYID-L6V4JMI89C55437LR049582A',
                         'rel': 'self',
                         'method': 'GET'
                        },
                        {'href': 'https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=EC-1XP88674MG656090F',
                         'rel': 'approval_url',
                         'method': 'REDIRECT'
                        },
                        {'href': 'https://api.sandbox.paypal.com/v1/payments/payment/PAYID-L6V4JMI89C55437LR049582A/execute',
                         'rel': 'execute',
                         'method': 'POST'
                        }
                ]
          }
"""
import re
import paypalrestsdk
from rest_framework import status
from rest_framework.response import Response
from .abstract_services import AbstractPaymentService

from billing.models import Order


class ServicePaymentPayPal(AbstractPaymentService):

    def __init__(self, request, settings):
        super().__init__(request, settings)
        paypalrestsdk.configure({
            'mode': 'sandbox',
            'client_id': settings.account_id,
            'client_secret': settings.secret
        })

    def create_payment(self, amount: float, description: str,
                       return_url=None, installments=None, receipt=None) -> str:
        self.payment_obj_paysystem = paypalrestsdk.Payment({
            "intent": "sale",

            # Set payment method
            "payer": {
                "payment_method": "paypal"
            },

            # Set redirect URLs
            "redirect_urls": {
                "return_url": return_url,
                "cancel_url": return_url
            },

            # Set transaction object
            "transactions": [{
                "amount": {
                    "total": f'{amount:.2f}',
                    "currency": "RUB"
                },
                "description": description
            }]
        })
        if self.payment_obj_paysystem.create():
            return self.execute_payment()
        else:
            raise Exception(self.payment_obj_paysystem.error)

    def execute_payment(self):
        assert self.payment_obj_paysystem, 'Payment object missing!'

        if self.payment_obj_paysystem.execute(self.get_token_payment()):
            return self.get_object()
        else:
            return Response(self.payment_obj_paysystem.error,
                            status=status.HTTP_400_BAD_REQUEST)

    def get_id_payment_obj_paysystem(self):
        assert self.payment_obj_paysystem, 'Payment object missing!'
        if hasattr(self.payment_obj_paysystem, 'id'):
            return self.payment_obj_paysystem['id']

    def get_status_payment_obj_paysystem(self):
        assert self.payment_obj_paysystem, 'Payment object missing!'
        if hasattr(self.payment_obj_paysystem, 'state'):
            return self.payment_obj_paysystem['state']

    def get_status_payment(self):
        status_txt = self.get_status_payment_obj_paysystem()
        if status_txt.upper() in ('CREATED', 'PENDING', ):
            return Order.PROCESSING
        elif status_txt.upper() in (
                'CAPTURED', 'DENIED','EXPIRED',
                'PARTIALLY_CAPTURED', 'VOIDED'):
            return Order.DECLINED

    def get_created_at_payment_obj_paysystem(self):
        assert self.payment_obj_paysystem, 'Payment object missing!'
        if hasattr(self.payment_obj_paysystem, 'create_time'):
            return self.payment_obj_paysystem['create_time']

    def get_amount_payment_obj_paysystem(self):
        assert self.payment_obj_paysystem, 'Payment object missing!'
        if not hasattr(self.payment_obj_paysystem, 'transactions'):
            return
        amount = 0
        for transaction in self.payment_obj_paysystem.transactions:
            amount += float(transaction.amount.total)
        return amount

    def get_href_payment(self):
        assert self.payment_obj_paysystem, 'Payment object missing!'
        for link in self.payment_obj_paysystem.links:
            if link.rel == "approval_url":
                # Convert to str to avoid Google App Engine Unicode issue
                # https://github.com/paypal/rest-api-sdk-python/pull/58
                return str(link.href)

    def get_token_payment(self):
        href = self.get_href_payment()
        regex = r".*token=(.*)"
        result = re.match(regex, href)
        if result:
            return ''

    @staticmethod
    def webhook_handler(request, data):
        pass