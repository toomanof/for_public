import requests

from .abstract_services import AbstractPaymentService


class PaymentAlfabank(AbstractPaymentService):
    """
    Класс реализации оплаты через Альфа Банк эквайринг
    Основной алгоритм проводки:
        Шаг 1. Создайние платежа
        Шаг 2. Отправка пользователя на страницу оплаты
        Шаг 3.

    Сумма указывается в копейках
    https://pay.alfabank.ru/ecommerce/instructions/merchantManual/pages/index.html
    """
    URL_POINT_ENTRY = 'https://server/payment/rest/'
    URL_REG_ORDER = 'register.do'
    URL_REG_ORDER_WITH_PRE_AUTH = 'registerPreAuth.do'
    URL_GET_STATE = 'getOrderStatus.do'
    URL_COMPLETION_PAYMENT = 'deposit.do'

    def __init__(self, request):
        super().__init__(request)

        # Настройки платёжной системы
        self.terminal_key = self.lector.settings_payment.secret_key
        self.password_terminal = self.lector.settings_payment.secret_key
        self.error = False
        self.error_message=''
        self.paymentUrl = None
        self.PaymentId = None


    def _send_request(self, url, data=None):
        response = requests.post(url, data=data)
        response_data = response.json()

        self.error = response_data['errorCode'] != 0
        if self.error > 0:
            self.error_message=response_data['errorMessage']

        self.paymentUrl = response_data['formUrl']
        self.PaymentId = response_data['orderId']

        return response_data

    def _request(self, url_action, data=None):
        data.update({
            'TerminalKey': self.terminal_key,
            'OrderId': OrderId,
        })

        self._send_request(f'{URL_POINT_ENTRY}'{url_action}, data=data)

    def register_order(self,orderNumber, **kwargs):
        """
        ЗАПРОС РЕГИСТРАЦИИ ОДНОСТАДИЙНОГО ПЛАТЕЖА В ПЛАТЕЖНОМ ШЛЮЗЕ
            register.do

        ПАРАМЕТРЫ
            userName     Логин магазина.
            password     Пароль магазина.
            orderNumber  Уникальный идентификатор заказа в магазине.
            amount       Сумма заказа в копейках.
            returnUrl    Адрес, на который надо перенаправить пользователя
                         в случае успешной оплаты.

        ОТВЕТ
            В случае ошибки:
                errorCode       Код ошибки. Список возможных значений приведен
                                в таблице ниже.
                errorMessage    Описание ошибки.
     
           В случае успешной регистрации:
                orderId  Номер заказа в платежной системе.
                         Уникален в пределах системы.
                formUrl  URL платежной формы, на который
                         надо перенаправить браузер клиента.
     
            Код ошибки   Описание
                0        Обработка запроса прошла без системных ошибок.
                1        Заказ с таким номером уже зарегистрирован в системе.
                3        Неизвестная (запрещенная) валюта.
                4        Отсутствует обязательный параметр запроса.
                5        Ошибка значения параметра запроса.
                7        Системная ошибка.
        Параметры запроса описаны по адресу:
        https://pay.alfabank.ru/ecommerce/instructions/merchantManual/pages/index/rest.html#interfeys_rest

        """
        kwargs['orderNumber'] = orderNumber
        return self._request(URL_REG_ORDER, data=kwargs)

    def register_order_with_pre_auth(self, orderNumber, **kwargs):
        """
        Запрос регистрации заказа с предавторизацией (REST) В ПЛАТЕЖНОМ ШЛЮЗЕ
            registerPreAuth.do

        ПАРАМЕТРЫ
            userName     Логин магазина.
            password     Пароль магазина.
            orderNumber  Уникальный идентификатор заказа в магазине.
            amount       Сумма заказа в копейках.
            returnUrl    Адрес, на который надо перенаправить пользователя
                         в случае успешной оплаты.

        ОТВЕТ
            В случае ошибки:
                errorCode       Код ошибки. Список возможных значений приведен
                                в таблице ниже.
                errorMessage    Описание ошибки.
     
           В случае успешной регистрации:
                orderId  Номер заказа в платежной системе.
                         Уникален в пределах системы.
                formUrl  URL платежной формы, на который
                         надо перенаправить браузер клиента.
     
            Код ошибки   Описание
                0        Обработка запроса прошла без системных ошибок.
                1        Заказ с таким номером уже зарегистрирован в системе.
                3        Неизвестная (запрещенная) валюта.
                4        Отсутствует обязательный параметр запроса.
                5        Ошибка значения параметра запроса.
                7        Системная ошибка.
        Параметры запроса описаны по адресу:
        https://pay.alfabank.ru/ecommerce/instructions/merchantManual/pages/index/rest.html#interfeys_rest

        """

        return self._request(URL_REG_ORDER_WITH_PRE_AUTH, data=kwargs)

    def get_state(self, order_id, **kwargs):
        """
        ЗАПРОС СОСТОЯНИЯ ЗАКАЗА
            getOrderStatus.do

        ПАРАМЕТРЫ
            userName  Логин магазина.
            password  Пароль магазина.
            orderId   Номер заказа в платежной системе.
                      Уникален в пределах системы.
     
        ОТВЕТ
            ErrorCode   Код ошибки. Список возможных значений приведен
                        в таблице ниже.
            OrderStatus По значению этого параметра определяется состояние
                        заказа в платежной системе.
                        Список возможных значений приведен в таблице ниже.
                        Отсутствует, если заказ не был найден.

       Код ошибки      Описание
           0           Обработка запроса прошла без системных ошибок.
           2           Заказ отклонен по причине ошибки в реквизитах платежа.
           5           Доступ запрещён;
                       Пользователь должен сменить свой пароль;
                       Номер заказа не указан.
           6           Неизвестный номер заказа.
           7           Системная ошибка.
     
       Статус заказа   Описание
           0           Заказ зарегистрирован, но не оплачен.
           1           Предавторизованная сумма захолдирована
                       (для двухстадийных платежей).
           2           Проведена полная авторизация суммы заказа.
           3           Авторизация отменена.
           4           По транзакции была проведена операция возврата.
           5           Инициирована авторизация через ACS банка-эмитента.
           6           Авторизация отклонена.
        """
        data['orderId'] = order_id
        return self._request(URL_GET_STATE, data=kwargs)

    def completion_of_order_payment(self, order_id, **kwargs):
        """
        ЗАПРОС СОСТОЯНИЯ ЗАКАЗА
            getOrderStatus.do

        ПАРАМЕТРЫ
            userName  Логин магазина.
            password  Пароль магазина.
            orderId   Номер заказа в платежной системе.
                      Уникален в пределах системы.
            amount    Сумма платежа в копейках (или центах)
                      Если указать в параметре amount ноль, завершение
                      произойдёт на всю предавторизованную сумму.
     
        ОТВЕТ
            errorCode     Код ошибки. Список возможных значений приведен
                          в таблице ниже.
            errorMessage  Описание ошибки на языке.

       Код ошибки      Описание
           0           Обработка запроса прошла без системных ошибок.
           2           Заказ отклонен по причине ошибки в реквизитах платежа.
           5           Доступ запрещён;
                       Пользователь должен сменить свой пароль;
                       Неверная сумма;
                       Сумма депозита должна быть равной нулю или не менее
                       одного рубля.
           6           Незарегистрированный OrderId
           7           Системная ошибка.
     

        """
        data['orderId'] = order_id
        return self._request(URL_COMPLETION_PAYMENT, data=kwargs)
