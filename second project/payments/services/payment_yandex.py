"""
Service payment with Yandex Kassa
https://kassa.yandex.ru/developers

"""
import logging
import uuid
from rest_framework.response import Response
from yookassa import Configuration, Payment
from yookassa.domain.notification import WebhookNotification

from billing.models import Order, ReestrBillingPayment
from events.models import CurrentElementEventForStudent
from hooks.services import HooksProcessing, H_SUCCESS_PAYMENT
from .abstract_services import AbstractPaymentService
from payments.models import ReestrPayment


logger = logging.getLogger('info')
logger_error = logging.getLogger('warning')


class ServicePaymentYandex(AbstractPaymentService):
    """
    Класс реализации оплаты через yandex кассу
    Основной алгоритм проводки:
        Шаг 1. Создайние платежа
        Шаг 2. Отправка пользователя на страницу оплаты
        Шаг 3. Получение подтверждения успешного выполнения платежа
    """

    def __init__(self, request, settings=None):
        super().__init__(request, settings)

        if settings:
            # Настройки платёжной системы
            Configuration.account_id = settings.account_id
            Configuration.secret_key = settings.secret

    def create_payment(self, amount, description,
                       return_url=None, installments=None, receipt=None):
        """
        Создание платежа
        """
        data = {
            'amount': {
                'value': f'{amount:.2f}',
                'currency': 'RUB'
            },
            'confirmation': {
                'type': 'redirect',
                'return_url': return_url
            },
            'receipt': receipt,
            'capture': True,
            'description': description
        }
        if installments:
            data['payment_method_data'] = {'type': 'installments'}
        logger.info(f'yandex payment {data}')
        self.payment_obj_paysystem = Payment.create(data, uuid.uuid4())
        return self.get_object()

    def get_id_payment_obj_paysystem(self):
        """
        Возвращает id объекта платежа созданого в платёжной системе
        @return: id объекта платежа созданого в платёжной системе
        """
        assert self.payment_obj_paysystem, 'Payment object missing!'
        assert self.payment_obj_paysystem.id, "It's not yandex payment object!"
        return self.payment_obj_paysystem.id

    def get_status_payment_obj_paysystem(self):
        """
        Возвращает статус объекта платежа созданого в платёжной системе
        @return: статус объекта платежа созданого в платёжной системе
        """
        assert self.payment_obj_paysystem, 'Payment object missing!'
        assert self.payment_obj_paysystem.status, "It's not yandex payment object!"
        return self.payment_obj_paysystem.status

    def get_href_payment(self):
        """
        Возвращает url страницы подтверждения оплаты возвращенную в
        объекте платежа созданого в платёжной системе
        @return: url страницы подтверждения оплаты
        """
        assert self.payment_obj_paysystem, 'Payment object missing!'
        assert self.payment_obj_paysystem.confirmation.confirmation_url, "It's not yandex payment object!"
        return self.payment_obj_paysystem.confirmation.confirmation_url

    def get_created_at_payment_obj_paysystem(self):
        """
        Возвращает дату создания объекта платежа созданого в платёжной системе
        @return: дату создания объекта платежа созданого в платёжной системе
        """
        assert self.payment_obj_paysystem, 'Payment object missing!'
        assert self.payment_obj_paysystem.created_at, "It's not yandex payment object!"
        return self.payment_obj_paysystem.created_at

    def get_amount_payment_obj_paysystem(self):
        """
        Возвращает сумму создания объекта платежа созданого в платёжной системе
        @return: сумму создания объекта платежа созданого в платёжной системе
        """
        assert self.payment_obj_paysystem, 'Payment object missing!'
        assert self.payment_obj_paysystem.amount, "It's not yandex payment object!"
        return self.payment_obj_paysystem.amount.value

    def get_status_payment(self):
        status_txt = self.get_status_payment_obj_paysystem()
        if status_txt.lower() in ('pending', 'waiting_for_capture'):
            return Order.PROCESSING
        elif status_txt.lower() == 'succeeded':
            return Order.COMPLETED
        elif status_txt.lower() == 'canceled':
            return Order.DECLINED

    @staticmethod
    def webhook_handler(request, data):
        from marketing.services import MarketingService
        logger.info(f'webhook_handler: {data}')
        if not data:
            return Response(status=200)
        try:
            notification_object = WebhookNotification(data)
        except Exception as e:
            raise Exception(f'This not payload yookassa! {e}')
        try:
            record_reestr_payment = ReestrPayment.objects.filter(
                payment_id=notification_object.object.id).first()
            logger.info(f'ServicePaymentYandex webhook_handler with status {notification_object.object.status}')
            if record_reestr_payment is not None:
                _status = notification_object.object.status
                record_reestr_payment.status_txt = _status
                if _status == 'pending':
                    record_reestr_payment.status = ReestrPayment.PROCESSING
                if _status == 'waiting_for_capture':
                    record_reestr_payment.status = ReestrPayment.PROCESSING
                elif _status == 'succeeded':
                    record_reestr_payment.status = ReestrPayment.COMPLETED
                    logger.info('webhook_handler: succeeded')
                    HooksProcessing.execute(request, H_SUCCESS_PAYMENT)
                elif _status == 'canceled':
                    record_reestr_payment.status = ReestrPayment.DECLINED
                    MarketingService.student_interested_event(
                        record_reestr_payment.student, record_reestr_payment.order.event.id)

                record_reestr_payment.save()

                logger.info(f'ServicePaymentYandex webhook_handler with status {_status}')

                record_reestr_payment.order.status = record_reestr_payment.status
                record_reestr_payment.order.save()

                ReestrBillingPayment.objects.filter(
                    student=record_reestr_payment.student,
                    order=record_reestr_payment.order).update(status=record_reestr_payment.status)

                if _status == 'succeeded':
                    AbstractPaymentService.create_record_reestr_available_events(
                        request, record_reestr_payment.order)

                    AbstractPaymentService.add_student_to_last_enabled_item(request, record_reestr_payment.order)

                    lesson_element = record_reestr_payment.order.event.units.all_active().first().lessons.all_active().first().elements.all_active().first()
                    CurrentElementEventForStudent.objects.update_or_create(
                        student=record_reestr_payment.student,
                        event=record_reestr_payment.order.event,
                        defaults={'lesson_element': lesson_element}
                    )

                    MarketingService.update_status_in_reest_student_in_school_to_have_course(
                        record_reestr_payment.student, record_reestr_payment.lector.school)

                    MarketingService.student_paid_course(
                        record_reestr_payment.student, record_reestr_payment.order.event)
                    logger.info(f'ServicePaymentYandex webhook_handler for order {record_reestr_payment.order}'
                                f'{record_reestr_payment.student} completed')
            else:
                logger.info(f'Not payment id {notification_object.object.id}')

        except Exception as e:
            logger_error.error(f'{request.path_info} {e}')
            return Response(status=200)

        return Response(status=200)


class ServicePartnerYandex:
    """
    Класса объекта партнерской программы Yandex-Kassa
    """

    def url_redirect_to_oauth_server(self, client_id, device_id, state):
        """
        Чтобы пользователь мог выдать разрешение магазину, необходимо
        перенаправить его на OAuth-сервер

        Пример адреса для перенаправления пользователя

            https://oauth.yandex.ru/authorize?response_type=code&
            client_id=<Идентификатор приложения>&device_id=123456&
            state=test-user

        В URL перенаправления укажите тип подтверждения code и передайте
        идентификатор вашего приложения (client_id), уникальный идентификатор
        магазина, для которого вы запрашиваете доступ (device_id) и
        идентификатор пользователя или сессии, в рамках которой выдаются
        права (state).
        Параметры url:
            response_type - Способ получения токена.
                            Для Яндекс.Кассы нужно указать значение code
                            (код подтверждения).
                            Обязательный параметр.
            client_id     - Идентификатор вашего приложения.
                            Обязательный параметр.
            device_id     - Уникальный идентификатор устройства, для которого
                            запрашивается токен.
                            Чтобы обеспечить уникальность, достаточно один раз
                            сгенерировать UUID и использовать его при каждом
                            запросе нового токена с данного устройства.
                            Также в качестве device_id вы можете использовать
                            уникальный идентификатор аккаунта пользователя в
                            вашем приложении. Идентификатор должен быть не
                            короче 6 символов и не длиннее 50. Допускается
                            использовать только печатаемые ASCII-символы
                            (с кодами от 32 до 126).
            state         - Строка состояния, которую Яндекс.OAuth возвращает
                            без изменения. Можно использовать для идентификации
                            пользователя, для которого запрашивается токен.
                            Максимальная допустимая длина строки — 1024 символа.
                            Рекомендуемый параметр для интеграции с Яндекс.Кассой

        """
        return (f'https://oauth.yandex.ru/authorize?'
               f'response_type=code&'
               f'client_id={client_id}'
               f'device_id={device_id}'
               f'state={state}')
