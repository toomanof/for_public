import decimal
from django.core.exceptions import ObjectDoesNotExist
from django.db import models, transaction
from django.shortcuts import get_object_or_404
from django.utils import timezone

from core.errors import PromoCodeExpiredError
from core.services.abstract_services import (
    AbstractService, MixinServiceRolePromocode)

from payments.models import PromoCode, Tariffication
from payments.serializers import PromoCodeSerializer, TarifficationSerializer


class PromocodeService(MixinServiceRolePromocode, AbstractService):
    model = PromoCode
    serializer_class = PromoCodeSerializer
    queryset = PromoCode.objects.all_active()

    def __init__(self, request, pk, tariff_pk):
        super().__init__(request)
        if pk:
            try:
                pk = int(pk) if isinstance(pk, str) else pk
            except ValueError:
                raise ObjectDoesNotExist('parameter pk  must be integer!')
            if isinstance(pk, int):
                self.object = get_object_or_404(
                    PromoCode, pk=pk, is_deleted=False, used=False)
            elif isinstance(pk, models.Model):
                self.object = pk

            if not self.is_active():
                raise PromoCodeExpiredError()
            self.tariff = get_object_or_404(Tariffication,
                                            pk=tariff_pk, is_deleted=False)

    @transaction.atomic
    def activated(self):
        if not self.is_active():
            raise PromoCodeExpiredError()

        self._activation_record()
        return {
            'tariff': self.tariff.pk,
            'discont': self.object.discount,
            'price': self.discounted_price()
        }

    def is_active(self):
        return self.object.expires >= timezone.now()

    def _activation_record(self):
        self.object.records_reestr_used_promocode.create(
            tariff=self.tariff, student=self.request.current_user)

    def discounted_price(self):
        return round(self.tariff.price - self.tariff.price * decimal.Decimal(self.object.discount / 100))
