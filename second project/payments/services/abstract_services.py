from django.conf import settings


class AbstractPaymentService:
    """
    Абстрактный сервис плетежной системы
    """
    payment_obj_paysystem = None  # Объект платежа возвращаемый платёжными системами

    def __init__(self, request, settings):
        """
        Initialization of the lecturer's payment settings
        """
        self.request = request
        self.settings = settings

    def create_payment(self, amount: float, description: str) -> str:
        """
        Создание платежа
        """
        pass

    def notification(self):
        """
        Обработка уведомлений
        """
        pass

    @property
    def cancel_url(self):
        """
        Returns the url of the page of unsuccessful payment
        @return: str
        """
        pass

    @property
    def successful_url(self):
        """
        Returns the url of the page of successful payment
        @return: str
        """
        return (f'{settings.DEFAULT_PROTOCOL}://'
                f'{settings.DEFAULT_SITE}'
                f'{self.get_sucurl()}')

    def get_href_payment(self):
        pass

    def get_object(self):
        return {
            'id': self.get_id_payment_obj_paysystem(),
            'status_txt': self.get_status_payment_obj_paysystem(),
            'status': self.get_status_payment(),
            'confirmation_url': self.get_confirmation_url_payment_obj_paysystem(),
            'created_at': self.get_created_at_payment_obj_paysystem(),
            'amount': self.get_amount_payment_obj_paysystem(),
            'link': self.get_href_payment()
        }

    def get_id_payment_obj_paysystem(self):
        """
        Возвращает id объекта платежа созданого в платёжной системе
        @return: id объекта платежа созданого в платёжной системе
        """
        pass

    def get_confirmation_url_payment_obj_paysystem(self):
        """
        Возвращает url страницы подтверждения оплаты возвращенную в
        объекте платежа созданого в платёжной системе
        @return: url страницы подтверждения оплаты
        """
        pass

    def get_status_payment_obj_paysystem(self):
        """
        Возвращает статус объекта платежа созданого в платёжной системе
        @return: статус объекта платежа созданого в платёжной системе
        """
        pass

    def get_created_at_payment_obj_paysystem(self):
        """
        Возвращает дату создания объекта платежа созданого в платёжной системе
        @return: дату создания объекта платежа созданого в платёжной системе
        """
        pass

    def get_amount_payment_obj_paysystem(self):
        """
        Возвращает сумму создания объекта платежа созданого в платёжной системе
        @return: сумму создания объекта платежа созданого в платёжной системе
        """
        pass

    def get_status_payment(self):
        pass

    @staticmethod
    def create_record_reestr_available_events(request, order):
        """
        Запись в реестр доступных курсов (элементов курса).
        Данные в записи:
            1. Пользователь;
            2. Тариф(тариф больше удалить нельзя!);
            3. Дата и время создания записи;
            4. Дата и время обновления записи;
        """

        from billing.models import ReestrAvailableEvents
        from billing.services import ServiceReestrAvailableLessonElements

        ReestrAvailableEvents.objects.get_or_create(
            student=order.student,
            tariff=order.tariff,
            event=order.event,
        )


    @staticmethod
    def add_student_to_last_enabled_item(request, order):
        from events.services import ServicesFlows
        return ServicesFlows(
            request, order.event
        ).add_student_to_last_enabled_item(order)
