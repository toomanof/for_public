"""
Service payment with YooMoney
https://yoomoney.ru/docs/wallet
"""
import hashlib
from rest_framework.response import Response

from .abstract_services import AbstractPaymentService
from billing.models import Order, ReestrBillingPayment
from payments.models import ReestrPayment


class ServicePaymentYooMoney(AbstractPaymentService):
    """
    Класс реализации оплаты через yoomoney
    Сценарий авторизации приложения пользователем
        1. Пользователь инициирует авторизацию приложения для управления
           своим счетом.
        2. Приложение отправляет запрос Authorization Request на сервер ЮMoney.
        3. ЮMoney перенаправляют пользователя на страницу авторизации.
        4. Пользователь вводит свой логин и пароль, просматривает список
           запрашиваемых прав и подтверждает, либо отклоняет запрос
           авторизации.
        5. Приложение получает ответ Authorization Response в виде
           HTTP Redirect со временным токеном для получения доступа
           или кодом ошибки.
        6. Приложение, используя полученный временный токен доступа,
           отправляет запрос на получение токена авторизации
           (Access Token Request).
        7. Ответ содержит токен авторизации (access_token).
        8. Приложение сообщает пользователю результат авторизации.
    """

    parameters_yomoney_hook = (
        'notification_type', 'operation_id', 'amount', 'withdraw_amount',
        'currency', 'datetime', 'sender', 'codepro', 'label', 'sha1_hash',
        'test_notification', 'unaccepted'
    )

    def create_payment(self, amount, description,
                       return_url=None, installments=None, receipt=None):
        """
        Создание платежа
        """
        self.payment_obj_paysystem = None
        return self.get_object()


    def get_id_payment_obj_paysystem(self):
        """
        Возвращает id объекта платежа созданого в платёжной системе
        @return: id объекта платежа созданого в платёжной системе
        """
        assert self.payment_obj_paysystem, 'Payment object missing!'
        assert self.payment_obj_paysystem.id, "It's not yandex payment object!"
        return self.payment_obj_paysystem.id

    def get_status_payment_obj_paysystem(self):
        """
        Возвращает статус объекта платежа созданого в платёжной системе
        @return: статус объекта платежа созданого в платёжной системе
        """
        assert self.payment_obj_paysystem, 'Payment object missing!'
        assert self.payment_obj_paysystem.status, "It's not yandex payment object!"
        return self.payment_obj_paysystem.status

    def get_href_payment(self):
        """
        Возвращает url страницы подтверждения оплаты возвращенную в
        объекте платежа созданого в платёжной системе
        @return: url страницы подтверждения оплаты
        """
        assert self.payment_obj_paysystem, 'Payment object missing!'
        assert self.payment_obj_paysystem.confirmation.confirmation_url, "It's not yandex payment object!"
        return self.payment_obj_paysystem.confirmation.confirmation_url

    def get_created_at_payment_obj_paysystem(self):
        """
        Возвращает дату создания объекта платежа созданого в платёжной системе
        @return: дату создания объекта платежа созданого в платёжной системе
        """
        assert self.payment_obj_paysystem, 'Payment object missing!'
        assert self.payment_obj_paysystem.created_at, "It's not yandex payment object!"
        return self.payment_obj_paysystem.created_at

    def get_amount_payment_obj_paysystem(self):
        """
        Возвращает сумму создания объекта платежа созданого в платёжной системе
        @return: сумму создания объекта платежа созданого в платёжной системе
        """
        assert self.payment_obj_paysystem, 'Payment object missing!'
        assert self.payment_obj_paysystem.amount, "It's not yandex payment object!"
        return self.payment_obj_paysystem.amount.value

    def get_status_payment(self):
        status_txt = self.get_status_payment_obj_paysystem()
        if status_txt.lower() in ('pending', 'waiting_for_capture'):
            return Order.PROCESSING
        elif status_txt.lower() == 'succeeded':
            return Order.COMPLETED
        elif status_txt.lower() == 'canceled':
            return Order.DECLINED

    def hash_calculation(self, data):
        """
        Расчет хэша.
        Формирует строку из параметров уведомления в кодировке UTF‑8
        и секретного слова кошелька.
        """
        check_str = ''
        for index in (0, 1, 2, 4, 5, 6, 7):
            check_str += f'{data[self.parameters_yomoney_hook[index]]}&'
        check_str += f'{self.settings.secret}&{data[self.parameters_yomoney_hook[8]]}'

        hash_object = hashlib.sha1(check_str)
        return hash_object.hexdigest()

    def check_in_data(self, data):
        """
        Проверка входящих параметров на соответсве формата YoMoney.
        Проверка подлиности входящих данных от YooMoney.
        """
        #assert self.parameters_yomoney_hook in data, 'Data in request is not YooMoney hook data!'
        assert self.hash_calculation(data) == data[self.parameters_yomoney_hook[9]],\
               'Data in request is not YooMoney hook data!'
        return True

    @staticmethod
    def webhook_handler(request, data):
        if not data:
            return Response(status=200)

        parameters_yomoney_hook = (
            'notification_type', 'operation_id', 'amount', 'withdraw_amount',
            'currency', 'datetime', 'sender', 'codepro', 'label', 'sha1_hash',
            'test_notification', 'unaccepted'
        )
        #assert parameters_yomoney_hook in data, 'Data in request is not YooMoney hook data!'

        record_reestr_payment = ReestrPayment.objects.filter(
            order__uuid=data['label']).first()

        if record_reestr_payment:

            record_reestr_payment.status = ReestrPayment.COMPLETED
            record_reestr_payment.save()

            record_reestr_payment.order.status = record_reestr_payment.status
            record_reestr_payment.order.save()

            ReestrBillingPayment.objects.filter(
                student=record_reestr_payment.student,
                order=record_reestr_payment.order).update(status=record_reestr_payment.status)

            AbstractPaymentService.create_record_reestr_available_events(
                request, record_reestr_payment.order)
            AbstractPaymentService.add_student_to_last_enabled_item(request, record_reestr_payment.order)

        return Response(status=200)
