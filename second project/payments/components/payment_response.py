from .base_object import BaseObject


class PaymentResponse(BaseObject):
    """
    Class representing response object.

    Contains data
    """
    __id = None

    __status = None

    __created_at = None

    __amount = None

    __confirmation_url = None