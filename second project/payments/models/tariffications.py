import uuid

from django.db import models

from core.models import AbstractNotRemoveModel
from events.models import Event, LessonElement


class Tariffication(AbstractNotRemoveModel):
    event = models.ForeignKey(
        Event, on_delete=models.CASCADE, related_name='tariffications',
        verbose_name='Курс')
    title = models.CharField('Наименование',
                             max_length=100, blank=False, null=False)
    description = models.TextField('Описание', blank=True, null=True)
    paid = models.BooleanField('Платный/Бесплатный', default=True)
    price = models.DecimalField('Цена', max_digits=15, decimal_places=2,
                                default=0)
    link = models.UUIDField('Ссылка', editable=False, default=uuid.uuid4)
    term_limits = models.BooleanField('Флаг ограничение срока', default=False)
    duration = models.PositiveIntegerField(
        'Продолжительность действия тарифа в секундах', blank=True, null=True)
    subscription = models.BooleanField('Подписка', default=False)

    def __str__(self):
        return self.title

    def nodes_lesson_elements(self, lesson):
        result = []
        for element in lesson.elements.all_active():
            node_element = {
                'id': element.pk,
                'name': element.title,
                'check': self.elements.filter(lesson_element=element).exists()
            }
            result.append(node_element)
        return result

    def nodes_lessons(self, unit):
        result = []
        for element in unit.lessons.all_active():
            node_element = {
                'id': element.pk,
                'name': element.title,
                'elements': self.nodes_lesson_elements(element)
            }
            result.append(node_element)
        return result

    def nodes_units(self):
        result = []
        for element in self.event.units.all_active():
            node_element = {'id': element.pk,
                            'name': element.title,
                            'elements': self.nodes_lessons(element)}
            result.append(node_element)
        return result

    @property
    def tree(self):
        return self.nodes_units()


class TarifficationElement(models.Model):
    parent = models.ForeignKey(
        Tariffication, on_delete=models.CASCADE, related_name='elements',
        verbose_name='Тариф')
    lesson_element = models.ForeignKey(
        LessonElement, on_delete=models.CASCADE,
        related_name='tarrification_elements',
        verbose_name='Элемент урока')

    def save(self,  *args, **kwargs):
        self.lesson = self.lesson_element.lesson
        self.unit = self.lesson_element.lesson.unit
        super().save(*args, **kwargs)

    class Meta:
        ordering = ['lesson_element__order']
