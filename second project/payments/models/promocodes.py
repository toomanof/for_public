import uuid

from django.db import models

from core.models import AbstractNotRemoveModel
from events.models import Event


class PromoCode(AbstractNotRemoveModel):
    event = models.ForeignKey(
        Event, on_delete=models.CASCADE, related_name='promocodes',
        verbose_name='Курс')
    promocode = models.CharField('Промо код', max_length=200)
    tariffs = models.ManyToManyField('Tariffication',
                                     verbose_name='Тариф',
                                     related_name='promocodes',
                                     blank=True)
    expires = models.DateTimeField('Действует до', blank=True, null=True)
    discount = models.FloatField('процент скидки', default=0)
    used = models.BooleanField('Использованный', default=False)

    class Meta:
        verbose_name = 'Промокод'
        verbose_name_plural = 'Промокоды'
        unique_together = ('promocode', 'event')

    def __str__(self):
        return self.promocode if self.promocode else ""
