from django.contrib.auth.models import User
from django.db import models


class ReestrPayment(models.Model):
    """
    Реестр оплат студентов лекторам
    """
    PS_NONE, PS_PAYPAL, PS_YMONEY, PS_YKASSA = range(4)
    CHOICES_PAY_SYSTEM = (
        (PS_NONE, 'none'),
        (PS_PAYPAL, 'PayPal'),
        (PS_YMONEY, 'yandex money'),
        (PS_YKASSA, 'yandex kassa'),
    )
    PROCESSING, DECLINED, COMPLETED, WAIT_PAYMENT = range(4)
    CHOICES_STATUS = (
        (PROCESSING, 'в обработке'),
        (DECLINED, 'отклонён'),
        (COMPLETED, 'выполнен'),
        (WAIT_PAYMENT, 'ожидание оплаты'),
    )
    lector = models.ForeignKey(User, on_delete=models.PROTECT,
                               verbose_name='Лектор',
                               related_name='payments_plus')
    student = models.ForeignKey(User, on_delete=models.PROTECT,
                                verbose_name='Студент',
                                related_name='payments_minus')
    order = models.ForeignKey('billing.Order', on_delete=models.PROTECT,
                              verbose_name='Заказ',
                              related_name='payments',
                              null=True, blank=True)
    payment_id = models.CharField(max_length=254, verbose_name='id платежа')
    pay_system = models.PositiveSmallIntegerField('Платежная система',
                                                  choices=CHOICES_PAY_SYSTEM,
                                                  default=PS_NONE)
    href = models.URLField('Ссылка на страницу оплаты', max_length=254)
    amount = models.IntegerField('Стоимость', default=0)
    status_txt = models.CharField(max_length=254, verbose_name='Тело сообщения')
    status = models.PositiveSmallIntegerField('Статус',
                                              choices=CHOICES_STATUS,
                                              default=PROCESSING)
    installments_id = models.CharField('ID платежа расрочки', max_length=255,
                                       blank=True, null=True)
    date_create = models.DateTimeField('Дата создание записи',
                                       auto_now_add=True)
    date_update = models.DateTimeField('Дата обновление записи',
                                       auto_now=True)

    class Meta:
        verbose_name = 'Запись реестра оплат студентов лекторам'
        verbose_name_plural = 'Реестр оплат студентов лекторам'


class ReestrUsedPromoCode(models.Model):
    """
    Реестр использования промокодов
    """
    promocode = models.ForeignKey('PromoCode', on_delete=models.CASCADE,
                                  verbose_name='Промокод',
                                  related_name='records_reestr_used_promocode')
    student = models.ForeignKey(User, on_delete=models.CASCADE,
                                verbose_name='Студент',
                                related_name='records_reestr_used_promocode',
                                null=True, blank=True)
    tariff = models.ForeignKey('Tariffication',
                               on_delete=models.CASCADE,
                               verbose_name='Тариф',
                               related_name='records_reestr_used_promocode')
    date_create = models.DateTimeField('Дата создание записи',
                                       auto_now_add=True)
    date_update = models.DateTimeField('Дата обновление записи',
                                       auto_now=True)
