from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

from core.models import AbstractNotRemoveModel


class SettingsPayment(AbstractNotRemoveModel):
    PS_NONE, PS_PAYPAL, PS_YMONEY, PS_YKASSA = range(4)
    CHOICES_PAY_SYSTEM = (
        (PS_NONE, 'none'),
        (PS_PAYPAL, 'PayPal'),
        (PS_YMONEY, 'yandex money'),
        (PS_YKASSA, 'yandex kassa'),
    )
    FT_GTS, FT_STSI, FT_STSC, FT_SE, FT_PATENT = range(5)
    CHOICES_FORM_TAXATION =(
        (FT_GTS, 'ОСН'),
        (FT_STSI, 'УСН Доходы'),
        (FT_STSC, 'УСН Расходы'),
        (FT_SE, 'Самозанятые'),
        (FT_PATENT, 'Патент'),
    )
    user = models.OneToOneField(
        User, on_delete=models.CASCADE, related_name='payment_setting',
        verbose_name='Настройки платёжной системы пользователя')
    pay_system = models.PositiveSmallIntegerField(
        'Платежная система', choices=CHOICES_PAY_SYSTEM, default=PS_NONE)
    account_id = models.CharField('Идентификатор',
        max_length=255, null=True, blank=True)
    secret = models.CharField('Секретный ключ',
        max_length=255, null=True, blank=True)
    email = models.CharField('Email бизнес аккаунта PayPal', max_length=200,
                             null=True, blank=True)
    vat = models.BooleanField('НДС', default=False)
    taxation = models.PositiveSmallIntegerField(
        'Форма налогообложения', choices=CHOICES_FORM_TAXATION, default=FT_GTS)
    currency = models.CharField('Валюта', max_length=5, default='RUB')
    wallet = models.CharField('Номер кошелька', max_length=100,
                              null=True, blank=True)
    installments = models.BooleanField('Рассрочка', default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    service_name = models.CharField('Наименование услуги', max_length=255,
                                    null=True, blank=True)

    @property
    def tax_system_code(self):
        if self.taxation == self.FT_GTS:
            return 1
        if self.taxation == self.FT_STSI:
            return 2
        if self.taxation == self.FT_STSC:
            return 3
        if self.taxation == self.FT_PATENT:
            return 6


@receiver(post_save, sender=User)
def create_user_settings(sender, instance, created, **kwargs):
    SettingsPayment.objects.get_or_create(user=instance)
