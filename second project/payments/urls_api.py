from django.conf import settings
from django.urls import path

from . import views


app_name = 'payments'
urlpatterns = [
    path('create-yandex-payment/',
         views.CreateYandexPayment.as_view(),
         name='create_yandex_payment'),

    path('yandex-payment-webhook/',
         views.WebHookYandexPayment.as_view(),
         name='webhook_yandex_payment'),
    path('yoomoney-webhook/',
         views.WebHookYooMoneyPayment.as_view(),
         name='webhook_yandex_payment'),
    path('paypal-webhook/',
         views.WebHookPayPalPayment.as_view(),
         name='webhook_yandex_payment'),

    path(f'{settings.YANDEX_NOTIFICATIONS_URL}<login>',
         views.NotificationYandexPayment.as_view(),
         name='payment_notifications'),
    path('settings/paypal/',
         views.SettingsPaymentPayPalAPIView.as_view(),
         name='settings_payment_paypal'),
    path('settings/ymoney/',
         views.SettingsPaymentPayPalAPIView.as_view(),
         name='settings_payment_ymoney'),
    path('settings/ykassa/',
         views.SettingsPaymentPayPalAPIView.as_view(),
         name='settings_payment_ykassa'),

    path('my/',
         views.PaymentsStudentAPIView.as_view(),
         name='my_payments'),

]
