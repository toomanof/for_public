from django.contrib import admin

from .models import *


@admin.register(ReestrPayment)
class ReestrPaymentAdmin(admin.ModelAdmin):
    model = ReestrPayment
    list_display = (
        'lector', 'student', 'payment_id', 'amount', 'status',
        'date_create', 'date_update', 'installments_id')
    list_filter = ('lector', 'student', 'status',)


@admin.register(ReestrUsedPromoCode)
class ReestrUsedPromoCodeAdmin(admin.ModelAdmin):
    model = ReestrUsedPromoCode
    list_display = ('promocode', 'tariff', 'date_create', 'date_update')


class TarifficationElementInline(admin.StackedInline):
    model = TarifficationElement


@admin.register(Tariffication)
class TarifficationAdmin(admin.ModelAdmin):
    model = Tariffication
    list_display = ('title', 'event', 'paid', 'price', 'link',)
    inlines = (TarifficationElementInline, )


@admin.register(PromoCode)
class PromoCodeAdmin(admin.ModelAdmin):
    model = PromoCode
    list_display = ('event', 'promocode', 'expires', 'discount', 'used',)


@admin.register(SettingsPayment)
class SettingsPaymentAdmin(admin.ModelAdmin):
    model = SettingsPayment
    list_display = ('user', 'pay_system', 'account_id', 'email', 'vat',
                    'taxation', 'currency', 'wallet', 'installments',
                    'created_at', 'updated_at')
