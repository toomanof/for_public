from rest_framework import serializers

from payments.models import Tariffication, TarifficationElement


class TarifficationElementSerializer(serializers.ModelSerializer):

    class Meta:
        model = TarifficationElement
        fields = '__all__'


class TarifficationSerializer(serializers.ModelSerializer):
    elements = serializers.SerializerMethodField()
    price = serializers.DecimalField(max_digits=15, decimal_places=2,
                                     coerce_to_string=False)

    class Meta:
        model = Tariffication
        fields = '__all__'

    def get_elements(self, element):
        return element.tree


class TarifficationSerializerSmall(serializers.ModelSerializer):
    price = serializers.DecimalField(max_digits=15,
                                     decimal_places=2,
                                     coerce_to_string=False)

    class Meta:
        model = Tariffication
        fields = ['title',
                  'price',
                  'paid',
                  'term_limits',
                  'subscription',
                  'duration',
                  'link']
