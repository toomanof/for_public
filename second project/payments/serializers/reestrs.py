from rest_framework import serializers

from payments.models import ReestrPayment


class ReestrPaymentForStudentSerializer(serializers.ModelSerializer):
    date = serializers.SerializerMethodField()
    order = serializers.SerializerMethodField()
    school = serializers.SerializerMethodField()

    class Meta:
        model = ReestrPayment
        fields = ('id', 'pay_system', 'amount', 'status', 'date', 'order', 'school', 'href')

    def get_date(self, element):
        return element.date_create

    def get_order(self, element):
        return {
            'id': element.order.id,
            'amount': element.order.amount,
            'discount': element.order.discount,
            'status': element.order.status,
            'date': element.order.date_create,
            'event': {
                'id': element.order.event.id,
                'title': element.order.event.title
            },
            'tariff': {
                'id': element.order.tariff.id,
                'title': element.order.tariff.title,
                'paid': element.order.tariff.paid,
                'price': element.order.tariff.price,
                'link': element.order.tariff.link
            },
        }

    def get_school(self, element):
        return {
            'id': element.lector.school.id,
            'title': element.lector.school.title,
            'description': element.lector.school.description,
            'domain_name': element.lector.school.domain_name
        }
