from rest_framework import serializers

from payments.models import SettingsPayment


class SettingsPaymentSerializer(serializers.ModelSerializer):

    class Meta:
        model = SettingsPayment
        fields = '__all__'
