from django.urls import path

from . import views


app_name = 'tariffs'

urlpatterns = [
    path('tariffs/<link>/',
         views.SingleTariif.as_view(),
         name='single_tariff'),
]
