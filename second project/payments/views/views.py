from django.views.generic import View

from payments.services import ServicePaymentYandex


class NotificationYandexPayment(View):
    """
    Представление обработки уведомлений от яндекс кассы
    """

    def get(self, request, *args, **kwargs):
        ServicePaymentYandex.notification()
