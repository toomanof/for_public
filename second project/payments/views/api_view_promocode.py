"""
API представления для работы с платежными системами
"""

import json
from django.shortcuts import get_object_or_404
from django.db.utils import IntegrityError
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from core.errors import wrapper_error
from core.services import camel_case_to_under_case
from events.models import Event
from payments.models import (PromoCode)
from payments.serializers import PromoCodeSerializer


class PromoCodeAPIView(ModelViewSet):
    serializer_class = PromoCodeSerializer
    queryset = PromoCode.objects.all_active()
    permission_classes = [IsAuthenticated]

    def set_new_tariffs(self, promocode, new_tariffs):
        if not new_tariffs:
            return
        try:
            promocode.tariffs.set(new_tariffs)
        except TypeError as e:
            raise TypeError('Tariffs must be in the form of a list!')
        except IntegrityError as e:
            raise IntegrityError('One of the ID list of tariffs '
                                 'does not correspond to the existing tariffs!')

    @wrapper_error
    def list(self, request, event):
        event = get_object_or_404(Event, pk=event)
        return Response(PromoCodeSerializer(
            event.promocodes.all_active(),
            many=True).data)

    @wrapper_error
    def create(self, request, event):
        data = camel_case_to_under_case(json.loads(request.body))
        data['event'] = get_object_or_404(Event, pk=event)
        new_tariffs = data.pop('tariffs', None)

        promocode = PromoCode.objects.create(**data)
        self.set_new_tariffs(promocode, new_tariffs)

        return Response(PromoCodeSerializer(promocode).data)

    @wrapper_error
    def update(self, request, event, pk):
        data = camel_case_to_under_case(json.loads(request.body))
        data['event'] = get_object_or_404(Event, pk=event)
        new_tariffs = data.pop('tariffs', None)

        PromoCode.objects.filter(pk=pk).update(**data)
        promocode = PromoCode.objects.get(pk=pk)
        self.set_new_tariffs(promocode, new_tariffs)

        return Response(PromoCodeSerializer(promocode).data)

    @wrapper_error
    def partial_update(self, request, event, pk):
        return self.update(request, event, pk)
