"""
API представления для работы с платежными системами
"""

import json
from django.core.exceptions import ObjectDoesNotExist
from django.db import models, transaction
from django.shortcuts import get_object_or_404


from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet

from core.errors import wrapper_error, AccessError
from core.services import camel_case_to_under_case, RoleUser
from events.models import Event, LessonElement
from payments.models import Tariffication, TarifficationElement
from payments.serializers import TarifficationSerializer


class SingleTariif(APIView):

    @wrapper_error
    def get(self, request, link):
        """
        Возвращает тариф по переданной ссылке link
        @param link: сылка тарифа
        @return: сирилизованный объект тарифа
        """
        return Response(
            TarifficationSerializer(
                Tariffication.objects.get(link=link)
            ).data
        )


class TarificationAPIView(ModelViewSet):
    serializer_class = TarifficationSerializer
    queryset = Tariffication.objects.all_active()

    @wrapper_error
    def list(self, request, event):
        event = get_object_or_404(Event, pk=event)

        return Response(TarifficationSerializer(
            event.tariffications.all_active(),
            many=True).data)

    @wrapper_error
    @transaction.atomic
    def create(self, request, event):
        elements = None
        data = camel_case_to_under_case(json.loads(request.body))
        data['event'] = get_object_or_404(Event, pk=event)
        if not RoleUser(request).current_user_is_access_tariffs(data['event']):
            raise AccessError('Access denied')
        if 'elements' in data:
            elements = data.pop('elements')
        tariff = Tariffication.objects.create(**data)
        tariff.elements.all().delete()

        if elements:
            list_elements = []

            for element in elements:
                try:
                    lesson_element = LessonElement.objects.get(pk=element)
                except ObjectDoesNotExist:
                    raise ObjectDoesNotExist('The id passed to elements in the parameter are not correct')
                else:
                    list_elements.append(TarifficationElement(parent=tariff, lesson_element=lesson_element))

            TarifficationElement.objects.bulk_create(list_elements)

        else:
            # Если не передается data['elements'] включаются все элементы
            list_elements = [TarifficationElement(parent=tariff, lesson_element=lesson_element)
                             for lesson_element in data['event'].lessons_elements]
            TarifficationElement.objects.bulk_create(list_elements)

        return TarifficationSerializer(tariff).data

    @wrapper_error
    @transaction.atomic
    def update(self, request, event, pk):
        elements = None
        data = camel_case_to_under_case(json.loads(request.body))
        data['event'] = get_object_or_404(Event, pk=event)
        if not RoleUser(request).current_user_is_access_tariffs(data['event']):
            raise AccessError('Access denied')
        if 'elements' in data:
            elements = data.pop('elements')
        Tariffication.objects.filter(pk=pk).update(**data)
        tariff = Tariffication.objects.get(pk=pk)
        if elements:
            tariff.elements.all().delete()
            for element in elements:
                try:
                    lesson_element = LessonElement.objects.get(pk=element)
                except ObjectDoesNotExist:
                    raise ObjectDoesNotExist('The id passed to elements in the parameter are not correct')
                else:
                    tariff.elements.create(lesson_element=lesson_element)

        return TarifficationSerializer(tariff).data

    def partial_update(self, request, event, pk):
        return self.update(request, event, pk)

