"""
API представления для работы с платежными системами
"""

import json

from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from core.errors import wrapper_error as obj_or_error
from core.services import camel_case_to_under_case
from core.errors import wrapper_error
from payments.models import SettingsPayment
from payments.serializers import SettingsPaymentSerializer
from payments.services import (ServicePaymentYandex, ServicePaymentYooMoney,
                               ServicePaymentPayPal)


class SettingsPaymentAPIView(APIView):
    serializer_class = SettingsPaymentSerializer
    queryset = SettingsPayment.objects.all_active()
    permission_classes = [IsAuthenticated]

    @obj_or_error
    def get(self, request):
        user = self.request.current_user
        pay_settings = user.payment_setting
        return Response(
            SettingsPaymentSerializer(
                SettingsPayment.objects.get(pk=pay_settings.pk)).data)

    def _update(self, data):
        data['user'] = self.request.current_user
        pay_settings = data['user'].payment_setting
        SettingsPayment.objects.filter(pk=pay_settings.pk).update(**data)
        return Response(
            SettingsPaymentSerializer(
                SettingsPayment.objects.get(pk=pay_settings.pk)).data)

    @obj_or_error
    def post(self, request):
        data = camel_case_to_under_case(json.loads(request.body))
        return self._update(data)


class SettingsPaymentPayPalAPIView(SettingsPaymentAPIView):
    pass
#    def _update(self, data):
#        data['pay_system'] = SettingsPayment.PS_PAYPAL
#        return super()._update(data)


class SettingsPaymentYMoneyAPIView(SettingsPaymentAPIView):
    pass
#    def _update(self, data):
#        data['pay_system'] = SettingsPayment.PS_YMONEY
#        return super()._update(data)


class SettingsPaymentYKassaAPIView(SettingsPaymentAPIView):
    pass
#    def _update(self, data):
#        data['pay_system'] = SettingsPayment.PS_YKASSA
#        return super()._update(data)


class AbstractCreatePayment(APIView):
    permission_classes = [IsAuthenticated]
    service = None

    @wrapper_error
    def post(self, request, format=None):
        params = camel_case_to_under_case(json.loads(request.body))
        if 'amount' not in params:
            raise Exception("Not found parameters 'amount'!")

        if 'description' not in params:
            raise Exception("Not found parameters 'description'!")

        payment_service = self.service(request)
        payment = payment_service.create_payment(
            amount=params['amount'], description=params['description'])
        return Response(payment)


class CreateYandexPayment(AbstractCreatePayment):

    service = ServicePaymentYandex


class CreatePayPalPayment(APIView):

    service = ServicePaymentPayPal


class AbstractWebHookPayment(APIView):

    service = None

    @wrapper_error
    def post(self, request):
        """
        Принимает входящие уведомления
        @param request:
        @return:
        """
        body = request.body.decode('utf-8')
        body = body.replace("True", "true").replace("False", "false")
        data = json.loads(body)
        return self.service.webhook_handler(request, data)


class WebHookYandexPayment(AbstractWebHookPayment):
    service = ServicePaymentYandex


class WebHookYooMoneyPayment(AbstractWebHookPayment):
    service = ServicePaymentYooMoney

    @wrapper_error
    def post(self, request):
        """
        Принимает входящие уведомления
        @param request:
        @return:
        """
        #body = request.body.decode('utf-8')
        data = request.POST.dict()
        print('======= WebHookYooMoneyPayment ========')
        print(data)
        return self.service.webhook_handler(request, data)


class WebHookPayPalPayment(AbstractWebHookPayment):
    service = ServicePaymentPayPal


class PaymentsStudentAPIView(APIView):

    @obj_or_error
    def get(self, request):
        """
        Возвращает список платежей студента
        """
        from billing.services import BillingService

        filter_params = request.GET.dict() if request.GET else None
        return BillingService(request).list_payments_student(filter_params)
