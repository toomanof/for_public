from django.contrib.auth.models import AnonymousUser
from django.db import close_old_connections

from channels.db import database_sync_to_async
from knox.models import AuthToken


@database_sync_to_async
def get_user(token_key):
        try:
            print('token_key: ', token_key)
            obj_auth = AuthToken.objects.get(token_key=token_key)
        except AuthToken.DoesNotExist:
            return AnonymousUser()
        else:
            return obj_auth.user

class TokenAuthMiddleware:
    """
    Custom middleware (insecure) that takes user IDs from the query string.
    """

    def __init__(self, inner):
        # Store the ASGI application we were passed
        self.inner = inner



    def __call__(self, scope):
        return TokenAuthMiddlewareInstance(scope, self)


class TokenAuthMiddlewareInstance:
    def __init__(self, scope, middleware):
        self.middleware = middleware
        self.scope = dict(scope)
        self.inner = self.middleware.inner


    async def __call__(self, receive, send):
        token = ''
        headers = dict(self.scope['headers'])
        if b'authorization' in headers:
            token_name, token_key = headers[b'authorization'].decode().split()
            if token_name == 'Token':
                token = token_key[:8]
        elif 'query_string' in self.scope:
            token = str(self.scope["query_string"], 'utf-8')[:8]

        self.scope['user'] = await get_user(token)
        inner = self.inner(self.scope)
        return await inner(receive, send)
