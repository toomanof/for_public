from django.urls import path
from django.contrib.auth.decorators import login_required

from .views import *

app_name = 'chat'

urlpatterns = [
    path('', index, name='index'),
    path('<str:room_name>/', room, name='room'),
]