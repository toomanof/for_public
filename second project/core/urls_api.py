from django.urls import path, include
from rest_framework.routers import DefaultRouter

from .views import (
    all_languages, CurrenUserApi, UserAPIView, RegisterAPIView,
    LoginAPIView, LogoutView, ProfileApiView, PasswordLoginResetApiView,
    ActivateUserApiView, SendMessageActivateUserApiView,
    PasswordChangeApiView,
    SendSmsAuthCodeApiView, ActivateUserWithAuthCodeApiView,
    MediumColorsAPIView, DownloadLogoAPIView, DesignApiView,
    PablicDomainSchoolAndDisignView,
    ValidationDomainNameSchoolView, SchoolApiView, SchoolStudentsList,
    SchoolFlowList, VerificationAccountSMS,
    SavePhoneNumberUserWithAuthCodeApiView,
    StatusDomainNameSchoolView, OrdersStudentList, CertificatesStudentList,
    CertificateStudent,
    TokenExchangeView, LoginAsStudent,
    SchoolCatalogVideoListApiView, SchoolCatalogVideoRetrieveApiView,
    SchoolCatalogVideoTariffsApiView)

app_name = 'api_core'
router = DefaultRouter()

urlpatterns = [
    path('all_languages/', all_languages),
    path('current_user/', CurrenUserApi.as_view(), name='current_user'),
    path('user/', UserAPIView.as_view()),

    path('register/', RegisterAPIView.as_view()),
    path('login/', LoginAPIView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('reset_password/', PasswordLoginResetApiView.as_view(),
         name='reset_password'),
    path('change_password/', PasswordChangeApiView.as_view(),
         name='reset_password'),
    path('activate/', ActivateUserApiView.as_view(),
         name='activate'),
    path('', include('knox.urls')),

    path('send_message_active_user/',
         SendMessageActivateUserApiView.as_view(),
         name='send_message_active_user'),

    path('auth/sms/', SendSmsAuthCodeApiView.as_view(),
         name='send_sms_auth_code'),
    path('auth/sms/confirm/',
         ActivateUserWithAuthCodeApiView.as_view(),
         name='check_auth_code'),

    path('medium_colors/',
         MediumColorsAPIView.as_view(),
         name='check_auth_code'),

    path('download_logo/',
         DownloadLogoAPIView.as_view(),
         name='download_logo'),

    path('school/',
         SchoolApiView.as_view(),
         name='school'),
    path('school/design/settings/',
         DesignApiView.as_view(),
         name='school_design_settings'),
    path('school/students/',
         SchoolStudentsList.as_view(),
         name='list_students_in_school'),
    path('school/students/<student_id>/orders',
         OrdersStudentList.as_view(),
         name='list_students_in_school'),
    path('school/students/<student_id>/certificates',
         CertificatesStudentList.as_view(),
         name='list_certifications_in_school'),
    path('school/<id_school>/catalog/video/<pk>/tariffs/',
         SchoolCatalogVideoTariffsApiView.as_view(),
         name='school_catalog_video_retrieve'),

    path('school/certificates/<int:pk>',
         CertificateStudent.as_view(),
         name='certification_in_school'),
    path('school/students/<student_id>/login',
         LoginAsStudent.as_view(),
         name='login_as_student'),

    path('school/flows/',
         SchoolFlowList.as_view(),
         name='list_flow_in_school'),

    path('school/<id_school>/catalog/video/',
         SchoolCatalogVideoListApiView.as_view(),
         name='school_catalog_video_list'),
    path('school/<id_school>/catalog/video/<pk>/',
         SchoolCatalogVideoRetrieveApiView.as_view(),
         name='school_catalog_video_retrieve'),

    path('domain/',
         PablicDomainSchoolAndDisignView.as_view(),
         name='public_domain_school_and_design'),
    path('domains/available',
         ValidationDomainNameSchoolView.as_view(),
         name='validate_domain_name-school'),
    path('domains/status/',
         StatusDomainNameSchoolView.as_view(),
         name='status_domain_name-school'),

    path('verification/account/sms/',
         VerificationAccountSMS.as_view(),
        name='verification_acount_sms'),
    path('verification/account/sms/confirm/',
         SavePhoneNumberUserWithAuthCodeApiView.as_view(),
        name='check_auth_code'),

    path('token/exchange/',
         TokenExchangeView.as_view(),
         name='check_auth_code'),
]

router.register('profiles',
                ProfileApiView, basename='profiles')

urlpatterns += router.urls
