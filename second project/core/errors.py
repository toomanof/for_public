"""
   Модуль ошибок уровня приложения
"""
import time
import logging
import resource

from django.db import connection

from django.core.exceptions import ObjectDoesNotExist, EmptyResultSet
from django.http.response import Http404
from rest_framework import status
from rest_framework.exceptions import AuthenticationFailed
from rest_framework.response import Response
from rest_framework.exceptions import APIException

logger_error = logging.getLogger('warning')
logger_timer = logging.getLogger('timer')


def wrapper_error(method):
    """
    Декоратор в случае совершения ошибки в обёрнутой функции
    возрашает msg ошибки.
    При удачном выполненнии функции возращает её ответ
    """
    def wrapper(self, *args, **kwargs):
        try:
            new_obj = method(self, *args, **kwargs)
        except Exception as e:
            print(type(e))
            json_ret = {
                'message': str(e),
                'code': 400001,
                'status': 'ERROR'}

            logger_error.error(f'{type(e)} {str(e)} in {method.__name__}')

            if isinstance(e, AuthenticationFailed):
                json_ret['code'] = status.HTTP_401_UNAUTHORIZED
                return Response(
                    json_ret, status=status.HTTP_401_UNAUTHORIZED)

            if isinstance(e, (EmptyResultSet, ObjectDoesNotExist, Http404)):
                json_ret['code'] = status.HTTP_404_NOT_FOUND
                return Response(
                    json_ret, status=status.HTTP_404_NOT_FOUND)

            json_ret['code'] = status.HTTP_400_BAD_REQUEST
            if hasattr(e, 'status_code'):
                return Response(json_ret,
                                status=e.status_code)
            else:
                return Response(json_ret,
                                status=status.HTTP_400_BAD_REQUEST)

            if isinstance(e, ConflictError):
                json_ret['code'] = status.HTTP_409_CONFLICT
                return Response(json_ret, status=status.HTTP_409_CONFLICT)
        else:
            if not isinstance(new_obj, Response):
                return Response(new_obj)
            return new_obj
    return wrapper


def timer_function(method):
    """
    Декоратор замеряющий время исполнения финкции и количество выполнения запросов к базе
    """
    def wrapper(self, *args, **kwargs):
        start_mem = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss
        start_qc = len(connection.queries)
        start_time = time.monotonic()

        logger_timer.info(f'Timer_function! Start method: "{self.__class__.__name__}.{method.__name__}"')

        result = method(self, *args, **kwargs)

        logger_timer.info("total queries function '%s %s: %d', time: %.4f sec, memory: %.4f Mb" % (
            self.__class__.__name__,
            method.__name__,
            (len(connection.queries) - start_qc),
            time.time() - start_time,
            (resource.getrusage(resource.RUSAGE_SELF).ru_maxrss - start_mem) / 1000.0
        ))
        return result
    return wrapper


class AccessError(APIException):
    status_code = status.HTTP_403_FORBIDDEN
    default_detail = 'Access denied!.'
    default_code = 'access_error'


class ConflictError(APIException):
    status_code = status.HTTP_409_CONFLICT
    default_detail = 'Conflict  data in request.'
    default_code = 'conflict_error'


class PromoCodeExpiredError(APIException):
    status_code = status.HTTP_410_GONE
    default_detail = 'Promo code expired.'
    default_code = 'expired_error'
