import logging
import subprocess

from celery import Celery

from core.models import School
from core.services import ServiceNginx


app = Celery()
logger = logging.getLogger('celery')


@app.task
def get_status_domains_record_a():
    list_domain_is_ping = []

    list_domains = School.objects\
        .filter(status_domain=School.ST_WAIT_RECORD_A,
                custom_domain=True,
                domain_name__isnull=False)\
        .values_list('domain_name', flat=True)
    logger.info(f'The list of domains will be pinged: {list_domains}')
    for domain in list_domains:
        command = ['ping', '-c', '1', domain]
        if subprocess.call(command) == 0:
            logger.info(f'"{domain}" change status to WAIT_CREATE_NGINX_CONFIG')
            list_domain_is_ping.append(domain)

    School.objects\
        .filter(domain_name__in=list_domain_is_ping)\
        .update(status_domain=School.ST_WAIT_CREATE_NGINX_CONFIG)


@app.task
def create_config_nginx():
    ServiceNginx().create_configs_nginx()


@app.task
def generation_certbot_certificates():
    ServiceNginx().generation_certbot_certificates()
