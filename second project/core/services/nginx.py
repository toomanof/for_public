import logging
import os
import subprocess
from inspect import cleandoc

from django.conf import settings

from core.models import School

logger = logging.getLogger('celery')
logger_error = logging.getLogger('error')


class ServiceNginx:
    body_http = """
            server {
                    listen 80;
                    listen [::]:80;
                    server_tokens off;
                    server_name %s;
                    root /var/www/letsencrypt;
                    location /.well-known {
                        try_files $uri $uri/ =404;
                    }
                    location / {
                        return 301 https://$host$request_uri;
                    }
            }"""
    body_https = """
                    server {
                        listen 443 ssl http2;
                        listen [::]:443 ssl http2;
                        server_name %s;
                        ssl_certificate /etc/letsencrypt/live/%s/fullchain.pem;
                        ssl_certificate_key /etc/letsencrypt/live/%s/privkey.pem;
                        include /etc/letsencrypt/options-ssl-nginx.conf;
                        ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;
                        access_log  off;
                        index index.html;
                        root %s;
                        location / {
                            try_files $uri $uri/ /index.html;
                        }
                        location /api/token/exchange {
                            proxy_pass https://%s;
                        }
                    }"""

    def __init__(self):
        self.PATH_NGINX_CONFIG = '/etc/nginx/conf.d'
        self.PATH_TO_APP = settings.PATH_TO_APP_GRAIN_CLUB

        assert self.PATH_TO_APP, 'Not set environment variable "PATH_TO_APP_GRAIN_CLUB".'

    def add_https_to_nginx_config(self, domain):
        name_config_file = f'{domain}.conf'
        body = cleandoc(self.body_https % (domain, domain, domain, self.PATH_TO_APP, domain))
        with open(os.path.join(self.PATH_NGINX_CONFIG, name_config_file),
                  'a') as file:
            file.write(body)
        return True

    def create_config_nginx(self, domain):
        if self.ping(domain) > 0:
            logger.info(f'{domain} not ping!')
            return

        name_config_file = f'{domain}.conf'
        body = cleandoc(self.body_http % (domain, ))

        try:
            if not os.path.exists(self.PATH_NGINX_CONFIG):
                os.makedirs(self.PATH_NGINX_CONFIG, mode=0o775)

            with open(os.path.join(self.PATH_NGINX_CONFIG, name_config_file), 'w') as file:
                file.write(body)
        except Exception as e:
            logger_error.error(f'Not create config nginx with exception: {e}')
            return

        logger.info(f'Create config nginx for domain: "{domain}"')
        return True

    def create_configs_nginx(self):

        domains = list(School.objects
                       .filter(status_domain=School.ST_WAIT_CREATE_NGINX_CONFIG,
                               custom_domain=True,
                               domain_name__isnull=False)
                       .values_list('domain_name', flat=True))

        logger.info(f'Will created config files for domains list: {domains}')

        for index, domain in enumerate(domains):
            if not self.create_config_nginx(domain):
                domains.pop(index)

        if self.test_nginx():
            try:
                if os.system('service nginx restart') == 0:
                    logger.info(f'Nginx restarted with domains list: {domains} !')

                    School.objects\
                        .filter(domain_name__in=domains)\
                        .update(status_domain=School.ST_WAIT_CERTIFICATION)

            except Exception as e:
                logger.info(f'Something went wrong! Error: {e}')
                return 1
        return 0

    def get_list_config_files_nginx(self):
        return ['.'.join(file.split('.')[:-1])
                for file in os.listdir(self.PATH_NGINX_CONFIG)
                if file.endswith('.conf')]

    def generation_certbot_cert(self, domain):
        logger.info(f'Проверка домена {domain}')
        if self.ping(domain) > 0:
            logger.info('{domain} not ping!')
            return

        try:
            logger.info('Генерация сертификата домена')
            if os.system(f'certbot certonly -d {domain}') == 0:
                logger.info('Congratulations!'
                      'Your certificate and chain have been saved!')
                #send_telegram_message(f'{domain} certificate and chain have been saved!')
        except Exception as e:
            logger_error.error(f'Something went wrong! Error: {e}')
        else:
            self.add_https_to_nginx_config(domain)
            return True

    def generation_certbot_certificates(self):
        domains = list(School.objects
                       .filter(status_domain=School.ST_WAIT_CERTIFICATION,
                               custom_domain=True,
                               domain_name__isnull=False)
                       .values_list('domain_name', flat=True))

        logger.info(f'Will generation certificates for domains list: {domains}')

        list_domain_has_certifications = []
        for domain in domains:
            if self.generation_certbot_cert(domain):
                list_domain_has_certifications.append(domain)

        logger.info(f'Certifications created for domains list: {list_domain_has_certifications}')
        if os.system('service nginx restart') == 0:
            logger.info(f'Nginx restarted with certificates domains list: {domains} !')

        School.objects\
            .filter(domain_name__in=list_domain_has_certifications)\
            .update(status_domain=School.ST_WORK)
        return 0

    def handle(self):
        if self.create_configs_nginx() == 0:
            self.generation_certbot_certificates()

    def ping(self, domain):
        return os.system(f'ping {domain} -c 5')

    def test_nginx(self):
        result = subprocess.getoutput('nginx -t')
        return 'test is successful' in result
