
"""
Модуль аутентификации
"""
import json
import base64
from random import randint
        
from django.conf import settings
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.contrib.auth.tokens import (
    default_token_generator, PasswordResetTokenGenerator)
from django.db.models import Q
from django.utils import timezone
from django.utils.encoding import force_bytes
from django.utils.http import (urlsafe_base64_decode, urlsafe_base64_encode)

from rest_framework.response import Response
from knox.models import AuthToken
from twilio.rest import Client

from core.models import Profile, Email, Emailer
from core.services.secondary_functions import camel_case_to_under_case
from core.serializers.profiles import UserSerializer

from .errors import UserNotFoundException, ERROR_USER_NOT_FOUND
from .twilio import TwilioService


TB_TELEGRAM, TB_VIBER, TB_VK, TB_FB = range(4)
MESSENGERS = (
    (TB_TELEGRAM, 'Telegram'),
    (TB_VIBER, 'Viber'),
    (TB_VK, 'VK'),
    (TB_FB, 'Facebook'),
)

def get_field_profile_id_chat(type_messanger):
    return f'id_bot_{MESSENGERS[type_messanger][1].lower()}'


def set_value_to_custom_field(_object, field_name, value):
    '''
    Метод присвоение объекту _object
    нового значения value, полю field_name.
    И запись объекта
    '''
    setattr(_object, field_name, value)
    _object.save()


class TokenActivationGenerator(PasswordResetTokenGenerator):
    def _make_hash_value(self, user, timestamp):
        return f'{user.pk}{timestamp}{user.is_active}'


class TokenGenerator(PasswordResetTokenGenerator):
    def _make_hash_value(self, user, timestamp):
        return f'{user.pk}{timestamp}'


account_activation_token = TokenActivationGenerator()
account_token = TokenGenerator()


class AccountService:
    """Класс работы с аккаунтом"""

    def __init__(self, request, *args, **kwargs):
        self.request = request
        super().__init__(*args, **kwargs)

    def authenticate(user):
        return authenticate(username=user)

    @staticmethod
    def create_user_on_phone_number(phone_number):
        """
        Создаёт пользователя по номеру телефона
        """
        user = User.objects.create(username=phone_number, email=phone_number)
        set_value_to_custom_field(
            user.profile, 'phone', phone_number)
        return user

    @staticmethod
    def set_active_user(user, status):
        set_value_to_custom_field(user, 'is_active', status)
        set_value_to_custom_field(user.profile, 'is_enabled', status)

    @staticmethod
    def set_enabled_user(user, status):
        set_value_to_custom_field(user.profile, 'is_enabled', status)

    @staticmethod
    def set_username_user(user, username):
        set_value_to_custom_field(user, 'username', username)

    @staticmethod
    def set_username_first_name(user, first_name):
        set_value_to_custom_field(user, 'first_name', first_name)

    @staticmethod
    def set_mesanger_id_chat(user, type_messanger, id_chat):
        ''' Записывает id бот чата в модель Profile. '''
        set_value_to_custom_field(
            user.profile, get_field_profile_id_chat(type_messanger), id_chat)

    @staticmethod
    def send_message_registration(request, new_user):
        """Отправка письма с сылкой акивации акаунта"""
        Emailer.objects.process_email(
            code=Email.ACTIVE_ACCOUNT,
            email_to=new_user.email,
            context={
                'user': new_user,
                'uid': urlsafe_base64_encode(force_bytes(new_user.pk)),
                'token': account_activation_token.make_token(new_user)},
            request=request
        )

    @staticmethod
    def send_message_invite(email_invited, course_id, link):
        """Отправка письма приглашения стать редактором курса"""
        Emailer.objects.process_email(
            code=Email.INVITE,
            email_to=email_invited,
            context={
                'link': link,
                'course_id': course_id}
        )

    @staticmethod
    def send_message_password_recovery(request, user):
        """Отправка письма с сылкой на востановление пароля"""
        Emailer.objects.process_email(
            code=Email.PASSWORD_RESET,
            email_to=user.email,
            context={
                'user': user,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                'token': default_token_generator.make_token(user)},
            request=request
        )

    @staticmethod
    def get_base64email(email):
        return base64.b64encode(email.encode('utf-8')).decode("utf-8")

    @staticmethod
    def decode_email(email):
        return base64.b64decode(email).decode("utf-8")

    @staticmethod
    def get_or_create_user(email):
        user, created = User.objects.get_or_create(
            defaults={'username': email},
            email=email
        )
        return user

    @staticmethod
    def get_or_create_not_active_user(username, email):
        '''
        Временно проставляется всегда активный пользователь
        '''
        created = False
        user = User.objects.filter(email=email).first()
        if not user:
            user = User.objects.create(
                username=username,
                email=email
            )
            created = True
        # временно
        user.is_active = True
        user.save()
        user.profile.is_enabled = True
        user.profile.save()
        return user, created

    @staticmethod
    def get_user_by_id_chat(type_messanger, id_chat):
        '''
        Метод возращает объекта User на основании id чата
        записанного в связанном профиле пользователя
        '''
        filter_params = {
            f'profile__{get_field_profile_id_chat(type_messanger)}': id_chat}
        return User.objects.filter(**filter_params).first()

    @staticmethod
    def get_all_subscriber_bots(type_messanger):
        """
        Метод возращает всех пользователей у которых прописаных
        id ботов в профиле
        """
        return User.objects.filter(
            f'profile__{get_field_profile_id_chat(type_messanger)}__isnull')

    @staticmethod
    def get_chats_id_all_subscriber_bots(type_messanger):
        """
        Метод возращает всех пользователей у которых прописаных
        id ботов в профиле
        """
        condition = {f'profile__{get_field_profile_id_chat(type_messanger)}__isnull': False}
        return User.objects.filter(**condition).values_list(
            f'profile__{get_field_profile_id_chat(type_messanger)}',
            flat=True)

    @staticmethod
    def get_id_chat_by_user(type_messanger, user):
        """
        Метод возращает id (type_messanger) бота пользователя
        записаного в профиле.
        """
        return getattr(user, get_field_profile_id_chat(type_messanger))

    @staticmethod
    def get_user_by_login_or_email(request):
        """ Объвертка над функцией get_user_by_str_login_or_email"""
        if request.is_ajax:
            data = camel_case_to_under_case(json.loads(request.body))
            if 'email_login' in data:
                email_login = data['email_login']

        elif 'email_login' in request.POST:
            email_login = request.POST['email_login']

        elif 'username' in request.POST:
            email_login = request.POST['username']

        else:
            return None, None
        return AccountService.get_user_by_str_login_or_email(email_login)

    @staticmethod
    def get_user_by_str_login_or_email(login_or_email):
        """ Получаем пользователя по имени или email.
        Возращается кортеж с пользователем или None
        и возможными ошибками или пустым списком.
        """
        try:
            try:
                try:
                    validate_email(login_or_email)
                except ValidationError:
                    user = User.objects.get(username=login_or_email)
                else:
                    user = User.objects.get(email=login_or_email)
            except User.DoesNotExist:
                raise UserNotFoundException(
                    login_or_email, False, module=__file__, row=106)
        except UserNotFoundException:
            return None, [ERROR_USER_NOT_FOUND]
        else:
            return user, []

    @staticmethod
    def get_user_by_uidb64(uidb64):
        try:
            uid = urlsafe_base64_decode(uidb64).decode()
            user = User.objects.get(pk=uid)
        except(TypeError, ValueError, OverflowError, User.DoesNotExist):
            user = None
        return user

    @staticmethod
    def get_user_by_id_soc_provider(provider_id, provider_name):
        dict_condition ={f'profile__id_{provider_name}': provider_id}
        return User.objects.filter(**dict_condition).first()

    @staticmethod
    def save_id_soc_provider_in_profile(profile, provider_id, provider_name):
        dict_condition ={f'id_{provider_name}': provider_id}
        Profile.objects.filter(pk=profile.pk).update(**dict_condition)

    @staticmethod
    def activation_user_by_token(**kwargs):
        is_active = False
        user = AccountService.get_user_by_uidb64(kwargs['uidb64'])
        if user and\
           account_activation_token.check_token(user, kwargs['token']):
            is_active = True
            AccountService.set_active_user(user, is_active)
        return user, is_active

    @staticmethod
    def get_user_by_token(**kwargs):
        user = AccountService.get_user_by_uidb64(kwargs['uidb64'])
        if user and account_token.check_token(user, kwargs['token']):
            return user

    @staticmethod
    def get_user_by_knox_token(request):
        token = AccountService.get_knox_token(request)
        if not token:
            return
        obj = AuthToken.objects.filter(token_key=token).first()
        if obj:
            return obj.user

    @staticmethod
    def get_user_by_cookie_token(request):
        token = ''
        if 'token' in request.COOKIES:
            token = request.COOKIES['token'][:8]
        if not token:
            return
        obj = AuthToken.objects.filter(token_key=token).first()
        if obj:
            return obj.user

    @staticmethod
    def get_user_by_phone_number(phone_number):
        return User.objects.filter(
            Q(username=phone_number) | Q(profile__phone=phone_number)
        ).first()

    def get_knox_token(request):
        if 'Authorization' in request.headers:
            token = request.headers['Authorization'].replace('Token ', '')
            return token[:8]

    def count_projects(self):
        """
        Возвращает текущее количество проектов
        """
        return self.request.user.projects.count()

    def count_stages(self):
        """
        Возвращает текущее количество этапов
        """
        return sum([project.stages.count()
                   for project in self.request.user.projects.all()])

    def count_tasks(self):
        """
        Возвращает текущее количество этапов
        """
        return sum([project.tasks.count()
                   for project in self.request.user.projects.all()])

    @staticmethod
    def create_token(user):
        return AuthToken.objects.create(user)[1]

    @staticmethod
    def json_response_with_token(request, user, **kwargs):
        result = {
            'user': UserSerializer(user).data,
            'token': AccountService.create_token(user),
            'need_edit_profile': user.profile.phone is None
        }
        result.update(**kwargs)
        return result

    @staticmethod
    def json_response(request, user, **kwargs):
        """
        Возвращаются данные пользователя с внутренним access token-ом
        """
        result = AccountService.json_response_with_token(request, user, **kwargs)
        response = Response(result)
        response.set_cookie(
            'token', result['token'],
            expires=request.session.get_expiry_date(),
            secure=True,
            domain=request.META['HTTP_HOST'].split(':')[0]
        )
        return response

    @staticmethod
    def save_email(user, email):
        User.objects.filter(pk=user.pk).update(email=email)

    @staticmethod
    def save_id_provider(user, provider_id, provider_name):
        """
        Запись id переданого провайдером соц сети
            provider_id - id соц сети пользователя
            provider_name - наименование соц сети
        """
        setattr(user.profile, f'id_{provider_name}', provider_id)
        user.profile.save()

    @staticmethod
    def save_url_pic_provider(user, url_pic, provider_name):
        """
        Запись url аватарки пользователя переданого провайдером соц сети
            url_pic - url аватарки соц сети пользователя
            provider_name - наименование соц сети
        """
        setattr(user.profile, f'url_pic_{provider_name}', url_pic)
        user.profile.save()

    @staticmethod
    def update_user_fields(user, **kwargs):
        fields = [field.name for field in User._meta.get_fields()]
        user_data = {
            key: val for key, val in kwargs.items() if key in fields}
        try:
            User.objects.filter(pk=user.pk).update(**user_data)
        except Exception:
            pass
        return User.objects.filter(pk=user.pk).first()

    @staticmethod
    def send_sms(body, to_number):
        """
        Отправляет смс через сервис Twilio
        @param body: тело сообщения
        @param to_number: номер телефона получателя
        @return: sid сообщения Twilio
        """
        try:
            tw_service = TwilioService()
            message_sid = tw_service.send_sms(
                to=to_number,
                from_=settings.TWILIO_NUMBER,
                body=body
            )
        except Exception as err:
            raise Exception(f'Unable to send SMS to this phone number! {err}')
        else:
            return message_sid

    @staticmethod
    def send_sms_auth_code_for_verification_number(request):
        data = camel_case_to_under_case(json.loads(request.body))

        recipient = AccountService.get_user_by_phone_number(data['phone'])
        assert not recipient, f"User with phone {data['phone']} not find!"
        obj_auth_phone_code, is_new_auth_code = request.current_user.profile.get_saved_auth_phone_code()

        auth_code = obj_auth_phone_code.value

        message_sid = AccountService.send_sms(str(auth_code), str(data['phone']))

        return {'status': 'send',
                'create_at': obj_auth_phone_code.update_at,
                'expiry': obj_auth_phone_code.expiry,
                'success': True,
                'status_code': 201 if is_new_auth_code else 200}

    @staticmethod
    def send_sms_auth_code_and_create_user(request):
        """
        Отправляет СМС с кодом авторизации.
        Создает пользователя с логином и email равным предоставленному номеру.
        Записывает код авторизации в базу данных. 
        """
        data = camel_case_to_under_case(json.loads(request.body))

        # Вначале ищем пользователя по телефону
        recipient = AccountService.get_user_by_phone_number(data['phone'])

        #  Получаем код авторизации
        if not recipient:
            # Если пользователь не найден в базе, то генерим новый код
            auth_code = randint(settings.MIN_VALUE_PHONE_CODE,
                  settings.MAX_VALUE_PHONE_CODE)
            is_new_auth_code = True
        else:
            try:
                obj_auth_phone_code, is_new_auth_code = recipient.profile.get_saved_auth_phone_code()
                auth_code = obj_auth_phone_code.value
            except Exception as err:
                return {'status': 'error',
                        'message': 'Error create auth code:' + str(err)}, 403

        # Делаем попытку отправить СМС
        if is_new_auth_code:
            message_sid = AccountService.send_sms(str(auth_code), str(data['phone']))

        # Если пользователь так и не найден в базе, то создаем его
        if not recipient:
            recipient = AccountService.create_user_on_phone_number(data['phone'])
            recipient.profile.set_new_auth_phone_code(auth_code)
            AccountService.set_active_user(recipient, True)
            obj_auth_phone_code = recipient.profile.phone_code

        result = {'status': 'send',
                  'create_at': obj_auth_phone_code.update_at,
                  'expiry': obj_auth_phone_code.expiry,
                  'success': True,
                  'status_code': 201 if is_new_auth_code else 200}

        return result, 200

    @staticmethod
    def check_auth_code(request):
        result = {'status': 'error',
                  'message': 'Invalid confirmation code'}

        data = camel_case_to_under_case(json.loads(request.body))

        try:
            recipient = AccountService.get_user_by_phone_number(data['phone'])
        except:
            raise UserNotFoundException(
                'Recipient not found', False, module=__file__, row=378)
            result['message'] = 'Recipient not found'

        if not recipient:
            result['message'] = 'Recipient not found'
        else:
            if recipient.profile.phone_code.is_active_checking and\
               str(recipient.profile.phone_code.value) == str(data['auth_code']):
                recipient.profile.phone_code.delete()
                return recipient
        return result

    @staticmethod
    def check_sms_code(request):
        result = {'status': 'error',
                  'message': 'Invalid confirmation code'}

        data = camel_case_to_under_case(json.loads(request.body))

        try:
            recipient = request.current_user
        except:
            raise UserNotFoundException(
                'Recipient not found', False, module=__file__, row=378)
            result['message'] = 'Recipient not found'

        if not recipient:
            result['message'] = 'Recipient not found'
        else:
            if recipient.profile.phone_code.is_active_checking and\
               str(recipient.profile.phone_code.value) == str(data['auth_code']):
                recipient.profile.phone_code.delete()
                return recipient
        return result

    @staticmethod
    def check_token(token):
        return AuthToken.objects.filter(token_key=token).exists()

    @staticmethod
    def get_current_class_role(request):
        from .roles import (RoleUser, RoleGuest)
        user = request.current_user
        if user:
            return RoleUser
        return RoleGuest
