import re
from django.core.exceptions import FieldError, EmptyResultSet
from django.db import models, transaction
from django.shortcuts import get_object_or_404
from rest_framework import status
from rest_framework.response import Response

from .mixins_services_role import *

from core.errors import wrapper_error as obj_or_error


def permissions_as_owner(method):
    """
    Проверяется совпадения токенов владельца и переданого токена
    При не совпадении возращается ответ отклонения 405
    В случаее возрата ответа типа строка, от вызываемого метода,
    добаляется статус 400
    """

    def wrapper(self, *args, **kwargs):
        from core.services import BaseRole
        role = BaseRole(self.request)
        try:
            user = self.request.current_user
        except Exception:
            return Response('Not found token',
                            status=status.HTTP_401_UNAUTHORIZED)
        try:
            if not role.current_user_is_access_content(self.object):
                return Response('Access denied!. You is not owner',
                                status=status.HTTP_403_FORBIDDEN)
        except Exception as exception:
            return Response(exception)

        response = method(self, *args, **kwargs)
        if isinstance(response, str):
            return Response(response,
                            status=status.HTTP_400_BAD_REQUEST)
        if isinstance(response, Response):
            return response
        return Response(response)

    return wrapper


def get_domain_fist_level(domain_name) -> str:
    result_re = re.match(r'(?!-)[A-Za-z0-9-]{1,63}(?!-)', domain_name)
    if result_re:
        return result_re.group(0)


class BaseService:
    """
    Основа сервисов для работы со объектом.

    """
    model = None
    serializer_class = None  # Сериализующий класс
    queryset = None

    def get_queryset(self):
        assert self.queryset is not None, (
            f"'{self.__class__.__name__}' should either"
            f"include a `model` attribute, "
            f"or override the `get_queryset()` method."
            )
        return self.queryset

    def get_model(self):
        assert self.model is not None, (
                f"'{self.__class__.__name__}' should either"
                f"include a `model` attribute, "
                f"or override the `get_model()` method."
        )
        return self.model

    def get_serializer_class(self):
        assert self.serializer_class is not None, (
                f"'{self.__class__.__name__}' should either include a"
                f"`serializer_class` attribute, or override the"
                f"`get_serializer_class()` method."
        )
        return self.serializer_class


class BaseItemsService(BaseService):
    """
    Основа сервисов для работы со множеством объектов.
    Должнен уметь принимат передавать в метод list параметры фильтрации
    """

    def get_sort_params(self):
        """
        Собирает с request.GET заданое поле сортировки и направление сортировки

        @return: строку с полем сортироки
        """
        index_sort = self.request.GET['sort'] \
            if 'sort' in self.request.GET else 'id'
        if 'dir' in self.request.GET:
            if self.request.GET['dir'] == 'DESC':
                index_sort = '-' + index_sort
        return index_sort

    def get_sort_queryset(self):
        return self.get_queryset().order_by(self.get_sort_params())

    def list(self):
        return self.get_sort_queryset()


class AbstractService(BaseService):
    """
    Абстрактный класс сервиса реализующий базовые
    (присущие всем потомкам) методы
    """
    title_field_active_item = ''  # Наименование переданого поля тек. сущности
    title_atribute_active_item = ''  # Наименование атрибута текущей сущности

    def __init__(self, request):
        self.request = request

    @obj_or_error
    def add_child_obj(self, parent_obj, attr_name, child_id, child_class):
        """
        Метод добавляем в связуемый атрибут 'attr_name' предка
        найденный по 'child_id' потомка
        ---

        paramentrs:

        parent_obj - объект к которому будет производится добавление;
        attr_name - наименование атрибута объекта-предка;
        child_id - id добавляемого объекта;
        child_class - класс добавляемого объекта;

        return:

        None - если произошла ошибка;
        obj - привязанный объект;
        """
        obj = child_class.objects.get(pk=child_id)
        attr_parent = getattr(parent_obj, attr_name)
        attr_parent.add(obj, bulk=False)
        return obj

    def create_child_obj(self, parent_obj, attr_name, **kwargs):
        """
        Создает новый объект и привязывает его к объекту-предку 'parent_obj'
        в автрибут 'attr_name'
        ---

        paramentrs:

        parent_obj - объект к которому будет производится добавление;
        attr_name - наименование атрибута объекта-предка;
        kwargs - поля нового объекта;

        return:

        None - если произошла ошибка;
        obj - созданный и привязанный объект;
        """
        attr_parent = getattr(parent_obj, attr_name)
        self.object = attr_parent.create(**kwargs)
        return self.object

    def create_child_obj_with_serializer(self, parent_obj, attr_name,
                                         serializer, **kwargs):
        """
        Создает новый объект и привязывает его к объекту-предку 'parent_obj'
        в автрибут 'attr_name'
        В случаее ошибки возвращается строковой результат с описанием ошибки
        или сериализованные данные
        """
        result = self.create_child_obj(
            parent_obj, attr_name, **kwargs)

        if isinstance(result, str):
            #  возращается ошибка создания
            return result
        return serializer(result, context={"request": self.request}).data

    def create_object(self, **kwargs):
        """
        Метод по созданию нового объекта
        """
        self.object = self.get_model().objects.create(**kwargs)
        return self.object

    @transaction.atomic
    def create_object_with_order_offset(self, queryset=None,
                                        _class_serializer=None, **kwargs):
        """
        Создание объекта с смещением веса остальных объектов в queryset.
        В начале сохраняется вес с параметров kwargs, далее выполняется
        предопределенный метод create_object и устанавливается выбранный вес
        """
        queryset = queryset if queryset else self.get_queryset()
        if not queryset:
            return

        _order, kwargs = self.pop_order(kwargs, queryset)
        kwargs['order'] = queryset.next_order()

        new_object = self.create_object(**kwargs)
        self._set_order(_order, new_object, queryset, _class_serializer)

        return new_object

    def get_class_object(self, class_obj, pk):
        """
        Возвращает объект класса class_obj по id=pk
        или None при отсутсвии объекта с id=pk
        """
        try:
            result = class_obj.objects.get(pk=pk)
        except Exception:
            return
        else:
            return result

    def get_class_object_by_condition(
            self, class_obj, order_by=None, **kwargs):
        """
        Возвращает объект класса class_obj по переданным услувиям
        или None при отсутсвии объекта с id=pk
        """
        order_by = 'pk' if not order_by else order_by
        return class_obj.objects.filter(**kwargs).order_by(order_by).first()

    def _get_serializered_data(self, data, many=False):
        return self.get_serializered_data(data, many)

    def get_data(self, **data):
        """ При возврате данных добавлем текущий активный атрибут"""
        if self.title_field_active_item and self.get_serializer_class():
            data[self.title_field_active_item] = self.get_serializer_class()(
                getattr(self, self.title_atribute_active_item),
                context={"request": self.request}
            ).data
        return data

    def get_editors(self):
        pass

    def get_object(self):
        return self.object

    def get_serializered_data(self, data, many=False):
        """
        Возврат сериализованых данных
        """
        if self.get_serializer_class():
            return self.get_serializer_class()(
                data, many=many, context={"request": self.request}).data

    def pop_order(self, kwargs, queryset=None):
        """
        Возвращает вес и переданные параметры kwargs
        с исключенным из них параметром order.
        Если order не был передан возвращается следющий в списке queryset
        """
        queryset = queryset if queryset else self.get_queryset()
        if not queryset:
            return

        return kwargs.pop('order', queryset.next_order()), kwargs

    def serializer_data(self):
        return self.get_serializered_data(self.get_object())

    def _set_order(self, value, _obj, queryset=None, _class_serializer=None):
        """
        Устанавливает вес объекту _obj.
        При этом вес элементов у которых он равен или больше значения value
        устанавливается на единицу больше.
            value             - новый вес;
            _obj              - объект у которого уставнавливается новый вес;
            queryset        - queryset с которого выбираются объекты;
            _class_serializer - сеарилизатор объекта;
        """
        queryset = queryset if queryset else self.get_queryset()

        if not queryset:
            return

        items = queryset.all_active().filter(order__gte=value).order_by(
            '-order')

        for item in items:
            item.order += 1
            item.save()
        _obj.order = value
        _obj.save()

        if _class_serializer:
            return _class_serializer(
                _obj, context={"request": self.request}).data
        else:
            return self.get_serializered_data(_obj)

    def update(self, **kwargs):
        """
        Обновляет объект с раздвиганием списка по весу
        """
        _order = 0
        if 'order' in kwargs:
            _order, kwargs = self.pop_order(kwargs)
            kwargs['order'] = self.get_queryset().next_order()

        self.get_queryset().filter(pk=self.get_object().pk).update(**kwargs)

        if 'order' in kwargs:
            return self._set_order(_order, self.get_object())
        else:
            return self.get_object()


class CourseContentService(AbstractService):
    def __init__(self, request, pk, verify_access=True):
        super().__init__(request)
        try:
            pk = int(pk) if isinstance(pk, str) else pk
        except ValueError:
            get_object_or_404(self.get_model(), pk=-1)
        if isinstance(pk, int):
            self.object = get_object_or_404(self.get_model(),
                                           pk=pk, is_deleted=False)
        elif isinstance(pk, models.Model):
            self.object = pk
        if verify_access:
            self.access()

        self.editors = self.get_editors()


class AbstractSubordinateObjectService(AbstractService):
    """
    Абстрактный сервис для одного объекта являющемуся членом множества объекта.
    """

    def __init__(self, request, parent_object, parent_items):
        """
        Инициализация сервиса.
        """
        super().__init__(request)

        if not parent_items:
            raise EmptyResultSet("Parameter parent_items not set!")

        if not isinstance(parent_object, models.Model):
            raise EmptyResultSet("Parameter parent must instance is model!")

        self.parent_object = parent_object
        self.parent_items = parent_items


class SubordinateObjectService(AbstractSubordinateObjectService):
    """
    Сервис для одного объекта являющемуся членом множества объекта.
    """

    def __init__(self, request, parent_object, parent_items, pk=None,
                 **kwargs):
        """
        Инициализация сервиса.
        """
        super().__init__(request, parent_object, parent_items)

        if pk:
            self.object = get_object_or_404(
                self.model, pk=pk, is_deleted=False)
        else:
            try:
                self.object = self.create_object_with_order_offset(**kwargs)
            except FieldError:
                self.object = self.create_object(**kwargs)

    @obj_or_error
    def get_serializer_object(self):
        return self.serializer_data()

    def create_object(self, **kwargs):
        """
        Создается новый вопрос теста
        """
        return self.parent_items.create(**kwargs)

    def set_order(self, value):
        return self._set_order(
            value,
            self.object,
            self.parent_items.all_active(),
            self.get_serializer_class()
        )


class SubordinateObjectsService(AbstractSubordinateObjectService):

    @obj_or_error
    def list(self):
        """
        Возвращает урок по id=pk
        """
        return self.get_serializered_data(
            self.parent_items.all_active(),
            True)

