from collections import namedtuple
from math import sqrt
import random
try:
    import Image
except ImportError:
    from PIL import Image

Point = namedtuple('Point', ('coords', 'n', 'ct'))
Cluster = namedtuple('Cluster', ('points', 'center', 'n'))

class ImageServices:

    def __init__(self, filename):
        self.image = Image.open(filename)

    def get_points(self, img):
        points = []
        w, h = img.size
        for count, color in img.getcolors(w * h):
            points.append(Point(color, 3, count))
        return points

    def get_medium_colors(self, n=3):
        """
        Возврат средних цветов изображения
        """
        thumbnail_image = self.image.copy()
        thumbnail_image.thumbnail((200, 200))
        w, h = self.image.size

        points = self.get_points(thumbnail_image)
        clusters = self.kmeans(points, n, 1)
        rgbs = [map(int, c.center.coords) for c in clusters]
        return [item for item in\
            map(lambda rgb: '#%s' % ''.join(('%02x' % p for p in rgb)), rgbs)]

    def _euclidean(self, p1, p2):
        return sqrt(sum([
            (p1.coords[i] - p2.coords[i]) ** 2 for i in range(p1.n)
        ]))

    def _calculate_center(self, points, n):
        vals = [0.0 for i in range(n)]
        plen = 1
        for p in points:
            plen += p.ct
            for i in range(n):
                vals[i] += (p.coords[i] * p.ct)
        return Point([(v / plen) for v in vals], n, 1)

    def kmeans(self, points, k, min_diff):
        """
        Метод k-средних для кластеризации цветов на изображении.
        Идея метода при кластеризации любых данных заключается в том,
        чтобы минимизировать суммарное квадратичное отклонение точек
        кластеров от центров этих кластеров.
        На первом этапе выбираются случайным образом начальные точки
        (центры масс) и вычисляется принадлежность каждого элемента
        к тому или иному центру. Затем на каждой итерации выполнения
        алгоритма происходит перевычисление центров масс — до тех пор,
        пока алгоритм не сходится.
        """
        clusters = [Cluster([p], p, p.n) for p in random.sample(points, k)]

        while 1:
            plists = [[] for i in range(k)]

            for p in points:
                smallest_distance = float('Inf')
                for i in range(k):
                    distance = self._euclidean(p, clusters[i].center)
                    if distance < smallest_distance:
                        smallest_distance = distance
                        idx = i
                plists[idx].append(p)

            diff = 0
            for i in range(k):
                old = clusters[i]
                center = self._calculate_center(plists[i], old.n)
                new = Cluster(plists[i], center, old.n)
                clusters[i] = new
                diff = max(diff, self._euclidean(old.center, new.center))

            if diff < min_diff:
                break
        return clusters
