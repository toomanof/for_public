"""
Модуль вспомогательных функций
"""
import os
import re
import requests

def clear_models_kwargs(class_model, kwargs):
    """
    Очистка kwargs. Оставляет только значение полей class_model
    """
    name_fields = (f.name for f in class_model._meta.get_fields())
    return (kwargs[field_name] for field_name in name_fields)


def  camel_case_to_under_case(_dict):
    """
    Преобразование название ключей словаря с camelCase стиля в under_case стиль
    """
    result ={}
    for key, val in _dict.items():
        lower_list = re.split(r'[A-Z]', key)
        upper_list = re.findall(r'[A-Z]', key)
        tmp_list = []
        for index in range(len(lower_list)-1):
            tmp_list.append(f'{lower_list[index]}_{upper_list[index].lower()}')
        tmp_list.append(lower_list[len(lower_list)-1])
        result[''.join(tmp_list)] = val

    return result


def get_fields(model, instance):
    field_names = [f.name for f in model._meta.fields]
    return dict((name, getattr(instance, name)) for name in field_names)


def create_txt_file(path, name, body):
    """
    Создает текстовый файл
    @param path:
    @param name:
    @param body:
    @return:
    """
    try:
        if not os.path.exists(path):
            os.makedirs(path, mode=0o666)

        with open(os.path.join(path, name), 'w') as file:
            file.write(body)
    except Exception:
        return False
    return True


def find_sub_str(txt, regex):
    result = ''
    matches = re.search(regex, txt)
    if matches:
        return matches.group(1)

def send_telegram_message(text: str):
    token = "739569173:AAFlJ_I4paomOQwmo1gNukdrX1VJR_hLFVU"
    url = "https://api.telegram.org/bot"
    channel_id = "@toomanof"
    url += token
    method = url + "/sendMessage"

    r = requests.post(method, data={
         "chat_id": channel_id,
         "text": text
          })

    if r.status_code != 200:
        raise Exception("post_text error")
