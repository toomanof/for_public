"""Zoom.us REST API Python Client -- Zoom Room Locations component"""

from __future__ import absolute_import

from zoomus import util
from zoomus.components import base


class RoomLocationComponent(base.BaseComponent):
    """Component dealing with all zoom room Locations related matters"""


    def list(self, **kwargs):
        return self.get_request("/rooms/locations", params=kwargs)

    def add_location(self, **kwargs):
        util.require_keys(kwargs, ["name", "parent_location_id"])
        return self.post_request("/rooms/locations", data=kwargs)

    def get (self, **kwargs):
        util.require_keys(kwargs, "id")
        return self.get_request(f"/rooms/locations/{kwargs['id']}", params=kwargs)

    def update(self, **kwargs):
        util.require_keys(kwargs, "id")
        return self.patch_request(f"/rooms/locations/{kwargs['id']}", data=kwargs)
