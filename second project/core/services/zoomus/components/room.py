"""Zoom.us REST API Python Client -- Zoom Room component"""

from __future__ import absolute_import

from zoomus import util
from zoomus.components import base


class RoomComponent(base.BaseComponent):
    """Component dealing with all zoom room related matters"""


    def list(self, **kwargs):
        return self.get_request(
            "/rooms", params=kwargs
        )

    def add_room(self, **kwargs):
        util.require_keys(kwargs, ["name", "type", "location_id"])
        return self.post_request("/rooms", data=kwargs)

    def get (self, **kwargs):
        util.require_keys(kwargs, "id")
        return self.get_request(f"/rooms/{kwargs['id']}", params=kwargs)

    def update(self, **kwargs):
        util.require_keys(kwargs, "id")
        return self.patch_request(f"/rooms/{kwargs['id']}", data=kwargs)

    def delete(self, **kwargs):
        util.require_keys(kwargs, "id")
        return self.delete_request(f"/rooms/{kwargs['id']}", data=kwargs)
