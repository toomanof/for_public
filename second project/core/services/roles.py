from billing.models import (
    ReestrAvailableEvents)
from events.models import Event, EditorsEvent

from core.errors import AccessError


class BaseRole:
    def __init__(self, request):
        self.request = request
        self.current_user = request.current_user

    def access_event(self):
        pass

    def get_events(self):
        filter_params = {
            'is_deleted': False
        }
        return Event.objects.all_active().filter(
            **self.get_filter_params_with_domain(filter_params))

    def get_filter_params_with_domain(self, filter_params, key_name=None):
        if not key_name:
            key_name = 'owner__school__domain_name'
        if 'domain' in self.request.GET:
            filter_params[key_name] = self.request.GET['domain']

        return filter_params

    def get_editors(self, event):
        """
        Возвращает список редакторов курса
        @param event: Проверяемый курс
        @return: список редакторов курса
        """
        return [element.user for element in event.editors.all()]

    def current_user_is_editor(self, event):
        """
        Проверяет является ли текущий пользователь редактором курса
        @param event: Проверяемый курс
        @return: bool
        """
        return self.current_user in self.get_editors(event)

    def get_settings_editor_current_user(self, event):
        """
        Возвращает запись настроек доступов пользователя как редактора.
        Если текущий пользовательне присутсвует в списоке редакторв курса
        возвращается None
        @param event: Проверяемый курс
        @return:
        """
        for element in event.editors.all():
            if element.user == self.current_user:
                return element

    def _current_user_is_access_field(self, event, field):
        """
        Возвращает флаг наличия у текущего пользователя права доступа к
        параметру 'field'
        настройкам
        @param event: Проверяемый курс
        @param field: Проверяемое поле
        @return:
        """
        if self.current_user == event.owner:
            return True
        current_element_settings_editor = self.get_settings_editor_current_user(event)
        if current_element_settings_editor:
            val = getattr(current_element_settings_editor, field)
            return val

    def current_user_is_access_base(self, event):
        """
        Возвращает флаг наличия у текущего пользователя права доступа
        к основным настройкам
        @param event: Проверяемый курс
        @return: bool
        """
        return self._current_user_is_access_field(event, 'settings')

    def current_user_is_access_tariffs(self, event):
        """
        Возвращает флаг наличия у текущего пользователя права доступа к тарифам
        @param event: Проверяемый курс
        @return: bool
        """
        return self._current_user_is_access_field(event, 'tariffs')

    def current_user_is_access_content(self, event):
        """
        Возвращает флаг наличия у текущего пользователя права доступа
        к содержимому курса
        @param event: Проверяемый курс
        @return: bool
        """
        return self._current_user_is_access_field(event, 'content')

    def current_user_is_access_students(self, event):
        """
        Возвращает флаг наличия у текущего пользователя права доступа
        к студентам курса
        @param event: Проверяемый курс
        @return: bool
        """
        return self._current_user_is_access_field(event, 'students')

    def current_user_is_access_homeworks(self, event):
        """
        Возвращает флаг наличия у текущего пользователя права доступа
        к домашнему заданию
        @param event: Проверяемый курс
        @return: bool
        """
        return self._current_user_is_access_field(event, 'homeworks')


class RoleUser(BaseRole):

    def access_school(self, school):
        result = Event.objects.\
            filter(owner=self.request.current_user, is_deleted=False)\
            .exists()
        return result or EditorsEvent.objects\
            .filter(user=self.request.current_user,
                    event__owner__school=school)\
            .exists()

    def access_event(self, event):
        """
        Проверяет доступ к курсу студентом.
        Вначале идет проверка по наличию доступа к курсу как создателем или
        редакторам,а далее идет проверка на вхождения пользователя в список
        студентов действующих поток курса.
        @param event: object model Event
        @return: Bool
        """
        from billing.services import BillingService

        filter_params = {
            'student': self.request.current_user,
            'event': event,
            'suspended': False
        }

        if self.current_user_is_access_content(event):
            return True

        assert event.published, 'Event not published!'

        if ReestrAvailableEvents.objects.filter(
                student=self.request.current_user,
                event=event,
                suspended=True).exists():
            raise AccessError()

        exists_student_in_flows_event =\
            BillingService(self.request).exists_student_in_flows_event(event)
        exists_student_in_event = ReestrAvailableEvents.objects.filter(**filter_params).exists()
        print('----RoleUser access_event----', exists_student_in_flows_event and exists_student_in_event)
        return exists_student_in_flows_event and exists_student_in_event

    def access_unit(self, unit):
        if not self.current_user_is_access_content(unit.event):
            return False

        list_lesson_element = []
        for lesson in unit.lessons.all_active():
            list_lesson_element = [element for element in lesson.elements.all_active()]

        return ReestrAvailableEvents.objects.filter(
            student=self.current_user,
            tariff__elements__lesson_element__in=list_lesson_element,
            suspended=False
        ).exists()

    def access_lesson(self, lesson):
        if self.current_user_is_access_content(lesson.unit.event):
            return True

        return self.access_event(lesson.unit.event) and\
            ReestrAvailableEvents.objects.filter(
                student=self.current_user,
                tariff__elements__lesson_element__in=lesson.elements.all_active(),
                suspended=False
            ).exists()

    def access_lesson_element(self, lesson_element):
        if self.current_user_is_access_content(lesson_element.lesson.unit.event):
            return True
        return self.access_event(lesson_element.lesson.unit.event) and\
            ReestrAvailableEvents.objects.filter(
                student=self.current_user,
                tariff__elements__lesson_element__in=[lesson_element],
                suspended=False
            ).exists()


class RoleGuest(BaseRole):

    def access_event(self, event=None):
        return False
