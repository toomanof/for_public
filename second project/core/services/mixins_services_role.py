from django.core.exceptions import EmptyResultSet

from core.errors import AccessError
from .users import AccountService


class MixinServiceRole:

    def _access_to_(self, role_name_method, **params_call_role_method):
        if (params_call_role_method and
           'obj' in params_call_role_method and params_call_role_method['obj']):
            obj = params_call_role_method['obj']
        else:
            obj = self.get_object()

        role = self.get_current_role()
        call_role_method = getattr(role, role_name_method)
        result = call_role_method(obj)

        if not result:
            raise AccessError("Access denied!")
        return result

    def get_current_role(self):
        current_class_role = AccountService.get_current_class_role(
            self.request)

        return current_class_role(self.request)

    def get_object(self):
        return self.object


class MixinServiceRoleBlock(MixinServiceRole):

    def access(self, obj=None):
        return self._access_to_('access_unit', obj=obj)


class MixinServiceRoleLesson(MixinServiceRole):

    def access(self, obj=None):
        return self._access_to_('access_unit', obj=obj)


class MixinServiceRoleSchool(MixinServiceRole):

    def access(self, school):
        return self._access_to_('access_school', obj=school)

    def access_to_design_settings(self, school=None):
        return self._access_to_('current_user_is_access_base',
                                obj=school)


class MixinServiceRoleEvent(MixinServiceRole):

    def access(self, event=None):
        return self._access_to_('access_event', obj=event)

    def access_to_students(self, event=None):
        return self._access_to_('current_user_is_access_students',
                                obj=event)

    def access_to_tarrifs(self, event=None):
        return self._access_to_('current_user_is_access_tariffs',
                                obj=event)

    def access_to_homeworks(self, event=None):
        return self._access_to_('current_user_is_access_homeworks',
                                obj=event)


class MixinServiceRoleEvents(MixinServiceRole):
    """
    Дополнение к сервисам для работы со множеством объектов.
    Должен уметь фильтровать выдаваемые объекты по полученому
    в request GET параметру domain и текущему пользователю.
    """

    def get_queryset(self):
        return self.get_current_role().get_events()


class MixinServiceRoleBilling(MixinServiceRole):
    def access(self):
        from core.services import RoleUser
        role = self.get_current_role()
        result = isinstance(role, RoleUser)
        if not result:
            raise AccessError("Access denied!")
        return result


class MixinServiceRoleOrder(MixinServiceRole):

    def get_object(self):
        return self.order.event


class MixinServiceRolePromocode(MixinServiceRole):

    def get_object(self):
        return self.object.event

    def access(self, obj=None):
        return self._access_to_('current_user_is_access_tariffs',
                                obj=obj)


class MixinServiceRoleExecutionEvent(MixinServiceRole):

    def access_unit(self, unit):
        role = self.get_current_role()
        result = role.access_unit(unit)
        if not result:
            raise AccessError("Access denied!")
        return result

    def access_lesson(self, lesson):
        role = self.get_current_role()
        result = role.access_lesson(lesson)
        if not result:
            raise AccessError("Access denied!")
        return result

    def access_lesson_element(self, lesson_element):
        role = self.get_current_role()
        try:
            result = role.access_lesson_element(lesson_element)
        except AttributeError:
            raise AccessError('Access denied!')
        if not result:
            raise AccessError("Access denied!")
        return result
