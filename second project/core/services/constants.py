from django.conf import settings
from django.contrib.sites.shortcuts import get_current_site
from django.utils.translation import ugettext_lazy as _

ERROR_DATA_NOT_TRANSMIT = _('Correct data not transmitted')
ERROR_USER_NOT_FOUND = _('User not found')

POST = 'POST'
GET = 'GET'


def get_common_context(request):
    '''
        Добавляются протокол и домен системы
    '''
    if request:
        current_site = get_current_site(request)
        domain = current_site.domain
    else:
        domain = settings.DEFAULT_DOMAIN
    return {'protocol': settings.DEFAULT_PROTOCOL, 'domain': domain}

