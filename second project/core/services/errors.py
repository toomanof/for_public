import datetime
import logging

from django.utils import timezone
from django.contrib import messages
from django.core.exceptions import ObjectDoesNotExist, EmptyResultSet
from rest_framework import status
from rest_framework.response import Response

from core.errors import wrapper_error as obj_or_error
from .constants import *
from ..models import FacadeLogging as modelFacadeLogging


logger = logging.getLogger('info')


def is_error(value, type_exception, *args):
    '''
        Функция вызывает исключение type_exception
        при false значении value.
        Возвращает обратное значение value
    '''
    try:
        if not value:
            type_exception(*args)
    except type_exception:
        pass
    return not value


class FacadeLogging(object):
    """
    Класс реализующий запись, чтение и удаление
    в/из модуля с внутринними сообщениями пользователю
    Реализует отображение сообщений пользователю в случаее
    инициализации с параметром reading=False
    """
    def __init__(self, message, module, row, reading=False):
        '''
       Запись сообщений в базу.
       Параметры;
         message - сообщение;
         module - модуль в котором был вызван класс;
         row - строка с которй был вызван класс;
         reading - флаг прочтения сообщения;
                   При отрицательном значении сообщение
                   будет выведено на экран пользователю
        '''
        find_messages = self.find_messages(message, module, row, reading)
        if find_messages:
            return
        if not row:
            row = 0
        obj = modelFacadeLogging()
        obj.message = message
        obj.row = row
        obj.module = module
        obj.reading = reading
        obj.save()

    @staticmethod
    def show(request):
        ''' Метод вывода сообщений на экран пользователя
            В данный момент вызывается через context_processors
        '''
        show_messages = modelFacadeLogging.objects.filter(reading=False)
        for msg in show_messages:
            messages.error(request, msg.message)
            msg.reading = True
            msg.save()

        if show_messages:
            FacadeLogging.clear()

    @staticmethod
    def delete(id: int):
        try:
            obj = modelFacadeLogging.objects.get(id)
        except obj.DoesNotExist:
            logger.info(f'FacadeLogging object id:{id} not found')
        else:
            obj.delete()

    @staticmethod
    def clear():
        messages = modelFacadeLogging.objects.filter(reading=True)
        clear_messages = FacadeLogging.find_messages_before(messages, 10)
        clear_messages.delete()

    def find_messages(self, message, module, row, reading=False):
        messages = modelFacadeLogging.objects.filter(
            message=message,
            module=module,
            row=row,
            reading=reading,
        )
        return FacadeLogging.find_messages_before(messages, 1)

    @staticmethod
    def find_messages_before(messages, delta_minutes):
        before_minute = timezone.now() -\
            datetime.timedelta(minutes=delta_minutes)
        return messages.filter(dt__gte=before_minute)


class WingowebException(Exception):
    """ Класс родитель ошибок приложений
    """
    def __init__(self, show=True, *args, **kwargs):
        super().__init__(self.message)
        module = '' if 'module' not in kwargs else kwargs['module']
        row = 0 if 'row' not in kwargs else kwargs['row']

        self.__logging(self.message, show, module, row)

    def __logging(self, message, show, module, row):
        """ Вызывает функцию логирования ошибки и
            вывода сообщения пользователю"""
        FacadeLogging(message, module, row, not show)


class NotHasPossibilityAccount(WingowebException):
    """Абстрактный класс"""
    sub_message = ''

    def __init__(self, user, show=False, *args, **kwargs):
        self.message = f'{user}: {self.sub_message}'
        super().__init__(show, *args, **kwargs)


class UserNotFoundException(NotHasPossibilityAccount):
    """Исключение отсутсвия пользователя в базе"""
    message = ERROR_USER_NOT_FOUND
