"""
Сервис работы с сервисом Vimeo
https://docs.api.video/reference
"""
from datetime import timedelta
import requests

from django.conf import settings
from django.utils import timezone

class ApiRequestsServices:
    def __init__(self):
        self.headers = {
            'Content-Type': 'application/json',
            'accept': 'application/json'}
        self.response = None
        self.created = timezone.now()      

    def get_headers(self, headers=None):
        return headers if headers else self.headers

    def get(self, url, headers=None):
        self.created = timezone.now()
        self.response = requests.get(url, headers=self.get_headers(headers))
        self.response.raise_for_status()
        return self.response

    def post(self, url, data=None, headers=None, files=None):
        self.created = timezone.now()
        self.response = requests.post(
            url, headers=self.get_headers(headers), data=data, files=files)
        self.response.raise_for_status()
        return self.response

    def json(self):
        if not self.response:
            return
        self.response.raise_for_status()
        return self.response.json()


class ServicesVimeo(ApiRequestsServices):
    
    def __init__(self):
        self.api_key = settings.API_KEY_VIMEO
        self.authenticate()

    @property
    def access_token(self):
        return self.authenticate_data.get('access_token')

    @property
    def refresh_token(self):
        return self.authenticate_data.get('refresh_token')

    @property
    def expires(self):
        return = self.created + timedelta(self.authenticate_data['expires_in'])

    @property
    def expires_in(self):
        return self.authenticate_data.get('expires_in')

    def authenticate(self):
        """
        The first action taken when interacting with api.video
        is the authentication step (NOTE: this is abstracted in many of the SDKs).
        To Authenticate, you must submit your API key.
        Upon authentication, api.video will return an access_token
        that is valid for one hour (3600 seconds).
        """
        url = 'https://sandbox.api.video/auth/api-key'
        self.post(url, data={'apiKey': self.api_key})
        return self.get_authenticate_data_with_json()

    def add_authorization_header(self, headers):
        headers['Authorization'] = f'Bearer {self.access_token}'
        return headers

    def set_refresh_token(self):
        self.post('https://ws.api.video/auth/refresh')
        return self.get_authenticate_data_with_json()

    def get_authenticate_data_with_json(self)
        self.authenticate_data = self.json()
        return self.authenticate_data

    def creating_hosted_video(self, host_url, **kwargs):
        """
        To create a video, you create its metadata first,
        then you upload contents.
        A video is public by default.
        You can also create a video directly from one hosted on a third-party
        server by giving its URI in source parameter.
        In this case, the service will respond 202 Accepted and download the
        video asynchronously.
        """
        self.post('https://ws.api.video/videos',
                  data=kwargs,
                  headers=self.add_authorization_header({
                    'Content-Type':'application/json'}))

    def upload_video(self, video_id, file,
                     _from=None, _to=None, total_size=None):
        """
        To upload a video to an existing videoId. Replace {videoId}
        with the id you'd like to use, {access_token} with your token,
        and /path/to/video.mp4 with the path to the video you'd like to upload.

        Video Upload (Large Videos) :
        https://api.video/blog/tutorials/video-upload-tutorial-large-videos
        """
        url = f'https://ws.api.video/videos/{video_id}/source'

        if _from and _to and total_size:
            headers = {
                'Content-Range': f'bytes {_from}-{_to}/{total_size}'}
        else:
            headers = {
                "accept": "application/vnd.api.video+json",
                "content-type": "multipart/form-data"
            }
        headers = self.add_authorization_header(headers)

        try:
            files = {'file': open(file, 'rb')}
        except Exception:
            pass
        self.post(url, files=files)
