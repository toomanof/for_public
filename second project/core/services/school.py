from operator import attrgetter
from django.db import models
from django.db.models import Sum, Count
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import get_object_or_404

from core.models import School, Design
from core.serializers import SchoolSerializer, DesignSerializer,\
    UserSerializer, UserSmallInfoSerializer
from core.services import secondary_functions as sf
from events.models import ReestrExecution
from .abstract_services import AbstractService, obj_or_error


class ServicesSchool(AbstractService):
    """ Бизнес-логика для одного курса"""

    serializer_class = SchoolSerializer
    title_field_active_item = 'active_event'
    title_atribute_active_item = 'event'

    def __init__(self, request, pk_school=None):
        super().__init__(request)
        self.school = None
        if isinstance(pk_school, str):
            try:
                pk_school = int(pk_school) if isinstance(pk_school, str)\
                    else pk_school
            except ValueError:
                get_object_or_404(School, pk=-1)

        if isinstance(pk_school, int):
            self.school = get_object_or_404(School, pk=pk_school)

        elif isinstance(pk_school, models.Model):
            self.school = pk_school

        if not pk_school and not self.school:
            get_object_or_404(School, pk=-1)

        if pk_school and self.school:
            if 'authorization' in self.request.headers:
                self.current_user = self.request.current_user
            else:
                self.current_user = None

    def get_design_settings(self):
        """
        Возвращает настроки дизайна страницы курса по id=pk
        или None при отсутсвии его отсутсвии
        """
        design_settings = Design.objects.get(pk=self.school.design.pk)
        if not design_settings:
            raise ObjectDoesNotExist("Designs not found!")
        return design_settings

    @obj_or_error
    def update_design_settings(self, **kwargs):
        Design.objects.filter(pk=self.school.design.pk).update(**kwargs)
        if not self.school.user.profile.design_configured:
            self.school.user.profile.design_configured = True
            self.school.user.profile.save()
        return self.get_design_settings_serializer()

    def get_design_settings_serializer(self):
        """
        Возвращает сериализованные настройки дизайна страницы курса
        """
        return DesignSerializer(
            self.get_design_settings(),
            context={"request": self.request}).data

    @obj_or_error
    def get_responce_design_settings(self):
        return self.get_design_settings_serializer()

    def list_students(self, **params):
        """
        Список студентов оплативших курс
        """
        from billing.services import BillingService

        print('------------ list_students----------------')

        del_key = [key for key, val in params.items() if not val]
        for key in del_key:
            params.pop(key)

        filter_params = {'event__owner__school': self.school} if not params else params
        if 'sorted' in filter_params:
            filter_params.pop('sorted')

        list_record_reestr_student_in_school =\
            BillingService.list_record_reestr_student_in_school(self.school, **filter_params)

        list_users = []
        print('list_record_reestr_student_in_school', list_record_reestr_student_in_school)

        if 'sorted' in params and params['sorted']:
            if params['sorted'] == 'score':
                list_users = ReestrExecution.objects\
                    .filter(student__in=list_record_reestr_student_in_school)\
                    .annotate(sum=Sum('score'))

                list_users = [record.student
                              for record in sorted(list_users,
                                                   key=attrgetter('sum'))]
            if params['sorted'] == 'progress':
                list_users = ReestrExecution.objects\
                    .filter(student__in=list_record_reestr_student_in_school)\
                    .annotate(count=Count('score'))

                list_users = [record.student
                              for record in sorted(list_users,
                                                   key=attrgetter('count'))]
        result = []
        if list_users:
            for element in list_users:
                for index, record in enumerate(list_record_reestr_student_in_school):
                    if element == record:
                        result.append({
                            **UserSmallInfoSerializer(record.student).data,
                            'status': record.status
                        })
                        list_record_reestr_student_in_school.pop(index)
        else:
            result = [{**UserSmallInfoSerializer(record.student).data,
                       'status': record.status
                        } for record in list_record_reestr_student_in_school]

        return result

    def list_flows(self):
        from events.models import Flow
        from events.serializers import FlowSerializer
        events_current_user = self.request.current_user.owner_events.all()
        return FlowSerializer(
            Flow.objects.filter(event__in=events_current_user),
            many=True
        ).data

    def create_config_nginx(self):
        path = '/etc/nginx/config/'
        name = f'{self.school.domain_name}.conf'
        body = """server {
                 listen 80;
                 listen [::]:80;
                 server_name %s;
                 return 301 https://$host$request_uri;
                 }
                 server {
                 listen 443 ssl http2;
                 listen [::]:443 ssl http2;
                 server_name %s;
                 ssl_certificate /etc/letsencrypt/live/dev.grain.club-0001/fullchain.pem;
                 ssl_certificate_key /etc/letsencrypt/live/dev.grain.club-0001/privkey.pem;
                 include /etc/letsencrypt/options-ssl-nginx.conf;
                 ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;
                 root /opt/grain/dev_edu_school/build;
                 access_log  off;
                 index index.html;
                 location / {
                 try_files $uri $uri/ /index.html;
                 }
                 }""" % (name, name)

        sf.create_txt_file(path, name, body)
