from django.contrib.auth import views as django_auth_views
from django.contrib.auth.decorators import login_required
from django.views.generic.base import RedirectView
from django.urls import path, reverse_lazy

from .forms import LoginForm
from .views import *

app_name = 'core'

urlpatterns = [
    path('', RedirectView.as_view(url='login/'), name='auth_root'),
    path('login/', LoginView.as_view(
        authentication_form=LoginForm), name='login'),
    path('logout/', django_auth_views.LogoutView.as_view(), name='logout'),

    path('password_reset/',
         PasswordLoginResetView.as_view(),
         name='password_reset'),
    path('password_reset/done/',
         django_auth_views.PasswordResetDoneView.as_view(),
         name='password_reset_done'),

    path('reset/<uidb64>/<token>/',
         django_auth_views.PasswordResetConfirmView.as_view(
            template_name='account/password_reset_confirm.html',
            success_url=reverse_lazy('core:password_reset_complete')),
         name='password_reset_confirm'),

    path('reset/done/',
         django_auth_views.PasswordResetCompleteView.as_view(
            template_name='account/password_reset_complete.html',
            ),
         name='password_reset_complete'),

    path('register/',
         RegistrationView.as_view(),
         name='register'),
    path('register_done/',
         RegistrationDoneView.as_view(),
         name='register_done'),

    path('remove/<int:pk>',
         login_required(UserDeleteView.as_view()),
         name='remove_account'),

    path('activate/<uidb64>/<token>',
         ActivateView.as_view(),
         name='activate'),
    path('login-token/<uidb64>/<token>',
         LoginTokenView.as_view(),
         name='login_token'),

    path('password_reset_now/',
         login_required(PasswordResetView.as_view()),
         name='password_reset_token'),
    path('change_email/',
         login_required(ChangeEmailView.as_view()),
         name='change_email'),
]
