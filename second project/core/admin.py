from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User
from payments.models import SettingsPayment
from .models import Profile, School, Design, AuthPhoneCode


class SchoolInline(admin.StackedInline):
    model = School
    can_delete = False
    fk_name = 'user'


class InfoInline(admin.StackedInline):
    model = Profile
    can_delete = False
    fk_name = 'user'


class InfoSettingsPaymentInline(admin.StackedInline):
    model = SettingsPayment
    can_delete = False
    fk_name = 'user'


class UserAdmin(BaseUserAdmin):
    search_fields = ('id', 'username', 'email', 'first_name', 'last_name')
    list_display = ('id', 'username', 'email', 'first_name', 'last_name', 'is_active')
    inlines = (InfoInline, SchoolInline, InfoSettingsPaymentInline)


class DesignEventInline(admin.StackedInline):
    model = Design
    can_delete = False
    fk_name = 'school'


@admin.register(School)
class SchoolAdmin(admin.ModelAdmin):
    model = School
    list_display = ('title', 'description', 'user',)
    inlines = (DesignEventInline,)
    list_filter = ('user',)


admin.site.unregister(User)
admin.site.register(User, UserAdmin)


@admin.register(AuthPhoneCode)
class AuthPhoneCodeAdmin(admin.ModelAdmin):
    model = AuthPhoneCode
