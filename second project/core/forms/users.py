from django import forms
from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from django.contrib.auth.models import User
from django.contrib.auth import (authenticate, password_validation)
from django.contrib.auth.forms import (
    AuthenticationForm, UserCreationForm, SetPasswordForm)
from django.utils.translation import ugettext_lazy as _

from bots.services.constants import MESSENGERS

def unique_email(value, *args, **kwargs):
    if UserRegistrationForm._meta.model.objects.filter(email=value).exists():
        raise ValidationError(
            _('A user with this email is already registered'),
            code='email'
        )


class LoginForm(AuthenticationForm):
    messenger = forms.ChoiceField(
        choices=MESSENGERS,
        widget=forms.RadioSelect(), 
        required=False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['username'].label = 'Имя или email пользователя'


class MixingCurrentPassword(forms.Form):
    password = forms.CharField(
        label=_("Current password"),
        strip=False,
        widget=forms.PasswordInput,
        help_text=password_validation.password_validators_help_text_html(),
    )

    error_messages = {
        'invalid_password': _(
            "Please enter a correct password. Note that "
            "field may be case-sensitive."
        ),
        'password_mismatch': _("The two password fields didn't match."),
        'inactive': _("This account is inactive."),
    }

    def __init__(self, *args, **kwargs):

        self.request = kwargs.pop('request')
        self.user = self.request.user

        if 'email' in self.fields:
            self.fields['email'].initial = self.user.email

    def clean_password(self):
        password = self.cleaned_data.get("password")
        if password:
            self.user_cache = authenticate(
                self.request, username=self.user.username, password=password
            )
            if self.user_cache is None:
                raise self.get_invalid_login_error()

    def get_invalid_login_error(self):
        return forms.ValidationError(
            self.error_messages['invalid_password'],
            code='invalid_password',
        )


class UserRegistrationForm(UserCreationForm):
    email = forms.EmailField(
        label="Email",
        max_length=254,
        validators=[validate_email, unique_email]
    )
    username = forms.CharField(
        max_length=50,
        required=True
    )
    messenger = forms.ChoiceField(
        choices=MESSENGERS,
        widget=forms.RadioSelect(), 
        required=False)

    def __init__(self, *args, **kwargs):
        _username = kwargs.pop('username', '')
        _email = kwargs.pop('email', '')
        super().__init__(*args, **kwargs)
        self.fields['username'].initial = _username
        self.fields['email'].initial = _email

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2')
        labels = {
            'username': _('Login'),
            'email': 'E-mail',
            'password1': _('Password'),
            'password2': _('Repeat password'),
            'messengers': _('Messengers')
        }
        help_texts = {
            'username': _('Login'),
            'email': _('Your email'),
            'password1': _('Password'),
            'password2': _('Repeat password'),
        }


class UserEditForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(UserEditForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].widget.attrs.update({'required': 'required'})
        self.fields['first_name'].max_length = 20
        self.fields['last_name'].max_length = 20

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name')
        labels = {
            'first_name': _('Fisrt name'),
            'last_name': _('Last name'),
        }


class UpdateEmailForm(MixingCurrentPassword):
    email = forms.EmailField(
        label="Email",
        max_length=254,
        validators=[validate_email, unique_email]
    )

    class Meta:
        fields = ('password', 'email')


class UpdatePasswordForm(MixingCurrentPassword, SetPasswordForm):
    class Meta:
        fields = ('password', 'new_password1', 'new_password2')
