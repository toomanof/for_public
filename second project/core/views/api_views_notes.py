import json
from rest_framework.viewsets import ModelViewSet
from rest_framework import permissions
from core.models import Note
from core.serializers import NoteSerializer
from core.errors import wrapper_error
from core.services import camel_case_to_under_case


class NoteViewSet(ModelViewSet):
    permission_classes = [permissions.IsAuthenticated]
    queryset = Note.objects.all()
    serializer_class = NoteSerializer

    def get_queryset(self):
        return self.queryset.filter(student=self.kwargs['id_student'],
                           school=self.kwargs['id_school'])

    @wrapper_error
    def create(self, request, id_school, id_student, *args, **kwargs):
        data = camel_case_to_under_case(json.loads(request.body))
        data['student_id'] = id_student
        data['school_id'] = id_school
        print(data)
        obj = Note.objects.create(**data)
        return NoteSerializer(obj).data

    @wrapper_error
    def list(self, request, id_school, id_student, *args, **kwargs):
        return super().list(request, *args, **kwargs)

    @wrapper_error
    def update(self, request, id_school, id_student, *args, **kwargs):
        data = camel_case_to_under_case(json.loads(request.body))
        data['student_id'] = id_student
        data['school_id'] = id_school
        print(data, kwargs)
        obj = Note.objects.filter(id=kwargs['pk']).update(**data)

        return NoteSerializer(self.get_object()).data

    @wrapper_error
    def retrieve(self, request, id_school, id_student, pk, *args, **kwargs):
        return super(NoteViewSet, self).retrieve(request,  *args, **kwargs)
