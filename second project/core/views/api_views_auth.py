"""
API представления для работы с пользователями

/api/current_user/   - GET возращает текущего пользователя
/api/user/           - GET возращает пользователя
/api/user/           - PUT, PATCH обнавляет данные пользователя
    Параметрами выступают поля моделей User и Profile
/api/profiles/       - GET возращает профили пользователей
/api/register/       - POST регистрация пользователя
    Параметры
        username - логин,
        email    - email,
        password - пароль
/api/login/          - POST авторизация пользователя
    Параметры
        username - логин или email пользователя,
        password - пароль
/api/logout/         - POST деавторизация пользователя
/api/reset_password/ - POST отправка мыла с сылкой на сброс пароля 
    Параметры
        email_login - логин или email пользователя
/api/change_password/ - PUT, PATCH смена пароля.
    Пользователь определяется по переданому токену
    Параметры
        old_password    - старый пароль,
        new_password:   - новый пароль,
        new_password1:  - повторение нового пароля

/api/activate/ - POST активация пользователя
    Параметры
        uidb64 - 
        token  - 
/api/send_message_active_user/ - POST отправка мыла на активацию пользователя
    Параметры
        email_login - логин или email пользователя

/api/send_sms_auth_code/ - POST отправка sms кода на активацию пользователя
    Параметры
        recipient - id получателя, передается когда пользователь
                    уже создан в базе
        phone - телефон получателя, передается когда пользователя необходимо
                создать в базе
        параметры взаимоисключающие

/api/check_auth_code/ - POST проверка кода на активацию пользователя
    Параметры
        recipient - id проверяемого пользователя
        auth_code - код переданный пользователем для проверки
"""
import json

from django.core.exceptions import EmptyResultSet
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.contrib.auth.signals import user_logged_out
from django.db import transaction
from django.shortcuts import get_object_or_404
from rest_framework import exceptions
from rest_framework import status
from rest_framework import generics, permissions
from rest_framework.decorators import api_view
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from django.shortcuts import HttpResponseRedirect

from knox.models import AuthToken

from bots.services import AccountServiceWorkWithMessengers
from core.auth import CookieTokenAuthentication
from core.errors import wrapper_error
from core.models import Profile
from core.services import (
    AccountService, obj_or_error, camel_case_to_under_case)
from core.serializers.profiles import (
    UserSerializer, ProfileSerializer,
    RegisterSerializer, LoginSerializer,
    ChangePasswordSerializer
)


class HttpResponseSeeOther(HttpResponseRedirect):
    status_code = 303


class CurrenUserApi(APIView):
    authentication_classes = [CookieTokenAuthentication]
    permission_classes = [
        permissions.IsAuthenticated,
    ]
    def get(self, request):
        """
        возращает текущего пользователя
        """
        serializer = UserSerializer(request.current_user)
        return Response(serializer.data)


class UserAPIView(generics.RetrieveUpdateAPIView):
    """
    Обнавляет данные пользователя
    """
    permission_classes = [
        permissions.IsAuthenticated,
    ]
    serializer_class = UserSerializer

    @obj_or_error
    @transaction.atomic
    def update(self, request, *args, **kwargs):
        serializer = self.serializer_class(
            self.request.current_user,
            data=camel_case_to_under_case(json.loads(request.body)),
            partial=True,
            context=self.get_serializer_context())
        serializer.is_valid(raise_exception=True)
        user = serializer.save(user_pk=self.request.current_user.pk)
        user = User.objects.get(pk=self.request.current_user.pk)
        return AccountService.json_response(
            request,
            user,
            session_expiry_date=request.session.get_expiry_date())


class RegisterAPIView(generics.GenericAPIView):
    """
    Регистрация пользователя
    """
    serializer_class = RegisterSerializer
    permission_classes = [
        permissions.AllowAny,
    ]
    @transaction.atomic
    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=camel_case_to_under_case(request.data))
        serializer.is_valid(raise_exception=True)
        new_user = serializer.save()

        new_user.username = new_user.email.split('@')[0]
        new_user.save()
        AccountService.set_active_user(new_user, False)
        AccountService.send_message_registration(self.request, new_user)
        as_with_messangers = AccountServiceWorkWithMessengers(self.request)
        self.request.session['start_link_to_messenger'] =\
            as_with_messangers.get_link_registration_to_messenger()
        return Response({
            "user": UserSerializer(new_user, context=self.get_serializer_context()).data,
            "token": AccountService.create_token(new_user),
            "session_expiry_date": request.session.get_expiry_date()
        })


class LoginAPIView(generics.GenericAPIView):
    """
    Авторизация пользователя
    """

    serializer_class = LoginSerializer
    message = {
        'is_active': False,
        'is_enabled': False,
        'code': 400001,
        'status': 'ERROR'
    }

    def check_user_is_active(self, user):

        if not user:
            self.message['message'] = 'User not found!'
            raise exceptions.NotAuthenticated(self.message)

        self.message['is_active'] = user.is_active
        self.message['is_enabled'] = user.profile.is_enabled

        if not self.message['is_active']:
            self.message['message'] = 'User not active!'
            raise exceptions.NotAuthenticated(self.message)
        if not self.message['is_enabled']:
            self.message['message'] = 'User not enabled!'
            raise exceptions.NotAuthenticated(self.message)
        return user.is_active and user.profile.is_enabled

    def post(self, request, *args, **kwargs):

        serializer = self.get_serializer(data=camel_case_to_under_case(request.data))
        serializer.is_valid()
        user, _ = AccountService.get_user_by_str_login_or_email(request.data['username'])
        try:
            self.check_user_is_active(user)
        except exceptions.NotAuthenticated as e:
            return Response(self.message, status=403)

        user = authenticate(username=user.username, password=request.data['password'])
        if user:
            login(request, user,  backend='django.contrib.auth.backends.ModelBackend')
            return AccountService.json_response(
                request,
                user,
                session_expiry_date=request.session.get_expiry_date())

        self.message['message'] = 'Login or password is incorrect'
        return Response(self.message, status=403)


class LogoutView(APIView):
    authentication_classes = [CookieTokenAuthentication]
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        request.current_user.auth_token_set.all().delete()
        if hasattr(request, 'current_user'):
            request.current_user = None
        user_logged_out.send(sender=request.user.__class__,
                             request=request, user=request.user)
        response = Response(None, status=status.HTTP_204_NO_CONTENT)
        response.set_cookie(
            'token', "",
            max_age=1,
            secure=True,
            domain=request.META['HTTP_HOST'].split(':')[0]
        )
        response.delete_cookie('token')
        return response


class ProfileApiView(ModelViewSet):
    serializer_class = ProfileSerializer
    queryset = Profile.objects.all()

    permission_classes = [IsAuthenticated]
    http_method_names = ['get', 'put', 'patch']

    def get_filter_dict(self, request):
        """
        Создает словарь для фильтрации списка студентов
        """
        filter_data = request.GET
        result = {
            'user__events__in': filter_data['event'],
        }
        return result

    def get_queryset(self, request):
        try:
            filter_dict = self.get_filter_dict(request)
            result = Profile.objects.filter(**filter_dict)
        except Exception:
            result = Profile.objects.all()
        return result

    @wrapper_error
    def update(self, request, pk):
        profile = get_object_or_404(Profile, pk=pk)

        serializer = UserSerializer(
            profile.user,
            data=camel_case_to_under_case(json.loads(request.body)),
            partial=True,
            context=self.get_serializer_context())
        serializer.is_valid(raise_exception=True)

        user = serializer.save(profile.user.pk)
        user = User.objects.get(pk=user.pk)

        return Response(ProfileSerializer(user.profile).data)

    def partial_update(self, request, pk):
        return self.update(request, pk)

    @obj_or_error
    def list(self, request):
        """
        Возращает профили пользователей
        """
        return Response(
            ProfileSerializer(self.get_queryset(request),
                              many=True).data
        )

    @obj_or_error
    def retrieve(self, request, pk):
        return Response(
            ProfileSerializer(
                get_object_or_404(Profile, pk=pk)).data)


class PasswordLoginResetApiView(APIView):
    """
    Отправка мыла с сылкой на сброс пароля 

    Parameters
    ----------
        email_login - логин или email пользователя

    """

    @obj_or_error
    def post(self, request, format=None):
        user, errors = AccountService.get_user_by_login_or_email(request)
        if not user:
            raise EmptyResultSet('User not found!')
        if errors:
            raise Exception(errors)
        AccountService.send_message_password_recovery(request, user)
        return Response('Mail sent')


class PasswordChangeApiView(generics.UpdateAPIView):
    """
    API обновление пароля

    Parameters
    ----------
        old_password    - старый пароль,
        new_password:   - новый пароль,
        new_password1:  - повторение нового пароля

    """
    serializer_class = ChangePasswordSerializer
    permission_classes = [permissions.IsAuthenticated]
    http_method_names = ['get', 'put', 'patch', 'head']
    @obj_or_error
    def update(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=camel_case_to_under_case(json.loads(request.body)))
        serializer.is_valid(raise_exception=True)
        user = serializer.save()

        if hasattr(user, 'auth_token'):
            user.auth_token.delete()
        return AccountService.json_response(
            request,
            user,
            session_expiry_date=request.session.get_expiry_date())


class ActivateUserApiView(APIView):
    """
    Активация пользователя
    """

    @obj_or_error
    def post(self, request, *args, **kwargs):
        if not request.body:
            raise Exception('Not paramentrs!')
        user, self.is_active = AccountService.\
            activation_user_by_token(**camel_case_to_under_case(json.loads(request.body)))
        if not user:
            raise Exception('Invalide token')
        login(request, user, backend='django.contrib.auth.backends.ModelBackend')
        return AccountService.json_response(
            request,
            user,
            session_expiry_date=request.session.get_expiry_date())


class SendMessageActivateUserApiView(APIView):
    @obj_or_error
    def post(self, request, *args, **kwargs):
        """
        Отправка мыла на активацию пользователя
        Пользователь определяется по переданому токену
        """
        user, errors = AccountService.get_user_by_login_or_email(request)
        if not user:
            raise EmptyResultSet('User not found!')
        AccountService.send_message_registration(request, user)

        return Response('Send message active user')


class SendSmsAuthCodeApiView(APIView):
    """
    Отправка sms кода на активацию пользователя.
    Создает пользователя с логином и email равным предоставленному номеру.
    Записывает код авторизации в базу данных.
    Parameters
    ----------
        phone - телефон получателя, передается когда пользователя необходимо
                создать в базе

    """
    @obj_or_error
    def post(self, request):
        response, status = AccountService.send_sms_auth_code_and_create_user(request)
        return Response(response, status=status)


class ActivateUserWithAuthCodeApiView(APIView):
    """
    Активация пользователя по коду из SMS.
    Проверка кода на активацию пользователя.

    Parameters
    ----------
        phone - телефон пользователя
        auth_code - код переданный пользователем для проверки

    """
    @obj_or_error
    def post(self, request):
        response = AccountService.check_auth_code(request)
        if isinstance(response, User):
            login(request, response,  backend='django.contrib.auth.backends.ModelBackend')
            return AccountService.json_response(
                request,
                response,
                session_expiry_date=request.session.get_expiry_date())

        return Response(response, status=403)


class VerificationAccountSMS(APIView):
    permission_classes = [
        permissions.IsAuthenticated,
    ]
    @obj_or_error
    def post(self, request):
        response = AccountService.send_sms_auth_code_for_verification_number(request)
        return Response(response)


class SavePhoneNumberUserWithAuthCodeApiView(APIView):
    permission_classes = [
        permissions.IsAuthenticated,
    ]
    @obj_or_error
    def post(self, request):
        data = camel_case_to_under_case(json.loads(request.body))
        assert 'phone' in data, 'Not field phone'

        response = AccountService.check_sms_code(request)
        if isinstance(response, User):
            response.profile.phone = data['phone']
            response.profile.save()
            return Response({'success': True, 'message': 'Phone number saved'})
        return Response(response, status=403)


class TokenExchangeView(APIView):
    def post(self, request):
        data = request.data
        target = data['return'] if 'return' in data else '/'
        response = HttpResponseSeeOther(redirect_to=target)

        if 'token' in data:
            response.set_cookie('token', data['token'], max_age=60, secure=True)

        return response

    def get(self, request):
        token = None
        if 'token' in request.COOKIES:
            token = request.COOKIES['token']
        
        return Response({'token': token})
