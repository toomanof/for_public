from django.contrib.auth import login, views, update_session_auth_hash
from django.contrib.auth.models import User

from django.db import transaction

from django.template.response import TemplateResponse
from django.views.generic.base import TemplateView
from django.views.generic.edit import (FormView, DeleteView)
from django.views.generic import RedirectView
from django.urls import reverse_lazy


from core.forms import (
    UserRegistrationForm, UpdatePasswordForm, UpdateEmailForm)
from bots.services import AccountServiceWorkWithMessengers
from core.services import AccountService


class LoginView(views.LoginView):
    """ Внесена регистрация по email"""

    success_url = reverse_lazy('events:home')

    def post(self, request, *args, **kwargs):
        if 'messenger' in request.POST and request.POST['messenger']:
            as_with_messangers = AccountServiceWorkWithMessengers(self.request)
            return self.render_to_response({
                'link_auth': as_with_messangers.get_link_authentication_to_messenger()}
            )
        form = self.get_form()
        user, errors = AccountService.get_user_by_login_or_email(request)
        if errors:
            form.is_valid()
            return self.form_invalid(form)

        _mutable = form.data._mutable
        form.data._mutable = True
        form.data['username'] = user.username
        form.data._mutable = _mutable
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)


class LoginTokenView(RedirectView):
    url = reverse_lazy('home')

    def get(self, request, *args, **kwargs):
        user =  AccountService.get_user_by_token(**kwargs)
        if user and not user.is_authenticated:
            login(request, user)
        return super().get(request, *args, **kwargs)


class RegistrationView(FormView):
    """ Класс регистрации новых клиентов
        Производится проерка заполнения формы
        и сохраненние данных пользователя
        При правильном предоставлении данных отправляется
        письмо с ссылкой на активацию аккаунта
   """
    template_name = 'account/register.html'
    form_class = UserRegistrationForm
    success_url = reverse_lazy('core:register_done')

    @transaction.atomic
    def form_valid(self, form):
        new_user = form.save()
        AccountService.set_active_user(new_user, False)
        AccountService.send_message_registration(self.request, new_user)
        as_with_messangers = AccountServiceWorkWithMessengers(self.request)
        self.request.session['start_link_to_messenger'] =\
            as_with_messangers.get_link_registration_to_messenger()
        return super().form_valid(form)


class RegistrationDoneView(TemplateView):
    template_name = 'account/register_done.html'

    def get_context_data(self, *args, **kwargs):
        ctx = super().get_context_data(*args, **kwargs)
        if 'start_link_to_messenger' in self.request.session:
            ctx['start_link_to_messenger'] =\
                self.request.session.pop('start_link_to_messenger')

        return ctx


class ActivateView(TemplateView):
    is_active = False
    template_name = 'account/activation_done.html'

    def get_context_data(self, *args, **kwargs):
        ctx = super().get_context_data(*args, **kwargs)
        ctx['is_active'] = self.is_active
        return ctx

    def get(self, request, *args, **kwargs):
        user, self.is_active = AccountService.\
            activation_user_by_token(**kwargs)
        if user:
            login(request, user)
        return super().get(request, *args, **kwargs)


class PasswordLoginResetView(TemplateView):
    template_name = 'registration/password_reset.html'

    def post(self, request, *args, **kwargs):
        user, errors = AccountService.get_user_by_login_or_email(request)
        if errors:
            return TemplateResponse(request,
                                    self.template_name,
                                    {'errors': errors})
        AccountService.send_message_password_recovery(request, user)
        return TemplateResponse(request, self.template_name, {'send': True})


class PasswordResetView(FormView):
    template_name = 'registration/password_reset_token.html'
    form_class = UpdatePasswordForm
    success_url = reverse_lazy('home')

    def get_form_kwargs(self):
        kwargs = super(FormView, self).get_form_kwargs()
        kwargs['request'] = self.request
        kwargs['send_user_to_parend'] = True
        return kwargs

    def form_valid(self, form):
        form.save()
        update_session_auth_hash(self.request, self.request.user)
        return super(FormView, self).form_valid(form)


class ChangeEmailView(FormView):
    model = User
    form_class = UpdateEmailForm
    template_name = 'account/change_email.html'
    success_url = reverse_lazy('home')

    def get_form_kwargs(self):
        kwargs = super(FormView, self).get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def form_valid(self, form):
        self.request.user.email = form.cleaned_data.get('email')
        self.request.user.save()
        return super(FormView, self).form_valid(form)


class UserDeleteView(DeleteView):
    model = User
    success_url = reverse_lazy('home')

    def get(self, *args, **kwargs):
        return self.post(*args, **kwargs)
