"""
API представления для работы с блоками курсами

/ru/api/school/{}/design/settings/      - GET      - список блоков курса

/ru/api/school/{}/design/settings/      - POST     - создание блока курса

/ru/api/school/{}/design/settings/{}/   - PUT      - редактирование блока курса

/ru/api/school/{}/design/settings/{}/   - PATCH    - редактирование блока курса

/ru/api/school/{}/design/settings/{}/   - DELETE   - удаление блока курса
"""
import json

from rest_framework.views import APIView

from django.shortcuts import get_object_or_404

from core.services import ServicesSchool, camel_case_to_under_case
from core.serializers import DesignSerializer

from core.services import obj_or_error
from core.models import Design, School

from .view_mixins import MixinGetPermission, MixinPostPermission


class DesignApiView(MixinGetPermission, MixinPostPermission, APIView):
    serializer_class = DesignSerializer

    @obj_or_error
    def get(self, request):
        """
        Возвращает настроки дизайна страницы курса
        """
        if 'domain' in request.GET:
            school = get_object_or_404(School, domain_name=request.GET['domain'])
            return ServicesSchool(request,
                                  school).get_design_settings_serializer()
        user = self.request.current_user
        assert user, 'User not authorization'
        return ServicesSchool(request,
                              user.school).get_responce_design_settings()

    def post(self, request):
        """
        Обновляет настройки дизайна страницы курса

        Parameters
        ----------
        logoId          - ID хранилища изображения
        logoIsText      - Логотип текстовый
        logoTxt         - текст логотипа
        fontLogo        - Шрифт логотипа
        urlFontLogo     - ссылка на шрифт логотипа
        sizeFontLogo    - Размер шрифта логотипа
        sizeFont        - Размер шрифта
        fontHead        - Шрифт заголовка
        fontMenu        - Шрифт меню
        fontBaseText    - Шрифт текста на странице
        colorBgBase     - Цвет основного фона
        colorTextBase   - Цвет основного текста
        colorBgMenu     - Цвет фона меню
        colorTextMenu   - Цвет меню
        colorHeaders    - Цвет заголовков
        colorBgButton   - Цвет фона кнопки
        colorTextButton - Цвет текста кнопки
        """
        user = self.request.current_user
        return ServicesSchool(
            request, user.school.pk).update_design_settings(
                **camel_case_to_under_case(json.loads(request.body)))
