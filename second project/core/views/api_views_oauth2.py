from oauth2_provider.models import get_application_model
from rest_framework.decorators import api_view
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import permissions

from core.errors import wrapper_error
from core.serializers.oauth2 import OAuth2AppsSerializer
from core.services.oauth2 import (
    GoogleServicesOAuth2, FacebookServicesOAuth2, VKServicesOAuth2)


@api_view(['GET'])
def applications(request):
    qs = get_application_model().objects.all()
    serializer = OAuth2AppsSerializer(qs, many=True)
    return Response(serializer.data)


def get_data_provider_for_login(request, provider_name):
    """
    На основании имени провайдера с базы берется данные авторизации
    """
    provider = get_application_model().objects.filter(
        name__icontains=provider_name)

    if not provider and 'code' not in request.GET:
        return

    redirect_uris = provider.redirect_uris.strip().split()
    return {
        'code': request.GET['code'],
        'client_id': provider.client_id,
        'client_secret': provider.client_secret,
        'redirect_uri': redirect_uris[0]
    }


@wrapper_error
def provider_login(request, provider_service):
    serviceOAuth2 = provider_service(request)
    return serviceOAuth2.execute()


@wrapper_error
def provider_verification(request, provider_service):
    serviceOAuth2 = provider_service(request)
    is_verified = serviceOAuth2.verification()

    if not is_verified:
        return Response({'success': False, 'message': 'Account already used!'}, status=409)

    return Response({'success': True, 'message': ' Account verified. Profile updated.'})


class GoogleLogin(APIView):

    def post(self, request):
        """
        POST запрос для подтверждения на google api
        полученного access token и регистрациии/авторизации в системе
       ---
            parameters:
                - accessToken: access token
                  description: Acces token issued by google
                  required: true
                  type: string
        """
        return provider_login(request, GoogleServicesOAuth2)


class FaceBookLogin(APIView):

    def post(self, request):
        """
        POST запрос для подтверждения на facebook api
        полученного access token и регистрациии/авторизации в системе
       ---
            parameters:
                - accessToken: access token
                  description: Acces token issued by facebook
                  required: true
                  type: string
        """
        return provider_login(request, FacebookServicesOAuth2)


class VKLogin(APIView):

    def post(self, request):
        """
        POST запрос для подтверждения на vk api
        полученного access token и регистрациии/авторизации в системе
       ---
            parameters:
                - accessToken: access token
                  description: Acces token issued by facebook
                  required: true
                  type: string
        ---

        """
        return provider_login(request, VKServicesOAuth2)


class VerificationGoogleAccount(APIView):
    permission_classes = [
        permissions.IsAuthenticated,
    ]

    def post(self, request):
        """
        POST запрос для подтверждения на google api
        полученного access token
            parameters:
                - accessToken: access token
                  description: Acces token issued by facebook
                  required: true
                  type: string
        """
        return provider_verification(request, GoogleServicesOAuth2)


class VerificationFaceBookAccount(APIView):
    permission_classes = [
        permissions.IsAuthenticated,
    ]

    def post(self, request):
        """
        POST запрос для подтверждения на facebook api
        полученного access token
            parameters:
                - accessToken: access token
                  description: Acces token issued by facebook
                  required: true
                  type: string
        """
        return provider_verification(request, FacebookServicesOAuth2)


class VerificationVKAccount(APIView):
    permission_classes = [
        permissions.IsAuthenticated,
    ]

    def post(self, request):
        """
        POST запрос для подтверждения на vk api
        полученного access token
            parameters:
                - accessToken: access token
                  description: Acces token issued by facebook
                  required: true
                  type: string
        """
        return provider_verification(request, VKServicesOAuth2)
