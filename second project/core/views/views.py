from django.shortcuts import render,redirect
from django.views import View

# HOME VIEW
class HomeView(View):

    def get(self, request):
        if request.user.is_authenticated:
            return redirect('dashboard')
#        return render(request, 'core/home.html')
        return render(request, 'core/build/index.html')


# DASHBOARD VIEW
class DashboardView(View):

    def get(self, request):
        if not request.user.is_authenticated:
            return redirect('home')
        return render(request, 'core/build/index.html')
#        return render(request, 'core/home.html')
