from django.core.exceptions import FieldDoesNotExist
from rest_framework import permissions


class MixinPermissionCustomActions:
    def get_permissions(self):
        try:
            # return permission_classes depending on `action`
            return [permission() for permission in\
                    self.permission_classes_by_action[self.action]]
        except KeyError:
            # action is not set return default permission_classes
            return [permission() for permission in self.permission_classes]


class MixinDeletePermission:
    def get_permissions(self):
        if self.request.method == 'DELETE':
            return [permission() for permission in (permissions.IsAuthenticated,)]
        return super().get_permissions()


class MixinGetPermission:
    def get_permissions(self):
        if self.request.method == 'GET':
            return [permission() for permission in (permissions.AllowAny,)]
        return super().get_permissions()


class MixinPostPermission:
    def get_permissions(self):
        if self.request.method == 'POST':
            return [permission() for permission in (permissions.IsAuthenticated,)]
        return super().get_permissions()


class AbstractFilterView:
    """
    Абстрактный класс фильтрации моделей по
    параметрам переданным в GET запросе
    """
    model = None  # модель с которой берется выборка
    foreign_fields = ()  # перечисление связанных полей

    def get_filter(self):
        """  Абстрактный метод получение словоря фильтрации. """
        f = self.add_prefix_id(self.get_query_filter())
        return f

    def get_query_filter(self):
        """
        Функция преобразовывает request.GET в словарь
        с удаление лишних (не присутсвующих в модели) полей
        """
        query_filter = {}
        for key, val in self.request.GET.items():
            try:
                self.model._meta.get_field(key)
            except FieldDoesNotExist:
                pass
            else:
                query_filter[key] = val
        return query_filter

    def get_queryset(self):
        """  Метод получение результатов с применением фильтров. """
        if not self.model or not self.request.GET:
            return self.model.objects.all_active()

        query_filter = self.get_filter()
        if not query_filter:
            result_qs = self.model.objects.all_active()
        else:
            result_qs = self.model.objects.filter(**query_filter)

        if ('limit' in self.request.GET):
            result_qs = result_qs[:int(self.request.GET['limit'])]
        return result_qs

    def add_prefix_id(self, query_filter):
        """  Функция добавляет в фильровочный словарь
           id поля связаных полей.
        """
        for field in self.foreign_fields:
            if field in self.request.GET:
                query_filter.pop(field)
                query_filter[f'{field}_id'] = self.request.GET[field]
        return query_filter
