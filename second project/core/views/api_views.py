import os
import json

from django.conf import settings

from rest_framework.decorators import api_view
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from core.services import camel_case_to_under_case
from core.services import ImageServices, obj_or_error
from core.models import School
from events.models import ImageStorage
from events.services import ServicesImageStorage


@obj_or_error
def get_medium_colors(request, image_storage, count_colors=None):
        count_medium_colors = 3 if not count_colors else int(count_colors)
        return Response({
            'medumColors': ImageServices(
                image_storage.image).get_medium_colors(count_medium_colors)
        })


@api_view(['GET'])
def all_languages(request):
    """
    Выводит список доступных языков.
    """
    return Response(settings.MODELTRANSLATION_LANGUAGES)


class MediumColorsAPIView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, format=None):
        """
        Возвращает массив средних цветов переданого изображения

        Parameters
        ----------
        imageStorageId - id объекта хранилища изображения
        countColors - количество возвращаемых средних цветов, по умолчанию три
        """
        data = camel_case_to_under_case(request.GET)
        return get_medium_colors(
            request, ImageStorage.objects.get(pk=data['image_storage_id']),
            int(data.get('count_colors', 3)))


class DownloadLogoAPIView(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request, format=None):
        """
        Метод загрузки логотипа

        Parameters
        ----------
        image - изображение;
        countColors - количество возвращаемых средних цветов, по умолчанию три
        """
        data = camel_case_to_under_case(request.data)
        si = ServicesImageStorage(request, file=request.FILES['file'])
        return get_medium_colors(
            request,
            si.object,
            int(data.get('count_colors', 3)))


class ValidationDomainNameSchoolView(APIView):

    @obj_or_error
    def get(self, request):
        """
        Проверяет на уникальность доменное имя.

        @param domain_name: проверяемое имя
        @return: Флаг уникальности доменого имени курса
        """
        assert request.GET['name'], 'Not set parameter name!'
        return not School.objects.filter(
            domain_name=request.GET['name']).exists()


class StatusDomainNameSchoolView(APIView):

    @obj_or_error
    def get(self, request):
        """
        Проверяет на уникальность доменное имя.

        @return: Статус домена у текущего пользователя
        """
        assert request.current_user, 'Not set current user!'
        return {'status_domain': request.current_user.school.status_domain}
