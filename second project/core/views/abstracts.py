import json
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ModelViewSet
from django.shortcuts import get_object_or_404

from core.errors import wrapper_error
from core.services import camel_case_to_under_case


class AbstractModelViewSetWithCurrentUser(ModelViewSet):
    permission_classes = [IsAuthenticated]
    object = None

    @wrapper_error
    def create(self, request, *args, **kwargs) -> object:
        data = camel_case_to_under_case(json.loads(request.body))
        data['user'] = request.current_user
        if hasattr(self.get_model(), 'order'):
            data['order'] = self.get_model().objects.next_order()

        self.object = self.get_model().objects.create(**data)
        return self.get_serializer(self.object).data

    def get_model(self):
        assert self.model, 'Undefined self model!'
        return self.model

    def get_object(self):
        return self.object

    def get_queryset(self):
        return self.get_model().objects.filter(
            user=self.request.current_user, is_deleted=False)

    @wrapper_error
    def list(self, request):
        return self.get_serializer(self.get_queryset(), many=True).data

    def update(self, request, pk=None, *args, **kwargs):
        data = camel_case_to_under_case(json.loads(request.body))
        data['user'] = request.current_user
        self.get_model().objects.filter(id=pk).update(**data)
        return self.retrieve(request, pk)

    @wrapper_error
    def retrieve(self, request, pk=None, *args, **kwargs):
        return self.get_serializer(
            get_object_or_404(self.get_model(), id=pk, is_deleted=False)).data

    @wrapper_error
    def destroy(self, request, pk=None):
        get_object_or_404(self.get_model(), id=pk, is_deleted=False).delete()
