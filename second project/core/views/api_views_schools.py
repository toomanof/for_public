import json
from django.db.models import Q
from rest_framework.views import APIView
from rest_framework.generics import ListAPIView, RetrieveAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404

from billing.serializers import OrderSmallSerializer
from billing.models import Order, ReestrAvailableEvents
from core.errors import AccessError
from core.serializers import SchoolSerializer
from core.services import (
    obj_or_error, camel_case_to_under_case, ServicesSchool, AccountService)
from core.models import School
from events.models import StudentCertificate
from events.serializers import StudentCertificateSerializer
from video_catalog.models import AccessVideoCourse, AccessVideoTariff, VideoCatalog
from video_catalog.serializers import \
    VideoCatalogSerializer, VideoCatalogPreviewSerializer


class SchoolApiView(APIView):

    @obj_or_error
    def get(self, request):
        user = self.request.current_user
        return Response(SchoolSerializer(user.school).data)

    @obj_or_error
    def post(self, request):
        user = self.request.current_user
        data = camel_case_to_under_case(json.loads(request.body))
        School.objects.filter(pk=user.school.pk).update(**data)
        if not user.profile.school_configured:
            user.profile.school_configured = True
            user.profile.save()
        return Response(
            SchoolSerializer(School.objects.get(pk=user.school.pk)).data
        )


class PablicDomainSchoolAndDisignView(APIView):

    @obj_or_error
    def get(self, request):
        """
        По переданному названию домена возвращает данные
        привязанной школы и её настроки дизайна.
        """
        assert 'name' in request.GET, 'Name is required'
        school = get_object_or_404(School, domain_name=request.GET['name'])
        return {
            'design': ServicesSchool(request,
                                     school).get_design_settings_serializer(),
            'school': SchoolSerializer(School.objects.get(pk=school.pk)).data
        }


class SchoolStudentsList(APIView):
    """
    Список студентов школы
    GET параметры фильтрации:
        - event - id курса
        - flow - id потока
        - tariff - id тарифа
        - promocode - id промокода
        - sorted - сортировка по
          - score - балам
          - progress - прогресу выполнения
    """

    @obj_or_error
    def get(self, request):
        return ServicesSchool(request,
                              request.current_user.school).list_students(
            event_id=request.GET.get('event', None),
            flow_id=request.GET.get('flow', None),
            tariff_id=request.GET.get('tariff', None),
            promocode_id=request.GET.get('promocode', None),
            sorted=request.GET.get('sorted', None),
        )


class CloseAccessToCourse(APIView):

    @obj_or_error
    def post(self, request, id_school, id_student, **kwargs):
        pass

class SchoolFlowList(APIView):
    """
    Список потоков школы
    """

    @obj_or_error
    def get(self, request):
        return ServicesSchool(request,
                              request.current_user.school).list_flows()


class OrdersStudentList(APIView):

    @obj_or_error
    def get(self, request, student_id):
        params = {'student_id': student_id}
        event_id = request.GET.get('event', None)
        if event_id:
            params['event_id'] = event_id
        return OrderSmallSerializer(
            Order.objects.filter(**params),
            many=True).data


class CertificatesStudentList(ListAPIView):
    """
        API метод список сертификатов студента.
    """
    queryset = StudentCertificate.objects.all()
    serializer_class = StudentCertificateSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return StudentCertificate.objects.filter(
            student_id=self.kwargs['student_id'],
        )


class CertificateStudent(RetrieveAPIView):
    queryset = StudentCertificate.objects.all()
    serializer_class = StudentCertificateSerializer
    permission_classes = [IsAuthenticated]


class LoginAsStudent(APIView):
    permission_classes = [IsAuthenticated]

    @obj_or_error
    def post(self, request, student_id):
        user = get_object_or_404(User, id=student_id)
        if not request.current_user.is_superuser:
            raise AccessError('You not superuser!')

        return AccountService.json_response(self.request, user,
                                            flagCreatedUser=False)


class SchoolCatalogVideoListApiView(ListAPIView):
    queryset = VideoCatalog.objects.filter(is_deleted=False, published=True)
    serializer_class = VideoCatalogPreviewSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return VideoCatalog.objects.filter(
            is_deleted=False,
            published=True,
            user__school__id=self.kwargs['id_school'])


class SchoolCatalogVideoRetrieveApiView(RetrieveAPIView):
    queryset = VideoCatalog.objects.filter(is_deleted=False, published=True)
    serializer_class = VideoCatalogSerializer
    permission_classes = [IsAuthenticated]

    def get_object(self):
        return get_object_or_404(VideoCatalog,
                                 id=self.kwargs['pk'],
                                 is_deleted=False,
                                 published=True,
                                 user__school__id=self.kwargs['id_school'])

    @obj_or_error
    def retrieve(self, request, *args, **kwargs):
        data = VideoCatalogSerializer(
                          self.get_object(),
                          context={
                              "request": request,
                          }).data
        return Response(data if data['is_access'] == True else {'id': kwargs['pk'],
                                                                'is_access': False})


class SchoolCatalogVideoTariffsApiView(APIView):
    permission_classes = [IsAuthenticated]
    kwargs_filter = {
        'video__is_deleted': False,
        'video__published': True
    }

    def get_filter(self, **kwargs: object) -> dict:
        return {'video_id': self.kwargs['pk'], **self.kwargs_filter, **kwargs}

    def get_access_courses(self):
        return list(AccessVideoCourse
                    .objects
                    .filter(**self.get_filter(course__is_deleted=False))
                    .values('course_id', 'course__title_en')
                    )

    def get_access_tariffs(self):
        return list(AccessVideoTariff
                    .objects
                    .filter(**self.get_filter(tariff__is_deleted=False))
                    .select_related('tariff__event_id', 'tariff__event__title_en')
                    .values('tariff_id', 'tariff__title',
                            'tariff__event_id', 'tariff__event__title_en')
                    )

    def get_list_objects(self):
        result = self.get_access_courses() + self.get_access_tariffs()

        for item in result:
            if 'tariff__event_id' in item:
                item['course_id'] = item['tariff__event_id']
                item.pop('tariff__event_id')
            if 'tariff__event__title_en' in item:
                item['course__title'] = item['tariff__event__title_en']
                item.pop('tariff__event__title_en')
            if 'event_id' in item:
                item['course_id'] = item['event_id']
                item.pop('event_id')
            if 'course__title_en' in item:
                item['course__title'] = item['course__title_en']
                item.pop('course__title_en')
        return result

    def get_tree(self):
        result = []
        list_objects = self.get_list_objects()
        print(list_objects)

        write_courses = []
        for record in list_objects:
            if record['course_id'] not in write_courses:
                result.append({'course': {
                        'id': record['course_id'],
                        'title': record['course__title']
                        }
                   })
                write_courses.append(record['course_id'])

        for record_result in result:
            tariffs = []
            index = 0
            for item in list_objects:
                if 'tariff_id' in item:
                    if record_result['course']['id'] == item['course_id']:
                        tariffs.append(
                            {'tariff': {
                                    'id': item['tariff_id'],
                                    'title': item['tariff__title']
                                }}
                            )
                    else:
                        index += 1
                record_result['course']['tariff'] = tariffs
        return result

    @obj_or_error
    def get(self, request, *args, **kwargs):
        self.kwargs = kwargs
        return self.get_tree()
