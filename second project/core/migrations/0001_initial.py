# Generated by Django 3.0.2 on 2020-12-07 11:12

import colorfield.fields
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import phonenumber_field.modelfields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('events', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Email',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.PositiveSmallIntegerField(choices=[(0, 'Password reset'), (1, 'Invite'), (2, 'Active account')], verbose_name='код')),
                ('name', models.CharField(max_length=255, verbose_name='название')),
                ('name_ru', models.CharField(max_length=255, null=True, verbose_name='название')),
                ('name_en', models.CharField(max_length=255, null=True, verbose_name='название')),
                ('subject', models.CharField(max_length=255, verbose_name='заголовок email')),
                ('subject_ru', models.CharField(max_length=255, null=True, verbose_name='заголовок email')),
                ('subject_en', models.CharField(max_length=255, null=True, verbose_name='заголовок email')),
                ('txt', models.TextField(blank=True, default='', verbose_name='текст')),
                ('txt_ru', models.TextField(blank=True, default='', null=True, verbose_name='текст')),
                ('txt_en', models.TextField(blank=True, default='', null=True, verbose_name='текст')),
                ('html', models.TextField(blank=True, default='', verbose_name='html')),
                ('is_active', models.BooleanField(default=True)),
            ],
            options={
                'verbose_name': 'Email сообщение',
                'verbose_name_plural': 'Email сообщения',
                'ordering': ['code'],
            },
        ),
        migrations.CreateModel(
            name='Emailer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('email_to', models.EmailField(max_length=100)),
                ('email_from', models.EmailField(max_length=100)),
                ('subject', models.CharField(max_length=255, verbose_name='заголовок')),
                ('txt', models.TextField(verbose_name='текст')),
                ('html', models.TextField(verbose_name='html')),
                ('attachment', models.CharField(blank=True, default=None, max_length=255, null=True)),
                ('report', models.TextField(default='', verbose_name='отчет')),
                ('sent', models.BooleanField(default=False, verbose_name='отправлен')),
                ('result', models.BooleanField(default=False, verbose_name='результат')),
                ('dt_created', models.DateTimeField(auto_now_add=True, verbose_name='дата')),
                ('dt_sent', models.DateTimeField(blank=True, null=True, verbose_name='дата отправки')),
            ],
            options={
                'verbose_name': 'Email отчет',
                'verbose_name_plural': 'Email отчеты',
                'ordering': ['-dt_created'],
            },
        ),
        migrations.CreateModel(
            name='FacadeLogging',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('message', models.TextField(default='')),
                ('module', models.CharField(default='', max_length=100)),
                ('row', models.PositiveSmallIntegerField(default=0, verbose_name='row')),
                ('reading', models.BooleanField(default=False, verbose_name='reading')),
                ('dt', models.DateTimeField(auto_now_add=True, verbose_name='date')),
            ],
            options={
                'verbose_name': 'FacadeLogging',
                'verbose_name_plural': 'FacadeLogging',
                'ordering': ['-dt'],
            },
        ),
        migrations.CreateModel(
            name='School',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=100, verbose_name='Наименование')),
                ('description', models.TextField(blank=True, null=True, verbose_name='Описание')),
                ('domain_name', models.CharField(blank=True, max_length=255, null=True, verbose_name='Доменное имя')),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='school', to=settings.AUTH_USER_MODEL, verbose_name='Владелец')),
            ],
        ),
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('utm_source', models.CharField(blank=True, max_length=191, null=True)),
                ('utm_compain', models.CharField(blank=True, max_length=191, null=True)),
                ('utm_content', models.CharField(blank=True, max_length=191, null=True)),
                ('id_bot_telegram', models.CharField(blank=True, max_length=191, null=True, verbose_name='Id бота telegram')),
                ('id_bot_vk', models.CharField(blank=True, max_length=191, null=True, verbose_name='Id бота vk')),
                ('id_bot_viber', models.CharField(blank=True, max_length=191, null=True, verbose_name='Id бота viber')),
                ('id_bot_facebook', models.CharField(blank=True, max_length=191, null=True, verbose_name='Id бота facebook')),
                ('id_facebook', models.CharField(blank=True, max_length=250, null=True, verbose_name='Id пользователя facebook')),
                ('id_vk', models.CharField(blank=True, max_length=250, null=True, verbose_name='Id пользователя vk')),
                ('id_google', models.CharField(blank=True, max_length=250, null=True, verbose_name='Id пользователя google')),
                ('is_enabled', models.BooleanField(default=False, verbose_name='Профиль включен')),
                ('url_pic_facebook', models.URLField(blank=True, max_length=255, null=True, verbose_name='Аватар с facebook')),
                ('url_pic_vk', models.URLField(blank=True, max_length=255, null=True, verbose_name='Аватар с vk')),
                ('url_pic_google', models.URLField(blank=True, max_length=255, null=True, verbose_name='Аватар с google')),
                ('phone', phonenumber_field.modelfields.PhoneNumberField(blank=True, max_length=128, null=True, region=None)),
                ('design_configured', models.BooleanField(default=False, verbose_name='Флаг настройки дизайна')),
                ('school_configured', models.BooleanField(default=False, verbose_name='Флаг настройки школы')),
                ('avatar', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='events.ImageStorage', verbose_name='Аватар')),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='profile', to=settings.AUTH_USER_MODEL, verbose_name='Профиль пользователя')),
            ],
        ),
        migrations.CreateModel(
            name='Design',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('logo_is_text', models.BooleanField(default=False, verbose_name='Логотип текстовый')),
                ('logo_txt', models.CharField(blank=True, max_length=255, null=True, verbose_name='Текст логотипа')),
                ('font_logo', models.CharField(blank=True, max_length=255, null=True, verbose_name='Шрифт логотипа')),
                ('url_font_logo', models.URLField(blank=True, max_length=255, null=True, verbose_name='Url шрифта логотипа')),
                ('color_logo', colorfield.fields.ColorField(default='#727171', max_length=18, verbose_name='Цвет текста логотипа')),
                ('size_font', models.PositiveSmallIntegerField(default=16, verbose_name='размер шрифта')),
                ('font_head', models.CharField(blank=True, max_length=255, null=True, verbose_name='Шрифт заголовка')),
                ('url_font_head', models.URLField(blank=True, max_length=255, null=True, verbose_name='Url шрифта заголовка')),
                ('font_menu', models.CharField(blank=True, max_length=255, null=True, verbose_name='Шрифт меню')),
                ('font_base_text', models.CharField(blank=True, max_length=255, null=True, verbose_name='Шрифт текста на странице')),
                ('url_font_base_text', models.URLField(blank=True, max_length=255, null=True, verbose_name='Url шрифта текста на странице')),
                ('color_bg_base', colorfield.fields.ColorField(default='#FFFFFF', max_length=18, verbose_name='Цвет основного фона')),
                ('color_text_base', colorfield.fields.ColorField(default='#727171', max_length=18, verbose_name='Цвет основного текста')),
                ('color_bg_menu', colorfield.fields.ColorField(default='#С4С4С4', max_length=18, verbose_name='Цвет фона меню')),
                ('color_text_menu', colorfield.fields.ColorField(default='#000000', max_length=18, verbose_name='Цвет ')),
                ('color_headers', colorfield.fields.ColorField(default='#000000', max_length=18, verbose_name='Цвет заголовков')),
                ('color_bg_button', colorfield.fields.ColorField(default='#000000', max_length=18, verbose_name='Цвет фона кнопки')),
                ('color_text_button', colorfield.fields.ColorField(default='#FFFFFF', max_length=18, verbose_name='Цвет текста кнопки')),
                ('logo', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='designs', to='events.ImageStorage', verbose_name='Хранилище изображения')),
                ('school', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='design', to='core.School', verbose_name='Школа')),
            ],
            options={
                'verbose_name': 'Дизайн школы',
                'verbose_name_plural': 'Дизайны школ',
            },
        ),
        migrations.CreateModel(
            name='AuthPhoneCode',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('value', models.PositiveIntegerField(verbose_name='Временный код авторизации через тел. номер')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('update_at', models.DateTimeField(auto_now=True)),
                ('profile', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='phone_code', to='core.Profile')),
            ],
        ),
    ]
