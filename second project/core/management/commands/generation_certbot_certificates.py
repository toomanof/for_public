from django.core.management.base import BaseCommand
from django_extensions.management.utils import signalcommand

from core.services import ServiceNginx


class Command(BaseCommand):

    @signalcommand
    def handle(self, *args, **options):
        ServiceNginx().generation_certbot_certificates()
