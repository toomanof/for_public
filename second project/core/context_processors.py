from .services.errors import FacadeLogging
from django.utils import timezone


def facade_logging(request):
    FacadeLogging.show(request)
    return {'now':timezone.now()}
