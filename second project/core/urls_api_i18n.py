from django.urls import path, include
from rest_framework.routers import DefaultRouter

from core.views import SchoolCatalogVideoTariffsApiView, NoteViewSet

app_name = 'api_core_i18n'
router = DefaultRouter()


urlpatterns = [

    ]

router.register(r'school/(?P<id_school>\d+)/students/(?P<id_student>\d+)/notes',
                NoteViewSet, basename='notes_view_set')

urlpatterns += router.urls
