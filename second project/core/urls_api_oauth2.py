from django.urls import path

from .views import (
    applications, GoogleLogin, FaceBookLogin, VKLogin,
    VerificationGoogleAccount, VerificationFaceBookAccount,
    VerificationVKAccount)
app_name = 'api_oauth2'

urlpatterns = [
    path('apps/', applications),
    path('gglogin/', GoogleLogin.as_view(), name='google_login'),
    path('fblogin/', FaceBookLogin.as_view(), name='facebook_login'),
    path('vklogin/', VKLogin.as_view(), name='vk_login'),

path('verification/account/google/',
     VerificationGoogleAccount.as_view(),
     name='verification_google_account'),
path('verification/account/facebook/',
     VerificationFaceBookAccount.as_view(),
     name='verification_facebook_account'),
path('verification/account/vk/',
     VerificationVKAccount.as_view(),
     name='verification_vk_acount'),
]
