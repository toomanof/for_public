from modeltranslation.translator import register, TranslationOptions
from .models import Email


@register(Email)
class EmailTranslationOptions(TranslationOptions):
    fields = ('name', 'subject', 'txt', )
