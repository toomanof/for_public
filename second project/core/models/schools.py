from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver


class School(models.Model):
    ST_WAIT_RECORD_A, ST_WAIT_CERTIFICATION, ST_WORK, ST_WAIT_CREATE_NGINX_CONFIG = range(4)
    CHOICE_STATUS_DOMAIN = (
        (ST_WAIT_RECORD_A, 'Ожидание установки записи А'),
        (ST_WAIT_CREATE_NGINX_CONFIG, 'Ожидание создания конфиг файла nginx'),
        (ST_WAIT_CERTIFICATION, 'Ожидание выпуска сертификата'),
        (ST_WORK, 'В работе')
    )

    user = models.OneToOneField(
        User, on_delete=models.CASCADE, related_name='school',
        verbose_name='Владелец')
    title = models.CharField('Наименование',
                             max_length=100, blank=False, null=False)
    description = models.TextField('Описание', blank=True, null=True)
    domain_name = models.CharField(
        max_length=255, blank=True, verbose_name='Доменное имя', null=True)
    status_domain = models.SmallIntegerField(
        'Статус домен', choices=CHOICE_STATUS_DOMAIN, default=ST_WAIT_RECORD_A)
    custom_domain = models.BooleanField('Доменное имя стороннее',
                                        default=True)

    def __init__(self, *args, **kwargs):
        super(School, self).__init__(*args, **kwargs)
        self.old_custom_domain = self.custom_domain

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        if self.old_custom_domain != self.custom_domain and self.custom_domain:
            self.status_domain = School.ST_WORK

    def __str__(self):
        return f'{self.title} ({self.domain_name})'


@receiver(post_save, sender=User)
def create_design_event(sender, instance, created, **kwargs):
    School.objects.get_or_create(user=instance)
