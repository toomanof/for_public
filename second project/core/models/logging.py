from django.db.models import Model
from django.db.models import CharField
from django.db.models import TextField
from django.db.models import DateTimeField
from django.db.models import BooleanField
from django.db.models import PositiveSmallIntegerField


class FacadeLogging(Model):
    '''
    Модель содержащая сообщения с приложений.
    '''
    message = TextField(default='')
    module = CharField(max_length=100, default='')
    row = PositiveSmallIntegerField(verbose_name='row', default=0)
    reading = BooleanField(verbose_name='reading', default=False)
    dt = DateTimeField(verbose_name='date', auto_now_add=True)

    class Meta:
        verbose_name = 'FacadeLogging'
        verbose_name_plural = 'FacadeLogging'
        ordering = ['-dt']

    def __str__(self):
        return f'{self.dt.strftime("%d %m %Y %H:%M:%S")} "{self.message}" {self.module} in {self.row}'
