from django.core.exceptions import FieldDoesNotExist
from django.db import models
from django.db.models import Max, Min
from django.utils import timezone


class NotRemoveRecordManager(models.Manager):

    def get_queryset(self):
        queryset = super(NotRemoveRecordManager, self).get_queryset()

        try:
            self.model._meta.get_field('order')
        except FieldDoesNotExist:
            return queryset
        else:
            return queryset.order_by('order')

    def all_not_delete(self):
        return self.get_queryset().filter(is_deleted=False)

    def all_active(self):
        try:
            self.model._meta.get_field('published')
        except FieldDoesNotExist:
            return self.all_not_delete()
        else:
            return self.all_not_delete().filter(published=True)

    def get_max_order(self):
        result = super().get_queryset().aggregate(Max('order'))['order__max']
        return result if result else 0

    def get_min_order(self):
        result = super().get_queryset().aggregate(Min('order'))['order__min']
        return result if result else 0

    def get_max_order_element(self):
        return super().all_active().filter(
            order=self.get_max_order()).first()

    def get_min_order_element(self):
        return super().all_active().filter(
            order=self.get_min_order()).first()

    def next_order(self):
        max_item = self.get_max_order()
        if isinstance(max_item, int):
            return max_item + 1
        else:
            return 0


class AbstractNotRemoveModel(models.Model):
    """
    Абстрактный класс с полем флага удаление записи
    Все записи моделей физически не удаляются,
    а выставляется флаг(признак) удления
    """

    is_deleted = models.BooleanField(
        default=False, verbose_name='Флаг удаления')
    date_delete = models.DateTimeField(
        'Время удаление записи', blank=True, null=True)
    objects = NotRemoveRecordManager()

    def delete(self, *args, **kwargs):
        self.is_deleted = True
        self.date_delete = timezone.now()
        self.save(update_fields=['is_deleted'])

    class Meta:
        abstract = True


class AbstractModelWithTitle(models.Model):
    """
    Абстрактный класс с полями наименование и веса записи.
    """
    title = models.CharField(
        max_length=100, blank=False, null=False, verbose_name='Наименование')
    order = models.IntegerField(
        default=0, blank=False, null=False, verbose_name='Вес', unique=True)

    def __str__(self):
        return f"{self.title}"

    class Meta:
        abstract = True
        ordering = ['-order']


class VideoMixin(models.Model):
#    SOURCE_YOUTUBE, SOURCE_VIMEO, SOURCE_RTC = range(1, 4)
#    CHOICES_SOURCE_VIDEO = (
#        (SOURCE_YOUTUBE, 'yuotube'),
#        (SOURCE_VIMEO, 'vimeo'),
#        (SOURCE_RTC, 'rtc'),
#    )
#    video_source = models.PositiveSmallIntegerField(
#        'Источник видео', choices=CHOICES_SOURCE_VIDEO,
#        default=SOURCE_YOUTUBE, blank=True, null=True)
    url = models.URLField('Url video', blank=True, null=True)

    class Meta:
        abstract = True
