from django.conf import settings
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

from colorfield.fields import ColorField

from .abstract_models import AbstractNotRemoveModel
from .schools import School
from events.models import ImageStorage

class Design(models.Model):
    school = models.OneToOneField(
        School, on_delete=models.CASCADE,
        related_name='design', verbose_name='Школа')
    logo = models.ForeignKey(
        ImageStorage, on_delete=models.CASCADE,
        related_name='designs', verbose_name='Хранилище изображения',
        null=True, blank=True)
    logo_is_text = models.BooleanField('Логотип текстовый', default=False)
    logo_txt = models.CharField('Текст логотипа', max_length=255,
                                null=True, blank=True)
    font_logo = models.CharField('Шрифт логотипа', max_length=255,
                                 null=True, blank=True)
    url_font_logo = models.URLField('Url шрифта логотипа', max_length=255,
                                    null=True, blank=True)
    color_logo = ColorField('Цвет текста логотипа',
                               default=settings.DEFAULT_COLOR_LOGO)
    size_font = models.PositiveSmallIntegerField(
        'размер шрифта', default=settings.DEFAULT_SIZE_FONT)
    font_head = models.CharField('Шрифт заголовка', max_length=255,
                                 null=True, blank=True)
    url_font_head = models.URLField('Url шрифта заголовка', max_length=255,
                                    null=True, blank=True)
    font_menu = models.CharField('Шрифт меню', max_length=255,
                                 null=True, blank=True)
    font_base_text = models.CharField(
        'Шрифт текста на странице', max_length=255, null=True, blank=True)
    url_font_base_text = models.URLField(
        'Url шрифта текста на странице', max_length=255, null=True, blank=True)
    color_bg_base = ColorField('Цвет основного фона',
                               default=settings.DEFAULT_COLOR_BG_BASE)
    color_text_base = ColorField('Цвет основного текста',
                                 default=settings.DEFAULT_COLOR_TEXT_BASE)
    color_bg_menu = ColorField('Цвет фона меню',
                               default=settings.DEFAULT_COLOR_BG_MENU)
    color_text_menu = ColorField('Цвет ',
                                 default=settings.DEFAULT_COLOR_TEXT_MENU)
    color_headers = ColorField('Цвет заголовков',
                               default=settings.DEFAULT_COLOR_HEADERS)
    color_bg_button = ColorField('Цвет фона кнопки',
                                 default=settings.DEFAULT_COLOR_BG_BUTTON)
    color_text_button = ColorField('Цвет текста кнопки',
                                   default=settings.DEFAULT_TEXT_BUTTON)

    class Meta:
        verbose_name = 'Дизайн школы'
        verbose_name_plural = 'Дизайны школ'


@receiver(post_save, sender=School)
def create_design_event(sender, instance, created, **kwargs):
    Design.objects.get_or_create(school=instance)
