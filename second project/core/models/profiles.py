from datetime import timedelta
from random import randint
from django.conf import settings
from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone
from phonenumber_field.modelfields import PhoneNumberField

from events.models import ImageStorage


class Profile(models.Model):

    user = models.OneToOneField(
        User, on_delete=models.CASCADE, related_name='profile',
        verbose_name='Профиль пользователя')
    utm_source = models.CharField(max_length=191, null=True, blank=True)
    utm_compain = models.CharField(max_length=191, null=True, blank=True)
    utm_content = models.CharField(max_length=191, null=True, blank=True)
    id_bot_telegram = models.CharField(
        max_length=191, null=True, blank=True,
        verbose_name='Id бота telegram')
    id_bot_vk = models.CharField(
        max_length=191, null=True, blank=True, verbose_name='Id бота vk')
    id_bot_viber = models.CharField(
        max_length=191, null=True, blank=True,
        verbose_name='Id бота viber')
    id_bot_facebook = models.CharField(
        max_length=191, null=True, blank=True,
        verbose_name='Id бота facebook')
    id_facebook = models.CharField(
        max_length=250, null=True, blank=True,
        verbose_name='Id пользователя facebook')
    id_vk = models.CharField(
        max_length=250, null=True, blank=True,
        verbose_name='Id пользователя vk')
    id_google = models.CharField(
        max_length=250, null=True, blank=True,
        verbose_name='Id пользователя google')
    is_enabled = models.BooleanField(default=False,
        verbose_name='Профиль включен')
    avatar = models.ForeignKey(
        ImageStorage, on_delete=models.CASCADE, verbose_name='Аватар',
         blank=True, null=True,)
    url_pic_facebook = models.URLField(
        'Аватар с facebook', null=True, blank=True, max_length=255)
    url_pic_vk = models.URLField(
        'Аватар с vk', null=True, blank=True, max_length=255)
    url_pic_google = models.URLField(
        'Аватар с google', null=True, blank=True, max_length=255)
    phone = PhoneNumberField(blank=True, null=True)
    design_configured = models.BooleanField("Флаг настройки дизайна",
                                            default=False)
    school_configured = models.BooleanField("Флаг настройки школы",
                                            default=False)

    def set_new_auth_phone_code(self, new_code):
        """
        Создаётся новый временный код авторизации по телефонному номеру.
        Сохраняет его в базе и возвращает его значение.
        """
        if not hasattr(self, 'phone_code'):
            AuthPhoneCode(profile=self, value=new_code).save()
        else:
            self.phone_code.value = new_code
            self.phone_code.save()

        return self.phone_code

    def get_saved_auth_phone_code(self):
        """
        Возвращает сохранённый в базе временный код авторизации
        по телефонному номеру и признак генерации нового кода.
        """
        if hasattr(self, 'phone_code') and self.phone_code.is_active:
            return self.phone_code, False
        else:
            new_code = randint(settings.MIN_VALUE_PHONE_CODE,
                               settings.MAX_VALUE_PHONE_CODE)
            return self.set_new_auth_phone_code(new_code), True

    def full_name(self):
        return f'{self.user.last_name} {self.user.first_name}'


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    Profile.objects.get_or_create(user=instance)


class AuthPhoneCode(models.Model):
    """
    Модель для хранения временных кодов авторизации по телефонному номеру
    """
    profile = models.OneToOneField(
        Profile, on_delete=models.CASCADE, related_name='phone_code',
        )
    value = models.PositiveIntegerField(
        'Временный код авторизации через тел. номер')
    created_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.value)

    @property
    def is_active(self):
        """
        Проверяет время жизни временный код авторизации
        по телефонному номеру.
        Время жизни составляет время создания + установленный
        в настройках период
        """
        return timezone.now() <= self.expiry

    @property
    def is_active_checking(self):
        """
        Проверяет время проверки временный код авторизации
        по телефонному номеру.
        Время проверки составляет время создания + установленный
        в настройках период
        """
        return timezone.now() <= self.expiry_checking



    @property
    def expiry(self):
        """
        Время конца жизни кода
        """
        delta_time = timedelta(seconds=settings.LIFETIME_AUTH_PHONE_CODE)
        return self.update_at + delta_time

    @property
    def expiry_checking(self):
        """
        Время конца проверки кода
        """
        delta_time = timedelta(seconds=settings.LIFETIME_CHECK_AUTH_PHONE_CODE)
        return self.update_at + delta_time
    
