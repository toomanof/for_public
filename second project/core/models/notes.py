from django.db import models
from django.contrib.auth.models import User
from core.models import School


class Note(models.Model):
    body = models.TextField('Текс заметки')
    student = models.ForeignKey(User, on_delete=models.CASCADE,
                                related_name='notes', verbose_name='Студент')
    school = models.ForeignKey(School, on_delete=models.CASCADE,
                               related_name='student_notes', verbose_name='Школа')
    date_create = models.DateTimeField('Дата создание записи',
                                       auto_now_add=True)
    date_update = models.DateTimeField('Дата обновление записи',
                                       auto_now=True)

    class Meta:
        ordering =('-date_create',)
