from rest_framework import serializers

from core.models import Design


class DesignSerializer(serializers.ModelSerializer):

    class Meta:
        model = Design
        fields = '__all__'
        depth = 1
