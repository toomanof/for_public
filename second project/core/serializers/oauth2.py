from rest_framework import serializers
from oauth2_provider.models import Application
from oauth2_provider.models import get_application_model


class OAuth2AppsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Application
        fields = '__all__'
