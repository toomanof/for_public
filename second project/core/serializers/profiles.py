from django.contrib.auth import password_validation
from django.contrib.auth import authenticate
from django.contrib.auth.models import AnonymousUser, User
from django.utils.translation import gettext_lazy as _

from rest_framework import serializers

from core.models import Profile


User._meta.get_field('email')._unique = True


class UserSmallInfoSerializer(serializers.ModelSerializer):
    profile = serializers.SerializerMethodField()
    class Meta:
        model = User
        fields = ['pk', 'username', 'email', 'first_name', 'last_name', 'profile']

    def get_profile(self, element):
        return {
            'id': element.profile.id,
            'phone': str(element.profile.phone),
            'user': element.id
        }

class UserSerializer(serializers.ModelSerializer):
    role = serializers.SerializerMethodField('get_role')
    domains = serializers.SerializerMethodField('get_domains')

    class Meta:
        model = User
        fields = ['pk', 'username', 'email', 'is_staff', 'role',
                  'last_login', 'first_name', 'last_name', 'is_superuser',
                  'is_active', 'date_joined', 'groups', 'user_permissions',
                  'profile', 'domains']
        depth = 1

    def get_role(self, element):
        return 'ADMIN'

    def get_domains(self, element):
        from events.models import Event
        result = []
        if not isinstance(element, AnonymousUser):
            school_domain = element.school.domain_name
            if school_domain:
                result.append(school_domain)

            result += list(Event.objects.filter(
                owner=element,
                published=True,
                is_deleted=False
            ).values_list('owner__school__domain_name'))
        return result

    def validate(self, data):
        """
        Валидация производится с учётом полей профиля.
        Т.е. в данные добавляется словарь profile с данными
        совпадающимми с полями Profile
        """
        profile_fields = [field.name for field in Profile._meta.get_fields()]
        self.profile_data = {
            key: val for key, val in self.initial_data.items()\
            if key in profile_fields
        }
        return data

    def save(self, user_pk, **kwargs):
        """
        Сохранение производится как записи модели User так
        и связанной записи модели Profile
        """
        from core.services import AccountService

        User.objects.filter(pk=user_pk).update(**self.validated_data)
        user = User.objects.get(pk=user_pk)

        if hasattr(self, 'profile_data'):
            Profile.objects.filter(pk=user.profile.pk).update(**self.profile_data)
        return user


class RegisterSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'password')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        user = User.objects.create_user(
            validated_data['username'],
            validated_data['email'],
            validated_data['password']
        )
        return user


class LoginSerializer(serializers.Serializer):
    username = serializers.CharField(required=False)
    email = serializers.EmailField(required=False)
    password = serializers.CharField()

    def validate(self, data):
        user = (
            authenticate(**data) or User.objects.filter(
                email=data['username'], password=data['password']).first())
        if user and user.is_active:
            return user
        raise serializers.ValidationError("Incorrect Credentials")


class ProfileSerializer(serializers.ModelSerializer):
    email = serializers.SerializerMethodField('get_email')
    first_name = serializers.SerializerMethodField('get_first_name')
    last_name = serializers.SerializerMethodField('get_last_name')
    courses = serializers.SerializerMethodField('get_courses')

    class Meta:
        model = Profile
        fields = '__all__'

    def get_email(self, element):
        return element.user.email

    def get_first_name(self, element):
        return element.user.first_name

    def get_last_name(self, element):
        return element.user.last_name

    def get_courses(self, element):
        events = element.user.events.all_active().values('pk', 'title')
        for event in events:
            event.update({'payment':False, 'percent_completion':0, 'points': 0})
        return events


class ChangePasswordSerializer(serializers.Serializer):
    old_password = serializers.CharField(max_length=128, write_only=True, required=True)
    new_password = serializers.CharField(max_length=128, write_only=True, required=True)
    new_password1 = serializers.CharField(max_length=128, write_only=True, required=True)

    def __init__(self, *args, **kwargs):
        from core.services import AccountService
        super().__init__(*args, **kwargs)
        self.user = self.context['request'].current_user

    def validate_old_password(self, value):
        if not self.user.check_password(value):
            raise serializers.ValidationError(
                _('Your old password was entered incorrectly. Please enter it again.')
            )
        return value

    def validate(self, data):
        if data['new_password'] != data['new_password1']:
            raise serializers.ValidationError({'new_password1': _("The two password fields didn't match.")})
        password_validation.validate_password(data['new_password'], self.user)
        return data

    def save(self, **kwargs):
        password = self.validated_data['new_password']
        self.user.set_password(password)
        self.user.save()
        return self.user