from rest_framework import serializers

from core.models import School


class SchoolSerializer(serializers.ModelSerializer):

    class Meta:
        model = School
        fields = '__all__'
        depth = 1


class SchoolSmallSerializer(serializers.ModelSerializer):

    class Meta:
        model = School
        fields = ('id', 'title', 'description', 'domain_name', 'custom_domain')
