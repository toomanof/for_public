from django.contrib import admin

from .models import *


@admin.register(HooksSubscribers)
class HooksSubscribersAdmin(admin.ModelAdmin):
    model = HooksSubscribers
    list_display = ('user', 'type_hook', 'methods', 'url', 'is_deleted')
    list_filter = ('user', 'type_hook', 'methods', 'is_deleted')


@admin.register(RequestQueue)
class RequestQueueAdmin(admin.ModelAdmin):
    model = RequestQueue
    list_display = ('user', 'type_hook', 'methods', 'url', 'status',
                    'date_create', 'date_update')
    list_filter = ('user', 'type_hook', 'methods', 'status')
