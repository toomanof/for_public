import json

from django.conf import settings
from django.utils.module_loading import import_string

from hooks.models import HooksSubscribers, RequestQueue
from hooks.services.constants import(
    LIST_HOOKS_VARIABLES_SOURCE_REQUEST, LIST_HOOKS_VARIABLES_SOURCE_COURSE)


class Hook:
    type = None

    def __init__(self, request):
        self.request = request

    def execute(self, type_hook, **kwargs):
        if self.type == type_hook:
            self.handler(**kwargs)

    def handler(self, **kwargs):
        for hook in self.select_subscriber_hooks(**kwargs):
            if hook:
                new_body = self.parser_request_body(
                    json.dumps(hook.body_request),
                    ** kwargs
                )
                self.hook_to_queue(hook, json.loads(new_body))

    def parser_body(self, body, dict_replacements, **kwargs):
        new_body = body
        for key, val in dict_replacements.items():
            if key in body:
                new = val(self.request, **kwargs)
                new_body = new_body.replace(key, new)
        return new_body

    def parser_request_body(self, body, **kwargs):
        new_body = self.parser_body(body, LIST_HOOKS_VARIABLES_SOURCE_REQUEST, **kwargs)

        course = kwargs.get('course', None)
        if course:
            new_body = self.parser_body(new_body, LIST_HOOKS_VARIABLES_SOURCE_COURSE, **kwargs)
        return new_body

    def hook_to_queue(self, hook, new_body):
        request, _ = RequestQueue.objects.update_or_create(
            user=hook.user,
            url=hook.url,
            methods=hook.methods,
            headers=hook.headers,
            body_request=new_body,
            type_hook=hook.type_hook
        )
        request.send()

    def select_subscriber_hooks(self, **kwargs):
        filter_params = {'type_hook': self.type}
        if 'course' in kwargs:
            filter_params['user'] = kwargs['course'].owner
        return HooksSubscribers.objects.all_active().filter(**filter_params)


class HookHandler:

    @staticmethod
    def execute(request, type_hook, **kwargs):
        hooks_list = getattr(settings, 'HOOKS_LIST', [])
        for hook_path in hooks_list:
            hook = import_string(hook_path)
            hook(request).execute(type_hook, **kwargs)
