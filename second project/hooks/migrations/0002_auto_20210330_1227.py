# Generated by Django 3.0.2 on 2021-03-30 09:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hooks', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='hookssubscribers',
            name='date_delete',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Время удаление записи'),
        ),
        migrations.AddField(
            model_name='hookssubscribers',
            name='is_deleted',
            field=models.BooleanField(default=False, verbose_name='Флаг удаления'),
        ),
    ]
