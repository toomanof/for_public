# Generated by Django 3.0.2 on 2021-03-30 13:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hooks', '0002_auto_20210330_1227'),
    ]

    operations = [
        migrations.AddField(
            model_name='hookssubscribers',
            name='type_hook',
            field=models.PositiveSmallIntegerField(choices=[(0, 'Регистрация пользователя в системе'), (1, 'Создание платежа'), (2, 'Платеж прошел успешно'), (3, 'Начало обучения'), (4, 'Окончание обучения')], verbose_name='Тип хука'),
            preserve_default=False,
        ),
    ]
