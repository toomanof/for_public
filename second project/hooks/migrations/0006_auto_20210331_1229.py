# Generated by Django 3.0.2 on 2021-03-31 09:29

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hooks', '0005_auto_20210331_1228'),
    ]

    operations = [
        migrations.RenameField(
            model_name='hookssubscribers',
            old_name='heads',
            new_name='headers',
        ),
    ]
