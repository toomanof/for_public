from django.urls import path, include
from rest_framework.routers import DefaultRouter

from . import views

app_name = 'api_hooks'
router = DefaultRouter()

router.register('',
                views.HooksApiView, basename='hooks')

urlpatterns = [
    path('info/', views.ViewInfoHookVaraibles.as_view(),
         name="hooks_info"),
] + router.urls