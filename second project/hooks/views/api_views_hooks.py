import json
from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from core.errors import wrapper_error, AccessError
from core.services import camel_case_to_under_case, RoleUser
from hooks.services.constants import LIST_HOOKS, LIST_HOOKS_VARIABLES
from hooks.models import HooksSubscribers
from hooks.serializers import HooksSubscribersSerializer


class ViewInfoHookVaraibles(APIView):

    def get(self, request, *args, **kwargs):
        """
        Перечисляются существующие хуки и их переменные
        """
        return Response({
            'hooks': dict((key, val[1]) for key, val in LIST_HOOKS.items()),
            'variables': dict((x, y) for x, y in LIST_HOOKS_VARIABLES)
        })


class HooksApiView(ModelViewSet):
    permission_classes = [IsAuthenticated]
    serializer_class = HooksSubscribersSerializer
    queryset = HooksSubscribers.objects.all_active()

    @wrapper_error
    def create(self, request, *args, **kwargs):
        if not RoleUser(request).access_school(request.current_user.school):
            raise AccessError('Access denied')
        request.data['user'] = request.current_user.id
        return super().create(request, *args, **kwargs)

    @wrapper_error
    def update(self, request, *args, **kwargs):
        if not RoleUser(request).access_school(request.current_user.school):
            raise AccessError('Access denied')
        request.data['user'] = request.current_user.id
        return super().update(request, *args, **kwargs)

    def get_queryset(self):
        return self.queryset.filter(user=self.request.current_user)

    @wrapper_error
    def list(self, request, *args, **kwargs):
        self.request = request
        return super(HooksApiView, self).list(request, *args, **kwargs)

    @wrapper_error
    def retrieve(self, request, *args, **kwargs):
        self.request = request
        return super().retrieve(request, *args, **kwargs)

    @wrapper_error
    def partial_update(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)
