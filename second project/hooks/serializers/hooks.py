import json
from rest_framework import serializers
from hooks.models import HooksSubscribers


class HooksSubscribersSerializer(serializers.ModelSerializer):

    class Meta:
        model = HooksSubscribers
        fields = '__all__'
