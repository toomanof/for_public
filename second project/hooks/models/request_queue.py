import logging
import requests
import jsonfield
from django.contrib.auth.models import User
from django.db import models


logger_error = logging.getLogger('warning')
logger_info = logging.getLogger('info')


class RequestQueue(models.Model):
    JSON_HEADER = {'content-type': 'application/json'}

    (H_REG_USER, H_CREATE_PAYMENT, H_SUCCESS_PAYMENT,
     H_START_STUDIES, H_END_STUDIES) = range(5)

    M_GET, M_POST = range(2)
    ST_PENDING, ST_SUCCESS, ST_CANCELED = range(3)

    CHOICES_METHODS = (
        (M_GET, 'GET'),
        (M_POST, 'POST')
    )

    LIST_TYPE_HOOKS = (
        (H_REG_USER, 'Регистрация пользователя в системе'),
        (H_CREATE_PAYMENT, 'Создание платежа'),
        (H_SUCCESS_PAYMENT, 'Платеж прошел успешно'),
        (H_START_STUDIES, 'Начало обучения'),
        (H_END_STUDIES, 'Окончание обучения'),
    )

    CHOICES_STATUS = (
        (ST_PENDING, 'в ожидании'),
        (ST_SUCCESS, 'доставлен'),
        (ST_CANCELED, 'отменен'),
    )

    user = models.ForeignKey(
        User, on_delete=models.CASCADE,
        related_name='requests_in_queue', verbose_name='Получатель')
    url = models.CharField(
        'url хука', max_length=254)
    methods = models.PositiveSmallIntegerField(
        'Метод хука', choices=CHOICES_METHODS, default=M_POST)
    headers = jsonfield.JSONField(
        'Заголовки запроса', default=JSON_HEADER)
    body_request = jsonfield.JSONField(
        'JSON тело запроса', null=True, blank=True)
    type_hook = models.PositiveSmallIntegerField(
        'Тип хука', choices=LIST_TYPE_HOOKS)
    status = models.PositiveSmallIntegerField(
        'Статус запроса', choices=CHOICES_STATUS, default=ST_PENDING)
    date_create = models.DateTimeField('Дата создание записи', auto_now_add=True)
    date_update = models.DateTimeField('Дата обновление записи', auto_now=True)

    class Meta:
        verbose_name = 'Запрос в очереди'
        verbose_name_plural = 'Очередь запросов'

    def send(self):
        try:
            if self.methods == self.M_GET:
                response = requests.get(self.url, params=self.body_request, headers=self.headers)
            elif self.methods == self.M_POST:
                response = requests.post(self.url, data=self.body_request,  headers=self.headers)
        except Exception as e:
            logger_error.error(f'Error in sending to {self.url} {e}')
        else:
            logger_info.info(f'send hook to {self.url} status {response.status_code}')
            if response.status_code == 200:
                self.status = self.ST_SUCCESS
                self.save()
