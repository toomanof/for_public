import jsonfield
from django.contrib.auth.models import User
from django.db import models

from core.models import AbstractNotRemoveModel


class HooksSubscribers(AbstractNotRemoveModel):
    JSON_HEADER = {'content-type': 'application/json'}
    (H_REG_USER, H_CREATE_PAYMENT, H_SUCCESS_PAYMENT,
     H_START_STUDIES, H_END_STUDIES) = range(5)

    M_GET, M_POST = range(2)

    CHOICES_METHODS = (
        (M_GET, 'GET'),
        (M_POST, 'POST')
    )

    LIST_TYPE_HOOKS = (
        (H_REG_USER, 'Регистрация пользователя в системе'),
        (H_CREATE_PAYMENT, 'Создание платежа'),
        (H_SUCCESS_PAYMENT, 'Платеж прошел успешно'),
        (H_START_STUDIES, 'Начало обучения'),
        (H_END_STUDIES, 'Окончание обучения'),
    )

    user = models.ForeignKey(
        User, on_delete=models.CASCADE,
        related_name='hooks', verbose_name='Подписчик')
    url = models.CharField(
        'url хука', max_length=254)
    methods = models.PositiveSmallIntegerField(
        'Метод хука', choices=CHOICES_METHODS, default=M_POST)
    headers = jsonfield.JSONField(
        'Заголовки запроса', default=JSON_HEADER)
    body_request = jsonfield.JSONField(
        'JSON тело запроса', null=True, blank=True)
    type_hook = models.PositiveSmallIntegerField(
        'Тип хука', choices=LIST_TYPE_HOOKS)

    class Meta:
        verbose_name = 'Подписчик на вебхуки'
        verbose_name_plural = 'Подписчики на вебхуки'

