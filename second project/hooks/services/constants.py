L_PEFIX = '($'
R_PREFIX = '$)'


def get_student_name(request, **kwargs):
    return request.current_user.profile.full_name() if request.current_user else ''


def get_student_phone(request, **kwargs):
    return str(request.current_user.profile.phone) if request.current_user else ''


def get_student_email(request, **kwargs):
    return request.current_user.email


def get_course(request, course):
    return str(course)


def get_course_is_started(request, course):
    return str(course.is_started(request.current_user))


def get_course_is_finished(request, course):
    return str(course.is_finished(request.current_user))


def add_template_prefix(variable):
    return f'{L_PEFIX}{variable}{R_PREFIX}'


JSON_HEADER = 'content-type: application/json'

H_REG_USER, H_CREATE_PAYMENT, H_SUCCESS_PAYMENT,\
    H_START_STUDIES, H_END_STUDIES = range(5)

LIST_HOOKS = {
        H_REG_USER: ('registration', 'Регистрация пользователя в системе'),
        H_CREATE_PAYMENT: ('create_payment', 'Создание платежа'),
        H_SUCCESS_PAYMENT: ('successful_payment', 'Платеж прошел успешно'),
        H_START_STUDIES: ('start_studies', 'Начало обучения'),
        H_END_STUDIES: ('end_studies', 'Окончание обучения'),
    }

LIST_TYPE_HOOKS = (
    (H_REG_USER, 'Регистрация пользователя в системе'),
    (H_CREATE_PAYMENT, 'Создание платежа'),
    (H_SUCCESS_PAYMENT, 'Платеж прошел успешно'),
    (H_START_STUDIES, 'Начало обучения'),
    (H_END_STUDIES, 'Окончание обучения'),
)

VH_STUDENT_NAME = 'student_name'
VH_STUDENT_PHONE = 'student_phone'
VH_STUDENT_EMAIL = 'student_email'
VH_COURSE = 'course'
VH_COURSE_START = 'course_start'
VH_COURSE_STOP = 'course_stop'

LIST_HOOKS_VARIABLES = (
    (add_template_prefix(VH_STUDENT_NAME), 'Фамилия Имя текущего пользователя '),
    (add_template_prefix(VH_STUDENT_PHONE), 'Телефон текущего пользователя '),
    (add_template_prefix(VH_STUDENT_EMAIL), 'Email текущего пользователя '),
    (add_template_prefix(VH_COURSE), 'Курс'),
    (add_template_prefix(VH_COURSE_START), 'Флаг начала курса'),
    (add_template_prefix(VH_COURSE_STOP), 'Флаг окончания курса'),
)

LIST_HOOKS_VARIABLES_SOURCE_REQUEST = {
    add_template_prefix(VH_STUDENT_NAME): get_student_name,
    add_template_prefix(VH_STUDENT_PHONE): get_student_phone,
    add_template_prefix(VH_STUDENT_EMAIL): get_student_email,
}

LIST_HOOKS_VARIABLES_SOURCE_COURSE = {
    add_template_prefix(VH_COURSE): get_course,
    add_template_prefix(VH_COURSE_START): get_course_is_started,
    add_template_prefix(VH_COURSE_STOP): get_course_is_finished,
}
