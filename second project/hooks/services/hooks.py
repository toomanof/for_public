import logging

from hooks.handlers.base import Hook, HookHandler
from hooks.services.constants import *


logger = logging.getLogger('info')


class HooksProcessing(HookHandler):
    pass


class RegistrationUserHook(Hook):
    type = H_REG_USER

    def handler(self, **kwargs):
        logger.info(f'Hook registration new user {self.request.current_user}')
        return super().handler(**kwargs)


class CreatePaymentHook(Hook):
    type = H_CREATE_PAYMENT

    def handler(self, **kwargs):
        logger.info(f'Hook create payment {self.request.current_user}, {kwargs}')
        return super().handler(**kwargs)


class SuccessfulPaymentHook(Hook):
    type = H_SUCCESS_PAYMENT

    def handler(self, **kwargs):
        logger.info(f'Hook successful payment {self.request.current_user}, {kwargs}')
        return super().handler(**kwargs)


class StartStudiesHook(Hook):
    type = H_START_STUDIES

    def handler(self, **kwargs):
        logger.info(f'Hook start student {self.request.current_user}, {kwargs}')
        return super().handler(**kwargs)


class EndStudiesHook(Hook):
    type = H_END_STUDIES

    def handler(self, **kwargs):
        logger.info(f'Hook end student {self.request.current_user}, {kwargs}')
        return super().handler(**kwargs)

