import logging

from datetime import timedelta
from django.utils import timezone

from core import celery_app
from hooks.models import RequestQueue


app = celery_app
logger = logging.getLogger('celery')


@app.task
def send_request_hooks():
    hooks = RequestQueue.objects.filter(status=RequestQueue.ST_PENDING)
    for hook in hooks:
        hook.send()


@app.task
def canceled_request_hooks():
    day_before = timezone.now() - timedelta(days=1)
    RequestQueue.objects.filter(
        status=RequestQueue.ST_PENDING,
        date_update__lte=day_before).delete()
