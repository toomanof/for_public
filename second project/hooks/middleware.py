import logging
from django.utils.deprecation import MiddlewareMixin

from core.services.users import AccountService
from django.http import HttpResponse

logger = logging.getLogger('info')


class HooksListMiddleware(MiddlewareMixin):

    def process_request(self, request):
        pass