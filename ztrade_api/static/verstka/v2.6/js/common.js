$(document).ready(function() {
	
	// Placeholder
	$('input.styler, textarea.styler').placeholder();


	// Mobile nav
	$('.mobnav').after('<div class="mobnav_overlay"></div>');
	if ($('div').is('#foottrig')) {
		var height_body = $('#foottrig').offset().top;
	}
	if ( document.documentElement.clientWidth < 1020 ) {
		$('.mobnav').height(height_body);
	}
	$('.showmenu, .showmenu_sm').on('click touchstart',function() {
		$('.mobnav').addClass('opened');
		$('.mobnav_overlay').fadeIn(100);
		return false;
	});
	$('.mobnav_overlay').on('click touchstart',function() {
		$('.mobnav').removeClass('opened');
		$('.mobnav_overlay').fadeOut(100);
		return false;
	});
	$('.mobnav_close').click(function() {
		$('.mobnav').removeClass('opened');
		$('.mobnav_overlay').fadeOut(100);
		return false;
	});


	// Unav
	$('.userleftmenu').after('<div class="userleftmenu_overlay"></div>');
	$('.unav').on('click',function() {
		$('.userleftmenu').addClass('opened');
		$('.userleftmenu_overlay').fadeIn(100);
		return false;
	});
	$('.userleftmenu_overlay').on('click touchstart',function() {
		$('.userleftmenu').removeClass('opened');
		$('.userleftmenu_overlay').fadeOut(100);
		return false;
	});
	$('.unav_close').click(function() {
		$('.userleftmenu').removeClass('opened');
		$('.userleftmenu_overlay').fadeOut(100);
		return false;
	});


	// Slider
	$('#hometopSlider').owlCarousel({
		items: 5,
		margin: 10,
		mouseDrag: false,
		dots: true,
		nav: false,
		responsive: {
			0: { items: 2 },
			800: { items: 3 },
			1020: { items: 4 },
			1100: { items: 5 }
		}
	});
	$('#homeslider_slide').owlCarousel({
		items: 1,
		margin: 0,
		mouseDrag: false,
		dots: true,
		nav: true
	});
	// Wrap around nav & dots
	$('#homeslider_slide').each(function(index) {
		$(this).find('.owl-nav, .owl-dots').wrapAll('<div class="owl-controls"></div>');
	});


	// phonemob
	$('.phonemob_del').click(function() {
		$(this).parent().remove();
		return false;
	});


	// Height chain
	var maxHeight = 0;
	$(".invest_row .module, .advbox ul").each(function () {
		var thisHeight = parseInt($(this).height());
		if (thisHeight > maxHeight) {
			maxHeight = thisHeight;
		};
	});
	$(".invest_row .module, .advbox ul").css({'min-height': maxHeight});
	

	// Spoiler
	$('.fshow').click(function() {
		$('.filternews_hide').slideToggle(200);
		$(this).toggleClass('open');
		return false;
	});
	$('.checkrow_show').click(function() {
		$('.checkrow_hide').toggleClass('open');
		$(this).toggleClass('open');
		return false;
	});


	if ( document.documentElement.clientWidth >= 600 ) {
		$('.signal_scroll').jScrollPane({
			autoReinitialise: true
		});
	}
	$('.list_scroll').jScrollPane({
		autoReinitialise: true
	});
	

	// Fancybox
	/*
	$('.js_callbackwin, .js_paywinmodal').fancybox({
		touch : false,
		closeBtn: false
	});

	// Checkbox, Radio, Select
	$('input.radio, input.checkbox, .selectbox, .alertchange, .filtersel').styler();

	// kursrow
	$('.kursrow').marquee({
		duration: 10000,
		delayBeforeStart: 0,
		direction: 'left'
	});
	*/
	

});