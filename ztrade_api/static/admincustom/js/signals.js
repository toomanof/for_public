// Script for iteracting with input result field
// On mount: add div id="resultsign" before input
// If value is positive in input: show green +
// If value is negative in input: show red -
// Watch event for changing input and apply sign.

var $ = django.jQuery;

$(document).ready(function(){
    applySign();
});

$(document).on('change', '#id_result', function() {
    applySign();
});

function applySign(){
    var value = parseInt($('#id_result').val());
    if (!value){
        return
    }

    var s;
    var el = $('label[for="id_result"]');

    if (value === 0){
        el.html = 'Результат, pips';
    } else if (value > 0){
        s = '<span style="color: #008000">Результат, pips: + ' + value + '</span>';
        el.html(s);
    } else {
        var s = '<span style="color: #ff3333">Результат, pips: ' + value + '</span>';
        el.html(s);
    }
}
