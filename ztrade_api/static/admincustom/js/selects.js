// Dependent selects for django admin

var $ = django.jQuery;

$(document).on('change', '#id_market', function() {
    marketSelectChanged($(this).val());
});

function marketSelectChanged(value){
    if (!value){
        return
    }

    var data = $('#symbol-ids').html();
    var d;

    try {
        d = $.parseJSON(data);
    } catch(err){
        return
    }

    value = parseInt(value)
    var ids = d[value];

    $('select[name="symbol"]').prop('selectedIndex',0);
    $('select[name="symbol"] option').each(function(){
        var el = $(this);
        var optionValue = parseInt(el.attr('value'))

        if ($.inArray(optionValue, ids) > -1){
            el.show();
        } else {
            el.hide();
        }
    });
}
