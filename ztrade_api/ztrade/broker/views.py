from rest_framework.generics import CreateAPIView
from rest_framework.generics import ListAPIView
from rest_framework.generics import RetrieveAPIView
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from django.db.models.query import QuerySet

from broker.serializers import CategorySerializer
from broker.serializers import BrokerSerializer
from broker.serializers import BrokerExtSerializer
from broker.serializers import DescCreateSerializer
from broker.serializers import ReviewCreateSerializer

from broker.models import Broker
from broker.models import Desc
from broker.models import Category

from web.views import AuthMixin
from web.services.helpers import get_ip_address

from django.conf import settings


class Categories(ListAPIView):
    """
    get:
        <h1>Get categories</h1>
        <b>Response:</b> list of category object<br />
        <b>Response code:</b> 200
    """
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class Brokers(ListAPIView):
    """
    get:
        <h1>Get brokers</h1>
        <b>Response:</b> list of broker object<br />
        <b>Response code:</b> 200
    """
    queryset = Broker.objects.all()
    serializer_class = BrokerSerializer
    limit = None

    def get_queryset(self):
        assert self.queryset is not None, (
            "'%s' should either include a `queryset` attribute, "
            "or override the `get_queryset()` method."
            % self.__class__.__name__
        )

        queryset = self.queryset
        if isinstance(queryset, QuerySet):
            # Ensure queryset is re-evaluated on each request.
            if hasattr(self, 'limit'):
                print(self.limit)
                queryset = queryset.all()[:self.limit]
            else:
                queryset = queryset.all()
        return queryset

    def list(self, request, version):
        if 'limit' in request.GET:
            self.limit = int(request.GET.get('limit', 0))
            print('limit:', self.limit)
        return super(ListAPIView, self).list(request)


class BrokerDetail(RetrieveAPIView):
    """
    get:
        <h1>Get broker</h1>
        <b>Response:</b> broker object<br />
        <b>Response code:</b> 200
    """
    lookup_field = 'slug'
    queryset = Broker.objects.all()
    serializer_class = BrokerExtSerializer


class BrokerDescs(AuthMixin, CreateAPIView):
    """
    post:
        <h1>Create desc for broker</h1>
        <b>Response:</b> updated broker object<br />
        <b>Response code:</b> 200
    """

    def post(self, request, **kwargs):
        ctx = {'user': request.user}
        s = DescCreateSerializer(data=request.data, context=ctx)
        s.is_valid(raise_exception=True)
        desc = s.save()

        s = BrokerExtSerializer(desc.broker)
        return Response(s.data, status=201)


class BrokerDescIncrement(AuthMixin, CreateAPIView):
    """
    post:
        <h1>Increment desc for broker</h1>
        <b>Response:</b> updated broker object<br />
        <b>Response code:</b> 200
    """

    def post(self, request, id, **kwargs):
        ip = get_ip_address(request)
        key = f'BROKER_INCREMENT_{id}'
        rs = settings.REDIS_DB
        if ip is None:
            return Response({'errors': ['Лимит на IP адрес исчерпан.']},
                            status=400)
        v = rs.get(key)
        if v is not None and v.decode("utf-8") == ip:
            return Response({'errors': ['Вы уже проголосовали.']}, status=400)

        desc = get_object_or_404(Desc, id=id)
        desc.count += 1
        desc.save()

        s = BrokerExtSerializer(desc.broker)
        rs.set(key, ip)
        rs.expire(key, 3600)
        return Response(s.data, status=201)


class BrokerReviewsCreateAuth(AuthMixin, CreateAPIView):
    """
    post:
        <h1>Create review for broker by auth user</h1>
        <b>Response:</b> updated broker object<br />
        <b>Response code:</b> 200
    """

    def post(self, request, **kwargs):
        ctx = {'user': request.user}
        s = ReviewCreateSerializer(data=request.data, context=ctx)
        s.is_valid(raise_exception=True)
        review = s.save()
        s = BrokerExtSerializer(review.broker)
        return Response(s.data, status=201)


class BrokerReviewsCreateNotAuth(CreateAPIView):
    """
    post:
        <h1>Create review for broker by not auth user</h1>
        <b>Response:</b> updated broker object<br />
        <b>Response code:</b> 200
    """

    def post(self, request, **kwargs):
        ctx = {'user': None}
        s = ReviewCreateSerializer(data=request.data, context=ctx)
        s.is_valid(raise_exception=True)
        review = s.save()

        s = BrokerExtSerializer(review.broker)
        return Response(s.data, status=201)
