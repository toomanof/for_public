from django.urls import re_path
from .import views as v

urlpatterns = [
    re_path(
        r'^(?P<version>(v1))/broker/categories$',
        v.Categories.as_view(),
        name='broker_categories'),

    re_path(
        r'^(?P<version>(v1))/broker/brokers$',
        v.Brokers.as_view(),
        name='brokers'),

    re_path(
        r'^(?P<version>(v1))/broker/brokers/(?P<slug>\w+)$',
        v.BrokerDetail.as_view(),
        name='broker'),

    re_path(
        r'^(?P<version>(v1))/broker/descs$',
        v.BrokerDescs.as_view(),
        name='broker_descs'),

    re_path(
        r'^(?P<version>(v1))/broker/descs/(?P<id>\d+)/increment$',
        v.BrokerDescIncrement.as_view(),
        name='broker_desc_increment'),

    re_path(
        r'^(?P<version>(v1))/broker/reviews/create_auth$',
        v.BrokerReviewsCreateAuth.as_view(),
        name='broker_reviews_create_auth'),

    re_path(
        r'^(?P<version>(v1))/broker/reviews/create_not_auth$',
        v.BrokerReviewsCreateNotAuth.as_view(),
        name='broker_reviews_create_not_auth'),
]
