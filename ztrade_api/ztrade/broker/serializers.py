from django.conf import settings
from django.utils import timezone
from rest_framework.serializers import ModelSerializer
from rest_framework.serializers import SerializerMethodField
from rest_framework.serializers import CharField
from rest_framework.serializers import ModelField

from broker.models import AccountType
from broker.models import AccountItem
from broker.models import Broker
from broker.models import Category
from broker.models import Desc
from broker.models import Pamm
from broker.models import PammRecommend
from broker.models import PammVideo
from broker.models import Param
from broker.models import Review

from web.models import Email
from web.models import Emailer


class BrokerSerializer(ModelSerializer):
    h1 = CharField(source='get_h1')
    title = CharField(source='get_title')
    category_ids = SerializerMethodField()

    class Meta:
        model = Broker
        fields = (
            'id',
            'guid',
            'title',
            'h1',
            'name',
            'slug',
            'is_featured',
            'rating',
            'min_deposit',
            'max_leverage',
            'spread',
            'site',
            'site_url',
            'link_open_account_with_robot',
            'link_open_account',
            'link2',
            'logo_url',
            'logo_srcset_url',
            'logo_white_url',
            'logo_white_srcset_url',
            'logo_circle_url',
            'logo_circle_srcset_url',
            'count_positive_reviews',
            'count_neutral_reviews',
            'count_negative_reviews',
            'frontend_url',
            'frontend_pamm_url',
            'category_ids'
        )

    def get_category_ids(self, obj):
        return obj.categories.values_list('id', flat=True)


class AccountTypeSerializer(ModelSerializer):
    class Meta:
        model = AccountType
        fields = (
            'id',
            'name',
        )


class AccountItemSerializer(ModelSerializer):
    account_type_id = ModelField(
        model_field=AccountItem._meta.get_field('account_type'))
    broker_id = ModelField(
        model_field=AccountItem._meta.get_field('broker'))

    class Meta:
        model = AccountItem
        fields = (
            'id',
            'account_type_id',
            'broker_id',
            'name',
            'value'
        )


class CategorySerializer(ModelSerializer):
    class Meta:
        model = Category
        fields = (
            'id',
            'name',
        )


class DescSerializer(ModelSerializer):
    broker_id = ModelField(
        model_field=Desc._meta.get_field('broker'))

    class Meta:
        model = Desc
        fields = (
            'id',
            'broker_id',
            'type',
            'text',
            'count'
        )


class DescCreateSerializer(ModelSerializer):
    broker_id = ModelField(
        model_field=Desc._meta.get_field('broker'))

    class Meta:
        model = Desc
        fields = (
            'broker_id',
            'type',
            'text',
        )

    def create(self, validated_data):
        d = validated_data
        d['user'] = self.context.get('user')
        obj = Desc.objects.create(**d)
        Emailer.objects.process_email(
            Email.NEW_BROKER_DESC_CREATED,
            email=settings.ADMIN_EMAIL,
            obj=obj)
        return obj


class PammSerializer(ModelSerializer):
    broker_id = ModelField(
        model_field=Param._meta.get_field('broker'))

    class Meta:
        model = Pamm
        fields = (
            'id',
            'broker_id',
            'first_name',
            'last_name',
            'age',
            'age_str',
            'min_deposit',
            'fee',
            'avg_month',
            'avg_year',
            'link',
            'html_code',
            'perc',
            'pic_url'
        )


class PammRecommendSerializer(ModelSerializer):
    broker_id = ModelField(
        model_field=PammRecommend._meta.get_field('broker'))

    class Meta:
        model = PammRecommend
        fields = (
            'id',
            'broker_id',
            'name',
            'url'
        )


class PammVideoSerializer(ModelSerializer):
    broker_id = ModelField(
        model_field=PammRecommend._meta.get_field('broker'))

    class Meta:
        model = PammVideo
        fields = (
            'id',
            'broker_id',
            'title',
            'link'
        )


class ParamSerializer(ModelSerializer):
    broker_id = ModelField(
        model_field=Param._meta.get_field('broker'))

    class Meta:
        model = Param
        fields = (
            'id',
            'broker_id',
            'name',
            'value',
        )


class ReviewSerializer(ModelSerializer):
    broker_id = ModelField(
        model_field=Review._meta.get_field('broker'))

    class Meta:
        model = Review
        fields = (
            'id',
            'broker_id',
            'type',
            'name',
            'text',
            'count_likes',
            'created_at',
            'title'
        )


class ReviewCreateSerializer(ModelSerializer):
    broker_id = ModelField(
        model_field=Desc._meta.get_field('broker'))

    class Meta:
        model = Review
        fields = (
            'broker_id',
            'name',
            'email',
            'type',
            'text',
        )

    def create(self, validated_data):
        d = validated_data
        d['user'] = self.context.get('user')
        d['created_at'] = timezone.now()
        obj = Review.objects.create(**d)
        Emailer.objects.process_email(
            Email.NEW_BROKER_REVIEW_CREATED,
            email=settings.ADMIN_EMAIL,
            obj=obj)
        return obj


class BrokerExtSerializer(BrokerSerializer):
    account_types = SerializerMethodField()
    account_items = SerializerMethodField()
    descs = SerializerMethodField()
    params = SerializerMethodField()
    reviews = SerializerMethodField()
    pamms = PammSerializer(many=True, read_only=True)
    pamm_recommends = PammRecommendSerializer(many=True, read_only=True)
    pamm_videos = PammVideoSerializer(many=True, read_only=True)

    class Meta(BrokerSerializer.Meta):
        model = Broker
        fields = BrokerSerializer.Meta.fields +\
            (
                'content',
                'content_pamm',
                'trade_platform',
                'regulation',
                'payout',
                'text_items_list',
                'pamm_perc',
                'pamm_pic_url',
                'pamm_link',
                'account_types',
                'account_items',
                'descs',
                'params',
                'reviews',
                'pamms',
                'pamm_recommends',
                'pamm_videos',
            )

    def get_account_types(self, obj):
        ids = AccountItem.objects\
            .filter(broker_id=obj.id)\
            .values_list('account_type_id', flat=True)
        qs = AccountType.objects.filter(id__in=ids)
        return AccountTypeSerializer(qs, many=True).data

    def get_account_items(self, obj):
        return AccountItemSerializer(obj.items.all(), many=True).data

    def get_descs(self, obj):
        return DescSerializer(obj.descs.all(), many=True).data

    def get_params(self, obj):
        return ParamSerializer(obj.params.all(), many=True).data

    def get_reviews(self, obj):
        return ReviewSerializer(obj.reviews.all(), many=True).data
