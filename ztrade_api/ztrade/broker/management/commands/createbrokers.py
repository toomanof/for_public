import os
import pandas
import random
from django.utils import timezone
from django.core.management.base import BaseCommand
from django.conf import settings

from broker.models import AccountItem
from broker.models import AccountType
from broker.models import Broker
from broker.models import Category
from broker.models import Desc
from broker.models import Param
from broker.models import PammRecommend
from broker.models import PammVideo
from broker.models import Review

from content.models import Menu

from content.management.commands.helpers import get_file_content
from web.services.helpers import splitlist


class Command(BaseCommand):
    BOOL_FIELDS = ('is_featured',)
    EXCEL_PATH = os.path.join(settings.BASE_DIR,
                              'ztrade/content/management/data/content.xlsx')

    TRADE_PLATFORM = 'MT4, MT5'
    REGULATION = 'Financial Commission, НАУФОР, НАФД, IFS'
    PAYOUT = 'Bank Transfers, Debit/Credit Cards'

    ITEMS = (
        'Семинары, обучение',
        'Низкий начальный депозит',
        'ПАММ-сервис',
        'Бонус на пополнение',
        'Доверительное управление',
        'ECN-счета',
        'Партнерская программа',
        'Широкий выбор торговых инструментов',
    )

    ADVANTAGES = (
        'Можно переводить с демо-счета всю заработанную прибыль',
        'Большой выбор способов снятия / пополнения средств',
        'Уникальные инструменты аналитики для клиентов',
        'Свежая аналитика и вебинары',
        'Бонусы 100% при первом пополнении счета',
        'Скорость исполнения ордеров 60 миллисекунд',
        'Молодая площадка ПАММ-счетов',
    )

    DISADVANTAGES = (
        'Требуется в обязательном порядке пройти верификацию',
        'На ECN-счетах высокий начальный депозит ',
        'Отсутствие центовых счетов',
    )

    CATEGORIES = (
        'Минимальный спред',
        'Бонус к депозиту',
        'Лучшие брокеры Forex',
        'Оффшорные брокеры',
        'ECN брокеры',
        'CFD брокеры',
        'Регулируемые брокеры',
        'Бездепозитный бонус',
        'Зарубежные Forex брокеры',
        'Партнерская программа',
        'Брокеры для скальпинга',
        'Для торговли советниками',
        'Брокеры FORTS',
        'С регистрацией в РФ',
        'С банковской лицензией',
    )

    ACCOUNT_TYPES = (
        'ecn.mt5',
        'pro.ecn.mt4',
        'ecn.mt4',
        'standard.mt4',
        'nano.mt4',
    )

    def get_data(self):
        path = self.EXCEL_PATH
        df = pandas.read_excel(open(path, 'rb'), sheet_name='Brokers')

        l = []
        for index, row in df.iterrows():
            rd = row.to_dict()

            for k, v in rd.items():
                if k in self.BOOL_FIELDS:
                    v = self.process_bool(v)

                if str(v).endswith('.txt'):
                    v = get_file_content(v)

                v = self.process_value(v)
                rd[k] = v

            l.append(rd)
        return l

    def process_bool(self, value):
        if pandas.isnull(value):
            return False

        if value.strip().lower() == 'y':
            return True

        return False

    def process_value(self, value):
        if pandas.isnull(value):
            return None
        return value

    def create_brokers(self):
        Broker.objects.all().delete()

        for d in self.get_data():
            d['regulation'] = self.REGULATION
            d['trade_platform'] = self.TRADE_PLATFORM
            d['payout'] = self.PAYOUT
            d['text_items'] = '\n'.join(self.ITEMS)

            d['link1'] = '/books'
            d['link2'] = 'https://www.youtube.com/embed/RNel9-r_IVM'
            b = Broker.objects.create(**d)

            b.logo.name = d['logo']
            b.logo_srcset.name = d['logo_srcset']
            b.pamm_pic.name = d['pamm_pic']

            if d['logo_white']:
                b.logo_white.name = d['logo_white']

            if d['logo_white_srcset']:
                b.logo_white_srcset.name = d['logo_white_srcset']

            if d['logo_circle']:
                b.logo_circle.name = d['logo_circle']

            if d['logo_circle_srcset']:
                b.logo_circle_srcset.name = d['logo_circle_srcset']

            b.save()

    def create_categories(self):
        Category.objects.all().delete()
        sort = 0
        for name in self.CATEGORIES:
            sort += 10
            Category.objects.create(name=name, sort=sort)

    def set_categories_to_brokers(self):
        for b in Broker.objects.all():
            qs = Category.objects.all().order_by('?')[:3]
            b.categories.set(qs)

    def create_account_types(self):
        AccountType.objects.all().delete()
        sort = 0
        for name in self.ACCOUNT_TYPES:
            sort += 10
            AccountType.objects.create(name=name, sort=sort)

    def create_account_items(self):
        AccountItem.objects.all().delete()
        path = os.path.join(settings.BASE_DIR,
                            'ztrade/broker/management/data/account_items.txt')
        data = open(path).read()
        l = data.split('\n')
        l = [x.strip() for x in l if x.strip()]
        tups = [x.split(';') for x in l]

        items = []
        for b in Broker.objects.all():
            d = {'broker': b}
            for atype in AccountType.objects.all():
                sort = 10
                for name, value in tups:
                    d['account_type'] = atype
                    d['name'] = name
                    d['value'] = value
                    d['sort'] = sort
                    items.append(AccountItem(**d))
                    sort += 10

        AccountItem.objects.bulk_create(items)

    def create_descs(self):
        descs = []
        for b in Broker.objects.all():
            d = {'broker': b}
            for text in self.ADVANTAGES:
                d['type'] = Desc.ADVANTAGE
                d['text'] = text
                d['count'] = random.randint(5, 20)
                desc = Desc(**d)
                descs.append(desc)

            for text in self.DISADVANTAGES:
                d['type'] = Desc.DISADVANTAGE
                d['text'] = text
                d['count'] = random.randint(5, 20)
                desc = Desc(**d)
                descs.append(desc)
        Desc.objects.bulk_create(descs)

    def create_params(self):
        Param.objects.all().delete()
        path = os.path.join(settings.BASE_DIR,
                            'ztrade/broker/management/data/params.txt')
        data = open(path).read()
        l = data.split('\n')
        l = [x.strip() for x in l if x.strip()]
        tups = splitlist(l, 2)

        params = []
        for b in Broker.objects.all():
            sort = 10
            d = {'broker': b}
            for name, value in tups:
                d['name'] = name
                d['value'] = value
                d['sort'] = sort
                p = Param(**d)
                params.append(p)

                sort += 10

        Param.objects.bulk_create(params)

    def create_reviews(self):
        Review.objects.all().delete()
        path = os.path.join(settings.BASE_DIR,
                            'ztrade/broker/management/data/review.txt')
        text = open(path).read()

        reviews = []
        for b in Broker.objects.all():
            for tup in Review.TYPE_CHOICES:
                d = {'broker': b}
                d['type'] = tup[0]
                d['name'] = 'Андрея'
                d['email'] = 'test@test.ru'
                d['count_likes'] = random.randint(5, 20)
                d['created_at'] = timezone.now()
                d['text'] = text
                reviews.append(Review(**d))

        Review.objects.bulk_create(reviews)
        # Run post_save
        for review in Review.objects.all():
            review.save()

    def create_pamm_recommends(self):
        """
        Create for each broker 2 PammRecommend
        """
        PammRecommend.objects.all().delete()
        menus = list(Menu.objects.all())

        for b in Broker.objects.all():
            PammRecommend.objects.create(
                broker=b, name='Ссылка 1', menu=random.choice(menus))
            PammRecommend.objects.create(
                broker=b, name='Ссылка 2', menu=random.choice(menus))

    def create_pamm_videos(self):
        """
        Create for each broker 2 PammVideo
        """
        PammVideo.objects.all().delete()
        for b in Broker.objects.all():
            PammVideo.objects.create(
                broker=b, title='Заголовок если нужен',
                link='https://www.youtube.com/embed/RNel9-r_IVM')
            PammVideo.objects.create(
                broker=b, title='Заголовок если нужен',
                link='https://www.youtube.com/embed/RNel9-r_IVM')

    def handle(self, *args, **options):
        self.create_brokers()
        self.create_categories()
        self.set_categories_to_brokers()
        self.create_account_types()
        self.create_account_items()
        self.create_descs()
        self.create_params()
        self.create_reviews()
        self.create_pamm_recommends()
        self.create_pamm_videos()
