import os
import pandas
from django.core.management.base import BaseCommand
from django.conf import settings

from broker.models import Broker
from broker.models import Pamm

from content.management.commands.helpers import get_file_content


class Command(BaseCommand):
    BOOL_FIELDS = []
    EXCEL_PATH = os.path.join(settings.BASE_DIR,
                              'ztrade/content/management/data/content.xlsx')

    def get_data(self):
        path = self.EXCEL_PATH
        df = pandas.read_excel(open(path, 'rb'), sheet_name='PAMM')

        l = []
        for index, row in df.iterrows():
            rd = row.to_dict()

            for k, v in rd.items():
                if k in self.BOOL_FIELDS:
                    v = self.process_bool(v)

                if str(v).endswith('.txt'):
                    v = get_file_content(v)

                v = self.process_value(v)
                rd[k] = v

            l.append(rd)
        return l

    def process_bool(self, value):
        if pandas.isnull(value):
            return False

        if value.strip().lower() == 'y':
            return True

        return False

    def process_value(self, value):
        if pandas.isnull(value):
            return None
        return value

    def create_pamm(self):
        Pamm.objects.all().delete()

        for d in self.get_data():
            slug = d.pop('broker_slug')
            d['broker'] = Broker.objects.get(slug=slug)
            p = Pamm.objects.create(**d)
            p.pic.name = d['pic']
            p.save()

    def handle(self, *args, **options):
        self.create_pamm()
