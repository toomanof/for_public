from django.db.models import Manager
from django.db.models import Model
from django.contrib import admin
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.db.models import ForeignKey
from django.db.models import CASCADE
from django.db.models import CharField
from django.db.models import IntegerField
from django.db.models import TextField
from django.db.models import DateTimeField


class ReviewManager(Manager):
    pass


class Review(Model):
    POSITIVE = 'positive'
    NEUTRAL = 'neutral'
    NEGATIVE = 'negative'

    TYPE_CHOICES = (
        (POSITIVE, 'positive'),
        (NEUTRAL, 'neutral'),
        (NEGATIVE, 'negative'),
    )

    user = ForeignKey('web.User', on_delete=CASCADE,
                      null=True, default=None, blank=True,
                      verbose_name='пользователь',
                      related_name='user_broker_reviews')
    broker = ForeignKey('Broker', on_delete=CASCADE,
                        verbose_name='брокер', related_name='reviews')
    name = CharField('имя', max_length=255)
    email = CharField('email', max_length=255)
    type = CharField('тип', max_length=20, choices=TYPE_CHOICES,
                     default=POSITIVE)
    text = TextField('текст')
    count_likes = IntegerField(default=0)
    created_at = DateTimeField(null=True, default=None, blank=True)

    objects = ReviewManager()

    class Meta:
        app_label = 'broker'
        verbose_name = 'отзыв'
        verbose_name_plural = 'отзывы'
        ordering = ['-created_at']

    def __str__(self):
        return self.name

    @property
    def title(self):
        if self.type == self.POSITIVE:
            return 'Положительный отзыв'
        elif self.type == self.NEGATIVE:
            return 'Негативный отзыв'
        return 'Нейтральный отзыв'


@receiver(post_save, sender=Review)
def reindex_count_reviews(sender, instance, created, **kwargs):
    broker = instance.broker
    qs = Review.objects.filter(broker=broker)
    broker.count_positive_reviews = qs.filter(type=Review.POSITIVE).count()
    broker.count_neutral_reviews = qs.filter(type=Review.NEUTRAL).count()
    broker.count_negative_reviews = qs.filter(type=Review.NEGATIVE).count()
    broker.save()


@admin.register(Review)
class ReviewAdmin(admin.ModelAdmin):
    list_filter = ('type', 'broker')
    list_display = (
        'name',
        'email',
        'user',
        'type',
        'count_likes',
        'created_at',
    )
