from django.db.models import Manager
from django.db.models import Model
from django.contrib import admin
from django.db.models import ForeignKey
from django.db.models import CharField
from django.db.models import CASCADE
from django.db.models import IntegerField


class PammVideoManager(Manager):
    pass


class PammVideo(Model):
    """
    Video links for PAMM Broker's page
    """
    broker = ForeignKey('Broker', on_delete=CASCADE, verbose_name='брокер',
                        related_name='pamm_videos')
    link = CharField('ссылка', max_length=255)
    title = CharField('заголовок', max_length=255, null=True, default=None,
                      blank=True)
    sort = IntegerField(default=0)

    objects = PammVideoManager()

    class Meta:
        app_label = 'broker'
        verbose_name = 'видео (памм)'
        verbose_name_plural = 'видео (памм)'
        ordering = ['sort']

    def __str__(self):
        return self.link


# @admin.register(PammVideo)
class PammVideoAdmin(admin.ModelAdmin):
    list_display = (
        'broker',
        'menu',
        'name',
        'sort'
    )
