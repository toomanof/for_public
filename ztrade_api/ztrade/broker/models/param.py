from django.db.models import Manager
from django.db.models import Model
from django.contrib import admin
from django.db.models import ForeignKey
from django.db.models import CASCADE
from django.db.models import CharField
from django.db.models import IntegerField


class ParamManager(Manager):
    pass


class Param(Model):
    broker = ForeignKey('Broker', on_delete=CASCADE,
                        verbose_name='брокер', related_name='params')
    name = CharField('название', max_length=255)
    value = CharField('значение', max_length=1000)
    sort = IntegerField(default=0)

    objects = ParamManager()

    class Meta:
        app_label = 'broker'
        verbose_name = 'параметр'
        verbose_name_plural = 'параметры'
        ordering = ['sort']

    def __str__(self):
        return self.name


@admin.register(Param)
class ParamAdmin(admin.ModelAdmin):
    list_filter = ('broker',)
    list_display = (
        'name',
        'value',
        'broker',
        'sort',
    )
