from django.db.models import Manager
from django.db.models import Model
from django.contrib import admin
from django.db.models import ForeignKey
from django.db.models import CharField
from django.db.models import IntegerField
from django.db.models import CASCADE


class PammRecommendManager(Manager):
    pass


class PammRecommend(Model):
    """
    Recommend links for PAMM Broker's page
    """
    broker = ForeignKey('Broker', on_delete=CASCADE, verbose_name='брокер',
                        related_name='pamm_recommends')
    menu = ForeignKey('content.Menu', on_delete=CASCADE, verbose_name='меню')
    name = CharField('название', max_length=100)
    sort = IntegerField(default=0)

    objects = PammRecommendManager()

    class Meta:
        app_label = 'broker'
        verbose_name = 'рекомендуем (памм)'
        verbose_name_plural = 'рекомендуем (памм)'
        ordering = ['sort']

    def __str__(self):
        return self.name

    @property
    def url(self):
        return self.menu.url


# @admin.register(PammRecommend)
class PammRecommendAdmin(admin.ModelAdmin):
    list_display = (
        'broker',
        'menu',
        'name',
        'sort'
    )
