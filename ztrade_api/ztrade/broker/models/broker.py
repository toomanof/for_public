from django.db.models import Manager
from django.db.models import Model
from django.contrib import admin
from django.conf import settings
from django.db.models import CharField
from django.db.models import IntegerField
from django.db.models import DecimalField
from django.db.models import ImageField
from django.db.models import TextField
from django.db.models import BooleanField
from django.db.models import ManyToManyField
from django.db.models import SlugField
from ckeditor_uploader.fields import RichTextUploadingField

from web.services.helpers import get_guid
from web.services.helpers import slicelist

from .pamm_recommend import PammRecommend
from .pamm_video import PammVideo


class BrokerManager(Manager):
    pass


class Broker(Model):
    SPREAD_CHOICES = (
        (1, 1),
        (2, 2),
        (3, 3),
    )

    guid = CharField(max_length=50, default=get_guid)
    title = CharField(max_length=255, null=True, default=None, blank=True)
    h1 = CharField(max_length=255, null=True, default=None, blank=True)
    name = CharField('название', max_length=100)
    slug = SlugField(max_length=100, unique=True)
    logo = ImageField(upload_to='images/logos/regular', null=True,
                      default=None, blank=True)
    logo_srcset = ImageField(upload_to='images/logos/regular', null=True,
                             default=None, blank=True)
    logo_white = ImageField(upload_to='images/logos/white', null=True,
                            default=None, blank=True)
    logo_white_srcset = ImageField(upload_to='images/logos/white', null=True,
                                   default=None, blank=True)
    logo_circle = ImageField(upload_to='images/logos/circle', null=True,
                             default=None, blank=True)
    logo_circle_srcset = ImageField(upload_to='images/logos/circle', null=True,
                                    default=None, blank=True)
    rating = DecimalField('рейтинг', max_digits=5, decimal_places=1)
    min_deposit = IntegerField('мин. депозит', null=True, default=None,
                               blank=True)
    max_leverage = IntegerField('макс. плечо', null=True, default=None,
                                blank=True)
    spread = IntegerField('спред', default=1, choices=SPREAD_CHOICES)
    trade_platform = CharField('торговая платформа', max_length=100,
                               null=True, default=None, blank=True)
    regulation = CharField('регулирование', max_length=255,
                           null=True, default=None, blank=True)
    payout = CharField('вывод средств', max_length=255,
                       null=True, default=None, blank=True)
    text_items = TextField(null=True, default=None, blank=True)
    site = CharField('сайт', max_length=100, null=True, default=None,
                     blank=True)
    site_url = CharField('сайт url', max_length=1000, null=True, default=None,
                         blank=True)
    link_open_account_with_robot = CharField(
        'ссылка: как открыть счет для аренды и подключения к биржевому роботу',
        max_length=255, null=True,
        default=None, blank=True,
        help_text='Подробная инструкция, как открыть счет для аренды и подключения к биржевому роботу')
    link_open_account = CharField(
        'ссылка: как открыть счет для аренды самостоятельной торговли на финансовых рынках',
        max_length=255, null=True,
        default=None, blank=True,
        help_text='Подробная инструкция, как открыть счет для аренды самостоятельной торговли на финансовых рынках')
    link2 = CharField(
        'ссылка: как инвестировать', max_length=255, null=True,
        default=None, blank=True,
        help_text='если ссылка начинается на http, отправка на сторонний ресурс')
    is_featured = BooleanField('рекоменд.', default=False)
    pamm_perc = DecimalField(max_digits=5, decimal_places=1,
                             null=True, default=None, blank=True,
                             verbose_name='ПАММ проценты',
                             help_text='ПАММ - доходность процентов в ТОП5')
    pamm_pic = ImageField(upload_to='images/brokers/pamm', null=True,
                          default=None, blank=True)
    pamm_link = CharField('Ссылка для ПАММ', max_length=255, null=True,
                          default=None, blank=True)
    content = RichTextUploadingField('контент', null=True,
                                     default=None, blank=True)
    content_pamm = RichTextUploadingField('контент памм страницы', null=True,
                                          default=None, blank=True)
    count_positive_reviews = IntegerField(default=0,
                                          help_text='Indexed automatically')
    count_neutral_reviews = IntegerField(default=0,
                                         help_text='Indexed automatically')
    count_negative_reviews = IntegerField(default=0,
                                          help_text='Indexed automatically')
    categories = ManyToManyField('Category', related_name='brokers')

    objects = BrokerManager()

    class Meta:
        app_label = 'broker'
        verbose_name = 'брокер'
        verbose_name_plural = 'брокеры'
        ordering = ['-rating']

    def __str__(self):
        return self.name

    @property
    def logo_url(self):
        if self.logo and self.logo.name:
            return '{}/{}'.format(settings.BUCKET_URL, self.logo.name)
        return None

    @property
    def logo_srcset_url(self):
        if self.logo_srcset and self.logo_srcset.name:
            return '{}/{}'.format(settings.BUCKET_URL, self.logo_srcset.name)
        return None

    @property
    def logo_white_url(self):
        if self.logo_white and self.logo_white.name:
            return '{}/{}'.format(settings.BUCKET_URL, self.logo_white.name)
        return None

    @property
    def logo_white_srcset_url(self):
        if self.logo_white_srcset and self.logo_white_srcset.name:
            return '{}/{}'.format(
                settings.BUCKET_URL, self.logo_white_srcset.name)
        return None

    @property
    def logo_circle_url(self):
        if self.logo_circle and self.logo_circle.name:
            return '{}/{}'.format(settings.BUCKET_URL, self.logo_circle.name)
        return None

    @property
    def logo_circle_srcset_url(self):
        if self.logo_circle_srcset and self.logo_circle_srcset.name:
            return '{}/{}'.format(
                settings.BUCKET_URL, self.logo_circle_srcset.name)
        return None

    @property
    def pamm_pic_url(self):
        if self.pamm_pic and self.pamm_pic.name:
            return '{}/{}'.format(settings.BUCKET_URL, self.pamm_pic.name)
        return None

    @property
    def text_items_list(self):
        """
        List of items splitted on 3 lists (columns).
        """
        if self.text_items is None:
            return []
        l = self.text_items.split('\n')
        l = [x.strip() for x in l if x.strip()]
        return list(slicelist(l, cols=3))

    @property
    def frontend_url(self):
        return '/brokers/{}'.format(self.slug)

    @property
    def frontend_pamm_url(self):
        return '/brokers/{}/pamm'.format(self.slug)

    def get_h1(self):
        if self.h1:
            return self.h1
        return 'Брокер ' + self.name

    def get_title(self):
        if self.title:
            return self.title
        return 'Брокер ' + self.name


class PammRecommendInline(admin.TabularInline):
    model = PammRecommend
    fields = ('name', 'menu', 'sort')
    extra = 0


class PammVideoInline(admin.TabularInline):
    model = PammVideo
    fields = ('link', 'title', 'sort')
    extra = 0


@admin.register(Broker)
class BrokerAdmin(admin.ModelAdmin):
    inlines = [
        PammRecommendInline,
        PammVideoInline
    ]

    list_display = (
        'name',
        'slug',
        'rating',
        'min_deposit',
        'max_leverage',
        'spread',
        'trade_platform',
        'regulation',
        'payout',
        'site',
        'logo',
        'logo_srcset',
        'logo_white',
        'logo_white_srcset',
        'logo_circle',
        'logo_circle_srcset',
    )
    exclude = (
        'count_positive_reviews',
        'count_neutral_reviews',
        'count_negative_reviews',
    )
