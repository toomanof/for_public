import datetime
from django.db.models import Manager
from django.db.models import Model
from django.contrib import admin
from django.conf import settings
from django.db.models import ForeignKey
from django.db.models import CharField
from django.db.models import IntegerField
from django.db.models import TextField
from django.db.models import ImageField
from django.db.models import CASCADE
from dateutil import relativedelta


class PammManager(Manager):
    pass


class Pamm(Model):
    broker = ForeignKey('Broker', on_delete=CASCADE,
                        related_name='pamms', verbose_name='брокер')
    first_name = CharField('имя', max_length=100)
    last_name = CharField('фамилия', max_length=100)
    age = IntegerField('возраст, дней')
    min_deposit = IntegerField('минимальный депозит')
    fee = CharField('комиссия', max_length=100)
    avg_month = CharField('среднее в месяц', max_length=100)
    avg_year = CharField('среднее в год', max_length=100)
    link = CharField('ссылка', max_length=1000,
                     null=True, default=None, blank=True)
    html_code = TextField('html код', null=True, default=None, blank=True)
    perc = IntegerField('процент', null=True, default=None, blank=True)
    pic = ImageField(upload_to='images/brokers/pamm/accounts', null=True,
                     default=None, blank=True)
    sort = IntegerField(default=0)

    objects = PammManager()

    class Meta:
        app_label = 'broker'
        verbose_name = 'памм'
        verbose_name_plural = 'памм'
        ordering = ['sort']

    def __str__(self):
        return self.first_name + ' ' + self.last_name

    @property
    def pic_url(self):
        if self.pic and self.pic.name:
            return '{}/{}'.format(settings.BUCKET_URL, self.pic.name)
        return None

    @property
    def age_str(self):
        """
        Returns age converted to years, months
        """
        dt = datetime.date.today()
        rd = relativedelta.relativedelta(
            dt, dt - datetime.timedelta(days=self.age))

        if rd.years == 0:
            s1 = ''
        elif rd.years == 1:
            s1 = 'год'
        elif rd.years > 1 and rd.years < 5:
            s1 = 'года'
        else:
            s1 = 'лет'

        if rd.months == 0:
            s2 = ''
        elif rd.months == 1:
            s2 = 'месяц'
        elif rd.months > 1 and rd.months < 5:
            s2 = 'месяца'
        else:
            s2 = 'месяцев'

        s = ''
        if s1:
            s += '{} {} '.format(rd.years, s1)
        if s2:
            s += '{} {}'.format(rd.months, s2)
        return s.rstrip()


@admin.register(Pamm)
class PammAdmin(admin.ModelAdmin):
    list_filter = ('broker',)
    list_display = (
        'first_name',
        'last_name',
        'broker',
        'age',
        'min_deposit',
        'fee',
        'avg_month',
        'avg_year',
        'link',
        'sort',
    )
