from django.db.models import Manager
from django.db.models import Model
from django.contrib import admin
from django.db.models import CharField
from django.db.models import IntegerField


class CategoryManager(Manager):
    pass


class Category(Model):
    name = CharField('название', max_length=100)
    sort = IntegerField(default=0)

    objects = CategoryManager()

    class Meta:
        app_label = 'broker'
        verbose_name = 'категория'
        verbose_name_plural = 'категории'
        ordering = ['sort']

    def __str__(self):
        return self.name


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'sort',
    )
