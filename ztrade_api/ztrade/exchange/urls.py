from django.urls import re_path
from .import views as v

urlpatterns = [
    re_path(
        r'^(?P<version>(v1))/exchange/exchanges$',
        v.ExchangeView.as_view(),
        name='exchanges'),
    re_path(
        r'^(?P<version>(v1))/exchange/exchanges/(?P<slug>\w+)$',
        v.ExchangeDetail.as_view(),
        name='exchange'),
    re_path(
        r'^(?P<version>(v1))/exchange/categories$',
        v.Categories.as_view(),
        name='exchange_categories$'),
    re_path(
        r'^(?P<version>(v1))/exchange/descs$',
        v.ExchangeDescs.as_view(),
        name='exchange_descs'),

    re_path(
        r'^(?P<version>(v1))/exchange/descs/(?P<id>\d+)/increment$',
        v.ExchangeDescIncrement.as_view(),
        name='exchange_desc_increment'),

    re_path(
        r'^(?P<version>(v1))/exchange/reviews/create_auth$',
        v.ExchangeReviewsCreateAuth.as_view(),
        name='exchange_reviews_create_auth'),

    re_path(
        r'^(?P<version>(v1))/exchange/reviews/create_not_auth$',
        v.ExchangeReviewsCreateNotAuth.as_view(),
        name='exchange_reviews_create_not_auth'),
    ]
