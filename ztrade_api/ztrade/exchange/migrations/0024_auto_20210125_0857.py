# Generated by Django 2.0.4 on 2021-01-25 08:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('exchange', '0023_exchange_trade_platform'),
    ]

    operations = [
        migrations.AddField(
            model_name='exchange',
            name='payout',
            field=models.CharField(blank=True, default=None, max_length=255, null=True, verbose_name='вывод средств'),
        ),
        migrations.AddField(
            model_name='exchange',
            name='regulation',
            field=models.CharField(blank=True, default=None, max_length=255, null=True, verbose_name='регулирование'),
        ),
    ]
