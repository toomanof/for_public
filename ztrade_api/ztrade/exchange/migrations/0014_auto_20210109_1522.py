# Generated by Django 2.0.4 on 2021-01-09 15:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('exchange', '0013_exchange_name'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='exchange',
            name='title',
        ),
        migrations.AddField(
            model_name='exchange',
            name='site',
            field=models.CharField(blank=True, default=None, max_length=100, null=True, verbose_name='сайт'),
        ),
        migrations.AddField(
            model_name='exchange',
            name='site_url',
            field=models.CharField(blank=True, default=None, max_length=1000, null=True, verbose_name='сайт url'),
        ),
        migrations.AddField(
            model_name='exchange',
            name='text_items',
            field=models.TextField(blank=True, default=None, null=True),
        ),
        migrations.AlterField(
            model_name='exchange',
            name='logo',
            field=models.ImageField(blank=True, default=None, max_length=255, null=True, upload_to='images/logos/regular'),
        ),
        migrations.AlterField(
            model_name='exchange',
            name='logo_circle',
            field=models.ImageField(blank=True, default=None, max_length=255, null=True, upload_to='images/logos/circle'),
        ),
        migrations.AlterField(
            model_name='exchange',
            name='logo_circle_srcset',
            field=models.ImageField(blank=True, default=None, max_length=255, null=True, upload_to='images/logos/circle'),
        ),
        migrations.AlterField(
            model_name='exchange',
            name='logo_srcset',
            field=models.ImageField(blank=True, default=None, max_length=255, null=True, upload_to='images/logos/regular'),
        ),
        migrations.AlterField(
            model_name='exchange',
            name='logo_white',
            field=models.ImageField(blank=True, default=None, max_length=255, null=True, upload_to='images/logos/white'),
        ),
        migrations.AlterField(
            model_name='exchange',
            name='logo_white_srcset',
            field=models.ImageField(blank=True, default=None, max_length=255, null=True, upload_to='images/logos/white'),
        ),
    ]
