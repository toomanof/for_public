from rest_framework.generics import CreateAPIView
from rest_framework.generics import ListAPIView
from rest_framework.generics import RetrieveAPIView
from rest_framework.response import Response
from django.conf import settings
from django.shortcuts import get_object_or_404

from .models import Category, Exchange, Desc
from .serializers import (ExchangeSerializer, CategorySerializer,
                          ExchangeExtSerializer, DescCreateSerializer,
                          ReviewCreateSerializer)

from web.views import AuthMixin
from web.services.helpers import get_ip_address


class Categories(ListAPIView):
    """
    get:
        <h1>Get categories</h1>
        <b>Response:</b> list of category object<br />
        <b>Response code:</b> 200
    """
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class ExchangeView(ListAPIView):
    queryset = Exchange.objects.all()
    serializer_class = ExchangeSerializer


class ExchangeDetail(RetrieveAPIView):
    """
    get:
        <h1>Get exchange</h1>
        <b>Response:</b> exchange object<br />
        <b>Response code:</b> 200
    """
    lookup_field = 'slug'
    queryset = Exchange.objects.all()
    serializer_class = ExchangeExtSerializer


class ExchangeDescs(AuthMixin, CreateAPIView):
    """
    post:
        <h1>Create desc for exchange</h1>
        <b>Response:</b> updated exchange object<br />
        <b>Response code:</b> 200
    """

    def post(self, request, **kwargs):
        ctx = {'user': request.user}
        s = DescCreateSerializer(data=request.data, context=ctx)
        s.is_valid(raise_exception=True)
        desc = s.save()

        s = ExchangeExtSerializer(desc.exchange)
        return Response(s.data, status=201)


class ExchangeDescIncrement(AuthMixin, CreateAPIView):
    """
    post:
        <h1>Increment desc for exchange</h1>
        <b>Response:</b> updated exchange object<br />
        <b>Response code:</b> 200
    """

    def post(self, request, id, **kwargs):
        ip = get_ip_address(request)
        key = f'EXCHANGE_INCREMENT_{id}'
        rs = settings.REDIS_DB
        if ip is None:
            return Response({'errors': ['Лимит на IP адрес исчерпан.']},
                            status=400)
        v = rs.get(key)
        if v is not None and v.decode("utf-8") == ip:
            return Response({'errors': ['Вы уже проголосовали.']}, status=400)

        desc = get_object_or_404(Desc, id=id)
        desc.count += 1
        desc.save()

        s = ExchangeExtSerializer(desc.exchange)
        rs.set(key, ip)
        rs.expire(key, 3600)
        return Response(s.data, status=201)


class ExchangeReviewsCreateAuth(AuthMixin, CreateAPIView):
    """
    post:
        <h1>Create review for exchange by auth user</h1>
        <b>Response:</b> updated exchange object<br />
        <b>Response code:</b> 200
    """

    def post(self, request, **kwargs):
        ctx = {'user': request.user}
        s = ReviewCreateSerializer(data=request.data, context=ctx)
        s.is_valid(raise_exception=True)
        review = s.save()
        s = ExchangeExtSerializer(review.exchange)
        return Response(s.data, status=201)


class ExchangeReviewsCreateNotAuth(CreateAPIView):
    """
    post:
        <h1>Create review for exchange by not auth user</h1>
        <b>Response:</b> updated exchange object<br />
        <b>Response code:</b> 200
    """

    def post(self, request, **kwargs):
        ctx = {'user': None}
        s = ReviewCreateSerializer(data=request.data, context=ctx)
        s.is_valid(raise_exception=True)
        review = s.save()

        s = ExchangeExtSerializer(review.exchange)
        return Response(s.data, status=201)
