from django.conf import settings
from django.utils import timezone
from rest_framework import serializers
from rest_framework.serializers import ModelSerializer

from .models import (AccountType, AccountItem, Category, Currency, Desc,
                     Exchange, Param, Review,   Localization, TradingVolume)

from web.models import Email
from web.models import Emailer


class AccountTypeSerializer(ModelSerializer):
    class Meta:
        model = AccountType
        fields = (
            'id',
            'name',
        )


class AccountItemSerializer(ModelSerializer):
    account_type_id = serializers.ModelField(
        model_field=AccountItem._meta.get_field('account_type'))
    exchange_id = serializers.ModelField(
        model_field=AccountItem._meta.get_field('exchange'))

    class Meta:
        model = AccountItem
        fields = (
            'id',
            'account_type_id',
            'exchange_id',
            'name',
            'value'
        )


class CategorySerializer(ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'


class CurrencySerializer(ModelSerializer):
    class Meta:
        model = Currency
        fields = '__all__'


class DescSerializer(ModelSerializer):
    exchange_id = serializers.ModelField(
        model_field=Desc._meta.get_field('exchange'))

    class Meta:
        model = Desc
        fields = (
            'id',
            'exchange_id',
            'type',
            'text',
            'count'
        )


class DescCreateSerializer(ModelSerializer):
    exchange_id = serializers.ModelField(
        model_field=Desc._meta.get_field('exchange'))

    class Meta:
        model = Desc
        fields = (
            'exchange_id',
            'type',
            'text',
        )

    def create(self, validated_data):
        d = validated_data
        d['user'] = self.context.get('user')
        obj = Desc.objects.create(**d)
        Emailer.objects.process_email(
            Email.NEW_EXCHANGE_DESC_CREATED,
            email=settings.ADMIN_EMAIL,
            obj=obj)
        return obj


class TradingVolumeSerializer(ModelSerializer):
    iso = serializers.SerializerMethodField()

    class Meta:
        model = TradingVolume
        fields = ('id', 'value', 'iso')

    def get_iso(self, obj):
        return obj.currency.iso


class LocalizationSerializer(ModelSerializer):
    class Meta:
        model = Localization
        fields = '__all__'




class ExchangeSerializer(ModelSerializer):
    h1 = serializers.CharField(source='get_h1')
    title = serializers.CharField(source='get_title')
    localizations = LocalizationSerializer(read_only=True, many=True)
    trading_volumes = TradingVolumeSerializer(read_only=True, many=True)
    category_ids = serializers.SerializerMethodField()
    currencies = CurrencySerializer(read_only=True, many=True)

    class Meta:
        model = Exchange
        fields = (
            'id',
            'guid',
            'title',
            'h1',
            'name',
            'slug',
            'is_featured',
            'site',
            'site_url',
            'link_open_account_with_robot',
            'link_open_account',
            'link2',
            'logo_url',
            'logo_srcset_url',
            'logo_white_url',
            'logo_white_srcset_url',
            'logo_circle_url',
            'logo_circle_srcset_url',
            'count_positive_reviews',
            'count_neutral_reviews',
            'count_negative_reviews',
            'frontend_url',
            'frontend_pamm_url',
            'category_ids',
            'localizations',
            'trading_volumes',
            'text_items_list',
            'currencies',
            'сommission',
        )

    def get_category_ids(self, obj):
        return obj.categories.values_list('id', flat=True)



class ParamSerializer(ModelSerializer):
    exchange_id = serializers.ModelField(
        model_field=Param._meta.get_field('exchange'))

    class Meta:
        model = Param
        fields = (
            'id',
            'exchange_id',
            'name',
            'value',
        )


class ReviewSerializer(ModelSerializer):
    exchange_id = serializers.ModelField(
        model_field=Review._meta.get_field('exchange'))

    class Meta:
        model = Review
        fields = (
            'id',
            'exchange_id',
            'type',
            'name',
            'text',
            'count_likes',
            'created_at',
            'title'
        )


class ReviewCreateSerializer(ModelSerializer):
    exchange_id = serializers.ModelField(
        model_field=Desc._meta.get_field('exchange'))

    class Meta:
        model = Review
        fields = (
            'exchange_id',
            'name',
            'email',
            'type',
            'text',
        )

    def create(self, validated_data):
        d = validated_data
        d['user'] = self.context.get('user')
        d['created_at'] = timezone.now()
        obj = Review.objects.create(**d)
        Emailer.objects.process_email(
            Email.NEW_EXCHANGE_REVIEW_CREATED,
            email=settings.ADMIN_EMAIL,
            obj=obj)
        return obj


class ExchangeExtSerializer(ExchangeSerializer):
    account_types = serializers.SerializerMethodField()
    account_items = serializers.SerializerMethodField()
    descs = serializers.SerializerMethodField()
    params = serializers.SerializerMethodField()
    reviews = serializers.SerializerMethodField()

    class Meta(ExchangeSerializer.Meta):
        model = Exchange
        fields = ExchangeSerializer.Meta.fields +\
            (
                'content',
                'trade_platform',
                'regulation',
                'payout',
                'text_items_list',
                'account_types',
                'account_items',
                'descs',
                'params',
                'reviews',
            )

    def get_account_types(self, obj):
        ids = AccountItem.objects\
            .filter(exchange_id=obj.id)\
            .values_list('account_type_id', flat=True)
        qs = AccountType.objects.filter(id__in=ids)
        return AccountTypeSerializer(qs, many=True).data

    def get_account_items(self, obj):
        return AccountItemSerializer(obj.items.all(), many=True).data

    def get_descs(self, obj):
        return DescSerializer(obj.descs.all(), many=True).data

    def get_params(self, obj):
        return ParamSerializer(obj.params.all(), many=True).data

    def get_reviews(self, obj):
        return ReviewSerializer(obj.reviews.all(), many=True).data
