from django.db.models import Manager
from django.db.models import Model
from django.contrib import admin
from django.db.models import ForeignKey
from django.db.models import CASCADE
from django.db.models import CharField
from django.db.models import IntegerField


class ParamManager(Manager):
    pass


class Param(Model):
    exchange = ForeignKey('Exchange', on_delete=CASCADE,
                          verbose_name='биржа', related_name='params')
    name = CharField('название', max_length=255)
    value = CharField('значение', max_length=1000)
    sort = IntegerField(default=0)

    objects = ParamManager()

    class Meta:
        verbose_name = 'параметр'
        verbose_name_plural = 'параметры'
        ordering = ['sort']

    def __str__(self):
        return self.name


@admin.register(Param)
class ParamAdmin(admin.ModelAdmin):
    list_filter = ('exchange',)
    list_display = (
        'name',
        'value',
        'exchange',
        'sort',
    )
