from django.db.models import Manager
from django.db.models import Model
from django.contrib import admin
from django.db.models import ForeignKey
from django.db.models import CASCADE
from django.db.models import CharField
from django.db.models import IntegerField

from .account_type import AccountType


class AccountItemManager(Manager):
    pass


class AccountItem(Model):
    exchange = ForeignKey('Exchange', on_delete=CASCADE,
                        verbose_name='брокер', related_name='items')
    account_type = ForeignKey(AccountType, on_delete=CASCADE,
                              verbose_name='тип аккаунта',
                              related_name='type_items')
    name = CharField('название', max_length=255)
    value = CharField('значение', max_length=1000)
    sort = IntegerField(default=0)

    objects = AccountItemManager()

    class Meta:
        verbose_name = 'аккаунт item'
        verbose_name_plural = 'аккаунт items'
        ordering = ['sort']

    def __str__(self):
        return self.name


@admin.register(AccountItem)
class AccountItemAdmin(admin.ModelAdmin):
    list_filter = ('account_type', 'exchange',)
    list_display = (
        'account_type',
        'name',
        'value',
        'exchange',
        'sort',
    )
