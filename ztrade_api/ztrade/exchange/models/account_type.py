from django.db.models import Manager
from django.db.models import Model
from django.contrib import admin
from django.db.models import CharField
from django.db.models import IntegerField


class AccountTypeManager(Manager):
    pass


class AccountType(Model):
    name = CharField('название', max_length=50)
    sort = IntegerField(default=0)

    objects = AccountTypeManager()

    class Meta:
        verbose_name = 'тип аккаунта'
        verbose_name_plural = 'типы аккаунтов'
        ordering = ['sort']

    def __str__(self):
        return self.name


@admin.register(AccountType)
class AccountTypeAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'sort',
    )
