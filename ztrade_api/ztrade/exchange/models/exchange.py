from django.conf import settings
from django.db import models
from django.contrib import admin
from ckeditor_uploader.fields import RichTextUploadingField

from web.services.helpers import get_guid
from web.services.helpers import slicelist


class Localization (models.Model):
    name = models.CharField('Название', max_length=255)
    iso = models.CharField('трёхбуквенное алфавитное сокращение', max_length=3)

    def __str__(self):
        return self.iso

    class Meta:
        verbose_name = 'Локализация'
        verbose_name_plural = 'Локализации'


class Category(models.Model):
    name = models.CharField('название', max_length=100)
    sort = models.IntegerField(default=0)

    class Meta:
        verbose_name = 'категория'
        verbose_name_plural = 'категории'
        ordering = ['sort']

    def __str__(self):
        return self.name


class Currency(models.Model):
    name = models.CharField('Название', max_length=255)
    iso = models.CharField('трёхбуквенное алфавитное сокращение', max_length=3)

    class Meta:
        verbose_name = 'Валюта'
        verbose_name_plural = 'Валюты'

    def __str__(self):
        return self.iso


class Exchange(models.Model):
    COMMISSION_CHOICES = (
        (1, 1),
        (2, 2),
        (3, 3),
    )
    guid = models.CharField(max_length=255, default=get_guid)
    name = models.CharField(max_length=255, null=True, default=None, blank=True)
    title = models.CharField(max_length=255, null=True, default=None, blank=True)
    h1 = models.CharField(max_length=255, null=True, default=None, blank=True)
    categories = models.ManyToManyField('Category', related_name='brokers')
    slug = models.SlugField(max_length=100, unique=True)
    sort = models.IntegerField('сортировка', default=0)
    localizations = models.ManyToManyField(Localization, verbose_name='локализации',
                                           related_name='Exchanges')
    is_featured = models.BooleanField('рекоменд.', default=False)
    сommission = models.SmallIntegerField('комиссия %', default=1, choices=COMMISSION_CHOICES)
    logo = models.ImageField(upload_to='images/logos/regular', null=True,
                             default=None, blank=True, max_length=255)
    logo_srcset = models.ImageField(upload_to='images/logos/regular', null=True,
                                    default=None, blank=True, max_length=255)
    logo_white = models.ImageField(upload_to='images/logos/white', null=True,
                                   default=None, blank=True, max_length=255)
    logo_white_srcset = models.ImageField(upload_to='images/logos/white', null=True,
                                          default=None, blank=True, max_length=255)
    logo_circle = models.ImageField(upload_to='images/logos/circle', null=True,
                                    default=None, blank=True, max_length=255)
    logo_circle_srcset = models.ImageField(upload_to='images/logos/circle', null=True,
                                           default=None, blank=True, max_length=255)
    content = RichTextUploadingField('контент', null=True,
                                     default=None, blank=True)
    text_items = models.TextField("Перечень информации", null=True, default=None, blank=True)
    site = models.CharField('сайт', max_length=100, null=True, default=None, blank=True)
    site_url = models.CharField('сайт url', max_length=1000, null=True, default=None, blank=True)
    link_open_account_with_robot = models.CharField(
        'ссылка: как открыть счет для аренды и подключения к биржевому роботу',
        max_length=255, null=True,
        default=None, blank=True,
        help_text='Подробная инструкция, как открыть счет для аренды и подключения к биржевому роботу')
    link_open_account = models.CharField(
        'ссылка: как открыть счет для аренды самостоятельной торговли на финансовых рынках',
        max_length=255, null=True,
        default=None, blank=True,
        help_text='Подробная инструкция, как открыть счет для аренды самостоятельной торговли на финансовых рынках')
    link2 = models.CharField(
        'ссылка: как инвестировать', max_length=255, null=True,
        default=None, blank=True,
        help_text='если ссылка начинается на http, отправка на сторонний ресурс')
    count_positive_reviews = models.IntegerField(default=0, help_text='Indexed automatically')
    count_neutral_reviews = models.IntegerField(default=0, help_text='Indexed automatically')
    count_negative_reviews = models.IntegerField(default=0, help_text='Indexed automatically')
    currencies = models.ManyToManyField(Currency, verbose_name='Криптовалюта', related_name='exchanges')
    trade_platform = models.CharField('торговая платформа', max_length=100,
                               null=True, default=None, blank=True)
    regulation = models.CharField('регулирование', max_length=255,
                           null=True, default=None, blank=True)
    payout = models.CharField('вывод средств', max_length=255,
                       null=True, default=None, blank=True)

    class Meta:
        verbose_name = 'Биржа криптавалют'
        verbose_name_plural = 'Биржи криптовалют'
        ordering = ['sort']

    def __str__(self):
        return str(self.name)

    @property
    def logo_url(self):
        if self.logo and self.logo.name:
            return '{}/{}'.format(settings.BUCKET_URL, self.logo.name)
        return None

    @property
    def logo_srcset_url(self):
        if self.logo_srcset and self.logo_srcset.name:
            return '{}/{}'.format(settings.BUCKET_URL, self.logo_srcset.name)
        return None

    @property
    def logo_white_url(self):
        if self.logo_white and self.logo_white.name:
            return '{}/{}'.format(settings.BUCKET_URL, self.logo_white.name)
        return None

    @property
    def logo_white_srcset_url(self):
        if self.logo_white_srcset and self.logo_white_srcset.name:
            return '{}/{}'.format(
                settings.BUCKET_URL, self.logo_white_srcset.name)
        return None

    @property
    def logo_circle_url(self):
        if self.logo_circle and self.logo_circle.name:
            return '{}/{}'.format(settings.BUCKET_URL, self.logo_circle.name)
        return None

    @property
    def logo_circle_srcset_url(self):
        if self.logo_circle_srcset and self.logo_circle_srcset.name:
            return '{}/{}'.format(
                settings.BUCKET_URL, self.logo_circle_srcset.name)
        return None

    @property
    def pamm_pic_url(self):
        if self.pamm_pic and self.pamm_pic.name:
            return '{}/{}'.format(settings.BUCKET_URL, self.pamm_pic.name)
        return None

    @property
    def text_items_list(self):
        """
        List of items splitted on 3 lists (columns).
        """
        if self.text_items is None:
            return []
        l = self.text_items.split('\n')
        l = [x.strip() for x in l if x.strip()]
        return list(slicelist(l, cols=3))

    @property
    def frontend_url(self):
        return '/exchanges/{}'.format(self.slug)

    @property
    def frontend_pamm_url(self):
        return '/exchanges/{}/pamm'.format(self.slug)


    def get_h1(self):
        if self.h1:
            return self.h1
        return 'Биржа ' + self.name

    def get_title(self):
        if self.title:
            return self.title
        return 'Биржа ' + self.name


class TradingVolume(models.Model):
    exchange = models.ForeignKey(Exchange, verbose_name='биржа', related_name='trading_volumes',
                                 on_delete=models.CASCADE)
    value = models.BigIntegerField('значение', default=0)
    currency = models.ForeignKey(Currency, verbose_name='валюта', related_name='trading_volumes',
                                 on_delete=models.CASCADE)
    sort = models.IntegerField('сортировка', default=0)

    class Meta:
        verbose_name = 'Объем рынка'
        verbose_name_plural = 'Объемы рынков'
        ordering = ['sort']


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'sort',
    )


@admin.register(Localization)
class CurrencyAdmin(admin.ModelAdmin):
    list_display = ('name', 'iso',)


@admin.register(Currency)
class CurrencyAdmin(admin.ModelAdmin):
    list_display = ('name', 'iso',)


class TradingVolumeline(admin.TabularInline):
    model = TradingVolume
    fields = ('value', 'currency', 'sort')
    extra = 0


@admin.register(Exchange)
class ExchangeAdmin(admin.ModelAdmin):
    list_display = ('name', 'sort',)
    list_editable = ('sort',)
    inlines = [
        TradingVolumeline,
    ]
