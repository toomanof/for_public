from django.db.models import Manager
from django.db.models import Model
from django.contrib import admin
from django.db.models import ForeignKey
from django.db.models import CASCADE
from django.db.models import CharField
from django.db.models import IntegerField


class DescManager(Manager):
    pass


class Desc(Model):
    ADVANTAGE = 'advantage'
    DISADVANTAGE = 'disadvantage'

    TYPE_CHOICES = (
        (ADVANTAGE, 'преимущество'),
        (DISADVANTAGE, 'недостаток')
    )

    user = ForeignKey('web.User', on_delete=CASCADE,
                      null=True, default=None, blank=True,
                      verbose_name='клиент', related_name='user_exchange_descs')
    exchange = ForeignKey('Exchange', on_delete=CASCADE,
                          verbose_name='Биржа', related_name='descs')
    type = CharField('тип', max_length=20, choices=TYPE_CHOICES)
    text = CharField('значение', max_length=1000)
    count = IntegerField(default=0)

    objects = DescManager()

    class Meta:
        verbose_name = 'преимущество / недостаток'
        verbose_name_plural = 'преимущества / недостатки'
        ordering = ['-count']

    def __str__(self):
        return self.text


@admin.register(Desc)
class DescAdmin(admin.ModelAdmin):
    list_filter = ('type', 'exchange')
    list_display = (
        'text',
        'exchange',
        'type',
        'count',
        'user'
    )
