from django.urls import re_path
from .import views as v

urlpatterns = [
    re_path(
        r'^(?P<version>(v1))/content/menu$',
        v.MenuView.as_view(),
        name='menu'),

    re_path(
        r'^(?P<version>(v1))/content/menu_right$',
        v.MenuRightView.as_view(),
        name='menu_right'),

    re_path(
        r'^(?P<version>(v1))/content/menu_right_link$',
        v.MenuRightLinkView.as_view(),
        name='menu_right_link'),

    re_path(
        r'^(?P<version>(v1))/content/menu_footer$',
        v.MenuFooterView.as_view(),
        name='menu_footer'),

    re_path(
        r'^(?P<version>(v1))/content/footer_link$',
        v.FooterLinkView.as_view(),
        name='footer_link$'),

    re_path(
        r'^(?P<version>(v1))/content/menu_top$',
        v.MenuTopView.as_view(),
        name='menu_top'),

    re_path(
        r'^(?P<version>(v1))/content/page_analytic$',
        v.PageAnalyticView.as_view(),
        name='page_analytic'),

    re_path(
        r'^(?P<version>(v1))/content/page_tool$',
        v.PageToolView.as_view(),
        name='page_tool'),

    re_path(
        r'^(?P<version>(v1))/content/home_slider$',
        v.HomeSliderView.as_view(),
        name='home_slider'),

    re_path(
        r'^(?P<version>(v1))/content/main-home-slider$',
        v.MainHomeSliderView.as_view(),
        name='main_home_slider'),

    re_path(
        r'^(?P<version>(v1))/content/home_why$',
        v.HomeWhyView.as_view(),
        name='home_why'),

    re_path(
        r'^(?P<version>(v1))/content/home_benefits$',
        v.HomeBenefitsView.as_view(),
        name='home_benefits'),

    re_path(
        r'^(?P<version>(v1))/content/signal_slider$',
        v.SignalSliderView.as_view(),
        name='signal_slider'),

    re_path(
        r'^(?P<version>(v1))/content/recommend/pages/main$',
        v.RecommendViewMainPage.as_view(),
        name='recommend'),

    re_path(
        r'^(?P<version>(v1))/content/recommend/pages/lenta$',
        v.RecommendViewLentaPage.as_view(),
        name='recommend_lenta_page'),

    re_path(
        r'^(?P<version>(v1))/content/recommend/pages/signals$',
        v.RecommendViewSignalPage.as_view(),
        name='recommend_signal-page'),

    re_path(
        r'^(?P<version>(v1))/content/menu/(?P<id>\d+)/items$',
        v.MenuContents.as_view(),
        name='menu_contents'),

    re_path(
        r'^(?P<version>(v1))/content/items/(?P<url_md5>\w+)$',
        v.ContentDetail.as_view(),
        name='content'),

    re_path(
        r'^(?P<version>(v1))/content/download_book_request/(?P<book_id>\d+)$',
        v.DownloadBookRequest.as_view(),
        name='download_book_request'),

    re_path(
        r'^(?P<version>(v1))/content/search$',
        v.Search.as_view(),
        name='search'),
    re_path(
        r'^(?P<version>(v1))/content/banners$',
        v.Banners.as_view(),
        name='banners'),

    re_path(
        r'^(?P<version>(v1))/content/top_header_slider$',
        v.TopHeaderSliderView.as_view(),
        name='top_header_slider'),

    re_path(
        r'^(?P<version>(v1))/content/tools_first_slider$',
        v.ToolsFirstSliderView.as_view(),
        name='tools_first_slider'),

    re_path(
        r'^(?P<version>(v1))/content/tools_second_slider$',
        v.ToolsSecondSliderView.as_view(),
        name='tools_second_slider'),

    re_path(
        r'^(?P<version>(v1))/content/articles$',
        v.ArticlesView.as_view(),
        name='articles'),
]
