from rest_framework.serializers import ModelSerializer
from rest_framework.serializers import CharField
from rest_framework.serializers import ModelField
from rest_framework.serializers import SerializerMethodField

from web.serializers import CountrySerializer
from web.serializers import UserProfileSerializer

from content.models import Content
from content.models import DownloadBook
from content.models import HomeSlider
from content.models import MainHomeSlider
from content.models import HomeBenefit
from content.models import HomeWhy
from content.models import Menu
from content.models import MenuRight
from content.models import MenuRightLink
from content.models import MenuFooter
from content.models import MenuTop
from content.models import PageTool
from content.models import Recommend
from content.models import SignalSlider
from content.models import FooterLink
from content.models import Banner
from content.models import TopHeaderSlider
from content.models import ToolsFirstSlider
from content.models import ToolsSecondSlider


class MainHomeSliderSerializer(ModelSerializer):
    """
    pic_url not used currently
    Uploading svg to S3 might have problems
    """
    class Meta:
        model = MainHomeSlider
        fields = (
            'id',
            'title',
            'content',
            'css',
            'sort'
        )


class HomeSliderSerializer(ModelSerializer):
    class Meta:
        model = HomeSlider
        fields = (
            'id',
            'title',
            'text',
            'pic_url',
            'pic_hover_url',
            'sort'
        )


class HomeWhySerializer(ModelSerializer):
    class Meta:
        model = HomeWhy
        fields = (
            'id',
            'title',
            'text',
            'pic_url',
        )


class HomeBenefitsSerializer(ModelSerializer):
    class Meta:
        model = HomeBenefit
        fields = (
            'id',
            'title',
            'fnumber',
            'pic_url',
        )


class MenuSmallSerializer(ModelSerializer):
    class Meta:
        model = Menu
        fields = (
            'id',
            'name',
            'url',
        )


class MenuSerializer(ModelSerializer):
    h1 = CharField(source='get_h1')
    title = CharField(source='get_title')
    breadcrumbs = SerializerMethodField()

    class Meta:
        model = Menu
        fields = (
            'id',
            'name',
            'h1',
            'title',
            'type',
            'breadcrumbs',
            'level',
            'parent',
            'ico_class',
            'class_name',
            'url',
            'pic_url',
            'ico_url',
            'content',
            'is_account',
        )

    def get_breadcrumbs(self, obj):
        if obj.url == '/':
            return None

        l = [{'name': 'Главная', 'url': '/'}]

        for m in obj.get_ancestors(include_self=True):
            d = {'name': m.name, 'url': m.url}
            l.append(d)
        return l


class MenuRightSerializer(ModelSerializer):
    class Meta:
        model = MenuRight
        fields = (
            'id',
            'name',
            'url',
        )


class MenuRightLinkSerializer(ModelSerializer):
    class Meta:
        model = MenuRightLink
        fields = (
            'id',
            'name',
            'url',
        )


class MenuFooterSerializer(ModelSerializer):
    class Meta:
        model = MenuFooter
        fields = (
            'id',
            'name',
            'url',
        )


class FooterLinkSerializer(ModelSerializer):
    class Meta:
        model = FooterLink
        fields = (
            'id',
            'name',
            'file_url',
        )


class MenuTopSerializer(ModelSerializer):
    class Meta:
        model = MenuTop
        fields = (
            'id',
            'name',
            'url',
            'class_name'
        )


class PageAnalyticSerializer(ModelSerializer):
    class Meta:
        model = PageTool
        fields = (
            'id',
            'name',
            'url',
            'ico_url',
            'ico_srcset_url',
            'short_description'
        )


class PageToolSerializer(ModelSerializer):
    class Meta:
        model = PageTool
        fields = (
            'id',
            'name',
            'url',
            'ico_url',
            'ico_srcset_url',
            'short_description'
        )


class RecommendSerializer(ModelSerializer):
    class Meta:
        model = Recommend
        fields = (
            'id',
            'url',
            'pic_url',
            'pic_srcset_url',
        )


class SignalSliderSerializer(ModelSerializer):
    """
    pic_url not used currently
    Uploading svg to S3 might have problems
    """
    class Meta:
        model = SignalSlider
        fields = (
            'id',
            'title',
            'content',
            'button',
            'link',
            'text2',
            'image',
            'pic_white_url',
            'pic_yellow_url',
            'color',
            'sort'
        )


class ContentSerializer(ModelSerializer):
    h1 = CharField(source='get_h1')
    title = CharField(source='get_title')
    tags = MenuSmallSerializer(read_only=True, many=True)

    class Meta:
        model = Content
        fields = (
            'id',
            'title',
            'h1',
            'name',
            'short_description',
            'type',
            'image_type',
            'ico_class',
            'url',
            'pic_url',
            'ico_url',
            'created_at',
            'has_book_file',
            'tags'
        )


class ContentExtSerializer(ContentSerializer):
    menu_item = MenuSerializer(read_only=True)

    class Meta:
        model = Content
        fields = ContentSerializer.Meta.fields + ('body', 'menu_item')


class DownloadBookSerializer(ModelSerializer):
    country = CountrySerializer(read_only=True)
    book = ContentExtSerializer(read_only=True)
    user = UserProfileSerializer(read_only=True)

    class Meta:
        model = DownloadBook
        fields = (
            'id',
            'book',
            'country',
            'user',
            'first_name',
            'last_name',
            'email',
            'phone',
        )


class DownloadBookRequestSerializer(ModelSerializer):
    country_id = ModelField(
        model_field=DownloadBook._meta.get_field('country'))

    class Meta:
        model = DownloadBook
        fields = (
            'country_id',
            'first_name',
            'last_name',
            'email',
            'phone',
        )

    def save(self):
        d = self.validated_data
        d['user'] = self.context.get('user')
        d['book'] = self.context.get('book')
        return DownloadBook.objects.create(**d)


class BannerSerializer(ModelSerializer):
    class Meta:
        model = Banner
        fields = '__all__'


class TopHeaderSliderSerializer(ModelSerializer):
    class Meta:
        model = TopHeaderSlider
        fields = (
            'id',
            'title',
            'text',
            'pic_url',
            'sort',
            'url'
        )


class ToolsFirstSliderSerializer(ModelSerializer):

    class Meta:
        model = ToolsFirstSlider
        fields = (
            'id',
            'title',
            'description',
            'pic',
            'is_visible',
            'sort',
        )


class ToolsSecondSliderSerializer(ModelSerializer):

    class Meta:
        model = ToolsSecondSlider
        fields = (
            'id',
            'title',
            'description',
            'pic',
            'is_visible',
            'sort',
        )