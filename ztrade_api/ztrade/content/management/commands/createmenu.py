import os
import pandas
from collections import OrderedDict
from django.core.management.base import BaseCommand
from django.db import transaction
from django.conf import settings

from content.models import Menu
from content.models import MenuRight
from content.models import MenuRightLink
from content.models import MenuFooter
from content.models import MenuTop
from content.models import PageAnalytic
from content.models import PageTool

from .helpers import get_file_content


class Command(BaseCommand):
    BOOL_FIELDS = (
        'is_use_for_tags',
        'is_account'
    )

    EXCEL_PATH = os.path.join(settings.BASE_DIR,
                              'ztrade/content/management/data/menu.xlsx')

    def get_data(self):
        """
        Returns menu data ordered dict:
        key - temp_id
        value - dict with all other columns data of the row
        """

        path = self.EXCEL_PATH
        df = pandas.read_excel(open(path, 'rb'), sheet_name='Main')

        od = OrderedDict()
        for index, row in df.iterrows():
            rd = row.to_dict()

            for k, v in rd.items():
                if k in self.BOOL_FIELDS:
                    v = self.process_bool(v)

                v = self.process_value(v)

                if str(v).endswith('.txt'):
                    v = get_file_content(v)

                rd[k] = v

            od[rd.pop('temp_id')] = rd
        return od

    def process_bool(self, value):
        if pandas.isnull(value):
            return False

        if value.strip().lower() == 'y':
            return True

        return False

    def process_value(self, value):
        if pandas.isnull(value):
            return None
        return value

    @transaction.atomic
    def create_right_menu(self):
        path = self.EXCEL_PATH
        df = pandas.read_excel(open(path, 'rb'), sheet_name='RightMenu')

        for index, row in df.iterrows():
            rd = row.to_dict()

            MenuRight.objects.create(
                name=rd['name'],
                menu=Menu.objects.get(code=rd['code']),
                sort=(index + 1) * 10
            )

    @transaction.atomic
    def create_right_menu_link(self):
        path = self.EXCEL_PATH
        df = pandas.read_excel(open(path, 'rb'), sheet_name='RightLink')

        for index, row in df.iterrows():
            rd = row.to_dict()

            MenuRightLink.objects.create(
                name=rd['name'],
                menu=Menu.objects.get(code=rd['code']),
                sort=(index + 1) * 10
            )

    @transaction.atomic
    def create_footer_menu(self):
        path = self.EXCEL_PATH
        df = pandas.read_excel(open(path, 'rb'), sheet_name='Footer')

        for index, row in df.iterrows():
            rd = row.to_dict()

            MenuFooter.objects.create(
                name=rd['name'],
                menu=Menu.objects.get(code=rd['code']),
                sort=(index + 1) * 10
            )

    @transaction.atomic
    def create_top_menu(self):
        path = self.EXCEL_PATH
        df = pandas.read_excel(open(path, 'rb'), sheet_name='TopMenu')

        for index, row in df.iterrows():
            rd = row.to_dict()

            MenuTop.objects.create(
                name=rd['name'],
                menu=Menu.objects.get(code=rd['code']),
                sort=(index + 1) * 10
            )

    @transaction.atomic
    def create_page_analytic(self):
        path = self.EXCEL_PATH
        df = pandas.read_excel(open(path, 'rb'), sheet_name='PageAnalytic')

        for index, row in df.iterrows():
            rd = row.to_dict()

            m = PageAnalytic.objects.create(
                name=rd['name'],
                short_description=rd['short_description'],
                menu=Menu.objects.get(code=rd['code']),
                sort=(index + 1) * 10
            )

            m.ico.name = rd['ico']
            m.ico_srcset.name = rd['ico_srcset']
            m.save()

    @transaction.atomic
    def create_page_tool(self):
        path = self.EXCEL_PATH
        df = pandas.read_excel(open(path, 'rb'), sheet_name='PageTool')

        for index, row in df.iterrows():
            rd = row.to_dict()

            m = PageTool.objects.create(
                name=rd['name'],
                short_description=rd['short_description'],
                menu=Menu.objects.get(code=rd['code']),
                sort=(index + 1) * 10
            )

            m.ico.name = rd['ico']
            m.ico_srcset.name = rd['ico_srcset']
            m.save()

    @transaction.atomic
    def create_menu(self):
        od = self.get_data()
        for k, dic in od.items():
            new_dic = dic.copy()
            new_dic.pop('children')

            m = Menu.objects.create(**new_dic)

            if new_dic['pic'] is not None:
                m.pic.name = new_dic['pic']
                m.save()

            if new_dic['ico'] is not None:
                m.ico.name = new_dic['ico']
                m.save()

        # process parents
        for k, dic in od.items():
            if dic['children'] is not None:
                v = str(dic['children'])
                l = v.strip().split(',')
                l = [int(x) for x in l]

                obj = Menu.objects.get(url=dic['url'])

                for tid in l:
                    child = od[tid]
                    child_obj = Menu.objects.get(url=child['url'])
                    child_obj.parent = obj
                    child_obj.save()

    def handle(self, *args, **options):
        Menu.objects.all().delete()
        MenuRight.objects.all().delete()
        MenuRightLink.objects.all().delete()
        MenuFooter.objects.all().delete()
        MenuTop.objects.all().delete()
        PageAnalytic.objects.all().delete()
        PageTool.objects.all().delete()

        self.create_menu()
        self.create_right_menu()
        self.create_right_menu_link()
        self.create_footer_menu()
        self.create_top_menu()
        self.create_page_analytic()
        self.create_page_tool()
