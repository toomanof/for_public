import datetime
import random
from django.core.management.base import BaseCommand
from django.utils import timezone
from django.db import transaction

from content.models import Content
from content.models import Menu

from content.constants import ARTICLE
from content.constants import PIC
from content.constants import ICO

from .helpers import get_file_content


class Command(BaseCommand):
    """
    Actual file upload via "uploadtos3" command.
    """
    CONTENT_ICO = 'images/content/ico/article'
    CONTENT_PIC = 'images/content/pic/article'

    def __init__(self):
        self.short_description = get_file_content('file05.txt')
        self.content = get_file_content('file04.txt')

    def create_articles_ico(self):
        """
        Create 50 items
        """
        for i in range(1, 51):
            days = random.randint(3, 200)

            a = Content.objects.create(
                name='Заголовок {}'.format(str(i).zfill(2)),
                short_description=self.short_description,
                body=self.content,
                type=ARTICLE,
                image_type=ICO,
                url='/some_url_i{}'.format(str(i).zfill(3)),
                created_at=timezone.now() - datetime.timedelta(days=days)
            )
            a.ico.name = self.CONTENT_ICO + str(i).zfill(3) + '.png'
            a.save()

    @transaction.atomic
    def create_articles_pic(self):
        """
        Create 50 items
        """
        for i in range(1, 51):
            days = random.randint(3, 200)

            a = Content.objects.create(
                name='Заголовок {}'.format(str(i).zfill(2)),
                short_description=self.short_description,
                body=self.content,
                type=ARTICLE,
                image_type=PIC,
                url='/some_url_p{}'.format(str(i).zfill(3)),
                created_at=timezone.now() - datetime.timedelta(days=days)
            )
            a.pic.name = self.CONTENT_PIC + str(i).zfill(3) + '.jpg'
            a.save()

    @transaction.atomic
    def set_articles_to_menu(self):
        qs = Content.objects.filter(type=ARTICLE,
                                    image_type=ICO)
        for m in Menu.objects.filter(type=Menu.CONTENT_ICO):
            m.contents.set(qs)

        qs = Content.objects.filter(type=ARTICLE,
                                    image_type=PIC)
        for m in Menu.objects.filter(type=Menu.CONTENT_PIC):
            m.contents.set(qs)

    def handle(self, *args, **options):
        Content.objects.filter(type=ARTICLE).delete()

        self.create_articles_ico()
        self.create_articles_pic()
        self.set_articles_to_menu()
