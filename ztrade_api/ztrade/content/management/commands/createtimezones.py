import pandas
import os
from django.conf import settings
from django.core.management.base import BaseCommand

from web.models import Timezone
from content.management.commands.helpers import get_file_content


class Command(BaseCommand):
    def get_data(self):
        path = os.path.join(settings.BASE_DIR,
                            'ztrade/content/management/data/content.xlsx')
        df = pandas.read_excel(open(path, 'rb'), sheet_name='Timezones')

        l = []
        for index, row in df.iterrows():
            rd = row.to_dict()

            for k, v in rd.items():
                v = self.process_value(v)

                if str(v).endswith('.txt'):
                    v = get_file_content(v)

                rd[k] = v
            l.append(rd)
        return l

    def process_value(self, value):
        if pandas.isnull(value):
            return None
        return value

    def create(self):
        l = []
        for d in self.get_data():
            l.append(Timezone(**d))
        Timezone.objects.bulk_create(l)

    def handle(self, *args, **options):
        self.create()
