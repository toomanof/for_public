import datetime
import random
import pandas
import os
from django.conf import settings
from django.core.management.base import BaseCommand
from django.utils import timezone
from django.db import transaction

from content.models import Content
from content.models import Menu

from content.constants import BOOK

from .helpers import get_file_content


class Command(BaseCommand):
    def __init__(self):
        self.content = get_file_content('file04.txt')

    def get_data(self):
        path = os.path.join(settings.BASE_DIR,
                            'ztrade/content/management/data/content.xlsx')
        df = pandas.read_excel(open(path, 'rb'), sheet_name='Books')

        l = []
        for index, row in df.iterrows():
            rd = row.to_dict()

            for k, v in rd.items():
                v = self.process_value(v)

                if str(v).endswith('.txt'):
                    v = get_file_content(v)
                rd[k] = v
            l.append(rd)
        return l

    def process_value(self, value):
        if pandas.isnull(value):
            return None
        return value

    def create_books(self):
        i = 0
        for d in self.get_data():
            i += 1
            days = random.randint(3, 200)

            d['created_at'] = timezone.now() - datetime.timedelta(days=days)
            d['body'] = self.content
            d['type'] = BOOK
            c = Content.objects.create(**d)

            if d['book_file'] is not None:
                c.book_file.name = d['book_file']
                c.save()

    @transaction.atomic
    def set_books_to_menu(self):
        qs = Content.objects.filter(type=BOOK)
        for m in Menu.objects.filter(type=Menu.BOOKS):
            m.contents.set(qs)

    def handle(self, *args, **options):
        Content.objects.filter(type=BOOK).delete()

        self.create_books()
        self.set_books_to_menu()
