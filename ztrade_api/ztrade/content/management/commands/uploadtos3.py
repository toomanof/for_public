import os
from django.core.management.base import BaseCommand
from django.conf import settings

from web.services.helpers import get_s3_client


class Command(BaseCommand):
    """
    Upload pics to S3
    """
    FOLDER_PATH = os.path.join(settings.BASE_DIR,
                               'ztrade/content/management/data')

    CONTENT_ICO = 'images/content/ico/article.png'
    CONTENT_PIC = 'images/content/pic/article.jpg'

    PICS = (
        'images/home/hometopbox1.svg',
        'images/home/hometopbox2.svg',
        'images/home/hometopbox3.svg',
        'images/home/hometopbox4.svg',
        'images/home/hometopbox5.svg',
        'images/home/hometopbox6.svg',
        'images/home/hometopbox7.svg',
        'images/home/hometopbox1-hover.svg',
        'images/home/hometopbox2-hover.svg',
        'images/home/hometopbox3-hover.svg',
        'images/home/hometopbox4-hover.svg',
        'images/home/hometopbox5-hover.svg',
        'images/home/hometopbox6-hover.svg',
        'images/home/hometopbox7-hover.svg',
        'images/home/home-1.png',
        'images/home/home-2.png',
        'images/home/home-3.png',
        'images/home/home-4.png',
        'images/home/home-5.png',
        'images/home/home-6.png',
        'images/home/home-7.png',
        'images/home/home-1-2x.png',
        'images/home/home-2-2x.png',
        'images/home/home-3-2x.png',
        'images/home/home-4-2x.png',
        'images/home/home-5-2x.png',
        'images/home/home-6-2x.png',
        'images/home/home-7-2x.png',
        'images/svg/instrumenty-white.svg',
        'images/svg/shop-white.svg',
        'images/svg/torg-white.svg',
        'images/svg/partneram-white.svg',
        'images/svg/investirovat-white.svg',
        'images/svg/obuchenie-white.svg',
        'images/svg/brokery-white.svg',
        'images/svg/instrumenty-yellow.svg',
        'images/svg/shop-yellow.svg',
        'images/svg/torg-yellow.svg',
        'images/svg/partneram-yellow.svg',
        'images/svg/investirovat-yellow.svg',
        'images/svg/obuchenie-yellow.svg',
        'images/svg/brokery-yellow.svg',
        'images/svg/icon-bubble.svg',
        'images/svg/icon-clock.svg',
        'images/svg/icon-layers.svg',
        'images/svg/icon-shield.svg',
        'images/svg/opbtn2.svg'
    )

    def __init__(self):
        self.client = get_s3_client()

    def get_content_type(self, filepath):
        if filepath.endswith('.jpg'):
            return 'image/jpeg'
        if filepath.endswith('.png'):
            return 'image/png'
        if filepath.endswith('.svg'):
            return 'image/svg+xml'
        if filepath.endswith('.pdf'):
            return 'application/pdf'
        if filepath.endswith('.mp4'):
            return 'video/mp4'
        return None

    def upload(self, key, filepath):
        extra = {'ACL': 'public-read'}

        ctype = self.get_content_type(filepath)
        if ctype is not None:
            extra['ContentType'] = ctype

        self.client.upload_file(
            filepath,
            settings.AWS_STORAGE_BUCKET_NAME,
            key,
            extra)
        print(key)

    def upload_articles(self):
        """
        Make 100 of each ico, pic and content
        """
        filepath = os.path.join(self.FOLDER_PATH, self.CONTENT_ICO)
        for i in range(1, 101):
            key = self.CONTENT_ICO.rstrip('.png') + str(i).zfill(3) + '.png'
            self.upload(key, filepath)

        filepath = os.path.join(self.FOLDER_PATH, self.CONTENT_PIC)
        for i in range(1, 101):
            key = self.CONTENT_PIC.rstrip('.jpg') + str(i).zfill(3) + '.jpg'
            self.upload(key, filepath)

        filepath = os.path.join(self.FOLDER_PATH, self.CONTENT_PIC)
        for i in range(1, 101):
            key = '{}content{}.jpg'.format(
                self.CONTENT_PIC.rstrip('article.jpg'), str(i).zfill(3))
            self.upload(key, filepath)

    def upload_images(self):
        for key in self.PICS:
            filepath = os.path.join(self.FOLDER_PATH, key)
            self.upload(key, filepath)

    def upload_icons(self):
        """
        Upload all icons from "icon" folder
        """
        folder = self.FOLDER_PATH + '/images/icons'
        l = os.listdir(folder)
        for file in l:
            filepath = os.path.join(folder, file)
            key = 'images/icons/' + file
            self.upload(key, filepath)

    def upload_pics(self):
        """
        Upload all icons from "pics" folder
        """
        folder = self.FOLDER_PATH + '/images/pics'
        l = os.listdir(folder)
        for file in l:
            filepath = os.path.join(folder, file)
            key = 'images/pics/' + file
            self.upload(key, filepath)

    def upload_img_images(self):
        """
        Upload all icons from "img" folder
        """
        folder = self.FOLDER_PATH + '/images/img'
        l = os.listdir(folder)
        for file in l:
            filepath = os.path.join(folder, file)
            key = 'images/' + file
            self.upload(key, filepath)

    def upload_pay_images(self):
        """
        Upload all icons from "pay" folder
        """
        folder = self.FOLDER_PATH + '/images/pay'
        l = os.listdir(folder)
        for file in l:
            filepath = os.path.join(folder, file)
            key = 'images/pay/' + file
            self.upload(key, filepath)

    def upload_sitebar_logo_images(self):
        """
        Upload all icons from "sitebar_logo" folder
        """
        folder = self.FOLDER_PATH + '/images/sitebar_logo'
        l = os.listdir(folder)
        for file in l:
            filepath = os.path.join(folder, file)
            key = 'images/sitebar_logo/' + file
            self.upload(key, filepath)

    def upload_logos(self):
        """
        Upload all icons from "logos/regular" folder
        """
        folder = self.FOLDER_PATH + '/images/logos/regular'
        l = os.listdir(folder)
        for file in l:
            filepath = os.path.join(folder, file)
            key = 'images/logos/regular/' + file
            self.upload(key, filepath)

    def upload_logos_white(self):
        """
        Upload all icons from "logos/white" folder
        """
        folder = self.FOLDER_PATH + '/images/logos/white'
        l = os.listdir(folder)
        for file in l:
            filepath = os.path.join(folder, file)
            key = 'images/logos/white/' + file
            self.upload(key, filepath)

    def upload_logos_circle(self):
        """
        Upload all icons from "logos/circle" folder
        """
        folder = self.FOLDER_PATH + '/images/logos/circle'
        l = os.listdir(folder)
        for file in l:
            filepath = os.path.join(folder, file)
            key = 'images/logos/circle/' + file
            self.upload(key, filepath)

    def upload_books(self):
        """
        Upload all books from "books" folder
        """
        folder = self.FOLDER_PATH + '/books'
        l = os.listdir(folder)
        for file in l:
            filepath = os.path.join(folder, file)
            key = 'books/' + file
            self.upload(key, filepath)

    def upload_strategy_pics(self):
        """
        Upload pics for strategies.
        Folder /images/strategy contains "strategy.jpg"
        Create 100 copies: strategy01.jpg, strategy02.jpg, etc
        """
        filepath = self.FOLDER_PATH + '/images/strategy/strategy.jpg'
        for i in range(1, 101):
            key = 'images/strategy/strategy{}.jpg'.format(str(i).zfill(2))
            self.upload(key, filepath)

    def upload_product_pics(self):
        """
        Upload pics for products.
        Folder /images/product contains "product.png"
        Create 100 copies: product01.png, product02.png, etc
        """
        for i in range(1, 8):
            name = 'product{}.png'.format(str(i).zfill(2))
            filepath = self.FOLDER_PATH + \
                '/images/product_page/{}'.format(name)
            key = 'shop/pics/1/{}'.format(name)
            self.upload(key, filepath)

    def upload_product_files(self):
        """
        Upload files for products.
        Folder /images/product contains "product.xlsx"
        Create 100 copies: product01.xlsx, product02.xlsx, etc
        """
        filepath = self.FOLDER_PATH + '/images/product/product.xlsx'
        for i in range(1, 101):
            key = 'shop/files/product{}.xlsx'.format(str(i).zfill(2))
            self.upload(key, filepath)

    def upload_videos(self):
        filepath = self.FOLDER_PATH + '/videos/sample_video.mp4'
        for k in range(1, 20):
            key = 'videos/video{}.mp4'.format(str(k).zfill(2))
            self.upload(key, filepath)

            key = 'video_lessons/video{}.mp4'.format(str(k).zfill(2))
            self.upload(key, filepath)

            key = 'subscription/videos/begin/video{}.mp4'.format(
                str(k).zfill(2))
            self.upload(key, filepath)

            key = 'subscription/videos/video{}.mp4'.format(str(k).zfill(2))
            self.upload(key, filepath)

    def upload_files(self):
        filepath = self.FOLDER_PATH + '/files/file.pdf'
        key = 'tutorial_book/file.pdf'
        self.upload(key, filepath)

        filepath = self.FOLDER_PATH + '/files/file.pdf.zip'
        key = 'tutorial_book/file.pdf.zip'
        self.upload(key, filepath)

    def upload_charts(self):
        filepath = self.FOLDER_PATH + '/images/charts/chart1.jpg'
        for i in range(1, 31):
            key = 'images/brokers/pamm/chart{}.jpg'.format(str(i).zfill(2))
            self.upload(key, filepath)

        filepath = self.FOLDER_PATH + '/images/charts/chart2.jpg'
        for i in range(1, 31):
            key = 'images/brokers/pamm/accounts/chart{}.jpg'\
                .format(str(i).zfill(2))
            self.upload(key, filepath)

    def upload_subscription_pics(self):
        pth = self.FOLDER_PATH + '/images/lk-tools/'

        l = (
            ('icon_1', 's1-col1'),
            ('icon_2', 's1-col2'),
            ('icon_4', 's1-col3'),
            ('icon_1', 's2-col1'),
            ('icon_3', 's2-col2'),
            ('icon_4', 's2-col3'),
        )

        for name_src, name_dst in l:
            file_src = pth + name_src + '.svg'
            key_dst = 'images/subscriptions/' + name_dst + '.svg'
            self.upload(key_dst, file_src)

    def upload_dfiles(self):
        """
        Upload DownloadFile for subscriptions
        """
        l = (
            'download/test/indicator_a1.pdf',
            'download/test/indicator_a2.pdf',
            'download/test/template_a1.pdf',
            'download/test/template_a2.pdf',
            'download/test/indicator_b1.pdf',
            'download/test/indicator_b2.pdf',
            'download/test/template_b1.pdf',
            'download/test/template_b2.pdf',
        )
        filepath = self.FOLDER_PATH + '/files/file.pdf'

        for key in l:
            self.upload(key, filepath)

    def handle(self, *args, **options):
        # self.upload_articles()
        self.upload_images()
        # self.upload_icons()
        # self.upload_pics()
        # self.upload_img_images()
        # self.upload_pay_images()
        # self.upload_sitebar_logo_images()
        # self.upload_logos()
        # self.upload_logos_white()
        # self.upload_logos_circle()
        # self.upload_books()
        # self.upload_strategy_pics()
        # self.upload_product_pics()
        # self.upload_product_files()
        # self.upload_videos()
        # self.upload_files()
        # self.upload_charts()
        # self.upload_subscription_pics()
        # self.upload_dfiles()
