import os
from django.conf import settings


def get_file_content(filename):
    path = os.path.join(settings.BASE_DIR,
                        'ztrade/content/management/data/text',
                        filename)
    return open(path).read()
