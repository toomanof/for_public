import pandas
import os
from django.conf import settings
from django.core.management.base import BaseCommand

from content.models import Recommend


class Command(BaseCommand):
    def get_data(self):
        path = os.path.join(settings.BASE_DIR,
                            'ztrade/content/management/data/content.xlsx')
        df = pandas.read_excel(open(path, 'rb'), sheet_name='Recommend')

        l = []
        for index, row in df.iterrows():
            rd = row.to_dict()

            for k, v in rd.items():
                v = self.process_value(v)
                rd[k] = v
            l.append(rd)
        return l

    def process_value(self, value):
        if pandas.isnull(value):
            return None
        return value

    def create_recommend(self):
        for d in self.get_data():
            r = Recommend.objects.create(**d)

            if d['pic'] is not None:
                r.pic.name = d['pic']
                r.save()

            if d['pic_srcset'] is not None:
                r.pic_srcset.name = d['pic_srcset']
                r.save()

    def handle(self, *args, **options):
        Recommend.objects.all().delete()
        self.create_recommend()
