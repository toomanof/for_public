import os
import pandas
from django.core.management.base import BaseCommand
from django.db import transaction
from django.conf import settings

from content.models import HomeWhy
from .helpers import get_file_content


class Command(BaseCommand):
    def get_data(self):
        path = os.path.join(settings.BASE_DIR,
                            'ztrade/content/management/data/content.xlsx')
        df = pandas.read_excel(open(path, 'rb'), sheet_name='HomeWhy')

        l = []
        for index, row in df.iterrows():
            rd = row.to_dict()

            for k, v in rd.items():
                v = self.process_value(v)

                if str(v).endswith('.txt'):
                    v = get_file_content(v)
                rd[k] = v
            l.append(rd)
        return l

    def process_value(self, value):
        if pandas.isnull(value):
            return None
        return value

    @transaction.atomic
    def process(self):
        for d in self.get_data():
            name = d.pop('pic')
            h = HomeWhy.objects.create(**d)
            h.pic.name = name
            h.save()

    def handle(self, *args, **options):
        HomeWhy.objects.all().delete()
        self.process()
