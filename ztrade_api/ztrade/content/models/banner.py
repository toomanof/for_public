from django.conf import settings
from django.db import models
from django.contrib import admin


class Banner(models.Model):
    name = models.CharField('Название', max_length=100)
    code = models.SlugField('Уникальный код',max_length=100)
    image = models.ImageField(upload_to='images/content/image', null=True,
                              default=None, blank=True)
    image_big = models.ImageField(upload_to='images/content/image',
                                  null=True, default=None, blank=True)
    url = models.CharField('Ссылка', max_length=255)

    class Meta:
        app_label = 'content'
        verbose_name = 'Банер'
        verbose_name_plural = 'Банеры'

    def __str__(self):
        return self.name


@admin.register(Banner)
class BannerAdmin(admin.ModelAdmin):
    list_display = ('code', 'name', 'image', 'image_big', 'url')
    if settings.PRODUCTION:
        list_editable = ('name', 'image', 'image_big', 'url')
        actions = None

        def has_add_permission(self, request):
            return False

        def has_delete_permission(self, request, obj=None):
            return False