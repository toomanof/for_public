from django.contrib import admin
from django.db import models


class ToolsFirstSlider(models.Model):
    title = models.CharField('заголовок', max_length=255)
    description = models.TextField('html контент')
    pic = models.ImageField(upload_to='images/tools', null=True, blank=True)
    is_visible = models.BooleanField('отображать на сайте', default=True)
    sort = models.IntegerField('сортировка', default=0)

    class Meta:
        verbose_name = 'Первый слайдер на странице "Инструменты"'
        verbose_name_plural = 'Первый слайдер на странице "Инструменты"'
        ordering = ['sort']

    def __str__(self):
        return self.title


class ToolsSecondSlider(models.Model):
    title = models.CharField('заголовок', max_length=255)
    description = models.TextField('html контент')
    pic = models.ImageField(upload_to='images/tools', null=True, blank=True)
    is_visible = models.BooleanField('отображать на сайте', default=True)
    sort = models.IntegerField('сортировка', default=0)

    class Meta:
        verbose_name = 'Второй слайдер на странице "Инструменты"'
        verbose_name_plural = 'Второй слайдер на странице "Инструменты"'
        ordering = ['sort']

    def __str__(self):
        return self.title


@admin.register(ToolsFirstSlider)
class ToolsFirstSliderAdmin(admin.ModelAdmin):
    list_display = (
        'title',
        'description',
        'pic',
        'is_visible',
        'sort',
    )
    list_editable = ('is_visible', 'sort',)


@admin.register(ToolsSecondSlider)
class ToolsSecondSliderAdmin(admin.ModelAdmin):
    list_display = (
        'title',
        'description',
        'pic',
        'is_visible',
        'sort',
    )
    list_editable = ('is_visible', 'sort',)