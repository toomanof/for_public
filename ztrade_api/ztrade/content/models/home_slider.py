from django.db.models import Manager
from django.db.models import Model
from django.contrib import admin
from django.conf import settings
from django.db.models import CharField
from django.db.models import TextField
from django.db.models import ImageField
from django.db.models import IntegerField
from django.db.models import BooleanField


class HomeSliderManager(Manager):
    pass


class HomeSlider(Model):
    title = CharField('заголовок', max_length=255)
    text = TextField('текст')
    pic = ImageField(upload_to='images/home')
    pic_hover = ImageField(upload_to='images/home')
    is_visible = BooleanField('отображать на сайте', default=True)
    sort = IntegerField('сортировка', default=0)

    objects = HomeSliderManager()

    class Meta:
        app_label = 'content'
        verbose_name = 'Слайдер на главной'
        verbose_name_plural = 'Слайдер на главной'
        ordering = ['sort']

    def __str__(self):
        return self.title

    @property
    def pic_url(self):
        if not self.pic or not self.pic.name:
            return None
        return '{}/{}'.format(settings.BUCKET_URL, self.pic.name)

    @property
    def pic_hover_url(self):
        if self.pic_hover and self.pic_hover.name:
            return '{}/{}'.format(settings.BUCKET_URL, self.pic_hover.name)
        return None


@admin.register(HomeSlider)
class HomeSliderAdmin(admin.ModelAdmin):
    list_display = (
        'title',
        'text',
        'pic',
        'pic_hover',
        'is_visible',
        'sort',
    )
    list_editable = ('sort',)
