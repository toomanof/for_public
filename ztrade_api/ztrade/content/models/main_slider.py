from django.db.models import Manager
from django.db.models import Model
from django.contrib import admin
from django.conf import settings
from django.db.models import CharField
from django.db.models import TextField
from django.db.models import ImageField
from django.db.models import IntegerField
from django.db.models import BooleanField


class MainHomeSliderManager(Manager):
    pass


class MainHomeSlider(Model):
    title = CharField('заголовок', max_length=255)
    content = TextField('html контент')
    is_visible = BooleanField('отображать на сайте', default=True)
    css = CharField('css', max_length=255)
    sort = IntegerField('сортировка', default=0)

    objects = MainHomeSliderManager()

    class Meta:
        app_label = 'content'
        verbose_name = 'Большой слайдер на главной'
        verbose_name_plural = 'Большой слайдер на главной'
        ordering = ['sort']

    def __str__(self):
        return self.title

    @property
    def image_url(self):
        if not self.image:
            return None
        return '{}/{}'.format(settings.BUCKET_URL, self.image.name)


@admin.register(MainHomeSlider)
class MainHomeSliderAdmin(admin.ModelAdmin):
    list_display = (
        'title',
        'is_visible',
        'css',
        'sort',
    )
    list_editable = ('sort',)
