import datetime
from django.db.models import Manager
from django.db.models import Model
from django.contrib import admin
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.apps import apps
from django.db.models import ForeignKey
from django.db.models import CharField
from django.db.models import EmailField
from django.db.models import FileField
from django.db.models import DateTimeField
from django.db.models import CASCADE

from web.services.helpers import get_md5
from web.services.helpers import get_guid
from web.services.helpers import get_s3_client


def book_upload_to(instance, filename):
    ext = filename.split('.')[-1]
    dt = datetime.date.today()
    return 'books/download/{}/{}.{}'.format(
        get_md5(dt.strftime('%Y-%m')), get_guid(), ext)


class DownloadBookManager(Manager):
    pass


class DownloadBook(Model):
    """
    After user created request to download book,
    create a copy of book file from original source path,
    to this particular item path.
    """
    user = ForeignKey('web.User', on_delete=CASCADE, null=True,
                      default=None, blank=True,
                      verbose_name='пользователь')
    book = ForeignKey('Content', on_delete=CASCADE, verbose_name='книга')
    country = ForeignKey('web.Country', on_delete=CASCADE,
                         verbose_name='страна')
    first_name = CharField('имя', max_length=100)
    last_name = CharField('фамилия', max_length=100)
    email = EmailField('email', max_length=100)
    phone = CharField('телефон', max_length=100)
    book_file = FileField(upload_to=book_upload_to, null=True,
                          default=None, blank=True,
                          verbose_name='файл книги')
    created_at = DateTimeField('дата', auto_now_add=True)

    objects = DownloadBookManager()

    class Meta:
        app_label = 'content'
        verbose_name = 'загрузка книги'
        verbose_name_plural = 'загрузки книг'
        ordering = ['-created_at']

    def __str__(self):
        return str(self.created_at)

    @property
    def book_file_url(self):
        if not self.book_file or not self.book_file.name:
            return None
        return '{}/{}'.format(settings.BUCKET_URL, self.book_file.name)


@receiver(post_save, sender=DownloadBook)
def dnb_created(sender, instance, created, **kwargs):
    """
    1) When DonwloadBook is created, copy original book's file,
       to DownloadBook's own path.

    2) Send email to user. Emails for auth users and not auth users
       are different.
    """
    if created and instance.book is not None and instance.book.has_book_file:
        Email = apps.get_model('web', 'Email')
        Emailer = apps.get_model('web', 'Emailer')

        old_key = instance.book.book_file.name
        new_key = book_upload_to(instance, old_key)

        client = get_s3_client()
        client.copy_object(
            ACL='public-read',
            Bucket=settings.AWS_STORAGE_BUCKET_NAME,
            CopySource={
                'Bucket': settings.AWS_STORAGE_BUCKET_NAME,
                'Key': old_key
            },
            Key=new_key
        )

        instance.book_file.name = new_key
        instance.save()

        if instance.user is None:
            code = Email.DOWNLOAD_BOOK_NOTAUTH
        else:
            code = Email.DOWNLOAD_BOOK_AUTH

        Emailer.objects.process_email(
            code, email=instance.email, dnb=instance)


@admin.register(DownloadBook)
class DownloadBookAdmin(admin.ModelAdmin):
    list_display = (
        'created_at',
        'first_name',
        'last_name',
        'email',
        'phone',
        'country',
        'book',
        'user',
        'book_file',
    )
