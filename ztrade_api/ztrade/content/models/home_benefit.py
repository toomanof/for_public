from django.db.models import Manager
from django.db.models import Model
from django.contrib import admin
from django.db.models import CharField
from django.db.models import SmallIntegerField
from django.db.models import ImageField
from django.conf import settings


class HomeBenefitManager(Manager):
    pass


class HomeBenefit(Model):
    title = CharField('заголовок', max_length=100)
    number = SmallIntegerField('номер')
    pic = ImageField(upload_to='images/home')

    objects = HomeBenefitManager()

    class Meta:
        app_label = 'content'
        verbose_name = 'преимущество на главной'
        verbose_name_plural = 'преимущества на главной'
        ordering = ['number']

    def __str__(self):
        return self.title

    @property
    def pic_url(self):
        if not self.pic or not self.pic.name:
            return None
        return '{}/{}'.format(settings.BUCKET_URL, self.pic.name)

    @property
    def fnumber(self):
        return str(self.number).zfill(2)


@admin.register(HomeBenefit)
class HomeBenefitAdmin(admin.ModelAdmin):
    list_display = ('title', 'number', 'pic')
