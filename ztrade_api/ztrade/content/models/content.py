import datetime
from django.db.models import Manager
from django.db.models import Model
from django.contrib import admin
from django.apps import apps
from django.dispatch import receiver
from django.db.models.signals import post_save
from django.conf import settings
from django.db.models import CharField
from django.db.models import IntegerField
from django.db.models import ImageField
from django.db.models import TextField
from django.db.models import DateTimeField
from django.db.models import ManyToManyField
from django.db.models import FileField

from content.constants import ARTICLE
from content.constants import BOOK
from content.constants import PIC
from content.constants import ICO

from web.services.helpers import get_md5
from web.services.helpers import get_guid


def book_upload_to(instance, filename):
    ext = filename.split('.')[-1]
    dt = datetime.date.today()
    return 'books/source/{}/{}.{}'.format(
        get_md5(dt.strftime('%Y-%m')), get_guid(), ext)


class ContentManager(Manager):
    pass


class Content(Model):
    TYPE_CHOICES = (
        (ARTICLE, 'Статья'),
        (BOOK, 'Книга'),
    )

    IMAGE_TYPE_CHOICES = (
        (PIC, 'pic'),
        (ICO, 'ico'),
    )

    title = CharField(max_length=255, null=True, default=None, blank=True)
    h1 = CharField(max_length=255, null=True, default=None, blank=True)
    name = CharField('название', max_length=255)
    short_description = TextField('краткое описание', null=True,
                                  default=None, blank=True)
    type = CharField('тип', max_length=50, null=True, default=None, blank=True,
                     choices=TYPE_CHOICES)
    image_type = CharField('тип картинки', max_length=50, null=True,
                           default=None, blank=True,
                           choices=IMAGE_TYPE_CHOICES)
    body = TextField('контент', null=True,
                     default=None, blank=True)
    ico_class = CharField(max_length=50, null=True, default=None, blank=True)
    url = CharField(max_length=255, unique=True)
    url_md5 = CharField(max_length=50, null=True, default=None, blank=True,
                        help_text='Indexed automatically')
    pic = ImageField(upload_to='images/content/pic', null=True,
                     default=None, blank=True)
    ico = ImageField(upload_to='images/content/ico', null=True,
                     default=None, blank=True)
    book_file = FileField(upload_to=book_upload_to, null=True,
                          default=None, blank=True,
                          verbose_name='файл книги')
    created_at = DateTimeField('дата')
    sort = IntegerField('сортировка', default=0)
    menu_items = ManyToManyField('Menu', related_name='contents',
                                 verbose_name='пункты меню')
    tags = ManyToManyField('Menu', related_name='tag_contents',
                           verbose_name='теги')

    objects = ContentManager()

    class Meta:
        app_label = 'content'
        verbose_name = 'Контент'
        verbose_name_plural = 'Контент'
        ordering = ['-created_at']

    def __str__(self):
        return self.name

    @property
    def menu_item(self):
        """
        Returns first menu item
        """
        return self.menu_items.first()

    @property
    def has_book_file(self):
        if self.book_file and self.book_file.name:
            return True
        return False

    @property
    def pic_url(self):
        if not self.pic or not self.pic.name:
            return None
        return '{}/{}'.format(settings.BUCKET_URL, self.pic.name)

    @property
    def ico_url(self):
        if not self.ico or not self.ico.name:
            return None
        return '{}/{}'.format(settings.BUCKET_URL, self.ico.name)

    @property
    def book_file_url(self):
        if not self.book_file or not self.book_file.name:
            return None
        return '{}/{}'.format(settings.BUCKET_URL, self.book_file.name)

    def get_h1(self):
        if self.h1:
            return self.h1
        return self.name

    def get_title(self):
        if self.title:
            return self.title
        return self.name

    def save(self, *args, **kwargs):
        self.url_md5 = get_md5(self.url)
        super().save(*args, **kwargs)


@receiver(post_save, sender=Content)
def create_tags(sender, instance, created, **kwargs):
    """
    When content created, auto attach 2 tags to content
    """
    if created:
        Menu = apps.get_model('content', 'Menu')
        qs = Menu.objects.filter(is_use_for_tags=True).order_by('?')[:2]
        instance.tags.set(qs)


@admin.register(Content)
class ContentAdmin(admin.ModelAdmin):
    list_display_fields = (
        'name',
        'type',
        'image_type',
        'ico_class',
        'url',
        'created_at',
        'menu_item',
    )
    exclude = ('sort',)
    list_filter = (
        'type',
        'image_type',
        'created_at'
    )
    readonly_fields = ('url_md5',)

    def has_book(self, obj):
        return obj.has_book_file

    has_book.short_description = 'книга загружена'
    has_book.boolean = True

    def changelist_view(self, request, extra_context=None):
        filter = request.GET.get('type__exact', None)
        if filter == 'book':
            self.list_display = self.list_display_fields + ('has_book',)
        else:
            self.list_display = self.list_display_fields

        return super().changelist_view(request, extra_context)
