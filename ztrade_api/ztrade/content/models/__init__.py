from .content import Content
from .download_book import DownloadBook
from .home_slider import HomeSlider
from .home_benefit import HomeBenefit
from .home_why import HomeWhy
from .menu import Menu
from .menu_footer import MenuFooter
from .menu_right import MenuRight
from .menu_right_link import MenuRightLink
from .menu_top import MenuTop
from .page_analytic import PageAnalytic
from .page_tool import PageTool
from .recommend import Recommend
from .signal_slider import SignalSlider
from .menu_footer_link import FooterLink
from .banner import Banner
from .top_header_slider import TopHeaderSlider
from .tools_sliders import *
from .main_slider import *
