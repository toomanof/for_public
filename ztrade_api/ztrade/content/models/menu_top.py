from django.db.models import Manager
from django.db.models import Model
from django.contrib import admin
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.db.models import CharField
from django.db.models import ForeignKey
from django.db.models import CASCADE
from django.db.models import IntegerField


class MenuTopManager(Manager):
    pass


class MenuTop(Model):
    name = CharField('название', max_length=100)
    menu = ForeignKey('Menu', on_delete=CASCADE)
    class_name = CharField(max_length=100, null=True, default=None,
                           blank=True)
    url = CharField(max_length=255, help_text='Auto indexed')
    sort = IntegerField('сортировка', default=0)

    objects = MenuTopManager()

    class Meta:
        app_label = 'content'
        verbose_name = 'Меню верхнее'
        verbose_name_plural = 'Меню верхнее'
        ordering = ['sort']

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.url = self.menu.url
        super().save(*args, **kwargs)


@receiver(post_save, sender=MenuTop)
def set_class_name(sender, instance, created, **kwargs):
    """
    Last 4 items in top menu should have class_name "item"
    Others should not have this class_name
    For mobile menu viceversa.
    """
    MenuTop.objects.update(class_name=None)
    ids = MenuTop.objects.values_list('id', flat=True).order_by('-sort')[:4]
    MenuTop.objects.filter(id__in=ids).update(class_name='item')


@admin.register(MenuTop)
class MenuTopAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'menu',
        'sort',
    )
    list_editable = ('sort',)
    exclude = ('url', 'class_name')
