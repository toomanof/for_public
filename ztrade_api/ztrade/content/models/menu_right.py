from django.db.models import Manager
from django.db.models import Model
from django.contrib import admin
from django.db.models import CharField
from django.db.models import ForeignKey
from django.db.models import IntegerField
from django.db.models import CASCADE


class MenuRightManager(Manager):
    pass


class MenuRight(Model):
    name = CharField('название', max_length=100)
    menu = ForeignKey('Menu', on_delete=CASCADE)
    url = CharField(max_length=255, help_text='Auto indexed')
    sort = IntegerField('сортировка', default=0)

    objects = MenuRightManager()

    class Meta:
        app_label = 'content'
        verbose_name = 'Меню правое'
        verbose_name_plural = 'Меню правое'
        ordering = ['sort']

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.url = self.menu.url
        super().save(*args, **kwargs)


@admin.register(MenuRight)
class MenuRightAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'menu',
        'sort'
    )
    list_editable = ('sort',)
    exclude = ('url',)
