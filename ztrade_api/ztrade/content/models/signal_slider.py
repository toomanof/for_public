from django.db.models import Manager
from django.db.models import Model
from django.contrib import admin
from django.conf import settings
from django.db.models import CharField
from django.db.models import TextField
from django.db.models import ImageField
from django.db.models import IntegerField
from django.db.models import BooleanField


class SignalSliderManager(Manager):
    pass


DEFAULT = 'default'
BLUE = 'blue'
ROYAL = 'royal'
COLOR_CHOICES = ((DEFAULT, 'по умолчанию'),
                 (BLUE, 'голубой'), (ROYAL, 'тёмно-синий'))


class SignalSlider(Model):
    title = CharField('заголовок', max_length=255)
    content = TextField('html контент')
    color = CharField('цвет', default=DEFAULT,
                      choices=COLOR_CHOICES, max_length=10)
    button = CharField('кнопка', max_length=255)
    link = CharField('ссылка', max_length=255)
    text2 = CharField('текст около кнопки', max_length=255)
    image = CharField('картинка', max_length=255, null=True,
                      default=None, blank=True)
    pic_white = ImageField(upload_to='images/home')
    pic_yellow = ImageField(upload_to='images/home')
    is_visible = BooleanField('отображать на сайте', default=True)
    sort = IntegerField('сортировка', default=0)

    objects = SignalSliderManager()

    class Meta:
        app_label = 'content'
        verbose_name = 'Слайдер 2'
        verbose_name_plural = 'Слайдер 2'
        ordering = ['sort']

    def __str__(self):
        return self.title

    @property
    def pic_white_url(self):
        if not self.pic_white or not self.pic_white.name:
            return None
        return '{}/{}'.format(settings.BUCKET_URL, self.pic_white.name)

    @property
    def pic_yellow_url(self):
        if not self.pic_yellow or not self.pic_yellow.name:
            return None
        return '{}/{}'.format(settings.BUCKET_URL, self.pic_yellow.name)


@admin.register(SignalSlider)
class SignalSliderAdmin(admin.ModelAdmin):
    list_display = (
        'title',
        'button',
        'link',
        'text2',
        'is_visible',
        'sort',
    )
    list_editable = ('sort',)
