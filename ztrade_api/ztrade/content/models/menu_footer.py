from django.db.models import Manager
from django.db.models import Model
from django.contrib import admin
from django.db.models import CharField
from django.db.models import ForeignKey
from django.db.models import CASCADE
from django.db.models import IntegerField


class MenuFooterManager(Manager):
    pass


class MenuFooter(Model):
    name = CharField('название', max_length=100)
    menu = ForeignKey('Menu', on_delete=CASCADE)
    url = CharField(max_length=255, help_text='Auto indexed')
    sort = IntegerField('сортировка', default=0)

    objects = MenuFooterManager()

    class Meta:
        app_label = 'content'
        verbose_name = 'Меню футера'
        verbose_name_plural = 'Меню футера'
        ordering = ['sort']

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.url = self.menu.url
        super().save(*args, **kwargs)


@admin.register(MenuFooter)
class MenuFooterAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'menu',
        'sort'
    )
    list_editable = ('sort',)
    exclude = ('url',)
