from django.db.models import Manager
from django.contrib import admin
from mptt.models import MPTTModel
from mptt.models import TreeForeignKey
from django.conf import settings
from django.db.models import CharField
from django.db.models import BooleanField
from django.db.models import IntegerField
from django.db.models import ImageField
from django.db.models import TextField
from django.db.models import SET_NULL


class MenuManager(Manager):
    pass


class Menu(MPTTModel):
    CONTENT = 'content'
    BOOKS = 'books'
    CONTENT_PIC = 'content_pic'
    CONTENT_ICO = 'content_ico'

    TYPE_CHOICES = (
        (CONTENT, 'Контент'),
        (BOOKS, 'Книги'),
        (CONTENT_PIC, 'Контент (pic)'),
        (CONTENT_ICO, 'Контент (ico)'),
    )

    title = CharField(max_length=255, null=True, default=None, blank=True)
    h1 = CharField(max_length=255, null=True, default=None, blank=True)
    name = CharField('название', max_length=100)
    code = CharField('код', max_length=4, null=True, default=None, blank=True)
    short_description = CharField('краткое описание', max_length=100,
                                  null=True, default=None, blank=True)
    parent = TreeForeignKey('self', null=True, blank=True,
                            related_name='children', db_index=True,
                            on_delete=SET_NULL)
    type = CharField('тип', max_length=50, null=True, default=None, blank=True,
                     choices=TYPE_CHOICES)
    content = TextField('контент', null=True,
                        default=None, blank=True)
    ico_class = CharField(max_length=50, null=True, default=None, blank=True)
    class_name = CharField(max_length=50, null=True, default=None, blank=True)
    url = CharField(max_length=255, unique=True)
    pic = ImageField(upload_to='images/menu/pic', null=True,
                     default=None, blank=True)
    ico = ImageField(upload_to='images/icons', null=True,
                     default=None, blank=True)
    is_account = BooleanField('меню личного кабинета', default=False)
    is_use_for_tags = BooleanField(default=False,
                                   verbose_name='использовать для авто тегов')
    sort = IntegerField('сортировка', default=0)

    objects = MenuManager()

    class Meta:
        app_label = 'content'
        verbose_name = 'Меню'
        verbose_name_plural = 'Меню'
        ordering = ['level', 'sort']

    def __str__(self):
        return '{} ({})'.format(self.name, self.url)

    @property
    def pic_url(self):
        if not self.pic or not self.pic.name:
            return None
        return '{}/{}'.format(settings.BUCKET_URL, self.pic.name)

    @property
    def ico_url(self):
        if not self.ico or not self.ico.name:
            return None
        return '{}/{}'.format(settings.BUCKET_URL, self.ico.name)

    def get_h1(self):
        if self.h1:
            return self.h1
        return self.name

    def get_title(self):
        if self.title:
            return self.title
        return self.name


@admin.register(Menu)
class MenuAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'class_name',
        'code',
        'parent',
        'type',
        'title',
        'h1',
        'level',
        'url',
        'is_account',
        'is_use_for_tags',
        'sort'
    )
    list_filter = ('is_account',)
    list_editable = ('sort',)
