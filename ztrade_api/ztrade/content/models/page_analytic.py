from django.db.models import Manager
from django.db.models import Model
from django.contrib import admin
from django.conf import settings
from django.db.models import CharField
from django.db.models import ForeignKey
from django.db.models import IntegerField
from django.db.models import ImageField
from django.db.models import CASCADE


class PageAnalyticManager(Manager):
    pass


class PageAnalytic(Model):
    name = CharField('название', max_length=100)
    menu = ForeignKey('Menu', on_delete=CASCADE)
    ico = ImageField(upload_to='images/icons', null=True,
                     default=None, blank=True)
    ico_srcset = ImageField(upload_to='images/icons', null=True,
                            default=None, blank=True)
    url = CharField(max_length=255, help_text='Auto indexed')
    short_description = CharField('краткое описание', max_length=1000,
                                  null=True, default=None, blank=True)
    sort = IntegerField('сортировка', default=0)

    objects = PageAnalyticManager()

    class Meta:
        app_label = 'content'
        verbose_name = 'аналитика'
        verbose_name_plural = 'аналитика'
        ordering = ['sort']

    def __str__(self):
        return self.name

    @property
    def ico_url(self):
        if not self.ico or not self.ico.name:
            return None
        return '{}/{}'.format(settings.BUCKET_URL, self.ico.name)

    @property
    def ico_srcset_url(self):
        if not self.ico_srcset or not self.ico_srcset.name:
            return None
        return '{}/{}'.format(settings.BUCKET_URL, self.ico_srcset.name)

    def save(self, *args, **kwargs):
        self.url = self.menu.url
        super().save(*args, **kwargs)


@admin.register(PageAnalytic)
class PageAnalyticAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'menu',
        'ico',
        'ico_srcset',
        'sort'
    )
    list_editable = ('sort',)
    exclude = ('url',)
