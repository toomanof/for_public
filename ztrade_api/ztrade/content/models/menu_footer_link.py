import uuid
from django.dispatch import receiver
from django.db.models import Model
from django.db.models.signals import post_delete
from django.conf import settings
from django.contrib import admin
from django.db import models


def upload_to(instance, filename):
    ext = filename.split('.')[-1]
    filename = '{}.{}'.format(uuid.uuid4().hex, ext)

    pth = 'download/footer_links_docs/'

    pth += filename
    return pth


class FooterLink(Model):
    name = models.CharField('название', max_length=100)
    sort = models.IntegerField('сортировка', default=0)
    file = models.FileField('файл', upload_to=upload_to,
                            null=True, blank=True)

    class Meta:
        app_label = 'content'
        verbose_name = 'Нижние ссылки ссылки'
        verbose_name_plural = 'Нижние ссылки ссылки'
        ordering = ['sort']

    @property
    def file_url(self):
        if not self.file or not self.file.name:
            return None
        return '{}/{}'.format(settings.BUCKET_URL, self.file.name)


@receiver(post_delete, sender=FooterLink)
def delete_file(sender, instance, **kwargs):
    if instance.file:
        instance.file.delete()

@admin.register(FooterLink)
class FooterLinkAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'file_url',
        'sort'
    )
    list_editable = ('sort',)
