from django.db import models
from django.conf import settings
from django.contrib import admin


class TopHeaderSlider(models.Model):
    title = models.CharField('заголовок', max_length=255)
    text = models.TextField('текст')
    pic = models.ImageField(upload_to='images/home')
    is_visible = models.BooleanField('отображать на сайте', default=True)
    sort = models.IntegerField('сортировка', default=0)
    url = models.CharField('Url', max_length=255, default='/')

    class Meta:
        app_label = 'content'
        verbose_name = 'Слайдер в заголовке главной страницы'
        verbose_name_plural = 'Слайдер в заголовке главной страницы'
        ordering = ['sort']

    def __str__(self):
        return self.title

    @property
    def pic_url(self):
        if not self.pic or not self.pic.name:
            return None
        return '{}/{}'.format(settings.BUCKET_URL, self.pic.name)

    @property
    def pic_hover_url(self):
        if self.pic_hover and self.pic_hover.name:
            return '{}/{}'.format(settings.BUCKET_URL, self.pic_hover.name)
        return None


@admin.register(TopHeaderSlider)
class TopHeaderSliderAdmin(admin.ModelAdmin):
    list_display = (
        'title',
        'text',
        'pic',
        'is_visible',
        'url',
        'sort',
    )
    list_editable = ('sort',)
