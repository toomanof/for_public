from django.db.models import Manager
from django.db.models import Model
from django.contrib import admin
from django.db.models import CharField
from django.db.models import ForeignKey
from django.db.models import IntegerField
from django.db.models import CASCADE


class MenuRightLinkManager(Manager):
    pass


class MenuRightLink(Model):
    name = CharField('название', max_length=100)
    menu = ForeignKey('Menu', on_delete=CASCADE)
    url = CharField(max_length=255, help_text='Auto indexed')
    sort = IntegerField('сортировка', default=0)

    objects = MenuRightLinkManager()

    class Meta:
        app_label = 'content'
        verbose_name = 'Меню правые ссылки'
        verbose_name_plural = 'Меню правые ссылки'
        ordering = ['sort']

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.url = self.menu.url
        super().save(*args, **kwargs)


@admin.register(MenuRightLink)
class MenuRightLinkAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'menu',
        'url',
        'sort'
    )
    list_editable = ('sort',)
