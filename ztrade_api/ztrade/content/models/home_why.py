from django.db.models import Manager
from django.db.models import Model
from django.contrib import admin
from django.conf import settings

from django.db.models import CharField
from django.db.models import ImageField
from django.db.models import IntegerField


class HomeWhyManager(Manager):
    pass


class HomeWhy(Model):
    title = CharField('заголовок', max_length=255)
    text = CharField('текст', max_length=255)
    pic = ImageField(upload_to='images/home')
    sort = IntegerField('сортировка', default=0)

    objects = HomeWhyManager()

    class Meta:
        app_label = 'content'
        verbose_name = 'почему мы на главной'
        verbose_name_plural = 'почему мы на главной'
        ordering = ['sort']

    def __str__(self):
        return self.title

    @property
    def pic_url(self):
        if not self.pic or not self.pic.name:
            return None
        return '{}/{}'.format(settings.BUCKET_URL, self.pic.name)


@admin.register(HomeWhy)
class HomeWhyAdmin(admin.ModelAdmin):
    list_display = ('title', 'text', 'pic', 'sort')
    list_editable = ('sort',)
