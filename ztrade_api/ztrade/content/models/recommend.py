from django.db.models import Model
from django.db.models import Manager
from django.contrib import admin
from django.conf import settings
from django.db.models import CharField
from django.db.models import IntegerField
from django.db.models import ImageField
from django.db.models import SmallIntegerField

class RecommendManager(Manager):
    pass


class Recommend(Model):
    MAIN_PAGE, LENTA_PAGE, SIGNAL_PAGE = range(3)
    CHOICE_LOCATION_PAGES = (
        (MAIN_PAGE, 'Главная страница'),
        (LENTA_PAGE, 'Страница Лента'),
        (SIGNAL_PAGE, 'Страница Сигналы')
    )

    url = CharField(max_length=1000, null=True, default=None, blank=True)
    pic = ImageField(upload_to='images/recommend', null=True,
                     default=None, blank=True)
    pic_srcset = ImageField(upload_to='images/recommend', null=True,
                            default=None, blank=True)
    sort = IntegerField('сортировка', default=0)
    location = SmallIntegerField('Расположение', default=0,
                                  choices=CHOICE_LOCATION_PAGES)

    objects = RecommendManager()

    class Meta:
        app_label = 'content'
        verbose_name = 'рекомендую'
        verbose_name_plural = 'рекомендую'
        ordering = ['sort']

    def __str__(self):
        return self.url

    @property
    def pic_url(self):
        if not self.pic or not self.pic.name:
            return None
        return '{}/{}'.format(settings.BUCKET_URL, self.pic.name)

    @property
    def pic_srcset_url(self):
        if not self.pic_srcset or not self.pic_srcset.name:
            return None
        return '{}/{}'.format(settings.BUCKET_URL, self.pic_srcset.name)


@admin.register(Recommend)
class RecommendAdmin(admin.ModelAdmin):
    list_display = (
        'url',
        'pic',
        'pic_srcset',
        'sort',
        'location',
    )
