# Generated by Django 2.0.4 on 2021-02-18 07:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0016_auto_20210218_0751'),
    ]

    operations = [
        migrations.AlterField(
            model_name='toolsfirstslider',
            name='image',
            field=models.ImageField(blank=True, null=True, upload_to='images/tools'),
        ),
        migrations.AlterField(
            model_name='toolssecondslider',
            name='image',
            field=models.ImageField(blank=True, null=True, upload_to='images/tools'),
        ),
    ]
