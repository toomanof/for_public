# Generated by Django 2.0.4 on 2021-04-26 17:07

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0021_auto_20210426_1004'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='mainhomeslider',
            name='button',
        ),
        migrations.RemoveField(
            model_name='mainhomeslider',
            name='image',
        ),
        migrations.RemoveField(
            model_name='mainhomeslider',
            name='link',
        ),
        migrations.RemoveField(
            model_name='mainhomeslider',
            name='text2',
        ),
    ]
