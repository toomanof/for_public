# Generated by Django 2.0.4 on 2021-04-26 08:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0018_auto_20210218_0816'),
    ]

    operations = [
        migrations.CreateModel(
            name='MainHomeSlider',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255, verbose_name='заголовок')),
                ('content', models.TextField(verbose_name='html контент')),
                ('button', models.CharField(max_length=255, verbose_name='кнопка')),
                ('link', models.CharField(max_length=255, verbose_name='ссылка')),
                ('text2', models.CharField(max_length=255, verbose_name='текст около кнопки')),
                ('background', models.ImageField(upload_to='images/home', verbose_name='фон')),
                ('is_visible', models.BooleanField(default=True, verbose_name='отображать на сайте')),
                ('sort', models.IntegerField(default=0, verbose_name='сортировка')),
            ],
            options={
                'verbose_name': 'Слайдер на главной',
                'verbose_name_plural': 'Слайдер на главной',
                'ordering': ['sort'],
            },
        ),
    ]
