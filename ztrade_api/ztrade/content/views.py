import operator
from functools import reduce

from django.shortcuts import get_object_or_404
from rest_framework.generics import ListAPIView
from rest_framework.generics import RetrieveAPIView
from rest_framework.views import APIView
from rest_framework.response import Response

from django.db.models import Q

from content.serializers import ContentSerializer
from content.serializers import ContentExtSerializer
from content.serializers import DownloadBookSerializer
from content.serializers import DownloadBookRequestSerializer
from content.serializers import HomeSliderSerializer
from content.serializers import MainHomeSliderSerializer
from content.serializers import HomeWhySerializer
from content.serializers import HomeBenefitsSerializer
from content.serializers import MenuSerializer
from content.serializers import MenuRightSerializer
from content.serializers import MenuRightLinkSerializer
from content.serializers import MenuFooterSerializer
from content.serializers import MenuTopSerializer
from content.serializers import PageAnalyticSerializer
from content.serializers import PageToolSerializer
from content.serializers import RecommendSerializer
from content.serializers import SignalSliderSerializer
from content.serializers import FooterLinkSerializer
from content.serializers import BannerSerializer
from content.serializers import TopHeaderSliderSerializer
from content.serializers import ToolsFirstSliderSerializer
from content.serializers import ToolsSecondSliderSerializer

from content.models import Content
from content.models import HomeBenefit
from content.models import HomeSlider
from content.models import MainHomeSlider
from content.models import HomeWhy
from content.models import Menu
from content.models import MenuRight
from content.models import MenuRightLink
from content.models import MenuFooter
from content.models import MenuTop
from content.models import PageAnalytic
from content.models import PageTool
from content.models import Recommend
from content.models import SignalSlider
from content.models import FooterLink
from content.models import Banner
from content.models import TopHeaderSlider
from content.models import ToolsFirstSlider
from content.models import ToolsSecondSlider

from content.constants import ARTICLE
from content.constants import BOOK


class ContentDetail(RetrieveAPIView):
    queryset = Content.objects.all()
    serializer_class = ContentExtSerializer
    lookup_field = 'url_md5'


class DownloadBookRequest(APIView):
    """
    post:
        <h1>Download book request by user</h1>
        <b>Input params:</b><br />
        first_name<br />
        last_name<br />
        email<br />
        phone<br />
        country_id<br />
        <b>Response:</b> created download book object<br />
        <b>Response code:</b> 201
    """

    def post(self, request, book_id, **kwargs):
        book = get_object_or_404(Content, id=book_id, type=BOOK)

        user = request.user if request.user.is_authenticated else None

        ctx = {
            'user': user,
            'book': book
        }
        s = DownloadBookRequestSerializer(data=request.data, context=ctx)
        s.is_valid(raise_exception=True)
        obj = s.save()
        s = DownloadBookSerializer(obj)
        return Response(s.data, status=201)


class HomeSliderView(ListAPIView):
    """
    get:
        <h1>Get home slider</h1>
        <b>Response:</b> list of home slider object<br />
        <b>Response code:</b> 200
    """
    queryset = HomeSlider.objects.filter(is_visible=True)
    serializer_class = HomeSliderSerializer


class MainHomeSliderView(ListAPIView):
    """
    get:
        <h1>Get home slider</h1>
        <b>Response:</b> list of home slider object<br />
        <b>Response code:</b> 200
    """
    queryset = MainHomeSlider.objects.filter(is_visible=True)
    serializer_class = MainHomeSliderSerializer


class HomeWhyView(ListAPIView):
    """
    get:
        <h1>Get home slider</h1>
        <b>Response:</b> list of home slider object<br />
        <b>Response code:</b> 200
    """
    queryset = HomeWhy.objects.filter()
    serializer_class = HomeWhySerializer


class HomeBenefitsView(ListAPIView):
    """
    get:
        <h1>Get home slider</h1>
        <b>Response:</b> list of home slider object<br />
        <b>Response code:</b> 200
    """
    queryset = HomeBenefit.objects.filter()
    serializer_class = HomeBenefitsSerializer


class MenuContents(ListAPIView):
    """
    get:
        <h1>Get contents that belongs to menu item</h1>
        <p>If request contains "is_initial", returns first 10 items.
        To speed up initial page load.</p>
        <b>Response:</b> list of content object<br />
        <b>Response code:</b> 200
    """
    serializer_class = ContentSerializer

    def get_queryset(self):
        m = get_object_or_404(Menu, id=self.kwargs.get('id'))
        qs = m.contents.all()

        is_initial = self.request.query_params.get('is_initial', None)
        if is_initial:
            return qs[:10]
        return qs


class MenuView(ListAPIView):
    """
    get:
        <h1>Get menu</h1>
        <b>Response:</b> list of menu object<br />
        <b>Response code:</b> 200
    """
    queryset = Menu.objects.all()
    serializer_class = MenuSerializer


class MenuRightView(ListAPIView):
    """
    get:
        <h1>Get menu right</h1>
        <b>Response:</b> list of menu right object<br />
        <b>Response code:</b> 200
    """
    queryset = MenuRight.objects.all()
    serializer_class = MenuRightSerializer


class MenuRightLinkView(ListAPIView):
    """
    get:
        <h1>Get menu right links</h1>
        <b>Response:</b> list of menu right link object<br />
        <b>Response code:</b> 200
    """
    queryset = MenuRightLink.objects.all()
    serializer_class = MenuRightLinkSerializer


class MenuFooterView(ListAPIView):
    """
    get:
        <h1>Get menu footer</h1>
        <b>Response:</b> list of menu footer object<br />
        <b>Response code:</b> 200
    """
    queryset = MenuFooter.objects.all()
    serializer_class = MenuFooterSerializer


class FooterLinkView(ListAPIView):
    """
    get:
        <h1>Get menu footer</h1>
        <b>Response:</b> list of menu footer object<br />
        <b>Response code:</b> 200
    """
    queryset = FooterLink.objects.all()
    serializer_class = FooterLinkSerializer


class MenuTopView(ListAPIView):
    """
    get:
        <h1>Get menu top</h1>
        <b>Response:</b> list of menu top object<br />
        <b>Response code:</b> 200
    """
    queryset = MenuTop.objects.all()
    serializer_class = MenuTopSerializer


class PageAnalyticView(ListAPIView):
    """
    get:
        <h1>Get page for "analytics"</h1>
        <b>Response:</b> list of page analytic object<br />
        <b>Response code:</b> 200
    """
    queryset = PageAnalytic.objects.all()
    serializer_class = PageAnalyticSerializer


class PageToolView(ListAPIView):
    """
    get:
        <h1>Get page for "tools"</h1>
        <b>Response:</b> list of page tool object<br />
        <b>Response code:</b> 200
    """
    queryset = PageTool.objects.all()
    serializer_class = PageToolSerializer

class BaseRecommendView(ListAPIView):
    """
    get:
        <h1>Get recommend block</h1>
        <b>Response:</b> list of recommend object<br />
        <b>Response code:</b> 200
    """
    location_page = Recommend.MAIN_PAGE
    serializer_class = RecommendSerializer

    def get_queryset(self):
        return Recommend.objects.filter(location=self.location_page)


class RecommendViewMainPage(BaseRecommendView):
    """
    get:
        <h1>Get recommend block</h1>
        <b>Response:</b> list of recommend object in manin page<br />
        <b>Response code:</b> 200
    """
    pass


class RecommendViewLentaPage(BaseRecommendView):
    """
    get:
        <h1>Get recommend block</h1>
        <b>Response:</b> list of recommend object in lenta page<br />
        <b>Response code:</b> 200
    """
    location_page = Recommend.LENTA_PAGE


class RecommendViewSignalPage(BaseRecommendView):
    """
    get:
        <h1>Get recommend block</h1>
        <b>Response:</b> list of recommend object in signals page<br />
        <b>Response code:</b> 200
    """
    location_page = Recommend.SIGNAL_PAGE


class SignalSliderView(ListAPIView):
    """
    get:
        <h1>Get signal slider</h1>
        <b>Response:</b> list of signal slider object<br />
        <b>Response code:</b> 200
    """
    queryset = SignalSlider.objects.filter(is_visible=True)
    serializer_class = SignalSliderSerializer


class Search(APIView):
    def get_data(self, qs, q, attr):
        words = q.split(' ')

        q_objects = Q()
        words = [x.strip() for x in words if x.strip()]
        query = reduce(
            operator.and_,
            (Q(name__icontains=word) for word in words))
        q_objects.add(Q(query), Q.OR)

        qs = qs.filter(q_objects)

        return [{'name': getattr(obj, attr), 'url': obj.url} for obj in qs]

    def get(self, request, **kwargs):
        q = request.GET.get('query', None)

        if q is None:
            return Response(status_code=400)
        menu = Menu.objects.all()
        articles = Content.objects.filter(type=ARTICLE)
        books = Content.objects.filter(type=BOOK)

        d = {
            'menu': self.get_data(menu, q, 'name'),
            'articles': self.get_data(articles, q, 'name'),
            'books': self.get_data(books, q, 'name')
        }
        return Response(d)


class Banners(APIView):

    def get(self, request, **kwargs):
        code = request.GET.get('code', None)

        if code:
            result = Banner.objects.filter(code=code)
        else:
            result = Banner.objects.all()

        return Response(BannerSerializer(result, many=True).data)


class TopHeaderSliderView(ListAPIView):
    """
    get:
        <h1>Get top header home slider</h1>
        <b>Response:</b> list of top header home slider object<br />
        <b>Response code:</b> 200
    """
    queryset = TopHeaderSlider.objects.filter(is_visible=True)
    serializer_class = TopHeaderSliderSerializer


class ToolsFirstSliderView(ListAPIView):
    """
    get:
        <h1>Get first slider in page tools</h1>
        <b>Response:</b> list of top header home slider object<br />
        <b>Response code:</b> 200
    """
    queryset = ToolsFirstSlider.objects.filter(is_visible=True)
    serializer_class = ToolsFirstSliderSerializer


class ToolsSecondSliderView(ListAPIView):
    """
    get:
        <h1>Get secondary slider in page tools</h1>
        <b>Response:</b> list of top header home slider object<br />
        <b>Response code:</b> 200
    """
    queryset = ToolsSecondSlider.objects.filter(is_visible=True)
    serializer_class = ToolsSecondSliderSerializer


class ArticlesView(ListAPIView):
    queryset = Content.objects.filter(type=ARTICLE)
    serializer_class = ContentSerializer

    def list(self, request, *args, **kwargs):

        if 'limit' in request.GET:
            self.queryset = self.queryset[:int(request.GET['limit'])]
        return super().list(request, *args, **kwargs)
