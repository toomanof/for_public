from django_filters import FilterSet
from django_filters import CharFilter

from shop.models import Product
from web.services.helpers import get_ids


class ProductFilter(FilterSet):
    category_ids = CharFilter(method='filter_category_ids')
    market_ids = CharFilter(method='filter_market_ids')

    class Meta:
        model = Product
        fields = (
            'category_ids',
            'market_ids',
        )

    def filter_category_ids(self, qs, name, value):
        ids = get_ids(value)
        if ids is None or len(ids) == 0:
            return qs

        qs = qs.filter(categories__id__in=ids).distinct()
        return qs

    def filter_market_ids(self, qs, name, value):
        ids = get_ids(value)
        if ids is None or len(ids) == 0:
            return qs

        qs = qs.filter(markets__id__in=ids).distinct()
        return qs
