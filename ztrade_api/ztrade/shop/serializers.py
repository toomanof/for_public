from rest_framework.serializers import ModelSerializer
from rest_framework.serializers import CharField
from rest_framework.serializers import ModelField

from shop.models import Category
from shop.models import Market
from shop.models import Product
from shop.models import ProductPic


class CategorySerializer(ModelSerializer):
    class Meta:
        model = Category
        fields = (
            'id',
            'name',
            'sort',
        )


class MarketSerializer(ModelSerializer):
    class Meta:
        model = Market
        fields = (
            'id',
            'name',
            'sort',
        )


class ProductPicSerializer(ModelSerializer):
    product_id = ModelField(model_field=ProductPic._meta.get_field('product'))

    class Meta:
        model = ProductPic
        fields = (
            'id',
            'product_id',
            'pic_url',
        )


class ProductSerializer(ModelSerializer):
    pics = ProductPicSerializer(many=True)

    class Meta:
        model = Product
        fields = (
            'id',
            'name',
            'title',
            'h1',
            'slug',
            'price',
            'potential_profit',
            'current_profit',
            'created_at',
            'frontend_url',
            'partner_amount',
            'pics'
        )


class ProductExtSerializer(ProductSerializer):
    h1 = CharField(source='get_h1')
    title = CharField(source='get_title')
    categories = CategorySerializer(many=True, read_only=True)
    markets = MarketSerializer(many=True, read_only=True)

    class Meta(ProductSerializer.Meta):
        model = Product
        fields = ProductSerializer.Meta.fields +\
            (
                'h1',
                'title',
                'content',
                'categories',
                'markets'
            )
