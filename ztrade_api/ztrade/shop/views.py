from rest_framework.generics import ListAPIView
from rest_framework.generics import RetrieveAPIView

from shop.serializers import CategorySerializer
from shop.serializers import MarketSerializer
from shop.serializers import ProductSerializer
from shop.serializers import ProductExtSerializer

from shop.models import Category
from shop.models import Market
from shop.models import Product

from shop.filters import ProductFilter


class Categories(ListAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class Markets(ListAPIView):
    queryset = Market.objects.all()
    serializer_class = MarketSerializer


class Products(ListAPIView):
    filter_class = ProductFilter
    queryset = Product.objects.all()
    serializer_class = ProductSerializer


class ProductDetail(RetrieveAPIView):
    lookup_field = 'slug'
    queryset = Product.objects.all()
    serializer_class = ProductExtSerializer
