import datetime
import random
from django.utils import timezone
from django.core.management.base import BaseCommand
from shop.models import Category
from shop.models import Market
from shop.models import Product
from shop.models import ProductPic

from content.management.commands.helpers import get_file_content


class Command(BaseCommand):
    def create_categories(self):
        Category.objects.all().delete()

        categories = (
            'роботы',
            'индикаторы',
            'торговые системы',
            'книги',
        )

        sort = 0
        for name in categories:
            sort += 10
            Category.objects.create(name=name, sort=sort)

    def create_markets(self):
        Market.objects.all().delete()

        markets = (
            'форекс',
            'фондовый рынок',
            'крипто-валютный рынок',
        )

        sort = 0
        for name in markets:
            sort += 10
            Market.objects.create(name=name, sort=sort)

    def create_products(self):
        Product.objects.all().delete()

        qs1 = Category.objects.all()
        qs2 = Market.objects.all()

        sort = 0
        product_name = 'Какой-то товар {}'

        for i in range(1, 101):
            sort += 10
            seconds = random.randint(100000, 5000000)
            pname = product_name.format(str(i).zfill(2))

            p = Product.objects.create(
                name=pname,
                content=get_file_content('file04.txt'),
                slug='product{}'.format(str(i).zfill(2)),
                sort=sort,
                created_at=timezone.now() - datetime.timedelta(seconds=seconds),
                price=random.randint(1000, 3000),
                potential_profit=random.randint(2, 15),
                current_profit=random.randint(1, 2),
                partner_amount=10)

            p.categories.set(qs1)
            p.markets.set(qs2)
            p.file.name = 'shop/files/product{}.xlsx'.format(str(i).zfill(2))
            p.save()

            if p.id == 1:
                p.created_at = timezone.now()
                p.save()

                for i in range(1, 8):
                    ppic = ProductPic.objects.create(product=p, sort=i * 10)

                    name = 'shop/pics/1/product{}.png'.format(str(i).zfill(2))
                    ppic.pic.name = name
                    ppic.save()

    def handle(self, *args, **options):
        self.create_categories()
        self.create_markets()
        self.create_products()
