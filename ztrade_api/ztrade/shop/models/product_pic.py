from django.db.models import Manager
from django.db.models import Model
from django.conf import settings
from django.db.models import ForeignKey
from django.db.models import ImageField
from django.db.models import IntegerField
from django.db.models import CASCADE


def upload_to(instance, filename):
    return 'shop/pics/{}/{}'.format(instance.product.slug, filename)


class ProductPicManager(Manager):
    pass


class ProductPic(Model):
    product = ForeignKey('Product', on_delete=CASCADE, related_name='pics',
                         verbose_name='товар')
    pic = ImageField('файл', upload_to=upload_to, null=True,
                     default=None, blank=True)
    sort = IntegerField(default=0)

    objects = ProductPicManager()

    class Meta:
        app_label = 'shop'
        verbose_name = 'изображение'
        verbose_name_plural = 'изображения'
        ordering = ['sort']

    def __str__(self):
        return str(self.id)

    @property
    def pic_url(self):
        if self.pic and self.pic.name:
            return '{}/{}'.format(settings.BUCKET_URL, self.pic.name)
        return None
