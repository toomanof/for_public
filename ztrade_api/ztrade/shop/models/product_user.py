import datetime
from os.path import basename
from django.utils import timezone
from django.db.models import Manager
from django.db.models import Model
from django.contrib import admin
from django.apps import apps
from django.dispatch import receiver
from django.db.models.signals import post_save
from django.db.models import ForeignKey
from django.db.models import DateTimeField
from django.db.models import CharField
from django.db.models import BooleanField
from django.db.models import SET_NULL
from web.constants import PRODUCT
from web.services.helpers import get_guid


class ProductUserManager(Manager):
    def create_bill(self, user_id, product_id, email, name, phone):
        User = apps.get_model('web', 'User')
        Product = apps.get_model('shop', 'Product')
        Bill = apps.get_model('payment', 'Bill')

        if user_id is not None:
            user = User.objects.get(id=user_id)
        else:
            user = None

        product = Product.objects.get(id=product_id)

        pu = self.model.objects.create(
            user=user,
            product=product,
            email=email,
            name=name,
            phone=phone)

        bill = Bill.objects.create(
            user=user, product_user=pu,
            name=product.name, amount=product.price,
            user_email=email)
        return bill


class ProductUser(Model):
    user = ForeignKey('web.User', on_delete=SET_NULL, null=True,
                      default=None, blank=True)
    product = ForeignKey('shop.Product', on_delete=SET_NULL,
                         null=True, default=None, blank=True,
                         verbose_name='товар')
    download_file = ForeignKey('web.DownloadFile', on_delete=SET_NULL,
                               null=True, default=None, blank=True,
                               verbose_name='загруженный файл')
    name = CharField('имя', max_length=100)
    email = CharField(max_length=255)
    phone = CharField('телефон', max_length=255, null=True,
                      default=None, blank=True)
    created_at = DateTimeField(auto_now_add=True)
    is_paid = BooleanField('оплачен', default=False,
                           help_text='Индексируется автоматически')

    objects = ProductUserManager()

    class Meta:
        app_label = 'shop'
        verbose_name = 'покупка товара'
        verbose_name_plural = 'покупка товара'
        ordering = ['-created_at']

    def __str__(self):
        return str(self.id)

    def create_download_file(self):
        """
        Copy product's file to download file.
        Create donwload file instance.
        """
        if not self.product:
            return

        if not self.product.file or not self.product.file.name:
            return

        DownloadFile = apps.get_model('web', 'DownloadFile')

        old_key = self.product.file.name
        filename = basename(old_key)
        ext = filename.split('.')[-1]

        new_key = 'download/product/' + get_guid() + '.' + ext

        from web.tasks import copy_s3_object
        copy_s3_object.delay(old_key, new_key)

        df = DownloadFile.objects.create(
            name='Покупка товара',
            type=PRODUCT,
            expire_at=timezone.now() + datetime.timedelta(days=7))

        df.file.name = new_key
        df.save()
        ProductUser.objects.filter(id=self.id).update(
            download_file_id=df.id)


@receiver(post_save, sender=ProductUser)
def create_download_file(sender, instance, created, **kwargs):
    """
    When ProductUser created, copy product's file and create
    download_file instance
    """
    if created:
        instance.create_download_file()


@admin.register(ProductUser)
class ProductUserAdmin(admin.ModelAdmin):
    list_display = (
        'created_at',
        'user',
        'name',
        'email',
        'phone',
        'product',
        'download_file',
        'is_paid'
    )

    readonly_fields = (
        'is_paid',
    )

    def has_delete_permission(self, request, obj=None):
        return False
