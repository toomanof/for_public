from django.db.models import Manager
from django.db.models import Model
from django.contrib import admin
from django.db.models import CharField
from django.db.models import IntegerField


class MarketManager(Manager):
    pass


class Market(Model):
    name = CharField('название', max_length=255)
    sort = IntegerField('сортировка', default=0)

    objects = MarketManager()

    class Meta:
        app_label = 'shop'
        verbose_name = 'рынок'
        verbose_name_plural = 'рынки'
        ordering = ['sort']

    def __str__(self):
        return self.name


@admin.register(Market)
class MarketAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'sort',
    )
    list_editable = ('sort',)
