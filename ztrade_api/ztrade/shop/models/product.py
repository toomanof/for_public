from django.db.models import Manager
from django.db.models import Model
from django.contrib import admin
from django.db.models import ImageField
from django.db.models import CharField
from django.db.models import DecimalField
from django.db.models import IntegerField
from django.db.models import ManyToManyField
from django.db.models import DateTimeField
from django.db.models import SlugField
from ckeditor_uploader.fields import RichTextUploadingField

from web.services.helpers import get_guid
from .product_pic import ProductPic


def file_upload_to(instance, filename):
    ext = filename.split('.')[-1]
    return 'shop/files/{}.{}'.format(get_guid(), ext)


class ProductManager(Manager):
    pass


class Product(Model):
    slug = SlugField(max_length=255, unique=True)
    title = CharField(max_length=255, null=True, default=None, blank=True)
    h1 = CharField(max_length=255, null=True, default=None, blank=True)
    name = CharField('название', max_length=255)
    price = DecimalField('цена', max_digits=10, decimal_places=2)
    file = ImageField('файл', upload_to=file_upload_to,
                      null=True, default=None, blank=True)
    content = RichTextUploadingField('контент', null=True,
                                     default=None, blank=True)
    potential_profit = DecimalField('можно заработать',
                                    max_digits=5, decimal_places=1)
    current_profit = DecimalField('текущий доход',
                                  max_digits=5, decimal_places=1)
    created_at = DateTimeField(null=True, default=None, blank=True)
    partner_amount = IntegerField(null=True, default=None, blank=True,
                                  help_text='партнерам')
    sort = IntegerField('сортировка', default=0)
    categories = ManyToManyField('Category', verbose_name='категории')
    markets = ManyToManyField('Market', verbose_name='рынки')

    objects = ProductManager()

    class Meta:
        app_label = 'shop'
        verbose_name = 'товар'
        verbose_name_plural = 'товары'
        ordering = ['sort']

    def __str__(self):
        return self.name

    @property
    def frontend_url(self):
        return '/shop/{}'.format(self.slug)

    def get_h1(self):
        if self.h1:
            return self.h1
        return self.name

    def get_title(self):
        if self.title:
            return self.title
        return self.name


class ProductPicInline(admin.TabularInline):
    model = ProductPic
    fields = ('pic', 'sort')
    extra = 0


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    inlines = (ProductPicInline,)

    list_display = (
        'name',
        'slug',
        'created_at',
        'price',
        'file',
        'potential_profit',
        'current_profit',
        'partner_amount',
        'sort',
    )
    list_editable = ('sort',)
