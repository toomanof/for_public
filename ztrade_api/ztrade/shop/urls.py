from django.urls import re_path
from .import views as v

urlpatterns = [
    re_path(
        r'^(?P<version>(v1))/shop/categories$',
        v.Categories.as_view(),
        name='shop_categories'),

    re_path(
        r'^(?P<version>(v1))/shop/markets$',
        v.Markets.as_view(),
        name='shop_markets'),

    re_path(
        r'^(?P<version>(v1))/shop/products$',
        v.Products.as_view(),
        name='shop_products'),

    re_path(
        r'^(?P<version>(v1))/shop/products/(?P<slug>\w+)$',
        v.ProductDetail.as_view(),
        name='shop_product'),
]
