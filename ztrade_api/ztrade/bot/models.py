from django.db import models


class SubscriptionUser(models.Model):
    id_bot = models.PositiveIntegerField('ID telefram bot')
    username = models.CharField('username in teleram bot', max_length=255)
    created_at = models.DateTimeField('Дата создание записи', auto_now_add=True)
    updated_at = models.DateTimeField('Дата обновление записи', auto_now=True)

    class Meta:
        verbose_name = 'Подписчик'
        verbose_name_plural = 'Подписчики'
        ordering = ['-created_at']
