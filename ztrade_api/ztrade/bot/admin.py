from django.contrib import admin

from bot.models import SubscriptionUser


admin.site.register(SubscriptionUser)


class SubscriptionUserAdmin(admin.ModelAdmin):
    model = SubscriptionUser
    list_display = ('id_bot', 'username', 'created_at', 'updated_at')

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False