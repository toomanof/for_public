from django.views.decorators.csrf import csrf_exempt
from django.urls import path
from django.urls import re_path

from .views import *

app_name = 'bots'

urlpatterns = [
    path('setwebhook', csrf_exempt(bot_setwebhook), name='bot_setwebhook'),
    path('unsetwebhook', csrf_exempt(bot_unsetwebhook), name='bot_unsetwebhook'),
    path('info', csrf_exempt(bot_info), name='bot_info'),
    path('incoming/telegram', csrf_exempt(ViewTelegramBot.as_view()), name="incoming_telegram"),
    path('send-message', csrf_exempt(SendTelegramBot.as_view()), name="send_message_telegram"),
]
