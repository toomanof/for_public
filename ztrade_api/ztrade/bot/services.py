import os
import json
from PIL import Image, ImageDraw, ImageFont
from django.conf import settings
from telebot import TeleBot

from bot.models import SubscriptionUser


class ServiceTelegrmaBot:

    dict_message = None

    def __init__(self, request=None):
        token = settings.TELEGRAM_BOT_TOKEN
        self.request = request
        self.bot = TeleBot(token)

    @property
    def chat_id(self):
        return self.dict_message['message']['chat']['id'] if self.dict_message else 0

    @property
    def chat_username(self):
        return self.dict_message['message']['chat']['first_name'] if self.dict_message else ''

    def set_dict_message(self):
        self.dict_message = json.loads(self.request.body)
        print('set_dict_message', self.dict_message)

    def save_chat_id_to_database(self):
        print('save_chat_id_to_database', self.chat_id, self.chat_username)
        SubscriptionUser.objects.get_or_create(
            id_bot=self.chat_id,
            username=self.chat_username
        )

    def incoming(self):
        self.set_dict_message()
        self.save_chat_id_to_database()

    def set_webhook(self):
        self.bot.set_webhook('https://api.ztrade.ru/bot/incoming/telegram')

    def delete_webhook(self):
        self.bot.delete_webhook()

    def get_info(self):
        self.bot.get_webhook_info()

    def send_message(self,
                     title='Новый сигнал',
                     str_time='UTC time: 10 jun 00:00',
                     tool='USD/JPY',
                     result=None):

        tmp_im_path = os.path.join(settings.MEDIA_ROOT, 'msg_tmp.png')
        self.create_image_msg(title, str_time, tool, result)

        for subscription in SubscriptionUser.objects.all():
            try:
                msg = self.bot.send_photo(subscription.id_bot, photo=open(tmp_im_path, 'rb'))
                msg = self.bot.forward_message(chat_id='@ztradesignals', from_chat_id=subscription.id_bot, message_id=msg.message_id)
                print(msg)
            except Exception as e:
                print(e)

        os.remove(os.path.join(settings.MEDIA_ROOT, 'msg_tmp.png'))
        return "Signal sending"

    def create_image_msg(self,
                         title='Новый сигнал',
                         str_time='UTC time: 10 jun 00:00',
                         tool='USD/JPY',
                         result=None):
        def draw_text(value, index, draw_obj, font, fill='#000000', width=None, y=None):
            if not width:
                width = im.width // 2

            if not y:
                y = 205 + index*35

            draw_obj.text(
                (width, y),
                str(value),
                anchor="mb",
                fill=fill,
                font=font,
                align='center'
            )
        str_time = f'UTC time: {str_time}'
        im_path = os.path.join(settings.MEDIA_ROOT, 'background.png')
        profit = 'Take Profit:'
        loss = 'Stop Loss'
        tmp_im_path = os.path.join(settings.MEDIA_ROOT, 'msg_tmp.png')
        font_path = os.path.join(settings.MEDIA_ROOT, 'FreeSans.ttf')
        font_bold_path = os.path.join(settings.MEDIA_ROOT, 'FreeSansBold.ttf')

        font = ImageFont.truetype(font_path, 18)
        font_title = ImageFont.truetype(font_bold_path, 22)
        im = Image.open(im_path)
        draw_obj = ImageDraw.Draw(im)
        y = None
        if result:
            x_r = 228
            y = 236

        if result and result >= 0:
            title = f'{title} +{result}'
        elif result and result < 0:
            title = f'{title} {result}'

        draw_text(str_time, 0, draw_obj, font)
        draw_text(title, 1, draw_obj, font_title)

        if result and result >= 0:
            draw_text(f'+{result}', 1, draw_obj, font_title, '#008000', width=x_r, y=y)
        elif result and result < 0:
            draw_text(result, 1, draw_obj, font_title, '#ff3333', width=x_r, y=y)

        draw_text(tool, 2, draw_obj, font)
        draw_text(profit, 3, draw_obj, font)
        draw_text(loss, 4, draw_obj, font)
        im.save(tmp_im_path)
