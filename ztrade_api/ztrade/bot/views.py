from django.views.generic import View
from django.http import HttpResponse
from bot.services import ServiceTelegrmaBot


def bot_setwebhook(request):
    bot = ServiceTelegrmaBot(request)
    bot.set_webhook()
    return HttpResponse(status=200)


def bot_unsetwebhook(request):
    bot = ServiceTelegrmaBot(request)
    bot.delete_webhook()
    return HttpResponse(status=200)


def bot_info(request):
    bot = ServiceTelegrmaBot(request)
    bot.get_info()
    return HttpResponse(status=200)


class ViewTelegramBot(View):

    def post(self, request, *args, **kwargs):
        try:
            service_bot = ServiceTelegrmaBot(request)
            service_bot.incoming()
        except Exception as e:
            print(e)
        return HttpResponse('ok', content_type="text/plain", status=200)


class SendTelegramBot(View):

    def post(self, request, *args, **kwargs):
        service_bot = ServiceTelegrmaBot(request)
        return HttpResponse(service_bot.send_message())
