from rest_framework.serializers import ModelSerializer

from robot.models import Broker, ChoiceBalance, RegistrationUser, Link


class BrokerSerializer(ModelSerializer):
    class Meta:
        model = Broker
        fields = '__all__'


class ChoiceBalanceSerializer(ModelSerializer):
    class Meta:
        model = ChoiceBalance
        fields = '__all__'


class RegistrationUserSerializer(ModelSerializer):
    class Meta:
        model = RegistrationUser
        fields = '__all__'


class LinksSerializer(ModelSerializer):
    class Meta:
        model = Link
        fields = '__all__'