from rest_framework.views import APIView
from rest_framework.generics import ListAPIView
from rest_framework.response import Response

from robot.models import Broker, ChoiceBalance, RegistrationUser, FooterText, Link
from robot.serializers import BrokerSerializer, ChoiceBalanceSerializer,\
    RegistrationUserSerializer, LinksSerializer


class BrokerViewList(ListAPIView):
    queryset = Broker.objects.all()
    serializer_class = BrokerSerializer


class BalancesViewList(ListAPIView):
    queryset = ChoiceBalance.objects.all()
    serializer_class = ChoiceBalanceSerializer


class RegistrationUserView(APIView):

    def post(self, request, **kwargs):
        try:
            first_name = request.data.get('first_name', None)
            last_name = request.data.get('last_name', None)
            email = request.data.get('email', None)
            phone = request.data.get('phone', None)
            if not first_name or not last_name or not email or not phone:
                return Response(status=400)
            broker = request.data.get('broker', None)
            number_account = request.data.get('number_account', None)
            name_server = request.data.get('name_server', None)
            password = request.data.get('password', None)
            balance = request.data.get('balance', None)
            type_trade = request.data.get('type_trade', None)

            obj, _ = RegistrationUser.objects.update_or_create(
                first_name=first_name,
                last_name=last_name,
                email=email,
                phone=phone,
                defaults={
                    'broker_id': broker,
                    'number_account': number_account,
                    'password': password,
                    'name_server': name_server,
                    'balance_id': balance,
                    'type_trade': type_trade
                }
            )
        except Exception as e:
            print(type(e), e)
        return  Response(RegistrationUserSerializer(obj).data)


class FooterBodyView(APIView):

    def get(self, request, **kwargs):
        record = FooterText.objects.all().first()
        return Response({'body': record.body if record else ""})


class LinksView(ListAPIView):
    queryset = Link.objects.all()
    serializer_class = LinksSerializer
