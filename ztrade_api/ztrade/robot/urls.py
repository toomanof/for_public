from django.urls import re_path
from .import views as v

urlpatterns = [
    re_path(
        r'^(?P<version>(v1))/robot/brokers$',
        v.BrokerViewList.as_view(),
        name='brokers'),
    re_path(
        r'^(?P<version>(v1))/robot/balances$',
        v.BalancesViewList.as_view(),
        name='balances'),

    re_path(
        r'^(?P<version>(v1))/robot/registration$',
        v.RegistrationUserView.as_view(),
        name='registration'),
    re_path(
        r'^(?P<version>(v1))/robot/footer$',
        v.FooterBodyView.as_view(),
        name='footer'),
    re_path(
        r'^(?P<version>(v1))/robot/links',
        v.LinksView.as_view(),
        name='links'),
]