from django.db import models
from django.contrib import admin


class Broker(models.Model):
    title = models.CharField("Название", max_length=255)

    class Meta:
        verbose_name = 'Брокер'
        verbose_name_plural = 'Брокеры'


@admin.register(Broker)
class BrokerAdmin(admin.ModelAdmin):
    list_display = ('title', )


class ChoiceBalance(models.Model):
    value = models.PositiveIntegerField('Сумма')

    class Meta:
        verbose_name = 'Сумма баланса'
        verbose_name_plural = 'Суммы баланса'

    def __str__(self):
        return f'${self.value}'


@admin.register(ChoiceBalance)
class ChoiceBalanceAdmin(admin.ModelAdmin):
    list_display = ('value', )


class RegistrationUser(models.Model):
    TT_CONSERVATIVE, TT_BALANCED, TT_AGGRESSIVE = range(3)
    CHOICE_TYPE_TRADE = (
        (TT_CONSERVATIVE, 'Консервативный'),
        (TT_BALANCED, 'Сбалансированный'),
        (TT_AGGRESSIVE, 'Агресивный')
    )

    first_name = models.CharField("Имя", max_length=255)
    last_name = models.CharField("Фамилия", max_length=255)
    email = models.EmailField(max_length=100)
    phone = models.CharField('телефон', max_length=50, null=True,
                             default=None, blank=True)
    broker = models.ForeignKey(Broker,
                               verbose_name="Брокер",
                               related_name='registration_robots',
                               on_delete=models.CASCADE)
    number_account = models.CharField("Номер торогового счета",
                                      max_length=255)
    balance = models.ForeignKey(ChoiceBalance,
                                verbose_name="Сумма баланса",
                                on_delete=models.CASCADE,
                                blank=True, null=True)
    type_trade = models.SmallIntegerField("Тип торговли",
                                          blank=True, null=True)
    name_server = models.CharField("Название сервера",
                                   max_length=255,
                                   blank=True, null=True)
    password = models.CharField("Пароль от торгового счета",
                                max_length=255,
                                blank=True, null=True)

    class Meta:
        verbose_name = 'Данные регистрации аренды торгового робота'
        verbose_name_plural = 'Данные регистрации аренды торгового робота'


@admin.register(RegistrationUser)
class RegistrationUserAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name', 'email', 'phone',
                    'broker', 'number_account', 'balance', 'type_trade', 'name_server')


class FooterText(models.Model):
    body = models.TextField('контент', null=True,
                            default=None, blank=True)

    class Meta:
        verbose_name = 'Контент внизу'
        verbose_name_plural = 'Контент внизу'


@admin.register(FooterText)
class FooterTextAdmin(admin.ModelAdmin):
    list_display = ('body',)


class Link(models.Model):
    name = models.CharField("Название", max_length=100,
                            help_text="Название для других ПО")
    href = models.CharField("Имя", max_length=255)

    class Meta:
        verbose_name = 'Ссылка'
        verbose_name_plural = 'Ссылки'


@admin.register(Link)
class LinkAdmin(admin.ModelAdmin):
    list_display = ('name', 'href')
    list_editable = ('href',)
    actions = None

    def has_add_permission(self, request):
        return True

    def has_delete_permission(self, request, obj=None):
        return True

