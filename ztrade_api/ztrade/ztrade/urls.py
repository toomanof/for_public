from django.urls import include
from django.urls import path
from django.contrib import admin
from rest_framework.schemas import get_schema_view
from rest_framework_swagger.renderers import SwaggerUIRenderer, OpenAPIRenderer

schema_view = get_schema_view(
    title='ztrade API',
    renderer_classes=[OpenAPIRenderer, SwaggerUIRenderer])


urlpatterns = [
    path('admin/', admin.site.urls),
    path('docs/', schema_view, name='docs'),
    path('ckeditor/', include('ckeditor_uploader.urls')),
    path('bot/', include('bot.urls')),
    path('', include('account.urls')),
    path('', include('broker.urls')),
    path('', include('content.urls')),
    path('', include('course.urls')),
    path('', include('exchange.urls')),
    path('', include('payment.urls')),
    path('', include('shop.urls')),
    path('', include('sgn.urls')),
    path('', include('web.urls')),
    path('', include('robot.urls')),

]

admin.site.site_header = 'ztrade'
admin.site.site_title = 'ztrade'
