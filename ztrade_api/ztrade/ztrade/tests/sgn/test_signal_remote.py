import decimal
import json
import pytest

from sgn.models import Signal
from web.models import User
from ztrade.tests.web import base
from ztrade.tests.web.factories import UserFactory


class TestSignalRemote(base.Mixin):
    """
    Tests for creating and updating signals from remote trading terminal
    The Auth is made by api_key (user admin signal field)
    """
    @pytest.mark.django_db
    def setup(self):
        super().setup()
        self.init_markets()

    @pytest.mark.django_db
    def test_remote(self, client):
        user = UserFactory.get()
        user.type = User.SIGNAL_ADMIN
        user.save()

        # Create signal
        d = {
            'api_key': user.api_key,
            'symbol_code': 'eurusd',
            'signal_type_code': 'short',
            'trade_type': 'buy',
            'order_type': 'market',
            'price': '0.100',
            'tp': '0.100',
            'sl': '0.100',
            'countdown': 10,
        }

        data = json.dumps(d)
        url = self.get_url('remote_signal_create')
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 201

        signal = Signal.objects.first()
        assert signal.owner == user
        assert signal.is_closed is False
        assert 'signal_guid' in r.json()

        # Update example 1
        d = {
            'api_key': user.api_key,
            'signal_guid': signal.guid,
            'tp': '0.200',
            'sl': '0.200'
        }
        data = json.dumps(d)
        url = self.get_url('remote_signal_update')
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 200
        signal.refresh_from_db()
        assert signal.tp == decimal.Decimal('0.2')
        assert signal.sl == decimal.Decimal('0.2')

        # Update example 2
        d = {
            'api_key': user.api_key,
            'signal_guid': signal.guid,
            'order_type': 'limit',
            'signal_type_code': 'daily',
            'price': '0.200'
        }
        data = json.dumps(d)
        url = self.get_url('remote_signal_update')
        r = client.post(url, data, content_type='application/json')

        assert r.status_code == 200
        signal.refresh_from_db()
        assert signal.order_type == 'limit'
        assert signal.signal_type.code == 'daily'
        assert signal.price == decimal.Decimal('0.2')

        # Close signal
        d = {
            'api_key': user.api_key,
            'signal_guid': signal.guid,
            'result': 25
        }
        data = json.dumps(d)
        url = self.get_url('remote_signal_close')
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 200
        signal.refresh_from_db()
        assert signal.result == 25
        assert signal.is_closed is True
