import factory

from django.utils import timezone

from sgn.models import Market
from sgn.models import Symbol
from sgn.models import SignalType
from sgn.models import Signal
from sgn.models import Tariff
from sgn.models import TariffRow
from web.services.helpers import randstring

from ztrade.tests.web.factories import UserFactory


class MarketFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Market

    code = factory.lazy_attribute(lambda _: randstring(10))
    name = factory.lazy_attribute(lambda _: randstring(10))

    @staticmethod
    def get():
        return MarketFactory()


class SymbolFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Symbol

    code = factory.lazy_attribute(lambda _: randstring(10))
    name = factory.lazy_attribute(lambda _: randstring(10))
    prec = 2

    @staticmethod
    def get(market=None):
        if market is None:
            market = MarketFactory.get()
        return SymbolFactory(market=market)


class SignalTypeFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = SignalType

    code = factory.lazy_attribute(lambda _: randstring(10))
    name = factory.lazy_attribute(lambda _: randstring(10))

    @staticmethod
    def get():
        return SignalTypeFactory()


class SignalFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Signal

    trade_type = Signal.BUY
    order_type = Signal.MARKET
    price = 100
    tp = 120
    sl = 130
    created_at = timezone.now()
    countdown = 60

    @staticmethod
    def get(owner=None, symbol=None, signal_type=None):
        if owner is None:
            owner = UserFactory.get()

        if symbol is None:
            symbol = SymbolFactory.get()

        if signal_type is None:
            signal_type = SignalTypeFactory.get()

        return SignalFactory(
            owner=owner,
            symbol=symbol,
            signal_type=signal_type
        )


class TariffFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Tariff

    code = factory.lazy_attribute(lambda _: randstring(10))
    name = factory.lazy_attribute(lambda _: randstring(10))
    subtitle = factory.lazy_attribute(lambda _: randstring(10))
    price = 100

    @staticmethod
    def get(market=None, is_active=True, period=1, period_type=Tariff.MONTH):
        if market is None:
            market = MarketFactory.get()

        t = TariffFactory(
            market=market,
            is_active=is_active,
            period=period,
            period_type=period_type
        )

        # Create rows for Tariff
        TariffRow.objects.create(tariff=t, name='name1', class_name='some1')
        TariffRow.objects.create(tariff=t, name='name2', class_name='some2')

        return t
