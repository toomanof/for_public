import pytest

from django.core.management import call_command

from sgn.models import Market
from ztrade.tests.web import base
from ztrade.tests.web.factories import UserFactory


class TestSignal(base.Mixin):
    @pytest.mark.django_db
    def setup(self):
        super().setup()
        UserFactory.get()
        call_command('createmarkets')
        call_command('createsymbols')
        call_command('createsignaltypes')
        call_command('createsignals')

    @pytest.mark.django_db
    def test_chart01(self, client):
        user = UserFactory.get()
        client = self.get_auth_client(user)

        market = Market.objects.get(code='forex')

        url = self.get_url('chart01', code=market.code)
        url += '?type=week'

        r = client.get(url)
        assert r.status_code == 200

    @pytest.mark.django_db
    def test_chart02(self, client):
        user = UserFactory.get()
        client = self.get_auth_client(user)

        market = Market.objects.get(code='forex')

        url = self.get_url('chart02', code=market.code)

        r = client.get(url)
        assert r.status_code == 200
