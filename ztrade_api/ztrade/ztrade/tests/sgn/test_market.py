import pytest

from sgn.models import Market
from ztrade.tests.web import base
from ztrade.tests.web.factories import UserFactory
from .factories import MarketFactory


class TestMarket(base.Mixin):
    @pytest.mark.django_db
    def test_get_markets(self, client):
        """
        Get list of markets
        """
        user = UserFactory.get()
        client = self.get_auth_client(user)

        MarketFactory.get()
        MarketFactory.get()
        MarketFactory.get()
        count = Market.objects.count()

        url = self.get_url('markets')
        r = client.get(url)
        assert r.status_code == 200
        assert len(r.json()) == count
