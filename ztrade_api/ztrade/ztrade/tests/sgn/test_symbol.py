import pytest

from sgn.models import Symbol
from ztrade.tests.web import base
from ztrade.tests.web.factories import UserFactory
from .factories import SymbolFactory


class TestSymbol(base.Mixin):
    @pytest.mark.django_db
    def test_get_symbols(self, client):
        """
        Get list of symbols
        """
        user = UserFactory.get()
        client = self.get_auth_client(user)

        SymbolFactory.get()
        SymbolFactory.get()
        SymbolFactory.get()
        count = Symbol.objects.count()

        url = self.get_url('symbols')
        r = client.get(url)
        assert r.status_code == 200
        assert len(r.json()) == count
