import pytest

from sgn.models import SignalType

from ztrade.tests.web import base
from ztrade.tests.web.factories import UserFactory
from .factories import SignalTypeFactory


class TestSignalType(base.Mixin):
    @pytest.mark.django_db
    def test_get_signal_types(self, client):
        """
        Get list of signal_types
        """
        user = UserFactory.get()
        client = self.get_auth_client(user)

        SignalTypeFactory.get()
        SignalTypeFactory.get()
        SignalTypeFactory.get()
        count = SignalType.objects.count()

        url = self.get_url('signal_types')
        r = client.get(url)
        assert r.status_code == 200
        assert len(r.json()) == count
