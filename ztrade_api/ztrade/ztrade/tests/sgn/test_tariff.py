import pytest

from sgn.models import Tariff
from ztrade.tests.web import base
from .factories import MarketFactory
from .factories import TariffFactory


class TestTariff(base.Mixin):
    @pytest.mark.django_db
    def test_get_tariffs(self, client):
        """
        Get list of tariffs
        """
        market = MarketFactory.get()

        TariffFactory.get(market=market)
        TariffFactory.get(market=market)
        TariffFactory.get()
        TariffFactory.get()

        count = Tariff.objects.filter(market=market).count()

        url = self.get_url('tariffs', code=market.code)
        r = client.get(url)
        assert r.status_code == 200
        assert len(r.json()) == count
