from ztrade.tests.web.factories import CountryFactory


def get_download_book_data():
    return {
        'email': 'bob@gmail.com',
        'first_name': 'Some name',
        'last_name': 'Some last name',
        'phone': '+7 900 111 11 11',
        'country_id': CountryFactory.get().id
    }
