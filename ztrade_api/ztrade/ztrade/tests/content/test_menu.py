import pytest

from ztrade.tests.web import base
from content.management.commands.createarticles import Command as ACommand
from content.management.commands.createmenu import Command as MCommand

from content.models import Menu


class TestMenu(base.Mixin):
    @pytest.mark.django_db
    def setup(self):
        super().setup()
        MCommand().handle()
        ACommand().handle()

    @pytest.mark.django_db
    def test_get_menu_items01(self, client):
        """
        Test get menu items.
        If "is_initial" is passed, returns 10 items.
        Otherwise returns all items.
        """
        assert Menu.objects.count() > 0

        m = Menu.objects.filter(type=Menu.CONTENT_ICO).first()
        assert m is not None
        assert m.contents.count() > 0

        url = self.get_url('menu_contents', id=m.id)
        r = client.get(url)
        assert r.status_code == 200
        assert len(r.json()) > 10

        url += '?is_initial=true'
        r = client.get(url)
        assert r.status_code == 200
        assert len(r.json()) == 10
