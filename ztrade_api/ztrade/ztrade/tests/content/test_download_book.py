import json
import pytest
import requests

from django.core.files.uploadedfile import SimpleUploadedFile

from content.models import DownloadBook
from web.models import Emailer

from ztrade.tests.web import base
from ztrade.tests.web.factories import UserFactory

from content.constants import BOOK

from .factories import ContentFactory
from .data import get_download_book_data


class TestDownloadBook(base.Mixin):
    @pytest.mark.django_db
    def setup(self):
        super().setup()
        self.init_emailer()

    @pytest.mark.django_db
    def test_download_book_request01(self, client):
        """
        Test for download book request by not auth user.
        User should get email.
        """
        book = ContentFactory.get(type=BOOK)
        path = self.get_sample_png_path()

        f = open(path, 'rb')
        book.book_file = SimpleUploadedFile('somebook.png', f.read())
        book.save()

        url = self.get_url('download_book_request', book_id=book.id)

        d = get_download_book_data()

        data = json.dumps(d)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 201

        dnb = DownloadBook.objects.filter(id=r.json()['id']).first()
        assert dnb is not None
        assert dnb.user is None

        em = Emailer.objects.first()
        assert em is not None

    @pytest.mark.django_db
    def test_download_book_request02(self, client):
        """
        Test for download book request by auth user.
        User should get email.
        """
        user = UserFactory.get()
        client = self.get_auth_client(user)

        book = ContentFactory.get(type=BOOK)
        path = self.get_sample_png_path()

        f = open(path, 'rb')
        book.book_file = SimpleUploadedFile('somebook.png', f.read())
        book.save()
        url = self.get_url('download_book_request', book_id=book.id)

        d = get_download_book_data()

        data = json.dumps(d)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 201

        dnb = DownloadBook.objects.filter(id=r.json()['id']).first()
        assert dnb is not None
        assert dnb.user_id == user.id

        em = Emailer.objects.first()
        assert em is not None

    @pytest.mark.django_db
    def test_download_book_request03(self, client):
        """
        When user creates download request, new DownloadBook object
        is created and original book should be copied to DownloadBook location.
        """
        user = UserFactory.get()
        client = self.get_auth_client(user)

        book = ContentFactory.get(type=BOOK)
        path = self.get_sample_png_path()

        f = open(path, 'rb')
        book.book_file = SimpleUploadedFile('somebook.png', f.read())
        book.save()

        url = self.get_url('download_book_request', book_id=book.id)

        d = get_download_book_data()

        data = json.dumps(d)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 201

        dnb = DownloadBook.objects.filter(id=r.json()['id']).first()
        assert dnb is not None

        url = dnb.book_file_url
        assert url is not None

        r = requests.get(url)
        assert r.status_code == 200
