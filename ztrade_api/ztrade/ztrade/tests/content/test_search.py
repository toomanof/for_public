import pytest

from ztrade.tests.web import base
from content.management.commands.createarticles import Command as ACommand
from content.management.commands.createmenu import Command as MCommand
from content.management.commands.createbooks import Command as BCommand


class TestMenu(base.Mixin):
    @pytest.mark.django_db
    def setup(self):
        super().setup()
        ACommand().handle()
        MCommand().handle()
        BCommand().handle()

    @pytest.mark.django_db
    def test_search_1(self, client):
        url = self.get_url('search')
        url += '?query=Заг 1'
        r = client.get(url)

        rd = r.json()
        assert 'menu' in rd
        assert 'articles' in rd
        assert 'books' in rd
        assert len(rd['articles']) > 0
