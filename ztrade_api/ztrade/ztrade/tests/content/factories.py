import factory
from faker import Faker
from django.utils import timezone

from content.models import Content
from content.models import DownloadBook

from content.constants import ARTICLE
from content.constants import BOOK
from content.constants import PIC

from web.services.helpers import randstring
from ztrade.tests.web.factories import CountryFactory


class ContentFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Content

    title = factory.lazy_attribute(lambda _: randstring(10))
    h1 = factory.lazy_attribute(lambda _: randstring(10))
    name = factory.lazy_attribute(lambda _: randstring(10))
    short_description = factory.lazy_attribute(lambda _: randstring(10))
    body = factory.lazy_attribute(lambda _: randstring(10))
    created_at = timezone.now()

    @staticmethod
    def get(type=ARTICLE, image_type=PIC):
        return ContentFactory(type=type, image_type=image_type)


class DonwloadBookFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = DownloadBook

    first_name = factory.lazy_attribute(lambda _: randstring(10))
    last_name = factory.lazy_attribute(lambda _: randstring(10))
    phone = factory.lazy_attribute(lambda _: randstring(10))
    email = factory.lazy_attribute(lambda _: Faker().email())

    @staticmethod
    def get(book=None, country=None):
        if book is None:
            book = ContentFactory.get(type=BOOK)

        if country is None:
            country = CountryFactory.get()

        return DonwloadBookFactory(book=book, country=country)
