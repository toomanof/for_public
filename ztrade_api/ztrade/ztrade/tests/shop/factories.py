import factory
from django.utils import timezone

from shop.models import Category
from shop.models import Market
from shop.models import Product

from web.services.helpers import randstring


class CategoryFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Category

    name = factory.lazy_attribute(lambda _: randstring(10))

    @staticmethod
    def get():
        return CategoryFactory()


class MarketFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Market

    name = factory.lazy_attribute(lambda _: randstring(10))

    @staticmethod
    def get():
        return MarketFactory()


class ProductFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Product

    slug = factory.lazy_attribute(lambda _: randstring(10))
    title = 'Product some'
    price = 100
    content = factory.lazy_attribute(lambda _: randstring(10))
    potential_profit = 1
    current_profit = 1
    created_at = timezone.now()

    @staticmethod
    def get():
        return ProductFactory()
