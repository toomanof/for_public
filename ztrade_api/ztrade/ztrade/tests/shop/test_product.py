import pytest

from django.conf import settings
from shop.models import Category
from shop.models import Market
from shop.models import Product
from shop.models import ProductUser
from ztrade.tests.web import base
from web.services.helpers import get_s3_client

from .factories import CategoryFactory
from .factories import MarketFactory
from .factories import ProductFactory


class TestBroker(base.Mixin):
    @pytest.mark.django_db
    def test_get_categories(self, client):
        """
        Get list of categories
        """
        CategoryFactory.get()
        CategoryFactory.get()
        CategoryFactory.get()
        count = Category.objects.count()

        url = self.get_url('shop_categories')
        r = client.get(url)
        assert r.status_code == 200
        assert len(r.json()) == count

    @pytest.mark.django_db
    def test_get_markets(self, client):
        """
        Get list of markets
        """
        MarketFactory.get()
        MarketFactory.get()
        MarketFactory.get()
        count = Market.objects.count()

        url = self.get_url('shop_markets')
        r = client.get(url)
        assert r.status_code == 200
        assert len(r.json()) == count

    @pytest.mark.django_db
    def test_get_products(self, client):
        """
        Get list of products
        """
        ProductFactory.get()
        ProductFactory.get()
        ProductFactory.get()
        count = Product.objects.count()

        url = self.get_url('shop_products')
        r = client.get(url)
        assert r.status_code == 200
        assert len(r.json()) == count

    @pytest.mark.django_db
    def test_get_product(self, client):
        """
        Get product detail page
        """
        categories = [
            CategoryFactory.get(),
            CategoryFactory.get(),
            CategoryFactory.get(),
        ]

        markets = [
            MarketFactory.get(),
            MarketFactory.get(),
            MarketFactory.get(),
        ]
        p = ProductFactory.get()
        p.categories.set(categories)
        p.markets.set(markets)

        url = self.get_url('shop_product', slug=p.slug)
        r = client.get(url)
        assert r.status_code == 200

    @pytest.mark.django_db
    def test_filter_products(self, client):
        c1 = CategoryFactory.get()
        c2 = CategoryFactory.get()

        m1 = MarketFactory.get()
        m2 = MarketFactory.get()

        p1 = ProductFactory.get()
        p2 = ProductFactory.get()

        base_url = self.get_url('shop_products')

        # should get 0 products
        url = base_url + '?category_ids=1,2'
        r = client.get(url)
        assert r.status_code == 200
        assert len(r.json()) == 0

        # should get 2 products
        p1.categories.add(c1)
        p2.categories.add(c2)

        url = base_url + '?category_ids={},{}'.format(c1.id, c2.id)
        r = client.get(url)
        assert r.status_code == 200
        assert len(r.json()) == 2

        # should get 2 products
        p1.markets.add(m1)
        p2.markets.add(m2)

        url = base_url + '?market_ids={},{}'.format(m1.id, m2.id)
        r = client.get(url)
        assert r.status_code == 200
        assert len(r.json()) == 2

    @pytest.mark.django_db
    def notest_product_download_file(self, client):
        """
        1) Create Product
        2) Add real file to product
        3) Create ProductUser
        4) When ProductUser is created, by post_save signal
           DownloadFile instance should be created.
        5) Run celery task manually to copy s3 object
        6) Check DownloadFile for correctness
        7) Delete DownloadFile and object on bucket also
           should be deleted
        """
        key = 'download/test/test.png'

        client = get_s3_client()
        client.upload_file(
            self.get_sample_png_path(),
            settings.AWS_STORAGE_BUCKET_NAME,
            key,
            ExtraArgs={'ACL': 'public-read'}
        )

        # Object should exist
        # If no object, will raise Exception
        client.get_object(
            Bucket=settings.AWS_STORAGE_BUCKET_NAME,
            Key=key)

        product = ProductFactory.get()
        product.file.name = key
        product.save()

        pu = ProductUser.objects.create(
            product=product, name='some', email='some@gmail.com')
        pu.refresh_from_db()

        df = pu.download_file
        assert df is not None
        assert df.file is not None
        assert df.file.name is not None

        new_key = df.file.name

        # Run celery task manually
        from web.tasks import copy_s3_object
        copy_s3_object(key, new_key)

        # Object should exist
        # If no object - will raise Exception
        client.get_object(
            Bucket=settings.AWS_STORAGE_BUCKET_NAME,
            Key=new_key)

        # File on aws should be deleted by post_delete signal
        df.delete()

        # Object should not exist
        with pytest.raises(Exception):
            client.get_object(
                Bucket=settings.AWS_STORAGE_BUCKET_NAME,
                Key=new_key)
