import pytest
from ztrade.tests.web import base
from yandex_checkout.domain.response.payment_response import PaymentResponse


class TestYandexCheckout(base.Mixin):
    """
    yandex_checkout - official SDK from Yandex
    """

    @pytest.mark.django_db
    def test_01(self, client):
        r = PaymentResponse({
            'id': '21b23365-000f-500b-9000-070fa3554403',
            'amount': {
                'value': '1000.00',
                'currency': 'RUB'
            },
            'confirmation': {
                'type': 'redirect',
                'return_url': 'https://test.test/test',
                'confirmation_url': 'https://url',
                'enforce': False
            },
            'payment_method': {
                'type': 'bank_card',
                'id': '21b23365-000f-500b-9000-070fa3554403',
                'saved': False
            },
            'status': 'pending',
            'recipient': {
                'account_id': '67192',
                'gateway_id': '352780'
            },
            'refunded_amount': {
                'value': '1000.00',
                'currency': 'RUB'
            },
            'receipt_registration': 'pending',
            'created_at': '2017-11-30T15:11:33+00:00',
            'expires_at': '2017-11-30T15:11:33+00:00',
            'captured_at': '2017-11-30T15:11:33+00:00',
            'paid': False,
            'metadata': {
                'float_value': '123.32',
                'key': 'data'
            },
            'cancellation_details': {
                'party': 'yandex_checkout',
                'reason': 'fraud_suspected'
            }
        })
        # print(r.id)
        # print(r.amount.value)
        # print(r.amount.currency)
        # print(r.status)
        # print(r.expires_at)
        # print(r.created_at)
        # print(r.paid)
        # print(r.payment_method.__dict__)
        # print(r.metadata)
        # print(r.cancellation_details.party)
        # print(r.cancellation_details.reason)
        # print(r.confirmation.confirmation_url)
