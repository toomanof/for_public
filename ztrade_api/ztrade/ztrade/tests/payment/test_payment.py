import json
import pytest
from ztrade.tests.web import base
from account.models import SubscriptionUser
from shop.models import ProductUser
from sgn.models import TariffUser
from payment.models import Bill
from payment.models import YandexPayment
from web.models import Emailer

from ztrade.tests.account.factories import SubscriptionFactory
from ztrade.tests.shop.factories import ProductFactory
from ztrade.tests.sgn.factories import TariffFactory
from ztrade.tests.web.factories import UserFactory


class TestBill(base.Mixin):
    @pytest.mark.django_db
    def setup(self):
        super().setup()
        self.init_emailer()

    @pytest.mark.django_db
    def test_manager_create_product_bill(self, client):
        user = UserFactory.get()
        product = ProductFactory.get()

        bill = ProductUser.objects.create_bill(
            user.id, product.id, 'some@gmail.com', 'somename', 'somephone')

        assert bill.user == user
        assert bill.product_user is not None
        assert bill.subscription_user is None
        assert bill.tariff_user is None
        assert bill.user_email == 'some@gmail.com'
        assert bill.amount == product.price
        assert bill.is_paid is False

        bill = ProductUser.objects.create_bill(
            None, product.id, 'some@gmail.com', 'somename', 'somephone')

        assert bill.user is None
        assert bill.product_user is not None
        assert bill.subscription_user is None
        assert bill.tariff_user is None
        assert bill.user_email == 'some@gmail.com'
        assert bill.amount == product.price
        assert bill.is_paid is False

    @pytest.mark.django_db
    def test_manager_create_subscription_bill(self, client):
        user = UserFactory.get()
        subscription = SubscriptionFactory.get()

        bill = SubscriptionUser.objects.create_bill(
            user.id, subscription.id, 1)

        assert bill.user == user
        assert bill.product_user is None
        assert bill.subscription_user is not None
        assert bill.tariff_user is None
        assert bill.user_email == user.email
        assert bill.amount == subscription.price1
        assert bill.is_paid is False

        bill = SubscriptionUser.objects.create_bill(
            user.id, subscription.id, 3)
        assert bill.amount == subscription.price3
        assert bill.is_paid is False

        bill = SubscriptionUser.objects.create_bill(
            user.id, subscription.id, 6)
        assert bill.amount == subscription.price6
        assert bill.is_paid is False

    @pytest.mark.django_db
    def test_manager_create_tariff_bill(self, client):
        user = UserFactory.get()
        tariff = TariffFactory.get()

        bill = TariffUser.objects.create_bill(
            user.id, tariff.id)

        assert bill.user == user
        assert bill.product_user is None
        assert bill.subscription_user is None
        assert bill.tariff_user is not None
        assert bill.user_email == user.email
        assert bill.amount == tariff.price
        assert bill.is_paid is False

    @pytest.mark.django_db
    def test_manager_create_yandex_payment_manager_method(self, client):
        user = UserFactory.get()
        tariff = TariffFactory.get()

        bill = TariffUser.objects.create_bill(
            user.id, tariff.id)

        yp = YandexPayment.objects.create_payment(bill.id)
        assert yp is not None

    @pytest.mark.django_db
    def test_api_create_product_bill_not_auth(self, client):
        product = ProductFactory.get()
        d = {
            'product_id': product.id,
            'email': 'some@gmail.com',
            'name': 'some name',
            'phone': '+79991231122',
        }
        data = json.dumps(d)
        url = self.get_url('product_create_bill_not_auth')
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 201
        bill = Bill.objects.get(id=r.json()['id'])
        assert bill.product_user is not None
        assert bill.product_user.name == d['name']
        assert bill.product_user.phone == d['phone']
        assert bill.product_user.email == d['email']

    @pytest.mark.django_db
    def test_api_create_product_bill_auth(self, client):
        user = UserFactory.get()
        client = self.get_auth_client(user)

        product = ProductFactory.get()
        d = {
            'product_id': product.id,
            'email': 'some@gmail.com',
            'name': 'some name',
            'phone': '+79991231122',
        }
        data = json.dumps(d)
        url = self.get_url('product_create_bill_not_auth')
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 201

    @pytest.mark.django_db
    def test_api_create_subscription(self, client):
        user = UserFactory.get()
        client = self.get_auth_client(user)

        subscription = SubscriptionFactory.get()
        d = {
            'subscription_id': subscription.id,
            'num_months': 1
        }
        data = json.dumps(d)
        url = self.get_url('subscription_create_bill')
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 201

    @pytest.mark.django_db
    def test_api_create_tariff(self, client):
        user = UserFactory.get()
        client = self.get_auth_client(user)

        tariff = TariffFactory.get()
        d = {
            'tariff_id': tariff.id,
        }
        data = json.dumps(d)
        url = self.get_url('tariff_create_bill')
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 201

    @pytest.mark.django_db
    def test_yandex_payment_tariff(self, client):
        """
        1) Create bill
        2) Create Yandex payment in test environment
        3) Finalize payment by id emulating success
        """
        user = UserFactory.get()
        tariff = TariffFactory.get()

        bill = TariffUser.objects.create_bill(
            user.id, tariff.id)

        # Create payment object at Yandex
        yp = YandexPayment.objects.create_payment(bill.id)
        assert yp is not None
        assert yp.status == YandexPayment.PENDING
        assert yp.yandex_id is not None
        assert yp.confirmation_url is not None

        # Fimalize payment
        YandexPayment.objects.finalize_payment(
            yp.yandex_id, is_testing=True)

        yp.refresh_from_db()
        yp.bill.refresh_from_db()
        yp.bill.tariff_user.refresh_from_db()

        assert yp.status == YandexPayment.SUCCEEDED
        assert yp.is_paid is True
        assert yp.paid_at is not None
        assert yp.bill.is_paid is True
        assert yp.bill.tariff_user.is_paid is True

        em = Emailer.objects.first()
        assert em is not None

    @pytest.mark.django_db
    def test_yandex_payment_product(self, client):
        """
        1) Create bill
        2) Create Yandex payment in test environment
        3) Finalize payment by id emulating success
        """
        user = UserFactory.get()
        product = ProductFactory.get()

        bill = ProductUser.objects.create_bill(
            user.id, product.id, 'some@gmail.com', 'somename', 'somephone')

        yp = YandexPayment.objects.create_payment(bill.id)
        YandexPayment.objects.finalize_payment(
            yp.yandex_id, is_testing=True)

        yp.refresh_from_db()
        yp.bill.refresh_from_db()
        yp.bill.product_user.refresh_from_db()

        assert yp.status == YandexPayment.SUCCEEDED
        assert yp.is_paid is True
        assert yp.paid_at is not None
        assert yp.bill.is_paid is True
        assert yp.bill.product_user.is_paid is True

        em = Emailer.objects.first()
        assert em is not None

    @pytest.mark.django_db
    def test_yandex_payment_subscription(self, client):
        """
        1) Create bill
        2) Create Yandex payment in test environment
        3) Finalize payment by id emulating success
        """
        user = UserFactory.get()
        subscription = SubscriptionFactory.get()

        bill = SubscriptionUser.objects.create_bill(
            user.id, subscription.id, 1)

        yp = YandexPayment.objects.create_payment(bill.id)
        YandexPayment.objects.finalize_payment(
            yp.yandex_id, is_testing=True)

        yp.refresh_from_db()
        yp.bill.refresh_from_db()
        yp.bill.subscription_user.refresh_from_db()

        assert yp.status == YandexPayment.SUCCEEDED
        assert yp.is_paid is True
        assert yp.paid_at is not None
        assert yp.bill.is_paid is True
        assert yp.bill.subscription_user.is_paid is True

        em = Emailer.objects.first()
        assert em is not None
