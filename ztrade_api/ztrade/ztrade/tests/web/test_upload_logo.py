import json
import pytest
from ztrade.tests.web import base
from .factories import UserFactory


class TestUploadLogo(base.Mixin):
    @pytest.mark.django_db
    def test_upload_and_delete_v1(self, client):
        """
        Test upload logo as base64 string.
        """
        user = UserFactory.get_admin()

        client = self.get_auth_client(user)
        tenant = user.tenant

        assert tenant.logo_url is None

        url = self.get_url('upload_logo')

        dic = {
            'image_data': self.get_sample_image_base64()
        }
        data = json.dumps(dic)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 201

        tenant = user.tenant
        tenant.refresh_from_db()
        assert tenant.logo_url is not None

        # Delete
        r = client.delete(url)
        assert r.status_code == 204

        tenant.refresh_from_db()
        assert tenant.logo_url is None

    @pytest.mark.django_db
    def test_upload_v2(self, client):
        """
        Test upload logo as file.
        """
        user = UserFactory.get_admin()

        client = self.get_auth_client(user)
        tenant = user.tenant

        assert tenant.logo_url is None

        f = open(self.get_sample_png_path(), 'rb')

        url = self.get_url('upload_logo_v2', version='v2')
        r = client.post(url, {'file': f}, format='multipart')
        assert r.status_code == 201

        tenant = user.tenant
        tenant.refresh_from_db()
        assert tenant.logo_url is not None

        # Clean
        tenant.logo.delete(save=True)
        assert tenant.logo_url is None
