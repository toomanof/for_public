from web.models import Country
from .factories import SMSFactory


def get_signup_data():
    country = Country.objects.first()

    return {
        'email': 'bob@gmail.com',
        'first_name': 'Some name',
        'last_name': 'Some last name',
        'phone': '+7 900 111 11 11',
        'city': 'Москва',
        'country_id': country.id,
    }


def get_signup_confirm_data(user):
    user.uid = None
    user.signup_email_code = '12345'
    user.save()
    sms = SMSFactory.get()

    return {
        'sms_guid': sms.guid,
        'sms_code': sms.sms_code,
        'email_code': user.signup_email_code
    }


def get_signup_change_data_data(user):
    user.uid = None
    user.signup_email_code = '12345'
    user.save()
    sms = SMSFactory.get()

    return {
        'sms_guid': sms.guid,
        'new_phone': '+7 900 111 22 33',
        'new_email': 'some_new_email@gmail.com'
    }
