import pytest
from django.conf import settings
from web.services.helpers import get_s3_client
from .import base


class TestEmail(base.Mixin):
    @pytest.mark.django_db
    def test_list_bucket(self, client):
        client = get_s3_client()
        r = client.list_objects(Bucket=settings.AWS_STORAGE_BUCKET_NAME)
        print(r)

    @pytest.mark.django_db
    def test_put_object(self, client):
        client = get_s3_client()
        r = client.upload_file(
            self.get_sample_png_path(),
            settings.AWS_STORAGE_BUCKET_NAME,
            'test.png',
            ExtraArgs={'ACL': 'public-read'}
        )
        print(r)
