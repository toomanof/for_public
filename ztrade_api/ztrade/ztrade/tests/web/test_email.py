import pytest
from web.models import Email
from web.models import Emailer
from .factories import UserFactory
from .import base


class TestEmail(base.Mixin):
    @pytest.mark.django_db
    def test_send_email(self, client):
        self.init_emailer()
        user = UserFactory.get()

        print('--TestEmail', user)
        user.email = 'toomanof@gmail.com'
        user.save()

        em = Emailer.objects.process_email(Email.SIGNUP, user=user, uid=user.uid)
        em.send()
        em.refresh_from_db()
        assert em.is_sent is True
