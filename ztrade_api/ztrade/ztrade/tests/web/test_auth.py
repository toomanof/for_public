import datetime
import json
import pytest

from web.models import User
from web.models import Emailer
from web.models import SMS

from web.constants import EMAIL
from web.constants import SMS as SMSCONST

from .factories import UserFactory
from .import base
from .data import get_signup_data
from .data import get_signup_confirm_data
from .data import get_signup_change_data_data


class TestAuth(base.Mixin):
    @pytest.mark.django_db
    def setup(self):
        self.init_emailer()
        self.init_countries()

    @pytest.mark.django_db
    def test_signup01(self, client):
        """
        Test with correct email.
        User should get confirmation code to email.
        """
        url = self.get_url('signup')
        d = get_signup_data()

        data = json.dumps(d)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 201
        user = User.objects.filter(email=d['email']).first()
        assert user is not None
        assert user.uid is None

        sms = SMS.objects.first()
        assert sms is not None

        em = Emailer.objects.first()
        assert user.signup_email_code in em.txt
        assert user.signup_email_code in em.html
        # self.print_emailer()
        # print(r.json())

    @pytest.mark.django_db
    def test_signup02(self, client):
        """
        Test with incorrect email.
        """
        url = self.get_url('signup')
        d = get_signup_data()
        d['email'] = '!!!'
        data = json.dumps(d)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 400

    @pytest.mark.django_db
    def test_signup03(self, client):
        """
        Test with empty email.
        """
        url = self.get_url('signup')
        d = get_signup_data()
        d['email'] = ''
        data = json.dumps(d)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 400

    @pytest.mark.django_db
    def test_signup04(self, client):
        """
        Test with empty phone.
        """
        url = self.get_url('signup')
        d = get_signup_data()
        d['phone'] = ''
        data = json.dumps(d)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 400

    @pytest.mark.django_db
    def test_signup05(self, client):
        """
        Test with taken email.
        """
        user = UserFactory.get()

        url = self.get_url('signup')
        d = get_signup_data()
        d['email'] = user.email
        data = json.dumps(d)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 400

    @pytest.mark.django_db
    def test_signup06(self, client):
        """
        Test with incorrect phone.
        """
        url = self.get_url('signup')
        d = get_signup_data()
        d['phone'] = '+7 987'
        data = json.dumps(d)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 400

    @pytest.mark.django_db
    def test_signup07(self, client):
        """
        Test with taken phone.
        """
        user = UserFactory.get()

        url = self.get_url('signup')
        d = get_signup_data()
        d['phone'] = user.phone
        data = json.dumps(d)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 400

    @pytest.mark.django_db
    def test_signup_change_data01(self, client):
        """
        Should change phone.
        Should send new sms.
        Should respond with new sms guid.
        Email should not be sent as an email is the same.
        """
        user = UserFactory.get()
        url = self.get_url('signup_change_data', guid=user.guid)

        d = get_signup_change_data_data(user)
        d['new_email'] = user.email  # do not change email
        data = json.dumps(d)

        assert user.uid is None
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 200

        user.refresh_from_db()
        assert user.phone == d['new_phone']
        assert r.json()['sms_guid'] != d['sms_guid']
        assert Emailer.objects.first() is None

    @pytest.mark.django_db
    def test_signup_change_data02(self, client):
        """
        Should change email.
        Should send email.
        Should respond with old sms guid.
        """
        user = UserFactory.get()
        url = self.get_url('signup_change_data', guid=user.guid)

        d = get_signup_change_data_data(user)
        d['new_phone'] = user.phone  # do not change phone
        data = json.dumps(d)

        assert user.uid is None
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 200

        user.refresh_from_db()
        assert user.email == d['new_email']
        assert r.json()['sms_guid'] == d['sms_guid']
        em = Emailer.objects.first()
        assert user.signup_email_code in em.txt
        assert user.signup_email_code in em.html

    @pytest.mark.django_db
    def test_signup_change_data03(self, client):
        """
        Should get error with incorrect phone.
        """
        user = UserFactory.get()
        url = self.get_url('signup_change_data', guid=user.guid)

        d = get_signup_change_data_data(user)
        d['new_phone'] = '+7 aaa bbb ccc ddd'
        data = json.dumps(d)

        assert user.uid is None
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 400

    @pytest.mark.django_db
    def test_signup_change_data04(self, client):
        """
        Should get error with incorrect email.
        """
        user = UserFactory.get()
        url = self.get_url('signup_change_data', guid=user.guid)

        d = get_signup_change_data_data(user)
        d['new_email'] = 'incorrect'
        data = json.dumps(d)

        assert user.uid is None
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 400

    @pytest.mark.django_db
    def test_signup_change_data05(self, client):
        """
        Should get error with taken email.
        """
        user = UserFactory.get()
        url = self.get_url('signup_change_data', guid=user.guid)

        user2 = UserFactory.get(email='bob@gmail.com')

        d = get_signup_change_data_data(user)
        d['new_email'] = user2.email
        data = json.dumps(d)

        assert user.uid is None
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 400

    @pytest.mark.django_db
    def test_signup_change_data06(self, client):
        """
        Should get error with taken phone.
        """
        user = UserFactory.get()
        url = self.get_url('signup_change_data', guid=user.guid)

        user2 = UserFactory.get(email='bob@gmail.com')

        d = get_signup_change_data_data(user)
        d['new_phone'] = user2.phone
        data = json.dumps(d)

        assert user.uid is None
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 400

    @pytest.mark.django_db
    def test_signup_resend_email_code01(self, client):
        """
        User can request to resend email with signup code.
        """
        user = UserFactory.get()
        user.uid = None
        user.save()

        url = self.get_url('signup_resend_email_code', guid=user.guid)
        d = {}
        data = json.dumps(d)

        assert user.uid is None
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 204
        em = Emailer.objects.first()
        assert user.signup_email_code in em.txt
        assert user.signup_email_code in em.html

    @pytest.mark.django_db
    def test_signup_resend_phone_code01(self, client):
        """
        User can request to resend sms with signup code.
        Interval between sms is no less than 5 minutes
        """
        user = UserFactory.get()
        user.uid = None
        user.save()

        sms = SMS.objects.create_sms(SMS.SIGNUP, user.phone_num)

        url = self.get_url('signup_resend_phone_code', guid=user.guid)
        d = {'sms_guid': sms.guid}
        data = json.dumps(d)

        assert user.uid is None
        r = client.post(url, data, content_type='application/json')
        # Interval too small
        assert r.status_code == 400

        sms.created_at = sms.created_at - datetime.timedelta(seconds=280)
        sms.save()
        r = client.post(url, data, content_type='application/json')
        # Interval too small
        assert r.status_code == 400

        sms.created_at = sms.created_at - datetime.timedelta(seconds=301)
        sms.save()
        r = client.post(url, data, content_type='application/json')
        # Should get success
        assert r.status_code == 200
        new_sms = SMS.objects.get(guid=r.json()['sms_guid'])
        assert new_sms != sms

    @pytest.mark.django_db
    def test_signup_confirm01(self, client):
        """
        Should get success.
        """
        user = UserFactory.get()
        url = self.get_url('signup_confirm', guid=user.guid)

        d = get_signup_confirm_data(user)
        data = json.dumps(d)

        assert user.uid is None
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 201

        user.refresh_from_db()
        assert user.uid is not None
        assert user.joined_at is not None

        em = Emailer.objects.first()
        assert user.uid_str in em.txt
        assert user.uid_str in em.html

    @pytest.mark.django_db
    def test_signup_confirm02(self, client):
        """
        Can not to confirm user if it is already confirmed.
        """
        user = UserFactory.get()
        url = self.get_url('signup_confirm', guid=user.guid)

        d = get_signup_confirm_data(user)
        data = json.dumps(d)

        user.uid = 123456789
        user.save()

        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 400

    @pytest.mark.django_db
    def test_signup_confirm03(self, client):
        """
        Test with incorrect sms_guid
        """
        user = UserFactory.get()
        url = self.get_url('signup_confirm', guid=user.guid)

        d = get_signup_confirm_data(user)
        d['sms_guid'] = '!!!'
        data = json.dumps(d)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 400

    @pytest.mark.django_db
    def test_signup_confirm04(self, client):
        """
        Test with incorrect sms_code
        """
        user = UserFactory.get()
        url = self.get_url('signup_confirm', guid=user.guid)

        d = get_signup_confirm_data(user)
        d['sms_code'] = '!!!'
        data = json.dumps(d)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 400

    @pytest.mark.django_db
    def test_signup_confirm05(self, client):
        """
        Test with incorrect email_code
        """
        user = UserFactory.get()
        url = self.get_url('signup_confirm', guid=user.guid)

        d = get_signup_confirm_data(user)
        d['email_code'] = '!!!'
        data = json.dumps(d)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 400

    @pytest.mark.django_db
    def test_login01(self, client):
        """
        Test with correct uid.
        """
        user = UserFactory.get()
        user.set_password('11111111')
        user.save()

        url = self.get_url('login')
        data = {
            'uid_email': user.uid_str,
            'password': '11111111'
        }

        r = client.post(url, data)
        assert r.status_code == 200

        d = r.json()
        user = User.objects.get(id=user.id)
        assert user.get_token().key == d['token']

    @pytest.mark.django_db
    def test_login02(self, client):
        """
        Test with correct email.
        """
        user = UserFactory.get()
        user.set_password('11111111')
        user.save()

        url = self.get_url('login')
        data = {
            'uid_email': user.email,
            'password': '11111111'
        }
        r = client.post(url, data)
        assert r.status_code == 200

    @pytest.mark.django_db
    def test_login03(self, client):
        """
        Test with incorrect uid.
        """
        user = UserFactory.get()
        user.set_password('11111111')
        user.save()

        url = self.get_url('login')
        data = {
            'uid_email': str(user.uid + 1),
            'password': '11111111'
        }
        r = client.post(url, data)
        assert r.status_code == 400

    @pytest.mark.django_db
    def test_login04(self, client):
        """
        Test with incorrect email.
        """
        user = UserFactory.get()
        user.set_password('11111111')
        user.save()

        url = self.get_url('login')
        data = {
            'uid_email': 'incorrect',
            'password': '11111111'
        }
        r = client.post(url, data)
        assert r.status_code == 400

    @pytest.mark.django_db
    def test_login05(self, client):
        """
        Test with incorrect password.
        """
        user = UserFactory.get()
        user.set_password('11111111')
        user.save()

        url = self.get_url('login')
        data = {
            'uid_email': user.uid_str,
            'password': '22222222'
        }
        r = client.post(url, data)
        assert r.status_code == 400

    @pytest.mark.django_db
    def test_login06(self, client):
        """
        User can't login if is_active=False
        """
        user = UserFactory.get()
        user.set_password('11111111')
        user.is_active = False
        user.save()

        url = self.get_url('login')
        data = {
            'uid_email': user.uid_str,
            'password': '11111111'
        }
        r = client.post(url, data)
        assert r.status_code == 400

    @pytest.mark.django_db
    def test_login07(self, client):
        """
        User can't login if signup not completed.
        """
        user = UserFactory.get()
        user.set_password('11111111')
        user.uid = None
        user.save()

        url = self.get_url('login')
        data = {
            'uid_email': user.email,
            'password': '11111111'
        }
        r = client.post(url, data)
        assert r.status_code == 400

    @pytest.mark.django_db
    def test_reset_password_send_code01(self, client):
        """
        User should get code by email.
        """
        user = UserFactory.get()

        url = self.get_url('reset_password_send_code')
        d = {
            'uid_email': user.uid_str,
            'type': EMAIL
        }

        data = json.dumps(d)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 204

        user.refresh_from_db()
        em = Emailer.objects.first()
        assert user.reset_password_code in em.txt
        assert user.reset_password_code in em.html

    @pytest.mark.django_db
    def test_reset_password_send_code02(self, client):
        """
        User should get code by sms.
        """
        user = UserFactory.get()

        url = self.get_url('reset_password_send_code')
        d = {
            'uid_email': user.uid_str,
            'type': SMSCONST
        }

        data = json.dumps(d)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 204

        user.refresh_from_db()
        sms = SMS.objects.first()
        assert user.reset_password_code in sms.message
        assert user.reset_password_code in sms.message

    @pytest.mark.django_db
    def test_reset_password_send_code03(self, client):
        """
        Test to get error with incorrect ID and email
        """
        url = self.get_url('reset_password_send_code')
        d = {
            'uid_email': '111',
            'type': EMAIL
        }

        data = json.dumps(d)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 400

        d = {
            'uid_email': 'incorrect@gmail.com',
            'type': EMAIL
        }

        data = json.dumps(d)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 400

    @pytest.mark.django_db
    def test_reset_password01(self, client):
        """
        User should reset password by ID
        """
        user = UserFactory.get()
        user.reset_password_code = '123456'
        user.save()

        url = self.get_url('reset_password')
        d = {
            'uid_email': user.uid_str,
            'code': '123456',
            'password': 'ABCD12345'
        }

        data = json.dumps(d)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 201

        user.refresh_from_db()
        assert user.check_password(d['password']) is True
        assert user.reset_password_code is None

    @pytest.mark.django_db
    def test_reset_password02(self, client):
        """
        User should reset password by email
        """
        user = UserFactory.get()
        user.reset_password_code = '123456'
        user.save()

        url = self.get_url('reset_password')
        d = {
            'uid_email': user.email,
            'code': '123456',
            'password': 'ABCD12345'
        }

        data = json.dumps(d)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 201

        user.refresh_from_db()
        assert user.check_password(d['password']) is True
        assert user.reset_password_code is None

    @pytest.mark.django_db
    def test_reset_password03(self, client):
        """
        User should not reset password with incorrect code.
        """
        user = UserFactory.get()
        user.reset_password_code = '123456'
        user.save()

        url = self.get_url('reset_password')
        d = {
            'uid_email': user.email,
            'code': 'incorrect',
            'password': 'ABCD12345'
        }

        data = json.dumps(d)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 400
