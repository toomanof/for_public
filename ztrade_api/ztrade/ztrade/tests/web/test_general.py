import json
import pytest

from django.utils import timezone
from content.management.commands.createmenu import Command

from web.models import Country
from web.models import Currency
from web.models import Lang
from web.models import SubscribeForm
from web.models import Support
from web.models import Timezone

from ztrade.tests.web import base

from .factories import CountryFactory
from .factories import CurrencyFactory
from .factories import LangFactory
from .factories import TimezoneFactory
from .factories import TranslationFactory
from .factories import UserFactory


class TestGeneral(base.Mixin):
    @pytest.mark.django_db
    def test_timezone_convert(self, client):
        TimezoneFactory.get()

        dt = timezone.now()
        for tz in Timezone.objects.all():
            assert tz.convert_dt(dt) is not None

    @pytest.mark.django_db
    def test_centrifugo_token(self, client):
        """
        Get centrifugo token
        """
        url = self.get_url('centrifugo_token')
        r = client.get(url)
        assert r.status_code == 200
        rd = r.json()
        assert 'token' in rd

    @pytest.mark.django_db
    def test_countries(self, client):
        """
        Get list of countries.
        """
        user = UserFactory.get()
        client = self.get_auth_client(user)

        CountryFactory.get()
        CountryFactory.get()
        count = Country.objects.count()

        url = self.get_url('countries')
        r = client.get(url)
        assert r.status_code == 200
        assert len(r.json()) == count

    @pytest.mark.django_db
    def test_currencies(self, client):
        """
        Get list of currencies.
        """
        user = UserFactory.get()
        client = self.get_auth_client(user)

        CurrencyFactory.get()
        CurrencyFactory.get()
        count = Currency.objects.count()

        url = self.get_url('currencies')
        r = client.get(url)
        assert r.status_code == 200
        assert len(r.json()) == count

    @pytest.mark.django_db
    def test_langs(self, client):
        """
        Get list of langs.
        """
        user = UserFactory.get()
        client = self.get_auth_client(user)

        LangFactory.get()
        LangFactory.get()
        count = Lang.objects.count()

        url = self.get_url('langs')
        r = client.get(url)
        assert r.status_code == 200
        assert len(r.json()) == count

    @pytest.mark.django_db
    def test_timezones(self, client):
        """
        Get list of timezones.
        """
        user = UserFactory.get()
        client = self.get_auth_client(user)

        TimezoneFactory.get()
        TimezoneFactory.get()
        count = Timezone.objects.count()

        url = self.get_url('timezones')
        r = client.get(url)
        assert r.status_code == 200
        assert len(r.json()) == count

    @pytest.mark.django_db
    def test_menu(self, client):
        """
        Get menu.
        """
        Command().handle()
        user = UserFactory.get()
        client = self.get_auth_client(user)

        url = self.get_url('menu')
        r = client.get(url)
        assert r.status_code == 200
        assert len(r.json()) > 0

    @pytest.mark.django_db
    def test_translation(self, client):
        """
        Get translation dict.
        """
        user = UserFactory.get()
        client = self.get_auth_client(user)

        TranslationFactory.get()
        TranslationFactory.get()
        TranslationFactory.get()
        TranslationFactory.get()
        TranslationFactory.get()

        url = self.get_url('translations')
        r = client.get(url)
        assert r.status_code == 200

    @pytest.mark.django_db
    def test_support(self, client):
        """
        Test to send support question.
        """
        url = self.get_url('support')
        d = {
            'first_name': 'Some first name',
            'last_name': 'Some last name',
            'email': 'some@email.com',
            'question': 'some question'
        }
        data = json.dumps(d)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 201
        assert Support.objects.first() is not None

    @pytest.mark.django_db
    def test_subscribe_form(self, client):
        """
        Test to subscribe email for "HOW_INVEST"
        """
        url = self.get_url('subscribe_form')
        d = {
            'email': 'some@email.com',
            'type': SubscribeForm.HOW_INVEST
        }
        data = json.dumps(d)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 201
        assert SubscribeForm.objects.first() is not None

    @pytest.mark.django_db
    def test_sign_s3_upload(self, client):
        """
        user = UserFactory.get()
        client = self.get_auth_client(user)

        d = {
            'guid': 'xxx',
            'filename': 'some.png',
            'content_type': 'image/png',
            'upload_type': FILE
        }

        url = self.get_url('sign_s3_upload')
        data = json.dumps(d)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 200
        """
