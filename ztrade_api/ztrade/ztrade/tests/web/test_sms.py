import json
import pytest
from web.models import SMS
from .import base


class TestSMS(base.Mixin):
    @pytest.mark.django_db
    def notest_send_sms(self, client):
        sms = SMS.objects.create(
            phone='+7 908 140 85 58',
            message='Тест',
        )

        assert sms.converted_phone.isdigit() is True
        sms.send()
        assert sms.is_sent is True
