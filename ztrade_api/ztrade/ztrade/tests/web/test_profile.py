import json
import pytest

from . import base
from .factories import CountryFactory
from .factories import UserFactory

from web.models import SMS


class TestProfile(base.Mixin):
    @pytest.mark.django_db
    def setup(self):
        self.init_emailer()

    @pytest.mark.django_db
    def notest_get_profile01(self, client):
        """
        User should get profile.
        """
        user = UserFactory.get()

        client = self.get_auth_client(user)
        url = self.get_url('user_profile')
        r = client.get(url)
        assert r.status_code == 200

    @pytest.mark.django_db
    def test_update_profile01(self, client):
        user = UserFactory.get()
        client = self.get_auth_client(user)
        country = CountryFactory.get()
        city = 'Москва'

        d = {
            'city': city,
            'country_id': country.id
        }

        url = self.get_url('user_profile')
        r = client.put(url, json.dumps(d), content_type='application/json')
        assert r.status_code == 201
        user.refresh_from_db()
        assert user.city == city
        assert user.country == country

    @pytest.mark.django_db
    def notest_change_password01(self, client):
        """
        User should change password.
        """
        user = UserFactory.get()
        user.set_password('11111111')
        user.save()

        client = self.get_auth_client(user)
        url = self.get_url('user_profile_change_password')
        dic = {
            'old_password': '11111111',
            'new_password': '22222222',
        }
        data = json.dumps(dic)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 204

    @pytest.mark.django_db
    def notest_change_password02(self, client):
        """
        User should not change password.
        Min lenght should be no less than 8 symbols.
        """
        user = UserFactory.get()
        user.set_password('11111111')
        user.save()

        client = self.get_auth_client(user)
        url = self.get_url('user_profile_change_password')
        dic = {
            'old_password': '11111111',
            'new_password': '222',
        }
        data = json.dumps(dic)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 400

    @pytest.mark.django_db
    def notest_change_password03(self, client):
        """
        User should not change password.
        Current password is not correct.
        """
        user = UserFactory.get()

        client = self.get_auth_client(user)
        url = self.get_url('user_profile_change_password')
        dic = {
            'old_password': 'incorrect',
            'new_password': '22222222',
        }
        data = json.dumps(dic)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 400

    @pytest.mark.django_db
    def notest_update_last_access(self, client):
        """
        User's last_access_at should be updated
        """
        user = UserFactory.get()
        client = self.get_auth_client(user)

        url = self.get_url('update_last_access')
        r = client.post(url, {}, content_type='application/json')
        assert r.status_code == 204

    def send_profile_sms_code(self, client, phone):
        d = {'phone': phone}
        d = json.dumps(d)

        url = self.get_url('user_profile_send_sms_code')
        r = client.post(url, d, content_type='application/json')
        guid = r.json()['sms_guid']
        sms = SMS.objects.filter(guid=guid).first()
        assert sms is not None
        return sms

    def send_profile_email_code(self, client, user, email):
        d = {'email': email}
        d = json.dumps(d)
        url = self.get_url('user_profile_send_email_code')
        client.post(url, d, content_type='application/json')
        user.refresh_from_db()
        code = user.change_email_code
        assert code is not None
        return code

    @pytest.mark.django_db
    def test_change_email01(self, client):
        user = UserFactory.get()
        client = self.get_auth_client(user)
        email = 'newemail@gmail.com'

        email_code = self.send_profile_email_code(client, user, email)
        d = {
            'email': email,
            'email_code': email_code
        }

        url = self.get_url('user_profile_change_email')
        r = client.post(url, json.dumps(d), content_type='application/json')
        assert r.status_code == 201
        user.refresh_from_db()
        assert user.email == email

    @pytest.mark.django_db
    def test_change_email02(self, client):
        user = UserFactory.get()
        client = self.get_auth_client(user)
        email = 'newemail@gmail.com'
        email_code = self.send_profile_email_code(client, user, email)

        d = {
            'email': email,
            'email_code': str(int(email_code) + 1)
        }

        url = self.get_url('user_profile_change_email')
        r = client.post(url, json.dumps(d), content_type='application/json')
        assert r.status_code == 400

    @pytest.mark.django_db
    def test_change_phone01(self, client):
        user = UserFactory.get()
        client = self.get_auth_client(user)
        phone = '+7 900 111 11 12'
        sms = self.send_profile_sms_code(client, phone)
        d = {
            'phone': phone,
            'sms_code': sms.sms_code,
            'sms_guid': sms.guid
        }

        url = self.get_url('user_profile_change_phone')
        r = client.post(url, json.dumps(d), content_type='application/json')
        assert r.status_code == 201
        user.refresh_from_db()
        assert user.phone == phone
