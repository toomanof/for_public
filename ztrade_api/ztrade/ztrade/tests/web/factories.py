import factory
from random import randint
from faker import Faker
from django.utils import timezone
from django.core.management import call_command

from web.models import Email
from web.models import Country
from web.models import Currency
from web.models import DownloadFile
from web.models import Lang
from web.models import SMS
from web.models import Timezone
from web.models import Translation
from web.models import User

from web.constants import SUBSCRIPTION
from web.services.helpers import randstring


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = User
        django_get_or_create = ('email',)

    email = factory.lazy_attribute(lambda _: Faker().email())
    first_name = 'Bob'
    last_name = 'Martin'
    phone = '+79001111111'
    is_active = True
    joined_at = timezone.now()

    @staticmethod
    def get(email=None, type=User.USER):
        if email is None:
            email = Faker().email()

        user = UserFactory(email=email, type=type)
        user.uid = User.objects.generate_uid()
        user.save()
        return user


class EmailFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Email
        django_get_or_create = ('code',)


class CountryFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Country

    code = factory.Sequence(lambda n: '{0}'.format(n))
    name = factory.lazy_attribute(
        lambda _: Faker().country()[:20] + str(randint(1000, 9999)))

    @staticmethod
    def get():
        return CountryFactory()


class CurrencyFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Currency

    code = factory.Sequence(lambda n: 'C{0}'.format(n))
    name = 'US Dollar'

    @staticmethod
    def get():
        obj = CurrencyFactory()
        obj.name = obj.code
        obj.save()
        return obj


class DownloadFileFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = DownloadFile

    name = factory.lazy_attribute(lambda _: Faker().word())

    @staticmethod
    def get(type=SUBSCRIPTION):
        df = DownloadFileFactory(type=type)
        df.file.name = 'some.xlsx'
        df.save()
        return df


class LangFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Lang

    @staticmethod
    def get(code=None):
        if code is None:
            code = Faker().language_code()
            name = code.upper()
        return LangFactory(code=code, name=name)


class SMSFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = SMS

    @staticmethod
    def get(sms_code='111', phone='+79001111111', message='some'):
        return SMSFactory(sms_code=sms_code, phone=phone, message=message)


class TimezoneFactory(factory.django.DjangoModelFactory):
    """
    Use real timezones.
    """
    class Meta:
        model = Timezone

    @staticmethod
    def get(name=None):
        tz = Timezone.objects.first()
        if tz is None:
            call_command('createtimezones')

        if name is None:
            tz = Timezone.objects.first()
        else:
            tz = Timezone.objects.filter(name=name).first()
            if tz is None:
                assert ValueError('Incorrect name for timezone')
        return tz


class TranslationFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Translation

    code = factory.lazy_attribute(lambda _: randstring(9))
    value_en = factory.lazy_attribute(lambda _: randstring(9))
    value_ru = factory.lazy_attribute(lambda _: randstring(9))

    @staticmethod
    def get():
        return TranslationFactory()
