import pytest
from django.urls import reverse
from django.apps import apps
from django.conf import settings
from rest_framework.test import APIClient

from sgn.management.commands.createmarkets import Command as MktCommand
from sgn.management.commands.createsymbols import Command as SymCommand
from sgn.management.commands.createsignaltypes import Command as STCommand

from web.management.commands.generator import Command as GCommand
from web.management.commands.createemails import Command


class Mixin:
    @pytest.mark.django_db
    def setup(self):
        Lang = apps.get_model('web', 'Lang')
        Lang.objects.create(code='en', name='English')

    def get_auth_client(self, user):
        """
        Returns auth api client.
        """
        User = apps.get_model('web', 'User')

        client = APIClient()
        user = User.objects.get(id=user.id)
        token = user.get_token()
        client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        return client

    def get_url(self, name, version='v1', **kwargs):
        """
        Wrapper to reverse url without kwargs.
        """
        d = {'version': version}
        d.update(kwargs)
        return reverse(name, kwargs=d)

    def get_sample_png_path(self):
        return '{}/ztrade/ztrade/tests/web/files/sample.png'\
            .format(settings.BASE_DIR)

    def get_sample_jpg_path(self):
        return '{}/ztrade/ztrade/tests/web/files/sample.jpg'\
            .format(settings.BASE_DIR)

    def get_sample_image_base64(self):
        """
        Returns image as base64 string
        """
        return 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAAmCAYAAACoPemuAAACNElEQVRYR+2XOY7VQBRFz1sKCQTM87QIGghoMoJWiwgI2AtbYSYhYpRYByEBCMRFD71CJbftGuwvfaT+kiP7y8f31rt1y9jSn20pF4dgrc78P4pJOgY8BB6Y2a/WL215XtI54B7wyMx+5/89oJik88DLuO5uCk7SWeAV8A64YWY/ZsH8pqSLwIu4VocLKP/498COmX0fKj25xjYFJ+lMKDUJ5ZCzi1/SJeB52Lq71FZJpwPqw5RSSbniVEq6HHBubTdcC1RRsUS/FE7SKeA1UFSqWrEM7grwLGy9U2trQPn0fSzZV5zKqSySdDXg3NYinKSToVQTVLWVOWgt3BKoLrDIuVw5H4ifA/gTodSnFvu6rRy8/BrwNEL4H5ykxVDdimUDcT3gPOt2gaPAG6BbqeapnBmIBPcW8E35c699q1g5sPU+8AT4Chwxs28tLWPs2WLyl16QtYQvgAepZ9bijX8RWNYSvLrsAMcjgBfDdYNlLeEvVOpT0ec8gBfBdYEFlPcph7o5LHlrlM1msKwljEJlUXIhMs4/oHnNNYHVQmVw3oRTn2uCqwbLqsusUsMp7m3CVWBZdXGoW8M1VREpzWeIIljWErqgMltTTfeJLdpa6vxpQ/aDQ7NSI7ammu7rbhZu7pSUoFyp2632zeytVXCjYJI8wb0lrAo1UtMnlRs7iTuUHxzcvtWUGrE1lU0/RxywdQzMz36Pgb217CucIfaB/WELLk5lKQo2df8QrFXZrVXsD7yeQDaEL5g7AAAAAElFTkSuQmCC'

    def init_emailer(self):
        Command().handle()

    def init_countries(self):
        GCommand().create_countries()

    def init_markets(self):
        MktCommand().create_markets()
        SymCommand().create_symbols()
        STCommand().create_signal_types()

    def print_emailer(self):
        Emailer = apps.get_model('web', 'Emailer')

        for em in Emailer.objects.all():
            print('-------')
            print('email_to: {}'.format(em.email_to))
            print('email_from: {}'.format(em.email_from))
            print('subject: {}'.format(em.subject))
            print('txt: {}'.format(em.txt))
            print('html: {}'.format(em.html))
