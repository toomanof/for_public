import json
import pytest

from account.models import Notification
from ztrade.tests.web import base
from ztrade.tests.sgn.factories import MarketFactory
from ztrade.tests.web.factories import UserFactory

from .factories import NotificationFactory


class TestStrategy(base.Mixin):
    @pytest.mark.django_db
    def setup(self):
        super().setup()

        user = UserFactory.get()
        market = MarketFactory.get()

        NotificationFactory.get()
        NotificationFactory.get()

        NotificationFactory.get(user=user, type=Notification.GLOBAL)
        NotificationFactory.get(user=user, type=Notification.GLOBAL)

        NotificationFactory.get(user=user, type=Notification.NEWS)
        NotificationFactory.get(user=user, type=Notification.NEWS)

        NotificationFactory.get(user=user, type=Notification.SIGNALS)
        NotificationFactory.get(user=user, type=Notification.SIGNALS)

        NotificationFactory.get(
            user=user, type=Notification.NEWS, market=market)
        NotificationFactory.get(
            user=user, type=Notification.NEWS, market=market)
        NotificationFactory.get(
            user=user, type=Notification.SIGNALS, market=market)
        NotificationFactory.get(
            user=user, type=Notification.SIGNALS, market=market)

        self.user = user
        self.market = market

    @pytest.mark.django_db
    def test_get_global_notifications(self, client):
        """
        User should get global notifications
        """
        client = self.get_auth_client(self.user)

        url = self.get_url('notifications')
        r = client.get(url)
        assert r.status_code == 200
        assert len(r.json()) == 2

    @pytest.mark.django_db
    def test_get_market_signals_notifications(self, client):
        """
        User should get market notifications
        """
        client = self.get_auth_client(self.user)

        url = self.get_url('notifications')
        url += '?market_id={}'.format(self.market.id)
        url += '&type={}'.format(Notification.SIGNALS)
        r = client.get(url)
        assert r.status_code == 200
        assert len(r.json()) == 2

    @pytest.mark.django_db
    def test_get_market_news_notifications(self, client):
        """
        User should get market notifications
        """
        client = self.get_auth_client(self.user)

        url = self.get_url('notifications')
        url += '?market_id={}'.format(self.market.id)
        url += '&type={}'.format(Notification.NEWS)
        r = client.get(url)
        assert r.status_code == 200
        assert len(r.json()) == 2

    @pytest.mark.django_db
    def test_get_count_global_notifications(self, client):
        client = self.get_auth_client(self.user)

        url = self.get_url('notifications_not_read_count')
        r = client.get(url)
        assert r.status_code == 200
        rd = r.json()
        assert rd['count'] == 2

    @pytest.mark.django_db
    def test_get_count_signals_notifications(self, client):
        client = self.get_auth_client(self.user)

        url = self.get_url('notifications_not_read_count')
        url += '?market_id={}'.format(self.market.id)
        url += '&type={}'.format(Notification.SIGNALS)
        r = client.get(url)
        assert r.status_code == 200
        rd = r.json()
        assert rd['count'] == 2

    @pytest.mark.django_db
    def test_get_count_news_notifications(self, client):
        client = self.get_auth_client(self.user)

        url = self.get_url('notifications_not_read_count')
        url += '?market_id={}'.format(self.market.id)
        url += '&type={}'.format(Notification.NEWS)
        r = client.get(url)
        assert r.status_code == 200
        rd = r.json()
        assert rd['count'] == 2

    @pytest.mark.django_db
    def test_mark_global_notifications(self, client):
        client = self.get_auth_client(self.user)
        d = {}
        data = json.dumps(d)
        url = self.get_url('notifications_mark_all')
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 204

        qs = Notification.objects.filter(
            user=self.user, type=Notification.GLOBAL)

        assert qs.filter(is_read=True).count() == 2
        assert qs.filter(is_read=False).count() == 0

    @pytest.mark.django_db
    def test_mark_signals_notifications(self, client):
        client = self.get_auth_client(self.user)
        d = {}
        data = json.dumps(d)
        url = self.get_url('notifications_mark_all')
        url += '?market_id={}'.format(self.market.id)
        url += '&type={}'.format(Notification.SIGNALS)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 204

        qs = Notification.objects.filter(
            user=self.user, market_id=self.market.id,
            type=Notification.SIGNALS)

        assert qs.filter(is_read=True).count() == 2
        assert qs.filter(is_read=False).count() == 0

    @pytest.mark.django_db
    def test_mark_news_notifications(self, client):
        client = self.get_auth_client(self.user)
        d = {}
        data = json.dumps(d)
        url = self.get_url('notifications_mark_all')
        url += '?market_id={}'.format(self.market.id)
        url += '&type={}'.format(Notification.NEWS)
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 204

        qs = Notification.objects.filter(
            user=self.user, market_id=self.market.id,
            type=Notification.NEWS)

        assert qs.filter(is_read=True).count() == 2
        assert qs.filter(is_read=False).count() == 0

    @pytest.mark.django_db
    def test_mark_notifications_by_ids(self, client):
        client = self.get_auth_client(self.user)

        qs = Notification.objects.filter(user=self.user)
        assert qs.count() == 10

        ids = list(qs.values_list('id', flat=True)[:2])

        d = {'ids': ids}
        data = json.dumps(d)
        url = self.get_url('notifications_mark')
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 204

        assert qs.filter(is_read=True).count() == 2
        assert qs.filter(is_read=False).count() == 8

    @pytest.mark.django_db
    def test_remove_notifications_by_ids(self, client):
        client = self.get_auth_client(self.user)

        qs = Notification.objects.filter(user=self.user)
        assert qs.count() == 10

        ids = list(qs.values_list('id', flat=True)[:2])

        d = {'ids': ids}
        data = json.dumps(d)
        url = self.get_url('notifications_remove')
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 204
        assert qs.count() == 8
