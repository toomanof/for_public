import json
import pytest

from account.models import ForexNewsFilter
from ztrade.tests.web import base
from ztrade.tests.web.factories import UserFactory

from .factories import ForexCategoryFactory
from .factories import ForexSymbolFactory
from .factories import ForexNewsFactory


class TestStrategy(base.Mixin):
    @pytest.mark.django_db
    def test_get_categories01(self, client):
        """
        User should get categories.
        """
        user = UserFactory.get()
        client = self.get_auth_client(user)

        ForexCategoryFactory.get()
        ForexCategoryFactory.get()
        ForexCategoryFactory.get()

        url = self.get_url('forex_categories')
        r = client.get(url)
        assert r.status_code == 200
        assert len(r.json()) == 3

    @pytest.mark.django_db
    def test_get_symbols01(self, client):
        """
        User should get symbols.
        """
        user = UserFactory.get()
        client = self.get_auth_client(user)

        ForexSymbolFactory.get()
        ForexSymbolFactory.get()
        ForexSymbolFactory.get()

        url = self.get_url('forex_symbols')
        r = client.get(url)
        assert r.status_code == 200
        assert len(r.json()) == 3

    @pytest.mark.django_db
    def test_get_forex_news_filter01(self, client):
        """
        User should get own filter for forex news.
        When filter not exists, it created automatically.
        """
        user = UserFactory.get()
        client = self.get_auth_client(user)

        url = self.get_url('forex_news_filter')
        r = client.get(url)
        assert r.status_code == 200

    @pytest.mark.django_db
    def test_update_forex_news_filter01(self, client):
        """
        User should update forex news own filter.
        """
        user = UserFactory.get()
        client = self.get_auth_client(user)

        filter = user.forex_news_filter
        filter.is_sound = False
        filter.is_all_symbols = False
        filter.is_all_categories = False
        filter.save()

        cat1 = ForexCategoryFactory.get()
        cat2 = ForexCategoryFactory.get()

        sym1 = ForexSymbolFactory.get()
        sym2 = ForexSymbolFactory.get()

        # Update sound
        d = {
            'is_sound': True,
        }

        data = json.dumps(d)
        url = self.get_url('forex_news_filter')
        r = client.patch(url, data, content_type='application/json')
        assert r.status_code == 200

        filter.refresh_from_db()
        assert filter.is_sound is True

        # update symbols
        d = {
            'is_all_symbols': True,
            'forex_symbol_ids': [sym1.id, sym2.id]
        }

        data = json.dumps(d)
        url = self.get_url('forex_news_filter')
        r = client.patch(url, data, content_type='application/json')
        assert r.status_code == 200

        filter.refresh_from_db()
        assert filter.is_all_symbols is True
        assert filter.forex_symbols.count() == 2

        # update categories
        d = {
            'is_all_categories': True,
            'forex_category_ids': [cat1.id, cat2.id],
        }

        data = json.dumps(d)
        url = self.get_url('forex_news_filter')
        r = client.patch(url, data, content_type='application/json')
        assert r.status_code == 200

        filter.refresh_from_db()
        assert filter.is_all_categories is True
        assert filter.forex_categories.count() == 2

    @pytest.mark.django_db
    def test_get_filtered_forex_news01(self, client):
        """
        User should get forex news based on it's filter.

        Data (categories and symbols):

        cat1 sym1
        cat2 sym2
        cat3 sym3

        User filter:

        cat1 sym1
        cat2 sym2

        News:

        N1 cat1, cat2, sym1, sym2 (true)
        N2 cat1, sym1 (true)
        N3 cat1, cat2, cat3 (false)
        N4 cat1, cat2, sym3 (false)
        N5 cat1, cat3, sym1, sym3 (true)

        User should get 3 news
        """
        user = UserFactory.get()
        client = self.get_auth_client(user)

        cat1 = ForexCategoryFactory.get()
        cat2 = ForexCategoryFactory.get()
        cat3 = ForexCategoryFactory.get()

        sym1 = ForexSymbolFactory.get()
        sym2 = ForexSymbolFactory.get()
        sym3 = ForexSymbolFactory.get()

        # User filter
        f = ForexNewsFilter.objects.create(user=user)
        f.is_all_categories = False
        f.is_all_symbols = False
        f.save()

        f.forex_categories.add(cat1, cat2)
        f.forex_symbols.add(sym1, sym2)

        # News
        n1 = ForexNewsFactory.get()
        n1.forex_categories.add(cat1, cat2)
        n1.forex_symbols.add(sym1, sym2)

        n2 = ForexNewsFactory.get()
        n2.forex_categories.add(cat1)
        n2.forex_symbols.add(sym1)

        n3 = ForexNewsFactory.get()
        n3.forex_categories.add(cat1, cat2, cat3)

        n4 = ForexNewsFactory.get()
        n4.forex_categories.add(cat1, cat2)
        n4.forex_symbols.add(sym3)

        n5 = ForexNewsFactory.get()
        n5.forex_categories.add(cat1, cat3)
        n5.forex_symbols.add(sym1, sym3)

        url = self.get_url('forex_news')
        r = client.get(url)
        assert r.status_code == 200
        assert len(r.json()) == 3
