import pytest

from ztrade.tests.web import base
from ztrade.tests.web.factories import UserFactory

from .factories import TimeFrameFactory


class TestTimeFrame(base.Mixin):
    @pytest.mark.django_db
    def test_get_time_frames01(self, client):
        """
        User should get time frames.
        """
        user = UserFactory.get()
        client = self.get_auth_client(user)

        TimeFrameFactory.get()
        TimeFrameFactory.get()
        TimeFrameFactory.get()

        url = self.get_url('time_frames')
        r = client.get(url)
        assert r.status_code == 200
        assert len(r.json()) == 3

    @pytest.mark.django_db
    def test_get_time_frames02(self, client):
        """
        Not auth user should not get timeframes.
        """
        url = self.get_url('time_frames')
        r = client.get(url)
        assert r.status_code == 401
