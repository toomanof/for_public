import pytest

from ztrade.tests.web import base
from ztrade.tests.web.factories import UserFactory
from web.models import Emailer

from .factories import SubscriptionUserFactory


class TestSubscription(base.Mixin):
    @pytest.mark.django_db
    def setup(self):
        super().setup()
        self.init_emailer()

    @pytest.mark.django_db
    def test_send_info_email01(self, client):
        """
        User should get email about subscription
        """
        user = UserFactory.get()
        client = self.get_auth_client(user)

        su = SubscriptionUserFactory.get(user=user)

        url = self.get_url('subscription_send_info_email',
                           id=su.subscription.id)
        r = client.post(url, {})
        assert r.status_code == 204

        # em = Emailer.objects.first()
        # print(em.html)
