import datetime
import factory
from django.utils import timezone
from ztrade.tests.web.factories import DownloadFileFactory
from ztrade.tests.web.factories import UserFactory

from account.models import ForexCategory
from account.models import ForexNews
from account.models import ForexSymbol
from account.models import Indicator
from account.models import Notification
from account.models import Strategy
from account.models import Subscription
from account.models import SubscriptionFile
from account.models import SubscriptionUser
from account.models import TimeFrame
from account.models import Video
from account.models import VideoLesson
from account.models import VideoTag

from web.services.helpers import randstring


class ForexCategoryFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ForexCategory

    name = factory.lazy_attribute(lambda _: randstring(10))

    @staticmethod
    def get():
        return ForexCategoryFactory()


class ForexSymbolFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ForexSymbol

    code = factory.lazy_attribute(lambda _: randstring(10))
    name = factory.lazy_attribute(lambda _: randstring(10))

    @staticmethod
    def get():
        return ForexSymbolFactory()


class ForexNewsFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ForexNews

    title = factory.lazy_attribute(lambda _: randstring(10))
    dt = timezone.now()
    supplier = factory.lazy_attribute(lambda _: randstring(10))

    @staticmethod
    def get(forex_symbols=None, forex_categories=None):
        if forex_symbols is None:
            forex_symbols = [
                ForexSymbolFactory(),
                ForexSymbolFactory(),
                ForexSymbolFactory(),
            ]

        if forex_categories is None:
            forex_categories = [
                ForexCategoryFactory(),
                ForexCategoryFactory(),
                ForexCategoryFactory(),
            ]

        fn = ForexNewsFactory()
        fn.is_all_categories = False
        fn.is_all_symbols = False
        fn.forex_symbols.set(forex_symbols)
        fn.forex_categories.set(forex_categories)
        return fn


class IndicatorFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Indicator

    name = factory.lazy_attribute(lambda _: randstring(10))
    description = factory.lazy_attribute(lambda _: randstring(10))

    @staticmethod
    def get():
        return IndicatorFactory()


class NotificationFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Notification

    text = factory.lazy_attribute(lambda _: randstring(10))

    @staticmethod
    def get(user=None, market=None, type=None, code=None):
        if user is None:
            user = UserFactory.get()

        if type is None:
            type = Notification.SIGNALS

        if code is None:
            code = Notification.SIGNAL_CREATED

        return NotificationFactory(
            user=user, market=market, type=type, code=code)


class TimeFrameFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = TimeFrame

    name = factory.lazy_attribute(lambda _: randstring(10))
    description = factory.lazy_attribute(lambda _: randstring(10))

    @staticmethod
    def get():
        return TimeFrameFactory()


class StrategyFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Strategy

    name = factory.lazy_attribute(lambda _: randstring(10))
    short_description = factory.lazy_attribute(lambda _: randstring(10))
    content = factory.lazy_attribute(lambda _: randstring(30))

    @staticmethod
    def get():
        st = StrategyFactory()
        st.time_frames.set([TimeFrameFactory.get(), TimeFrameFactory.get()])
        st.indicators.set([IndicatorFactory.get(), IndicatorFactory.get()])
        return st


class SubscriptionFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Subscription

    name = factory.lazy_attribute(lambda _: randstring(10))
    code = factory.lazy_attribute(lambda _: randstring(10))
    update_date = timezone.now()
    block_css_class = factory.lazy_attribute(lambda _: randstring(10))
    block_title = factory.lazy_attribute(lambda _: randstring(10))
    block_content = factory.lazy_attribute(lambda _: randstring(10))
    col1_title = factory.lazy_attribute(lambda _: randstring(10))
    col2_title = factory.lazy_attribute(lambda _: randstring(10))
    col3_title = factory.lazy_attribute(lambda _: randstring(10))
    tools_button = factory.lazy_attribute(lambda _: randstring(10))
    tools_button_link = factory.lazy_attribute(lambda _: randstring(10))
    col1_html = factory.lazy_attribute(lambda _: randstring(10))
    col2_html = factory.lazy_attribute(lambda _: randstring(10))
    col3_html = factory.lazy_attribute(lambda _: randstring(10))
    price1 = 100
    price3 = 200
    price6 = 300

    @staticmethod
    def get():
        subscription = SubscriptionFactory()

        for __ in range(3):
            SubscriptionFile.objects.create(
                subscription=subscription,
                name=randstring(10),
                type=SubscriptionFile.INDICATOR,
                download_file=DownloadFileFactory.get())

        return subscription


class SubscriptionUserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = SubscriptionUser

    @staticmethod
    def get(user=None, subscription=None):
        if user is None:
            user = UserFactory.get()

        if subscription is None:
            subscription = SubscriptionFactory.get()

        date_from = datetime.date.today()
        date_to = date_from + datetime.timedelta(days=30)
        return SubscriptionUserFactory(
            user=user, subscription=subscription,
            date_from=date_from, date_to=date_to, is_paid=True)


class VideoTagFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = VideoTag

    name = factory.lazy_attribute(lambda _: randstring(10))

    @staticmethod
    def get():
        return VideoTagFactory()


class VideoLessonFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = VideoLesson

    name = factory.lazy_attribute(lambda _: randstring(10))

    @staticmethod
    def get():
        return VideoLessonFactory()


class VideoFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Video

    name = factory.lazy_attribute(lambda _: randstring(10))

    @staticmethod
    def get():
        v = VideoFactory()
        v.tags.set([VideoTagFactory.get(), VideoTagFactory.get()])
        return v
