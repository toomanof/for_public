import pytest

from ztrade.tests.web import base
from ztrade.tests.web.factories import UserFactory

from .factories import VideoLessonFactory


class TestVideoLessonFrame(base.Mixin):
    @pytest.mark.django_db
    def test_get_videos01(self, client):
        """
        User should get video lessons.
        """
        user = UserFactory.get()
        client = self.get_auth_client(user)

        VideoLessonFactory.get()
        VideoLessonFactory.get()
        VideoLessonFactory.get()

        url = self.get_url('video_lessons')
        r = client.get(url)
        assert r.status_code == 200
        assert len(r.json()) == 3

    @pytest.mark.django_db
    def test_get_videos02(self, client):
        """
        Not auth user should not get video lessons.
        """
        url = self.get_url('video_lessons')
        r = client.get(url)
        assert r.status_code == 401
