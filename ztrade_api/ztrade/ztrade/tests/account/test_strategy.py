import pytest

from ztrade.tests.web import base
from ztrade.tests.web.factories import UserFactory

from .factories import StrategyFactory


class TestStrategy(base.Mixin):
    @pytest.mark.django_db
    def test_get_strategies01(self, client):
        """
        User should get strategies.
        """
        user = UserFactory.get()
        client = self.get_auth_client(user)

        StrategyFactory.get()
        StrategyFactory.get()
        StrategyFactory.get()

        url = self.get_url('strategies')
        r = client.get(url)
        assert r.status_code == 200
        assert len(r.json()['strategies']) == 3
        assert len(r.json()['time_frames']) > 0
        assert len(r.json()['indicators']) > 0

    @pytest.mark.django_db
    def test_get_strategies02(self, client):
        """
        Not auth user should not get strategies.
        """
        url = self.get_url('strategies')
        r = client.get(url)
        assert r.status_code == 401

    @pytest.mark.django_db
    def test_get_strategy01(self, client):
        """
        User should get strategy detail.
        """
        user = UserFactory.get()
        client = self.get_auth_client(user)

        st = StrategyFactory.get()

        url = self.get_url('strategy', id=st.id)
        r = client.get(url)
        assert r.status_code == 200
