import pytest

from ztrade.tests.web import base
from ztrade.tests.web.factories import UserFactory

from .factories import VideoFactory


class TestVideo(base.Mixin):
    @pytest.mark.django_db
    def test_get_videos01(self, client):
        """
        User should get videos.
        """
        user = UserFactory.get()
        client = self.get_auth_client(user)

        VideoFactory.get()
        VideoFactory.get()
        VideoFactory.get()

        url = self.get_url('videos')
        r = client.get(url)
        assert r.status_code == 200
        assert len(r.json()['videos']) == 3
        assert len(r.json()['video_tags']) > 0

    @pytest.mark.django_db
    def test_get_videos02(self, client):
        """
        Not auth user should not get videos.
        """
        url = self.get_url('videos')
        r = client.get(url)
        assert r.status_code == 401
