import json
import pytest

from broker.models import Broker
from broker.models import Review
from web.models import Emailer
from ztrade.tests.web import base
from ztrade.tests.web.factories import UserFactory

from .factories import BrokerFactory
from .data import get_desc_data
from .data import get_review_data


class TestBroker(base.Mixin):
    def setup(self):
        super().setup()
        self.init_emailer()

    @pytest.mark.django_db
    def test_get_brokers(self, client):
        """
        Get list of brokers
        """
        BrokerFactory.get()
        BrokerFactory.get()
        BrokerFactory.get()
        count = Broker.objects.count()

        url = self.get_url('brokers')
        r = client.get(url)
        assert r.status_code == 200
        assert len(r.json()) == count

    @pytest.mark.django_db
    def test_get_broker(self, client):
        """
        Get broker detail page
        """
        b = BrokerFactory.get()

        url = self.get_url('broker', slug=b.slug)
        r = client.get(url)
        assert r.status_code == 200
        # import pprint
        # pprint.pprint(r.json())

    @pytest.mark.django_db
    def test_create_desc01(self, client):
        """
        Test create advantage desc
        Admin should get email.
        """
        user = UserFactory.get()
        client = self.get_auth_client(user)
        d = get_desc_data()
        data = json.dumps(d)
        url = self.get_url('broker_descs')
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 201
        assert Emailer.objects.count() == 1

    @pytest.mark.django_db
    def test_create_desc02(self, client):
        """
        Not auth user can not create desc
        """
        d = get_desc_data()
        data = json.dumps(d)
        url = self.get_url('broker_descs')
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 401

    @pytest.mark.django_db
    def test_create_review01(self, client):
        """
        Test create review by auth user
        Admin should get email.
        """
        user = UserFactory.get()
        client = self.get_auth_client(user)
        d = get_review_data()
        data = json.dumps(d)
        url = self.get_url('broker_reviews_create_auth')
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 201
        assert Emailer.objects.count() == 1
        review = Review.objects.first()
        assert review is not None
        assert review.user_id == user.id

    @pytest.mark.django_db
    def test_create_review02(self, client):
        """
        Test create review by not auth user
        Admin should get email.
        """
        d = get_review_data()
        data = json.dumps(d)
        url = self.get_url('broker_reviews_create_not_auth')
        r = client.post(url, data, content_type='application/json')
        assert r.status_code == 201
        assert Emailer.objects.count() == 1
        review = Review.objects.first()
        assert review is not None
        assert review.user_id is None
