import factory
from random import choice
from django.utils import timezone

from broker.models import AccountType
from broker.models import AccountItem
from broker.models import Broker
from broker.models import Category
from broker.models import Desc
from broker.models import Param
from broker.models import Review

from web.services.helpers import randstring


class AccountTypeFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = AccountType

    name = factory.lazy_attribute(lambda _: randstring(10))

    @staticmethod
    def get():
        return AccountTypeFactory()


class AccountItemFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = AccountItem

    name = 'name'
    value = 'value'

    @staticmethod
    def get(broker=None, account_type=None):
        if broker is None:
            broker = BrokerFactory.get()

        if account_type is None:
            account_type = AccountTypeFactory.get()

        return AccountItemFactory(broker=broker, account_type=account_type)


class CategoryFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Category

    name = factory.lazy_attribute(lambda _: randstring(10))

    @staticmethod
    def get():
        return CategoryFactory()


class DescFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Desc

    text = factory.lazy_attribute(lambda _: randstring(10))
    count = 10

    @staticmethod
    def get(broker=None, type=None):
        if broker is None:
            broker = BrokerFactory.get()

        if type is None:
            type = choice([Desc.ADVANTAGE, Desc.DISADVANTAGE])

        return DescFactory(broker=broker, type=type)


class ParamFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Param

    name = 'name'
    value = 'value'

    @staticmethod
    def get(broker=None, type=None):
        if broker is None:
            broker = BrokerFactory.get()
        return ParamFactory(broker=broker)


class ReviewFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Review

    name = 'name'
    text = factory.lazy_attribute(lambda _: randstring(10))
    count_likes = 10
    created_at = timezone.now()

    @staticmethod
    def get(broker=None, type=None):
        if broker is None:
            broker = BrokerFactory.get()

        if type is None:
            type = choice([Review.POSITIVE, Review.NEUTRAL, Review.NEGATIVE])

        return ReviewFactory(broker=broker, type=type)


class BrokerFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Broker

    slug = factory.lazy_attribute(lambda _: randstring(10))
    rating = 500
    min_deposit = 500
    max_leverage = 500
    payout = 'payout'
    regulation = 'regulation'
    trade_platform = 'trade_platform'
    spread = 1
    site = 'example.com'
    text_items = 'item1\nitem2\nitem3\nitem4\nitem5\nitem6\nitem7'

    @staticmethod
    def get():
        broker = BrokerFactory()

        for _ in range(10):
            AccountItemFactory.get(broker=broker)
            DescFactory.get(broker=broker)
            ParamFactory.get(broker=broker)
            ReviewFactory.get(broker=broker)

        return broker
