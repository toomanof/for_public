from broker.models import Desc
from broker.models import Review
from .factories import BrokerFactory


def get_desc_data():
    return {
        'broker_id': BrokerFactory.get().id,
        'text': 'Some desc',
        'type': Desc.ADVANTAGE,
    }


def get_review_data():
    return {
        'broker_id': BrokerFactory.get().id,
        'text': 'Some review',
        'type': Review.POSITIVE,
        'name': 'some name',
        'email': 'some@email.com'
    }
