import pytest

from broker.models import Category
from ztrade.tests.web import base

from .factories import CategoryFactory


class TestCategory(base.Mixin):
    @pytest.mark.django_db
    def test_get_categories(self, client):
        """
        Get list of categories
        """
        CategoryFactory.get()
        CategoryFactory.get()
        CategoryFactory.get()
        count = Category.objects.count()

        url = self.get_url('broker_categories')
        r = client.get(url)
        assert r.status_code == 200
        assert len(r.json()) == count
