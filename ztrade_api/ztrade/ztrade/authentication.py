"""
Custom TokenAuthentication
"""
from django.utils.translation import ugettext_lazy as _
from django.apps import apps
from django.utils import timezone

from rest_framework.authentication import BaseAuthentication
from rest_framework.authentication import get_authorization_header
from rest_framework.exceptions import AuthenticationFailed

from web.exceptions import TokenExpired
from web.exceptions import TokenInvalid


class TokenAuthentication(BaseAuthentication):
    keyword = 'Token'

    def authenticate(self, request):
        auth = get_authorization_header(request).split()

        if not auth or auth[0].lower() != self.keyword.lower().encode():
            return None

        if len(auth) == 1:
            msg = _('Invalid token header. No credentials provided.')
            raise AuthenticationFailed(msg)
        elif len(auth) > 2:
            msg = 'Invalid token header. '
            msg += ' Token string should not contain spaces.'
            msg = _(msg)
            raise AuthenticationFailed(msg)

        try:
            token = auth[1].decode()
        except UnicodeError:
            msg = 'Invalid token header. '
            msg += 'Token string should not contain invalid characters.'
            msg = _(msg)
            raise AuthenticationFailed(msg)

        return self.authenticate_credentials(token, request)

    def authenticate_credentials(self, token, request):
        Token = apps.get_model('web', 'Token')
        try:
            t = Token.objects.get(key=token)
        except Token.DoesNotExist:
            raise TokenInvalid()

        if t.expire_at is not None:
            if timezone.now() > t.expire_at:
                raise TokenExpired()

        if not t.user.is_active:
            raise AuthenticationFailed(
                _('User inactive.'))

        return (t.user, token)

    def authenticate_header(self, request):
        return self.keyword
