from .base import *

DEBUG = True
PRODUCTION = False
TESTING = True


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'ztrade/db', DB_NAME)
    }
}

# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.postgresql',
#         'NAME': DB_NAME,
#         'USER': DB_USER,
#         'PASSWORD': DB_PASSWORD,
#         'HOST': DB_HOST,
#         'PORT': '5432'
#     }
# }

AWS_ACCESS_KEY_ID = AWS_ACCESS_KEY_ID_DEV
AWS_SECRET_ACCESS_KEY = AWS_SECRET_ACCESS_KEY_DEV
AWS_REGION = AWS_REGION_DEV
AWS_STORAGE_BUCKET_NAME = AWS_STORAGE_BUCKET_NAME_DEV
AWS_S3_REGION_NAME = 'eu-central-1'
AWS_S3_SIGNATURE_VERSION = 's3v4'
BUCKET_URL = 'https://{}.s3.amazonaws.com'.format(AWS_STORAGE_BUCKET_NAME)

DEFAULT_FILE_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'
MEDIA_URL = 'https://{}.s3.amazonaws.com/'.format(AWS_STORAGE_BUCKET_NAME)

YANDEX_SHOP_ID = YANDEX_SHOP_ID_DEV
YANDEX_SECRET_KEY = YANDEX_SECRET_KEY_DEV
