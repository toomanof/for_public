import json
import os
import redis
from os.path import abspath, join, dirname
from corsheaders.defaults import default_headers

BASE_DIR = abspath(join(dirname(abspath(__file__)), '..', '..', '..'))

data_path = os.path.join(BASE_DIR, 'ztrade/ztrade/settings/data.json')
data = json.loads(open(data_path).read())

cent_path = os.path.join(BASE_DIR, 'ztrade/ztrade/settings/centrifugo.json')
cent_data = json.loads(open(cent_path).read())

LOGIN_URL = '/auth/login/'

ALLOWED_HOSTS = data['ALLOWED_HOSTS']
SECRET_KEY = data['SECRET_KEY']
DB_NAME = data['DB_NAME']
DB_USER = data['DB_USER']
DB_PASSWORD = data['DB_PASSWORD']
DB_HOST = data['DB_HOST']
SERVER_IP = data['SERVER_IP']
SMS_LOGIN = data['SMS_LOGIN']
SMS_PASSWORD = data['SMS_PASSWORD']

AWS_ACCESS_KEY_ID_PROD = data['AWS_ACCESS_KEY_ID_PROD']
AWS_SECRET_ACCESS_KEY_PROD = data['AWS_SECRET_ACCESS_KEY_PROD']
AWS_REGION_PROD = data['AWS_REGION_PROD']
AWS_STORAGE_BUCKET_NAME_PROD = data['AWS_STORAGE_BUCKET_NAME_PROD']

AWS_ACCESS_KEY_ID_DEV = data['AWS_ACCESS_KEY_ID_DEV']
AWS_SECRET_ACCESS_KEY_DEV = data['AWS_SECRET_ACCESS_KEY_DEV']
AWS_REGION_DEV = data['AWS_REGION_DEV']
AWS_STORAGE_BUCKET_NAME_DEV = data['AWS_STORAGE_BUCKET_NAME_DEV']

# Yandex Kassa
YANDEX_SHOP_ID_PROD = data['YANDEX_SHOP_ID_PROD']
YANDEX_SECRET_KEY_PROD = data['YANDEX_SECRET_KEY_PROD']
YANDEX_SHOP_ID_DEV = data['YANDEX_SHOP_ID_DEV']
YANDEX_SECRET_KEY_DEV = data['YANDEX_SECRET_KEY_DEV']

CENTRIFUGO_API_KEY = cent_data['api_key']
CENTRIFUGO_SECRET = cent_data['secret']
CENTRIFUGO_LOCAL_URL = 'http://127.0.0.1:9000'
CENTRIFUGO_SERVER_URL = 'http://{}:9000'.format(SERVER_IP)

BACKEND_SCHEME = 'https://'
BACKEND_DOMAIN = 'api.ztrade.ru'
BACKEND_URL = BACKEND_SCHEME + BACKEND_DOMAIN

FRONTEND_SCHEME = 'https://'
FRONTEND_DOMAIN = 'ztrade.ru'
FRONTEND_URL = FRONTEND_SCHEME + FRONTEND_DOMAIN

AUTH_USER_MODEL = 'web.User'
APPEND_SLASH = False

DEFAULT_FROM_EMAIL = 'support@ztrade.ru'
EMAIL_HOST = 'smtp.yandex.ru'
EMAIL_HOST_PASSWORD = '187@HQSv187nd8x'
EMAIL_HOST_USER = 'support@ztrade.ru'
EMAIL_USE_TLS = True
EMAIL_PORT = '465'
EMAIL_SUBJECT_PREFIX = ''

TRELLO_FROM_EMAIL = 'user09459619+pjmc941dssjfj4ooud1r@boards.trello.com'

ADMIN_EMAIL = 'ztrade.ru@bk.ru'

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'storages',
    'django_filters',
    'rest_framework',
    'rest_framework_swagger',
    'mptt',
    'corsheaders',
    'ckeditor',
    'ckeditor_uploader',
    'django_premailer',
    'account',
    'bot',
    'broker',
    'content',
    'payment',
    'shop',
    'sgn',
    'web',
    'course',
    'exchange',
    'robot',
)

MIDDLEWARE = (
    'web.middleware.AccessControlMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'web.middleware.ActiveUserMiddleware',
)

ROOT_URLCONF = 'ztrade.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [join(BASE_DIR, 'ztrade/ztrade/templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'ztrade.wsgi.application'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': DB_NAME,
        'USER': DB_USER,
        'PASSWORD': DB_PASSWORD,
        'HOST': DB_HOST,
        'PORT': '5432'
    }
}

LANGUAGE_CODE = 'ru-RU'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True

MEDIA_ROOT = BASE_DIR + '/static/media/'
STATIC_URL = '/static/'

FILE_UPLOAD_HANDLERS = (
    'django.core.files.uploadhandler.MemoryFileUploadHandler',
    'django.core.files.uploadhandler.TemporaryFileUploadHandler',
)

FILE_UPLOAD_TEMP_DIR = MEDIA_ROOT + 'tmp/'

STATICFILES_DIRS = [join(BASE_DIR, 'static')]

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)


BROKER_URL = 'redis://127.0.0.1:6379/3'
CELERY_ACCEPT_CONTENT = ['json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'


REST_FRAMEWORK = {
    'DEFAULT_FILTER_BACKENDS': (
        'django_filters.rest_framework.DjangoFilterBackend',
    ),
    'DEFAULT_RENDERER_CLASSES': (
        'rest_framework.renderers.JSONRenderer',
        'rest_framework.renderers.BrowsableAPIRenderer'
    ),
    'DEFAULT_PARSER_CLASSES': (
        'rest_framework.parsers.JSONParser',
        'rest_framework.parsers.FormParser',
        'rest_framework.parsers.MultiPartParser'
    ),
    'DEFAULT_PERMISSION_CLASSES': (),
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.BasicAuthentication',
        'rest_framework.authentication.SessionAuthentication',
        'ztrade.authentication.TokenAuthentication'
    ),
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.LimitOffsetPagination',
    'DATETIME_FORMAT': '%Y-%m-%dT%H:%M:%S',
    'EXCEPTION_HANDLER': 'web.exceptions.custom_exception_handler',
    'NON_FIELD_ERRORS_KEY': 'errors'
}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,

    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        },
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue'
        }
    },

    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d \
            %(thread)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },

    'handlers': {
        'console': {
            'filters': ['require_debug_true'],
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
        'db': {
            'class': 'ztrade.loggers.MyDbLogHandler',
            'formatter': 'verbose'
        }
    },

    'loggers': {
        'django': {
            'level': 'INFO',
            'handlers': ['console'],
            'propagate': True,
        },

        # 'django.db.backends': {
        #     'handlers': ['console'],
        #     'level': 'DEBUG',
        # },

        'django.request': {
            'handlers': ['db'],
            'level': 'ERROR',
            'propagate': False,
        },

        'app': {
            'level': 'DEBUG',
            'handlers': ['console', 'db'],
            'propagate': False,
        }
    }
}

# Custom token auth.
TOKEN_EXPIRE_MINUTES = 1400

REDIS_DB = redis.StrictRedis(host='127.0.0.1', port=6379, db=4)

ALLOWED_FILE_EXTENTIONS = (
    'png', 'jpg', 'jpeg', 'gif', 'doc', 'docx', 'xls', 'xlsx',
    'odt', 'pdf', 'zip', 'rar'
)

# Types for websocket server
# Set the same constants to WSUpdater.js
NEWS_CREATED = 'NEWS_CREATED'
SIGNAL_CREATED = 'SIGNAL_CREATED'
SIGNAL_STOP_LOSS_UPDATED = 'SIGNAL_STOP_LOSS_UPDATED'
SIGNAL_TAKE_PROFIT_UPDATED = 'SIGNAL_TAKE_PROFIT_UPDATED'
SIGNAL_CLOSED = 'SIGNAL_CLOSED'
NOTIFICATION_CREATED = 'NOTIFICATION_CREATED'


# CKEDITOR
CKEDITOR_BASEPATH = '/static/ckeditor/ckeditor/'
CKEDITOR_UPLOAD_PATH = 'uploads/'
CKEDITOR_RESTRICT_BY_USER = False
CKEDITOR_BROWSE_SHOW_DIRS = True
CKEDITOR_RESTRICT_BY_DATE = True
CKEDITOR_ALLOW_NONIMAGE_FILES = True
AWS_QUERYSTRING_AUTH = False

CKEDITOR_CONFIGS = {
    'default': {
        'toolbar': 'full',
        'height': 700,
        'width': 700,
    },
}
CORS_ORIGIN_ALLOW_ALL = True
CORS_ALLOWED_ORIGINS = [
"127.0.0.1", "localhost", "ztrade.ru", "api.ztrade.ru"
]
CSRF_TRUSTED_ORIGINS = [
"127.0.0.1", "localhost", "ztrade.ru", "api.ztrade.ru"
]

CORS_ALLOW_HEADERS = list(default_headers) + [
    'Cookie', 'cookie', 'csrftoken', 'x-csrfmiddlewaretoken', 'X-CSRFToken',
    'HTTP_X_FORWARDED_PROTO'
]


EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

CSRF_COOKIE_SECURE = True
CSRF_TRUSTED_ORIGINS = ["127.0.0.1", "localhost", "ztrade-files.s3.amazonaws.com", "ztrade-dev.s3.amazonaws.com"]
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
SESSION_COOKIE_SECURE = True

TELEGRAM_BOT_TOKEN = '1752933501:AAHu8esLBZfSVyrtPglN0qFBWG5loVMwAyA'
