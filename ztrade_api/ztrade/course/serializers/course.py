from rest_framework.serializers import ModelSerializer

from account.serializers import VideoSerializer
from ..models import Course, Market, Lesson, Carrying


class LessonSerializer(ModelSerializer):
    class Meta:
        model = Lesson
        fields = '__all__'


class CarryingSerializer(ModelSerializer):
    class Meta:
        model = Carrying
        fields = '__all__'


class CourseSerializer(ModelSerializer):
    lessons = LessonSerializer(read_only=True, many=True)
    carrying = CarryingSerializer(read_only=True, many=True)
    video = VideoSerializer(read_only=True)

    class Meta:
        model = Course
        fields = ('id', 'guid', 'title', 'h1', 'name', 'logo', 'description',
                  'file', 'paid', 'analysis', 'market', 'video', 'price',
                  'discount_price', 'end_discount', 'difference_end_discount',
                  'lessons', 'carrying')


class MarketSerializer(ModelSerializer):
    class Meta:
        model = Market
        fields = '__all__'
