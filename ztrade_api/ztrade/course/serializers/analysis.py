from rest_framework.serializers import ModelSerializer

from ..models import Analysis


class AnalysisSerializer(ModelSerializer):
    class Meta:
        model = Analysis
        fields = '__all__'
