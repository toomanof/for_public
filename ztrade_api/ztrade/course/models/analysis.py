from django.db import models
from django.contrib import admin


class Analysis(models.Model):
    name = models.CharField('название', max_length=100)
    sort = models.IntegerField('сортировка', default=0)

    class Meta:
        verbose_name = 'Метод аналализа'
        verbose_name_plural = 'Методы аналализа'
        ordering = ['sort']

    def __str__(self):
        return self.name


@admin.register(Analysis)
class AnalysisAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'sort'
    )
    list_editable = ('sort',)
