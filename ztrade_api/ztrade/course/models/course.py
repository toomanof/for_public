import uuid

from django.db import models
from django.db.models.signals import post_delete
from django.dispatch import receiver
from django.contrib import admin
from django.utils import timezone

from web.services.helpers import get_guid


from .analysis import Analysis

def upload_to(instance, filename):
    ext = filename.split('.')[-1]
    filename = '{}.{}'.format(uuid.uuid4().hex, ext)

    pth = 'download/courses/'

    pth += filename
    return pth


class Market(models.Model):
    name = models.CharField('Название', max_length=255)
    sort = models.IntegerField('сортировка', default=0)
    published = models.BooleanField('Публикация', default=False)
    all_sub_menu = models.BooleanField('Отображать подменю', default=False)

    class Meta:
        verbose_name = 'Рынок'
        verbose_name_plural = 'Рынки'
        ordering = ['sort']

    def __str__(self):
        return self.name


@admin.register(Market)
class MarketAdmin(admin.ModelAdmin):
    list_display = ('name', 'sort', 'published')
    list_editable = ('sort', 'published')


class Course(models.Model):
    guid = models.CharField(max_length=50, default=get_guid)
    title = models.CharField(max_length=255, null=True, default=None, blank=True)
    h1 = models.CharField(max_length=255, null=True, default=None, blank=True)
    name = models.CharField('название', max_length=100)
    logo = models.ImageField(upload_to='images/logos/regular', null=True,
                             default=None, blank=True)
    description = models.TextField('Описание')
    file = models.FileField('файл', upload_to=upload_to,
                            null=True, blank=True)
    paid = models.BooleanField('Платный', default=False)
    analysis = models.ForeignKey(Analysis, on_delete=models.CASCADE,
                                 verbose_name='Методы анализа',
                                 related_name='courses',
                                 null=True, blank=True)
    market = models.ForeignKey(Market, on_delete=models.CASCADE,
                               verbose_name='Рынок',
                               related_name='courses',
                               null=True, blank=True)
    video = models.ForeignKey('account.Video', on_delete=models.PROTECT,
                              verbose_name='Видео',
                              related_name='courses',
                              null=True, blank=True)
    price = models.PositiveIntegerField('Цена', default=0)
    discount_price = models.PositiveIntegerField('Цена со скидкой', default=0)
    end_discount = models.DateTimeField('Конец окончания акции', null=True, blank=True)

    class Meta:
        verbose_name = 'курс'
        verbose_name_plural = 'курсы'

    def __str__(self):
        return self.name

    @property
    def difference_end_discount(self):
        now = timezone.now()
        if not self.end_discount:
            return
        if now >= self.end_discount:
            return

        diff = self.end_discount - now

        diff_minutes = divmod(diff.total_seconds(), 60)
        diff_days = int(diff_minutes[0] // 60 // 24)
        diff_hours = int(diff_minutes[0] // 60 - diff_days * 24)
        diff_min = int((diff_minutes[0]) - (diff_days * 24 * 60 + diff_hours * 60))

        return {
            'now': now,
            'days': diff_days,
            'hours': diff_hours,
            'minutes': diff_min,
        }


@receiver(post_delete, sender=Course)
def delete_file(sender, instance, **kwargs):
    if instance.file:
        instance.file.delete()


class Lesson(models.Model):
    course = models.ForeignKey(
        Course, verbose_name='Курс', related_name='lessons', on_delete=models.CASCADE)
    title = models.CharField('название', max_length=100)


class LessonInline(admin.TabularInline):
    model = Lesson
    fields = ('title', )
    extra = 0


class Carrying(models.Model):
    course = models.ForeignKey(
        Course, verbose_name='Курс', related_name='carrying', on_delete=models.CASCADE)
    title = models.CharField('название', max_length=100)


class Carryingline(admin.TabularInline):
    model = Carrying
    fields = ('title', )
    extra = 0


@admin.register(Course)
class CourseAdmin(admin.ModelAdmin):
    inlines = [
        Carryingline, LessonInline,
    ]

    list_display = ('title', 'description',)
