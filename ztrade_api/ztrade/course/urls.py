from django.urls import re_path
from .import views as v

urlpatterns = [
    re_path(
        r'^(?P<version>(v1))/courses/analysis$',
        v.AnalysisView.as_view(),
        name='courses'),
    re_path(
        r'^(?P<version>(v1))/courses$',
        v.CourseView.as_view(),
        name='courses'),

    re_path(
        r'^(?P<version>(v1))/courses/markets$',
        v.MarketView.as_view(),
        name='markets'),
    ]
