from rest_framework.generics import ListAPIView

from ..models import Analysis
from ..serializers import AnalysisSerializer


class AnalysisView(ListAPIView):
    """
    get:
        <h1>Get analysis </h1>
        <b>Response:</b> list of analysis object<br />
        <b>Response code:</b> 200
    """
    queryset = Analysis.objects.all()
    serializer_class = AnalysisSerializer
