from rest_framework.generics import ListAPIView

from web.services.helpers import camel_case_to_under_case as cc_to_uc
from ..models import Course, Market
from ..serializers import CourseSerializer, MarketSerializer


class CourseView(ListAPIView):
    """
    get:
        <h1>Get courses </h1>
        <b>Params to GET:</b><br>
        paid - bool,<br>
        finMarket- int,<br>
        stokMarket - int,<br>
        cryptoMarket - int<br>
        <b>Response:</b> list of course object<br />
        <b>Response code:</b> 200
    """
    queryset = Course.objects.all()
    serializer_class = CourseSerializer
    filter_params = {}

    def filter_queryset(self, queryset):
        if self.filter_params:
            queryset = queryset.filter(**self.filter_params)
        else:
            queryset = queryset.all()
        return queryset

    def list(self, request, version):
        self.filter_params = {}

        get_dict = cc_to_uc(dict(request.GET.items()))
        for key, value in get_dict.items():
            value = value.title() if value in ('true', 'false') else value
            self.filter_params[key] = value
        return super().list(request)


class MarketView(ListAPIView):
    """
    get:
        <h1>Get markets </h1>
        <b>Response:</b> list of market object<br />
        <b>Response code:</b> 200
    """
    queryset = Market.objects.all()
    serializer_class = MarketSerializer

