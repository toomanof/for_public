# Generated by Django 2.0.4 on 2020-12-17 16:15

import course.models.course
from django.db import migrations, models
import django.db.models.deletion
import web.services.helpers


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('account', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Analysis',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, verbose_name='название')),
                ('sort', models.IntegerField(default=0, verbose_name='сортировка')),
            ],
            options={
                'verbose_name': 'Метод аналализа',
                'verbose_name_plural': 'Методы аналализа',
                'ordering': ['sort'],
            },
        ),
        migrations.CreateModel(
            name='Carrying',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=100, verbose_name='название')),
            ],
        ),
        migrations.CreateModel(
            name='Course',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('guid', models.CharField(default=web.services.helpers.get_guid, max_length=50)),
                ('title', models.CharField(blank=True, default=None, max_length=255, null=True)),
                ('h1', models.CharField(blank=True, default=None, max_length=255, null=True)),
                ('name', models.CharField(max_length=100, verbose_name='название')),
                ('logo', models.ImageField(blank=True, default=None, null=True, upload_to='images/logos/regular')),
                ('description', models.TextField(verbose_name='Описание')),
                ('file', models.FileField(blank=True, null=True, upload_to=course.models.course.upload_to, verbose_name='файл')),
                ('paid', models.BooleanField(default=False, verbose_name='Платный')),
                ('price', models.PositiveIntegerField(default=0, verbose_name='Цена')),
                ('discount_price', models.PositiveIntegerField(default=0, verbose_name='Цена со скидкой')),
                ('end_discount', models.DateTimeField(blank=True, null=True, verbose_name='Конец окончания акции')),
                ('analysis', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='courses', to='course.Analysis', verbose_name='Методы анализа')),
            ],
            options={
                'verbose_name': 'курс',
                'verbose_name_plural': 'курсы',
            },
        ),
        migrations.CreateModel(
            name='Lesson',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=100, verbose_name='название')),
                ('course', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='lessons', to='course.Course', verbose_name='Курс')),
            ],
        ),
        migrations.CreateModel(
            name='Market',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, verbose_name='Название')),
                ('sort', models.IntegerField(default=0, verbose_name='сортировка')),
                ('published', models.BooleanField(default=False, verbose_name='Публикация')),
            ],
            options={
                'verbose_name': 'Рынок',
                'verbose_name_plural': 'Рынки',
                'ordering': ['sort'],
            },
        ),
        migrations.AddField(
            model_name='course',
            name='market',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='courses', to='course.Market', verbose_name='Рынок'),
        ),
        migrations.AddField(
            model_name='course',
            name='video',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='courses', to='account.Video', verbose_name='Видео'),
        ),
        migrations.AddField(
            model_name='carrying',
            name='course',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='carrying', to='course.Course', verbose_name='Курс'),
        ),
    ]
