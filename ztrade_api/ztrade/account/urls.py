from django.urls import re_path
from .import views as v

urlpatterns = [
    re_path(
        r'^(?P<version>(v1))/account/strategies$',
        v.Strategies.as_view(),
        name='strategies'),

    re_path(
        r'^(?P<version>(v1))/account/strategies/(?P<id>\d+)$',
        v.StrategyDetail.as_view(),
        name='strategy'),

    re_path(
        r'^(?P<version>(v1))/account/time_frames$',
        v.TimeFrames.as_view(),
        name='time_frames'),

    re_path(
        r'^(?P<version>(v1))/account/forex_categories$',
        v.ForexCategories.as_view(),
        name='forex_categories'),

    re_path(
        r'^(?P<version>(v1))/account/forex_symbols$',
        v.ForexSymbols.as_view(),
        name='forex_symbols'),

    re_path(
        r'^(?P<version>(v1))/account/forex_news$',
        v.ForexNewsView.as_view(),
        name='forex_news'),

    re_path(
        r'^(?P<version>(v1))/account/forex_news_filter$',
        v.ForexNewsFilterView.as_view(),
        name='forex_news_filter'),

    re_path(
        r'^(?P<version>(v1))/account/subscriptions$',
        v.Subscriptions.as_view(),
        name='subscriptions'),

    re_path(
        r'^(?P<version>(v1))/account/subscriptions/(?P<id>\d+)/send_info_email$',
        v.SubscriptionSendInfoEmail.as_view(),
        name='subscription_send_info_email'),

    re_path(
        r'^(?P<version>(v1))/account/subscriptions/send-active-info$',
        v.SubscriptionsSendActiveInfoToTrello.as_view(),
        name='send_active_info_to_trello'),

    re_path(
        r'^(?P<version>(v1))/account/tutorial_book$',
        v.TutorialBookView.as_view(),
        name='tutorial_book'),

    re_path(
        r'^(?P<version>(v1))/account/user_settings$',
        v.UserSettingsView.as_view(),
        name='user_settings'),

    re_path(
        r'^(?P<version>(v1))/account/videos$',
        v.Videos.as_view(),
        name='videos'),

    re_path(
        r'^(?P<version>(v1))/account/video_lessons$',
        v.VideoLessons.as_view(),
        name='video_lessons'),

    re_path(
        r'^(?P<version>(v1))/account/notifications$',
        v.Notifications.as_view(),
        name='notifications'),

    re_path(
        r'^(?P<version>(v1))/account/notifications/not_read_count$',
        v.NotificationsNotReadCount.as_view(),
        name='notifications_not_read_count'),

    re_path(
        r'^(?P<version>(v1))/account/notifications/mark_all$',
        v.NotificationsMarkAll.as_view(),
        name='notifications_mark_all'),

    re_path(
        r'^(?P<version>(v1))/account/notifications/mark$',
        v.NotificationsMark.as_view(),
        name='notifications_mark'),

    re_path(
        r'^(?P<version>(v1))/account/notifications/remove$',
        v.NotificationsRemove.as_view(),
        name='notifications_remove'),

    re_path(
        r'^(?P<version>(v1))/account/free_period$',
        v.FreePeriodView.as_view(),
        name='forex_categories'),
]
