from rest_framework.views import APIView
from rest_framework.generics import ListAPIView
from rest_framework.generics import RetrieveAPIView
from rest_framework.response import Response
from django.shortcuts import get_object_or_404

from account.serializers import ForexCategorySerializer
from account.serializers import ForexSymbolSerializer
from account.serializers import ForexNewsSerializer
from account.serializers import ForexNewsFilterSerializer
from account.serializers import ForexNewsFilterUpdateSerializer
from account.serializers import NotificationSerializer
from account.serializers import StrategySerializer
from account.serializers import StrategyExtSerializer
from account.serializers import SubscriptionPaidSerializer
from account.serializers import SubscriptionNotPaidSerializer
from account.serializers import IndicatorSerializer
from account.serializers import TimeFrameSerializer
from account.serializers import TutorialBookSerializer
from account.serializers import UserSettingsSerializer
from account.serializers import UserSettingsUpdateSerializer
from account.serializers import VideoSerializer
from account.serializers import VideoLessonSerializer
from account.serializers import VideoTagSerializer
from account.serializers import FreePeriodSerializer

from account.models import ForexCategory
from account.models import ForexSymbol
from account.models import Indicator
from account.models import Notification
from account.models import Strategy
from account.models import Subscription
from account.models import SubscriptionUser
from account.models import TimeFrame
from account.models import TutorialBook
from account.models import UserSettings
from account.models import Video
from account.models import VideoLesson
from account.models import VideoTag
from account.models import FreePeriod
from web.models import Email
from web.models import Emailer

from web.views import AuthMixin


class ForexCategories(AuthMixin, ListAPIView):
    queryset = ForexCategory.objects.all()
    serializer_class = ForexCategorySerializer


class ForexSymbols(AuthMixin, ListAPIView):
    queryset = ForexSymbol.objects.all()
    serializer_class = ForexSymbolSerializer


class ForexNewsView(AuthMixin, ListAPIView):
    serializer_class = ForexNewsSerializer

    def get_queryset(self):
        """
        Returns 10 freshest news
        """
        qs = self.request.user.forex_news_qs
        return qs.all()[:10]


class ForexNewsFilterView(AuthMixin, APIView):
    """
    get:
        <h1>User filter for forex news</h1>
        <b>Response:</b> filter object<br />
        <b>Response code:</b> 200
    patch:
        <h1>Update filter endpoint</h1>
        <p>If is_all_categories passed, then forex_category_ids are ignorred.
        If is_all_symbols passed, then forex_symbol_ids are ignored.
        </p>
        <b>Input params:</b><br />
        forex_symbol_ids<br />
        forex_category_ids<br />
        is_all_symbols (bool)<br />
        is_all_categories (bool)<br />
        is_sound (bool)<br />
        <b>Response:</b> updated filter object<br />
        <b>Response code:</b> 200
    """

    def get(self, request, **kwargs):
        filter = request.user.forex_news_filter
        s = ForexNewsFilterSerializer(filter)
        return Response(s.data)

    def patch(self, request, **kwargs):
        filter = request.user.forex_news_filter
        s = ForexNewsFilterUpdateSerializer(filter, data=request.data)
        s.is_valid(raise_exception=True)
        obj = s.save()
        s = ForexNewsFilterSerializer(obj)
        return Response(s.data)


class Strategies(AuthMixin, APIView):
    """
    Returns strategies, time_frames used in strategies,
    indicators used in strategies.
    """

    def get(self, request, **kwargs):
        qs = Strategy.objects\
            .prefetch_related('time_frames', 'indicators').all()
        s = StrategySerializer(qs, many=True)

        tids = []
        for obj in qs:
            if obj.time_frames.count() > 0:
                tids += [x.id for x in obj.time_frames.all()]
        tids = list(set(tids))

        iids = []
        for obj in qs:
            if obj.indicators.count() > 0:
                iids += [x.id for x in obj.indicators.all()]
        iids = list(set(iids))

        qs1 = TimeFrame.objects.filter(id__in=tids).order_by('sort')
        qs2 = Indicator.objects.filter(id__in=iids).order_by('sort')

        d = {
            'strategies': s.data,
            'time_frames': TimeFrameSerializer(qs1, many=True).data,
            'indicators': IndicatorSerializer(qs2, many=True).data,
        }
        return Response(d)


class StrategyDetail(AuthMixin, RetrieveAPIView):
    lookup_field = 'id'
    queryset = Strategy.objects.all()
    serializer_class = StrategyExtSerializer


class Subscriptions(AuthMixin, APIView):
    """
    Each subscription received separately.
    Depends on whether user subscribed to this subscription
    or not.
    """

    def get(self, request, **kwargs):
        user = request.user

        l = []
        qs = Subscription.objects.filter(is_visible=True)
        for obj in qs:
            su = SubscriptionUser.objects.get_su(user.id, obj.id)

            if su is None:
                s = SubscriptionNotPaidSerializer(obj)
            else:
                s = SubscriptionPaidSerializer(obj, context={'su': su})
            l.append(s.data)
        return Response(l)


class SubscriptionSendInfoEmail(AuthMixin, APIView):
    """
    Send email to user with info about subscription
    """

    def post(self, request, id, **kwargs):
        user = request.user

        su = SubscriptionUser.objects.get_su(user.id, id)
        if su is None:
            return Response(status=400)

        Emailer.objects.process_email(
            Email.GET_SUBSCRIPTION_INFO, user=user, su=su)
        return Response(status=204)


class SubscriptionsSendActiveInfoToTrello(AuthMixin, APIView):
    """
    Send email to user with info about subscription
    """

    def post(self, request, **kwargs):
        user = request.user
        print('SubscriptionsSendActiveInfoToTrello', request.data)
        Emailer.objects.process_email(
            Email.GET_SUBSCRIPTION_ACTIVE_INFO, user=user, **request.data)
        return Response(status=204)



class TimeFrames(AuthMixin, ListAPIView):
    queryset = TimeFrame.objects.all()
    serializer_class = TimeFrameSerializer


class TutorialBookView(AuthMixin, APIView):
    """
    Model contains only 1 item
    """

    def get(self, request, **kwargs):
        tb = TutorialBook.objects.first()
        s = TutorialBookSerializer(tb)
        return Response(s.data)


class UserSettingsView(AuthMixin, APIView):
    """
    get:
        <h1>Get user settings</h1>
    patch:
        <h1>Update user settings</h1>
    """

    def get(self, request, **kwargs):
        us, __ = UserSettings.objects.get_or_create(user=request.user)
        s = UserSettingsSerializer(us)
        return Response(s.data)

    def patch(self, request, **kwargs):
        us = UserSettings.objects.get(user=request.user)
        s = UserSettingsUpdateSerializer(us, data=request.data, partial=True)
        s.is_valid(raise_exception=True)
        s.save()
        return Response(s.data)


class Videos(AuthMixin, APIView):
    """
    Returns videos, video tags used in videos
    """

    def get(self, request, **kwargs):
        qs = Video.objects.prefetch_related('tags').all()
        s = VideoSerializer(qs, many=True)

        tids = []
        for obj in qs:
            if obj.tags.count() > 0:
                tids += [x.id for x in obj.tags.all()]
        tids = list(set(tids))

        qs1 = VideoTag.objects.filter(id__in=tids).order_by('sort')

        d = {
            'videos': s.data,
            'video_tags': VideoTagSerializer(qs1, many=True).data,
        }
        return Response(d)


class VideoLessons(AuthMixin, ListAPIView):
    queryset = VideoLesson.objects.all()
    serializer_class = VideoLessonSerializer


class NotificationsMixin:
    def get_queryset(self):
        qs = Notification.objects.filter(user_id=self.request.user.id)

        type = self.request.query_params.get('type', Notification.GLOBAL)
        qs = qs.filter(type=type)

        market_id = self.request.query_params.get('market_id', None)
        if market_id is not None:
            qs = qs.filter(market_id=int(market_id))

        return qs


class Notifications(AuthMixin, NotificationsMixin, ListAPIView):
    """
    get:
        <h1>Get notifications</h1>
        <p>query param: type: global | signals | news</p>
        <p>query param: market_id for signals and news</p>
        <p>If no passing type, means global notifications</p>
    """
    serializer_class = NotificationSerializer


class NotificationsNotReadCount(AuthMixin, NotificationsMixin, APIView):
    """
    get:
        <h1>Get count not read notifications</h1>
        <p>query param: market_id</p>
        <p>Passing market_id means market notifications</p>
        <p>No market_id means global notifications</p>
    """
    def get(self, request, **kwargs):
        qs = self.get_queryset()
        d = {'count': qs.filter(is_read=False).count()}
        return Response(d)


class NotificationsMarkAll(AuthMixin, NotificationsMixin, APIView):
    """
    post:
        <h1>Mark all notifications as read</h1>
        <p>query param: market_id</p>
        <p>Passing market_id means market notifications</p>
        <p>No market_id means global notifications</p>
    """
    def post(self, request, **kwargs):
        qs = self.get_queryset()
        qs.update(is_read=True)
        return Response(status=204)


class NotificationsMark(AuthMixin, APIView):
    def post(self, request, **kwargs):
        ids = request.data.get('ids', None)
        if ids is not None:
            qs = Notification.objects.filter(user_id=request.user.id)
            qs.filter(id__in=ids).update(is_read=True)
        return Response(status=204)


class NotificationsRemove(AuthMixin, APIView):
    def post(self, request, **kwargs):
        ids = request.data.get('ids', None)
        if ids is not None:
            qs = Notification.objects.filter(user_id=request.user.id)
            qs.filter(id__in=ids).delete()
        return Response(status=204)


class FreePeriodView(AuthMixin, APIView):
    """
    get:
        <h1>Get user settings</h1>
    patch:
        <h1>Update user settings</h1>
    """

    def get(self, request, **kwargs):
        free_period = get_object_or_404(FreePeriod,
                                        user=request.user)
        return Response(FreePeriodSerializer(free_period).data)

    def post(self, request, **kwargs):
        free_period = FreePeriod.objects.create(user=request.user)
        return Response(FreePeriodSerializer(free_period).data)
