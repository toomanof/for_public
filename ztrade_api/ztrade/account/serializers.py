from rest_framework.serializers import ModelSerializer
from rest_framework.serializers import ModelField
from rest_framework.serializers import BooleanField
from rest_framework.serializers import ListField
from rest_framework.serializers import IntegerField
from rest_framework.serializers import SerializerMethodField

from account.models import ForexCategory
from account.models import ForexSymbol
from account.models import ForexNewsFilter
from account.models import ForexNews
from account.models import Indicator
from account.models import Notification
from account.models import Strategy
from account.models import Subscription
from account.models import SubscriptionFile
from account.models import SubscriptionVideoBegin
from account.models import SubscriptionVideo
from account.models import TimeFrame
from account.models import TutorialBook
from account.models import UserSettings
from account.models import Video
from account.models import VideoLesson
from account.models import VideoTag
from account.models import FreePeriod


class ForexCategorySerializer(ModelSerializer):
    class Meta:
        model = ForexCategory
        fields = (
            'id',
            'name'
        )


class ForexSymbolSerializer(ModelSerializer):
    class Meta:
        model = ForexSymbol
        fields = (
            'id',
            'code',
            'name',
        )


class ForexNewsSerializer(ModelSerializer):
    class Meta:
        model = ForexNews
        fields = (
            'id',
            'title',
            'dt',
            'supplier',
        )


class ForexNewsFilterSerializer(ModelSerializer):
    user_id = ModelField(model_field=ForexNewsFilter._meta.get_field('user'))
    forex_category_ids = SerializerMethodField()
    forex_symbol_ids = SerializerMethodField()

    class Meta:
        model = ForexNewsFilter
        fields = (
            'id',
            'user_id',
            'is_sound',
            'is_all_categories',
            'is_all_symbols',
            'forex_category_ids',
            'forex_symbol_ids',
            'forex_news_category_checked_items',
            'forex_news_symbol_checked_items',
        )

    def get_forex_category_ids(self, obj):
        return list(obj.forex_categories.values_list('id', flat=True))

    def get_forex_symbol_ids(self, obj):
        return list(obj.forex_symbols.values_list('id', flat=True))


class ForexNewsFilterUpdateSerializer(ModelSerializer):
    """
    For partial update:
    """
    forex_category_ids = ListField(child=IntegerField(), required=False)
    forex_symbol_ids = ListField(child=IntegerField(), required=False)

    class Meta:
        model = ForexNewsFilter
        fields = (
            'is_sound',
            'is_all_categories',
            'is_all_symbols',
            'forex_category_ids',
            'forex_symbol_ids',
        )

    def update(self, instance, validated_data):
        d = validated_data

        if 'is_sound' in d:
            instance.is_sound = d['is_sound']
            instance.save()

        if 'is_all_categories' in d:
            instance.is_all_categories = d['is_all_categories']
            instance.save()
            ids = d.pop('forex_category_ids')
            instance.forex_categories.set(
                ForexCategory.objects.filter(id__in=ids))

        if 'is_all_symbols' in d:
            instance.is_all_symbols = d['is_all_symbols']
            instance.save()
            ids = d.pop('forex_symbol_ids')
            instance.forex_symbols.set(ForexSymbol.objects.filter(id__in=ids))
        return instance


class IndicatorSerializer(ModelSerializer):
    class Meta:
        model = Indicator
        fields = (
            'id',
            'name',
            'description'
        )


class NotificationSerializer(ModelSerializer):
    user_id = ModelField(model_field=Notification._meta.get_field('user'))
    created_at = SerializerMethodField()

    class Meta:
        model = Notification
        fields = (
            'id',
            'user_id',
            'code',
            'type',
            'text',
            'created_at',
            'is_read',
            'contract'
        )

    def get_created_at(self, obj):
        return obj.dt


class TimeFrameSerializer(ModelSerializer):
    class Meta:
        model = TimeFrame
        fields = (
            'id',
            'name',
            'description'
        )


class StrategySerializer(ModelSerializer):
    indicators = IndicatorSerializer(read_only=True, many=True)
    time_frames = TimeFrameSerializer(read_only=True, many=True)

    class Meta:
        model = Strategy
        fields = (
            'id',
            'name',
            'short_description',
            'pic_url',
            'time_frames',
            'indicators',
            'time_frames_name'
        )


class StrategyExtSerializer(StrategySerializer):
    class Meta(StrategySerializer.Meta):
        model = Strategy
        fields = StrategySerializer.Meta.fields + ('content',)


class SubscriptionFileSerializer(ModelSerializer):
    subscription_id = ModelField(
        model_field=SubscriptionFile._meta.get_field('subscription'))

    class Meta:
        model = SubscriptionFile
        fields = (
            'id',
            'subscription_id',
            'name',
            'type',
            'download_link'
        )


class SubscriptionVideoBeginSerializer(ModelSerializer):
    subscription_id = ModelField(
        model_field=SubscriptionVideoBegin._meta.get_field('subscription'))

    class Meta:
        model = SubscriptionVideoBegin
        fields = (
            'id',
            'subscription_id',
            'name',
            'link',
            'file_url',
        )


class SubscriptionVideoSerializer(ModelSerializer):
    subscription_id = ModelField(
        model_field=SubscriptionVideo._meta.get_field('subscription'))

    class Meta:
        model = SubscriptionVideo
        fields = (
            'id',
            'subscription_id',
            'name',
            'link',
            'file_url',
        )


class SubscriptionNotPaidSerializer(ModelSerializer):
    is_paid = BooleanField(default=False)
    begin_videos = SubscriptionVideoBeginSerializer(many=True)
    files = SubscriptionFileSerializer(many=True)

    class Meta:
        model = Subscription
        fields = (
            'id',
            'name',
            'update_date',
            'block_css_class',
            'block_title',
            'block_content',
            'col1_ico_url',
            'col2_ico_url',
            'col3_ico_url',
            'col1_title',
            'col2_title',
            'col3_title',
            'tools_button',
            'tools_button_link',
            'price1',
            'price3',
            'price6',
            'col1_html',
            'col2_html',
            'col3_html',
            'begin_videos',
            'is_paid',
            'files',
        )


class SubscriptionPaidSerializer(SubscriptionNotPaidSerializer):
    """
    Difference between Paid and NotPaid serializer.
    Paid serializer has is_paid=True and additional fields:
    files, paid_videos, date_from, date_to.
    """
    is_paid = BooleanField(default=True)
    files = SubscriptionFileSerializer(many=True)
    paid_videos = SubscriptionVideoSerializer(many=True)
    date_from = SerializerMethodField()
    date_to = SerializerMethodField()

    class Meta(SubscriptionNotPaidSerializer.Meta):
        model = Subscription
        fields = SubscriptionNotPaidSerializer.Meta.fields\
            + (
                'files',
                'paid_videos',
                'date_from',
                'date_to'
            )

    def get_date_from(self, obj):
        su = self.context.get('su')
        return su.date_from.strftime('%Y-%m-%d')

    def get_date_to(self, obj):
        su = self.context.get('su')
        return su.date_to.strftime('%Y-%m-%d')


class TutorialBookSerializer(ModelSerializer):
    class Meta:
        model = TutorialBook
        fields = (
            'id',
            'pdf_file_url',
            'zip_file_url',
        )


class UserSettingsSerializer(ModelSerializer):
    class Meta:
        model = UserSettings
        fields = (
            'id',
            'featured_video_id',
        )


class UserSettingsUpdateSerializer(ModelSerializer):
    class Meta:
        model = UserSettings
        fields = (
            'featured_video_id',
        )


class VideoTagSerializer(ModelSerializer):
    class Meta:
        model = VideoTag
        fields = (
            'id',
            'name',
        )


class VideoSerializer(ModelSerializer):
    tag_ids = SerializerMethodField()

    class Meta:
        model = Video
        fields = (
            'id',
            'name',
            'file_url',
            'link',
            'tag_ids',
            'tags_name'
        )

    def get_tag_ids(self, obj):
        return list(obj.tags.values_list('id', flat=True))


class VideoLessonSerializer(ModelSerializer):
    class Meta:
        model = VideoLesson
        fields = (
            'id',
            'name',
            'link',
            'file_url',
        )


class FreePeriodSerializer(ModelSerializer):
    is_active = SerializerMethodField()
    class Meta:
        model = FreePeriod
        fields = (
            'id',
            'user',
            'duration',
            'created_at',
            'updated_at',
            'is_active',
            'date_to',
            'used'
        )

    def get_is_active(self, element):
        return element.is_active
