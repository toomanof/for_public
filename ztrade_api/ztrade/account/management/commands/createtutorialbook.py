from django.core.management.base import BaseCommand


from account.models import TutorialBook


class Command(BaseCommand):
    def create_tutorial_book(self):
        tb = TutorialBook.objects.filter(id=1).first()
        if tb is None:
            tb = TutorialBook.objects.create()

        tb.pdf_file.name = 'tutorial_book/file.pdf'
        tb.zip_file.name = 'tutorial_book/file.pdf.zip'
        tb.save()

    def handle(self, *args, **options):
        self.create_tutorial_book()
