import datetime
import os
import pandas
from django.core.management.base import BaseCommand
from django.conf import settings

from account.models import Subscription
from account.models import SubscriptionUser
from account.models import SubscriptionFile
from web.models import DownloadFile
from web.models import User
from content.management.commands.helpers import get_file_content


class Command(BaseCommand):
    BOOL_FIELDS = []
    EXCEL_PATH = os.path.join(settings.BASE_DIR,
                              'ztrade/content/management/data/content.xlsx')

    def get_data(self):
        path = self.EXCEL_PATH
        df = pandas.read_excel(open(path, 'rb'), sheet_name='Subscriptions')

        l = []
        for index, row in df.iterrows():
            rd = row.to_dict()

            for k, v in rd.items():
                if k in self.BOOL_FIELDS:
                    v = self.process_bool(v)

                if str(v).endswith('.txt'):
                    v = get_file_content(v)

                v = self.process_value(v)
                rd[k] = v

            l.append(rd)
        return l

    def get_data_dfiles(self):
        path = self.EXCEL_PATH
        df = pandas.read_excel(open(path, 'rb'), sheet_name='DownloadFiles')

        l = []
        for index, row in df.iterrows():
            rd = row.to_dict()

            for k, v in rd.items():
                if k in self.BOOL_FIELDS:
                    v = self.process_bool(v)

                if str(v).endswith('.txt'):
                    v = get_file_content(v)

                v = self.process_value(v)
                rd[k] = v

            l.append(rd)
        return l

    def process_bool(self, value):
        if pandas.isnull(value):
            return False

        if value.strip().lower() == 'y':
            return True

        return False

    def process_value(self, value):
        if pandas.isnull(value):
            return None
        return value

    def create_subscription_files(self):
        """
        Create Subscription Files
        Create Download Files that used for SubscriptionFile
        """
        DownloadFile.objects.all().delete()
        SubscriptionFile.objects.all().delete()

        l = list(Subscription.objects.all())

        subscription1 = l[0]
        subscription2 = l[1]

        sort = 0
        for d in self.get_data_dfiles():
            sort += 10
            file_name = d.pop('file')
            df = DownloadFile.objects.create(**d)
            df.file.name = file_name
            df.save()

            if 'A' in d['name']:
                subscription = subscription1
            else:
                subscription = subscription2

            if 'Индикатор' in d['name']:
                sf_type = SubscriptionFile.INDICATOR
            else:
                sf_type = SubscriptionFile.TEMPLATE

            SubscriptionFile.objects.create(
                subscription=subscription,
                download_file=df,
                name=d['name'],
                type=sf_type,
                sort=sort

            )

    def create_subscriptions(self):
        Subscription.objects.all().delete()

        i = 1
        for d in self.get_data():
            d['update_date'] = datetime.date.today()
            s = Subscription.objects.create(**d)

            pris = 'images/subscriptions/s{}-'.format(i)
            s.col1_ico.name = pris + 'col1.svg'
            s.col2_ico.name = pris + 'col2.svg'
            s.col3_ico.name = pris + 'col3.svg'
            s.save()

            i += 1

    def subscribe_superadmin_for_1month(self):
        """
        Subscribe superadmin to all subscriptions
        """
        date_from = datetime.date.today()
        date_to = date_from + datetime.timedelta(days=30)

        for subscription in Subscription.objects.all():
            SubscriptionUser.objects.create(
                user=User.objects.filter(is_superuser=True).first(),
                subscription=subscription,
                date_from=date_from,
                date_to=date_to,
                is_paid=True
            )

    def handle(self, *args, **options):
        self.create_subscriptions()
        self.create_subscription_files()
        self.subscribe_superadmin_for_1month()
