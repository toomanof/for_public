import datetime
import random
from django.utils import timezone
from django.core.management.base import BaseCommand

from account.models import ForexSymbol
from account.models import ForexCategory
from account.models import ForexNews


class Command(BaseCommand):
    TITLE = (
        'Отчет <num> Baker Hughes по активным нефтяным '
        'платформам в США (США): падение с 737 до 729')

    @property
    def categories(self):
        return (
            'Технический анализ',
            'Комментарии банков',
            'Центральный банк',
            'Индикаторы',
            'Акции, нефть и металлы',
            'Макроэкономика',
        )

    @property
    def symbols(self):
        return (
            'EUR/USD',
            'GBP/USD',
            'USD/JPY',
            'EUR/GBP',
            'USD/CHF',
            'AUD/USD',
            'NZD/USD',
        )

    def create_symbols(self):
        ForexSymbol.objects.all().delete()
        sort = 0
        for sym in self.symbols:
            sort += 10
            ForexSymbol.objects.create(
                code=sym.replace('/', '').lower(),
                name=sym,
                sort=sort)

    def create_categories(self):
        ForexCategory.objects.all().delete()
        sort = 0
        for name in self.categories:
            sort += 10
            ForexCategory.objects.create(
                name=name,
                sort=sort)

    def create_news(self):
        ForexNews.objects.all().delete()

        qs1 = ForexSymbol.objects.all()
        qs2 = ForexCategory.objects.all()

        dt = timezone.now()

        for i in range(1, 100):
            num = str(i).zfill(2)
            title = self.TITLE.replace('<num>', num)

            dt = dt - datetime.timedelta(
                minutes=random.randint(15, 30))

            fn = ForexNews.objects.create(
                dt=dt,
                title=title,
                supplier='FXStreet'
            )
            fn.forex_symbols.set(qs1)
            fn.forex_categories.set(qs2)

    def handle(self, *args, **options):
        self.create_symbols()
        self.create_categories()
        self.create_news()
