import os
import pandas
from django.core.management.base import BaseCommand
from django.conf import settings

from account.models import Subscription
from account.models import SubscriptionVideoBegin
from content.management.commands.helpers import get_file_content


class Command(BaseCommand):
    BOOL_FIELDS = []
    EXCEL_PATH = os.path.join(settings.BASE_DIR,
                              'ztrade/content/management/data/content.xlsx')

    def get_data(self):
        path = self.EXCEL_PATH
        df = pandas.read_excel(
            open(path, 'rb'), sheet_name='SubscriptionVideosBegin')

        l = []
        for index, row in df.iterrows():
            rd = row.to_dict()

            for k, v in rd.items():
                if k in self.BOOL_FIELDS:
                    v = self.process_bool(v)

                if str(v).endswith('.txt'):
                    v = get_file_content(v)

                v = self.process_value(v)
                rd[k] = v

            l.append(rd)
        return l

    def process_bool(self, value):
        if pandas.isnull(value):
            return False

        if value.strip().lower() == 'y':
            return True

        return False

    def process_value(self, value):
        if pandas.isnull(value):
            return None
        return value

    def create_videos(self):
        SubscriptionVideoBegin.objects.all().delete()

        for d in self.get_data():
            file_name = d.pop('file')
            d['subscription'] = Subscription.objects.get(
                code=d.pop('subscription_code'))

            svb = SubscriptionVideoBegin.objects.create(**d)
            svb.file.name = file_name
            svb.save()

    def handle(self, *args, **options):
        self.create_videos()
