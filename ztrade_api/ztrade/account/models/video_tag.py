from django.db.models import Manager
from django.db.models import Model
from django.contrib import admin
from django.db.models import CharField
from django.db.models import IntegerField


class VideoTagManager(Manager):
    pass


class VideoTag(Model):
    name = CharField('название', max_length=100)
    sort = IntegerField('сортировка', default=0)

    objects = VideoTagManager()

    class Meta:
        app_label = 'account'
        verbose_name = 'видео тег'
        verbose_name_plural = 'видео теги'
        ordering = ['sort']

    def __str__(self):
        return self.name


@admin.register(VideoTag)
class VideoTagAdmin(admin.ModelAdmin):
    list_display = ('name', 'sort')
