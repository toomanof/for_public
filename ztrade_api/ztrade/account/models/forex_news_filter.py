from django.db.models import Manager
from django.db.models import Model
from django.contrib import admin
from django.apps import apps
from django.db.models import OneToOneField
from django.db.models import ManyToManyField
from django.db.models import BooleanField
from django.db.models import CASCADE


class ForexNewsFilterManager(Manager):
    pass


class ForexNewsFilter(Model):
    user = OneToOneField('web.User', on_delete=CASCADE,
                         verbose_name='пользователь',
                         related_name='forex_news_user_filter')
    forex_categories = ManyToManyField('ForexCategory',
                                       verbose_name='категории')
    forex_symbols = ManyToManyField('ForexSymbol', verbose_name='пары')
    is_sound = BooleanField(default=False)
    is_all_categories = BooleanField(default=True)
    is_all_symbols = BooleanField(default=True)

    objects = ForexNewsFilterManager()

    class Meta:
        app_label = 'account'
        verbose_name = 'forex новости user'
        verbose_name_plural = 'forex новости users'
        ordering = ['id']

    def __str__(self):
        return str(self.id)

    @property
    def forex_news_category_checked_items(self):
        """
        Prepare for frontend ready data to work with
        "CheckedItems" component
        """
        ForexCategory = apps.get_model('account', 'ForexCategory')
        qs = ForexCategory.objects.all()

        if self.is_all_categories:
            l = []
            l.append({'id': None, 'name': 'Все', 'isChecked': True})
            for obj in qs:
                l.append({'id': obj.id, 'name': obj.name, 'isChecked': True})
            return l

        l = []
        l.append({'id': None, 'name': 'Все', 'isChecked': False})

        ids = self.forex_categories.values_list('id', flat=True)

        for obj in qs:
            if obj.id in ids:
                is_checked = True
            else:
                is_checked = False

            l.append(
                {'id': obj.id, 'name': obj.name, 'isChecked': is_checked})
        return l

    @property
    def forex_news_symbol_checked_items(self):
        """
        Prepare for frontend ready data to work with
        "CheckedItems" component
        """
        ForexSymbol = apps.get_model('account', 'ForexSymbol')
        qs = ForexSymbol.objects.all()

        if self.is_all_symbols:
            l = []
            l.append({'id': None, 'name': 'Все', 'isChecked': True})
            for obj in qs:
                l.append({'id': obj.id, 'name': obj.name, 'isChecked': True})
            return l

        l = []
        l.append({'id': None, 'name': 'Все', 'isChecked': False})

        ids = self.forex_symbols.values_list('id', flat=True)

        for obj in qs:
            if obj.id in ids:
                is_checked = True
            else:
                is_checked = False

            l.append(
                {'id': obj.id, 'name': obj.name, 'isChecked': is_checked})
        return l


@admin.register(ForexNewsFilter)
class ForexNewsFilterAdmin(admin.ModelAdmin):
    list_display = (
        'user',
        'is_sound',
        'is_all_categories',
        'is_all_symbols'
    )
