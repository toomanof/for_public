from django.db.models import Manager
from django.db.models import Model
from django.contrib import admin
from django.conf import settings
from django.db.models import FileField


class TutorialBookManager(Manager):
    pass


class TutorialBook(Model):
    pdf_file = FileField('pdf файл', upload_to='tutorial_book', null=True,
                         default=None, blank=True)
    zip_file = FileField('zip файл', upload_to='tutorial_book', null=True,
                         default=None, blank=True)

    objects = TutorialBookManager()

    class Meta:
        app_label = 'account'
        verbose_name = 'учебник'
        verbose_name_plural = 'учебник'
        ordering = ['id']

    def __str__(self):
        return str(self.id)

    @property
    def pdf_file_url(self):
        if not self.pdf_file or not self.pdf_file.name:
            return None
        return '{}/{}'.format(settings.BUCKET_URL, self.pdf_file.name)

    @property
    def zip_file_url(self):
        if not self.zip_file or not self.zip_file.name:
            return None
        return '{}/{}'.format(settings.BUCKET_URL, self.zip_file.name)


@admin.register(TutorialBook)
class TutorialBookAdmin(admin.ModelAdmin):
    actions = None
    list_display = (
        'id',
        'pdf_file',
        'zip_file',
    )

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False
