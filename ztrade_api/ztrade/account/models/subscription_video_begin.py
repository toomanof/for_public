from django.db.models import Manager
from django.contrib import admin
from django.conf import settings
from django.db.models import ForeignKey
from django.db.models import CASCADE
from .video_base import VideoBase


class SubscriptionVideoBeginManager(Manager):
    pass


class SubscriptionVideoBegin(VideoBase):
    """
    Free videos.
    Video can be uploaded as youtube link or mp4 file.
    """
    subscription = ForeignKey('Subscription', on_delete=CASCADE,
                              related_name='begin_videos',
                              verbose_name='подписка')

    objects = SubscriptionVideoBeginManager()

    class Meta:
        app_label = 'account'
        verbose_name = 'подписка: ознакомительное видео'
        verbose_name_plural = 'подписка: ознакомительные видео'
        ordering = ['sort']

    def __str__(self):
        return self.name

    @property
    def file_url(self):
        if not self.file or not self.file.name:
            return None
        return '{}/{}'.format(settings.BUCKET_URL, self.file.name)


@admin.register(SubscriptionVideoBegin)
class SubscriptionVideoBeginAdmin(admin.ModelAdmin):
    list_filter = ('subscription',)
    list_display = (
        'name',
        'subscription',
        'file',
        'link',
        'sort'
    )
