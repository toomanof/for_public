from django.db.models import Manager
from django.db.models import Model
from django.contrib import admin
from django.conf import settings
from django.db.models import BooleanField
from django.db.models import CharField
from django.db.models import DateField
from django.db.models import IntegerField
from django.db.models import TextField
from django.db.models import ImageField
from django.db.models import DecimalField


class SubscriptionManager(Manager):
    pass


class Subscription(Model):
    code = CharField('код', max_length=20, null=True, default=None, blank=True,
                     help_text='для внутреннего трекинга')
    name = CharField('название', max_length=100)
    update_date = DateField('дата обновления')
    block_css_class = CharField('css класс', max_length=50,
                                null=True, default=None, blank=True)
    block_title = CharField('заголовок блока', max_length=100)
    block_content = TextField('контент блока')
    col1_ico = ImageField(upload_to='images/subscriptions',
                          null=True, default=None, blank=True)
    col2_ico = ImageField(upload_to='images/subscriptions',
                          null=True, default=None, blank=True)
    col3_ico = ImageField(upload_to='images/subscriptions',
                          null=True, default=None, blank=True)
    col1_title = CharField('заголовок 1-й колонки', max_length=100)
    col2_title = CharField('заголовок 2-й колонки', max_length=100)
    col3_title = CharField('заголовок 3-й колонки', max_length=100)
    tools_button = CharField('кнопка инструментов', max_length=100)
    tools_button_link = CharField('ссылка кнопки инструментов',
                                  max_length=255)
    col1_html = TextField('html 1-й колонки')
    col2_html = TextField('html 2-й колонки')
    col3_html = TextField('html 3-й колонки')
    price1 = DecimalField('цена 1 мес', max_digits=10, decimal_places=2)
    price3 = DecimalField('цена 3 мес', max_digits=10, decimal_places=2)
    price6 = DecimalField('цена 6 мес', max_digits=10, decimal_places=2)
    sort = IntegerField('сортировка', default=0)
    is_visible = BooleanField('отображать на сайте', default=True)

    objects = SubscriptionManager()

    class Meta:
        app_label = 'account'
        verbose_name = 'подписка'
        verbose_name_plural = 'подписки'
        ordering = ['sort']

    def __str__(self):
        return self.name

    @property
    def col1_ico_url(self):
        if not self.col1_ico or not self.col1_ico.name:
            return None
        return '{}/{}'.format(settings.BUCKET_URL, self.col1_ico.name)

    @property
    def col2_ico_url(self):
        if not self.col2_ico or not self.col2_ico.name:
            return None
        return '{}/{}'.format(settings.BUCKET_URL, self.col2_ico.name)

    @property
    def col3_ico_url(self):
        if not self.col3_ico or not self.col3_ico.name:
            return None
        return '{}/{}'.format(settings.BUCKET_URL, self.col3_ico.name)

    @property
    def frontend_link_videos(self):
        """
        Returns frontend's link to subscription videos
        """
        return '{}/account/video?subscription_id={}'.format(
            settings.FRONTEND_URL, self.id)

    @property
    def all_files_html(self):
        """
        HTML for email
        """
        s = ''
        for obj in self.files.all():
            s += '<a href="{}" style="color: #488dc6; text-decoration: none; display: block; padding: 0 24px;">{} ({})</a><br />'.format(
                obj.download_link, obj.name, obj.get_type_display())
        return s


@admin.register(Subscription)
class SubscriptionAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'update_date',
        'block_title',
        'col1_ico',
        'col2_ico',
        'col3_ico',
        'col1_title',
        'col2_title',
        'col3_title',
        'tools_button',
        'tools_button_link',
        'price1',
        'price3',
        'price6',
        'sort',
        'is_visible',
    )
    exclude = ('code',)
