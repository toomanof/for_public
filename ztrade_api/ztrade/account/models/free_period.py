from datetime import timedelta
from django.utils import timezone
from django.contrib import admin
from django.db import models


class FreePeriod(models.Model):
    user = models.OneToOneField('web.User', on_delete=models.CASCADE)
    duration = models.DurationField('Период')
    date_to = models.DateTimeField('Дата до', default=timezone.now)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    used = models.BooleanField('Флаг использования', default=False)

    class Meta:
        app_label = 'account'
        verbose_name = 'Бесплатный период пользователя'
        verbose_name_plural = 'Бесплатные периоды пользователей'

    def save(self, *args, **kwargs):
        if not self.id:
            self.duration = timedelta(days=7)
            self.date_to = timezone.now() + self.duration
        super().save(*args, **kwargs)

    @property
    def is_active(self):
        return timezone.now() <= self.date_to and not self.used



@admin.register(FreePeriod)
class FreePeriodAdmin(admin.ModelAdmin):
    list_display = ('user', 'duration', 'created_at', 'updated_at')
    readonly_fields = ('created_at', 'updated_at')
