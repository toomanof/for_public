from django.db.models import Manager
from django.contrib import admin
from django.conf import settings
from django.db.models import ManyToManyField
from .video_base import VideoBase


class VideoManager(Manager):
    pass


class Video(VideoBase):
    """
    Video can be uploaded as youtube link or mp4 file.
    """
    tags = ManyToManyField('VideoTag', verbose_name='теги',
                           related_name='videos')

    objects = VideoManager()

    class Meta:
        app_label = 'account'
        verbose_name = 'видео'
        verbose_name_plural = 'видео'
        ordering = ['sort']

    def __str__(self):
        return self.name

    @property
    def tags_name(self):
        if self.tags.count() == 0:
            return '-'
        return ', '.join(self.tags.values_list('name', flat=True))

    @property
    def file_url(self):
        if not self.file or not self.file.name:
            return None
        return '{}/{}'.format(settings.BUCKET_URL, self.file.name)


@admin.register(Video)
class VideoAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'file',
        'link',
        'sort'
    )
