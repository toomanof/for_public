from django.db.models import Manager
from django.db.models import Model
from django.contrib import admin
from django.db.models import CharField
from django.db.models import IntegerField


class ForexCategoryManager(Manager):
    pass


class ForexCategory(Model):
    name = CharField('название', max_length=255)
    sort = IntegerField('сортировка', default=0)

    objects = ForexCategoryManager()

    class Meta:
        app_label = 'account'
        verbose_name = 'forex категория'
        verbose_name_plural = 'forex категории'
        ordering = ['sort']

    def __str__(self):
        return self.name


@admin.register(ForexCategory)
class ForexCategoryAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'sort'
    )
