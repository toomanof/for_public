from django.db.models import Manager
from django.db.models import Model
from django.contrib import admin
from django.db.models import IntegerField
from django.db.models import OneToOneField
from django.db.models import CASCADE


class UserSettingsManager(Manager):
    pass


class UserSettings(Model):
    user = OneToOneField('web.User', on_delete=CASCADE)
    featured_video_id = IntegerField('избранное видео', null=True,
                                     default=None, blank=True)

    objects = UserSettingsManager()

    class Meta:
        app_label = 'account'
        verbose_name = 'настройки пользователя'
        verbose_name_plural = 'настройки пользователей'
        ordering = ['id']

    def __str__(self):
        return 'UserSettings: {} {}'.format(self.user.first_name,
                                            self.user.last_name)


@admin.register(UserSettings)
class UserSettingsAdmin(admin.ModelAdmin):
    actions = None
    list_display = (
        'user',
        'featured_video_id',
    )

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False
