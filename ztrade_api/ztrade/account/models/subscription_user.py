import datetime
from django.core.exceptions import ValidationError
from dateutil import relativedelta
from django.db.models import Manager
from django.db.models import Model
from django.contrib import admin
from django.apps import apps
from django.db.models import ForeignKey
from django.db.models import DateField
from django.db.models import BooleanField
from django.db.models import CASCADE
from django.db.models import SET_NULL


class SubscriptionUserManager(Manager):
    def create_bill(self, user_id, subscription_id, num_months):
        """
        num_months can be one of 1 / 3 / 6
        accordingly to fields: price1 / price3 / price6
        """
        User = apps.get_model('web', 'User')
        Subscription = apps.get_model('account', 'Subscription')
        Bill = apps.get_model('payment', 'Bill')

        if num_months not in (1, 3, 6):
            raise ValidationError('Неверное кол-во месяцев')

        user = User.objects.get(id=user_id)
        subscription = Subscription.objects.get(id=subscription_id)

        date_from = datetime.date.today()
        date_to = date_from + relativedelta.relativedelta(
            months=num_months)

        su = self.model.objects.create(
            user=user,
            subscription=subscription,
            date_from=date_from,
            date_to=date_to)

        name = 'Подписка на инструменты. {} c {} до {}'.format(
            subscription.name,
            date_from.strftime('%d.%m.%y'),
            date_to.strftime('%d.%m.%y'))

        amount = getattr(subscription, 'price{}'.format(num_months))

        bill = Bill.objects.create(
            user=user, subscription_user=su, name=name, amount=amount)
        return bill

    def get_su(self, user_id, subscription_id):
        """
        Returns SU object, if user currently subscribed
        to particular subscription.
        Otherwise returns None.
        """
        today = datetime.date.today()
        qs = self.model.objects.filter(
            subscription_id=subscription_id,
            user_id=user_id,
            is_paid=True,
            date_from__lte=today,
            date_to__gte=today)
        return qs.first()


class SubscriptionUser(Model):
    user = ForeignKey('web.User', on_delete=CASCADE)
    subscription = ForeignKey('Subscription', on_delete=SET_NULL,
                              null=True, default=None, blank=True,
                              verbose_name='подписка')
    date_from = DateField('дата с')
    date_to = DateField('дата до')
    is_paid = BooleanField('оплачен', default=False,
                           help_text='Индексируется автоматически')

    objects = SubscriptionUserManager()

    class Meta:
        app_label = 'account'
        verbose_name = 'подписка: пользователя'
        verbose_name_plural = 'подписка: пользователи'
        ordering = ['-date_to']

    def __str__(self):
        return 'Подписка пользователя: {}'.format(self.user.email)


@admin.register(SubscriptionUser)
class SubscriptionUserAdmin(admin.ModelAdmin):
    list_filter = ('subscription',)
    list_display = (
        'date_from',
        'date_to',
        'user',
        'subscription',
        'is_paid',
    )

#    readonly_fields = (
#        'is_paid',
#        'user',
#        'subscription',
#    )

    def has_delete_permission(self, request, obj=None):
        return False
