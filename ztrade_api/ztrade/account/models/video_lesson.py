from django.db.models import Manager
from django.contrib import admin
from django.conf import settings
from .video_base import VideoBase


class VideoLessonManager(Manager):
    pass


class VideoLesson(VideoBase):
    """
    Video can be uploaded as youtube link or mp4 file.
    """
    objects = VideoLessonManager()

    class Meta:
        app_label = 'account'
        verbose_name = 'видео урок'
        verbose_name_plural = 'видео уроки'
        ordering = ['sort']

    def __str__(self):
        return self.name

    @property
    def file_url(self):
        if not self.file or not self.file.name:
            return None
        return '{}/{}'.format(settings.BUCKET_URL, self.file.name)


@admin.register(VideoLesson)
class VideoLessonAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'file',
        'link',
        'sort'
    )
