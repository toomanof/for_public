from django.db.models import Manager
from django.db.models import Model
from django.contrib import admin
from django.db.models import CharField
from django.db.models import IntegerField


class TimeFrameManager(Manager):
    pass


class TimeFrame(Model):
    name = CharField('название', max_length=50)
    description = CharField('описание', max_length=255,
                            null=True, default=None, blank=True)
    sort = IntegerField('сортировка', default=0)

    objects = TimeFrameManager()

    class Meta:
        app_label = 'account'
        verbose_name = 'тайм-фрейм'
        verbose_name_plural = 'тайм-фреймы'
        ordering = ['sort']

    def __str__(self):
        return self.name


@admin.register(TimeFrame)
class TimeFrameAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'description',
        'sort'
    )
