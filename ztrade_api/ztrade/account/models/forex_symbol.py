from django.db.models import Manager
from django.db.models import Model
from django.contrib import admin
from django.db.models import CharField
from django.db.models import IntegerField


class ForexSymbolManager(Manager):
    pass


class ForexSymbol(Model):
    code = CharField('код', max_length=10)
    name = CharField('название', max_length=10)
    sort = IntegerField('сортировка', default=0)

    objects = ForexSymbolManager()

    class Meta:
        app_label = 'account'
        verbose_name = 'forex пара'
        verbose_name_plural = 'forex пары'
        ordering = ['sort']

    def __str__(self):
        return self.name


@admin.register(ForexSymbol)
class ForexSymbolAdmin(admin.ModelAdmin):
    list_display = (
        'code',
        'name',
        'sort'
    )
