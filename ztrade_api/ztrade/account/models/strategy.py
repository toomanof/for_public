from django.db.models import Manager
from django.db.models import Model
from django.contrib import admin
from django.conf import settings
from django.db.models import CharField
from django.db.models import IntegerField
from django.db.models import TextField
from django.db.models import ImageField
from django.db.models import ManyToManyField
from ckeditor_uploader.fields import RichTextUploadingField


class StrategyManager(Manager):
    pass


class Strategy(Model):
    name = CharField('название', max_length=255)
    short_description = TextField('краткое описание', null=True,
                                  default=None, blank=True)
    pic = ImageField(upload_to='images/strategies',
                     null=True, default=None, blank=True)
    content = RichTextUploadingField('контент', null=True,
                                     default=None, blank=True)
    sort = IntegerField('сортировка', default=0)
    time_frames = ManyToManyField('TimeFrame', verbose_name='тайм-фреймы',
                                  related_name='tf_strategies')
    indicators = ManyToManyField('Indicator', verbose_name='индикаторы',
                                 related_name='indicator_strategies')

    objects = StrategyManager()

    class Meta:
        app_label = 'account'
        verbose_name = 'стратегия'
        verbose_name_plural = 'стратегии'
        ordering = ['sort']

    def __str__(self):
        return self.name

    @property
    def time_frames_name(self):
        """
        Comma separated name
        """
        if self.time_frames.count() == 0:
            return '-'
        return ', '.join(self.time_frames.values_list('name', flat=True))

    @property
    def pic_url(self):
        if not self.pic or not self.pic.name:
            return None
        return '{}/{}'.format(settings.BUCKET_URL, self.pic.name)


@admin.register(Strategy)
class StrategyAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'pic',
        'sort',
    )
