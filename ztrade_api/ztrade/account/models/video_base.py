from django.db.models import Model
from django.db.models import CharField
from django.db.models import FileField
from django.db.models import IntegerField


class VideoBase(Model):
    name = CharField('название', max_length=255)
    file = FileField('файл', upload_to='videos', null=True,
                     default=None, blank=True)
    link = CharField('youtube link', max_length=255,
                     null=True, default=None, blank=True)
    sort = IntegerField('сортировка', default=0)

    class Meta:
        app_label = 'account'
        abstract = True
