from django.db.models import Manager
from django.db.models import Model
from django.contrib import admin
from django.conf import settings
from django.utils import timezone
from django.db.models import DateTimeField
from django.db.models import CharField
from django.db.models import ManyToManyField

from web.tasks import send_ws_message
from web.constants import CHANNEL_ACCOUNT


class ForexNewsManager(Manager):
    pass


class ForexNews(Model):
    title = CharField('заголовок', max_length=1000)
    dt = DateTimeField('дата')
    supplier = CharField('поставщик', max_length=100,
                         null=True, default=None, blank=True)
    forex_symbols = ManyToManyField('ForexSymbol', verbose_name='пары')
    forex_categories = ManyToManyField('ForexCategory',
                                       verbose_name='категории')

    objects = ForexNewsManager()

    class Meta:
        app_label = 'account'
        verbose_name = 'forex новость'
        verbose_name_plural = 'forex новости'
        ordering = ['-dt']

    def __str__(self):
        return self.title


@admin.register(ForexNews)
class ForexNewsAdmin(admin.ModelAdmin):
    list_display = (
        'dt',
        'title',
        'supplier'
    )

    def add_view(self, request, extra_context=None):
        # To have created_at in admin in UTC zone.
        timezone.deactivate()
        return super().add_view(request, extra_context=extra_context)

    def save_model(self, request, obj, form, change):
        """
        When new news added, send message to centrifugo.
        """
        obj.save()

        if change is False:
            msg = {
                'channel': CHANNEL_ACCOUNT,
                'news_id': obj.id,
                'type': settings.NEWS_CREATED
            }
            send_ws_message.delay(msg)
