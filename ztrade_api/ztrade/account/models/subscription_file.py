from django.db.models import Manager
from django.db.models import Model
from django.contrib import admin
from django.db.models import CharField
from django.db.models import ForeignKey
from django.db.models import IntegerField
from django.db.models import CASCADE
from django.db.models import SET_NULL


class SubscriptionFileManager(Manager):
    pass


class SubscriptionFile(Model):
    INDICATOR = 'indicator'
    TEMPLATE = 'template'

    TYPE_CHOICES = (
        (INDICATOR, 'индикатор'),
        (TEMPLATE, 'шаблон'),
    )

    subscription = ForeignKey('Subscription', on_delete=CASCADE,
                              verbose_name='подписка', related_name='files')
    download_file = ForeignKey('web.DownloadFile', on_delete=SET_NULL,
                               null=True, default=None, blank=True,
                               verbose_name='файл загрузки')
    name = CharField('название', max_length=100)
    type = CharField('тип', max_length=20, choices=TYPE_CHOICES)
    sort = IntegerField('сортировка', default=0)

    objects = SubscriptionFileManager()

    class Meta:
        app_label = 'account'
        verbose_name = 'подписка: файл'
        verbose_name_plural = 'подписка: файлы'
        ordering = ['sort']

    def __str__(self):
        return self.name

    @property
    def download_link(self):
        if self.download_file and self.download_file.file_url:
            return self.download_file.file_url
        return None


@admin.register(SubscriptionFile)
class SubscriptionFileAdmin(admin.ModelAdmin):
    list_filter = ('type', 'subscription')
    list_display = (
        'name',
        'subscription',
        'download_file',
        'type',
        'sort',
    )
