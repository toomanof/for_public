import copy
from os.path import join
from django.db.models import Model
from django.db.models import Manager
from django.dispatch import receiver
from django.db.models.signals import post_save
from django.contrib import admin
from django.template.loader import get_template
from django.conf import settings
from django.db.models import ForeignKey
from django.db.models import CharField
from django.db.models import DateTimeField
from django.db.models import BooleanField
from django.db.models import CASCADE
from django.contrib.postgres.fields import JSONField


class NotificationManager(Manager):
    def get_template(self, code):
        file_path = join(
            settings.BASE_DIR,
            f'ztrade/ztrade/templates/account/notifications/{code}.html')
        return get_template(file_path)

    def process(self, code, type, user, market, **kwargs):
        """
        Get template by type and render with kwargs all notification types.
        Additional contexts (if needed):
        def <code>_context
        """
        html = self.get_template(code)
        d = copy.deepcopy(kwargs)

        # Add user object for whome notification to context
        d['user'] = user

        addtitional_context = getattr(self, f'{code}_context', None)
        if addtitional_context is not None:
            d.update(addtitional_context(**kwargs))

        # kwargs may contain "contract"
        contract = d['contract'] if 'contract' in d else None

        n = self.model.objects.create(
            user=user,
            code=code,
            type=type,
            market=market,
            contract=contract,
            text=html.render(d))
        return n


class Notification(Model):
    SIGNAL_CREATED = 'signal_created'
    SIGNAL_TP_UPDATED = 'signal_tp_updated'
    SIGNAL_SL_UPDATED = 'signal_sl_updated'
    SIGNAL_CLOSED = 'signal_closed'
    NEWS = 'news'

    CODE_CHOICES = (
        (SIGNAL_CREATED, SIGNAL_CREATED),
        (SIGNAL_TP_UPDATED, SIGNAL_TP_UPDATED),
        (SIGNAL_SL_UPDATED, SIGNAL_SL_UPDATED),
        (SIGNAL_CLOSED, SIGNAL_CLOSED),
        (NEWS, NEWS),
    )

    GLOBAL = 'global'
    SIGNALS = 'signals'

    TYPE_CHOICES = (
        (GLOBAL, GLOBAL),
        (SIGNALS, SIGNALS),
        (NEWS, NEWS),
    )

    user = ForeignKey('web.User', on_delete=CASCADE,
                      related_name='notifications')
    market = ForeignKey('sgn.Market', on_delete=CASCADE,
                        null=True, default=None, blank=True)
    code = CharField(max_length=100, null=True, default=None, blank=True,
                     choices=CODE_CHOICES)
    type = CharField(max_length=100, default=GLOBAL, choices=TYPE_CHOICES)
    text = CharField(max_length=1000, help_text='Text for notification')
    created_at = DateTimeField(auto_now_add=True)
    contract = JSONField(
        null=True, default=None, blank=True,
        help_text='key: value object with contract for particular code')
    is_read = BooleanField(default=False)

    objects = NotificationManager()

    class Meta:
        app_label = 'account'
        verbose_name = 'уведомление'
        verbose_name_plural = 'уведомления'
        ordering = ['-created_at']

    def __str__(self):
        return self.text[:10]

    @property
    def dt(self):
        """
        Market notifications ajust to timezone
        """
        if self.market_id is None:
            return self.created_at

        f = self.user.get_signals_filter(self.market_id)
        return f.convert_dt(self.created_at)

    def get_ws_message(self):
        """
        Message for WebSocket
        """
        return {
            'channel': 'notifications{}'.format(self.user_id),
            'type': settings.NOTIFICATION_CREATED,
            'notification_id': self.id,
            'market_id': self.market_id,
            'notification_type': self.type,
            'alert_text': self.text
        }


@receiver(post_save, sender=Notification)
def notification_created(sender, instance, created, **kwargs):
    if created:
        from web.tasks import send_ws_message
        send_ws_message.delay(instance.get_ws_message())


@admin.register(Notification)
class NotificationAdmin(admin.ModelAdmin):
    list_filter = ('user',)
    search_fields = ('user__email',)
    list_display = (
        'user',
        'market',
        'text',
        'code',
        'type',
        'created_at',
        'is_read',
    )
