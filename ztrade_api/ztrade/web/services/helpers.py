import boto3
import functools
import hashlib
import os
import random
import re
import string
import time
import uuid
from datetime import datetime, timedelta
from django.conf import settings
from django.db.models import Q


def get_dates_by_week(year, week):
    """
    Returns first and last date of the week
    """
    d = '{}-W{}'.format(year, week)
    start_date = datetime.strptime(d + '-1', '%G-W%V-%u')
    end_date = start_date + timedelta(days=6)
    return start_date, end_date


def get_minutes_string(minutes, seconds):
    """
    Returns string:
    Example: 4 мин. 10 сек.
    Example: 50 сек.
    """
    s = ''
    if minutes > 0:
        s += '{} мин. '.format(minutes)
    s += '{} сек.'.format(seconds)
    return s


def get_ids(value):
    """
    Returns list from comma separated integer string.
    Example: value=1,2,3
    Returns [1, 2, 3]
    If value is not comma separated integers, return None
    """
    ids = value.split(',')
    ids = [x.strip() for x in ids]
    try:
        ids = [int(x) for x in ids]
    except Exception:
        return None

    return ids


def get_s3_client():
    return boto3.client(
        's3',
        aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
        aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY,
        region_name=settings.AWS_REGION)


def is_queryset_has_date_overlap(qs, start, end):
    """
    Returns bool.
    Queryset has date overlap.
    Model should have "start_date" and "end_date" fields

    qs - queryset
    start - start_date
    end - end_date
    """
    qsc = qs.filter(
        Q(end_date__gte=start) | Q(end_date__isnull=True),
        start_date__lte=start)

    if qsc.exists():
        return True

    if end is not None:
        qsc = qs.filter(
            Q(end_date__gte=end) | Q(end_date__isnull=True),
            start_date__lte=end)

        if qsc.exists():
            return True

    return False


def is_queryset_has_dt_overlap(qs, start, end):
    """
    Returns bool.
    Queryset has datetime overlap.
    Model should have "started_at" and "ended_at" fields

    qs - queryset
    start - started_at
    end - ended_at
    """
    qsc = qs.filter(
        Q(ended_at__gte=start) | Q(ended_at__isnull=True),
        started_at__lte=start)

    if qsc.exists():
        return True

    if end is not None:
        qsc = qs.filter(
            Q(ended_at__gte=end) | Q(ended_at__isnull=True),
            started_at__lte=end)

        if qsc.exists():
            return True

    return False


def get_guid():
    return uuid.uuid4().hex


def get_md5(value):
    md5 = hashlib.md5()
    md5.update(str(value).encode())
    return md5.hexdigest()


def randstring(len):
    s = string.ascii_lowercase + string.digits
    return ''.join(random.sample(s, len))


def splitlist(list, chunk_size):
    """
    Split list on chunks.
    """
    return [list[offs:offs + chunk_size] for offs in range(0, len(list), chunk_size)]


def slicelist(l, cols=2):
    """
    Slice list into number of columns
    Returns generator
    """
    start = 0
    for i in range(cols):
        stop = start + len(l[i::cols])
        yield l[start:stop]
        start = stop


def generate_filename():
    value = str(time.time()) + randstring(10)
    return get_md5(value)


def create_filename(filename):
    """
    Creates filename from filename.
    Uses random name for name itself
    and lowercase extension.
    """
    tup = os.path.splitext(filename)
    ext = tup[1].lower()
    return generate_filename() + ext


def  camel_case_to_under_case(_dict):
    """
    Преобразование название ключей словаря с camelCase стиля в under_case стиль
    """
    result = {}
    for key, val in _dict.items():
        lower_list = re.split(r'[A-Z]', key)
        upper_list = re.findall(r'[A-Z]', key)
        tmp_list = []
        for index in range(len(lower_list)-1):
            tmp_list.append(f'{lower_list[index]}_{upper_list[index].lower()}')
        tmp_list.append(lower_list[len(lower_list)-1])
        result[''.join(tmp_list)] = val

    return result


def to_camel(value):
    words = [word.capitalize() for word in value.split('_')]
    words[0] = words[0].lower()
    return ''.join(words)


def to_underscore(value):
    s = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', value)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s).lower()


def get_ip_address(request):
    try:
        value = request.META.get('HTTP_X_FORWARDED_FOR', None)
        if value:
            ip = value.split(',')[0].strip()
        else:
            ip = request.META.get('REMOTE_ADDR', None)
    except Exception:
        ip = None
    return ip


def rs_singleton(rs, key, exp=1800):
    """
    Redis singleton.
    rs - Redis server.
    key - key that used in Redis db.
    exp - expire time seconds, if fault (exception).
    """
    def deco(func):
        @functools.wraps(func)
        def inner(*args, **kwargs):
            if rs.get(key):
                return
            rs.set(key, 'true')
            rs.expire(key, exp)
            output = func(*args, **kwargs)
            rs.delete(key)
            return output
        return inner
    return deco


class cached:
    def __init__(self, *args, **kwargs):
        self.cached_function_responses = {}
        self.default_max_age = kwargs.get(
            'default_max_age', timedelta(seconds=0))

    def __call__(self, func):
        def inner(*args, **kwargs):
            max_age = kwargs.get('max_age', self.default_max_age)
            if not max_age or func not in self.cached_function_responses or \
                    (datetime.now() - self.cached_function_responses[func]['fetch_time'] > max_age):
                if 'max_age' in kwargs:
                    del kwargs['max_age']
                res = func(*args, **kwargs)
                self.cached_function_responses[func] = {
                    'data': res, 'fetch_time': datetime.now()}
            return self.cached_function_responses[func]['data']
        return inner
