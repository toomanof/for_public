config_nginx_backend = """
server {
    server_name %(server_name)s;

    listen 80;

    client_max_body_size 20M;
    charset utf-8;
    access_log  /var/log/nginx/sites/access/ztrade_api.log;
    access_log off;
    error_log   /var/log/nginx/sites/error/ztrade_api.log crit;

    # Centrifugo websocket server
    location = /connection/websocket {
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $http_host;

        proxy_pass http://127.0.0.1:9000;
        proxy_redirect off;

        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
    }


    location /static/ {
        autoindex off;
        root /home/ubuntu/ztrade_api/;
    }

    location / {
        uwsgi_pass 127.0.0.1:4002;
        include uwsgi_params;
        uwsgi_buffers 8 128k;
    }
}
"""


config_nginx_frontend = """

server {
    server_name %(server_name)s www.%(server_name)s;

    listen 80;

    charset utf-8;
    access_log  /var/log/nginx/sites/access/%(server_name)s.log;
    error_log   /var/log/nginx/sites/error/%(server_name)s.log crit;
    access_log off;

    root /home/ubuntu/ztrade_react;

    location / {
        try_files $uri  $uri/ /index.html;
    }

    # Attempt to load static files, if not found route to @rootfiles
    location ~ (.+)\.(html|json|txt|js|css|jpg|jpeg|gif|png|svg|ico|eot|otf|woff|woff2|ttf)$ {
        try_files $uri @rootfiles;
    }

    location @rootfiles {
        # rewrite ^/(?:projects|foo/bar)/(.*)  /$1 redirect;
        # rewrite ^/(.*)/(.*)  /404 redirect;
    }
}
"""

config_pg_hba = """
local   all   postgres      trust
local   all   all                   md5
host    all   all    127.0.0.1/32   md5
host    all   all    ::1/128        md5
host    all   all    0.0.0.0/0      md5
"""


config_systemd_backend = """
[Unit]
Description=Backend API
After=network.target

[Service]
Type=simple
User=ubuntu
Group=ubuntu
WorkingDirectory=/home/ubuntu/ztrade_api
ExecStart=/home/ubuntu/.venvs/ztrade/bin/uwsgi --ini /home/ubuntu/ztrade_api/ztrade/ztrade/%(uwsgi_file)s
Restart=on-failure
RestartSec=10

[Install]
WantedBy=multi-user.target
"""


config_systemd_celery = """
[Unit]
Description=Celery
After=network.target

[Service]
Type=simple
User=ubuntu
Group=ubuntu
WorkingDirectory=/home/ubuntu/ztrade_api/ztrade
ExecStart=/home/ubuntu/.venvs/ztrade/bin/celery worker -A ztrade -l info -B
Restart=on-failure
RestartSec=10

[Install]
WantedBy=multi-user.target
"""


config_systemd_rates = """
[Unit]
Description=RatesServer
After=network.target

[Service]
Type=simple
User=ubuntu
Group=ubuntu
WorkingDirectory=/home/ubuntu/ztrade_rates
ExecStart=/home/ubuntu/ztrade_rates/ztrade
Restart=on-failure
RestartSec=10

[Install]
WantedBy=multi-user.target
"""


config_systemd_centrifugo = """
[Unit]
Description=Centrifugo
After=network.target

[Service]
Type=simple
User=ubuntu
Group=ubuntu
WorkingDirectory=/usr/bin
ExecStart=/usr/bin/centrifugo --config=/home/ubuntu/ztrade_api/ztrade/ztrade/settings/centrifugo.json  --port=9000 --log_level=debug
Restart=on-failure
RestartSec=10

[Install]
WantedBy=multi-user.target
"""
