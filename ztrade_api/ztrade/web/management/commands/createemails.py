from django.core.management.base import BaseCommand
from web.models import Email


class Command(BaseCommand):
    def process(self):
        tups = (
            (
                Email.CHANGE_EMAIL,
                'изменение e-mail в личном кабинете',
                'изменение e-mail в личном кабинете'
            ),
            (
                Email.DOWNLOAD_BOOK_AUTH,
                'скачивание книги',
                'скачивание книги'
            ),
            (
                Email.SIGNUP,
                'регистрация',
                'регистрация'
            ),
            (
                Email.SIGNUP_CONFIRM,
                'подтверждение регистрации',
                'подтверждение регистрации'
            ),
            (
                Email.RESET_PASSWORD,
                'восстановление пароля',
                'восстановление пароля'
            ),
            (
                Email.NEW_BROKER_DESC_CREATED,
                'добавлен новый пункт для брокера',
                'добавлен новый пункт для брокера'
            ),
            (
                Email.NEW_BROKER_REVIEW_CREATED,
                'добавлен новый отзыв для брокера',
                'добавлен новый отзыв для брокера'
            ),
            (
                Email.GET_SUBSCRIPTION_INFO,
                'информация по подписке',
                'информация по подписке',
            ),
            (
                Email.GET_SUBSCRIPTION_ACTIVE_INFO,
                'информация по активации подписки',
                'информация по активации подписки',
            ),
            (
                Email.PAY_PRODUCT,
                'получена оплата за товар',
                'получена оплата за товар',
            ),
            (
                Email.PAY_SUBSCRIPTION,
                'получена оплата за подписку',
                'получена оплата за подписку',
            ),
            (
                Email.PAY_TARIFF,
                'получена оплата за тариф',
                'получена оплата за тариф',
            ),
        )

        for code, name, subject in tups:
            Email.objects.create(code=code, name=name, subject=subject)

    def handle(self, *args, **options):
        self.process()
