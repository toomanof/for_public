from os.path import dirname
from django.core.management.base import BaseCommand
from django.conf import settings
from fabric.api import run
from fabric.api import sudo
from fabric.api import put
from fabric.api import env


class Command(BaseCommand):
    help = 'Deploy WS server to remote server'

    def init(self):
        env.host_string = 'ubuntu@{}'.format(settings.SERVER_IP)

    def deploy(self):
        run('mkdir -p /home/ubuntu/ztrade_rates')

        dir = settings.BASE_DIR + '/'
        local_path = dirname(dirname(dir)) + '/' + 'ztrade_rates/bin/ztrade'
        remote_path = '/home/ubuntu/ztrade_rates/ztrade'
        sudo('systemctl stop rates_server')
        put(local_path, remote_path, mode='0755')
        sudo('systemctl start rates_server')

    def handle(self, *args, **options):
        self.init()
        self.deploy()
