from fabric.api import put
from fabric.api import sudo
from fabric.api import env
from django.conf import settings
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    def __init__(self):
        env.host_string = 'ubuntu@{}'.format(settings.SERVER_IP)
        print(f'Connected to: {settings.SERVER_IP}')

    def create_db(self, *args, **options):
        cmd = "psql -U postgres -c 'drop database if exists {}';"\
            .format(settings.DB_NAME)
        sudo(cmd, user='postgres')

        cmd = "psql -U postgres -c 'drop database if exists test_{}';"\
            .format(settings.DB_NAME)
        sudo(cmd, user='postgres')

        cmd = "psql -U postgres -c 'drop user if exists {}';"\
            .format(settings.DB_USER)
        sudo(cmd, user='postgres')

        cmd = "psql -U postgres -c 'create database {}';"\
            .format(settings.DB_NAME)
        sudo(cmd, user='postgres')

        cmd = "psql -U postgres -c 'create database test_{}';"\
            .format(settings.DB_NAME)
        sudo(cmd, user='postgres')

        cmd = "psql -U postgres -c 'create user {}';"\
            .format(settings.DB_USER)
        sudo(cmd, user='postgres')

        cmd = "psql -U postgres -c \"alter role {} password '{}'\";"\
            .format(settings.DB_USER, settings.DB_PASSWORD)
        sudo(cmd, user='postgres')

        cmd = "psql -U postgres -c 'alter role {} createdb';"\
            .format(settings.DB_USER)
        sudo(cmd, user='postgres')

        cmd = "psql -U postgres -c \"grant all privileges on database {} to {}\";"\
            .format(settings.DB_NAME, settings.DB_USER)
        sudo(cmd, user='postgres')

    def upload_db(self):
        src_pth = settings.BASE_DIR + '/ztrade/db.dump'
        dst_pth = '/tmp/db.dump'
        put(src_pth, dst_pth, mode='0664', use_sudo=True)

    def restore_db(self):
        cmd = 'pg_restore -d {} -U postgres /tmp/db.dump'\
            .format(settings.DB_NAME)
        sudo(cmd, user='postgres')

    def handle(self, *args, **options):
        self.upload_db()
        self.create_db()
        self.restore_db()
