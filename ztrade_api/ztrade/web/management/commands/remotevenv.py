import io
from django.core.management.base import BaseCommand
from django.conf import settings
from fabric.api import run
from fabric.api import env
from fabric.api import cd


class Command(BaseCommand):
    help = 'Install virtual environemnt on remote server'

    def init(self):
        env.host_string = 'ubuntu@{}'.format(settings.SERVER_IP)

    def handle(self, *args, **options):
        self.init()

        with cd('/home/ubuntu/ztrade_api/ztrade/ztrade'):
            cmd = '. /home/ubuntu/.venvs/ztrade/bin/activate'
            cmd += ' && pip install -r requirements.txt'
            run(cmd)
