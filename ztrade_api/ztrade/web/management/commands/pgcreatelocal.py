from django.core.management.base import BaseCommand
from django.conf import settings
from web.management.helpers import (
    get_local_root_connection, get_local_db_connection)


class Command(BaseCommand):
    help = 'Drop db and user and creates new db and user from scratch.'

    def handle(self, *args, **options):

        con, cur = get_local_root_connection()

        cur.execute('drop database if exists {}'.format(settings.DB_NAME))
        # database for tests
        cur.execute('drop database if exists test_{}'.format(
            settings.DB_NAME))

        cur.execute('drop user if exists {}'.format(settings.DB_USER))

        cur.execute('create database {}'.format(settings.DB_NAME))
        cur.execute('create user {}'.format(settings.DB_USER))
        cur.execute("alter role {} password '{}'".format(
            settings.DB_USER, settings.DB_PASSWORD))
        cur.execute('alter role {} createdb'.format(settings.DB_USER))

        self.stdout.write('New local database initialized.')
        try:
            con, cur = get_local_db_connection()
        except Exception:
            self.stdout.write('Error: connection doesn\'t work.')
