import shutil
from django.conf import settings
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    def handle(self, *args, **options):
        l = [
            'web',
            'account',
            'broker',
            'content',
            'payment',
            'shop',
            'sgn'
        ]
        for name in l:
            path = '{}/ztrade/{}/migrations'.format(
                settings.BASE_DIR, name)
            shutil.rmtree(path, ignore_errors=True)

        print('Migrations have been removed.')
