from django.core.management.base import BaseCommand
from django.conf import settings

from fabric.api import env


class RemoteControl(BaseCommand):
    def __init__(self):
        env.host_string = 'ubuntu@{}'.format(settings.SERVER_IP)
        print(f'Connected to: {settings.SERVER_IP}')
