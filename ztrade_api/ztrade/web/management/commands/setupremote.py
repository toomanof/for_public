import io
import re
from os.path import dirname
from django.core.management.base import BaseCommand
from django.conf import settings
from fabric.api import sudo
from fabric.api import run
from fabric.api import env
from fabric.api import put
from fabric.api import cd
from fabric.contrib.project import rsync_project
from web.management.data.configs import config_nginx_backend
from web.management.data.configs import config_nginx_frontend
from web.management.data.configs import config_pg_hba
from web.management.data.configs import config_systemd_backend
from web.management.data.configs import config_systemd_rates
from web.management.data.configs import config_systemd_centrifugo
from web.management.helpers import get_remote_pg_root_password


class Command(BaseCommand):
    help = 'Install remote server'

    def init(self):
        env.host_string = 'ubuntu@{}'.format(settings.SERVER_IP)
        print(settings.SERVER_IP)

    def setup_initial(self):
        sudo('apt update')
        cmd = (
            'apt install -y build-essential libevent-dev libjpeg-dev nano '
            'htop curl libcurl4-gnutls-dev libgnutls28-dev libghc-gnutls-dev '
            'libxml2-dev libxslt1-dev libpq-dev '
            'git-core debconf-utils python-dev python3-dev'
        )
        sudo(cmd)
        sudo('apt install -y python3-pip')
        sudo('pip3 install virtualenv')

    def setup_ufw(self):
        sudo('apt-get install -y ufw')
        sudo('ufw default deny incoming')
        sudo('ufw default allow outgoing')
        sudo('ufw allow from 109.106.139.37')
        sudo('ufw allow from 194.58.42.44')
        sudo('ufw allow from 159.69.113.201')
        sudo('ufw allow 80/tcp')
        sudo('ufw allow 443/tcp')
        sudo('ufw allow 9000/tcp')  # centrifugo
        sudo('ufw enable')
        sudo('ufw status')

    def setup_nginx(self):
        sudo('apt install -y nginx')
        sudo('systemctl enable nginx')
        sudo('systemctl start nginx')
        sudo('mkdir -p /var/log/nginx/sites/access')
        sudo('mkdir -p /var/log/nginx/sites/error')
        sudo('chown -R www-data:www-data /var/log/nginx/sites')

    def setup_redis(self):
        sudo('apt install -y redis-server')
        sudo('systemctl enable redis-server')
        sudo('systemctl start redis-server')

    def get_postgresql_version(self):
        fh = io.StringIO()
        sudo('pg_config --version', stdout=fh)
        fh.seek(0)
        for line in fh.readlines():
            m = re.search('PostgreSQL (\d+)(\.\d)', line)
            if m is None:
                continue

            version = m.group(1)
            subversion = m.group(2)

            if int(version) == 10:
                return version
            return '{}{}'.format(version, subversion)
        raise ValueError('Can not get PostgreSQL version')

    def setup_postgresql(self):
        sudo('apt install -y postgresql')
        sudo('apt install -y pgadmin3')
        sudo('apt install -y postgresql-contrib')
        sudo('systemctl enable postgresql')
        sudo('systemctl start postgresql')

        pg_version = self.get_postgresql_version()

        remote_path = '/etc/postgresql/{}/main/pg_hba.conf'.format(pg_version)

        f = io.StringIO()
        f.write(config_pg_hba)
        put(f, remote_path, use_sudo=True)
        f.close()

        sudo('chown postgres:postgres {}'.format(remote_path))

        sudo('cd /tmp')

        cmd = 'psql -U postgres -d postgres -c "alter user postgres with password \'{}\';"'
        cmd = cmd.format(get_remote_pg_root_password())
        sudo(cmd, user='postgres')

        cmd = 'echo "listen_addresses = \'*\'" >> /etc/postgresql/{}/main/postgresql.conf'\
            .format(pg_version)
        sudo(cmd)
        sudo('systemctl restart postgresql')

    def setup_backend_project(self):
        """
        Create virtual environment.
        Copy project to remote server.
        Install virtual environment on remote server.
        Setup systemd config.
        Setup nginx config.
        """
        run('mkdir -p /home/ubuntu/.venvs')
        run('virtualenv /home/ubuntu/.venvs/ztrade --python=/usr/bin/python3')
        run('mkdir -p /home/ubuntu/ztrade_api')

        local_dir = settings.BASE_DIR + '/'
        remote_dir = '/home/ubuntu/ztrade_api/'
        rsync_project(local_dir=local_dir, remote_dir=remote_dir,
                      delete=True,
                      exclude=['__pycache__', '.cache', 'db.dump', '.git'])

        with cd('/home/ubuntu/ztrade_api/ztrade/ztrade'):
            cmd = '. /home/ubuntu/.venvs/ztrade/bin/activate'
            cmd += ' && pip install -r requirements.txt'
            run(cmd)

        # Systemd
        remote_path = '/etc/systemd/system/ztradeapi.service'

        uwsgi_file = 'uwsgi.ini'
        content = config_systemd_backend % {'uwsgi_file': uwsgi_file}

        f = io.StringIO()
        f.write(content)
        put(f, remote_path, mode='0664', use_sudo=True)
        f.close()

        sudo('systemctl daemon-reload')
        sudo('systemctl enable ztradeapi')
        sudo('systemctl start ztradeapi')

    def setup_backend_nginx(self):
        remote_path = '/etc/nginx/conf.d/ztrade_api.conf'
        f = io.StringIO()

        content = config_nginx_backend \
            % {
                'server_name': settings.BACKEND_DOMAIN,
                'server_ip': settings.SERVER_IP
            }

        f.write(content)
        put(f, remote_path, mode='0664', use_sudo=True)
        f.close()

        sudo('systemctl reload nginx')

    def setup_frontend_nginx(self):
        remote_path = '/etc/nginx/conf.d/ztrade_react.conf'
        f = io.StringIO()

        content = config_nginx_frontend \
            % {'server_name': settings.FRONTEND_DOMAIN}

        f.write(content)
        put(f, remote_path, mode='0664', use_sudo=True)
        f.close()

        sudo('systemctl reload nginx')

    def setup_rates_server(self):
        """
        Golang server that get prices from Oanda and send to centrifugo
        to "rates" channel.
        """
        sudo('systemctl stop ratesserver')
        run('mkdir -p /home/ubuntu/ztrade_rates')

        dir = settings.BASE_DIR + '/'
        local_path = dirname(dirname(dir)) + '/' + 'ztrade_rates/bin/ztrade'
        remote_path = '/home/ubuntu/ztrade_rates/ztrade'
        put(local_path, remote_path, mode='0755')

        # Systemd
        remote_path = '/etc/systemd/system/ratesserver.service'
        f = io.StringIO()
        f.write(config_systemd_rates)
        put(f, remote_path, mode='0664', use_sudo=True)
        f.close()

        sudo('systemctl daemon-reload')
        sudo('systemctl enable ratesserver')
        sudo('systemctl start ratesserver')

    def install_centrifugo(self):
        cmd = 'curl -s https://packagecloud.io/install/repositories/'
        cmd += 'FZambia/centrifugo/script.deb.sh | sudo bash'
        sudo(cmd)
        sudo('sudo apt-get install centrifugo=2.2.4-0')

        # disable default service that runs on localhost:8000
        sudo('sudo systemctl disable centrifugo')

    def setup_centrifugo(self):
        """
        Setup systemd service for centrifugo
        """
        remote_path = '/etc/systemd/system/centrifugo.service'
        f = io.StringIO()
        f.write(config_systemd_centrifugo)
        put(f, remote_path, mode='0664', use_sudo=True)
        f.close()

        sudo('systemctl daemon-reload')
        sudo('systemctl enable centrifugo')
        sudo('systemctl start centrifugo')

    def handle(self, *args, **options):
        self.init()
        # self.setup_initial()
        # self.setup_ufw()
        # self.setup_nginx()
        # self.setup_redis()
        # self.setup_postgresql()
        # self.setup_backend_project()
        # self.setup_backend_nginx()
        # self.setup_frontend_nginx()
        self.setup_rates_server()
        # self.install_centrifugo()
        # self.setup_centrifugo()
        print('Server setuped successfully.')
