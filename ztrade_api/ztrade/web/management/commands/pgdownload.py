import datetime
import os
import shutil
from fabric.api import sudo
from fabric.api import local
from fabric.api import get
from os.path import dirname
from django.conf import settings
from django.core.management import call_command

from web.management.helpers import get_local_pg_root_password
from web.management.commands.mixins.remotecontrol import RemoteControl


class Command(RemoteControl):
    help = 'Download remote db and fill local db.'

    LOCAL_PATH = '/tmp/db.dump'

    @property
    def remote_path(self):
        dt = datetime.date.today()
        return '/home/ubuntu/backups/db-{}.dump'\
            .format(dt.strftime('%Y-%m-%d'))

    def download(self):
        call_command('pgcreatelocal')

        dic = {
            'db': settings.DB_NAME,
            'user': settings.DB_USER,
            'passwd': settings.DB_PASSWORD,
            'local_path': self.LOCAL_PATH,
            'remote_path': self.remote_path,
        }

        cmd = 'pg_dump -U postgres {db} -f {remote_path} --format=c'\
            .format(**dic)
        sudo(cmd, user='postgres')
        print('Remote dump has been created.')

        get(self.remote_path, self.LOCAL_PATH)
        print('Remote dump has been copied to localhost.')

        cmd = f'PGPASSWORD="{get_local_pg_root_password()}" '
        cmd += 'pg_restore -d {db} -U postgres {local_path}'.format(**dic)
        local(cmd)
        print('Local db has been filled.')

    def backup(self):
        dt = datetime.date.today()

        dst = os.path.join(
            dirname(dirname(settings.BASE_DIR)),
            'misc/db/db-{}.dump'.format(dt.strftime('%Y-%m-%d')))
        shutil.copy2(self.LOCAL_PATH, dst)

    def handle(self, *args, **options):
        self.download()
        self.backup()
