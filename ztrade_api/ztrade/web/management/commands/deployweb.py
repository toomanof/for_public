import os
from django.core.management.base import BaseCommand
from django.conf import settings
from fabric.api import local
from fabric.api import lcd
from fabric.api import run
from fabric.api import env
from fabric.contrib.project import rsync_project


class Command(BaseCommand):
    help = 'Deploy React to remote server'

    def init(self):
        env.host_string = 'ubuntu@{}'.format(settings.SERVER_IP)

    def deploy(self):
        run('mkdir -p /home/ubuntu/ztrade_react')

        dir = os.path.dirname(settings.BASE_DIR) + '/ztrade_react'
        with lcd(dir):
            local('npm run build')

        local_dir = os.path.dirname(settings.BASE_DIR) + '/ztrade_react/build/'
        remote_dir = '/home/ubuntu/ztrade_react/'
        rsync_project(local_dir=local_dir, remote_dir=remote_dir)

    def handle(self, *args, **options):
        self.init()
        self.deploy()
