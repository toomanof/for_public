import io
from django.core.management.base import BaseCommand
from django.conf import settings
from fabric.api import env
from fabric.api import put
from fabric.api import sudo
from web.management.data.configs import config_systemd_celery


class Command(BaseCommand):
    help = 'Update celery on remote server'

    def init(self):
        env.host_string = 'ubuntu@{}'.format(settings.SERVER_IP)

    def setup_systemd(self):
        remote_path = '/etc/systemd/system/ztradecelery.service'
        f = io.StringIO()
        f.write(config_systemd_celery)
        put(f, remote_path, mode='0664', use_sudo=True)
        f.close()

        sudo('systemctl daemon-reload')
        sudo('systemctl enable ztradecelery')
        sudo('systemctl start ztradecelery')

    def restart_celery(self):
        sudo('systemctl restart ztradecelery')

    def handle(self, *args, **options):
        self.init()
        self.setup_systemd()
        self.restart_celery()
