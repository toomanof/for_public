from django.core.management.base import BaseCommand


class Command(BaseCommand):
    def temp01(self):
        from account.models import Notification
        n = Notification.objects.get(id=256)
        from web.tasks import send_ws_message
        msg = n.get_ws_message()
        print(msg)
        send_ws_message.delay(msg)

    def handle(self, *args, **options):
        self.temp01()
