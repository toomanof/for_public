import os
from django.utils import timezone
from django.core.management.base import BaseCommand
from django.db import transaction
from django.conf import settings

from web.models import Country
from web.models import Currency
from web.models import Lang
from web.models import User


class Command(BaseCommand):
    @transaction.atomic
    def create_currencies(self):
        l = [
            ('RUR', 'Рубль'),
            ('USD', 'US Dollar'),
            ('EUR', 'Euro'),
        ]
        for code, name in l:
            Currency.objects.create(code=code, name=name)

    @transaction.atomic
    def create_countries(self):
        path = os.path.join(settings.BASE_DIR,
                            'ztrade/web/management/data/countries.txt')
        f = open(path)
        for line in f:
            line = line.strip()
            code, name, class_name, prefix, sort = line.split(';')
            Country.objects.create(
                code=code, name=name,
                class_name=class_name,
                phone_prefix=prefix,
                sort=sort)

    @transaction.atomic
    def create_langs(self):
        l = [
            ('en', 'English'),
            ('ru', 'Русский')
        ]
        for code, name in l:
            Lang.objects.create(code=code, name=name)

    @transaction.atomic
    def create_superuser(self):
        u = User.objects.create_superuser('a@a.com', '111')
        u.first_name = 'Макс'
        u.last_name = 'Админ'
        u.city = 'Екатеринбург'
        u.country = Country.objects.get(code='ru')
        u.phone = '+7 922 030 84 44'
        u.joined_at = timezone.now()
        u.save()

        u.uid = User.objects.generate_uid()
        u.save()

    @transaction.atomic
    def create_desit(self):
        u = User.objects.create_user('it@desit.ru', '111')
        u.first_name = 'Владимир'
        u.last_name = 'Грищенко'
        u.city = 'Воронеж'
        u.country = Country.objects.get(code='ru')
        u.phone = '+7 908 140 85 58'
        u.joined_at = timezone.now()
        u.save()

        u.uid = User.objects.generate_uid()
        u.save()

    @transaction.atomic
    def create_ztrade(self):
        u = User.objects.create_user('ztrade.ru@bk.ru', '111')
        u.first_name = 'Максим'
        u.last_name = 'Серков'
        u.city = 'Екатеринбург'
        u.country = Country.objects.get(code='ru')
        u.phone = '+7 900 000 00 00'
        u.joined_at = timezone.now()
        u.save()

        u.uid = User.objects.generate_uid()
        u.save()

    @transaction.atomic
    def create_content_admin(self):
        u = User.objects.create_user('c@c.com', '111')
        u.type = User.CONTENT_ADMIN
        u.first_name = 'Админ'
        u.last_name = 'Контента'
        u.city = 'Москва'
        u.country = Country.objects.get(code='ru')
        u.phone = '+7 900 000 00 01'
        u.joined_at = timezone.now()
        u.save()

        u.uid = User.objects.generate_uid()
        u.save()

    @transaction.atomic
    def create_signal_admin(self):
        u = User.objects.create_user('sig@sig.com', '111')
        u.type = User.SIGNAL_ADMIN
        u.first_name = 'Админ'
        u.last_name = 'Сигналов'
        u.city = 'Москва'
        u.country = Country.objects.get(code='ru')
        u.phone = '+7 900 000 00 02'
        u.joined_at = timezone.now()
        u.save()

        u.uid = User.objects.generate_uid()
        u.save()

    def handle(self, *args, **options):
        self.create_currencies()
        self.create_countries()
        self.create_langs()
        self.create_superuser()
        self.create_desit()
        self.create_ztrade()
        self.create_content_admin()
        self.create_signal_admin()
