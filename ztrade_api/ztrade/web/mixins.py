from django.core.exceptions import ValidationError
from web.models import User


class UidEmailMixin:
    def validate_uid_email(self, value):
        if value.isdigit():
            uid = int(value)
            user = User.objects.filter(uid=uid).first()
            if user is None:
                raise ValidationError('ID не найден')
        else:
            user = User.objects.filter(email=value).first()
            if user is None:
                raise ValidationError('Email не найден')

        self.context['user'] = user
        return value
