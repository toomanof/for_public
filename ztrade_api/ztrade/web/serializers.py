import datetime
from dateutil import relativedelta
from django.shortcuts import get_object_or_404
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from rest_framework.serializers import ModelSerializer
from rest_framework.serializers import Serializer
from rest_framework.serializers import ValidationError
from rest_framework.serializers import CharField
from rest_framework.serializers import ChoiceField
from rest_framework.serializers import ModelField
from rest_framework.serializers import EmailField

from web.models import Country
from web.models import Currency
from web.models import Email
from web.models import Emailer
from web.models import Lang
from web.models import SMS
from web.models import SubscribeForm
from web.models import Support
from web.models import Timezone
from web.models import User

from web.mixins import UidEmailMixin

from web.services.helpers import randstring
from web.services.helpers import get_minutes_string

from web.constants import EMAIL
from web.constants import SMS as SMSCONST


class LoginSerializer(UidEmailMixin, Serializer):
    """
    User is set to context in UidEmailMixin
    """

    uid_email = CharField()
    password = CharField()

    def validate_password(self, value):
        user = self.context.get('user', None)

        if user is None:
            return value

        if not user.check_password(value):
            raise ValidationError('Неправильный пароль')

        return value

    def validate(self, data):
        user = self.context.get('user')

        if user is None:
            return data

        if not user.is_signup_completed:
            raise ValidationError('Пользователь не завершил регистрацию')

        if not user.is_active:
            raise ValidationError('Пользователь деактивирован')

        return data


class SignupSerializer(ModelSerializer):
    country_id = ModelField(model_field=User._meta.get_field('country'))
    first_name = CharField()
    last_name = CharField()
    email = EmailField()
    phone = CharField()
    city = CharField()

    class Meta:
        model = User
        fields = (
            'country_id',
            'city',
            'email',
            'first_name',
            'last_name',
            'phone',
        )

    def validate_email(self, value):
        if User.objects.filter(email=value.lower()).exists():
            raise ValidationError('Email принадлежит другому аккаунту')
        return value

    def validate_phone(self, value):
        v = User.objects.strip_phone(value)
        if User.objects.filter(phone_num=v):
            raise ValidationError('Телефон принадлежит другому аккаунту')

        if len(v) < 9:
            raise ValidationError('Телефон введен неверно')
        return value

    def save(self):
        d = self.validated_data

        user = User.objects.create_user(d['email'], randstring(10))
        user.__dict__.update(d)
        user.save()
        return user


class SignupChangeDataSerializer(Serializer):
    sms_guid = CharField()
    new_email = EmailField()
    new_phone = CharField()

    def validate_new_email(self, value):
        user = self.context.get('user')
        qs = User.objects.exclude(id=user.id).filter(email=value.lower())
        if qs.exists():
            raise ValidationError('Email принадлежит другому аккаунту')
        return value

    def validate_new_phone(self, value):
        user = self.context.get('user')
        v = User.objects.strip_phone(value)

        qs = User.objects.exclude(id=user.id).filter(phone_num=v)
        if qs.exists():
            raise ValidationError('Телефон принадлежит другому аккаунту')

        if len(v) < 9:
            raise ValidationError('Телефон введен неверно')
        return value

    def validate(self, data):
        user = self.context.get('user')
        if user.is_signup_completed:
            raise ValidationError('Пользователь был ранее подтвержден')
        return data

    def save(self):
        """
        If phone changed - update profile and send new sms
                           and respond with new sms guid
        If phone not changed - respond with old sms guid
        If email changed - update profile and send new email
        """
        d = self.validated_data
        user = self.context.get('user')
        sms_guid = d['sms_guid']
        new_phone = d['new_phone']
        new_email = d['new_email']

        is_email_changed = False
        is_phone_changed = False

        # only numbers
        new_phone_num = User.objects.strip_phone(new_phone)
        if new_phone_num != user.phone_num:
            user.phone = new_phone
            user.save()
            sms = SMS.objects.create_sms(SMS.SIGNUP, new_phone_num)
            sms_guid = sms.guid
            is_phone_changed = True

        if new_email != user.email:
            user.email = new_email
            user.save()
            Emailer.objects.process_email(Email.SIGNUP, user=user)
            is_email_changed = True

        return user, sms_guid, is_email_changed, is_phone_changed


class SignupConfirmSerializer(Serializer):
    sms_guid = CharField()
    sms_code = CharField()
    email_code = CharField()

    def validate_email_code(self, value):
        user = self.context.get('user')
        if user.signup_email_code != value:
            raise ValidationError('Неправильный код подтверждения email')
        return value

    def validate_sms_code(self, value):
        qs = SMS.objects.filter(guid=self.context.get('sms_guid'),
                                sms_code=value)
        if not qs.exists():
            raise ValidationError('Неправильный код SMS')
        return value

    def validate(self, data):
        user = self.context.get('user')
        if user.is_signup_completed:
            raise ValidationError('Пользователь был ранее подтвержден')
        return data

    def save(self):
        user = self.context.get('user')
        user.signup_confirm()
        return user


class SignupResendEmailCodeSerializer(Serializer):
    def validate(self, data):
        user = self.context.get('user')
        if user.is_signup_completed:
            raise ValidationError('Пользователь был ранее подтвержден')
        return data

    def save(self):
        user = self.context.get('user')
        Emailer.objects.process_email(Email.SIGNUP, user=user)
        return user


class SignupResendPhoneCodeSerializer(Serializer):
    """
    Interval between sending sms should be more than 5 minutes
    """
    sms_guid = CharField()

    def validate(self, data):
        user = self.context.get('user')
        if user.is_signup_completed:
            raise ValidationError('Пользователь был ранее подтвержден')

        sms = get_object_or_404(SMS, guid=data['sms_guid'])
        allow_at = sms.created_at + datetime.timedelta(seconds=300)

        rd = relativedelta.relativedelta(allow_at, timezone.now())
        if rd.minutes >= 0 and rd.minutes < 5 and rd.seconds >= 0:
            s = get_minutes_string(rd.minutes, rd.seconds)
            message = 'Повторную SMS можно отправить через {}'.format(s)
            raise ValidationError(message)
        return data

    def save(self):
        user = self.context.get('user')
        sms = SMS.objects.create_sms(SMS.SIGNUP, user.phone_num)
        return sms


class ResetPasswordSendCodeSerializer(UidEmailMixin, Serializer):
    uid_email = CharField()
    type = ChoiceField(choices=[EMAIL, SMSCONST])

    def save(self):
        d = self.validated_data
        user = self.context.get('user')
        user.reset_password_send_code(d['type'])


class ResetPasswordSerializer(UidEmailMixin, Serializer):
    uid_email = CharField()
    code = CharField()
    password = CharField(min_length=8)

    def validate_code(self, value):
        user = self.context.get('user')
        if user.reset_password_code != value:
            raise ValidationError('Неправильный код')
        return value

    def save(self):
        user = self.context.get('user')
        d = self.validated_data

        user.set_password(d['password'])
        user.reset_password_code = None
        user.save()
        return {'token': user.get_token().key}


class CountrySerializer(ModelSerializer):
    class Meta:
        model = Country
        fields = (
            'id',
            'code',
            'name',
            'class_name',
            'phone_prefix'
        )


class CurrencySerializer(ModelSerializer):
    class Meta:
        model = Currency
        fields = (
            'id',
            'code',
            'name'
        )


class LangSerializer(ModelSerializer):
    class Meta:
        model = Lang
        fields = (
            'id',
            'code',
            'name'
        )


class TimezoneSerializer(ModelSerializer):
    class Meta:
        model = Timezone
        fields = (
            'id',
            'name',
            'full_name',
            'offset'
        )


class SubscribeFormCreateSerializer(ModelSerializer):
    class Meta:
        model = SubscribeForm
        fields = (
            'type',
            'email',
        )


class SupportCreateSerializer(ModelSerializer):
    class Meta:
        model = Support
        fields = (
            'first_name',
            'last_name',
            'email',
            'question',
        )


class UserProfileSerializer(ModelSerializer):
    country = CountrySerializer(read_only=True)

    class Meta:
        model = User
        fields = (
            'id',
            'guid',
            'uid',
            'type',
            'email',
            'first_name',
            'last_name',
            'full_name',
            'phone',
            'country',
            'city',
            'is_signup_completed',
            'is_signal_paid',
            'is_subscription_paid',
            'is_receive_news',
            'is_receive_notifications',
            'is_receive_email',
        )


class UserProfileUpdateSerializer(ModelSerializer):
    country_id = ModelField(model_field=User._meta.get_field('country'))

    class Meta:
        model = User
        fields = (
            'country_id',
            'city',
            'is_receive_news',
            'is_receive_notifications',
            'is_receive_email',
        )


class UserProfileSendEmailCodeSerializer(Serializer):
    email = EmailField()

    def validate_email(self, value):
        if User.objects.filter(email=value):
            raise ValidationError('E-mail принадлежит другому аккаунту')
        return value

    def save(self):
        d = self.validated_data
        user = self.context.get('user')
        user.change_email_send_code(d['email'])


class UserProfileSendSMSCodeSerializer(Serializer):
    phone = CharField()

    def validate_phone(self, value):
        v = User.objects.strip_phone(value)
        if User.objects.filter(phone_num=v):
            raise ValidationError('Телефон принадлежит другому аккаунту')

        if len(v) < 9:
            raise ValidationError('Телефон введен неверно')
        return value

    def save(self):
        d = self.validated_data
        sms = SMS.objects.create_sms(SMS.CHANGE_PHONE, d['phone'])
        return sms.guid


class UserProfileChangeEmailSerializer(Serializer):
    email = EmailField()
    email_code = CharField()

    def validate_email_code(self, value):
        if self.context.get('user').change_email_code != value:
            raise ValidationError('Неправильный e-mail код')
        return value

    def save(self):
        d = self.validated_data
        user = self.context.get('user')
        user.email = d['email']
        user.save()
        return user


class UserProfileChangePhoneSerializer(Serializer):
    phone = CharField()
    sms_code = CharField()
    sms_guid = CharField()

    def validate_sms_code(self, value):
        sms = SMS.objects.filter(
            sms_code=value, guid=self.context.get('sms_guid')).first()
        if sms is None:
            raise ValidationError('Неправильный sms код')
        return value

    def save(self):
        d = self.validated_data
        user = self.context.get('user')
        user.phone = d['phone']
        user.save()
        return user


class UserProfileChangePasswordSerializer(Serializer):
    old_password = CharField()
    new_password = CharField(min_length=8, max_length=30)

    def validate_old_password(self, value):
        user = self.context.get('user')
        if not user.check_password(value):
            raise ValidationError(
                _('Current password is not correct.'))
        return value

    def save(self):
        d = self.validated_data
        u = self.context.get('user')
        u.set_password(d['new_password'])
        u.save()
