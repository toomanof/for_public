from django.urls import re_path
from django.views.generic import TemplateView
from .import views as v

handler404 = v.error404
handler500 = v.error500


urlpatterns = [
    re_path(r'^robots\.txt$', TemplateView.as_view(
        template_name='web/txt/robots.txt', content_type='text/plain')),

    re_path(
        r'^(?P<version>(v1))/settings$',
        v.Settings.as_view(),
        name='settings'),

    re_path(
        r'^(?P<version>(v1))/auth/signup$',
        v.Signup.as_view(),
        name='signup'),

    re_path(
        r'^(?P<version>(v1))/auth/signup/change_data/(?P<guid>\w+)$',
        v.SignupChangeData.as_view(),
        name='signup_change_data'),

    re_path(
        r'^(?P<version>(v1))/auth/signup/confirm/(?P<guid>\w+)$',
        v.SignupConfirm.as_view(),
        name='signup_confirm'),

    re_path(
        r'^(?P<version>(v1))/auth/signup/resend_email_code/(?P<guid>\w+)$',
        v.SignupResendEmailCode.as_view(),
        name='signup_resend_email_code'),

    re_path(
        r'^(?P<version>(v1))/auth/signup/resend_phone_code/(?P<guid>\w+)$',
        v.SignupResendPhoneCode.as_view(),
        name='signup_resend_phone_code'),

    re_path(
        r'^(?P<version>(v1))/auth/login$',
        v.Login.as_view(),
        name='login'),

    re_path(
        r'^(?P<version>(v1))/auth/check_token$',
        v.CheckToken.as_view(),
        name='check_token'),

    re_path(
        r'^(?P<version>(v1))/auth/reset_password_send_code$',
        v.ResetPasswordSendCode.as_view(),
        name='reset_password_send_code'),

    re_path(
        r'^(?P<version>(v1))/auth/reset_password$',
        v.ResetPassword.as_view(),
        name='reset_password'),

    re_path(
        r'^(?P<version>(v1))/profile$',
        v.UserProfile.as_view(),
        name='user_profile'),

    re_path(
        r'^(?P<version>(v1))/profile/send_sms_code$',
        v.UserProfileSendSMSCode.as_view(),
        name='user_profile_send_sms_code'),

    re_path(
        r'^(?P<version>(v1))/profile/send_email_code$',
        v.UserProfileSendEmailCode.as_view(),
        name='user_profile_send_email_code'),

    re_path(
        r'^(?P<version>(v1))/profile/change_email$',
        v.UserProfileChangeEmail.as_view(),
        name='user_profile_change_email'),

    re_path(
        r'^(?P<version>(v1))/profile/change_phone$',
        v.UserProfileChangePhone.as_view(),
        name='user_profile_change_phone'),

    re_path(
        r'^(?P<version>(v1))/profile/change_password$',
        v.UserProfileChangePassword.as_view(),
        name='user_profile_change_password'),

    re_path(
        r'^(?P<version>(v1))/profile/update_last_access$',
        v.UpdateLastAccess.as_view(),
        name='update_last_access'),

    # re_path(
    #     r'^(?P<version>(v1))/sms/check_code$',
    #     v.SMSCheckCode.as_view(),
    #     name='sms_check_code'),

    # re_path(
    #     r'^(?P<version>(v1))/sms/send$',
    #     v.SMSSend.as_view(),
    #     name='sms_send'),

    re_path(
        r'^(?P<version>(v1))/general/countries$',
        v.Countries.as_view(),
        name='countries'),

    re_path(
        r'^(?P<version>(v1))/general/currencies$',
        v.Currencies.as_view(),
        name='currencies'),

    re_path(
        r'^(?P<version>(v1))/general/langs$',
        v.Langs.as_view(),
        name='langs'),

    re_path(
        r'^(?P<version>(v1))/general/timezones$',
        v.Timezones.as_view(),
        name='timezones'),

    re_path(
        r'^(?P<version>(v1))/general/translations$',
        v.Translations.as_view(),
        name='translations'),

    re_path(
        r'^(?P<version>(v1))/general/signupload$',
        v.SignS3Upload.as_view(),
        name='sign_s3_upload'),

    re_path(
        r'^(?P<version>(v1))/general/support$',
        v.SupportView.as_view(),
        name='support'),

    re_path(
        r'^(?P<version>(v1))/general/subscribe_form$',
        v.SubscribeFormView.as_view(),
        name='subscribe_form'),

    re_path(
        r'^(?P<version>(v1))/general/centrifugo_token$',
        v.CentrifugoToken.as_view(),
        name='centrifugo_token'),

    re_path(
        r'^(?P<version>(v1))/admin/upload_logo$',
        v.UploadLogo.as_view(),
        name='upload_logo'),

    re_path(
        r'^(?P<version>(v2))/admin/upload_logo$',
        v.UploadLogoV2.as_view(),
        name='upload_logo_v2'),

    re_path(r'^$', v.home, name='home'),
]
