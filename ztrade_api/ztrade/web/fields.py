from django.db.models.fields import BooleanField, CharField


class LowerCharField(CharField):
    """
    To create LowerCharField - set lowercase=True in field.
    """
    def __init__(self, *args, **kwargs):
        self.is_lowercase = kwargs.pop('lowercase', False)
        super().__init__(*args, **kwargs)

    def get_prep_value(self, value):
        value = super().get_prep_value(value)
        if value is not None and self.is_lowercase:
            return value.lower()
        return value

    def from_db_value(self, value, expression, connection, context):
        return value

    def to_python(self, value):
        return value


class UniqueBooleanField(BooleanField):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def pre_save(self, model_instance, add):
        objects = model_instance.__class__.objects
        # If True then set all others as False
        if getattr(model_instance, self.attname):
            objects.update(**{self.attname: False})
        # If no true object exists that isnt saved model, save as True
        elif not objects.exclude(id=model_instance.id)\
                        .filter(**{self.attname: True}):
            return True
        return getattr(model_instance, self.attname)
