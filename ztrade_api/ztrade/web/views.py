import base64
import boto3
import datetime
import jwt
import logging
import time
from botocore.client import Config
from PIL import Image
from os.path import splitext
from django.shortcuts import get_object_or_404
from django.utils import timezone
from django.shortcuts import render
from django.http import HttpResponse
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.core.files.base import ContentFile
from rest_framework.permissions import IsAuthenticated
from rest_framework.permissions import IsAdminUser
from rest_framework.views import APIView
from rest_framework.generics import ListAPIView
from rest_framework.generics import CreateAPIView

from rest_framework.response import Response

from web.serializers import LoginSerializer
from web.serializers import SignupSerializer
from web.serializers import SignupChangeDataSerializer
from web.serializers import SignupResendEmailCodeSerializer
from web.serializers import SignupResendPhoneCodeSerializer
from web.serializers import SignupConfirmSerializer
from web.serializers import SubscribeFormCreateSerializer
from web.serializers import SupportCreateSerializer
from web.serializers import ResetPasswordSerializer
from web.serializers import ResetPasswordSendCodeSerializer

from web.serializers import CountrySerializer
from web.serializers import CurrencySerializer
from web.serializers import LangSerializer
from web.serializers import TimezoneSerializer

from web.serializers import UserProfileSerializer
from web.serializers import UserProfileUpdateSerializer
from web.serializers import UserProfileChangeEmailSerializer
from web.serializers import UserProfileChangePhoneSerializer
from web.serializers import UserProfileChangePasswordSerializer
from web.serializers import UserProfileSendEmailCodeSerializer
from web.serializers import UserProfileSendSMSCodeSerializer

from web.models import Country
from web.models import Currency
from web.models import Lang
from web.models import SubscribeForm
from web.models import Support
from web.models import Timezone
from web.models import Translation
from web.models import User

from web.services.helpers import get_guid
from web.services.helpers import get_md5

logger = logging.getLogger('app')


def error404(request):
    return render(request, 'web/html/404.html', status=404)


def error500(request):
    return render(request, 'web/html/500.html', status=500)


def r400(error_message):
    return Response({
        'errors': [error_message]}, status=400)


def r403(error_message):
    return Response(error_message, status=403)


class AuthMixin:
    permission_classes = (IsAuthenticated,)


class AdminMixin:
    permission_classes = (IsAuthenticated, IsAdminUser)


def home(request):
    return HttpResponse('API')


class CheckToken(APIView):
    """
    post:
        <h1>Endpoint to check token</h1>
        <b>Input params:</b><br />
        token<br />
        <b>Response:</b> object with is_valid bool prop <br />
        <b>Response code:</b> 200
    """

    def post(self, request, **kwargs):
        token = request.data.get('token', None)
        if token is None or not User.objects.check_token(token):
            return Response({'is_valid': False})
        return Response({'is_valid': True})


class CentrifugoToken(APIView):
    """
    get:
        <h1>Get centrifugo token valid for 24 hours</h1>
        <b>Response code:</b> 200<br />
    """
    def get(self, request, **kwargs):
        claims = {'sub': 'ztrade', 'exp': int(time.time()) + 24 * 3600}
        token = jwt.encode(
            claims, settings.CENTRIFUGO_SECRET, algorithm='HS256').decode()
        return Response({'token': token})


class Signup(APIView):
    """
    post:
        <h1>Signup endpoint</h1>
        <p>After signup backend registeres user, send email with code,
        send sms with code and user go to next step on frontend</p>
        <b>Input params:</b><br />
        email<br />
        first_name<br />
        last_name<br />
        phone<br />
        country<br />
        city<br />
        <b>Response:</b> user object, sms guid<br />
        <b>Response code:</b> 201
    """

    def post(self, request, **kwargs):
        s = SignupSerializer(data=request.data)
        s.is_valid(raise_exception=True)
        user = s.save()

        sms = user.signup()
        res = {
            'user': UserProfileSerializer(user).data,
            'sms_guid': sms.guid
        }
        return Response(res, status=201)


class SignupChangeData(APIView):
    """
    post:
        <h1>Signup change data endpoint</h1>
        <p>On 2nd step, before user has confirmed signup,
        it can edit email or phone.</p>
        <p>If email changed - update user profile and send new
        email to user</p>
        <p>If phone changed - update user profile and send new
        sms change and respond with new sms guid</p>
        <p>If phone not changed - respond with old sms guid</p>
        <b>Input params:</b><br />
        sms_guid<br />
        new_email<br />
        new_phone<br />
        <b>Response:</b> user object, sms guid, is_email_changed,
        is_phone_changed<br />
        <b>Response code:</b> 201
    """

    def post(self, request, guid, **kwargs):
        user = get_object_or_404(User, guid=guid)
        ctx = {'user': user}
        s = SignupChangeDataSerializer(data=request.data, context=ctx)
        s.is_valid(raise_exception=True)
        user, sms_guid, is_email_changed, is_phone_changed = s.save()

        d = {
            'user': UserProfileSerializer(user).data,
            'sms_guid': sms_guid,
            'is_email_changed': is_email_changed,
            'is_phone_changed': is_phone_changed
        }
        return Response(d)


class SignupConfirm(APIView):
    """
    post:
        <h1>Signup confirm endpoint</h1>
        <p>User send verification codes that it get to phone and email</p>
        <p>If codes are correct set uid, joined_at and password to user
        and send to user welcome email</p>
        <b>Input params:</b><br />
        sms_guid<br />
        sms_code<br />
        email_code<br />
        <b>Response:</b> user object<br />
        <b>Response code:</b> 201
    """

    def post(self, request, guid, **kwargs):
        user = get_object_or_404(User, guid=guid)
        ctx = {
            'user': user,
            'sms_guid': request.data.get('sms_guid', '')
        }
        s = SignupConfirmSerializer(data=request.data, context=ctx)
        s.is_valid(raise_exception=True)
        user = s.save()
        return Response(UserProfileSerializer(user).data, status=201)


class SignupResendEmailCode(APIView):
    """
    post:
        <h1>Resend email code during signup</h1>
        <b>No input params:</b><br />
        <b>Response code:</b> 204
    """

    def post(self, request, guid, **kwargs):
        user = get_object_or_404(User, guid=guid)
        ctx = {'user': user}
        s = SignupResendEmailCodeSerializer(data=request.data, context=ctx)
        s.is_valid(raise_exception=True)
        s.save()
        return Response(status=204)


class SignupResendPhoneCode(APIView):
    """
    post:
        <h1>Resend phone code during signup</h1>
        <b>Input params:</b><br />
        sms_guid<br />
        <b>Response:</b> new sms_guid<br />
        <b>Response code:</b> 200
    """

    def post(self, request, guid, **kwargs):
        user = get_object_or_404(User, guid=guid)
        ctx = {'user': user}
        s = SignupResendPhoneCodeSerializer(data=request.data, context=ctx)
        s.is_valid(raise_exception=True)
        sms = s.save()
        return Response({'sms_guid': sms.guid})


class Login(APIView):
    """
    post:
        <h1>Login endpoint</h1>
        <p>Server responds with Auth Token</p>
        <b>Input params:</b> uid_email, password<br />
        <b>Response:</b> token<br />
        <b>Response code:</b> 200
    """

    def post(self, request, **kwargs):
        s = LoginSerializer(data=request.data)
        s.is_valid(raise_exception=True)
        user = s.context.get('user')
        data = {'token': user.get_token().key}

        user.last_access_at = timezone.now()
        user.save()
        return Response(data, status=200)


class ResetPasswordSendCode(APIView):
    """
    post:
        <h1>Before user can reset password, it should get code</h1>
        <p>Endpoint generates random code, saves it to "reset_password_code"
        user field and send to user by email or sms</p>
        <b>Input params:</b><br />
        uid_email<br />
        type (email/sms)<br />
        <b>Response code:</b> 204
    """

    def post(self, request, **kwargs):
        s = ResetPasswordSendCodeSerializer(data=request.data)
        s.is_valid(raise_exception=True)
        s.save()
        return Response(status=204)


class ResetPassword(APIView):
    """
    post:
        <h1>Reset password endpoint</h1>
        <p>As soon as user got code to reset password it sends
        request to set new password</p>
        <b>Input params:</b><br />
        uid_email<br />
        code<br />
        password<br />
        <b>Response:</b> token<br />
        <b>Response code:</b> 201
    """

    def post(self, request, **kwargs):
        s = ResetPasswordSerializer(data=request.data)
        s.is_valid(raise_exception=True)
        data = s.save()
        return Response(data, status=201)


class Settings(AuthMixin, APIView):
    """
    Settings for auth user
    """

    def get(self, request, **kwargs):
        d = {}
        return Response(d, status=200)


class UserProfile(AuthMixin, APIView):
    """
    get:
        <h1>User can get profile</h1>
        <b>Response code:</b> 200<br />
    put:
        <h1>User can change country and city</h1>
        <b>Response code:</b> 200<br />
    """
    def get(self, request, **kwargs):
        s = UserProfileSerializer(request.user)
        return Response(s.data)

    def put(self, request, **kwargs):
        s = UserProfileUpdateSerializer(request.user, data=request.data)
        s.is_valid(raise_exception=True)
        s.save()
        return Response(UserProfileSerializer(request.user).data, status=201)


class UserProfileSendSMSCode(AuthMixin, APIView):
    """
    post:
        <h1>Send SMS code for changing email in profile</h1>
        <b>Response code:</b> 200<br />
    """

    def post(self, request, **kwargs):
        s = UserProfileSendSMSCodeSerializer(data=request.data)
        s.is_valid(raise_exception=True)
        sms_guid = s.save()
        return Response({'sms_guid': sms_guid}, status=201)


class UserProfileSendEmailCode(AuthMixin, APIView):
    """
    post:
        <h1>Send email code for changing email in profile</h1>
        <b>Response code:</b> 204<br />
    """

    def post(self, request, **kwargs):
        ctx = {'user': request.user}
        s = UserProfileSendEmailCodeSerializer(data=request.data, context=ctx)
        s.is_valid(raise_exception=True)
        s.save()
        return HttpResponse(status=204)


class UserProfileChangeEmail(AuthMixin, APIView):
    """
    post:
        <h1>User can change own email</h1>
        <b>Input params:</b> email, email_code<br />
        <b>Response code:</b> 201<br />
    """

    def post(self, request, **kwargs):
        ctx = {
            'user': request.user,
            'sms_guid': request.data.get('sms_guid', None)
        }
        s = UserProfileChangeEmailSerializer(data=request.data, context=ctx)
        s.is_valid(raise_exception=True)
        user = s.save()
        return Response(UserProfileSerializer(user).data, status=201)


class UserProfileChangePhone(AuthMixin, APIView):
    """
    post:
        <h1>User can change own phone</h1>
        <b>Input params:</b> phone, sms_code, sms_guid<br />
        <b>Response code:</b> 201<br />
    """

    def post(self, request, **kwargs):
        ctx = {
            'user': request.user,
            'sms_guid': request.data.get('sms_guid', None)
        }
        s = UserProfileChangePhoneSerializer(data=request.data, context=ctx)
        s.is_valid(raise_exception=True)
        user = s.save()
        return Response(UserProfileSerializer(user).data, status=201)


class UserProfileChangePassword(AuthMixin, APIView):
    """
    post:
        <h1>User can change own password</h1>
        <b>Input params:</b> old_password, new_password<br />
        <b>Response code:</b> 204<br />
    """

    def post(self, request, **kwargs):
        s = UserProfileChangePasswordSerializer(data=request.data,
                                                context={'user': request.user})
        s.is_valid(raise_exception=True)
        s.save()
        return Response(status=204)


class UpdateLastAccess(AuthMixin, APIView):
    """
    post:
        <h1>Endpoint to update user's last_access_at</h1>
        <p>Frontend call this endpoint once per "X" minutes</p>
        <b>Input params: </b> No<br />
        <b>Response code:</b> 204<br />
    """

    def post(self, request, **kwargs):
        u = request.user
        u.last_access_at = timezone.now()
        u.save()
        return Response(status=204)


class Countries(ListAPIView):
    """
    get:
        <h1>Get list of countries</h1>
        <b>Response:</b> id, code, name<br />
        <b>Response code:</b> 200
    """
    queryset = Country.objects.all()
    serializer_class = CountrySerializer


class Currencies(ListAPIView):
    """
    get:
        <h1>Get list of currencies</h1>
        <b>Response:</b> id, code, name<br />
        <b>Response code:</b> 200
    """
    queryset = Currency.objects.all()
    serializer_class = CurrencySerializer


class Langs(ListAPIView):
    """
    get:
        <h1>Get list of langs</h1>
        <b>Response:</b> id, code, name<br />
        <b>Response code:</b> 200
    """
    queryset = Lang.objects.all()
    serializer_class = LangSerializer


class Timezones(ListAPIView):
    """
    get:
        <h1>Get list of timezones</h1>
        <b>Response:</b> id, name, offset<br />
        <b>Response code:</b> 200
    """
    queryset = Timezone.objects.all()
    serializer_class = TimezoneSerializer


class Translations(APIView):
    """
    get:
        <h1>Get translations</h1>
        <b>Response code:</b> 200
    """

    def get(self, request, **kwargs):
        res = Translation.objects.get_dict('ru')
        return Response(res)


class UploadLogo(AdminMixin, APIView):
    """
    post:
        <h1>Admin can upload logo for own subdomain</h1>
        <p>Endpoint expects image as base64 string</p>
        <p>Image must be in jpeg, png or gif format.</p>
        <p>Server responds with link to uploaded image</p>
        <b>Input params:</b> image_data<br />
        <b>Response:</b> link<br />
        <b>Response code:</b> 201
    delete:
        <h1>Admin can delete logo</h1>
        <b>Response code:</b> 204
    """

    def post(self, request, **kwargs):
        image_data = request.data.get('image_data', None)
        if image_data is None:
            return r400(_('Incorrect request'))

        format, imgstr = image_data.split(';base64,')
        ext = format.split('/')[-1]

        if not ext or ext.lower() not in ['png', 'gif', 'jpg', 'jpeg']:
            return r400(_('The image format should be one of png, jpeg, gif'))

        data = ContentFile(base64.b64decode(imgstr))

        filename = get_guid() + '.' + ext

        tenant = request.user.tenant
        tenant.logo.save(filename, data)
        tenant.save()

        d = {
            'link': tenant.logo_url,
        }
        return Response(d, status=201)

    def delete(self, request, **kwargs):
        tenant = request.user.tenant
        tenant.width = None
        tenant.height = None
        tenant.logo.delete(save=True)
        return Response(status=204)


class UploadLogoV2(AdminMixin, APIView):
    """
    post:
        <h1>Admin can upload logo for own subdomain</h1>
        <p>Endpoint expects multipart format</p>
        <p>Image must be in jpeg, png or gif format.
        Max image width is 500 px</p>
        <p>Server responds with link to uploaded image</p>
        <b>Input params:</b> file<br />
        <b>Response:</b> link<br />
        <b>Response code:</b> 201
    """

    def post(self, request, **kwargs):
        file = request.FILES.get('file', None)
        if file is None:
            return r400(_('Incorrect request'))

        img = Image.open(file)
        max_width = settings.MAX_LOGO_WIDTH
        if img.width > max_width:
            return r400(
                _('The maximum image width is {} px'.format(max_width)))

        if img.format.lower() not in ['png', 'gif', 'jpg', 'jpeg']:
            return r400(_('The image format should be one of png, jpeg, gif'))

        __, ext = splitext(file.name)
        ext = ext.lower()
        if ext not in ['.png', '.gif', '.jpg', '.jpeg']:
            return r400(_('The image format should be one of png, jpeg, gif'))

        filename = get_guid() + ext

        tenant = request.user.tenant
        tenant.logo.save(filename, file)

        tenant.width = img.width
        tenant.height = img.height
        tenant.save()

        return Response({'link': tenant.logo_url}, status=201)


class SignS3Upload(AuthMixin, APIView):
    """
    get:
        <h1>Get sign url for upload file directly to S3</h1>
        Request params<br />
        guid - any unique guid to identify file<br />
        filename<br />
        content_type - content type of uploaded file<br />
        upload_type - (file)<br />
    """

    def post(self, request, **kwargs):
        guid = request.data.get('guid')
        filename = request.data.get('filename')
        content_type = request.data.get('content_type')
        upload_type = request.data.get('upload_type')

        bucket_name = settings.AWS_STORAGE_BUCKET_NAME

        if upload_type == FILE:
            upload_to = 'files/{}/{}/{}'.format(
                request.user.tenant.subdomain,
                get_md5(datetime.date.today().strftime('%Y%m')),
                filename)
        else:
            return r400(_('Incorrect upload_type'))

        client = boto3.client(
            's3',
            aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
            aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY,
            region_name=settings.AWS_REGION,
            config=Config(signature_version='s3v4')
        )

        signed_url = client.generate_presigned_url(
            ClientMethod='put_object',
            Params={
                'ACL': 'public-read',
                'Bucket': bucket_name,
                'Key': upload_to,
                'ContentType': content_type,
                'CacheControl': 'max-age=31536000',
            }
        )
        return Response(
            {
                'guid': guid,
                'signed_url': signed_url,
                'upload_to': upload_to
            }
        )


class SubscribeFormView(CreateAPIView):
    queryset = SubscribeForm.objects.all()
    serializer_class = SubscribeFormCreateSerializer


class SupportView(CreateAPIView):
    queryset = Support.objects.all()
    serializer_class = SupportCreateSerializer
