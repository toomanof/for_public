
EMAIL = 'email'
SMS = 'sms'

PRODUCT = 'product'
SUBSCRIPTION = 'subscription'

# Centrifugo channels
# "rates" is dedicated channel for Oanda prices
# "account" channel get all account messages and each message
# should contain "type"
CHANNEL_ACCOUNT = 'account'
CHANNEL_RATES = 'rates'
