import datetime
import json
import requests
from cent import Client
from celery.decorators import periodic_task
from celery.task.schedules import crontab
from django.utils import timezone
from django.conf import settings
from django.apps import apps
from ztrade import capp
from fabric.api import sudo
from web.services.helpers import get_s3_client
from django.conf import settings

from telebot import TeleBot


@capp.task
def send_email(id):
    Emailer = apps.get_model('web', 'Emailer')

    em = Emailer.objects.filter(id=id, is_sent=False).first()
    if em is None:
        return

    em.send()
    em.refresh_from_db()

    if em.is_sent is False:
        send_email.apply_async(args=[em.id], countdown=300)


@capp.task
def send_sms(id):
    SMS = apps.get_model('web', 'SMS')

    sms = SMS.objects.filter(id=id, is_sent=False).first()
    if sms is None:
        return
    sms.send()


@capp.task
def send_ws_message(msg):
    """
    Send message to centrifugo.
    "msg" is dict that should contain "type" and
    may contain any other fields.
    """
    print(msg)
    url = settings.CENTRIFUGO_LOCAL_URL
    api_key = settings.CENTRIFUGO_API_KEY
    client = Client(url, api_key=api_key, timeout=3)
    client.publish(msg['channel'], msg)

@capp.task
def send_telegram_bot_message(msg):
    token = settings.TELEGRAM_BOT_TOKEN
    bot = TeleBot(token)
    bot.send_message()


@periodic_task(run_every=crontab(minute=0, hour=0))
def clear_old_logs():
    """
    Removes old rows from Log
    """
    Logging = apps.get_model('web', 'Logging')
    Logging.objects.filter(
        dt__lt=timezone.now() - datetime.timedelta(days=15)).delete()


@periodic_task(run_every=crontab(minute='*/15'))
def restart_wserver():
    sudo('systemctl restart wserver')


@periodic_task(run_every=crontab(minute=10, hour=0))
def clear_expired_downloads():
    DownloadFile = apps.get_model('web', 'DownloadFile')
    qs = DownloadFile.objects.filter(expire_at__isnull=False)
    qs = qs.filter(expire_at__lt=timezone.now())

    # Delete objects one by one to run post_delete signal
    for df in qs:
        df.delete()


@capp.task
def copy_s3_object(old_key, new_key):
    """
    Copy object with old key to new key
    """
    client = get_s3_client()
    client.copy_object(
        ACL='public-read',
        Bucket=settings.AWS_STORAGE_BUCKET_NAME,
        CopySource={
            'Bucket': settings.AWS_STORAGE_BUCKET_NAME,
            'Key': old_key
        },
        Key=new_key
    )


@periodic_task(run_every=crontab(hour='*'))
def check_free_period():
    FreePeriod = apps.get_model('account', 'FreePeriod')
    FreePeriod.objects.filter(date_to__lt=timezone.now()).update(used=True)
