# Generated by Django 2.0.4 on 2021-02-08 10:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='email',
            name='code',
            field=models.CharField(choices=[('change_email', 'изменение e-mail в личном кабинете'), ('download_book_auth', 'загрузка книги зарегистрированным пользователем'), ('download_book_notauth', 'загрузка книги посетителем'), ('get_subscription_info', 'выслать информацию по подписке'), ('new_broker_desc_created', 'добавлен новый пункт для брокера'), ('new_broker_review_created', 'добавлен новый отзыв для брокера'), ('new_exchange_desc_created', 'добавлен новый пункт для биржи криптовалют'), ('new_exchange_review_created', 'добавлен новый отзыв для биржи криптовалют'), ('pay_product', 'платеж за товар'), ('pay_subscription', 'платеж за подписку'), ('pay_tariff', 'платеж за тариф'), ('reset_password', 'восстановление пароля'), ('signup', 'регистрация'), ('signup_confirm', 'подтверждение регистрации')], max_length=50),
        ),
    ]
