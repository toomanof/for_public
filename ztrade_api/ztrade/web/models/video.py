from django.db.models import Manager
from django.db.models import Model
from django.contrib import admin
from django.db.models import CharField
from django.db.models import IntegerField


class VideoManager(Manager):
    pass


class Video(Model):
    name = CharField('название', max_length=255)
    link = CharField('youtube link', max_length=255)
    width = IntegerField('ширина', null=True, default=None,
                         blank=True, help_text='Задать жестко')
    sort = IntegerField('сортировка', default=0)

    objects = VideoManager()

    class Meta:
        app_label = 'web'
        verbose_name = 'видео'
        verbose_name_plural = 'видео'
        ordering = ['sort']

    def __str__(self):
        return self.name


@admin.register(Video)
class VideoAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'link',
        'width',
        'sort'
    )
