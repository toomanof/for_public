from django.contrib import admin
from django.db.models import Model
from django.db.models import CharField
from django.db.models import TextField
from django.db.models import DateTimeField


class Logging(Model):
    http_code = CharField(max_length=10, default='')
    level = CharField(max_length=8, default='')
    logger_name = CharField(max_length=20, default='')
    module = CharField(max_length=100, default='')
    thread = CharField(max_length=50, default='')
    thread_name = CharField(max_length=100, default='')
    exc_info = CharField(max_length=255, default='')
    stack_info = TextField(default='')
    message = TextField(default='')
    dt = DateTimeField(verbose_name='date', auto_now_add=True)

    class Meta:
        app_label = 'web'
        verbose_name = 'logging'
        verbose_name_plural = 'logging'
        ordering = ['-dt']

    def __str__(self):
        return str(self.dt)


@admin.register(Logging)
class LoggingAdmin(admin.ModelAdmin):
    list_display = (
        'dt',
        'level',
        'http_code',
        'logger_name',
        'module',
        'exc_info',
        'message'
    )
    list_filter = ('level', 'logger_name')

    def has_add_permission(self, request, obj=None):
        return False

    def get_model_perms(self, request):
        return {}
