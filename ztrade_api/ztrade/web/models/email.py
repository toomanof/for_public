import os
from django.contrib import admin
from django.conf import settings
from django.db.models import Model
from django.db.models import CharField
from django.db.models import TextField
from django.template import Template


class Email(Model):
    CHANGE_EMAIL = 'change_email'
    DOWNLOAD_BOOK_AUTH = 'download_book_auth'
    DOWNLOAD_BOOK_NOTAUTH = 'download_book_notauth'
    GET_SUBSCRIPTION_INFO = 'get_subscription_info'
    NEW_BROKER_DESC_CREATED = 'new_broker_desc_created'
    NEW_BROKER_REVIEW_CREATED = 'new_broker_review_created'
    NEW_EXCHANGE_DESC_CREATED = 'new_exchange_desc_created'
    NEW_EXCHANGE_REVIEW_CREATED = 'new_exchange_review_created'
    PAY_PRODUCT = 'pay_product'
    PAY_SUBSCRIPTION = 'pay_subscription'
    PAY_TARIFF = 'pay_tariff'
    RESET_PASSWORD = 'reset_password'
    SIGNUP = 'signup'
    SIGNUP_CONFIRM = 'signup_confirm'
    GET_SUBSCRIPTION_ACTIVE_INFO = 'get_subscription_active_info'

    CODE_CHOICES = (
        (CHANGE_EMAIL, 'изменение e-mail в личном кабинете'),
        (DOWNLOAD_BOOK_AUTH, 'загрузка книги зарегистрированным пользователем'),
        (DOWNLOAD_BOOK_NOTAUTH, 'загрузка книги посетителем'),
        (GET_SUBSCRIPTION_INFO, 'выслать информацию по подписке'),
        (NEW_BROKER_DESC_CREATED, 'добавлен новый пункт для брокера'),
        (NEW_BROKER_REVIEW_CREATED, 'добавлен новый отзыв для брокера'),
        (NEW_EXCHANGE_DESC_CREATED, 'добавлен новый пункт для биржи криптовалют'),
        (NEW_EXCHANGE_REVIEW_CREATED, 'добавлен новый отзыв для биржи криптовалют'),
        (PAY_PRODUCT, 'платеж за товар'),
        (PAY_SUBSCRIPTION, 'платеж за подписку'),
        (PAY_TARIFF, 'платеж за тариф'),
        (RESET_PASSWORD, 'восстановление пароля'),
        (SIGNUP, 'регистрация'),
        (SIGNUP_CONFIRM, 'подтверждение регистрации'),
        (GET_SUBSCRIPTION_ACTIVE_INFO, 'выслать информацию по активации подписке')
    )

    code = CharField(max_length=50, choices=CODE_CHOICES)
    name = CharField(max_length=255)
    subject = CharField(max_length=255)
    variables = TextField(default='', blank=True)
    txt = TextField(default='', blank=True)
    html = TextField(default='', blank=True)

    def __str__(self):
        return self.name

    class Meta:
        app_label = 'web'
        verbose_name = 'email'
        verbose_name_plural = 'emails'
        ordering = ['code']
        db_table = 'email'

    def save(self, *args, **kwargs):
        if not self.pk:
            if not self.subject:
                self.subject = self.name
        super().save(*args, **kwargs)

    @property
    def folder(self):
        return os.path.join(
            settings.BASE_DIR, 'ztrade/ztrade/templates/web/emails/')

    def wrap_with_premailer(self, str_data):
        s = '{% load premailer %}{% premailer %}'
        s += str_data
        s += '{% endpremailer %}'
        return s

    def get_default_txt_template(self):
        """
        Returns Django Template instance.
        The name of file should be equal to code.
        Can be overriden by "txt" TextField
        """
        file_path = '{}{}.txt'.format(self.folder, self.code)
        f = open(file_path, 'r')
        data = f.read()
        f.close()
        return Template(self.wrap_with_premailer(data))

    def get_default_html_template(self):
        """
        Returns Django Template instance.
        The name of file should be equal to code.
        Can be overriden by "html" TextField
        """
        file_path = '{}{}.html'.format(self.folder, self.code)
        f = open(file_path, 'r')
        data = f.read()
        f.close()
        return Template(self.wrap_with_premailer(data))

    def get_txt_template(self):
        """
        Set content to "txt" field and override default txt template
        """
        if not self.txt:
            return self.get_default_txt_template()

        try:
            t = Template(self.wrap_with_premailer(self.txt))
        except Exception:
            t = self.get_default_txt_template()
        return t

    def get_html_template(self):
        """
        Set content to "html" field and override default html template
        """
        if not self.html:
            return self.get_default_html_template()

        try:
            t = Template(self.wrap_with_premailer(self.html))
        except Exception:
            t = self.get_default_html_template()
        return t


@admin.register(Email)
class EmailAdmin(admin.ModelAdmin):
    list_display = (
        'code',
        'name',
        'subject',
        'txt',
        'html'
    )
    list_editable = (
        'subject',
    )
    exclude = ('txt',)

#    def has_delete_permission(self, request, obj=None):
#        return False

#   def has_add_permission(self, request):
#        return False
