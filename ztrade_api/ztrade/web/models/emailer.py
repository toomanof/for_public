import copy
import datetime
from django.utils import timezone
from django.core.mail import EmailMultiAlternatives
from django.contrib import admin
from django.conf import settings
from django.apps import apps
from django.core.exceptions import ValidationError
from django.db.models import Manager
from django.db.models import Model
from django.db.models import EmailField
from django.db.models import CharField
from django.db.models import TextField
from django.db.models import BooleanField
from django.db.models import DateTimeField
from django.template import Context


class EmailerManager(Manager):
    def get_common_context(self):
        return {'domain': settings.FRONTEND_DOMAIN}

    def signup_context(self, **kwargs):
        d = self.get_common_context()
        return d

    def process_email(self, code, **kwargs):
        """
        kwargs should have user object or
        email (email_to).
        """
        Email = apps.get_model('web', 'Email')
        try:
            email = Email.objects.get(code=code)
        except Email.DoesNotExist:
            raise ValidationError('Wrong code')

        txt = email.get_txt_template()
        html = email.get_html_template()

        d = copy.deepcopy(kwargs)
        addtitional_context = getattr(
            self, '{}_context'.format(email.code), None)
        if addtitional_context is not None:
            d.update(addtitional_context(**kwargs))

        if 'user' in kwargs:
            email_to = kwargs['user'].email
        else:
            email_to = kwargs['email']

        html_template = html.render(Context(d))
        if code == Email.GET_SUBSCRIPTION_ACTIVE_INFO:
            email_to = settings.TRELLO_FROM_EMAIL
            html_template = txt.render(Context(d))

        em = self.model(
            email_to=email_to,
            email_from=settings.DEFAULT_FROM_EMAIL,
            subject=email.subject,
            html=html_template,
            txt=txt.render(Context(d))
        )
        em.save()

        if not settings.TESTING:
            from web.tasks import send_email
            send_email.delay(em.id)
        return em


class Emailer(Model):
    email_to = EmailField(max_length=100)
    email_from = EmailField(max_length=100)
    subject = CharField(max_length=255)
    txt = TextField()
    html = TextField()
    report = TextField(default='')
    is_sent = BooleanField(default=False)
    result = BooleanField(default=False)
    created_at = DateTimeField(auto_now_add=True)
    sent_at = DateTimeField(null=True, blank=True)

    objects = EmailerManager()

    def __str__(self):
        return str(self.id)

    class Meta:
        app_label = 'web'
        verbose_name = 'email report'
        verbose_name_plural = 'email reports'
        ordering = ['-created_at']
        db_table = 'emailer'

    def send(self):
        msg = EmailMultiAlternatives(
            self.subject,
            self.txt,
            self.email_from,
            [self.email_to]
        )
        msg.attach_alternative(self.html, 'text/html')

        try:
            msg.send()
            self.result = True
            self.report = 'OK'
            self.sent_at = timezone.now()
            self.is_sent = True
        except Exception as e:
            self.result = False
            self.report = 'send error:' + str(e)

        self.save()


@admin.register(Emailer)
class EmailerAdmin(admin.ModelAdmin):
    list_filter = ('result',)
    list_display = (
        'created_at',
        'sent_at',
        'email_to',
        'email_from',
        'subject',
        'result',
        'is_sent'
    )
    exclude = ('is_sent', 'result')
    readonly_fields = ('report',)

    def changelist_view(self, request, extra_context=None):
        # Delete rows that older than two weeks
        dt = datetime.date.today() - datetime.timedelta(days=14)

        Emailer.objects.filter(is_sent=True, sent_at__lt=dt).delete()
        return super().changelist_view(request, extra_context)

    def has_add_permission(self, request):
        return False
