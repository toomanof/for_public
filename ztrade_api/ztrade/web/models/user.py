import datetime
import random
import re
from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import BaseUserManager
from django.contrib.auth.models import Permission
from django.contrib.auth.models import PermissionsMixin
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.apps import apps
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.db.models import ForeignKey
from django.db.models import CharField
from django.db.models import EmailField
from django.db.models import BooleanField
from django.db.models import DateTimeField
from django.db.models import IntegerField
from django.db.models import ManyToManyField
from django.db.models import CASCADE
from web.services.helpers import get_guid
from web.services.helpers import randstring

from web.constants import EMAIL


def get_email_code():
    return str(random.randint(111111, 999999))


class UserManager(BaseUserManager):
    def create_user(self, email, password):
        if not email:
            raise ValueError('Please input email.')
        user = self.model(email=email)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password):
        user = self.create_user(email, password=password)
        user.type = User.ADMIN
        user.joined_at = timezone.now()
        user.is_superuser = True
        user.save(using=self._db)
        return user

    def generate_uid(self):
        """
        Create unique user id.
        """
        while True:
            uid = random.randint(10000, 999999)
            if User.objects.filter(uid=uid).exists():
                continue
            return uid

    def check_token(self, key):
        Token = apps.get_model('web', 'Token')
        token = Token.objects.filter(
            key=key, expire_at__gt=timezone.now()).first()
        return token is not None

    def strip_phone(self, value):
        """
        Remove all symbols except numbers
        """
        if not value:
            return value
        return re.sub(r'[\D]', '', value)


class User(PermissionsMixin, AbstractBaseUser):
    """
    is_superuser added via PermissionsMixin
    """
    ADMIN = 'admin'
    CONTENT_ADMIN = 'content_admin'
    SIGNAL_ADMIN = 'signal_admin'
    USER = 'user'

    TYPE_CHOICES = (
        (ADMIN, _('Админ')),  # is_superuser = True
        (CONTENT_ADMIN, _('Админ контента')),
        (SIGNAL_ADMIN, _('Админ сигналов')),
        (USER, 'Пользователь'),
    )

    guid = CharField(max_length=32, default=get_guid)
    uid = IntegerField(unique=True, null=True, default=None, blank=True)
    country = ForeignKey('Country', on_delete=CASCADE, null=True, default=None,
                         blank=True, verbose_name='страна')
    type = CharField('тип', max_length=20, null=True, default=None, blank=True,
                     choices=TYPE_CHOICES)
    email = EmailField(
        max_length=255, null=True, unique=True, default=None)
    first_name = CharField('имя', max_length=100, null=True, default=None,
                           blank=True)
    last_name = CharField('фамилия', max_length=100, null=True,
                          default=None, blank=True)
    phone = CharField('телефон', max_length=50, null=True,
                      default=None, blank=True)
    phone_num = CharField(
        'телефон (только цифры)', max_length=50, null=True,
        default=None, blank=True,
        help_text='Indexed automatically', )
    city = CharField('город', max_length=255, null=True,
                     default=None, blank=True)
    joined_at = DateTimeField('дата регистрации', blank=True,
                              default=None, null=True)
    signup_email_code = CharField(max_length=10, default=get_email_code)
    reset_password_code = CharField(max_length=50, null=True, default=None,
                                    blank=True)
    change_email_code = CharField(
        max_length=10, null=True, blank=True, default=None)
    last_access_at = DateTimeField(null=True, default=None, blank=True)
    new_email = EmailField(
        max_length=255, null=True, default=None)
    new_email_token = CharField(
        max_length=50, null=True, default=None, blank=True)
    new_email_expire_at = DateTimeField(
        null=True, default=None, blank=True)
    api_key = CharField(max_length=32, default=get_guid)
    is_active = BooleanField('активен', default=True)
    permissions = ManyToManyField(Permission, related_name='users',
                                  verbose_name='доступ')
    is_receive_news = BooleanField('получать новости', default=True)
    is_receive_notifications = BooleanField(
        'получать уведомления', default=True)
    is_receive_email = BooleanField(
        'получать общие почтовые сообщения', default=True)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name_plural = 'Users'
        ordering = ['email']
        app_label = 'web'

    def __str__(self):
        return self.email

    def has_perm(self, perm, obj=None):
        """
        Admin permissions for different types of admins.
        """
        if self.type == User.ADMIN:
            return True

        # Signal admin can only add and change signals
        if self.type == User.SIGNAL_ADMIN:
            if perm in ('sgn.add_signal', 'sgn.change_signal'):
                if obj is not None:
                    if obj.owner != self:
                        return False
                return True
            return False

        tup = perm.split('.')
        if len(tup) != 2:
            return False

        app_label, codename = tup

        qs = self.permissions.filter(
            codename=codename, content_type__app_label=app_label)
        if qs.exists():
            return True
        return False

    def has_perms(self, perm_list, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    @property
    def is_staff(self):
        """
        User is able to login to Django admin
        """
        l = (self.ADMIN, self.CONTENT_ADMIN, self.SIGNAL_ADMIN)
        if self.type in l:
            return True
        return False

    @property
    def is_signup_completed(self):
        if self.uid is not None:
            return True
        return False

    @property
    def uid_str(self):
        if self.uid is None:
            return '-'
        return str(self.uid).zfill(6)

    @property
    def full_name(self):
        return f'{self.first_name} {self.last_name}'

    @property
    def forex_news_filter(self):
        ForexNewsFilter = apps.get_model('account', 'ForexNewsFilter')

        filter = ForexNewsFilter.objects.filter(user_id=self.id).first()
        if filter is None:
            filter = ForexNewsFilter.objects.create(user_id=self.id)
        return filter

    @property
    def forex_news_qs(self):
        """
        Returns ForexNews queryset based on user's filter
        """
        ForexCategory = apps.get_model('account', 'ForexCategory')
        ForexSymbol = apps.get_model('account', 'ForexSymbol')
        ForexNews = apps.get_model('account', 'ForexNews')

        f = self.forex_news_filter

        if f.is_all_categories:
            qs = ForexCategory.objects.all()
        else:
            qs = f.forex_categories
        cat_ids = qs.values_list('id', flat=True)

        if f.is_all_symbols:
            qs = ForexSymbol.objects.all()
        else:
            qs = f.forex_symbols
        sym_ids = qs.values_list('id', flat=True)

        return ForexNews.objects.filter(
            forex_categories__id__in=cat_ids,
            forex_symbols__id__in=sym_ids).distinct().order_by('-dt')

    @property
    def is_signal_paid(self):
        """
        Returns bool.
        Does user have active paid plan.

        Currently for testing True for admin.
        """
        if self.type == User.ADMIN:
            return True
        return False

    @property
    def is_subscription_paid(self):
        """
        Returns bool.
        Does user have any active subscription
        """
        SubscriptionUser = apps.get_model('account', 'SubscriptionUser')

        today = datetime.date.today()
        qs = SubscriptionUser.objects.filter(
            user_id=self.id,
            is_paid=True,
            date_from__lte=today,
            date_to__gte=today)
        return qs.exists()

    def get_signals_filter(self, market_id):
        """
        User's signals filter for particular market.
        Create it automatically if it not exists.
        """
        SignalFilter = apps.get_model('sgn', 'SignalFilter')

        filter = SignalFilter.objects.filter(
            user_id=self.id, market_id=market_id).first()
        if filter is None:
            filter = SignalFilter.objects.create(
                user_id=self.id, market_id=market_id)
        return filter

    def is_market_paid(self, market_id):
        """
        Returns bool.
        Is user paid for this market
        """
        TariffUser = apps.get_model('sgn', 'TariffUser')

        exists_tariff_user = TariffUser.objects.filter(market_id=market_id, user_id=self.id,
                                                       date_to__gt=timezone.now(),
                                                       is_paid=True).exists()
        return exists_tariff_user

    def is_signal_notify(self, signal_id):
        """
        Returns bool.
        Notify user about signal.
        """
        Signal = apps.get_model('sgn', 'Signal')
        signal = Signal.objects.filter(id=signal_id).first()
        if signal is not None:
            f = self.get_signals_filter(signal.market_id)

            if f.is_all_symbols:
                return True

            if f.symbols.filter(id=signal.symbol_id).exists():
                return True

        return False

    def get_paid_symbol_ids(self, market_id):
        """
        Returns list of symbol_id for that user has active tariff
        """
        Symbol = apps.get_model('sgn', 'Symbol')
        Tariff = apps.get_model('sgn', 'Tariff')
        TariffUser = apps.get_model('sgn', 'TariffUser')
        FreePeriod = apps.get_model('account', 'FreePeriod')
        free_period_obj = FreePeriod.objects.filter(user_id=self.id).first()
        kwargs_tariff = {
            'market_id': market_id,
            'user_id': self.id,
            'date_to__gt': timezone.now(),
            'is_paid': True
        }

        if free_period_obj and free_period_obj.is_active:
            tariff_qs = Tariff.objects.all()
        else:
            tariff_ids = TariffUser.objects \
                .filter(**kwargs_tariff) \
                .values_list('tariff_id', flat=True)
            tariff_qs = Tariff.objects.filter(id__in=tariff_ids)

        ids = []
        for tariff in tariff_qs:
            if tariff.symbols.count() == 0:
                qs = Symbol.objects.filter(market_id=tariff.market_id)
            else:
                qs = tariff.symbols.all()

            ids += list(qs.values_list('id', flat=True))
        return list(set(ids))

    def get_signals_qs(self, market_id):
        """
        Returns Signal queryset for particular market
        based on user's filter for particular market.
        """
        Signal = apps.get_model('sgn', 'Signal')
        SignalHide = apps.get_model('sgn', 'SignalHide')

        ex_ids = SignalHide.objects \
            .filter(user_id=self.id) \
            .values_list('signal_id', flat=True)

        qs = Signal.objects.exclude(id__in=ex_ids)
        qs = qs \
            .select_related('symbol', 'signal_type') \
            .filter(market_id=market_id)

        f = self.get_signals_filter(market_id)

        if f.is_all_symbols:
            return qs

        ids = f.symbols.values_list('id', flat=True)
        return qs.filter(symbol_id__in=ids)

    def get_token(self):
        Token = apps.get_model('web', 'Token')
        token = Token.objects.filter(
            user=self, expire_at__gt=timezone.now()).first()
        if token:
            return token
        return Token.objects.create(self)

    def update_token(self):
        now = timezone.now()
        mins = settings.TOKEN_EXPIRE_MINUTES
        t = self.get_token()
        t.expire_at = now + datetime.timedelta(minutes=mins)
        t.save()
        return t

    def signup(self):
        """
        1) Send email to user
        2) Send sms to user
        """
        Emailer = apps.get_model('web', 'Emailer')
        Email = apps.get_model('web', 'Email')
        SMS = apps.get_model('web', 'SMS')

        Emailer.objects.process_email(Email.SIGNUP, user=self)
        sms = SMS.objects.create_sms(SMS.SIGNUP, self.phone_num)
        return sms

    def signup_confirm(self):
        """
        Complete signup
        """
        Emailer = apps.get_model('web', 'Emailer')
        Email = apps.get_model('web', 'Email')

        password = randstring(7)

        self.uid = User.objects.generate_uid()
        self.joined_at = timezone.now()
        self.set_password(password)
        self.save()

        Emailer.objects.process_email(Email.SIGNUP_CONFIRM, user=self,
                                      password=password)

    def change_email_send_code(self, email):
        Emailer = apps.get_model('web', 'Emailer')
        Email = apps.get_model('web', 'Email')
        code = str(random.randint(11111111, 99999999))
        self.change_email_code = code
        self.save()
        Emailer.objects.process_email(
            Email.CHANGE_EMAIL, user=self, email=email, email_code=code)

    def reset_password_send_code(self, type):
        Emailer = apps.get_model('web', 'Emailer')
        Email = apps.get_model('web', 'Email')
        SMS = apps.get_model('web', 'SMS')

        code = str(random.randint(11111111, 99999999))
        self.reset_password_code = code
        self.save()

        if type == EMAIL:
            Emailer.objects.process_email(Email.RESET_PASSWORD, user=self, uid=self.uid)
        else:
            SMS.objects.create_sms(SMS.RESET_PASSWORD,
                                   self.phone_num, sms_code=code)

    def change_email_request(self, new_email):
        Emailer = apps.get_model('web', 'Emailer')
        Email = apps.get_model('web', 'Email')

        self.new_email = new_email
        self.new_email_token = get_guid()
        self.new_email_expire_at = \
            timezone.now() + datetime.timedelta(hours=1)
        self.save()
        Emailer.objects.process_email(Email.CHANGE_EMAIL, user=self)

    def change_email_confirm(self, token):
        """
        Returns bool.
        """
        if self.new_email_token != token:
            return False

        if timezone.now() > self.new_email_expire_at:
            return False

        self.email = self.new_email
        self.new_email = None
        self.new_email_token = None
        self.new_email_expire_at = None
        self.save()
        return True

    def get_change_email_link(self):
        dic = dict(
            url=self.frontend_url,
            id=self.guid,
            token=self.new_email_token
        )
        link = '{url}/change_email_confirm?id={id}&token={token}'
        return link.format(**dic)

    @property
    def new_email_expired(self):
        if not self.new_email_expire_at:
            return True
        return timezone.now() > self.new_email_expire_at

    def save(self, *args, **kwargs):
        if self.email and not self.email.islower():
            self.email = self.email.lower()

        if self.type == self.ADMIN and self.is_superuser is False:
            self.is_superuser = True

        if self.type != self.ADMIN and self.is_superuser is True:
            self.is_superuser = False

        if self.type is None:
            self.type = self.USER

        self.phone_num = User.objects.strip_phone(self.phone)
        super().save(*args, **kwargs)


@receiver(post_save, sender=User)
def create_token(sender, instance, created, **kwargs):
    if created:
        Token = apps.get_model('web', 'Token')
        Token.objects.create(user=instance)
