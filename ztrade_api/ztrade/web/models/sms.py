import copy
import os
import random
import re
from django.db.models import Manager
from django.contrib import admin
from django.conf import settings
from django.template.loader import get_template
from django.db.models import Model
from django.db.models import CharField
from django.db.models import DateTimeField
from django.db.models import BooleanField

from web.services.helpers import get_guid
from web.services.smsc_api import SMSC


def get_sms_code():
    return str(random.randint(111111, 999999))


class SMSManager(Manager):
    def get_template(self, code):
        """
        Returns Django Template instance.
        The name of file should be equal to code.
        """
        folder = os.path.join(
            settings.BASE_DIR, 'ztrade/ztrade/templates/web/sms/')

        file_path = '{}{}.html'.format(folder, code)
        return get_template(file_path)

    def create_sms(self, code, phone, sms_code=None, **kwargs):
        """
        Create and send SMS by celery
        """
        if sms_code is None:
            sms_code = get_sms_code()

        d = copy.deepcopy(kwargs)
        d['sms_code'] = sms_code

        tpl = self.get_template(code)
        message = tpl.render(d)

        sms = self.model.objects.create(
            code=code,
            phone=phone,
            message=message,
            sms_code=sms_code
        )

        if not settings.TESTING:
            from web.tasks import send_sms
            send_sms.delay(sms.id)

        return sms


class SMS(Model):
    RESET_PASSWORD = 'reset_password'
    SIGNUP = 'signup'
    LOGIN = 'login'
    CHANGE_PHONE = 'change_phone'
    CUSTOM = 'custom'

    CODE_CHOICES = (
        (RESET_PASSWORD, 'Восстановление пароля'),
        (SIGNUP, 'Регистрация'),
        (LOGIN, 'Вход в кабинет'),
        (CHANGE_PHONE, 'Изменение телефона в личном кабинете'),
        (CUSTOM, 'Другое'),
    )

    guid = CharField(max_length=50, default=get_guid)
    code = CharField('внутренний код', max_length=50, choices=CODE_CHOICES)
    sms_code = CharField('смс код', max_length=10, null=True,
                         default=None, blank=True)
    phone = CharField('телефон', max_length=50)
    message = CharField('сообщение', max_length=1000)
    created_at = DateTimeField('дата', auto_now_add=True)
    is_sent = BooleanField('отправлено', default=False)

    objects = SMSManager()

    def __str__(self):
        return str(self.created_at)

    class Meta:
        app_label = 'web'
        verbose_name = 'SMS'
        verbose_name_plural = 'SMS'
        ordering = ['-created_at']

    @property
    def converted_phone(self):
        """
        Convert user's input phone to phone that can be used
        to send SMS
        Replace all symbols except numbers.
        """
        return re.sub(r'[\D]', '', self.phone)

    def send(self):
        smsc = SMSC()
        smsc.send_sms(self.converted_phone, self.message)
        self.is_sent = True
        self.save()


@admin.register(SMS)
class SMSAdmin(admin.ModelAdmin):
    list_display = (
        'created_at',
        'code',
        'guid',
        'sms_code',
        'phone',
        'message',
        'is_sent'
    )
