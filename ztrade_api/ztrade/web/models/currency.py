from django.db.models import Manager
from django.db.models import Model
from django.contrib import admin
from django.db.models import CharField
from django.db.models import IntegerField


class CurrencyManager(Manager):
    pass


class Currency(Model):
    code = CharField(max_length=5)
    name = CharField(max_length=100)
    sort = IntegerField(default=0)

    objects = CurrencyManager()

    class Meta:
        app_label = 'web'
        verbose_name = 'валюта'
        verbose_name_plural = 'валюты'
        ordering = ['sort']

    def __str__(self):
        return self.name


class CurrencyAdmin(admin.ModelAdmin):
    list_display = ('code', 'name', 'sort')
