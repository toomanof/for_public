from django.db.models import Manager
from django.db.models import Model
from django.dispatch import receiver
from django.db.models.signals import post_delete
from django.contrib import admin
from django.conf import settings
from django.db.models import CharField
from django.db.models import FileField
from django.db.models import DateTimeField

from web.constants import PRODUCT
from web.constants import SUBSCRIPTION
from web.services.helpers import get_guid


def upload_to(instance, filename):
    ext = filename.split('.')[-1]
    filename = '{}.{}'.format(get_guid(), ext)

    pth = 'download/'

    if instance.type is not None:
        pth += '{}/'.format(type)

    pth += filename
    return pth


class DownloadFileManager(Manager):
    pass


class DownloadFile(Model):
    """
    Product files will be copied to this model automatically
    Subscription files are uploaded by admin.
    Expired files automatically deleted by celery task
    """
    TYPE_CHOICES = (
        (PRODUCT, 'товар'),
        (SUBSCRIPTION, 'подписка')
    )

    name = CharField('название', max_length=255)
    type = CharField('тип', max_length=15, choices=TYPE_CHOICES, null=True,
                     default=None, blank=True)
    file = FileField('файл', upload_to=upload_to)
    expire_at = DateTimeField(
        'годен до', null=True, default=None, blank=True,
        help_text='Если не задать, то без срока действия')

    objects = DownloadFileManager()

    class Meta:
        app_label = 'web'
        verbose_name = 'загрузка файла'
        verbose_name_plural = 'загрузка файлов'
        ordering = ['expire_at']

    def __str__(self):
        return self.name

    @property
    def file_url(self):
        if not self.file or not self.file.name:
            return None
        return '{}/{}'.format(settings.BUCKET_URL, self.file.name)


@receiver(post_delete, sender=DownloadFile)
def delete_file(sender, instance, **kwargs):
    if instance.file:
        instance.file.delete()


@admin.register(DownloadFile)
class DownloadFileAdmin(admin.ModelAdmin):
    list_filter = ('type',)
    list_display = (
        'name',
        'type',
        'file',
        'expire_at'
    )
