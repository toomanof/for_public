from django.contrib import admin
from django.db.models import Manager
from django.db.models import Model
from django.db.models import CharField
from django.db.models import SlugField
from django.db.models import TextField
from django.db.models import DateTimeField

from web.services.helpers import get_guid


class TranslationManager(Manager):
    def get_dict(self, lang_code):
        attr = 'value_' + lang_code
        qs = self.model.objects.values_list('code', attr)
        d = {}
        for obj in qs:
            d[obj[0]] = obj[1]
        return d


class Translation(Model):
    guid = CharField(
        max_length=50, default=get_guid, unique=True)
    code = SlugField('код', max_length=10, unique=True)
    component = CharField('компонент', max_length=255, null=True,
                          default=None, blank=True)
    comment = CharField('комментарий', max_length=255, null=True,
                        default=None, blank=True)
    value_en = TextField('en', blank=True, default='')
    value_ru = TextField('значение', blank=True, default='')
    created_at = DateTimeField('дата', auto_now_add=True)

    objects = TranslationManager()

    class Meta:
        app_label = 'web'
        ordering = ['code']
        verbose_name = 'Надпись'
        verbose_name_plural = 'Надписи'

    def __str__(self):
        return self.code


@admin.register(Translation)
class TranslationAdmin(admin.ModelAdmin):
    actions = None
    list_display = (
        'code',
        'value_ru',
        'comment'
    )

    search_fields = (
        'code',
        'value_ru',
        'comment'
    )

    list_display_links = ('code',)
    exclude = ('value_en', 'guid', 'component')
    readonly_fields = ('code', 'comment')
    list_editable = ('value_ru',)

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False
