import datetime
from django.db.models import Manager
from django.db.models import Model
from django.contrib import admin
from django.db.models import CharField
from django.db.models import IntegerField


class TimezoneManager(Manager):
    pass


class Timezone(Model):
    name = CharField(max_length=255, default=None, blank=True, null=True)
    full_name = CharField(max_length=255)
    offset = CharField(max_length=6)
    sort = IntegerField(default=0)

    objects = TimezoneManager()

    class Meta:
        app_label = 'web'
        verbose_name = 'timezone'
        verbose_name_plural = 'timezones'
        ordering = ['sort']

    def __str__(self):
        return self.name

    def convert_dt(self, dt):
        """
        Convert dt accordingly to timezone
        """
        is_minus = True if self.offset.startswith('-') else False
        offset = self.offset.lstrip('-').lstrip('+')

        hours, minutes = offset.split(':')
        hours = int(hours)
        minutes = int(minutes)

        delta = datetime.timedelta(hours=hours, minutes=minutes)

        if is_minus:
            return dt - delta
        return dt + delta


@admin.register(Timezone)
class TimezoneAdmin(admin.ModelAdmin):
    actions = None
    list_display = ('name', 'offset', 'full_name', 'sort')
    search_fields = ('name',)
    readonly_fields = ('offset',)
