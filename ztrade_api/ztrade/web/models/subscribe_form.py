from django.db.models import Manager
from django.db.models import Model
from django.contrib import admin
from django.db.models import EmailField
from django.db.models import CharField
from django.db.models import DateTimeField


class SubscribeFormManager(Manager):
    pass


class SubscribeForm(Model):
    HOW_INVEST = 'how_invest'

    TYPE_CHOICES = (
        (HOW_INVEST, 'Как стать инвестором'),
    )

    email = EmailField('email', max_length=100)
    type = CharField('тип', max_length=50, choices=TYPE_CHOICES)
    created_at = DateTimeField(auto_now_add=True)

    objects = SubscribeFormManager()

    class Meta:
        app_label = 'web'
        verbose_name = 'подписка'
        verbose_name_plural = 'подписки'
        ordering = ['-created_at']

    def __str__(self):
        return self.email


# Used previously for investment page
# @admin.register(SubscribeForm)
class SubscribeFormAdmin(admin.ModelAdmin):
    list_display = (
        'created_at',
        'type',
        'email'
    )
