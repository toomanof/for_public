from django.db.models import Manager
from django.db.models import Model
from django.contrib import admin
from django.db.models import CharField
from django.db.models import IntegerField


class CountryManager(Manager):
    pass


class Country(Model):
    code = CharField('код', max_length=3, unique=True)
    name = CharField('название', max_length=50, unique=True)
    class_name = CharField('css класс', max_length=20,
                           null=True, default=None, blank=True)
    phone_prefix = CharField('префикс телефона', max_length=50)
    sort = IntegerField('сортировка', default=0)

    objects = CountryManager()

    class Meta:
        app_label = 'web'
        verbose_name = 'страна'
        verbose_name_plural = 'страны'
        ordering = ['sort', 'code']

    def __str__(self):
        return self.name


@admin.register(Country)
class CountryAdmin(admin.ModelAdmin):
    actions = None
    list_display = (
        'code',
        'name',
        'phone_prefix',
        'class_name',
        'sort'
    )
    list_editable = ('sort',)
    search_fields = ('code', 'name')
    readonly_fields = ('code', 'class_name')
    exclude = ('phone_prefix',)

    def has_delete_permission(self, request, obj=None):
        return False
