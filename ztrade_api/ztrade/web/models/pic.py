from django.db.models import Manager
from django.db.models import Model
from django.contrib import admin
from django.conf import settings
from django.db.models import CharField
from django.db.models import ImageField
from django.db.models import IntegerField


class PicManager(Manager):
    pass


class Pic(Model):
    name = CharField('название', max_length=255)
    pic = ImageField('файл', upload_to='images/picfile', null=True,
                     default=None, blank=True)
    sort = IntegerField(default=0)

    objects = PicManager()

    class Meta:
        app_label = 'web'
        verbose_name = 'изображение'
        verbose_name_plural = 'изображения'
        ordering = ['sort']

    def __str__(self):
        return str(self.id)

    @property
    def pic_url(self):
        if not self.pic or not self.pic.name:
            return None
        return '{}/{}'.format(settings.BUCKET_URL, self.pic.name)


@admin.register(Pic)
class PicAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'name',
        'pic',
        'pic_url',
        'sort'
    )
    list_editable = ('sort',)
