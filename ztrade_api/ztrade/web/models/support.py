from django.db.models import Manager
from django.db.models import Model
from django.contrib import admin
from django.db.models import CharField
from django.db.models import TextField
from django.db.models import DateTimeField
from django.db.models import BooleanField


class SupportManager(Manager):
    pass


class Support(Model):
    first_name = CharField('имя', max_length=100)
    last_name = CharField('фамилия', max_length=100)
    email = CharField('email', max_length=100)
    question = TextField('вопрос')
    created_at = DateTimeField(auto_now_add=True)
    is_read = BooleanField('обработан', default=False)

    objects = SupportManager()

    class Meta:
        app_label = 'web'
        verbose_name = 'задать вопрос'
        verbose_name_plural = 'задать вопрос'
        ordering = ['-created_at']

    def __str__(self):
        return str(self.created_at)


@admin.register(Support)
class SupportAdmin(admin.ModelAdmin):
    list_display = (
        'created_at',
        'first_name',
        'last_name',
        'email',
        'question',
        'is_read'
    )
    list_editable = ('is_read',)
    list_filter = ('is_read',)
