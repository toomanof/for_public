from django.db.models import Manager
from django.db.models import Model
from django.conf import settings
from django.contrib import admin
from django.db.models import CharField
from django.db.models import IntegerField


class LangManager(Manager):
    pass


class Lang(Model):
    code = CharField(max_length=3)
    name = CharField(max_length=50)
    sort = IntegerField(default=0)

    objects = LangManager()

    class Meta:
        app_label = 'web'
        verbose_name = 'lang'
        verbose_name_plural = 'langs'
        ordering = ['sort']

    def __str__(self):
        return self.code


class LangAdmin(admin.ModelAdmin):
    list_display = ('code', 'name', 'sort')

    if settings.PRODUCTION:
        def has_change_permission(self, request, obj=None):
            return False

        def has_add_permission(self, request):
            return False

        def has_module_permission(self, request):
            return False
