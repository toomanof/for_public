from django.utils.translation import ugettext_lazy as _
from django.core.validators import RegexValidator


class SubdomainValidator(RegexValidator):
    regex = '^[A-Za-z0-9-]+$'
    message = _('Please use alphanumeric for this field (abs123-)')
