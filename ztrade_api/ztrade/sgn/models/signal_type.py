from django.db.models import Manager
from django.db.models import Model
from django.contrib import admin
from django.db.models import CharField
from django.db.models import IntegerField


class SignalTypeManager(Manager):
    pass


class SignalType(Model):
    code = CharField('код', max_length=100, unique=True)
    name = CharField('название', max_length=100)
    class_name = CharField('css класс', max_length=100)
    sort = IntegerField('сортировка', default=0)

    objects = SignalTypeManager()

    class Meta:
        app_label = 'sgn'
        verbose_name = 'тип сигнала'
        verbose_name_plural = 'типы сигналов'
        ordering = ['sort']

    def __str__(self):
        return self.name


@admin.register(SignalType)
class SignalTypeAdmin(admin.ModelAdmin):
    actions = None
    list_display = (
        'code',
        'name',
        'sort',
    )
    exclude = ('class_name',)
    readonly_fields = ('code',)

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False
