import json
import datetime
from django.utils import timezone
from django.db.models import Manager
from django.db.models import Model
from django.contrib import admin
from django.apps import apps
from django.conf import settings
from django.utils.html import mark_safe
from django.db.models import CharField
from django.db.models import IntegerField
from django.db.models import ForeignKey
from django.db.models import DecimalField
from django.db.models import CASCADE
from django.db.models import DateTimeField
from django.db.models import BooleanField

from web.services.helpers import get_guid
from web.tasks import send_ws_message
from sgn.tasks import send_signal_notifications
from web.constants import CHANNEL_ACCOUNT
from bot.services import ServiceTelegrmaBot


class SignalManager(Manager):
    pass


class Signal(Model):
    BUY = 'buy'
    SELL = 'sell'

    TRADE_TYPE_CHOICES = (
        (BUY, 'buy'),
        (SELL, 'sell'),
    )

    MARKET = 'market'
    LIMIT = 'limit'

    ORDER_TYPE_CHOICES = (
        (MARKET, 'market'),
        (LIMIT, 'limit'),
    )

    guid = CharField(max_length=50, default=get_guid, unique=True)
    owner = ForeignKey('web.User', on_delete=CASCADE,
                       verbose_name='сиг. админ',
                       related_name='owner_signals')
    market = ForeignKey('Market', on_delete=CASCADE,
                        verbose_name='рынок', related_name='market_signals')
    symbol = ForeignKey('Symbol', on_delete=CASCADE,
                        verbose_name='инструмент',
                        related_name='symbol_signals')
    signal_type = ForeignKey('SignalType', on_delete=CASCADE,
                             verbose_name='тип сигнала',
                             related_name='signal_type_signals')
    trade_type = CharField('тип сделки', max_length=4,
                           choices=TRADE_TYPE_CHOICES)
    order_type = CharField('тип ордера', max_length=20,
                           choices=ORDER_TYPE_CHOICES)
    price = DecimalField('цена', max_digits=12, decimal_places=6,
                         null=True, default=None, blank=True)
    tp = DecimalField('take profit', max_digits=12, decimal_places=6,
                      null=True, default=None, blank=True)
    sl = DecimalField('stop loss', max_digits=12, decimal_places=6,
                      null=True, default=None, blank=True)
    created_at = DateTimeField('время создания')
    countdown = IntegerField('отсчет, секунд', default=60)
    result = IntegerField('результат, pips', null=True, default=None,
                          blank=True)
    publish_at_paid = DateTimeField('время публикации',
                                    help_text='Индексируется автоматически')
    is_closed = BooleanField('завершен', default=False)

    objects = SignalManager()

    class Meta:
        app_label = 'sgn'
        verbose_name = 'сигнал'
        verbose_name_plural = 'сигналы'
        ordering = ['-created_at']

    def __str__(self):
        return 'Signal: {}'.format(self.symbol.name)

    def save(self, *args, **kwargs):
        if not self.pk and self.created_at is None:
            self.created_at = timezone.now()
        self.publish_at_paid = self.created_at +\
            datetime.timedelta(seconds=self.countdown)
        self.market = self.symbol.market
        super().save(*args, **kwargs)

    @property
    def market_code(self):
        return self.market.code

    @property
    def publish_at_free(self):
        return self.publish_at_paid + datetime.timedelta(hours=2)

    def get_base_message(self):
        return {
            'channel': CHANNEL_ACCOUNT,
            'signal_id': self.id,
        }

    def send_message_signal_created(self):
        Notification = apps.get_model('account', 'Notification')
        text = 'Новый сигнал по {}'.format(self.symbol.name)

        ServiceTelegrmaBot().send_message(
            'Новый сигнал',
            self.created_at.strftime("%d %b %Y %H:%M"),
            self.symbol.name)

        msg = self.get_base_message()
        msg['type'] = settings.SIGNAL_CREATED
        msg['alert_text'] = text
        send_ws_message.delay(msg)
        send_signal_notifications(self.id, Notification.SIGNAL_CREATED)

    def send_message_take_profit_updated(self):
        Notification = apps.get_model('account', 'Notification')

        text = '{} Update T/P'.format(self.symbol.name)

        msg = self.get_base_message()
        msg['type'] = settings.SIGNAL_TAKE_PROFIT_UPDATED
        msg['alert_text'] = text
        send_ws_message.delay(msg)
        send_signal_notifications(self.id, Notification.SIGNAL_TP_UPDATED)

    def send_message_stop_loss_updated(self):
        Notification = apps.get_model('account', 'Notification')

        text = '{} Update S/L'.format(self.symbol.name)

        msg = self.get_base_message()
        msg['type'] = settings.SIGNAL_STOP_LOSS_UPDATED
        msg['alert_text'] = text
        send_ws_message.delay(msg)
        send_signal_notifications(self.id, Notification.SIGNAL_SL_UPDATED)

    def send_message_signal_closed(self):
        Notification = apps.get_model('account', 'Notification')

        text = '{} Closed'.format(self.symbol.name)
        ServiceTelegrmaBot().send_message(
            'Прибыль',
            self.created_at.strftime("%d %b %Y %H:%M"),
            self.symbol.name,
            self.result)

        msg = self.get_base_message()
        msg['type'] = settings.SIGNAL_CLOSED
        msg['alert_text'] = text
        send_ws_message.delay(msg)
        send_signal_notifications(self.id, Notification.SIGNAL_CLOSED)


@admin.register(Signal)
class SignalAdmin(admin.ModelAdmin):
    fields = (
        'owner',
        'market',
        'symbol',
        'signal_type',
        'trade_type',
        'order_type',
        'price',
        'tp',
        'sl',
        'countdown',
        'is_closed',
        'result'
    )
    list_filter = ('market', 'symbol', 'trade_type', 'created_at')
    list_editable = ('is_closed',)
    readonly_fields = ('owner', 'market', 'symbol')

    list_display = (
        'symbol',
        'owner',
        'market',
        'signal_type',
        'trade_type',
        'order_type',
        'price',
        'tp',
        'sl',
        'created_at',
        'countdown',
        'publish_at_paid',
        'is_closed',
        'show_result'
    )

    def show_result(self, obj):
        if obj.result is None:
            return '-'

        if obj.result > 0:
            s = '<div style="color:#008000">{}</div>'.format(obj.result)
            return mark_safe(s)
        elif obj.result < 0:
            s = '<div style="color:#ff3333">{}</div>'.format(obj.result)
            return mark_safe(s)
        return obj.result
    show_result.short_description = 'результат, pips'

    def get_queryset(self, request):
        """
        ADMIN (superuser) see all signals.
        SIGNAL_ADMIN can view and change only own signals.
        """
        User = apps.get_model('web', 'User')

        qs = super().get_queryset(request)
        user = request.user

        if user.type == User.ADMIN:
            return qs

        return qs.filter(owner=user)

    def add_view(self, request, extra_context=None):
        Market = apps.get_model('sgn', 'Market')

        d = {}
        for obj in Market.objects.all():
            d[obj.id] = list(obj.symbols.values_list('id', flat=True))

        extra_context = extra_context or {}
        extra_context['sids'] = json.dumps(d)

        self.readonly_fields = []
        self.fields = (
            'market',
            'symbol',
            'signal_type',
            'trade_type',
            'order_type',
            'price',
            'tp',
            'sl',
            'countdown',
        )

        timezone.deactivate()
        return super().add_view(request, extra_context=extra_context)

    def change_view(self, request, object_id):
        self.readonly_fields = ['owner', 'market', 'symbol']
        self.fields = (
            'owner',
            'market',
            'symbol',
            'signal_type',
            'trade_type',
            'order_type',
            'price',
            'tp',
            'sl',
            'countdown',
            'is_closed',
            'result'
        )
        timezone.deactivate()
        return super().change_view(request, object_id)

    def save_model(self, request, obj, form, change):
        if change is False:
            obj.owner = request.user
            super().save_model(request, obj, form, change)
            obj.send_message_signal_created()
            return

        super().save_model(request, obj, form, change)

        if 'sl' in form.changed_data:
            obj.send_message_stop_loss_updated()

        if 'tp' in form.changed_data:
            obj.send_message_take_profit_updated()

        if 'is_closed' in form.changed_data:
            if obj.is_closed:
                obj.send_message_signal_closed()
