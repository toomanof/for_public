from django.db.models import Manager
from django.db.models import Model
from django.contrib import admin
from django.apps import apps
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.db.models import ForeignKey
from django.db.models import TextField
from django.db.models import DateTimeField
from django.db.models import CASCADE


class NewsNotificationManager(Manager):
    pass


class NewsNotification(Model):
    market = ForeignKey('sgn.Market', on_delete=CASCADE)
    text = TextField()
    created_at = DateTimeField(auto_now_add=True)

    objects = NewsNotificationManager()

    class Meta:
        app_label = 'sgn'
        verbose_name = 'новостное уведомление'
        verbose_name_plural = 'новостные уведомления'
        ordering = ['-created_at']

    def __str__(self):
        return self.text


@receiver(post_save, sender=NewsNotification)
def news_notification_created(sender, instance, created, **kwargs):
    """
    Do not use bulk create here, because post_save signal is used
    to send web socket notification to each user
    """
    if created:
        User = apps.get_model('web', 'User')
        Notification = apps.get_model('account', 'Notification')

        for user in User.objects.all():
            Notification.objects.create(
                user=user, market=instance.market,
                text=instance.text,
                code=Notification.NEWS,
                type=Notification.NEWS)


@admin.register(NewsNotification)
class NewsNotificationAdmin(admin.ModelAdmin):
    list_display = (
        'created_at',
        'market',
        'text',
    )
