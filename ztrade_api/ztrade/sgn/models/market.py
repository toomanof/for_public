from django.db.models import Manager
from django.db.models import Model
from django.contrib import admin
from django.db.models import CharField
from django.db.models import IntegerField
from django.db.models import TextField
from django.db.models import BooleanField


class MarketManager(Manager):
    pass


class Market(Model):
    code = CharField('код', max_length=100, unique=True, db_index=True)
    name = CharField('название', max_length=100)
    sort = IntegerField('сортировка', default=0)
    page_stat = TextField('страница статистика', null=True,
                          default=None, blank=True)
    page_rules = TextField('страница правила', null=True,
                           default=None, blank=True)
    page_samples = TextField('страница примеры', null=True,
                             default=None, blank=True)
    page_rating = TextField('страница рейтинг', null=True,
                            default=None, blank=True)
    tariff_footer = TextField(blank=True, default='')
    active = BooleanField('В работе', default=True)

    objects = MarketManager()

    class Meta:
        app_label = 'sgn'
        verbose_name = 'рынок'
        verbose_name_plural = 'рынки'
        ordering = ['sort']

    def __str__(self):
        return self.name


@admin.register(Market)
class MarketAdmin(admin.ModelAdmin):
    actions = None
    list_display = (
        'code',
        'name',
        'active',
        'sort',
    )
    readonly_fields = ('code',)

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False
