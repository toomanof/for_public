from django.db.models import Manager
from django.db.models import Model
from django.contrib import admin
from django.db.models import CharField
from django.db.models import IntegerField
from django.db.models import ForeignKey
from django.db.models import CASCADE


class SymbolManager(Manager):
    pass


class Symbol(Model):
    market = ForeignKey('Market', on_delete=CASCADE,
                        verbose_name='рынок', related_name='symbols')
    code = CharField('код', max_length=100, unique=True)
    name = CharField('название', max_length=100)
    prec = IntegerField('кол-во знаков после запятой', default=2)
    sort = IntegerField('сортировка', default=0)

    objects = SymbolManager()

    class Meta:
        app_label = 'sgn'
        verbose_name = 'инструмент'
        verbose_name_plural = 'инструменты'
        ordering = ['sort']

    def __str__(self):
        return self.name

    @property
    def market_code(self):
        return self.market.code


@admin.register(Symbol)
class SymbolAdmin(admin.ModelAdmin):
    list_display = (
        'code',
        'name',
        'market',
        'sort',
    )
    list_filter = ('market',)
