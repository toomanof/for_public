import datetime
from dateutil import relativedelta
from django.db.models import Manager
from django.db.models import Model
from django.contrib import admin
from django.apps import apps
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.db.models import ForeignKey
from django.db.models import DateField
from django.db.models import BooleanField
from django.db.models import CASCADE


class TariffUserManager(Manager):
    def create_bill(self, user_id, tariff_id):
        User = apps.get_model('web', 'User')
        Tariff = apps.get_model('sgn', 'Tariff')
        Bill = apps.get_model('payment', 'Bill')

        user = User.objects.get(id=user_id)
        tariff = Tariff.objects.get(id=tariff_id)

        date_from = datetime.date.today()

        if tariff.period_type == Tariff.DAY:
            date_to = date_from + datetime.timedelta(days=tariff.period)
        else:
            date_to = date_from + relativedelta.relativedelta(
                months=tariff.period)

        tu = self.model.objects.create(
            user=user,
            tariff=tariff,
            market=tariff.market,
            date_from=date_from,
            date_to=date_to)

        name = 'Торговые сигналы. Тариф: {} c {} до {}'.format(
            tariff.name,
            date_from.strftime('%d.%m.%y'),
            date_to.strftime('%d.%m.%y'))

        bill = Bill.objects.create(
            user=user, tariff_user=tu, name=name, amount=tariff.price)
        return bill


class TariffUser(Model):
    user = ForeignKey('web.User', on_delete=CASCADE)
    market = ForeignKey('Market', on_delete=CASCADE)
    tariff = ForeignKey('Tariff', on_delete=CASCADE)
    date_from = DateField('Дата с')
    date_to = DateField('Дата до')
    is_paid = BooleanField('оплачен', default=False,
                           help_text='Индексируется автоматически')

    objects = TariffUserManager()

    class Meta:
        app_label = 'sgn'
        verbose_name = 'тариф пользователя'
        verbose_name_plural = 'тарифы пользователей'
        ordering = ['-date_from']

    def __str__(self):
        return 'TariffUser {}'.format(self.id)

    def save(self, *args, **kwargs):
        self.market_id = self.tariff.market_id
        super().save(*args, **kwargs)


@receiver(post_save, sender=TariffUser)
def reindex(sender, instance, created, **kwargs):
    Bill = apps.get_model('payment', 'Bill')

    is_paid = False
    qs = Bill.objects.filter(tariff_user_id=instance.id, is_paid=True)
    if qs.exists():
        is_paid = True

    TariffUser.objects.filter(id=instance.id).update(is_paid=is_paid)


@admin.register(TariffUser)
class TariffUserAdmin(admin.ModelAdmin):
    list_display = (
        'date_from',
        'date_to',
        'user',
        'market',
        'tariff',
        'is_paid'
    )

    exclude = ('market',)

    readonly_fields = (
        'is_paid',
    )

    def has_delete_permission(self, request, obj=None):
        return False
