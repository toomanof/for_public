from django.db.models import Manager
from django.db.models import Model
from django.contrib import admin
from django.apps import apps
from django.db.models import ForeignKey
from django.db.models import ManyToManyField
from django.db.models import BooleanField
from django.db.models import CASCADE


class SignalFilterManager(Manager):
    pass


class SignalFilter(Model):
    user = ForeignKey('web.User', on_delete=CASCADE,
                      verbose_name='пользователь')
    market = ForeignKey('Market', on_delete=CASCADE, verbose_name='рынок')
    timezone = ForeignKey('web.Timezone', on_delete=CASCADE, null=True,
                          default=None, blank=True)
    is_sound = BooleanField(default=False)
    is_all_symbols = BooleanField(default=True)
    symbols = ManyToManyField('Symbol', verbose_name='инструменты')

    objects = SignalFilterManager()

    class Meta:
        app_label = 'sgn'
        verbose_name = 'сигналы фильтр'
        verbose_name_plural = 'сигналы фильтр'
        ordering = ['id']

    def __str__(self):
        return str(self.id)

    @property
    def market_code(self):
        return self.market.code

    @property
    def symbol_checked_items(self):
        """
        Prepare for frontend ready data to work with
        "CheckedItems" component
        """
        Symbol = apps.get_model('sgn', 'Symbol')
        qs = Symbol.objects.filter(market_id=self.market_id)

        if self.is_all_symbols:
            l = []
            l.append({'id': None, 'name': 'Все', 'isChecked': True})
            for obj in qs:
                l.append({'id': obj.id, 'name': obj.name, 'isChecked': True})
            return l

        l = []
        l.append({'id': None, 'name': 'Все', 'isChecked': False})

        ids = self.symbols.values_list('id', flat=True)

        for obj in qs:
            if obj.id in ids:
                is_checked = True
            else:
                is_checked = False

            l.append(
                {'id': obj.id, 'name': obj.name, 'isChecked': is_checked})
        return l

    def convert_dt(self, dt):
        """
        Convert dt accordingly to timezone
        """
        if self.timezone is None:
            return dt
        return self.timezone.convert_dt(dt)


@admin.register(SignalFilter)
class SignalFilterAdmin(admin.ModelAdmin):
    list_display = (
        'user',
        'market',
        'timezone',
        'is_sound',
        'is_all_symbols'
    )
