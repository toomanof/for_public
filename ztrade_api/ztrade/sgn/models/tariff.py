from django.db.models import Manager
from django.db.models import Model
from django.contrib import admin
from django.db.models import ForeignKey
from django.db.models import CASCADE
from django.db.models import CharField
from django.db.models import IntegerField
from django.db.models import DecimalField
from django.db.models import BooleanField
from django.db.models import ManyToManyField

from .tariff_row import TariffRow


class TariffManager(Manager):
    pass


class Tariff(Model):
    DAY = 'day'
    MONTH = 'month'

    PERIOD_TYPE_CHOICES = (
        (DAY, 'дней'),
        (MONTH, 'месяцев'),
    )

    market = ForeignKey('Market', on_delete=CASCADE)
    code = CharField('код', max_length=50,
                     help_text='для внутренней маркировки', unique=True)
    name = CharField('название', max_length=100)
    subtitle = CharField('подзаголовок', max_length=100)
    subtitle_pic = CharField('подзаголовок pic', max_length=100,
                             blank=True, default='',
                             help_text='1.svg | 2.svg | 3.svg')
    subtitle_class = CharField('подзаголовок class', blank=True,
                               max_length=100, default='',
                               help_text='img2 или ничего')
    period = IntegerField('период')
    period_type = CharField('тип периода', max_length=10,
                            choices=PERIOD_TYPE_CHOICES)
    price = DecimalField('стоимость', max_digits=10, decimal_places=2)
    is_active = BooleanField('активен', default=True)
    header_css_class = CharField('css класс хеадера', max_length=100,
                                 null=True, default=None, blank=True)
    sale = IntegerField('скидка', blank=True, null=True, default=None)
    sort = IntegerField('сортировка', default=0)
    symbols = ManyToManyField('Symbol', verbose_name='символы')
    signal_types = ManyToManyField('SignalType', verbose_name='типы сигналов')

    objects = TariffManager()

    class Meta:
        app_label = 'sgn'
        verbose_name = 'тариф'
        verbose_name_plural = 'тарифы'
        ordering = ['sort']

    def __str__(self):
        return self.name

    @property
    def period_type_str(self):
        if self.period_type == self.DAY:
            if self.period == 1:
                return 'день'
            elif self.period > 1 and self.period < 5:
                return 'дня'
            return 'дней'

        if self.period_type == self.MONTH:
            if self.period == 1:
                return 'месяц'
            elif self.period > 1 and self.period < 5:
                return 'месяца'
            return 'месяцев'

    @property
    def period_str(self):
        return '{} {}'.format(self.period, self.period_type_str)


class TariffRowInline(admin.TabularInline):
    model = TariffRow
    fields = ('name', 'class_name', 'sort')
    extra = 0


@admin.register(Tariff)
class TariffAdmin(admin.ModelAdmin):
    actions = None
    inlines = [
        TariffRowInline
    ]

    list_filter = ('is_active', 'market')
    list_display = (
        'name',
        'code',
        'market',
        'subtitle',
        'subtitle_pic',
        'subtitle_class',
        'period',
        'period_type',
        'price',
        'header_css_class',
        'is_active',
        'sort'
    )
    exclude = ('signal_types', 'symbols')

    def has_delete_permission(self, request, obj=None):
        return False
