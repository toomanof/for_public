from django.db.models import Manager
from django.db.models import Model
from django.db.models import ForeignKey
from django.db.models import DateTimeField
from django.db.models import CASCADE


class SignalHideManager(Manager):
    pass


class SignalHide(Model):
    """
    Hide signal for particular user
    """
    user = ForeignKey('web.User', on_delete=CASCADE)
    signal = ForeignKey('Signal', on_delete=CASCADE)
    created_at = DateTimeField(auto_now_add=True)

    objects = SignalHideManager()

    class Meta:
        app_label = 'sgn'
        verbose_name = 'signal hide'
        verbose_name_plural = 'signals hide'
        ordering = ['-created_at']

    def __str__(self):
        return str(self.id)
