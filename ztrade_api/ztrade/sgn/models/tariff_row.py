from django.db.models import Manager
from django.db.models import Model
from django.db.models import ForeignKey
from django.db.models import CharField
from django.db.models import IntegerField
from django.db.models import CASCADE


class TariffRowManager(Manager):
    pass


class TariffRow(Model):
    tariff = ForeignKey('Tariff', on_delete=CASCADE, verbose_name='тариф',
                        related_name='rows')
    name = CharField('название', max_length=100)
    class_name = CharField('css класс', max_length=100)
    sort = IntegerField('сортировка', default=0)

    objects = TariffRowManager()

    class Meta:
        app_label = 'sgn'
        verbose_name = 'тариф строка'
        verbose_name_plural = 'тариф строки'
        ordering = ['sort']

    def __str__(self):
        return self.name
