from django.utils import timezone
from django.core.exceptions import ValidationError
from rest_framework.serializers import ModelSerializer
from rest_framework.serializers import ModelField
from rest_framework.serializers import IntegerField
from rest_framework.serializers import CharField
from rest_framework.serializers import DecimalField
from rest_framework.serializers import ListField
from rest_framework.serializers import SerializerMethodField

from sgn.models import Market
from sgn.models import Symbol
from sgn.models import SignalFilter
from sgn.models import SignalType
from sgn.models import Signal
from sgn.models import Tariff
from sgn.models import TariffRow

from sgn.mixins import RemoteMixin


class MarketSerializer(ModelSerializer):
    class Meta:
        model = Market
        fields = (
            'id',
            'code',
            'name',
            'page_stat',
            'page_rules',
            'page_samples',
            'page_rating',
            'tariff_footer',
            'active',
        )


class SymbolSerializer(ModelSerializer):
    class Meta:
        model = Symbol
        fields = (
            'id',
            'code',
            'name',
            'prec',
            'market_code'
        )


class SignalFilterSerializer(ModelSerializer):
    user_id = ModelField(model_field=SignalFilter._meta.get_field('user'))
    market_id = ModelField(model_field=SignalFilter._meta.get_field('market'))
    timezone_id = ModelField(
        model_field=SignalFilter._meta.get_field('timezone'))
    symbol_ids = SerializerMethodField()

    class Meta:
        model = SignalFilter
        fields = (
            'id',
            'user_id',
            'timezone_id',
            'market_id',
            'market_code',
            'symbol_ids',
            'is_all_symbols',
            'is_sound',
            'symbol_checked_items',
        )

    def get_symbol_ids(self, obj):
        return list(obj.symbols.values_list('id', flat=True))


class SignalFilterUpdateSerializer(ModelSerializer):
    """
    Partial update for particular market.
    """
    symbol_ids = ListField(child=IntegerField(), required=False)
    timezone_id = ModelField(
        model_field=SignalFilter._meta.get_field('timezone'), required=False)

    class Meta:
        model = SignalFilter
        fields = (
            'is_all_symbols',
            'symbol_ids',
            'is_sound',
            'timezone_id'
        )

    def update(self, instance, validated_data):
        d = validated_data

        if 'is_sound' in d:
            instance.is_sound = d['is_sound']
            instance.save()

        if 'timezone_id' in d:
            instance.timezone_id = d['timezone_id']
            instance.save()

        if 'is_all_symbols' in d:
            instance.is_all_symbols = d['is_all_symbols']
            instance.save()
            ids = d.pop('symbol_ids')
            instance.symbols.set(Symbol.objects.filter(id__in=ids))
        return instance


class SignalTypeSerializer(ModelSerializer):
    class Meta:
        model = SignalType
        fields = (
            'id',
            'code',
            'name'
        )


class SignalSerializer(ModelSerializer):
    market_id = ModelField(model_field=Signal._meta.get_field('market'))
    symbol_id = ModelField(model_field=Signal._meta.get_field('symbol'))
    symbol = SymbolSerializer(read_only=True)
    signal_type = SignalTypeSerializer(read_only=True)

    class Meta:
        model = Signal
        fields = (
            'id',
            'guid',
            'symbol',
            'market_id',
            'market_code',
            'symbol_id',
            'signal_type',
            'trade_type',
            'order_type',
            'price',
            'tp',
            'sl',
            'created_at',
            'countdown',
            'is_closed',
            'result',
            'publish_at_paid',
            'publish_at_free'
        )


class SignalExtSerializer(SignalSerializer):
    """
    Serializer for detailed signal page
    It has additional field "is_notify"
    Should we notify user about signal.
    """
    is_sound = SerializerMethodField()
    is_notify = SerializerMethodField()

    class Meta(SignalSerializer.Meta):
        model = Signal
        fields = SignalSerializer.Meta.fields + ('is_notify', 'is_sound')

    def get_is_sound(self, obj):
        user = self.context.get('user')
        f = user.get_signals_filter(obj.market_id)
        return f.is_sound

    def get_is_notify(self, obj):
        user = self.context.get('user')
        return user.is_signal_notify(obj.id)


class TariffRowSerializer(ModelSerializer):
    class Meta:
        model = TariffRow
        fields = (
            'id',
            'name',
            'class_name',
        )


class TariffSerializer(ModelSerializer):
    rows = TariffRowSerializer(many=True, read_only=True)

    class Meta:
        model = Tariff
        fields = (
            'id',
            'name',
            'subtitle',
            'subtitle_pic',
            'subtitle_class',
            'period',
            'period_type',
            'period_str',
            'price',
            'sale',
            'is_active',
            'header_css_class',
            'rows'
        )


class RemoteSignalSerializer(ModelSerializer):
    symbol_code = CharField(source='symbol.code')
    signal_type_code = CharField(source='signal_type.code')
    signal_guid = CharField(source='guid')

    class Meta:
        model = Signal
        fields = (
            'id',
            'signal_guid',
            'symbol_code',
            'signal_type_code',
            'trade_type',
            'order_type',
            'price',
            'tp',
            'sl',
            'created_at',
            'countdown',
            'is_closed',
            'result'
        )


class RemoteSignalCreateSerializer(RemoteMixin, ModelSerializer):
    api_key = CharField()
    symbol_code = CharField()
    signal_type_code = CharField()
    price = DecimalField(max_digits=12, decimal_places=6)
    countdown = IntegerField()

    class Meta:
        model = Signal
        fields = (
            'api_key',
            'symbol_code',
            'signal_type_code',
            'trade_type',
            'order_type',
            'price',
            'tp',
            'sl',
            'countdown'
        )

    def validate(self, data):
        symbol = Symbol.objects.filter(code=data.pop('symbol_code')).first()
        if symbol is None:
            raise ValidationError('Incorrect symbol code')

        signal_type = SignalType.objects.filter(
            code=data.pop('signal_type_code')).first()
        if signal_type is None:
            raise ValidationError('Incorrect signal type code')

        data['symbol'] = symbol
        data['signal_type'] = signal_type
        return data

    def to_representation(self, instance):
        return RemoteSignalSerializer(instance).data

    def save(self):
        d = self.validated_data
        d.pop('api_key')
        d['owner_id'] = self.context.get('user').id
        d['created_at'] = timezone.now()
        signal = Signal.objects.create(**d)
        signal.send_message_signal_created()
        return signal


class RemoteSignalUpdateSerializer(RemoteMixin, ModelSerializer):
    api_key = CharField()
    signal_guid = CharField()
    order_type = CharField(required=False)
    signal_type_code = CharField(required=False)
    tp = DecimalField(max_digits=12, decimal_places=6, required=False)
    sl = DecimalField(max_digits=12, decimal_places=6, required=False)
    price = DecimalField(max_digits=12, decimal_places=6, required=False)

    class Meta:
        model = Signal
        fields = (
            'api_key',
            'signal_guid',
            'tp',
            'sl',
            'signal_type_code',
            'order_type',
            'price',
        )

    def validate(self, data):
        if 'signal_type_code' in data:
            signal_type = SignalType.objects.filter(
                code=data.pop('signal_type_code')).first()
            if signal_type is None:
                raise ValidationError('Incorrect signal type code')

            data['signal_type'] = signal_type

        ts = ('market', 'limit')
        if 'order_type' in data and data['order_type'] not in ts:
            raise ValidationError('Incorrect order type')

        return data

    def save(self):
        d = self.validated_data
        signal = self.context.get('signal')

        old_tp = signal.tp
        old_sl = signal.sl

        if 'signal_type' in d:
            signal.signal_type = d['signal_type']

        for k in ('tp', 'sl', 'order_type', 'price'):
            if k in d:
                setattr(signal, k, d[k])
        signal.save()

        if 'tp' in d and d['tp'] != old_tp:
            signal.send_message_take_profit_updated()

        if 'sl' in d and d['sl'] != old_sl:
            signal.send_message_stop_loss_updated()

        return signal


class RemoteSignalCloseSerializer(RemoteMixin, ModelSerializer):
    api_key = CharField()
    signal_guid = CharField()
    result = IntegerField()

    class Meta:
        model = Signal
        fields = (
            'api_key',
            'signal_guid',
            'result',
        )

    def save(self):
        d = self.validated_data
        signal = self.context.get('signal')
        signal.result = d['result']
        signal.is_closed = True
        signal.save()
        signal.send_message_signal_closed()
        return signal
