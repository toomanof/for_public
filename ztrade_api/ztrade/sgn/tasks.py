from django.apps import apps
from ztrade import capp


@capp.task
def send_signal_notifications(signal_id, code):
    """
    Create notifications for all users about signal
    """
    Notification = apps.get_model('account', 'Notification')
    Signal = apps.get_model('sgn', 'Signal')
    User = apps.get_model('web', 'User')

    signal = Signal.objects.filter(id=signal_id).first()
    if signal is None:
        return

    for user in User.objects.all():
        if user.is_signal_notify(signal.id):
            Notification.objects.process(
                code, Notification.SIGNALS, user,
                signal.market, signal=signal,
                contract={'signal_id': signal.id})
