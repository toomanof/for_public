from django.apps import apps
from django.core.exceptions import ValidationError


class RemoteMixin:
    def validate_api_key(self, value):
        User = apps.get_model('web', 'User')

        user = User.objects.filter(api_key=value).first()
        if user is None or user.type != User.SIGNAL_ADMIN:
            raise ValidationError('Incorrect API Key')

        self.context['user'] = user
        return value

    def validate_signal_guid(self, value):
        Signal = apps.get_model('sgn', 'Signal')

        signal = Signal.objects.filter(guid=value).first()
        if signal is None:
            raise ValidationError('Incorrect guid')

        if signal.owner != self.context.get('user'):
            raise ValidationError('Incorrect guid')

        if signal.is_closed:
            raise ValidationError('You can not edit closed signal')

        self.context['signal'] = signal
        return value
