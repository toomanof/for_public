import datetime
import decimal
import random
from django.core.management.base import BaseCommand
from django.utils import timezone

from sgn.models import Market
from sgn.models import Symbol
from sgn.models import Signal
from sgn.models import SignalType
from web.models import User


class Command(BaseCommand):
    def generate_price(self):
        value = random.randint(1000, 3000)
        s = '1.{}'.format(value)
        return decimal.Decimal(s)

    def create_signals(self):
        """
        Creates 5 signals for each symbol
        """
        base = datetime.datetime.today()
        l = [base - datetime.timedelta(days=x) for x in range(0, 300)]
        l.reverse()

        stypes = list(SignalType.objects.all())
        markets = list(Market.objects.all())[:2]

        signals = []
        for date in l:
            if date.isoweekday() in (6, 7):
                continue

            dt = datetime.datetime.combine(
                date, datetime.datetime.min.time())
            dt = timezone.make_aware(dt)

            for market in markets:
                symbols = Symbol.objects.filter(market=market)
                for i in range(1, 6):

                    dt += datetime.timedelta(minutes=random.randint(50, 100))

                    result = random.randint(
                        20, 40) if i % 2 == 0 else random.randint(41, 50)

                    if result <= 40:
                        result = -result

                    s = Signal(
                        owner=User.objects.first(),
                        symbol=random.choice(symbols),
                        signal_type=random.choice(stypes),
                        trade_type=random.choice([Signal.BUY, Signal.SELL]),
                        order_type=random.choice(
                            [Signal.MARKET, Signal.LIMIT]),
                        created_at=dt,
                        countdown=300,
                        is_closed=True,
                        price=self.generate_price(),
                        tp=self.generate_price(),
                        sl=self.generate_price(),
                        result=result,
                        publish_at_paid=dt
                    )
                    s.market = s.symbol.market
                    signals.append(s)
        Signal.objects.bulk_create(signals)

    def handle(self, *args, **options):
        Signal.objects.all().delete()
        self.create_signals()
