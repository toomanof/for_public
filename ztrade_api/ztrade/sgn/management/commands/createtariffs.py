import os
import pandas
from django.core.management.base import BaseCommand
from django.conf import settings

from sgn.models import Market
from sgn.models import Tariff
from sgn.models import TariffRow
from sgn.models import TariffUser
from web.models import User


from content.management.commands.helpers import get_file_content


class Command(BaseCommand):
    BOOL_FIELDS = []
    EXCEL_PATH = os.path.join(settings.BASE_DIR,
                              'ztrade/content/management/data/content.xlsx')

    def get_data(self):
        path = self.EXCEL_PATH
        df = pandas.read_excel(open(path, 'rb'), sheet_name='Tariffs')

        l = []
        for index, row in df.iterrows():
            rd = row.to_dict()

            for k, v in rd.items():
                if k in self.BOOL_FIELDS:
                    v = self.process_bool(v)

                if str(v).endswith('.txt'):
                    v = get_file_content(v)

                v = self.process_value(v)
                rd[k] = v

            l.append(rd)
        return l

    def process_bool(self, value):
        if pandas.isnull(value):
            return False

        if value.strip().lower() == 'y':
            return True

        return False

    def process_value(self, value):
        if pandas.isnull(value):
            return None
        return value

    def create_tariffs(self):
        Tariff.objects.all().delete()

        for d in self.get_data():
            d['market_id'] = Market.objects.get(
                code=d.pop('market_code')).id

            Tariff.objects.create(**d)

    def create_tariff_rows(self):
        TariffRow.objects.all().delete()

        rows = (
            ('Продвижение в Яндекс, Google, Mail', 'tarif_vol'),
            ('Контектстная реклама с первых дней', 'tarif_form'),
            ('Ваш персональный менеджер', 'tarif_user'),
            ('Комплексный аудит Вашего сайта', 'tarif_pic'),
        )

        for t in Tariff.objects.all():
            sort = 0
            for name, class_name in rows:
                sort += 10
                TariffRow.objects.create(
                    tariff=t,
                    name=name,
                    class_name=class_name,
                    sort=sort)

    def create_bill(self):
        """
        Create bill for admin, so it paid for signal's tariff
        """
        t = Tariff.objects.get(code='t2')
        user = User.objects.get(email='a@a.com')
        bill = TariffUser.objects.create_bill(user.id, t.id)
        bill.is_paid = True
        bill.save()
        bill.tariff_user.is_paid = True
        bill.tariff_user.save()

    def handle(self, *args, **options):
        self.create_tariffs()
        self.create_tariff_rows()
        self.create_bill()
