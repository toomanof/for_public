from django.core.management.base import BaseCommand

from sgn.models import Market
from sgn.models import NewsNotification


class Command(BaseCommand):
    def create(self):
        """
        Do not use bulk_create here
        We need post_save signal
        """
        for market in Market.objects.all():
            for i in range(1, 10):
                num = str(i).zfill(2)
                text = 'Какое-то уведомление {}'.format(num)
                NewsNotification.objects.create(market=market, text=text)

    def handle(self, *args, **options):
        self.create()
