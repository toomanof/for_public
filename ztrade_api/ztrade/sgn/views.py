import datetime
import logging
import time
from django.db.models.functions import ExtractWeek
from django.db.models.functions import ExtractMonth
from django.db.models.functions import ExtractYear
from django.db.models import Sum
from django.db.models import Case
from django.db.models import When
from django.db.models import F
from django.db.models import IntegerField

from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.generics import ListAPIView
from rest_framework.response import Response

from sgn.serializers import MarketSerializer
from sgn.serializers import SymbolSerializer
from sgn.serializers import SignalSerializer
from sgn.serializers import SignalExtSerializer
from sgn.serializers import SignalFilterSerializer
from sgn.serializers import SignalFilterUpdateSerializer
from sgn.serializers import SignalTypeSerializer
from sgn.serializers import TariffSerializer

from sgn.serializers import RemoteSignalSerializer
from sgn.serializers import RemoteSignalCreateSerializer
from sgn.serializers import RemoteSignalUpdateSerializer
from sgn.serializers import RemoteSignalCloseSerializer

from sgn.models import Market
from sgn.models import Symbol
from sgn.models import Signal
from sgn.models import SignalHide
from sgn.models import SignalType
from sgn.models import Tariff


from web.views import AuthMixin
from web.services.helpers import get_dates_by_week

logger = logging.getLogger('app')


class Markets(AuthMixin, APIView):
    def get(self, request, **kwargs):
        qs = Market.objects.all()
        s = MarketSerializer(qs, many=True)
        for d in s.data:
            d['is_paid'] = request.user.is_market_paid(d['id'])
        return Response(s.data)


class Symbols(AuthMixin, ListAPIView):
    queryset = Symbol.objects.all()
    serializer_class = SymbolSerializer


class SignalFilters(AuthMixin, APIView):
    """
    get:
        <h1>User filters for signals for all markets</h1>
        <b>Response:</b> array of filter object<br />
        <b>Response code:</b> 200
    """
    def get(self, request, **kwargs):
        l = []
        for m in Market.objects.all():
            # get or create signal filter
            filter = request.user.get_signals_filter(m.id)
            l.append(filter)

        s = SignalFilterSerializer(l, many=True)
        return Response(s.data)


class SignalFilterDetail(AuthMixin, APIView):
    """
    patch:
        <h1>Update signal filter endpoint for particular market</h1>
        <p>If is_all_symbols passed, then symbol_ids are ignored.</p>
        <b>Input params:</b><br />
        symbol_ids<br />
        is_all_symbols (bool)<br />
        is_sound (bool)<br />
        <b>Response:</b> updated filter object<br />
        <b>Response code:</b> 200
    """
    def patch(self, request, code, **kwargs):
        market = get_object_or_404(Market, code=code)
        filter = request.user.get_signals_filter(market.id)
        s = SignalFilterUpdateSerializer(filter, data=request.data)
        s.is_valid(raise_exception=True)
        obj = s.save()
        s = SignalFilterSerializer(obj)
        return Response(s.data)


class SignalTypes(AuthMixin, ListAPIView):
    queryset = SignalType.objects.all()
    serializer_class = SignalTypeSerializer


class Signals(AuthMixin, APIView):
    """
    Signals - for particular user for particular market.
    Add "is_paid" info
    """
    serializer_class = SignalSerializer

    def get(self, request, code, **kwargs):
        market = get_object_or_404(Market, code=code)
        ids = request.user.get_paid_symbol_ids(market.id)

        qs = self.request.user.get_signals_qs(market.id)[:100]
        s = SignalSerializer(qs, many=True)

        # additional info
        for d in s.data:
            d['is_paid'] = d['symbol_id'] in ids

        return Response(s.data)


class SignalDetail(AuthMixin, APIView):
    def get(self, request, id, **kwargs):
        signal = get_object_or_404(Signal, id=id)

        s = SignalExtSerializer(signal, context={'user': request.user})
        return Response(s.data)

    def delete(self, request, id, **kwargs):
        SignalHide.objects.create(signal_id=id, user_id=request.user.id)
        return Response(status=204)


class Tariffs(ListAPIView):
    serializer_class = TariffSerializer

    def get_queryset(self):
        return Tariff.objects.filter(
            is_active=True, market__code=self.kwargs.get('code'))


class RemoteSignalCreate(APIView):
    """
    post:
        <h1>Create signal from remote terminal</h1>
        <b>Input params:</b><br />
        api_key (signal admin user api_key)<br />
        symbol_code<br />
        signal_type<br />
        trade_type<br />
        order_type<br />
        price<br />
        tp<br />
        sl<br />
        countdown<br />
        <b>Response:</b> created signal object<br />
        <b>Response code:</b> 201
    """
    def post(self, request, **kwargs):
        s = RemoteSignalCreateSerializer(data=request.data)
        s.is_valid(raise_exception=True)
        signal = s.save()
        s = RemoteSignalSerializer(signal)
        logger.info('Remote request to create signal')
        return Response(s.data, status=201)


class RemoteSignalUpdate(APIView):
    """
    post:
        <h1>Update take profit</h1>
        <b>Input params:</b><br />
        api_key<br />
        signal_type<br />
        order_type<br />
        price<br />
        tp<br />
        sl<br />
        <b>Response:</b> updated signal object<br />
        <b>Response code:</b> 200
    """
    def post(self, request, **kwargs):
        s = RemoteSignalUpdateSerializer(data=request.data)
        s.is_valid(raise_exception=True)
        signal = s.save()
        s = RemoteSignalSerializer(signal)
        logger.info('Remote request to update signal')
        return Response(s.data, status=200)


class RemoteSignalClose(APIView):
    """
    post:
        <h1>Update stop loss</h1>
        <b>Input params:</b><br />
        api_key<br />
        guid<br />
        result<br />
        <b>Response:</b> updated signal object<br />
        <b>Response code:</b> 200
    """
    def post(self, request, **kwargs):
        s = RemoteSignalCloseSerializer(data=request.data)
        s.is_valid(raise_exception=True)
        signal = s.save()
        s = RemoteSignalSerializer(signal)
        logger.info('Remote request to close signal')
        return Response(s.data, status=200)


class ChartsMixin:
    def get_queryset(self, code):
        qs = Signal.objects.filter(market__code=code)

        codes = self.request.query_params.get('symbol_codes', None)
        if codes is not None:
            codes = codes.split(',')
            codes = [x.strip() for x in codes if x.strip()]
            qs = qs.filter(symbol__code__in=codes)
        return qs


class Chart01(AuthMixin, ChartsMixin, APIView):
    def get(self, request, code, **kwargs):
        qs = self.get_queryset(code)

        period = request.query_params.get('period', 'month')

        if period == 'month':
            qs = qs\
                .annotate(month=ExtractMonth('created_at'))\
                .annotate(year=ExtractYear('created_at'))\
                .values('year', 'month')\
                .annotate(profit=Sum(
                    Case(
                        When(result__gte=0, then=F('result')),
                        output_field=IntegerField()
                    )
                ))\
                .annotate(loss=Sum(
                    Case(
                        When(result__lt=0, then=F('result')),
                        output_field=IntegerField()
                    )
                ))\
                .order_by('-year', '-month')

            for d in qs:
                date = datetime.date(d.pop('year'), d.pop('month'), 1)
                d['name'] = date.strftime('%b %Y')

        else:
            qs = qs\
                .annotate(week=ExtractWeek('created_at'))\
                .annotate(year=ExtractYear('created_at'))\
                .values('year', 'week')\
                .annotate(profit=Sum(
                    Case(
                        When(result__gte=0, then=F('result')),
                        output_field=IntegerField()
                    )
                ))\
                .annotate(loss=Sum(
                    Case(
                        When(result__lt=0, then=F('result')),
                        output_field=IntegerField()
                    )
                ))\
                .order_by('-year', '-week')

            for d in qs:
                sd, ed = get_dates_by_week(d.pop('year'), d.pop('week'))
                d['name'] = sd.strftime('%d %b') + ' - ' + ed.strftime('%d %b')

        l = list(qs)[:10]
        l.reverse()
        return Response({'data': l})


class Chart02(AuthMixin, ChartsMixin, APIView):
    def get(self, request, code, **kwargs):
        qs = self.get_queryset(code)
        qs = qs.values('created_at', 'result').order_by('created_at')

        l = []
        balance = 0
        for d in qs:
            result = d.pop('result')
            if result is None:
                continue

            balance += result
            x = time.mktime(d.pop('created_at').timetuple()) * 1000
            x = int(x)
            l.append([x, balance])

        l.reverse()
        return Response({'data': l})


class Chart03(AuthMixin, ChartsMixin, APIView):
    def get(self, request, code, **kwargs):
        qs = self.get_queryset(code)

        qs = qs\
            .annotate(week=ExtractWeek('created_at'))\
            .annotate(year=ExtractYear('created_at'))\
            .values('year', 'week')\
            .annotate(result=Sum('result'))\
            .order_by('year', 'week')

        l = []
        balance = 0
        for d in qs:
            if d['result'] is None:
                continue

            balance += d['result']
            _, ed = get_dates_by_week(d.pop('year'), d.pop('week'))

            x = time.mktime(ed.timetuple()) * 1000
            x = int(x)
            l.append([x, balance])

        l.reverse()
        l = l[:20]
        l.reverse()
        return Response({'data': l})
