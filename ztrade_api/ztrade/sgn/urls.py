from django.urls import re_path
from .import views as v

urlpatterns = [
    re_path(
        r'^(?P<version>(v1))/sgn/markets$',
        v.Markets.as_view(),
        name='markets'),

    re_path(
        r'^(?P<version>(v1))/sgn/symbols$',
        v.Symbols.as_view(),
        name='symbols'),

    re_path(
        r'^(?P<version>(v1))/sgn/signal_types$',
        v.SignalTypes.as_view(),
        name='signal_types'),

    re_path(
        r'^(?P<version>(v1))/sgn/markets/(?P<code>\w+)/signals$',
        v.Signals.as_view(),
        name='signals'),

    re_path(
        r'^(?P<version>(v1))/sgn/signals/(?P<id>\d+)$',
        v.SignalDetail.as_view(),
        name='signal'),

    re_path(
        r'^(?P<version>(v1))/sgn/signal_filters$',
        v.SignalFilters.as_view(),
        name='signal_filters'),

    re_path(
        r'^(?P<version>(v1))/sgn/markets/(?P<code>\w+)/signal_filter$',
        v.SignalFilterDetail.as_view(),
        name='signal_filter'),

    re_path(
        r'^(?P<version>(v1))/sgn/markets/(?P<code>\w+)/tariffs$',
        v.Tariffs.as_view(),
        name='tariffs'),

    re_path(
        r'^(?P<version>(v1))/sgn/markets/(?P<code>\w+)/chart01$',
        v.Chart01.as_view(),
        name='chart01'),

    re_path(
        r'^(?P<version>(v1))/sgn/markets/(?P<code>\w+)/chart02$',
        v.Chart02.as_view(),
        name='chart02'),

    re_path(
        r'^(?P<version>(v1))/sgn/markets/(?P<code>\w+)/chart03$',
        v.Chart03.as_view(),
        name='chart03'),

    re_path(
        r'^(?P<version>(v1))/sgn/remote/signal_create$',
        v.RemoteSignalCreate.as_view(),
        name='remote_signal_create'),

    re_path(
        r'^(?P<version>(v1))/sgn/remote/signal_update$',
        v.RemoteSignalUpdate.as_view(),
        name='remote_signal_update'),

    re_path(
        r'^(?P<version>(v1))/sgn/remote/signal_close$',
        v.RemoteSignalClose.as_view(),
        name='remote_signal_close'),
]
