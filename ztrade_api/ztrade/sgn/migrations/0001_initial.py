# Generated by Django 2.0.4 on 2019-12-09 18:23

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import web.services.helpers


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('web', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Market',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(db_index=True, max_length=100, unique=True, verbose_name='код')),
                ('name', models.CharField(max_length=100, verbose_name='название')),
                ('sort', models.IntegerField(default=0, verbose_name='сортировка')),
                ('page_stat', models.TextField(blank=True, default=None, null=True, verbose_name='страница статистика')),
                ('page_rules', models.TextField(blank=True, default=None, null=True, verbose_name='страница правила')),
                ('page_samples', models.TextField(blank=True, default=None, null=True, verbose_name='страница примеры')),
                ('page_rating', models.TextField(blank=True, default=None, null=True, verbose_name='страница рейтинг')),
            ],
            options={
                'verbose_name': 'рынок',
                'verbose_name_plural': 'рынки',
                'ordering': ['sort'],
            },
        ),
        migrations.CreateModel(
            name='NewsNotification',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('text', models.TextField()),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('market', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='sgn.Market')),
            ],
            options={
                'verbose_name': 'новостное уведомление',
                'verbose_name_plural': 'новостные уведомления',
                'ordering': ['-created_at'],
            },
        ),
        migrations.CreateModel(
            name='Signal',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('guid', models.CharField(default=web.services.helpers.get_guid, max_length=50, unique=True)),
                ('trade_type', models.CharField(choices=[('buy', 'buy'), ('sell', 'sell')], max_length=4, verbose_name='тип сделки')),
                ('order_type', models.CharField(choices=[('market', 'market'), ('limit', 'limit')], max_length=20, verbose_name='тип ордера')),
                ('price', models.DecimalField(blank=True, decimal_places=6, default=None, max_digits=12, null=True, verbose_name='цена')),
                ('tp', models.DecimalField(blank=True, decimal_places=6, default=None, max_digits=12, null=True, verbose_name='take profit')),
                ('sl', models.DecimalField(blank=True, decimal_places=6, default=None, max_digits=12, null=True, verbose_name='stop loss')),
                ('created_at', models.DateTimeField(verbose_name='время создания')),
                ('countdown', models.IntegerField(default=60, verbose_name='отсчет, секунд')),
                ('result', models.IntegerField(blank=True, default=None, null=True, verbose_name='результат, pips')),
                ('publish_at_paid', models.DateTimeField(help_text='Индексируется автоматически', verbose_name='время публикации')),
                ('is_closed', models.BooleanField(default=False, verbose_name='завершен')),
                ('market', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='market_signals', to='sgn.Market', verbose_name='рынок')),
                ('owner', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='owner_signals', to=settings.AUTH_USER_MODEL, verbose_name='сиг. админ')),
            ],
            options={
                'verbose_name': 'сигнал',
                'verbose_name_plural': 'сигналы',
                'ordering': ['-created_at'],
            },
        ),
        migrations.CreateModel(
            name='SignalFilter',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('is_sound', models.BooleanField(default=False)),
                ('is_all_symbols', models.BooleanField(default=True)),
                ('market', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='sgn.Market', verbose_name='рынок')),
            ],
            options={
                'verbose_name': 'сигналы фильтр',
                'verbose_name_plural': 'сигналы фильтр',
                'ordering': ['id'],
            },
        ),
        migrations.CreateModel(
            name='SignalHide',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('signal', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='sgn.Signal')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'signal hide',
                'verbose_name_plural': 'signals hide',
                'ordering': ['-created_at'],
            },
        ),
        migrations.CreateModel(
            name='SignalType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=100, unique=True, verbose_name='код')),
                ('name', models.CharField(max_length=100, verbose_name='название')),
                ('class_name', models.CharField(max_length=100, verbose_name='css класс')),
                ('sort', models.IntegerField(default=0, verbose_name='сортировка')),
            ],
            options={
                'verbose_name': 'тип сигнала',
                'verbose_name_plural': 'типы сигналов',
                'ordering': ['sort'],
            },
        ),
        migrations.CreateModel(
            name='Symbol',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=100, unique=True, verbose_name='код')),
                ('name', models.CharField(max_length=100, verbose_name='название')),
                ('prec', models.IntegerField(default=2, verbose_name='кол-во знаков после запятой')),
                ('sort', models.IntegerField(default=0, verbose_name='сортировка')),
                ('market', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='symbols', to='sgn.Market', verbose_name='рынок')),
            ],
            options={
                'verbose_name': 'инструмент',
                'verbose_name_plural': 'инструменты',
                'ordering': ['sort'],
            },
        ),
        migrations.CreateModel(
            name='Tariff',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(help_text='для внутренней маркировки', max_length=50, unique=True, verbose_name='код')),
                ('name', models.CharField(max_length=100, verbose_name='название')),
                ('subtitle', models.CharField(max_length=100, verbose_name='подзаголовок')),
                ('period', models.IntegerField(verbose_name='период')),
                ('period_type', models.CharField(choices=[('day', 'дней'), ('month', 'месяцев')], max_length=10, verbose_name='тип периода')),
                ('price', models.DecimalField(decimal_places=2, max_digits=10, verbose_name='стоимость')),
                ('is_active', models.BooleanField(default=True, verbose_name='активен')),
                ('header_css_class', models.CharField(blank=True, default=None, max_length=100, null=True, verbose_name='css класс хеадера')),
                ('sale', models.IntegerField(blank=True, default=None, null=True, verbose_name='скидка')),
                ('sort', models.IntegerField(default=0, verbose_name='сортировка')),
                ('market', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='sgn.Market')),
                ('signal_types', models.ManyToManyField(to='sgn.SignalType', verbose_name='типы сигналов')),
                ('symbols', models.ManyToManyField(to='sgn.Symbol', verbose_name='символы')),
            ],
            options={
                'verbose_name': 'тариф',
                'verbose_name_plural': 'тарифы',
                'ordering': ['sort'],
            },
        ),
        migrations.CreateModel(
            name='TariffRow',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, verbose_name='название')),
                ('class_name', models.CharField(max_length=100, verbose_name='css класс')),
                ('sort', models.IntegerField(default=0, verbose_name='сортировка')),
                ('tariff', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='rows', to='sgn.Tariff', verbose_name='тариф')),
            ],
            options={
                'verbose_name': 'тариф строка',
                'verbose_name_plural': 'тариф строки',
                'ordering': ['sort'],
            },
        ),
        migrations.CreateModel(
            name='TariffUser',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_from', models.DateField(verbose_name='Дата с')),
                ('date_to', models.DateField(verbose_name='Дата до')),
                ('is_paid', models.BooleanField(default=False, help_text='Индексируется автоматически', verbose_name='оплачен')),
                ('market', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='sgn.Market')),
                ('tariff', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='sgn.Tariff')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'тариф пользователя',
                'verbose_name_plural': 'тарифы пользователей',
                'ordering': ['-date_from'],
            },
        ),
        migrations.AddField(
            model_name='signalfilter',
            name='symbols',
            field=models.ManyToManyField(to='sgn.Symbol', verbose_name='инструменты'),
        ),
        migrations.AddField(
            model_name='signalfilter',
            name='timezone',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='web.Timezone'),
        ),
        migrations.AddField(
            model_name='signalfilter',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='пользователь'),
        ),
        migrations.AddField(
            model_name='signal',
            name='signal_type',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='signal_type_signals', to='sgn.SignalType', verbose_name='тип сигнала'),
        ),
        migrations.AddField(
            model_name='signal',
            name='symbol',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='symbol_signals', to='sgn.Symbol', verbose_name='инструмент'),
        ),
    ]
