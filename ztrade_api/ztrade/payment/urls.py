from django.urls import re_path
from .import views as v

urlpatterns = [
    re_path(
        r'^(?P<version>(v1))/payment/create_bill/product/auth$',
        v.ProductCreateBillAuth.as_view(),
        name='product_create_bill_auth'),

    re_path(
        r'^(?P<version>(v1))/payment/create_bill/product/not_auth$',
        v.ProductCreateBillNotAuth.as_view(),
        name='product_create_bill_not_auth'),

    re_path(
        r'^(?P<version>(v1))/payment/create_bill/subscription$',
        v.SubscriptionCreateBill.as_view(),
        name='subscription_create_bill'),

    re_path(
        r'^(?P<version>(v1))/payment/create_bill/tariff$',
        v.TariffCreateBill.as_view(),
        name='tariff_create_bill'),

    re_path(
        r'^(?P<version>(v1))/payment/check_order$',
        v.PaymentCheckOrder.as_view(),
        name='payment_check_order'),
]
