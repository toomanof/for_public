from django.http import HttpResponse
from rest_framework.views import APIView
from rest_framework.generics import CreateAPIView

from payment.serializers import ProductCreateBillSerializer
from payment.serializers import SubscriptionCreateBillSerializer
from payment.serializers import TariffCreateBillSerializer

from payment.models import YandexPayment
from web.views import AuthMixin


class ProductCreateBillAuth(AuthMixin, CreateAPIView):
    """
    post:
        <h1>Create bill and related YandexPayment (request to Yandex)</h1>
        <b>Response:</b> bill object<br />
        <b>Response code:</b> 201
    """
    serializer_class = ProductCreateBillSerializer

    def get_serializer_context(self):
        return {'user': self.request.user}


class ProductCreateBillNotAuth(CreateAPIView):
    """
    post:
        <h1>Create bill and related YandexPayment (request to Yandex)</h1>
        <b>Response:</b> bill object<br />
        <b>Response code:</b> 201
    """
    serializer_class = ProductCreateBillSerializer

    def get_serializer_context(self):
        return {'user': None}


class SubscriptionCreateBill(AuthMixin, CreateAPIView):
    """
    post:
        <h1>Create bill and related YandexPayment (request to Yandex)</h1>
        <b>Response:</b> bill object<br />
        <b>Response code:</b> 201
    """
    serializer_class = SubscriptionCreateBillSerializer

    def get_serializer_context(self):
        return {'user': self.request.user}


class TariffCreateBill(AuthMixin, CreateAPIView):
    """
    post:
        <h1>Create bill and related YandexPayment (request to Yandex)</h1>
        <b>Response:</b> bill object<br />
        <b>Response code:</b> 201
    """
    serializer_class = TariffCreateBillSerializer

    def get_serializer_context(self):
        return {'user': self.request.user}


class PaymentCheckOrder(APIView):
    """
    Webhook notification from Yandex about payment.
    As we create all payments with capture = True,
    we don't need to confirm payment.
    All we need from notifications - is succeeded payments.
    """
    def post(self, request, **kwargs):
        res = YandexPayment.objects.check_webhook(request.data)
        if res is True:
            return HttpResponse(status=200)
        return HttpResponse(status=400)
