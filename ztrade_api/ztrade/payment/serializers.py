from django.shortcuts import get_object_or_404
from rest_framework.serializers import ModelSerializer
from rest_framework.serializers import IntegerField
from rest_framework.serializers import EmailField
from rest_framework.serializers import CharField

from account.models import Subscription
from account.models import SubscriptionUser
from shop.models import Product
from shop.models import ProductUser
from sgn.models import Tariff
from sgn.models import TariffUser
from payment.models import Bill
from payment.models import YandexPayment


class YandexPaymentSerializer(ModelSerializer):
    class Meta:
        model = YandexPayment
        fields = (
            'id',
            'status',
            'currency',
            'amount',
            'description',
            'confirmation_url',
        )


class BillSerializer(ModelSerializer):
    yandex_payment = YandexPaymentSerializer(read_only=True)

    class Meta:
        model = Bill
        fields = (
            'id',
            'name',
            'user_email',
            'amount',
            'currency',
            'created_at',
            'paid_at',
            'is_paid',
            'yandex_payment'
        )


class ProductCreateBillSerializer(ModelSerializer):
    product_id = IntegerField()
    email = EmailField()
    name = CharField()
    phone = CharField(default='', allow_blank=True)

    class Meta:
        model = Bill
        fields = (
            'product_id',
            'email',
            'name',
            'phone'
        )

    def to_representation(self, instance):
        return BillSerializer(instance).data

    def validate(self, data):
        get_object_or_404(Product, id=data['product_id'])
        return data

    def create(self, validated_data):
        d = validated_data
        user = self.context.get('user')
        user_id = user.id if user is not None else None
        bill = ProductUser.objects.create_bill(
            user_id, d['product_id'], d['email'], d['name'], d['phone'])
        YandexPayment.objects.create_payment(bill.id)
        return bill


class SubscriptionCreateBillSerializer(ModelSerializer):
    subscription_id = IntegerField()
    num_months = IntegerField()

    class Meta:
        model = Bill
        fields = (
            'subscription_id',
            'num_months'
        )

    def to_representation(self, instance):
        return BillSerializer(instance).data

    def validate(self, data):
        get_object_or_404(Subscription, id=data['subscription_id'])
        return data

    def create(self, validated_data):
        d = validated_data
        user = self.context.get('user')
        bill = SubscriptionUser.objects.create_bill(
            user.id, d['subscription_id'], d['num_months'])
        YandexPayment.objects.create_payment(bill.id)
        return bill


class TariffCreateBillSerializer(ModelSerializer):
    tariff_id = IntegerField()

    def to_representation(self, instance):
        return BillSerializer(instance).data

    class Meta:
        model = Bill
        fields = (
            'tariff_id',
        )

    def validate(self, data):
        get_object_or_404(Tariff, id=data['tariff_id'])
        return data

    def create(self, validated_data):
        d = validated_data
        user = self.context.get('user')
        bill = TariffUser.objects.create_bill(user.id, d['tariff_id'])
        YandexPayment.objects.create_payment(bill.id)
        return bill
