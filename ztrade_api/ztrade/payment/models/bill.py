from django.db.models import Manager
from django.db.models import Model
from django.contrib import admin
from django.apps import apps
from django.utils.html import mark_safe
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.db.models import ForeignKey
from django.db.models import BooleanField
from django.db.models import CharField
from django.db.models import DateTimeField
from django.db.models import DecimalField
from django.db.models import SET_NULL
from payment.constants import RUB


class BillManager(Manager):
    pass


class Bill(Model):
    CURRENCY_CHOICES = (
        (RUB, 'рубль'),
    )

    user = ForeignKey('web.User', on_delete=SET_NULL,
                      null=True, default=None, blank=True)
    tariff_user = ForeignKey('sgn.TariffUser', on_delete=SET_NULL,
                             null=True, default=None, blank=True,
                             verbose_name='покупка тарифа')
    product_user = ForeignKey('shop.ProductUser', on_delete=SET_NULL,
                              null=True, default=None, blank=True,
                              verbose_name='покупка товара')
    subscription_user = ForeignKey('account.SubscriptionUser',
                                   on_delete=SET_NULL,
                                   null=True, default=None, blank=True,
                                   verbose_name='покупка подписки')
    name = CharField('название', max_length=500)
    user_email = CharField('email', max_length=255, null=True,
                           default=None, blank=True)
    currency = CharField('валюта', max_length=5, default=RUB,
                         choices=CURRENCY_CHOICES)
    amount = DecimalField('сумма', max_digits=10, decimal_places=2,
                          null=True, default=None, blank=True)
    created_at = DateTimeField('дата создания', auto_now_add=True)
    is_paid = BooleanField('оплачен', default=False,
                           help_text='Индексируется автоматически')
    paid_at = DateTimeField('дата оплаты', null=True, default=None, blank=True,
                            help_text='Индексируется автоматически')

    objects = BillManager()

    class Meta:
        app_label = 'payment'
        verbose_name = 'счет на оплату'
        verbose_name_plural = 'счета на оплату'
        ordering = ['-created_at']

    def __str__(self):
        return self.name

    @property
    def yandex_payment(self):
        YandexPayment = apps.get_model('payment', 'YandexPayment')
        return YandexPayment.objects.filter(bill_id=self.id).first()

    def success_payment_post_process(self):
        """
        When bill got payment, make some actions.
        Send emails, etc.
        """
        Email = apps.get_model('web', 'Email')
        Emailer = apps.get_model('web', 'Emailer')

        if self.product_user is not None:
            pu = self.product_user
            Emailer.objects.process_email(
                Email.PAY_PRODUCT, bill=self, email=self.user_email, pu=pu)
        elif self.subscription_user is not None:
            su = self.subscription_user
            Emailer.objects.process_email(
                Email.PAY_SUBSCRIPTION, bill=self, user=su.user, su=su)
        elif self.tariff_user is not None:
            tu = self.tariff_user
            Emailer.objects.process_email(
                Email.PAY_TARIFF, bill=self, user=tu.user, tu=tu)


@receiver(post_save, sender=Bill)
def reindex(sender, instance, created, **kwargs):
    """
    Reindex itself.
    Reindex related service.
    """
    YandexPayment = apps.get_model('payment', 'YandexPayment')

    if created:
        if instance.user and not instance.user_email:
            instance.user_email = instance.user.email

    yp = YandexPayment.objects.filter(bill_id=instance.id).first()
    if yp is not None and yp.is_paid:
        Bill.objects.filter(
            id=instance.id).update(is_paid=True, paid_at=yp.paid_at)

        if instance.tariff_user is not None:
            instance.tariff_user.is_paid = True
            instance.tariff_user.save()
        elif instance.subscription_user is not None:
            instance.subscription_user.is_paid = True
            instance.subscription_user.save()
        elif instance.product_user is not None:
            instance.product_user.is_paid = True
            instance.product_user.save()


@admin.register(Bill)
class BillAdmin(admin.ModelAdmin):
    actions = None
    list_display = (
        'created_at',
        'name',
        'user',
        'admin_service_link',
        'admin_payment_link',
        'amount',
        'currency',
        'is_paid',
        'paid_at'
    )
    exclude = (
        'is_paid',
        'paid_at',
    )
    readonly_fields = (
        'user',
        'created_at',
        'name',
        'tariff_user',
        'product_user',
        'subscription_user',
        'amount',
        'currency',
        'user_email',
    )

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def admin_service_link(self, obj):
        if obj.product_user:
            link = '/admin/shop/productuser/{}/'.format(
                obj.product_user.id)
            s = '<a href="{}">покупка товар</a>'.format(link)
            return mark_safe(s)
        elif obj.subscription_user:
            link = '/admin/account/subscriptionuser/{}/'.format(
                obj.subscription_user.id)
            s = '<a href="{}">покупка подписки</a>'.format(link)
            return mark_safe(s)
        elif obj.tariff_user:
            link = '/admin/sgn/tariffuser/{}/'.format(
                obj.tariff_user.id)
            s = '<a href="{}">покупка тарифа</a>'.format(link)
            return mark_safe(s)
        return ''
    admin_service_link.short_description = 'товар/услуга'

    def admin_payment_link(self, obj):
        yp = obj.yandex_payment
        if yp is None:
            return '-'

        link = '/admin/payment/yandexpayment/{}/'.format(yp.id)
        s = '<a href="{}">платеж</a>'.format(link)
        return mark_safe(s)
    admin_payment_link.short_description = 'оплата'
