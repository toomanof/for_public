import json
import logging
from django.core.exceptions import ValidationError
from django.utils.html import mark_safe
from django.utils import timezone
from django.db.models import Manager
from django.db.models import Model
from django.contrib import admin
from django.apps import apps
from django.conf import settings
from django.dispatch import receiver
from django.db.models.signals import post_save
from django.db.models import BooleanField
from django.db.models import DateTimeField
from django.db.models import DecimalField
from django.db.models import CharField
from django.db.models import OneToOneField
from django.db.models import TextField
from django.db.models import CASCADE
from payment.constants import RUB
from web.services.helpers import get_guid

from yandex_checkout import Configuration
from yandex_checkout import Payment
from yandex_checkout import WebhookNotification


logger = logging.getLogger('app')


class YandexPaymentManager(Manager):
    def create_payment(self, bill_id):
        """
        Creates YandexPayment.
        Send request to Yandex and fill YandexPayment.
        Returns YandexPayment.
        """
        Bill = apps.get_model('payment', 'Bill')
        bill = Bill.objects.get(id=bill_id)

        yp = self.model.objects.filter(bill_id=bill_id).first()
        if yp is None:
            yp = self.model.objects.create(
                bill=bill,
                currency=bill.currency,
                amount=bill.amount,
                description=bill.name,
            )

        Configuration.account_id = settings.YANDEX_SHOP_ID
        Configuration.secret_key = settings.YANDEX_SECRET_KEY

        obj = {
            'amount': {
                # 'value': yp.amount, TODO return after testing
                'value': 1,
                'currency': yp.currency
            },
            'capture': True,  # auto confirmation
            'confirmation': {
                'type': 'redirect',
                'return_url': yp.return_url
            },
            'description': '№{}. '.format(bill.id) + yp.description
        }

        try:
            r = Payment.create(obj, yp.guid)
            msg = 'Created payment at Yandex, id: {}, bill: {}'.format(
                r.id, bill.id)
            logger.info(msg)
        except Exception as e:
            msg = 'Could not create payment at Yandex: ' + str(e)
            logger.error(msg)
            msg = 'Произошла ошибка. Пожалуйста попробуйте позже.'
            raise ValidationError(msg)

        yp.yandex_id = r.id
        yp.created_at = timezone.now()
        yp.confirmation_url = r.confirmation.confirmation_url
        yp.save()
        return yp

    def check_webhook(self, data):
        """
        Check Yandex webhook notification.
        Returns bool.
        """
        msg = 'Notification webhook\n'
        msg += json.dumps(data, indent=4)
        logger.info(msg)

        try:
            notification = WebhookNotification(data)
        except Exception as e:
            msg = 'Webhook notification error: ' + str(e)
            logger.error(msg)
            return False

        payment = notification.object

        # Check and process webhook
        return self.finalize_payment(payment.id)

    def finalize_payment(self, payment_id, is_testing=False):
        """
        Request to Yandex to get Payment by id.
        Then check payment for status SUCCEEDED
        Returns bool.
        """
        msg = 'Getting payment by id: {}'.format(payment_id)
        logger.info(msg)

        try:
            payment = Payment.find_one(payment_id)
        except Exception as e:
            msg = 'Could not get payment by id: {}. {}'.format(
                payment_id, str(e))
            logger.error(msg)
            return False

        msg = 'Payment status: {}'.format(payment.status)
        logger.info(msg)

        if is_testing:
            payment.status = self.model.SUCCEEDED

        if payment.status == self.model.SUCCEEDED:
            yp = self.model.objects.filter(yandex_id=payment.id).first()
            if yp is not None:
                yp.status = self.model.SUCCEEDED
                yp.paid_at = timezone.now()
                yp.save()

                yp.bill.success_payment_post_process()

        return True


class YandexPayment(Model):
    PENDING = 'pending'
    WAITING_FOR_CAPTURE = 'waiting_for_capture'
    SUCCEEDED = 'succeeded'
    CANCELED = 'canceled'

    STATUS_CHOICES = (
        (PENDING, 'в ожидании оплаты'),
        (WAITING_FOR_CAPTURE, 'ожидает подтверждения'),
        (SUCCEEDED, 'оплачен'),
        (CANCELED, 'отменен'),
    )

    CURRENCY_CHOICES = (
        (RUB, 'рубль'),
    )

    bill = OneToOneField('Bill', on_delete=CASCADE, verbose_name='счет')
    guid = CharField(max_length=50, default=get_guid, unique=True)
    yandex_id = CharField('Yandex Id', max_length=100, null=True,
                          default=None, blank=True, unique=True)
    status = CharField('статус', max_length=25, default=PENDING,
                       choices=STATUS_CHOICES)
    currency = CharField('валюта', max_length=5, default=RUB,
                         choices=CURRENCY_CHOICES)
    amount = DecimalField('сумма', max_digits=10, decimal_places=2)
    description = CharField('описание платежа', max_length=500)
    confirmation_url = CharField(max_length=500, null=True, default=None,
                                 blank=True)
    cancelation_party = CharField(max_length=255, null=True, default=None,
                                  blank=True)
    cancelation_reason = CharField(max_length=255, null=True, default=None,
                                   blank=True)
    debug_data = TextField(null=True, default=None, blank=True)
    meta_data = TextField('мета данные', null=True,
                          default=None, blank=True)
    payment_method = TextField('платежный метод', null=True,
                               default=None, blank=True)
    created_at = DateTimeField('создан', auto_now_add=True)
    expire_at = DateTimeField('истекает', null=True, default=None, blank=True)
    paid_at = DateTimeField('истекает', null=True, default=None, blank=True)
    is_paid = BooleanField('оплачен', default=False,
                           help_text='Индексируется по статусу')

    objects = YandexPaymentManager()

    class Meta:
        app_label = 'payment'
        verbose_name = 'платеж'
        verbose_name_plural = 'платежи'
        ordering = ['-created_at']

    def __str__(self):
        return self.description

    @property
    def return_url(self):
        url = settings.FRONTEND_URL
        if self.bill.tariff_user is not None:
            return url + '/payment/success/tariff'

        if self.bill.subscription_user is not None:
            return url + '/payment/success/subscription'

        return url + '/payment/success/product'

    def get_json_data(self, field):
        value = getattr(self, field)
        if value is None:
            return ''
        try:
            value = json.loads(value)
        except Exception:
            value = ''
        return value

    def set_json_data(self, field, data):
        try:
            value = json.dumps(data, indent=4)
            setattr(self, field, value)
        except Exception as e:
            return

    def get_debug_data(self):
        return self.get_json_data('debug_data')

    def set_debug_data(self, data):
        self.set_json_data('debug_data', data)

    def get_meta_data(self):
        return self.get_json_data('meta_data')

    def set_meta_data(self, data):
        self.set_json_data('meta_data', data)

    def get_payment_method(self):
        return self.get_json_data('payment_method')

    def set_payment_method(self, data):
        self.set_json_data('payment_method', data)

    def save(self, *args, **kwargs):
        if self.status == self.SUCCEEDED:
            self.is_paid = True
        else:
            self.is_paid = False
        super().save(*args, **kwargs)


@receiver(post_save, sender=YandexPayment)
def reindex(sender, instance, created, **kwargs):
    """
    Reindex corresponding Bill automatically.
    """
    if instance.bill is not None:
        instance.bill.save()


@admin.register(YandexPayment)
class YandexPaymentAdmin(admin.ModelAdmin):
    actions = None
    list_display = (
        'created_at',
        'admin_bill_link',
        'status',
        'currency',
        'amount',
        'description',
        'paid_at',
        'is_paid',
    )

    exclude = ('guid', 'confirmation_url')

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

#    def get_readonly_fields(self, request, obj=None):
#        if obj:
#            self.readonly_fields = [
#                field.name for field in obj.__class__._meta.fields]
#        return self.readonly_fields

    def admin_bill_link(self, obj):
        if not obj.bill:
            return '-'

        link = '/admin/payment/bill/{}/'.format(obj.bill.id)
        s = '<a href="{}">счет №{}</a>'.format(link, obj.bill.id)
        return mark_safe(s)
    admin_bill_link.short_description = 'счет'
