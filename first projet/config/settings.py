"""
Django settings for srm_lab project.

Generated by 'django-admin startproject' using Django 3.2.3.

For more information on this file, see
https://docs.djangoproject.com/en/3.2/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/3.2/ref/settings/
"""
import os
from pathlib import Path
from celery.schedules import crontab
from django.conf.locale.ru import formats as ru_formats

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent
CONFIG_DIR = os.path.join(BASE_DIR, 'config')


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.2/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'django-insecure-94)5o6jkzk-3-ejdy7fnino)83*v*j(g^(vct*34y8o#8l-1+e'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'oauth2_provider',
    'rest_framework',
    'drf_yasg',
    'knox',
    'phonenumber_field',
    'django_extensions',
    'core',
    'appointment',
    'newsletters',
    'billing',

]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    # 'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'core.middleware.CurrentUserMiddleware',
]

ROOT_URLCONF = 'config.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.i18n'
            ],
        },
    },
]

WSGI_APPLICATION = 'config.wsgi.application'


# Database
# https://docs.djangoproject.com/en/3.2/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': BASE_DIR / 'db.sqlite3',
    }
}


# Password validation
# https://docs.djangoproject.com/en/3.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


CORS_ORIGIN_ALLOW_ALL = True

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'core.auth.TokenAuthentication',
    ),
    'DATETIME_FORMAT': "iso-8601",
    'DEFAULT_SCHEMA_CLASS': 'rest_framework.schemas.coreapi.AutoSchema',

    'DEFAULT_RENDERER_CLASSES': (
        'djangorestframework_camel_case.render.CamelCaseJSONRenderer',
        'djangorestframework_camel_case.render.CamelCaseBrowsableAPIRenderer',
    ),

    'DEFAULT_PARSER_CLASSES': (
        'djangorestframework_camel_case.parser.CamelCaseFormParser',
        'djangorestframework_camel_case.parser.CamelCaseMultiPartParser',
        'djangorestframework_camel_case.parser.CamelCaseJSONParser',
    ),
    'JSON_UNDERSCOREIZE': {
        'no_underscore_before_number': True,
    },
}


TIME_ZONE = 'Europe/Moscow'

# Internationalization
# https://docs.djangoproject.com/en/3.2/topics/i18n/

LANGUAGE_CODE = 'ru-ru'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.2/howto/static-files/


STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static')

# Default primary key field type
# https://docs.djangoproject.com/en/3.2/ref/settings/#default-auto-field

DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'

EMAIL_FROM_SENDGRID = ''
SENDGRID_API_KEY = ''

APPEND_SLASH = False


TWILIO_ACCOUNT_SID = ''
TWILIO_TOKEN = ''
TWILIO_NUMBER = '+12672145259'

SMS_RU_ACCOUNT_ID = '40C9D1A9-46A4-5F39-516A-FF9872C62272'

MIN_VALUE_PHONE_CODE = 11111
MAX_VALUE_PHONE_CODE = 99999
LIFETIME_AUTH_PHONE_CODE = 300  # в секундах
LIFETIME_CHECK_AUTH_PHONE_CODE = 60 * 20  # по умолчанию 20мин

KEY_SENDGRID = ''

AUTHENTICATION_BACKENDS = (
    'oauth2_provider.backends.OAuth2Backend',
    # and maybe some others ...
    'django.contrib.auth.backends.ModelBackend',
)

OAUTH2_PROVIDER_APPLICATION_MODEL = 'oauth2_provider.Application'

FIREBASE_SERVER_KEY = ''
GOOGLE_APP_CLIENT_ID = ''

DEFAULT_FILE_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'

AWS_ACCESS_KEY_ID = ''
AWS_SECRET_ACCESS_KEY = ''

AWS_STORAGE_BUCKET_NAME = 'graine'
AWS_S3_ENDPOINT_URL = 'https://ams3.digitaloceanspaces.com'
AWS_S3_CUSTOM_DOMAIN = 'spaceone.grain.club'
AWS_S3_OBJECT_PARAMETERS = {
    'CacheControl': 'max-age=86400',
}
AWS_LOCATION = 'static'
AWS_DEFAULT_ACL = 'public-read'
AWS_REGION_PROD = 'eu-central-1'
AWS_STORAGE_BUCKET_NAME_PROD = 'spaceone'

CELERY_BROKER_URL = 'redis://127.0.0.1:6379'
CELERY_RESULT_BACKEND = 'redis://127.0.0.1:6379'
CELERY_ACCEPT_CONTENT = ['application/json']
CELERY_RESULT_SERIALIZER = 'json'
CELERY_TASK_SERIALIZER = 'json'
CELERY_TIMEZONE = 'Europe/Moscow'

CELERY_BEAT_SCHEDULE = {
    'send_push_remind_about_procedures_in_today': {
        'task': 'appointment.tasks.send_push_remind_about_procedures_in_today',
        'schedule': crontab(0, 8),
        'args': (),
    },
    'send_push_remind_about_unmarked_clients': {
        'task': 'appointment.tasks.send_push_remind_about_unmarked_clients',
        'schedule': crontab(0, 20),
        'args': (),
    },
    'send_push_before_procedure': {
        'task': 'appointment.tasks.send_push_before_procedure',
        'schedule': crontab(minute='*/5'),
        'args': (),
    },
    'checking_activity_subscriptions': {
        'task': 'billing.tasks.checking_activity_subscriptions',
        'schedule': crontab(0, 2),
        'args': (),
    },
    'calculate_active_all_clients': {
        'task': 'core.tasks.calculate_active_all_clients',
        'schedule': crontab(0, 1),
        'args': (),
    },
    'send_sms_newsletters_not_regular': {
        'task': 'newsletters.tasks.send_sms_newsletters_not_regular',
        'schedule': crontab(minute='*/3'),
        'args': (),
    },
    'create_birthday_queues_beginning_of_day': {
        'task': 'newsletters.tasks.create_birthday_queues_beginning_of_day',
        'schedule': crontab(0, 5),
        'args': (),
    },
    'create_holiday_queues_gender_holiday': {
        'task': 'newsletters.tasks.create_holiday_queues_gender_holiday',
        'schedule': crontab(30, 5),
        'args': (),
    },
    'create_queues_default_newsletters': {
        'task': 'newsletters.tasks.create_queues_default_newsletters',
        'schedule': crontab(minute='*/30'),
        'args': (),
    },
    'send_sms_birthdays': {
        'task': 'newsletters.tasks.send_sms_birthdays',
        'schedule': crontab(0, 9),
        'args': (),
    },
}

ru_formats.DATE_FORMAT = 'd.m.Y'
ru_formats.TIME_FORMAT = 'H:i'
ru_formats.DATETIME_FORMAT = 'd.m.Y H:i'
DATE_FORMAT = 'j E Y'
TIME_FORMAT = 'H:i'
DATETIME_FORMAT = 'j E Y H:i'

PHONE_NUMBER_FREE_ACCOUNT = '+16505601020'
SMS_CODE_FREE_ACCOUNT = '65057'


PASSWORD_APP_STORE = ''
