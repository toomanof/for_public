from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi


schema_view = get_schema_view(
   openapi.Info(
      title="API CMM",
      default_version='v1',
      description="Test description",
      terms_of_service="https://www.google.com/policies/terms/",
      contact=openapi.Contact(email="contact@snippets.local"),
      license=openapi.License(name="BSD License"),
   ),
   public=True,
   permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/account/', include('core.urls.account_api')),
    path('api/v1/account/', include('core.urls.urls_api_oauth2')),
    path('api/v1/profile', include('core.urls.profile_api')),
    path('api/v1/', include('core.urls.procedure_api')),
    path('api/v1/', include('appointment.urls')),
    path('api/v1/', include('newsletters.urls')),
    path('api/v1/', include('core.urls.client_api')),
    path('api/v1/', include('billing.urls')),

    path('swagger.json', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    path('api/v1/doc', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('api/redoc', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
]

urlpatterns += staticfiles_urlpatterns()

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
