from .settings import *

DEBUG = True


PATH_LOG_FILES = '/var/log/crm/'
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'console': {
            'format': '%(name)-12s %(levelname)-8s %(message)s'
        },
        'file': {
            'format': '%(asctime)s %(name)-12s %(levelname)-8s %(message)s'
        }
    },
    'handlers': {
        'file_warning': {
            'level': 'WARNING',
            'class': 'logging.FileHandler',
            'formatter': 'file',
            'filename': f'{PATH_LOG_FILES}errors.log'
        },
        'file_info': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'formatter': 'file',
            'filename': f'{PATH_LOG_FILES}info.log'
        },
        'file_timer': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'formatter': 'file',
            'filename': f'{PATH_LOG_FILES}timer.log'
        },
        'file_debug': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'formatter': 'file',
            'filename': f'{PATH_LOG_FILES}debug.log'
        },
        'celery': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'formatter': 'file',
            'filename': f'{PATH_LOG_FILES}celery.log'
        },
    },
    'loggers': {
        'debug': {
            'level': 'DEBUG',
            'handlers': ['file_debug']
        },
        'warning': {
            'level': 'WARNING',
            'handlers': ['file_warning']
        },
        'info': {
            'level': 'INFO',
            'handlers': ['file_info']
        },
        'timer': {
            'level': 'INFO',
            'handlers': ['file_timer']
        },
        'django.request': {
            'level': 'INFO',
            'handlers': ['file_info']
        },
        'celery': {
            'level': 'INFO',
            'handlers': ['celery']
        },
    }
}