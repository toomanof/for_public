from django.utils import timezone
from core.models import RegisterRestrictionModel
from billing.models import ReestrSubscription


class SubscriptionManagementService:
    """
    Класс отвечающий за ограничения действий без подписок
    и предоставление всех доступых возможностей при оформлении подписки
    """

    def __init__(self, request):
        self.request = request

    def save_restriction(self, **kwargs):
        RegisterRestrictionModel.objects.create(
            creator=self.request.current_user,
            procedure=kwargs.get('procedure', None),
            client=kwargs.get('client', None),
            appointment=kwargs.get('appointment', None),
        )

    def save_procedure_to_restriction(self, procedure: object):
        if self.has_ability_create_multiple_procedures:
            self.save_restriction(procedure=procedure)

    def save_client_to_restriction(self, client:object):
        if self.has_ability_create_multiple_clients:
            self.save_restriction(client=client)

    def save_appointment_to_restriction(self, appointment: object):
        if self.has_ability_create_multiple_appointments:
            self.save_restriction(appointment=appointment)

    @property
    def has_subscription(self):
        return True  # временно пока не запуститься в прод релиз
        #return ReestrSubscription.objects.filter(
        #    user=self.request.current_user,
        #    active=True
        #).exists()

    @property
    def has_ability_create_multiple_clients(self):
        """
        Проверка имеет ли возможность создавать несколько клиентов
        текущий пользователь
        """
        return (self.has_subscription or
                not RegisterRestrictionModel.objects.filter(
                    creator=self.request.current_user,
                    client__isnull=False
                ).exists())

    @property
    def has_ability_create_multiple_procedures(self):
        """
        Проверка имеет ли возможность создавать несколько процедур
        текущий пользователь
        """
        return (self.has_subscription or
                not RegisterRestrictionModel.objects.filter(
                    creator=self.request.current_user,
                    procedure__isnull=False
                ).exists())

    @property
    def has_ability_create_multiple_appointments(self):
        """
        Проверка имеет ли возможность создавать несколько записей в день
        текущий пользователь
        """
        return (self.has_subscription or
                not RegisterRestrictionModel.objects.filter(
                    creator=self.request.current_user,
                    appointment__isnull=False,
                    date_create=timezone.now().date()
                ).exists())
