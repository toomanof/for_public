import requests

from icecream import ic
from django.conf import settings


SEND = 'send'
COST = 'cost'
SERVICE_CODES = {
    100: "Сообщение принято к отправке."
         "На следующих строчках вы найдете идентификаторы отправленных сообщений"
         "в том же порядке, в котором вы указали номера, на которых совершалась отправка.",
    200: "Неправильный api_id",
    201: "Не хватает средств на лицевом счету",
    202: "Неправильно указан получатель",
    203: "Нет текста сообщения",
    204: "Имя отправителя не согласовано с администрацией",
    205: "Сообщение слишком длинное (превышает 8 СМС)",
    206: "Будет превышен или уже превышен дневной лимит на отправку сообщений",
    207: "На этот номер (или один из номеров) нельзя отправлять сообщения,"
         "либо указано более 100 номеров в списке получателей",
    208: "Параметр time указан неправильно",
    209: "Вы добавили этот номер (или один из номеров) в стоп-лист",
    210: "Используется GET, где необходимо использовать POST",
    211: "Метод не найден",
    220: "Сервис временно недоступен, попробуйте чуть позже.",
    300: "Неправильный token (возможно истек срок действия, либо ваш IP изменился)",
    301: "Неправильный пароль, либо пользователь не найден",
    302: "Пользователь авторизован, но аккаунт не подтвержден"
         "(пользователь не ввел код, присланный в регистрационной смс)",
}


class SMSRuService:
    """
    Класс работы с возможностями сервиса sms.ru
    Документация: https://sms.ru/api
    """

    def __init__(self, to_number: str, body: str):
        self.account_sid = settings.SMS_RU_ACCOUNT_ID
        self.to_number = to_number
        self.body = body

    def get_url_sms(self, action: str) -> str:
        return f'https://sms.ru/sms/{action}?api_id={self.account_sid}&to={self.to_number}&msg={self.body}&json=1'

    @property
    def url_send_sms(self) -> str:
        """
        Формирование урл для отправки СМС сообщение HTTP запросом
        """
        return self.get_url_sms(SEND)

    @property
    def url_cost_sms(self) -> str:
        """
        Формирование урл для получения стоимость перед
        отправкой СМС сообщение HTTP запросом
        """
        return self.get_url_sms(COST)

    def _send_get_request(self, url) -> dict:
        ic(url)
        response = requests.get(url=url)
        return response.json()

    def send_sms(self) -> dict:
        """
        Отправка смс.
        """
        result = self._send_get_request(self.url_send_sms)
        status_code = result.get('status_code', None)

        assert status_code and status_code == 100, SERVICE_CODES[status_code]
        return result

    def cost_sms(self) -> dict:
        """
        Стоимость смс.
        """
        result = self._send_get_request(self.url_cost_sms)
        status_code = result.get('status_code', None)

        assert status_code and status_code == 100, SERVICE_CODES[status_code]
        sms = result.get('sms')
        ic(sms)
        to_number = sms.get(self.to_number.replace('+', ''))
        return float(to_number.get('cost')), int(to_number.get('sms'))
