import datetime
from functools import cached_property
from icecream import ic
from django.utils import timezone

from appointment.models import AppointmentModel
from core.models import ClientModel
from newsletters.services import NewsLettersService

SPAN_INC_INTERVAL = 2


class ClientService:

    def __init__(self, client):
        self.client = client

    def exists_appointments_of_greater_date(self, last_visit):
        return AppointmentModel\
            .objects\
            .filter(client=self.client, last_visit__gt=last_visit)\
            .exists()

    def list_appointments(self):
        return AppointmentModel\
            .objects\
            .filter(client=self.client)\
            .order_by('-last_visit')

    def list_appointments_with_interval_procedure(self):
        return AppointmentModel\
            .objects\
            .filter(client=self.client,
                    last_visit__isnull=False,
                    procedure__interval__isnull=False)\
            .order_by('-last_visit')

    @cached_property
    def last_procedure(self):
        record = self.list_appointments().last()
        return record.procedure if record else None

    @cached_property
    def last_interval_procedure(self):
        record = self.list_appointments_with_interval_procedure().last()
        return record.procedure if record else None

    def calculate_active(self):
        last_appointment_with_interval = self.list_appointments_with_interval_procedure().last()
        ic(last_appointment_with_interval)
        if last_appointment_with_interval is None:
            return False
        delta_days = last_appointment_with_interval.procedure.interval * SPAN_INC_INTERVAL
        return last_appointment_with_interval.last_visit + datetime.timedelta(delta_days) >= timezone.now()

    @cached_property
    def is_active(self):
        return self.calculate_active()

    def create_default_newsletters(self):

        NewsLettersService.create_default_newsletters(
            recipient=self.client,
            newsletters_cls=self.client.default_newsletters_cls)


class ClientsService:

    @staticmethod
    def calculate_active_all_clients():
        clients = ClientModel.objects.all()

        for client in clients:
            client.active = ClientService(client).is_active
            client.save()
