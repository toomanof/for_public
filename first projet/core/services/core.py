import logging

from django.core.exceptions import ObjectDoesNotExist, EmptyResultSet
from django.http.response import Http404
from rest_framework.response import Response
from rest_framework.exceptions import AuthenticationFailed
from rest_framework import status
from ..errors import AccessError

logger_error = logging.getLogger('warning')
logger_timer = logging.getLogger('timer')


def wrapper_response(method):
    """
    Декоратор в случае совершения ошибки в обёрнутой функции
    возрашает msg ошибки.
    При удачном выполненнии функции возращает её ответ
    """
    def wrapper(self, *args, **kwargs):
        try:
            new_obj = method(self, *args, **kwargs)
        except Exception as e:
            print(type(e))
            json_ret = {
                'msg': str(e),
                'code': 400001,
                'status': 'ERROR'}

            logger_error.error(f'{type(e)} {str(e)} in {method.__name__}')

            if isinstance(e, AuthenticationFailed):
                json_ret['code'] = status.HTTP_401_UNAUTHORIZED
                return Response(
                    json_ret, status=status.HTTP_401_UNAUTHORIZED)

            if isinstance(e, (EmptyResultSet, ObjectDoesNotExist, Http404)):
                json_ret['code'] = status.HTTP_404_NOT_FOUND
                return Response(
                    json_ret, status=status.HTTP_404_NOT_FOUND)

            if isinstance(e, AccessError):
                json_ret['code'] = status.HTTP_403_FORBIDDEN
                return Response(
                    json_ret, status=status.HTTP_403_FORBIDDEN)

            json_ret['code'] = status.HTTP_400_BAD_REQUEST
            if hasattr(e, 'status_code'):
                return Response(json_ret,
                                status=e.status_code)
            else:
                return Response(json_ret,
                                status=status.HTTP_400_BAD_REQUEST)

            if isinstance(e, ConflictError):
                json_ret['code'] = status.HTTP_409_CONFLICT
                return Response(json_ret, status=status.HTTP_409_CONFLICT)
        else:
            if not isinstance(new_obj, Response):
                return Response(new_obj)
            return new_obj
    return wrapper