"""Модуль сервисных класов и функций по работе по протоколу OAuth2."""
import json
import requests
from django.conf import settings
from django.contrib.auth.models import User
from rest_framework.response import Response
from rest_framework.exceptions import AuthenticationFailed
from oauth2_provider.models import get_application_model
from google.oauth2 import id_token
from google.auth.transport import requests as google_requests

from .account import AccountService
from ..constants import (GET, POST)
from ..errors import AccessError
from ..models import Profile

#from hooks.services import HooksProcessing, H_REG_USER


class AbstractServicesOAuth2:
    """
    Абстрактный класс авторизации по протоколу OAuth2.
    """
    methods_access_token = (POST, GET)
    provider_name = None
    headers = {}
    url_access_token = None
    url_get_params_user = None
    params_user = None
    method_access_token = POST
    access_token = None
    _email_user = None
    filed_id_provider = ''
    filed_access_token = 'accessToken'

    def __init__(self, request):
        self.request = request
        self.user_id = None
        if request.method == 'POST':
            self.request_dict = json.loads(request.body)
            if 'accessToken' in self.request_dict:
                self.access_token = self.request_dict[self.filed_access_token]
            if 'access_token' in self.request_dict:
                self.access_token = self.request_dict[self.filed_access_token]
            if 'email' in self.request_dict:
                self.email = self.request_dict['email']
            if 'code' in self.request_dict:
                self.code = self.request_dict['code']

    def authorization(self, user, flag_created_user=False):
        # if flag_created_user:
            # HooksProcessing.execute(self.request, H_REG_USER)
        return AccountService.json_response(self.request, user,
                                            flagCreatedUser=flag_created_user)

    def set_access_token(self, json_response):
        """
        Метод записи access_token с ответа провайдера
        """
        self.access_token = json_response['accessToken'] if 'accessToken' in json_response else\
            json_response['access_token'] if 'access_token' in json_response else None
        self.user_id = json_response['user_id'] if 'user_id' in json_response else None
        self._email_user = json_response['email'] if 'email' in json_response else None

    def get_provider(self, name):
        try:
            _provider = get_application_model().objects.get(name__icontains=name)
        except:
            return None
        return _provider

    def get_data_provider_for_get_at(self, request, provider_name):
        """
        Функция получения данных для получения access token от провайдера
        На основании имени провайдера с базы берется данные авторизации
        """
        self._provider = self.get_provider(provider_name)

        if not self._provider or 'code' not in self.request_dict:
            return
        redirect_uris = self._provider.redirect_uris.strip().split()
        return {
            'code': self.code,
            'client_id': self._provider.client_id,
            'client_secret': self._provider.client_secret,
            'redirect_uri': redirect_uris[0],
            'grant_type': 'authorization_code',
        }

    def get_filed_id_provider(self):
        assert self.filed_id_provider
        return self.filed_id_provider

    def get_provider_name(self):
        assert self.provider_name
        return self.provider_name

    def get_headers(self):
        return self.headers

    def get_url_access_token(self):
        assert self.url_access_token
        return self.url_access_token

    def get_url_get_params_user(self):
        assert self.url_get_params_user
        return self.url_get_params_user

    def supplement_payload(self):
        """
        Абстрактный метод дополняющий данные отправляемые
        post_access_token методом
        """
        pass

    def code_to_access_token(self):
        """
        Отправка GET запроса на api провайдера для получения данных с access token
        ulr на который будет отправлен запрос получем методом get_url_access_token
        data отправлемые будет получена методом get_data_provider_for_get_at
        и дополнены методом supplement_payload
        """
        self._payload = self.get_data_provider_for_get_at(
            self.request, self.get_provider_name())
        self.supplement_payload()

        return self.get_access_token()

    def get_access_token(self):
        """
        Отправка GET запроса на api провайдера для получения данных с access token
        ulr на который будет отправлен запрос получем методом get_url_access_token
        """
        return requests.get(self.get_url_access_token())

    def get_user_params_from_provider(self):
        """
        Отправка GET запроса на api провайдера для проверки access token
        ulr на который будет отправлен запрос получем методом get_url_get_params_user
        """
        response = requests.get(self.get_url_get_params_user()).json()
        if 'response' in response:
            response = response['response'][0]
        return response

    def full_execute(self):
        """
        Метод выполняет полный сценарий авторизации по протоколу OAuth2.
        """
        response = self.code_to_access_token()
        self.set_access_token(response.json())
        return self.execute_with_access_token()

    def execute(self):
        return self.execute_with_access_token()

    def connect(self):
        created_user = False
        self.send_data_to_provider_for_verification(self.get_filed_id_provider())

        id_social = getattr(self.request.current_user.profile, f'id_{self.provider_name}')
        if not id_social:
            AccountService.save_id_provider(
                self.request.current_user,
                self.params_user[self.get_filed_id_provider()],
                self.provider_name)

        return AccountService.json_response(self.request, self.request.current_user, flagCreatedUser=created_user)

    def execute_with_email(self):
        user, _ = AccountService.get_user_by_str_login_or_email(self.email)
        # if not user:
        #    return ('Email is exist', {'status': 200})
        self.params_user = self.get_user_params_from_provider()

        user, flag_created_user = self.get_or_create_not_active_user(
            self.params_user[self.get_filed_id_provider()], self.email)
        self.set_local_params_user_from_provider(user)
        user.email = self.email
        user.save()
        return self.authorization(user, flag_created_user)

    def execute_with_access_token(self):
        """
        Метод отправляет на проверку имеющийся self.access_token
        в api провайдера.
            В случаее ошибочного ответа - код и message ошибки
        возращается в return;
            В случаее отсвутсвия поля с id_user провайдера возвращается
        ответ 415 - Unsupported Media Type

        Полученный id_user провайдера проверяется на наличие в базе,
            в случае отсутсвия создается не активный пользователь
        с username соответсвующим id_user и возращается ответ:
        201 - Create not active user;
            в случаее наличия id_user пользователь авторизуется
        и возвращается ответ:
        200 - данные пользователя с внутренним access token-ом
        и email если провадер его возращает
        """

        # Отправка GET запроса на api провайдера для проверки access token
        flag_created_user = False
        _filed_id_provider = self.get_filed_id_provider()

        data = self.send_data_to_provider_for_verification(_filed_id_provider)
        if isinstance(data, tuple):
            return data
        user = data
        assert user, f'{self.provider_name} social network is not connected!'

        return self.authorization(user, flag_created_user)

    def get_user_by_id_provider(self, id_provider):
        return AccountService.get_user_by_id_soc_provider(
            id_provider, self.provider_name)

    def get_or_create_not_active_user(self, username, email=None):
        email = email if email else username
        user, created = AccountService.get_or_create_not_active_user(username, email)
        AccountService.save_id_provider(
            user, username, self.provider_name)
        return user, created

    def provider_params_to_local_params(self):
        """
        Метод переобразования в формат локальных параметров,
        параметров полученных от провайдера соц. сети
        """
        result = dict()
        result['first_name'] = self.params_user['first_name']\
            if 'first_name' in self.params_user else ''
        result['last_name'] = self.params_user['last_name']\
            if 'last_name' in self.params_user else ''
        result['email'] = self.params_user['email']\
            if 'email' in self.params_user else ''
        return result

    def set_local_params_user_from_provider(self, user):
        """
        Метод записи данных о пользователе полученных от провайдера соц. сети
        в локальной базе данных
        """
        if not self.params_user:
            return

        params = self.provider_params_to_local_params()
        if not params:
            return

        AccountService.update_user_fields(user, **params)
        # Запись url аватарки полученной от провайдера соц сети
        if 'avatar' in params:
            AccountService.save_url_pic_provider(
                user, params['avatar'], self.provider_name)
            self.save_avatar_in_local_storage(user, params['avatar'])

    def save_avatar_in_local_storage(self, user, url_avatar, rewrite=False):
        """
        Записывает аватар полученный от провайдера соц сети в ImageStorage
        и присваивает его к профилю если rewrite == True
        """
        pass

    def verification(self):
        """
        Метод отправляет на проверку имеющийся self.access_token
        в api провайдера.
        Полученный id_user провайдера проверяется на наличие в базе

        @return: user or None
            В случаее ошибочного ответа - код и message ошибки
        возращается в return;
            В случаее отсвутсвия поля с id_user провайдера возвращается
        ответ 415 - Unsupported Media Type
        """
        # Отправка GET запроса на api провайдера для проверки access token
        if self.provider_name == 'vk':
            response = self.code_to_access_token()
            self.set_access_token(response.json())
        _filed_id_provider = self.get_filed_id_provider()
        user = self.send_data_to_provider_for_verification(_filed_id_provider)
        if user:
            return False
        AccountService.save_id_soc_provider_in_profile(
            self.request.current_user.profile, self.params_user[_filed_id_provider], self.provider_name)
        return True

    def send_data_to_provider_for_verification(self, _filed_id_provider):
        """
        Метод отправляет на проверку имеющийся self.access_token
        в api провайдера.
        Полученный id_user провайдера проверяется на наличие в базе

        @return: user or None
            В случаее ошибочного ответа - код и message ошибки
        возращается в return;
            В случаее отсвутсвия поля с id_user провайдера возвращается
        ответ 415 - Unsupported Media Type
        """
        self.params_user = self.get_user_params_from_provider()
        if 'error' in self.params_user:
            err_msg = self.params_user['error']['message']\
                if 'message' in self.params_user['error'] else\
                   self.params_user['error']['error_msg']\
                   if 'error_msg' in self.params_user['error'] else ''
            raise AuthenticationFailed(err_msg)

        # имя поля id OAth как оно передается провайдером

        if _filed_id_provider not in self.params_user:
            return ('Unsupported Media Type', {'status': 415})

        # проверка на существования id OAuth провайдера  в базе
        return self.get_user_by_id_provider(
            self.params_user[_filed_id_provider])


class YandexServicesOAuth2(AbstractServicesOAuth2):
    provider_name = 'yandex'
    url_access_token = 'https://oauth.yandex.ru/token'
    filed_id_provider = 'id'

    def get_url_get_params_user(self):
        return (f'https://login.yandex.ru/info?'
                f'format=json&oauth_token={self.access_token}')


class MailRuServicesOAuth2(AbstractServicesOAuth2):
    provider_name = 'mailru'
    url_access_token = 'https://connect.mail.ru/oauth/token'
    filed_id_provider = 'id'

    def get_url_get_params_user(self):
        return (f'https://login.yandex.ru/info'
               f'?format=json&oauth_token={self.access_token}')


class FacebookServicesOAuth2(AbstractServicesOAuth2):
    provider_name = 'facebook'
    url_access_token = 'https://graph.facebook.com/v10.0/oauth/access_token'
    method_access_token = GET
    filed_id_provider = 'id'

    def get_url_get_params_user(self):
        return (f'https://graph.facebook.com/me?'
               f'access_token={self.access_token}&'
               f'fields=email,name,first_name,last_name')

    #def provider_params_to_local_params(self):
    #    result = super().provider_params_to_local_params()
    #    result['avatar'] = self.params_user['profile_pic']
    #    return result


class VKServicesOAuth2(AbstractServicesOAuth2):
    provider_name = 'vk'
    method_access_token = GET
    filed_id_provider = 'id'

    def get_url_access_token(self):
        if not self._payload:
            return "https://oauth.vk.com/access_token"

        url = (f"https://oauth.vk.com/access_token"
               f"?client_id={self._payload['client_id']}&"
               f"redirect_uri={self._payload['redirect_uri']}&"
               f"client_secret={self._payload['client_secret']}&"
               f"code={self._payload['code']}")
        return url

    def get_url_get_params_user(self):
        url = (f'https://api.vk.com/method/users.get?uids={self.user_id}&fields=uid,first_name,last_name,nickname,screen_name,sex,bdate,city,country,timezone,photo&'
               f'access_token={self.access_token}&v=5.103')
        return url

    def provider_params_to_local_params(self):
        result = super().provider_params_to_local_params()
        result['avatar'] = self.params_user['photo']
        return result


class GoogleServicesOAuth2:
    """

    """
    idinfo = []
    user = None

    def __init__(self, request):
        self.data = request.data
        print('GoogleLogin', self.data)
        assert 'id_token' in self.data, 'Not set id token!'
        self.request = request
        self.id_token = self.data['id_token']

    def execute(self):
        created_user = False
        self.verify_id_token()
        assert self.get_user_on_aud(), 'Google social network is not connected!'
        return AccountService.json_response(self.request, self.user, flagCreatedUser=created_user)

    def connect(self):
        created_user = False
        self.verify_id_token()
        self.user = self.request.current_user
        if not self.request.current_user.profile.id_google:
            self.save_info_user()

        return AccountService.json_response(self.request, self.user, flagCreatedUser=created_user)

    def get_or_create_not_active_user(self):
        assert 'email' and 'name' in self.idinfo, 'No information received from google!'

        return AccountService.get_or_create_not_active_user(self.idinfo['name'], self.idinfo['email'])

    def create_user(self):
        User.objects.get_or_create(username=self.idinfo['name'])

    def get_user_on_aud(self):
        assert 'sub' in self.idinfo, 'No information received from google!'

        profile = Profile.objects.filter(id_google=self.idinfo['sub']).first()
        if profile:
            self.user = profile.user
        return self.user

    def save_info_user(self):
        if self.user:
            self.user.profile.url_pic_google = self.idinfo['picture'] if 'picture' in self.idinfo else None
            self.user.profile.id_google = self.idinfo['sub']
            self.user.profile.save()

            self.user.username = self.idinfo['name'] if 'name' in self.idinfo else self.user.username
            self.user.email = self.idinfo['email'] if 'email' in self.idinfo else self.user.email
            self.user.first_name = self.idinfo['given_name'] if 'given_name' in self.idinfo else self.user.first_name
            self.user.last_name = self.idinfo['family_name'] if 'family_name' in self.idinfo else self.user.last_name
            self.user.save()

    def verify_id_token(self):
        try:
            self.idinfo = id_token.verify_oauth2_token(
                self.data['id_token'],
                google_requests.Request(),
                settings.GOOGLE_APP_CLIENT_ID)
            print('verify_id_token', self.idinfo)
        except ValueError as e:
            raise AccessError(f'Invalid token!, {e}')

        return self.idinfo
