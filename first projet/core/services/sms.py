import json
from icecream import ic
from typing import TYPE_CHECKING
from django.conf import settings

from ..errors import UserNotFoundException
from ..errors import ConflictError
from .base import BaseService
from .twilio import TwilioService
from .sms_ru import SMSRuService


class SMSService(BaseService):

    def cost_sms(self, body: str, to_number: str) -> float:
        return SMSRuService(to_number=to_number, body=body).cost_sms()

    def send_sms(self, body: str, to_number: str) -> str:
        """
        Отправляет смс через сервис Twilio
        @param body: тело сообщения
        @param to_number: номер телефона получателя
        @return: sid сообщения Twilio
        """
        try:
            message = SMSRuService(to_number, body).send_sms()
        #    tw_service = TwilioService()
        #    message = tw_service.send_sms(
        #        to=to_number,
        #        from_=settings.TWILIO_NUMBER,
        #        body=body
        #    )
        except Exception as err:
            raise Exception(f'Unable to send SMS to this phone number! {err}')
        else:
            return message

    def check_auth_code(self, request):
        from .account import AccountService

        data = request.data
        recipient = AccountService.get_user_by_phone_number(data['phone'])

        if not recipient:
            raise UserNotFoundException('Recipient not found')
        else:

            if str(recipient.profile.phone) == settings.PHONE_NUMBER_FREE_ACCOUNT and \
                    str(data['auth_code']) == settings.SMS_CODE_FREE_ACCOUNT:
                recipient.profile.phone_code.delete()
                return recipient

            if hasattr(recipient.profile, 'phone_code') and recipient.profile.phone_code.is_active_checking and\
               str(recipient.profile.phone_code.value) == str(data['auth_code']):
                recipient.profile.phone_code.delete()
                return recipient

        raise ConflictError('Invalid confirmation code')
