from icecream import ic
from django.conf import settings

from twilio.rest import Client


class TwilioService:
    """
    Класс работы с возможностями сервиса twilio.com
    Документация: https://www.twilio.com/docs
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.account_sid = settings.TWILIO_ACCOUNT_SID
        self.auth_token = settings.TWILIO_TOKEN
        self.client = Client(self.account_sid, self.auth_token)

    def send_sms(self, **kwargs):
        """
        Отправка смс.
        Параметры:
            to - телефонный номер получателя;
            from_ - телефонный номер отправителя;
            body - текст сообщения
        """
        message = self.client.messages.create(**kwargs)
        ic(message, message.price, message.price_unit, message.status, message.body, dir(message))
        return message
