# Generated by Django 3.2.3 on 2021-07-11 06:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0018_clientmodel_token_device'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='token_device',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Токен устройства'),
        ),
    ]
