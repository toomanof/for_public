from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import *
from django.conf import settings
from django.core.mail import EmailMultiAlternatives
"""
from core.mail import mail_sendgrid
from django.template.loader import get_template
from premailer import Premailer

html_template = get_template('email/welcome.html')
context_dict = {
        'user': request.user
    }
if html_template:
    html_content = Premailer(
        html_template.render(context_dict),
        base_url=settings.SITE_URL,
        remove_classes=False,
        strip_important=False
    ).transform()

    mail_sendgrid(request.user.email,'Welcome to Grain Club',html_content)
"""

def mail_sendgrid(mail_to, mail_subj, mail_html):

    sendgrid_client = SendGridAPIClient(settings.KEY_SENDGRID)

    data = {
      "personalizations": [
        {
          "to": [
            {
              "email": mail_to
            }
          ],
          "subject": mail_subj
        }
      ],
      "from": {
        "email": settings.EMAIL_FROM_SENDGRID
      },
      "content": [
        {
          "type": "text/html",
          "value": mail_html
        }
      ]
    }
    return sendgrid_client.client.mail.send.post(request_body=data)
