from django.contrib.auth.models import User
from django.db import models


class DeviceUserModel(models.Model):
    TDO_ANDROID, TDO_IOS = range(2)
    CHOICE_TYPE_OS_DEVICES = (
        (TDO_ANDROID, 'android'),
        (TDO_IOS, 'ios')
    )

    user = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='devices',
        verbose_name='Устройства пользователей')
    os = models.PositiveSmallIntegerField(
        'ОС устройства', choices=CHOICE_TYPE_OS_DEVICES,
        default=TDO_ANDROID)
    token = models.CharField(
        'Токен устройства', max_length=255, blank=True, null=True)

    class Meta:
        verbose_name = 'Устройство пользователя'
        verbose_name_plural = 'Устройства пользователей'
