from django.contrib.auth.models import User
from django.db import models
from .abstract_models import AbstractModelWithTitle


class ProcedureModel(AbstractModelWithTitle):
    performer = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='procedures',
        verbose_name='Исполнитель процедур', blank=True, null=True)
    description = models.TextField('Описание', blank=True, null=True)
    duration = models.IntegerField('Продолжительность', blank=True, null=True)
    interval = models.IntegerField('Интервал (в днях)', blank=True, null=True)
    price = models.FloatField('Стоимость услуги', default=0)
    prime_cost = models.FloatField('Себестоимость услуги', default=0)

    class Meta:
        verbose_name = 'Процедура'
        verbose_name_plural = 'Процедуры'
