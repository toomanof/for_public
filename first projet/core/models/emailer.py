import time
import os

from django.core.exceptions import ValidationError
from django.conf import settings
from django.contrib import admin
from django.contrib.sites.shortcuts import get_current_site
from django.db import models
from django.template import Template
from django.template import Context
from django.utils import timezone

from core.mail import mail_sendgrid
from .email import Email


class EmailerManager(models.Manager):
    """
        Менеждер совершающий запись и отправку в очередь
        отправляемые электронные письма
    """
    def __init__(self):
        super().__init__()
        self.request = None

    def get_common_context(self):
        """
            Добавляются протокол и домен системы
        """
        if self.request:
            current_site = get_current_site(self.request)
            domain = current_site.domain
        else:
            domain = settings.DEFAULT_DOMAIN
        return {'protocol': settings.DEFAULT_PROTOCOL, 'domain': domain}

    def process_email(self, code, email_to, context=None, request=None):
        """
           Метод записи електроного письма в очередь.
           context - дополнительный контекст.
        """
        self.request = request
        try:
            email = Email.objects.get(code=code)
        except Email.DoesNotExist:
            raise ValidationError('Invalid code template email')

        if context is None:
            context = {}

        context.update(self.get_common_context())
        ctx = Context(context)

        subject = email.subject
        txt = email.txt
        html = email.html
        attachment = None

        subject_tpl = Template(subject)
        txt_tpl = Template(txt)
        html_tpl = Template(html)

        em = self.model(
            email_to=email_to,
            email_from=email.get_admin_email_to(),
            subject=subject_tpl.render(ctx),
            html=html_tpl.render(ctx),
            txt=txt_tpl.render(ctx),
            attachment=attachment
        )
        em.save()

        # В дебаг режиме отправка производится сразу
        #if settings.DEBUG:
        em.send()
        return em

    def run(self):
        """
        Отправить email.
        """
        qs = self.model.objects.filter(sent=False)
        for em in qs:
            em.send()
            time.sleep(3)


class Emailer(models.Model):
    email_to = models.EmailField(max_length=100)
    email_from = models.EmailField(max_length=100)
    subject = models.CharField(max_length=255, verbose_name='заголовок')
    txt = models.TextField(verbose_name='текст')
    html = models.TextField(verbose_name='html')
    attachment = models.CharField(
        max_length=255, null=True, default=None, blank=True)
    report = models.TextField(default='', verbose_name='отчет')
    sent = models.BooleanField(default=False, verbose_name='отправлен')
    result = models.BooleanField(default=False, verbose_name='результат')
    dt_created = models.DateTimeField(auto_now_add=True, verbose_name='дата')
    dt_sent = models.DateTimeField(null=True,
                                   blank=True, verbose_name='дата отправки')

    objects = EmailerManager()

    class Meta:
        verbose_name = 'Email отчет'
        verbose_name_plural = 'Email отчеты'
        ordering = ['-dt_created']

    def __str__(self):
        return str(self.id)

    def send(self):
        try:
            response = mail_sendgrid(self.email_to, self.subject, self.html)

            self.result = True
            self.report = "OK. Mail send."
        except Exception as e:
            self.result = False
            self.report = str(e)

        self.dt_sent = timezone.now()
        self.sent = True
        self.save()


@admin.register(Emailer)
class EmailerAdmin(admin.ModelAdmin):
    list_display = (
        'dt_created',
        'email_to',
        'email_from',
        'subject',
        'show_txt',
        'show_html',
        'show_attachment',
        'report',
        'sent',
        'result',
        'dt_sent'
    )
    exclude = ('attachment',)

    def show_txt(self, obj):
        return obj.txt[:30] + '...'
    show_txt.short_description = 'Текст'

    def show_html(self, obj):
        return obj.html[:30] + '...'
    show_html.short_description = 'Текст'

    def show_attachment(self, obj):
        if not obj.attachment:
            return '-'
        return os.path.basename(obj.attachment)

        return obj.html[:30] + '...'
    show_attachment.short_description = 'attachment'
