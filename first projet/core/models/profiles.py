from datetime import timedelta
from random import randint
from django.conf import settings
from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone
from phonenumber_field.modelfields import PhoneNumberField

from .image_storages import ImageStorage


class Profile(models.Model):

    user = models.OneToOneField(
        User, on_delete=models.CASCADE, related_name='profile',
        verbose_name='Профиль пользователя')
    id_facebook = models.CharField(
        max_length=250, null=True, blank=True,
        verbose_name='Id пользователя facebook')
    id_vk = models.CharField(
        max_length=250, null=True, blank=True,
        verbose_name='Id пользователя vk')
    id_google = models.CharField(
        max_length=250, null=True, blank=True,
        verbose_name='Id пользователя google')
    is_enabled = models.BooleanField(default=False,
                                     verbose_name='Профиль включен')
    url_pic_facebook = models.URLField(
        'Аватар с facebook', null=True, blank=True, max_length=255)
    url_pic_vk = models.URLField(
        'Аватар с vk', null=True, blank=True, max_length=255)
    url_pic_google = models.URLField(
        'Аватар с google', null=True, blank=True, max_length=255)
    phone = PhoneNumberField(blank=True, null=True)
    avatar = models.ImageField(
        upload_to='uploads/avatars', blank=True, null=True, verbose_name='Изображение',
        max_length=255)
    description = models.TextField('Описание (биография)', null=True, blank=True)

    def __str__(self):
        return self.full_name

    @property
    def full_name(self):
        result = f'{self.user.last_name} {self.user.first_name}'
        return result if len(result)>1 else self.user.username

    def set_new_auth_phone_code(self, new_code):
        """
        Создаётся новый временный код авторизации по телефонному номеру.
        Сохраняет его в базе и возвращает его значение.
        """
        if not hasattr(self, 'phone_code'):
            AuthPhoneCode(profile=self, value=new_code).save()
        else:
            self.phone_code.value = new_code
            self.phone_code.save()

        return self.phone_code

    def get_saved_auth_phone_code(self):
        """
        Возвращает сохранённый в базе временный код авторизации
        по телефонному номеру и признак генерации нового кода.
        """
        if hasattr(self, 'phone_code') and self.phone_code.is_active:
            return self.phone_code, False
        else:
            new_code = randint(settings.MIN_VALUE_PHONE_CODE,
                               settings.MAX_VALUE_PHONE_CODE)
            return self.set_new_auth_phone_code(new_code), True


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    Profile.objects.get_or_create(user=instance)


class AuthPhoneCode(models.Model):
    """
    Модель для хранения временных кодов авторизации по телефонному номеру
    """
    profile = models.OneToOneField(
        Profile, on_delete=models.CASCADE, related_name='phone_code',
        )
    value = models.PositiveIntegerField(
        'Временный код авторизации через тел. номер')
    created_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.value)

    @property
    def is_active(self):
        """
        Проверяет время жизни временный код авторизации
        по телефонному номеру.
        Время жизни составляет время создания + установленный
        в настройках период
        """
        return timezone.now() <= self.expiry

    @property
    def is_active_checking(self):
        """
        Проверяет время проверки временный код авторизации
        по телефонному номеру.
        Время проверки составляет время создания + установленный
        в настройках период
        """
        return timezone.now() <= self.expiry_checking

    @property
    def expiry(self):
        """
        Время конца жизни кода
        """
        delta_time = timedelta(seconds=settings.LIFETIME_AUTH_PHONE_CODE)
        return self.update_at + delta_time

    @property
    def expiry_checking(self):
        """
        Время конца проверки кода
        """
        delta_time = timedelta(seconds=settings.LIFETIME_CHECK_AUTH_PHONE_CODE)
        return self.update_at + delta_time
