from django.db.models import (
    Model, PositiveSmallIntegerField,
    CharField, TextField, BooleanField)
from django.conf import settings
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _


class Email(Model):
    """
        Класс хранящий шаблоны служебных електронных писем
        которые может отправлять система.
        Типы сообщений перечисленны в CODE_CHOICES.
    """

    (PASSWORD_RESET, INVITE, ACTIVE_ACCOUNT) = range(3)
    STR_PASSWORD_RESET = _('Password reset')
    STR_INVITE = _('Invite')
    STR_ACTIVE_ACCOUNT = _('Active account')

    CODE_CHOICES = (
        (PASSWORD_RESET, STR_PASSWORD_RESET),
        (INVITE, STR_INVITE),
        (ACTIVE_ACCOUNT, STR_ACTIVE_ACCOUNT),
    )

    code = PositiveSmallIntegerField(verbose_name='код', choices=CODE_CHOICES)
    name = CharField(verbose_name='название', max_length=255)
    subject = CharField(verbose_name='заголовок email', max_length=255)
    txt = TextField(verbose_name='текст', default='', blank=True)
    html = TextField(verbose_name='html', default='', blank=True)
    is_active = BooleanField(default=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Email сообщение'
        verbose_name_plural = 'Email сообщения'
        ordering = ['code']

    def save(self, *args, **kwargs):
        if not self.pk and not self.subject:
            self.subject = self.name
        super().save(*args, **kwargs)

    def get_admin_email_to(self):
        """
            Предназначено для выбора email при раздичных сообщениях
        """
        if self.code == self.PASSWORD_RESET:
            return settings.DEFAULT_FROM_EMAIL
        elif self.code == self.INVITE:
            return settings.DEFAULT_FROM_EMAIL
        elif self.code == self.ACTIVE_ACCOUNT:
            return settings.DEFAULT_FROM_EMAIL
        return settings.DEFAULT_FROM_EMAIL


@admin.register(Email)
class EmailAdmin(admin.ModelAdmin):
    actions = None
    list_display = (
        'name',
        'code',
        'subject',
        'txt',
        'html'
    )
    list_editable = ('subject', 'txt', 'html')
