from django.db import models
from django.utils import timezone

from .abstract_models import AbstractModelWithTitle


class HolidaysManager(models.Manager):

    def holidays_today(self):
        today = timezone.now().today()
        return self.get_queryset().filter(
            value__day=today.day,
            value__month=today.month
        )


class HolidayModel(AbstractModelWithTitle):
    MAN, WOMAN = range(2)
    CHOICES_GENDER = (
        (MAN, 'мужской'),
        (WOMAN, 'женский')
    )
    value = models.DateField('Дата праздника')
    gender = models.SmallIntegerField('Мужской/Женский',
                                      choices=CHOICES_GENDER,
                                      blank=None, null=True)
    template = models.TextField('Текст поздравления', blank=True, null=True)
    objects = HolidaysManager()

    @property
    def day(self):
        return self.value.day

    @property
    def month(self):
        return self.value.month
