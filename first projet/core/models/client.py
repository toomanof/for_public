from icecream import ic
from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from phonenumber_field.modelfields import PhoneNumberField

from appointment.models import AppointmentModel
from newsletters.services import (
    DefaultNewsletterRemindReception, DefaultNewsletterRemindProcedure,
    DefaultNewsletterFeedBack, DefaultNewsletterBirthday
)


class ClientModel(models.Model):
    MAN, WOMAN = range(2)
    CHOICES_GENDER = (
        (MAN, 'мужской'),
        (WOMAN, 'женский')
    )
    active = models.BooleanField('Флаг активности клиента', default=False)
    performer = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='clients',
        verbose_name='Исполнитель процедур', blank=True, null=True)
    first_name = models.CharField('Имя', max_length=255)
    last_name = models.CharField('Фамилия', max_length=255,
                                 blank=True, null=True)
    phone = PhoneNumberField('Телефон', blank=True, null=True)
    whatsapp = PhoneNumberField('Whatsapp', blank=True, null=True)
    telegram = models.CharField('Telegram', max_length=255,
                                blank=True, null=True)
    viber = models.CharField('Viber', max_length=255,
                             blank=True, null=True)
    instagram = models.CharField('Instagram', max_length=255,
                                 blank=True, null=True)
    last_visit = models.DateTimeField('Дата последнего визита',
                                      blank=True, null=True)
    reception_date = models.DateTimeField('Дата следующего визита',
                                          blank=True, null=True)
    birthday = models.DateField('Дата рождения', blank=True, null=True)
    has_nl_remind_reception = models.BooleanField(
        'Флаг автоматического создания рассылки напоминание о записях',
        default=False)
    has_nl_remind_procedure = models.BooleanField(
        'Флаг автоматического создания рассылки напоминание о процедурах',
        default=False)
    has_nl_feedback = models.BooleanField(
        'Флаг автоматического создания рассылки обратной связи',
        default=False)
    has_nl_birthday = models.BooleanField(
        'Флаг автоматического создания рассылки поздравлений с днём рождения',
        default=False)
    has_nl_gender_holiday = models.BooleanField(
        'Флаг автоматического создания рассылки муж/жен поздравлений',
        default=False)
    gender = models.PositiveSmallIntegerField(
        'Пол', choices=CHOICES_GENDER, blank=True, null=True)
    notes = models.TextField('Заметки', blank=True, null=True)
    token_device = models.CharField('Токен устройства', max_length=255,
                                    blank=True, null=True)

    class Meta:
        verbose_name = 'Клиент'
        verbose_name_plural = 'Клиенты'

    def __str__(self):
        return f'{self.first_name} {self.last_name}'

    @property
    def default_newsletters_cls(self):
        result = []
        if self.has_nl_remind_reception:
            result.append(DefaultNewsletterRemindReception)
        if self.has_nl_remind_procedure:
            result.append(DefaultNewsletterRemindProcedure)
        if self.has_nl_feedback:
            result.append(DefaultNewsletterFeedBack)
        if self.has_nl_birthday:
            result.append(DefaultNewsletterBirthday)
        return result


@receiver(post_save, sender=AppointmentModel)
def appointment_model_post_save(sender, **kwargs):
    from core.services import ClientService

    instance = kwargs['instance']
    client_service = ClientService(instance.client)
    if instance.last_visit and not client_service.exists_appointments_of_greater_date(instance.last_visit):
        instance.client.last_visit = instance.last_visit

    instance.client.active = client_service.is_active
    instance.client.save()

