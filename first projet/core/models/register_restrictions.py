from django.contrib.auth.models import User
from django.db import models

from appointment.models import AppointmentModel
from .client import ClientModel
from .procedure import ProcedureModel


class RegisterRestrictionModel(models.Model):
    creator = models.ForeignKey(User, on_delete=models.CASCADE,
                                related_name='restrictions',
                                verbose_name='Создатель')
    procedure = models.ForeignKey(ProcedureModel, on_delete=models.CASCADE,
                                  related_name='restrictions',
                                  verbose_name='Процедуры',
                                  blank=True, null=True)
    client = models.ForeignKey(ClientModel, on_delete=models.CASCADE,
                               related_name='restrictions',
                               verbose_name='Клиент',
                               blank=True, null=True)
    appointment = models.ForeignKey(AppointmentModel, on_delete=models.CASCADE,
                                    related_name='restrictions',
                                    verbose_name='Запись',
                                    blank=True, null=True)
    date_create = models.DateField('Дата записи', auto_now_add=True)
    date_update = models.DateTimeField('Дата обновление записи', auto_now=True)

    class Meta:
        verbose_name = 'Запись ограничения пользователя'
        verbose_name_plural = 'Реестр ограничений пользоватлелей'
