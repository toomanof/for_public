from django.db import models


class SubscriptionModel(models.Model):
    title = models.CharField('Наименование', max_length=255)
    duration = models.PositiveSmallIntegerField('Длительность в месецах',
                                                default=1)

    class Meta:
        verbose_name = 'Подписка'
        verbose_name_plural = 'Подписки'

    def __str__(self):
        return self.title
