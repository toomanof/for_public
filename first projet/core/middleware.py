import logging
from icecream import ic

from core.services import AccountService
from django.http import HttpResponse


logger = logging.getLogger('info')


class CurrentUserMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        ic('input ', request.path)
        if 'Authorization' in request.headers:
            current_user = AccountService.get_user_by_knox_token(request)
        else:
            current_user = AccountService.get_user_by_cookie_token(request)
        request.current_user = current_user
        response = self.get_response(request)
        ic('CurrentUserMiddleware ', request.current_user)
        logger.info(f'Enter user {request.current_user} to {request.path_info}')
        return response


class AccessControlMiddleware:
    """
    Should be first middleware.
    """
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if request.method == 'OPTIONS':
            response = HttpResponse()
            self.add_headers(response)
            return response

        response = self.get_response(request)
        self.add_headers(response)
        return response

    def add_headers(self, response):
        """
        Adds headers to response.
        """
        response['Access-Control-Allow-Origin'] = '*'
        response['Access-Control-Allow-Credentials'] = 'true'
        response['Access-Control-Allow-Headers'] = (
            'Authorization, Origin, X-Requested-With, Content-Type, Accept,'
            'Cookie')
        response['Access-Control-Allow-Methods'] = (
            'GET, PUT, POST, DELETE, PATCH, OPTIONS')