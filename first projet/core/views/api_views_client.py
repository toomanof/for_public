from rest_framework.viewsets import ModelViewSet
from rest_framework import permissions

from core.errors import UserNotSubscriptionInAccountException
from core.services import wrapper_response, SubscriptionManagementService

from ..models import ClientModel
from ..serializers import ClientSerializer
from newsletters.services import NewsLettersService


class ClientViewSet(ModelViewSet):
    serializer_class = ClientSerializer
    permission_classes = [
        permissions.IsAuthenticated,
    ]

    def get_queryset(self):
        return ClientModel.objects.filter(performer=self.request.current_user)

    @wrapper_response
    def create(self, request, *args, **kwargs):
        sub_manager = SubscriptionManagementService(request)
        if not sub_manager.has_ability_create_multiple_clients:
            raise UserNotSubscriptionInAccountException()

        request.data['performer'] = self.request.current_user.id
        response = super().create(request, *args, **kwargs)
        instance = self.get_queryset().get(pk=response.data['id'])

        NewsLettersService.create_default_newsletters(
            request.current_user, instance, instance.default_newsletters_cls)
        sub_manager.save_client_to_restriction(instance)
        return response

    @wrapper_response
    def update(self, request, *args, **kwargs):
        request.data['performer'] = self.request.current_user.id
        response = super().update(request, *args, **kwargs)
        instance = self.get_object()
        NewsLettersService.update_default_newsletters(
            request.current_user, instance, instance.default_newsletters_cls)
        return response
