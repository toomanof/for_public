"""
API представления для работы с пользователями
"""

import json

from django.core.exceptions import EmptyResultSet
from django.contrib.auth import login
from django.contrib.auth.models import User
from django.db import transaction
from django.shortcuts import get_object_or_404
from rest_framework import generics, permissions
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from django.shortcuts import HttpResponseRedirect

from ..auth import CookieTokenAuthentication
from ..models import Profile
from ..services import AccountService, SMSService, wrapper_response
from ..serializers import UserSerializer, ProfileSerializer


class HttpResponseSeeOther(HttpResponseRedirect):
    status_code = 303


class ProfileApiView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    @wrapper_response
    def get(self, request, **kwargs):
        profile = request.current_user.profile
        return Response(ProfileSerializer(profile, context={'request': request}).data)

    @wrapper_response
    def put(self, request, **kwargs):
        user = request.current_user
        data = request.data
        user.username = data['username'] if 'username' in data else user.username
        user.first_name = data['first_name'] if 'first_name' in data else user.first_name
        user.last_name = data['last_name'] if 'last_name' in data else user.last_name
        user.save()

        user.profile.phone = data['phone'] if 'phone' in data else user.username
        user.profile.description = data['description'] if 'description' in data else user.profile.description
        user.profile.save()

        user = User.objects.get(pk=self.request.current_user.pk)
        return ProfileSerializer(user.profile, context={'request': request}).data

    def patch(self, request, **kwargs):
        return self.put(request, **kwargs)


class SendSmsAuthCodeApiView(APIView):
    """
     Отправка sms кода на активацию пользователя.
     Создает пользователя с логином и email равным предоставленному номеру.
     Записывает код авторизации в базу данных.

    Parameters
    ----------
         - phone - телефон получателя, передается когда пользователя необходимо
                 создать в базе
     """
    @wrapper_response
    def post(self, request: object) -> object:

        response, status = AccountService.send_sms_auth_code_and_create_user(request)
        return Response(response, status=status)


class ActivateUserWithAuthCodeApiView(APIView):
    """
    Активация пользователя по коду из SMS.
    Проверка кода на активацию пользователя.

    Parameters
    ----------
        phone - телефон пользователя
        auth_code - код переданный пользователем для проверки

    """
    @wrapper_response
    def post(self, request, **kwargs):
        response = SMSService().check_auth_code(request)
        if isinstance(response, User):
            login(request, response,  backend='django.contrib.auth.backends.ModelBackend')
            return AccountService.json_response(
                request,
                response,
                session_expiry_date=request.session.get_expiry_date())

        return Response(response, status=403)


class DownloadAvatarApiView(APIView):
    """
    Загрузка аватарки текущего пользователя

    Parameters
    ---

        file - файл аватарки

    """
    permission_classes = [
        permissions.IsAuthenticated,
    ]

    @wrapper_response
    def post(self, request, **kwargs):
        self.request.current_user.profile.avatar = request.FILES['file']
        self.request.current_user.profile.save()
        return {
            "avatar": self.request.current_user.profile.avatar.url
        }


class CheckPhoneApiView(APIView):
    """
    Проверка существования пользователя по номеру телефона
    """

    def post(self, request, **kwargs):
        if 'phone' not in request.data:
            return Response({
                'exist': False,
                'google': False,
                'facebook': False,
                'vk': False,
            })
        user = AccountService.get_user_by_phone_number(request.data['phone'])
        return Response({
            'exist': bool(user),
            'google': bool(user and user.profile.id_google),
            'facebook': bool(user and user.profile.id_facebook),
            'vk': bool(user and user.profile.id_vk),
        })
