from rest_framework.viewsets import ModelViewSet
from rest_framework import permissions
from core.services import wrapper_response
from rest_framework import status
from rest_framework.response import Response

from appointment.services import PushSender
from ..serializers import DeviceUserSerializer
from ..models import DeviceUserModel


class DevicesTokensViewSet(ModelViewSet):
    serializer_class = DeviceUserSerializer
    permission_classes = [
        permissions.IsAuthenticated,
    ]
    TYPES_OS = {
        'android': DeviceUserModel.TDO_ANDROID,
        'ios': DeviceUserModel.TDO_IOS
    }

    def get_queryset(self):
        PushSender().send_push_before_procedure()
        return DeviceUserModel.objects.filter(
            user=self.request.current_user)

    @wrapper_response
    def create(self, request, *args, **kwargs):
        request.data['user'] = self.request.current_user.id
        if 'os' in request.data:
            request.data['os'] = self.TYPES_OS[request.data['os']]
        if not DeviceUserModel.objects.filter(**request.data).exists():
            return super().create(request, *args, **kwargs)

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    @wrapper_response
    def update(self, request, *args, **kwargs):
        request.data['user'] = self.request.current_user.id
        return super().update(request, *args, **kwargs)
