from rest_framework.viewsets import ModelViewSet
from rest_framework import permissions

from core.errors import UserNotSubscriptionInAccountException
from core.services import wrapper_response, SubscriptionManagementService
from ..models import ProcedureModel
from ..serializers import ProcedureSerializer


class ProcedureViewSet(ModelViewSet):
    serializer_class = ProcedureSerializer
    queryset = ProcedureModel.objects.all()
    permission_classes = [
        permissions.IsAuthenticated,
    ]

    def get_queryset(self):
        return ProcedureModel.objects.filter(performer=self.request.current_user)

    @wrapper_response
    def create(self, request, *args, **kwargs):
        sub_manager = SubscriptionManagementService(request)
        if not sub_manager.has_ability_create_multiple_procedures:
            raise UserNotSubscriptionInAccountException()

        request.data['performer'] = self.request.current_user.id
        response = super().create(request, *args, **kwargs)
        instance = self.get_queryset().get(pk=response.data['id'])
        sub_manager.save_procedure_to_restriction(instance)

        return response

    @wrapper_response
    def update(self, request, *args, **kwargs):
        request.data['performer'] = self.request.current_user.id
        return super().update(request, *args, **kwargs)
