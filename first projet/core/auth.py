from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from rest_framework.authentication import BaseAuthentication
from rest_framework import exceptions

from .services import AccountService


class UserAuthentication(BaseAuthentication):
    def get_current_user(self):
        pass

    def authenticate(self, request):
        current_user = self.get_current_user(request)
        message = {
            'code': 400001,
            'status': 'ERROR'
        }

        if not current_user:
            return None

        message['is_active'] = current_user.is_active
        message['is_enabled'] = current_user.profile.is_enabled

        if not message['is_active']:
            message['message'] = 'User not active!'
            raise exceptions.NotAuthenticated(message, code=401)
        if not message['is_enabled']:
            message['message'] = 'User not enabled!'
            raise exceptions.NotAuthenticated(message, code=401)

        return current_user, None


class TokenAuthentication(UserAuthentication):

    def get_current_user(self, request):
        return AccountService.get_user_by_knox_token(request)


class CookieTokenAuthentication(UserAuthentication):

    def get_current_user(self, request):
        if 'Authorization' in request.headers:
            return AccountService.get_user_by_knox_token(request)
        user = AccountService.get_user_by_cookie_token(request)
        if user:
            request.current_user = user
        return user
