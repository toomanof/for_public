from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User
from .models import Profile, AuthPhoneCode, ClientModel, ProcedureModel, DeviceUserModel, SubscriptionModel


@admin.register(AuthPhoneCode)
class AuthPhoneCodeInline(admin.ModelAdmin):
    model = AuthPhoneCode
    can_delete = False
    list_display =('profile', 'value', 'created_at', 'update_at')


@admin.register(ClientModel)
class ClientAdmin(admin.ModelAdmin):
    model = ClientModel
    list_display = ('performer', 'first_name', 'last_name', 'last_visit', 'reception_date')
    list_filter = ('performer', 'first_name', 'last_name', 'last_visit', 'reception_date')
    search_fields = ('first_name', 'last_name',)


@admin.register(ProcedureModel)
class ProcedureAdmin(admin.ModelAdmin):
    model = ProcedureModel
    list_display = ('title', 'performer', 'description', 'duration', 'interval', 'price', 'prime_cost')


class ProfileInline(admin.StackedInline):
    model = Profile
    can_delete = False
    fk_name = 'user'


class DeviceUserInline(admin.StackedInline):
    model = DeviceUserModel
    can_delete = False
    fk_name = 'user'


class UserAdmin(BaseUserAdmin):
    search_fields = ('id', 'username', 'email', 'first_name', 'last_name')
    list_display = ('id', 'username', 'email', 'first_name', 'last_name', 'is_active')
    inlines = (ProfileInline, DeviceUserInline, )


admin.site.unregister(User)
admin.site.register(User, UserAdmin)


@admin.register(SubscriptionModel)
class SubscriptionAdmin(admin.ModelAdmin):
    model = SubscriptionModel
    list_display = ('title', 'duration',)
