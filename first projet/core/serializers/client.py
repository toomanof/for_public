from rest_framework import serializers

from ..models import ClientModel


class ClientSerializer(serializers.ModelSerializer):
    #procedures = serializers.SerializerMethodField()

    class Meta:
        model = ClientModel
        fields = '__all__'

    #def get_procedures(self, element):
    #    return((item.procedure.id, item.procedure.title) for item in element.appointments.all())
