from rest_framework import serializers

from ..models import ProcedureModel


class ProcedureSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProcedureModel
        fields = '__all__'
