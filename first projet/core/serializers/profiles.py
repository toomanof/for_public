from django.contrib.auth.models import User
from rest_framework import serializers

from ..models import Profile
from billing.services import SMSAccountingService, SubscriptionAccountingService


User._meta.get_field('email')._unique = True


class UserSmallInfoSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ['pk', 'username', 'email', 'first_name', 'last_name']


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ['pk', 'username', 'email', 'is_staff',
                  'last_login', 'first_name', 'last_name', 'is_superuser',
                  'is_active', 'date_joined', 'groups', 'user_permissions',
                  'profile']
        depth = 1

    def validate(self, data):
        """
        Валидация производится с учётом полей профиля.
        Т.е. в данные добавляется словарь profile с данными
        совпадающимми с полями Profile
        """
        profile_fields = [field.name for field in Profile._meta.get_fields()]
        self.profile_data = {
            key: val for key, val in self.initial_data.items() if key in profile_fields
        }
        return data

    def save(self, user_pk, **kwargs):
        """
        Сохранение производится как записи модели User так
        и связанной записи модели Profile
        """

        User.objects.filter(pk=user_pk).update(**self.validated_data)
        user = User.objects.get(pk=user_pk)

        if hasattr(self, 'profile_data'):
            Profile.objects.filter(pk=user.profile.pk).update(**self.profile_data)
        return user


class ProfileSerializer(serializers.ModelSerializer):
    email = serializers.SerializerMethodField()
    first_name = serializers.SerializerMethodField()
    last_name = serializers.SerializerMethodField()
    balance = serializers.SerializerMethodField()

    class Meta:
        model = Profile
        fields = '__all__'

    def get_balance(self, element):
        request = self.context.get("request")
        return {
            'sms': SMSAccountingService(element.user).balance,
            'subscriptions': SubscriptionAccountingService(request).balance_current_user()
        }

    def get_email(self, element):
        return element.user.email

    def get_first_name(self, element):
        return element.user.first_name

    def get_last_name(self, element):
        return element.user.last_name
