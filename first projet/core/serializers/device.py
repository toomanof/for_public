from rest_framework import serializers

from ..models import DeviceUserModel


class DeviceUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = DeviceUserModel
        fields = '__all__'
