from datetime import timedelta, datetime
from django.utils import timezone

ERROR_DATA_NOT_TRANSMIT = 'Correct data not transmitted'
ERROR_USER_NOT_FOUND = 'User not found'

POST = 'POST'
GET = 'GET'

MT_HOUR, MT_DAY = range(2)


def timedelta_with_measurements(interval, measurements):
    if measurements == MT_HOUR:
        return timedelta(hours=interval)
    elif measurements == MT_DAY:
        return timedelta(days=interval)


def get_date_to_minus_interval(interval: int,
                               measurements: int,
                               value: datetime = None) -> datetime:
    if not value:
        return timezone.now() - timedelta_with_measurements(interval, measurements)
    return value - timedelta_with_measurements(interval, measurements)


def get_date_to_plus_interval(interval: int,
                              measurements: int,
                              value: datetime = None) -> datetime:
    if not value:
        return timezone.now() + timedelta_with_measurements(interval, measurements)
    return value + timedelta_with_measurements(interval, measurements)


def get_begin_end_day_with(year: int, month: int, day: int) -> tuple:
    begin_moment = datetime.strptime(f'{year}/{month}/{day} 00:00:00',
                                     '%Y/%m/%d %H:%M:%S')
    end_moment = datetime.strptime(f'{year}/{month}/{day} 23:59:59',
                                   '%Y/%m/%d %H:%M:%S')
    return begin_moment, end_moment


def get_begin_end_day(value: datetime) -> tuple:

    begin_moment = datetime.strptime(f'{value.year}/{value.month}/{value.day} 00:00:00',
                                     '%Y/%m/%d %H:%M:%S')
    end_moment = datetime.strptime(f'{value.year}/{value.month}/{value.day} 23:59:59',
                                   '%Y/%m/%d %H:%M:%S')
    return(begin_moment, end_moment)
