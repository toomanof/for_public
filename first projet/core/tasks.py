from celery import Celery

from .services import ClientsService


app = Celery()


@app.task
def calculate_active_all_clients():
    ClientsService.calculate_active_all_clients()
