from django.urls import path
from rest_framework.routers import DefaultRouter
from ..views import *

app_name = 'api_core_procedure'
router = DefaultRouter(trailing_slash=False)


router.register('procedures',
                ProcedureViewSet, basename='procedures')

urlpatterns = router.urls
