from django.urls import path

from ..views import (
    applications, GoogleLogin, FaceBookLogin, VKLogin,
    ConnectGoogleAccount, ConnectFaceBookAccount,
    ConnectVKAccount)
app_name = 'api_oauth2'

urlpatterns = [
    path('auth/google', GoogleLogin.as_view(), name='google_login'),
    path('auth/facebook', FaceBookLogin.as_view(), name='facebook_login'),
    path('auth/vk', VKLogin.as_view(), name='vk_login'),

    path('connect/google', ConnectGoogleAccount.as_view(), name='google_login'),
    path('connect/facebook', ConnectFaceBookAccount.as_view(), name='facebook_login'),
    path('connect/vk', ConnectVKAccount.as_view(), name='vk_login'),

]
