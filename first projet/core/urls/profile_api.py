from django.urls import path
from rest_framework.routers import DefaultRouter

from ..views import (
    ProfileApiView,
    DevicesTokensViewSet,
    DownloadAvatarApiView,
)

app_name = 'api_core_profile'
router = DefaultRouter(trailing_slash=False)

router.register('/devices/tokens',
                DevicesTokensViewSet, basename='devices_tokens')

urlpatterns = [
    path('', ProfileApiView.as_view(),
         name='profile'),
    path('/avatar', DownloadAvatarApiView.as_view(),
         name='download_avatar'),

] + router.urls
