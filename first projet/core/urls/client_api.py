from django.urls import path
from rest_framework.routers import DefaultRouter

from appointment.views import AppointmentViewSet
from ..views import *

app_name = 'api_core_clients'
router = DefaultRouter(trailing_slash=False)


router.register(r'clients', ClientViewSet, basename='clients')
router.register(r'clients/(?P<id_client>\d+)/receptions',
                AppointmentViewSet, basename='receptions')

urlpatterns = router.urls
