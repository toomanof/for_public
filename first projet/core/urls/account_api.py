from django.urls import path
from rest_framework.routers import DefaultRouter

from ..views import (
    SendSmsAuthCodeApiView,
    ActivateUserWithAuthCodeApiView,
    CheckPhoneApiView)

app_name = 'api_core_account'
router = DefaultRouter(trailing_slash=False)

urlpatterns = [
    path('auth/sms', SendSmsAuthCodeApiView.as_view(),
         name='send_sms_auth_code'),
    path('auth/sms/confirmation',
         ActivateUserWithAuthCodeApiView.as_view(),
         name='check_auth_code'),
    path('auth/check-phone',
         CheckPhoneApiView.as_view(),
         name='check_phone'),
]


urlpatterns += router.urls
