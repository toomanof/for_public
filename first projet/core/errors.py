"""
   Модуль ошибок уровня приложения
"""
import time
import logging
import resource

from django.db import connection
from rest_framework import status
from rest_framework.exceptions import APIException

from .constants import ERROR_USER_NOT_FOUND

logger_error = logging.getLogger('warning')
logger_timer = logging.getLogger('timer')


def timer_function(method):
    """
    Декоратор замеряющий время исполнения финкции и количество выполнения запросов к базе
    """
    def wrapper(self, *args, **kwargs):
        start_mem = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss
        start_qc = len(connection.queries)
        start_time = time.monotonic()

        logger_timer.info(f'Timer_function! Start method: "{self.__class__.__name__}.{method.__name__}"')

        result = method(self, *args, **kwargs)

        logger_timer.info("total queries function '%s %s: %d', time: %.4f sec, memory: %.4f Mb" % (
            self.__class__.__name__,
            method.__name__,
            (len(connection.queries) - start_qc),
            time.time() - start_time,
            (resource.getrusage(resource.RUSAGE_SELF).ru_maxrss - start_mem) / 1000.0
        ))
        return result
    return wrapper


class AccessError(APIException):
    status_code = status.HTTP_403_FORBIDDEN
    default_detail = 'Access denied!.'
    default_code = 'access_error'


class ConflictError(APIException):
    status_code = status.HTTP_409_CONFLICT
    default_detail = 'Conflict  data in request.'
    default_code = 'conflict_error'


class PromoCodeExpiredError(APIException):
    status_code = status.HTTP_410_GONE
    default_detail = 'Promo code expired.'
    default_code = 'expired_error'


class NotHasPossibilityAccount(Exception):
    """Абстрактный класс"""
    sub_message = ''

    def __init__(self, user, show=False, *args, **kwargs):
        self.message = f'{user}: {self.sub_message}'
        super().__init__(show, *args, **kwargs)


class UserNotFoundException(APIException):
    """Исключение отсутсвия пользователя в базе"""
    status_code = status.HTTP_403_FORBIDDEN
    default_detail = ERROR_USER_NOT_FOUND


class UserNotSubscriptionInAccountException(APIException):
    """Исключение отсутсвия  у пользователя смс на счету"""
    status_code = status.HTTP_402_PAYMENT_REQUIRED
    default_detail = 'You not have subscriptions in your account!'


class UserNotSMSInAccountException(APIException):
    """Исключение отсутсвия  у пользователя смс на счету"""
    status_code = status.HTTP_402_PAYMENT_REQUIRED
    default_detail = 'You not have sms in your account!'
