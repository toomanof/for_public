from icecream import ic
from rest_framework.response import Response
from rest_framework.views import APIView

from core.services import wrapper_response
from ..models import GOOGLE_PLAY
from ..services import PaymentAppService


class GoogleNotificationsView(APIView):

    def post(self, request, *args, **kwargs):
        ic('GoogleNotificationsView', request.data, request.POST)
        return Response('')


class GoogleReceiptView(APIView):

    @wrapper_response
    def post(self, request, *args, **kwargs):
        payment_service = PaymentAppService(request, market=GOOGLE_PLAY)
        if not payment_service.is_verify_receipt:
            ic('GOOGLE_PLAY not is_verify_receipt')
            return Response({'status': 'error', 'msg': 'Receipt not verified!'})
        ic('GOOGLE_PLAY  is_verify_receipt')

        return payment_service.move_through_register()
