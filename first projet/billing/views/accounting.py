from rest_framework.views import APIView
from rest_framework.response import Response

from ..services import SMSAccountingService


class SMSBalance(APIView):

    def get(self, request):
        return Response(
            {'qnt': SMSAccountingService(request.current_user).balance}
        )
