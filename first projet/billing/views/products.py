from rest_framework.generics import ListAPIView
from rest_framework import permissions

from ..models import ProductModel
from ..serializers import ProductSerializer


class ProductsListView(ListAPIView):
    permission_classes = (permissions.IsAuthenticated, )

    queryset = ProductModel.objects.all()
    serializer_class = ProductSerializer
