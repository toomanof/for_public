from icecream import ic
from rest_framework.response import Response
from rest_framework.views import APIView

from core.services import wrapper_response
from ..models import APP_STORE
from ..services import PaymentAppService


class AppleNotificationsView(APIView):

    @wrapper_response
    def post(self, request, *args, **kwargs):
        ic(request.data, request.POST)

        unified_receipt = request.data.get('unified_receipt', None)
        assert unified_receipt, 'Incoming data does not match the pattern!'
        latest_receipt = unified_receipt.get('latest_receipt', None)
        assert unified_receipt, 'Incoming data does not match the pattern!'
        request.data['market'] = APP_STORE
        PaymentAppService.save_notification(latest_receipt, request.data)
        return Response({'status': 'success'})


class AppleReceiptView(APIView):

    @wrapper_response
    def post(self, request, *args, **kwargs):
        payment_service = PaymentAppService(request, market=APP_STORE)
        if not payment_service.is_verify_receipt:
            return Response({'status': 'error', 'msg': 'Receipt not verified!'})

        return payment_service.move_through_register()
