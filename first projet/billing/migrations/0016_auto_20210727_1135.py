# Generated by Django 3.2.3 on 2021-07-27 08:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('billing', '0015_auto_20210721_1646'),
    ]

    operations = [
        migrations.AlterField(
            model_name='notificationmodel',
            name='market',
            field=models.SmallIntegerField(choices=[(0, 'Google Play'), (1, 'App Store')], default=1, verbose_name='Магазин приложений'),
        ),
        migrations.AlterField(
            model_name='receiptmodel',
            name='market',
            field=models.SmallIntegerField(choices=[(0, 'Google Play'), (1, 'App Store')], default=1, verbose_name='Магазин приложений'),
        ),
    ]
