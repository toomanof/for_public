# Generated by Django 3.2.3 on 2021-07-20 12:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('billing', '0010_auto_20210720_1453'),
    ]

    operations = [
        migrations.AlterField(
            model_name='applenotificationmodel',
            name='data',
            field=models.JSONField(verbose_name='Данные с AppStore'),
        ),
        migrations.AlterField(
            model_name='applereceiptmodel',
            name='data',
            field=models.JSONField(verbose_name='Данные с AppStore'),
        ),
    ]
