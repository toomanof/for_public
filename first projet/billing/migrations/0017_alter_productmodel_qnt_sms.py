# Generated by Django 3.2.3 on 2021-07-27 09:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('billing', '0016_auto_20210727_1135'),
    ]

    operations = [
        migrations.AlterField(
            model_name='productmodel',
            name='qnt_sms',
            field=models.SmallIntegerField(blank=True, null=True, verbose_name='Кол-во начисляеммых баллов'),
        ),
    ]
