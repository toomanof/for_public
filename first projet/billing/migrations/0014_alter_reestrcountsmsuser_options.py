# Generated by Django 3.2.3 on 2021-07-21 09:51

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('billing', '0013_auto_20210720_1545'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='reestrcountsmsuser',
            options={'verbose_name': 'Остаток суммы по смс', 'verbose_name_plural': 'Баланс счетов по смс'},
        ),
    ]
