from celery import Celery

from .services import SubscriptionAccountingService

app = Celery()


@app.task
def checking_activity_subscriptions():
    SubscriptionAccountingService().calculate_active_subscriptions()
