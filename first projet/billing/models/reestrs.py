from functools import cached_property

from icecream import ic
from dateutil.relativedelta import relativedelta
from django.utils import timezone

from django.contrib.auth.models import User
from django.db import models


GOOGLE_PLAY, APP_STORE = range(2)

MARKETS = (
    (GOOGLE_PLAY, 'Google Play', ),
    (APP_STORE, 'App Store',)
)


class ReestrSubscription(models.Model):
    """
    Реестр подписок пользователей
    """
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='subscriptions',
        verbose_name='Пользователь')
    subscription = models.ForeignKey(
        'core.SubscriptionModel', on_delete=models.CASCADE,
        verbose_name='Подписка', related_name='records_reestr_subs')
    active = models.BooleanField('Флаг активности подписки', default=False)
    date_create = models.DateTimeField('Дата записи', auto_now_add=True)
    date_update = models.DateTimeField('Дата обновление записи', auto_now=True)

    class Meta:
        verbose_name = 'Запись реестра подписок пользователя'
        verbose_name_plural = 'Реестр подписок пользоватлелей'

    @cached_property
    def date_end(self):
        return self.date_create + relativedelta(months=+self.subscription.duration)


class ReestrCountSMSUser(models.Model):
    """
    Реестр остатка СМС на счеу пользователя
    """
    user = models.OneToOneField(
        User, on_delete=models.CASCADE, related_name='remainder_sms',
        verbose_name='Пользователь')
    remainder = models.PositiveSmallIntegerField('Остаток СМС', default=0)
    date_update = models.DateTimeField('Дата обновление записи', auto_now=True)

    class Meta:
        verbose_name = 'Остаток суммы по смс'
        verbose_name_plural = 'Баланс счетов по смс'

    def add(self, value):
        self.remainder += value
        self.save()

    def subtract(self, value):
        if self.remainder - value < 0:
            self.remainder = 0
        else:
            self.remainder -= value
        self.save()


class ReceiptModel(models.Model):
    token = models.TextField('Токен квитанции')
    product_id = models.CharField('sku в App Store', max_length=255,
                                  blank=True, null=True)
    data = models.JSONField('Данные с AppStore')
    date_create = models.DateTimeField('Дата записи', auto_now_add=True)
    date_update = models.DateTimeField('Дата обновление записи', auto_now=True)
    active = models.BooleanField('Флаг активности', default=True)
    market = models.SmallIntegerField('Магазин приложений',
                                      choices=MARKETS, default=APP_STORE)

    class Meta:
        verbose_name = 'Квитанция с магазина приложений'
        verbose_name_plural = 'Квитанции с магазинов приложений'


class NotificationModel(models.Model):
    token = models.TextField('Токен квитанции')
    data = models.JSONField('Данные с AppStore')
    date_create = models.DateTimeField('Дата записи', auto_now_add=True)
    date_update = models.DateTimeField('Дата обновление записи', auto_now=True)
    active = models.BooleanField('Флаг активности', default=True)
    market = models.SmallIntegerField('Магазин приложений',
                                      choices=MARKETS, default=APP_STORE)

    class Meta:
        verbose_name = 'Уведомление с магазина приложений'
        verbose_name_plural = 'Уведомления с магазина приложений'
