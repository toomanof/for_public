from django.db import models


class ProductModel(models.Model):
    T_PRODUCT, T_SUBSCRIPTION = range(2)
    TYPE_CHOICE = (
        (T_PRODUCT, 'товар'),
        (T_SUBSCRIPTION, 'подписка')
    )

    title = models.CharField('Ниименование', max_length=255)
    description = models.TextField('Описание', blank=True, null=True)
    appstore_id = models.CharField('sku в App Store', max_length=255, blank=True, null=True)
    play_id = models.CharField('sku в Google Play', max_length=255, blank=True, null=True)
    type = models.SmallIntegerField('тип', choices=TYPE_CHOICE, default=T_PRODUCT)
    subscription = models.ForeignKey(
        'core.SubscriptionModel', on_delete=models.CASCADE,
        verbose_name='Подписка', related_name='products',
        blank=True, null=True)
    qnt_sms = models.SmallIntegerField('Кол-во начисляеммых баллов', blank=True, null=True)

    class Meta:
        verbose_name = 'Товар'
        verbose_name_plural = 'Товары'

    def __str__(self):
        return self.title

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.type == ProductModel.T_SUBSCRIPTION:
            assert self.subscription, \
                'Продукт типа подписка должен содержать ссылку на модель подписки'
        elif self.type == ProductModel.T_PRODUCT:
            assert self.qnt_sms, 'Продукт типа продукт должен содержать количество СМС'

        super().save(force_insert, force_update, using, update_fields)
