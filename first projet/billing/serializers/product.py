from rest_framework import serializers

from ..models import ProductModel


class ProductSerializer(serializers.ModelSerializer):

    class Meta:
        model = ProductModel
        exclude = ('subscription', 'qnt_sms')
