from icecream import ic
from functools import cached_property
from django.utils import timezone

from billing.models import ReestrSubscription, ReestrCountSMSUser
from core.errors import UserNotSMSInAccountException


class SubscriptionAccountingService:

    def __init__(self, request=None):
        self.request = request

    def add_subscription(self, subscription):
        assert self.request, 'This method will only work with a request!'

        ReestrSubscription.objects.update_or_create(
            user=self.request.current_user,
            subscription=subscription,
            active=True
        )

    def remove_subscription(self, subscription):
        assert self.request, 'This method will only work with a request!'
        ReestrSubscription.objects.fiter(
            user=self.request.current_user,
            subscription=subscription,
        ).delete()

    def calculate_active_subscriptions(self):
        """
        Просмотр всех подписок на строк их действия.
        При истечении срока выставляет флаг active=False
        """
        for record in ReestrSubscription.objects.filter(active=True):
            record.active = record.date_end >= timezone.now()
            record.save()

    def balance_current_user(self):
        assert self.request, 'This method will only work with a request!'

        active = False
        date_end = timezone.now()
        change_date_end = False

        for record in ReestrSubscription.objects.filter(active=True, user=self.request.current_user):
            if record.date_end > date_end:
                change_date_end = True
                date_end = record.date_end
            active = True

        return {
            'active': active,
            'date_end': date_end if change_date_end else None
        }


class SMSAccountingService:

    def __init__(self, current_user):
        self.record_reest_about_user, _ =\
            ReestrCountSMSUser.objects.get_or_create(user=current_user)

    def add(self, value):
        self.record_reest_about_user.add(value)

    @cached_property
    def balance(self):
        return self.record_reest_about_user.remainder

    def subtract(self, value):
        self.record_reest_about_user.subtract(value)
