import base64
import json

import requests
import pickle
from inapppy import GooglePlayVerifier, errors
from functools import cached_property
from icecream import ic
from django.conf import settings
from .accounting import SMSAccountingService, SubscriptionAccountingService
from ..models import GOOGLE_PLAY, APP_STORE, ProductModel, ReceiptModel, NotificationModel

URL_VERIFY_RECEIPT = 'https://buy.itunes.apple.com/verifyReceipt'
URL_VERIFY_RECEIPT_TESTING = 'https://sandbox.itunes.apple.com/verifyReceipt'


class PaymentAppService:
    """
    Сервис производящий взаимосвязсь по платежам
    между App приложением и AppStore
    """
    data_from_server = property()

    def __init__(self, request: object, market: int, **kwargs):
        ic('PaymentAppleService __init__', request.data)
        key_receipt_token = 'transaction_receipt' if market == APP_STORE else 'purchase_token'

        self.market = market
        self.request = request
        self._obj_product = None
        self._product_id = request.data.get('product_id', None)
        self._transaction_id = request.data.get('transaction_id', None)
        self._receipt_token = request.data.get(key_receipt_token, None)
        self._is_verify_receipt = False
        self._quantity = None
        self._data_from_server = None

        assert self.receipt_is_not_used, 'Receipt is used!'

    @data_from_server.getter
    def data_from_server(self):
        return self._data_from_server

    @data_from_server.setter
    def data_from_server(self, value):
        self._data_from_server = value

    @cached_property
    def product(self):
        """
        Возвражает объект модели продукта соответсвующий переданому product_id
        """
        field_market = 'appstore_id' if self.market==APP_STORE else 'play_id'
        self._obj_product = ProductModel.objects.filter(**{field_market: self._product_id}).first()
        assert self._obj_product, f'Not found product with productId: {self._product_id}'

        ic('get product in PaymentAppService', self._obj_product)
        return self._obj_product

    @cached_property
    def receipt_is_not_used(self):
        return not ReceiptModel.objects.filter(
            token=self._receipt_token, active=False).exists()

    @staticmethod
    def save_notification(token, _data):
        obj, _ = NotificationModel.objects.update_or_create(
            token=token, defaults={'data': _data})
        return obj

    @staticmethod
    def save_receipt(token, **kwargs):
        ic('ReceiptModel.objects.update_or_create', kwargs)
        obj, _ = ReceiptModel.objects.update_or_create(
            token=token, defaults=kwargs)
        return obj

    def send_post_to_app_store_server(self, url: str) -> dict:
        assert self._receipt_token, 'Not set property receipt!'

        payload = {
            "receipt-data": self._receipt_token
        }
        password = self.get_password_receipt()
        if password:
            payload['password'] = password

        response = requests.post(url=url, json=payload)
        try:
            json_responce = response.json()
        except Exception as e:
            return {'status': 'error', 'msg': e}

        return json_responce

    @cached_property
    def verify_receipt_in_prod_server(self):
        response = self.send_post_to_app_store_server(URL_VERIFY_RECEIPT)
        ic(response)
        status = response.get('status', None)
        ic(response, status)
        return status and status == 21007

    @cached_property
    def verify_receipt_in_test_server(self):
        assert self._product_id, 'Not set property product_id'

        response = self.send_post_to_app_store_server(URL_VERIFY_RECEIPT_TESTING)
        receipt = response.get('receipt', None)
        status = response.get('status', None)

        assert receipt, f'App store return status {status}'

        self.data_from_server = receipt.get('in_app', None) if isinstance(receipt, dict) else None
        ic(self._data_from_server)

        for data in self._data_from_server:
            if 'transaction_id' in data and 'product_id' in data:
                ic(data['transaction_id'], self._transaction_id, data['product_id'], self._product_id)
                result = (data['transaction_id'] == self._transaction_id
                          and data['product_id'] == self._product_id)
                if result:
                    PaymentAppService.save_receipt(
                        self._receipt_token, data=receipt,
                        product_id=self._product_id, market=self.market)
                    return result
        return False

    @cached_property
    def verify_receipt_in_google_server(self):
        assert self._product_id, 'Not set property product_id'
        bundle_id = "online.smmlab.crm"
        with open(settings.CONFIG_DIR + '/pc-api-6137064794369301934-212-6512e9bb4771.json') as json_file:
            api_credentials = json.load(json_file)

        verifier = GooglePlayVerifier(bundle_id, api_credentials)
        ic('verify_receipt_in_google_server', api_credentials)
        ic(verifier)
        ic(self._receipt_token)
        ic(self._product_id)
        try:
            result = verifier.verify(
                self._receipt_token,
                self._product_id,
                is_subscription=self.product.type == ProductModel.T_SUBSCRIPTION
            )
            ic('receipt is verify')
            return True
        except errors.GoogleError as exc:
            ic('Purchase validation failed {}'.format(exc))
        return False

    @property
    def is_verify_receipt(self):
        """
        В начале отправляется запрос на боевй сервер, если получаем статус 21007, то
        отправляется запрос на тестовый сервер при получении 200 сопоставляем productId
        """
        if self.market == APP_STORE:
            self._is_verify_receipt = self.verify_receipt_in_prod_server \
                                      and self.verify_receipt_in_test_server
        elif self.market == GOOGLE_PLAY:
            self._is_verify_receipt = self.verify_receipt_in_google_server
        return self._is_verify_receipt

    def get_password_receipt(self):
        #  password = None
        #  receipt_obj = AppleNotificationModel.objects.filter(token=self._receipt_token).first()
        #  if receipt_obj:
        #      password = receipt_obj.data.get('password', None)
        #  return password
        return settings.PASSWORD_APP_STORE

    def move_through_register(self):
        """
        Движение соотвесвующих регистров в зависимости от типа приобретоенного товара
        """
        try:
            ic('move_through_register', self.product, self.product.type)
            if self.product.type == ProductModel.T_PRODUCT:

                assert self.product.qnt_sms, 'The item must have a quantity sms'
                SMSAccountingService(self.request.current_user).add(self.product.qnt_sms)

            elif self.product.type == ProductModel.T_SUBSCRIPTION:

                assert self.product.subscription, 'The item must have a subscription'
                SubscriptionAccountingService(self.request).add_subscription(self.product.subscription)
        except Exception as e:
            return {'status': 'error', 'msg': str(e)}

        PaymentAppService.save_receipt(
            self._receipt_token,
            active=False,
            product_id=self._product_id,
            data=self.request.data,
            market=self.market)
        return {'status': 'success', 'msg': f'Item {self.product.title}  purchased'}
