from django.urls import path

from .views import AppleNotificationsView, AppleReceiptView, \
    GoogleNotificationsView, GoogleReceiptView, ProductsListView, SMSBalance

urlpatterns = [
    path('apple/notifications',
         AppleNotificationsView.as_view(),
         name='apple_notifications'),
    path('apple/receipt',
         AppleReceiptView.as_view(),
         name='apple_receipt'),
    path('google/notifications',
         GoogleNotificationsView.as_view(),
         name='google_notifications'),
    path('google/receipt',
         GoogleReceiptView.as_view(),
         name='google_receipt'),
    path('balance/sms',
         SMSBalance.as_view(),
         name='sms_balance'),
    path('products',
         ProductsListView.as_view(),
         name='products_list'),
]