from django.contrib import admin

from .models import ProductModel, ReestrSubscription,\
    ReestrCountSMSUser, NotificationModel, ReceiptModel


@admin.register(ProductModel)
class ProductAdmin(admin.ModelAdmin):
    model = ProductModel
    list_display = ('title', 'appstore_id', 'play_id', )
    search_fields = ('title', 'appstore_id', 'play_id', )


@admin.register(ReestrSubscription)
class ReestrSubscriptionAdmin(admin.ModelAdmin):
    model = ReestrSubscription
    list_display = ('user', 'subscription', 'active', 'date_create', 'date_update')
    list_filter = ('user', 'subscription', 'active',)


@admin.register(ReestrCountSMSUser)
class ReestrCountSMSUserAdmin(admin.ModelAdmin):
    model = ReestrCountSMSUser
    list_display = ('user', 'remainder', 'date_update',)
    list_filter = ('user',)


@admin.register(NotificationModel)
class AppleNotificationModelAdmin(admin.ModelAdmin):
    model = NotificationModel
    list_display = ('pk', 'date_create', 'date_update', 'active', 'market',)


@admin.register(ReceiptModel)
class AppleReceiptModelAdmin(admin.ModelAdmin):
    model = ReceiptModel
    list_display = ('product_id', 'date_create', 'date_update', 'active', 'market',)
