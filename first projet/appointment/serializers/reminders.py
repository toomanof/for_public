from rest_framework import serializers
from ..models import AppointmentModel


class ClientsRegisteredProceduresSerializer(serializers.ModelSerializer):
    price = serializers.SerializerMethodField()
    last_name = serializers.SerializerMethodField()
    first_name = serializers.SerializerMethodField()
    procedure_title = serializers.SerializerMethodField()

    class Meta:
        model = AppointmentModel
        fields = ('id', 'client_id', 'procedure_id',
                  'reception_date', 'last_visit',
                  'done', 'note', 'price', 'last_name', 'first_name',
                  'procedure_title')

    def get_price(self, element):
        return element.price

    def get_first_name(self, element):
        return element.client.first_name

    def get_last_name(self, element):
        return element.client.last_name

    def get_procedure_title(self, element):
        return element.procedure.title


class ClientsUnmarkedSerializer(serializers.ModelSerializer):
    price = serializers.SerializerMethodField()
    last_name = serializers.SerializerMethodField()
    first_name = serializers.SerializerMethodField()
    procedure_title = serializers.SerializerMethodField()

    class Meta:
        model = AppointmentModel
        fields = ('id', 'client_id', 'procedure_id',
                  'reception_date', 'last_visit',
                  'done', 'note', 'price',
                  'last_name', 'first_name',
                  'procedure_title')

    def get_price(self, element):
        return element.procedure.price

    def get_first_name(self, element):
        return element.client.first_name

    def get_last_name(self, element):
        return element.client.last_name

    def get_procedure_title(self, element):
        return element.procedure.title
