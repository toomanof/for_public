from rest_framework import serializers

from ..models import AppointmentModel


class AppointmentSerializer(serializers.ModelSerializer):
    procedure_obj = serializers.SerializerMethodField()

    class Meta:
        model = AppointmentModel
        fields = '__all__'

    def get_procedure_obj(self, element):
        print('AppointmentSerializer get_procedure', element)
        return {'id': element.procedure.id,
                'title': element.procedure.title} if element else {}


class AppointmentSmallSerializer(serializers.ModelSerializer):


    class Meta:
        model = AppointmentModel
        fields = '__all__'

    def get_date(self, element):
        return element.reception_date

    def get_duration(self, element):
        return element.procedure.duration

    def get_client_id(self, element):
        return element.client.id

    def get_first_name(self, element):
        return element.client.first_name

    def get_last_name(self, element):
        return element.client.last_name


class AppointmentCountSerializer(serializers.ModelSerializer):
    total = serializers.IntegerField( read_only=True)

    class Meta:
        model = AppointmentModel
        fields = ('reception_date', 'total',)

