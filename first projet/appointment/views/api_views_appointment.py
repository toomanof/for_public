from datetime import datetime
from icecream import ic
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet
from rest_framework import permissions
from django.db.models import Count

from core.constants import get_begin_end_day_with
from core.errors import UserNotSubscriptionInAccountException
from core.services import wrapper_response, SubscriptionManagementService
from ..models import AppointmentModel
from ..serializers import AppointmentSerializer, AppointmentCountSerializer


class AppointmentViewSet(ModelViewSet):
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = AppointmentSerializer

    def get_queryset(self):
        return AppointmentModel.objects.filter(
            client_id=self.kwargs['id_client'],
            performer=self.request.current_user)

    @wrapper_response
    def create(self, request, id_client, *args, **kwargs):
        sub_manager = SubscriptionManagementService(request)
        if not sub_manager.has_ability_create_multiple_appointments:
            raise UserNotSubscriptionInAccountException()

        request.data['client'] = self.kwargs['id_client']
        request.data['performer'] = self.request.current_user.id
        response = super().create(request, *args, **kwargs)
        instance = self.get_queryset().get(pk=response.data['id'])
        sub_manager.save_appointment_to_restriction(instance)
        return response

    @wrapper_response
    def update(self, request, id_client, *args, **kwargs):
        request.data['performer'] = self.request.current_user.id
        request.data['client'] = self.kwargs['id_client']
        return super().update(request, *args, **kwargs)


class AppointmentsListView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    @wrapper_response
    def get(self, *args, **kwargs):
        begin_moment, end_moment = get_begin_end_day_with(
            year=kwargs["year"], month=kwargs["month"], day=kwargs["day"])

        data = AppointmentModel.objects.filter(
                reception_date__gte=begin_moment,
                reception_date__lte=end_moment,
                performer=self.request.current_user)\
            .values('id', 'client__first_name', 'client__last_name',
                    'client_id', 'procedure__duration', 'procedure__title',
                    'reception_date', 'done')\
            .order_by('reception_date')
        for item in data:
            item['duration'] = item.pop('procedure__duration')
            item['first_name'] = item.pop('client__first_name')
            item['last_name'] = item.pop('client__last_name')
            item['date'] = item.pop('reception_date')
            item['procedure'] = item.pop('procedure__title')
        return data


class AppointmentsCountListView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    @wrapper_response
    def get(self, *args, **kwargs):

        begin_moment = datetime.strptime(f"{kwargs['from_year']}/{kwargs['from_month']}/{kwargs['from_day']} 00:00:00",
                                         "%Y/%m/%d %H:%M:%S")
        end_moment = datetime.strptime(f"{kwargs['to_year']}/{kwargs['to_month']}/{kwargs['to_day']} 23:59:59",
                                       "%Y/%m/%d %H:%M:%S")

        data = AppointmentModel.objects\
            .filter(
                reception_date__gte=begin_moment,
                reception_date__lte=end_moment,
                performer=self.request.current_user)\
            .values('reception_date').annotate(total=Count('id'))
        return AppointmentCountSerializer(data, many=True).data
