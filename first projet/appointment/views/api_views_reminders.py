from icecream import ic
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.generics import ListAPIView

from appointment.services import Reminder, PushSender
from appointment.serializers import \
    ClientsRegisteredProceduresSerializer,\
    ClientsUnmarkedSerializer


class BaseTasksViewList(APIView):
    """
    Общие данные по заданиям
    Типы задач:
    Запись клиентов = 0,
    Отметить клиентов = 1
    """
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, **kwargs):
        return Response(Reminder(self.request).total())


class UnmarkedClientsViewList(ListAPIView):
    """
    Список клиентов у которых время на следующую записьпрошло
    и записи не отмечены как выполненные
    """
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = ClientsUnmarkedSerializer

    def get_queryset(self):
        return Reminder(self.request).clients_unmarked()


class RemindClientsToAppointmentsViewList(ListAPIView):
    """
    Список клиентов у которых подходит время на следующую запись
    """
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = ClientsRegisteredProceduresSerializer

    def get_queryset(self):
        return Reminder(self.request).clients_registered_for_procedures
