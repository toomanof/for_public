# Generated by Django 3.2.3 on 2021-07-02 15:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('appointment', '0005_appointmentmodel_performer'),
    ]

    operations = [
        migrations.AddField(
            model_name='appointmentmodel',
            name='note',
            field=models.TextField(blank=True, null=True, verbose_name='Заметка'),
        ),
    ]
