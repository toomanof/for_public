from celery import Celery
from django.utils import timezone

from .services import PushSender

app = Celery()


@app.task
def send_push_remind_about_procedures_in_today():
    PushSender().send_remind_procedures_upcoming_today()


@app.task
def send_push_remind_about_unmarked_clients():
    PushSender().send_remind_clients_unmarked()


@app.task()
def send_push_before_procedure():
    PushSender().send_push_before_procedure()
