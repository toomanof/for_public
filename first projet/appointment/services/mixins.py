import requests
import firebase_admin
import json
from firebase_admin import credentials
from firebase_admin import messaging
from icecream import ic
from django.conf import settings


class MixinSendPushFireBase:
    registration_token = ''

    def initialize_app(self):
        cred = credentials.Certificate(
            f'{settings.CONFIG_DIR}/ilab-c97d8-firebase-adminsdk-54bbn-7e08786643.json')
        ic(cred)
        try:
            firebase_admin.get_app()
        except ValueError:
            firebase_admin.initialize_app(cred)

    def convert_token(self, token):
        url = 'https://iid.googleapis.com/iid/v1:batchImport'
        headers = {
            "Authorization": f"key={settings.FIREBASE_SERVER_KEY}",
           # "Content-Type": "application/json",
          #  "accept": 
        }
        payload = {
                "application": "crm.smmlab.app",
                "sandbox": True,
                "apns_tokens": [token]
            }
        ic('convert token', headers, payload)
        response = requests.post(url, headers=headers, json=payload)
        ic(response, response.headers)
        if response.status_code == requests.codes.ok:
            response_data = response.json()
            ic('response_data', response_data)
            if 'results' in response_data:
                if 'registration_token' in response_data['results'][0]:
                    return response_data['results'][0]['registration_token']
        if response.status_code == 400:
            response.raise_for_status()
        return token

    def send(self, recipient_token, title, body, data, type_os=0):
        ic('type_os', type_os)
        if type_os == 1:
            recipient_token = self.convert_token(recipient_token)
        self.initialize_app()
        ic(title, body, data)
        message = messaging.Message(
            data=data,
            notification=messaging.Notification(
                title=title,
                body=body,
                #sound='default',
                #channel_id='default-channel-id'
            ),
            token=recipient_token,
        )
        try:
            response = messaging.send(message)
        except Exception as e:
            ic("Error send notification", e)
        ic(response)
