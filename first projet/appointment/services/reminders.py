from datetime import timedelta
from django.db.models import Sum
from django.utils import timezone
from django.utils.functional import cached_property
from icecream import ic

from appointment.models import AppointmentModel, AppointmentDoneModel
from core.constants import get_begin_end_day


class Reminder:
    """
    Класс создающий напоминания о предстоящих записях на процедуры,
    о не отмеченных клиентов которым прошло время процедур.
    """

    INTERVAL_BEFORE_FIVE_DAYS = 5

    def __init__(self, request=None):
        if request:
            ic(request)
            self.request = request

    def add_condition(self):
        return f"AND app.performer_id={self.request.current_user.id}" if hasattr(self, 'request') else ""

    @property
    def get_sql_pre_entry(self) -> str:
        return f"SELECT " \
               f"app.id, app.performer_id, app.client_id, app.reception_date, "\
               f"app.last_visit, app.done, app.note, "\
               f"app.procedure_id , p.price "\
               f"FROM appointment_appointmentmodel as app "\
               f"LEFT JOIN core_proceduremodel as p "\
               f"ON app.procedure_id=p.id "\
               f"WHERE date(app.last_visit, (p.interval/2) ||' day')=CURRENT_DATE " \
               f"AND app.done=1 " \
               f"{self.add_condition()} "

    @property
    def get_sql_pre_entry_not_done(self) -> str:
        return f"SELECT " \
               f"app.id, app.performer_id, app.client_id, app.reception_date, "\
               f"app.last_visit, app.done, app.note, "\
               f"app.procedure_id , p.price "\
               f"FROM appointment_appointmentmodel as app "\
               f"LEFT JOIN core_proceduremodel as p "\
               f"ON app.procedure_id=p.id "\
               f"WHERE date(app.last_visit, (p.interval/2) ||' day')=CURRENT_DATE "\
               f"{self.add_condition()} "\
               f"AND app.done=1 "\
               f"AND app.id not in( "\
               f"SELECT pre_entry_id FROM appointment_appointmentdonemodel as app2 "\
               f"WHERE app2.procedure_id = app.procedure_id " \
               f"AND app2.client_id=app.client_id " \
               f"AND app2.performer_id=app.performer_id)"

    @property
    def get_sql_total_price_procedures_upcoming_today(self) -> str:
        return f"SELECT 1 as id, sum(p.price) as sum "\
               f"FROM appointment_appointmentmodel as app "\
               f"LEFT JOIN core_proceduremodel as p "\
               f"ON app.procedure_id=p.id "\
               f"WHERE date(app.last_visit, (p.interval/2) ||' day')=CURRENT_DATE " \
               f"AND app.done=1 " \
               f"{self.add_condition()} "

    def upcoming_procedure(self):
        """
        Получить записи о предстаящих процедурах за 15 минут
        """
        return self.selection(
            done=False,
            reception_date__range=(timezone.now() + timedelta(minutes=10),
                                   timezone.now() + timedelta(minutes=16))
        )

    def filter_with_current_user(self, **kwargs):
        if hasattr(self, 'request'):
            kwargs['performer'] = self.request.current_user
        return kwargs

    def selection(self, **kwargs):
        return AppointmentModel.objects.filter(
            **self.filter_with_current_user(**kwargs))

    @cached_property
    def clients_registered_for_procedures(self):
        return list(AppointmentModel.objects.raw(self.get_sql_pre_entry_not_done))

    def clients_unmarked(self):
        return self.selection(
            done=False,
            reception_date__range=get_begin_end_day(timezone.now())
        ).only('id', 'client_id', 'reception_date', 'last_visit', 'done',
               'procedure_id', 'procedure__price')

    @cached_property
    def procedures_upcoming_today(self):
        return list(AppointmentModel.objects.raw(self.get_sql_pre_entry))

    @property
    def count_procedures_upcoming_today(self):
        return len(self.procedures_upcoming_today)

    @cached_property
    def count_done_appointments(self):
        result = AppointmentDoneModel.objects.filter(**self.filter_with_current_user(
            pre_entry_id__in=self.procedures_upcoming_today
        )).count()
        return result if result else 0

    @cached_property
    def total_price_procedures_upcoming_today(self):
        result = list(AppointmentModel.objects.raw(self.get_sql_total_price_procedures_upcoming_today))
        ic(result[0].sum)
        return result[0].sum if result and result[0].sum else 0

    def total(self):
        profit_all_clients_in_today = self.selection(
            reception_date__range=get_begin_end_day(timezone.now())
        ).annotate(sum=Sum('procedure__price'))
        profit_all_clients_in_today = profit_all_clients_in_today[0].sum if profit_all_clients_in_today else 0

        count_all_clients_in_today = self.selection(
            reception_date__range=get_begin_end_day(timezone.now())
        ).count()

        count_clients_marked = self.selection(
            done=True,
            reception_date__range=get_begin_end_day(timezone.now())
        ).count()

        total_clients_registered_for_procedures = {
            'type': 0,
            'total': self.count_procedures_upcoming_today,
            'done': self.count_done_appointments,
            'profit': self.total_price_procedures_upcoming_today
        }

        total_clients_unmarked = {
            'type': 1,
            'total': count_all_clients_in_today,
            'done': count_clients_marked,
            'profit': profit_all_clients_in_today
        }
        return {
            0: total_clients_registered_for_procedures,
            1: total_clients_unmarked
        }


