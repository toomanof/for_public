from abc import ABC
from icecream import ic
from django.utils.functional import cached_property

from .reminders import Reminder
from .mixins import MixinSendPushFireBase

(
    TM_REMIND_ABOUT_PROCEDURES,
    TM_REMIND_MARKED_CLIENTS,
    TM_REMIND_UPCOMING_PROCEDURE,
    TM_NEGATIVE_BALANCE
 ) = range(4)

TITLE_REMIND_ABOUT_PROCEDURES = 'Запланированые процедуры'
TITLE_REMIND_MARKED_CLIENTS = 'Неотмеченные клиенты'
TITLE_REMIND_UPCOMING_PROCEDURE = 'Через несколько минут предстоит процедура'


class AbstractSendReminder(ABC):
    """
    Абстрактный класс отправляющий напроминания
    """
    def __init__(self):
        self.reminder = Reminder()

    @cached_property
    def procedures_upcoming_today(self):
        return self.reminder.procedures_upcoming_today()

    @cached_property
    def clients_unmarked(self):
        return self.reminder.clients_unmarked()

    def get_title(self):
        pass

    def get_body(self):
        pass

    def tokens_performers(self, list_values: list) -> list:
        result = []
        for appointment in list_values:
            result.extend(
                ((device.token, device.oc) for device in appointment.performer.devices.all()))

    def send(self, **kwargs):
        pass

    def send_remind_procedures_upcoming_today(self):
        for appointment in self.reminder.procedures_upcoming_today:
            for device in appointment.performer.devices.all():
                ic(device.token)
                try:
                    self.send(device.token,
                              TITLE_REMIND_ABOUT_PROCEDURES,
                              TITLE_REMIND_ABOUT_PROCEDURES,
                              data={
                                  'type_msg': str(TM_REMIND_ABOUT_PROCEDURES),
                                  'clientId': str(appointment.client.id),
                                  'receptionId': str(appointment.id)
                              },
                              type_os=device.os)
                except Exception as e:
                    pass

    def send_remind_clients_unmarked(self):
        for recipient_token, type_os in self.tokens_performers(self.clients_unmarked):
            ic(recipient_token)
            try:
                self.send(recipient_token,
                          TITLE_REMIND_MARKED_CLIENTS,
                          TITLE_REMIND_MARKED_CLIENTS,
                          data={'type_msg': str(TM_REMIND_ABOUT_PROCEDURES)},
                          type_os=type_os)
            except Exception as e:
                pass

    def send_push_before_procedure(self):
        for appointment in self.reminder.upcoming_procedure():
            ic(appointment)
            body = f'Клиент {appointment.client}. Заметка: {appointment.note}'
            for device in appointment.performer.devices.all():
                try:
                    self.send(device.token,
                              TITLE_REMIND_UPCOMING_PROCEDURE,
                              body,
                              data={
                                  'type_msg': str(TM_REMIND_UPCOMING_PROCEDURE),
                                  'clientId': str(appointment.client.id),
                                  'receptionId': str(appointment.id)
                              },
                              type_os=device.os)
                except Exception as e:
                    pass


class PushSender(MixinSendPushFireBase, AbstractSendReminder):
    pass
