from django.contrib import admin
from .models import *


@admin.register(AppointmentModel)
class AppointmentModelAdmin(admin.ModelAdmin):
    model = AppointmentModel
    can_delete = False
    list_display = ('performer',  'client', 'procedure',
                    'reception_date', 'last_visit', 'done',
                    'date_done', 'date_create', 'date_update')


@admin.register(AppointmentDoneModel)
class AppointmentModelDoneAdmin(admin.ModelAdmin):
    model = AppointmentDoneModel
    can_delete = False
    list_display = ('performer',  'client', 'procedure',
                    'last_visit', 'pre_entry',
                    'date_create', 'date_update')
