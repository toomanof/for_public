from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone
from django.db.models.signals import post_save
from django.dispatch import receiver


class AppointmentModel(models.Model):
    performer = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='appointments',
        verbose_name='Исполнитель процедур', blank=True, null=True)
    client = models.ForeignKey('core.ClientModel', on_delete=models.CASCADE,
                               related_name='appointments')
    procedure = models.ForeignKey('core.ProcedureModel', on_delete=models.CASCADE,
                                  related_name='appointments')
    reception_date = models.DateTimeField('Дата следующего визита', blank=True, null=True)
    last_visit = models.DateTimeField('Дата последнего визита', blank=True, null=True)
    done = models.BooleanField('Выполнено', default=False)
    date_done = models.DateTimeField('Дата исполнения', blank=True, null=True)
    date_create = models.DateTimeField('Дата записи', auto_now_add=True)
    date_update = models.DateTimeField('Дата обновление записи', auto_now=True)
    note = models.TextField('Заметка', blank=True, null=True)

    class Meta:
        verbose_name = "Запись на процедуру"
        verbose_name_plural = "Записи на процедуры"

    def __str__(self):
        return f"{self.procedure} {self.client}"

    def save(self, **kwargs):
        #if self.done:
        #    self.last_visit = timezone.now()
        #    self.reception_date = None
        return super().save(**kwargs)


class AppointmentDoneModel(models.Model):
    performer = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='done_appointments',
        verbose_name='Исполнитель процедур')
    client = models.ForeignKey('core.ClientModel', on_delete=models.CASCADE,
                               related_name='done_appointments')
    procedure = models.ForeignKey('core.ProcedureModel', on_delete=models.CASCADE,
                                  related_name='done_appointments')
    pre_entry = models.ForeignKey(
        AppointmentModel, on_delete=models.CASCADE, related_name='done_appointments',
        verbose_name='Предварительная запись')
    last_visit = models.DateTimeField('Дата последнего визита', blank=True, null=True)
    date_create = models.DateTimeField('Дата записи', auto_now_add=True)
    date_update = models.DateTimeField('Дата обновление записи', auto_now=True)

    class Meta:
        verbose_name = "Запись о выполнении процедуры"
        verbose_name_plural = "Реестр выполненных процедур"

    def __str__(self):
        return f"{self.procedure} {self.client}"


@receiver(post_save, sender=AppointmentModel)
def create_pre_appointment(sender, instance, created, **kwargs):
    if instance.done:
        instance.last_visit
        AppointmentDoneModel.objects.get_or_create(
            pre_entry=instance,
            defaults={
                'performer': instance.performer,
                'client': instance.client,
                'procedure': instance.procedure,
                'last_visit': instance.last_visit}
        )
