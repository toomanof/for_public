from django.urls import re_path, path
from rest_framework.routers import DefaultRouter

from .views import (
    AppointmentViewSet, AppointmentsListView, RemindClientsToAppointmentsViewList,
    BaseTasksViewList, UnmarkedClientsViewList, AppointmentsCountListView)

app_name = 'api_appointment'
router = DefaultRouter(trailing_slash=False)

urlpatterns = [
    path('tasks',
         BaseTasksViewList.as_view(),
         name='base_list_tasks'),
    path('tasks/registration',
         RemindClientsToAppointmentsViewList.as_view(),
         name='remind_clients_to_appointments'),
    path('tasks/unmarked-clients',
         UnmarkedClientsViewList.as_view(),
         name='remind_unmarked_clients'),

    re_path(r'receptions/date/(?P<year>\d{4})-(?P<month>\d{2})-(?P<day>\d{2})',
            AppointmentsListView.as_view(),
            name='receptions_list'),
    re_path(r'receptions/count/from/(?P<from_year>\d{4})-(?P<from_month>\d{2})-(?P<from_day>\d{2})/to/(?P<to_year>\d{4})-(?P<to_month>\d{2})-(?P<to_day>\d{2})',
            AppointmentsCountListView.as_view(),
            name='count_receptions_list'),
]



urlpatterns += router.urls
