from rest_framework.viewsets import ModelViewSet
from rest_framework import permissions
from django.db.models import Q

from core.services import wrapper_response
from ..models import TemplateModel
from ..serializers import TemplateSerializer


class TemplateViewSet(ModelViewSet):
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = TemplateSerializer

    def get_queryset(self):
        return TemplateModel.objects.filter(Q(creator=self.request.current_user)|
                                            Q(public=True))

    @wrapper_response
    def create(self, request, *args, **kwargs):
        request.data['creator'] = self.request.current_user.id
        return super().create(request, *args, **kwargs)

    @wrapper_response
    def update(self, request, *args, **kwargs):
        request.data['creator'] = self.request.current_user.id
        return super().update(request, *args, **kwargs)
