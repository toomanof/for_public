from rest_framework import permissions
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.generics import ListAPIView
from rest_framework.viewsets import ModelViewSet
from icecream import ic

from django.db.models import Q

from core.services import wrapper_response
from ..models import NewsLetterModel, MixinTypeNewsLetter, QueueModel
from ..services import NewsLetterService
from ..serializers import (
    NewsLetterAllSerializer,
    NewsLetterWithReceptionSerializer, NewsLetterWithProcedureSerializer,
    NewsLetterWithFeedBackSerializer, NewsLetterWithBirthdaySerializer,
    NewsLetterWithOnceSerializer, NewsLetterWithRegularSerializer,
    QueueSerializer
)


class AbstractNewsLettersViewSet(ModelViewSet):
    permission_classes = [permissions.IsAuthenticated]
    newsletter_type = MixinTypeNewsLetter.NT_RECEPTION

    def get_queryset(self):
        return NewsLetterModel.objects.filter(
            creator=self.request.current_user,
            newsletter_type=self.newsletter_type)

    def add_params(self, request):
        request.data['creator'] = request.current_user.id
        request.data['newsletter_type'] = self.newsletter_type
        return request

    def create(self, request, *args, **kwargs):
        response = super().create(self.add_params(request), *args, **kwargs)
        instance = self.get_queryset().get(pk=response.data['id'])
        NewsLetterService(instance).create_queues_newsletter()
        return response

    def update(self, request, *args, **kwargs):
        response = super().update(self.add_params(request), *args, **kwargs)
        instance = self.get_object()
        NewsLetterService(instance).update_queues_newsletter()
        return response

    @action(detail=True)
    def statistics(self, request, pk=None):
        """
        Статистика по рассылке
        """
        instance = self.get_object()
        ic(instance.queues)
        recipients = []
        count_sms = 0
        for item in instance.queues.all():
            recipients.append({
                'fio': str(item.recipient),
                'phone_number': str(item.phone),
                'body': item.text,
                'count_sms': item.num_segments
            })
            count_sms += item.num_segments

        return Response({
            'recipients': recipients,
            'count_sms': count_sms

        })


class NewsLettersReceptionViewSet(AbstractNewsLettersViewSet):
    newsletter_type = MixinTypeNewsLetter.NT_RECEPTION
    serializer_class = NewsLetterWithReceptionSerializer


class NewsLettersProcedureViewSet(AbstractNewsLettersViewSet):
    newsletter_type = MixinTypeNewsLetter.NT_PROCEDURE
    serializer_class = NewsLetterWithProcedureSerializer


class NewsLettersFeedbackViewSet(AbstractNewsLettersViewSet):
    newsletter_type = MixinTypeNewsLetter.NT_FEEDBACK
    serializer_class = NewsLetterWithFeedBackSerializer


class NewsLettersBirthdayViewSet(AbstractNewsLettersViewSet):
    newsletter_type = MixinTypeNewsLetter.NT_BIRTHDAY
    serializer_class = NewsLetterWithBirthdaySerializer


class NewsLettersOnceViewSet(AbstractNewsLettersViewSet):
    newsletter_type = MixinTypeNewsLetter.NT_ONCE
    serializer_class = NewsLetterWithOnceSerializer


class NewsLettersRegularViewSet(AbstractNewsLettersViewSet):
    newsletter_type = MixinTypeNewsLetter.NT_REGULAR
    serializer_class = NewsLetterWithRegularSerializer


class NewsLettersAllView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    @wrapper_response
    def get(self, request, *args, **kwargs):
        return NewsLetterAllSerializer(
            NewsLetterModel.objects.filter(creator=request.current_user),
            many=True).data


class NewsLettersStatisticsView(ListAPIView):
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = QueueSerializer

    def get_queryset(self):
        instances = QueueModel.objects.filter(sender=self.request.current_user,
                                              is_send=True,
                                              status=QueueModel.ST_SUCCESS)
        search_param = self.request.GET.get('search', None)
        if search_param:
            ic(search_param)
            instances = QueueModel.objects.filter(
                Q(recipient__first_name__icontains=search_param) |
                Q(recipient__last_name__icontains=search_param))
        return instances
