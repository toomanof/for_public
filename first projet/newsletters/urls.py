from django.urls import path, re_path
from rest_framework.routers import DefaultRouter

from .views import (
    TemplateViewSet, NewsLettersReceptionViewSet, NewsLettersProcedureViewSet,
    NewsLettersFeedbackViewSet, NewsLettersBirthdayViewSet,
    NewsLettersOnceViewSet, NewsLettersRegularViewSet, NewsLettersAllView,
    NewsLettersStatisticsView
)

app_name = 'api_newsletters'
router = DefaultRouter(trailing_slash=False)

router.register(r'templates', TemplateViewSet, basename='templates')

router.register('newsletters/receptions', NewsLettersReceptionViewSet,
                basename='newsletters_reception')
router.register(r'newsletters/procedures', NewsLettersProcedureViewSet,
                basename='newsletters_procedures')
router.register(r'newsletters/birthdays', NewsLettersBirthdayViewSet,
                basename='newsletters_birthdays')
router.register(r'newsletters/feedbacks', NewsLettersFeedbackViewSet,
                basename='newsletters_feedbacks')
router.register(r'newsletters/once', NewsLettersOnceViewSet,
                basename='newsletters_once')
router.register(r'newsletters/regular', NewsLettersRegularViewSet,
                basename='newsletters_regular')
urlpatterns = [
    path('newsletters', NewsLettersAllView.as_view(),
         name='newsletters_all'),
    path('newsletters/statistics', NewsLettersStatisticsView.as_view(),
         name='newsletters_all'),

]

urlpatterns += router.urls
