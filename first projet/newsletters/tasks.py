from celery import Celery
from django.utils import timezone

from appointment.services import PushSender, TM_NEGATIVE_BALANCE
from billing.services import SMSAccountingService
from core.services.sms import SMSService
from .models import (
    QueueModel, MixinTypeNewsLetter, NewsLetterBirthdayModel, NewsLetterRemindProcedureModel,
    NewsLetterRemindReceptionModel, NewsLetterFeedBackModel, NewsLetterGenderHolidayModel)
from .services import NewsLetterService

app = Celery()


def send_sms_in_queues(queues):
    for queue in queues:
        sms_service = SMSService()
        sms_account_service = SMSAccountingService(queue.sender)

        cost_sms, count_sms = sms_service.cost_sms(queue.text, str(queue.recipient.phone))

        if sms_account_service.balance - cost_sms <= 0:
            queue.status = QueueModel.ST_ERROR
            queue.msg_error = f'Баланс ({sms_account_service.balance}) на вашем счету не позволяет отправку смс!'
            queue.save()
            for device in queue.sender.devices.all():
                PushSender().send(recipient_token=device.token,
                                  title="Предупреждение",
                                  body=queue.msg_error,
                                  data={"type_msg": str(TM_NEGATIVE_BALANCE)},
                                  type_os=device.os)
            continue

        try:
            response = SMSService().send_sms(queue.text, str(queue.recipient.phone))
        except Exception as e:
            queue.status = QueueModel.ST_ERROR
            queue.msg_error = str(e)
        else:
            queue.is_send = True
            queue.status = QueueModel.ST_SUCCESS
            queue.cost_sms = cost_sms
            queue.num_segments = count_sms
            sms_account_service.subtract(cost_sms)
        finally:
            queue.phone = queue.recipient.phone
            queue.save()


@app.task
def send_sms_newsletters_not_regular():
    send_sms_in_queues(QueueModel.objects.filter(
        is_send=False,
        status__isnull=True,
        date_send__lte=timezone.now())[:10]
    )


@app.task
def send_sms_birthdays():
    send_sms_in_queues(QueueModel.objects.filter(
        is_send=False,
        status__isnull=True,
        date_send__lte=timezone.now(),
        newsletter__newsletter_type=MixinTypeNewsLetter.NT_BIRTHDAY)
    )


@app.task
def create_birthday_queues_beginning_of_day():
    for newsletter in NewsLetterBirthdayModel.objects.all():
        NewsLetterService(newsletter.parent).create_queues_newsletter()


@app.task
def create_holiday_queues_gender_holiday():
    for newsletter in NewsLetterGenderHolidayModel.objects.all():
        NewsLetterService(newsletter.parent).create_queues_newsletter()


@app.task
def create_queues_default_newsletters():
    for newsletter in NewsLetterRemindReceptionModel.objects.filter(parent__default=True):
        NewsLetterService(newsletter.parent).create_queues_newsletter()
    for newsletter in NewsLetterRemindProcedureModel.objects.filter(parent__default=True):
        NewsLetterService(newsletter.parent).create_queues_newsletter()
    for newsletter in NewsLetterFeedBackModel.objects.filter(parent__default=True):
        NewsLetterService(newsletter.parent).create_queues_newsletter()
    for newsletter in NewsLetterBirthdayModel.objects.filter(parent__default=True):
        NewsLetterService(newsletter.parent).create_queues_newsletter()
    for newsletter in NewsLetterGenderHolidayModel.objects.filter(parent__default=True):
        NewsLetterService(newsletter.parent).create_queues_newsletter()
