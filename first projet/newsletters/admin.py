from django.contrib import admin
from .models import *


@admin.register(NewsLetterRemindReceptionModel)
class NewsLetterRemindReceptionInLine(admin.ModelAdmin):
    model = NewsLetterRemindReceptionModel
    list_display = ('parent', 'remind', )
    fields = ('procedures',)


@admin.register(NewsLetterRemindProcedureModel)
class NewsLetterRemindProcedureModelInLine(admin.ModelAdmin):
    model = NewsLetterRemindProcedureModel
    list_display = ('parent', 'remind',)
    fields = ('procedures', )


@admin.register(NewsLetterFeedBackModel)
class NewsLetterFeedBackModelInLine(admin.ModelAdmin):
    model = NewsLetterFeedBackModel
    list_display = ('parent', 'when_send',)
    fields = ('when_send', 'procedures',)


@admin.register(NewsLetterBirthdayModel)
class NewsLetterBirthdayModelInLine(admin.ModelAdmin):
    model = NewsLetterBirthdayModel
    list_display = ('parent',)
    fields = ('clients',)


@admin.register(NewsLetterModel)
class NewsLetterModelAdmin(admin.ModelAdmin):
    model = NewsLetterModel
    list_display = ('creator', 'caption', 'template', 'text',
                    'has_template', 'data_send', 'time_send', 'repetitions')

    inlines = ()


@admin.register(QueueModel)
class QueueModelAdmin(admin.ModelAdmin):
    model = QueueModel
    list_display = ('sender',  'newsletter', 'text',
                    'phone', 'is_send', 'date_send', 'status', 'msg_error',
                    'num_segments', 'cost_sms', 'date_create', 'date_update')


@admin.register(TemplateModel)
class TemplateModelAdmin(admin.ModelAdmin):
    model = TemplateModel
    list_display = ('creator',  'title', 'default', 'public', 'text')
