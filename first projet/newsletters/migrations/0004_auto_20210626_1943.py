# Generated by Django 3.2.3 on 2021-06-26 16:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('newsletters', '0003_queuemodel'),
    ]

    operations = [
        migrations.AlterField(
            model_name='queuemodel',
            name='date_send',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Дата отправки'),
        ),
        migrations.AlterField(
            model_name='queuemodel',
            name='status',
            field=models.PositiveSmallIntegerField(blank=True, choices=[(0, 'Успешно'), (1, 'ошибка')], null=True, verbose_name='Статус'),
        ),
    ]
