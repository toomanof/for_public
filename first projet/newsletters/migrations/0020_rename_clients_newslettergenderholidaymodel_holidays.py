# Generated by Django 3.2.3 on 2021-07-15 11:54

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('newsletters', '0019_auto_20210715_1454'),
    ]

    operations = [
        migrations.RenameField(
            model_name='newslettergenderholidaymodel',
            old_name='clients',
            new_name='holidays',
        ),
    ]
