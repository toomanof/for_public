# Generated by Django 3.2.3 on 2021-07-02 06:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('newsletters', '0012_auto_20210701_1228'),
    ]

    operations = [
        migrations.AddField(
            model_name='templatemodel',
            name='default',
            field=models.BooleanField(default=False, verbose_name='Дефолтный шаблон'),
        ),
    ]
