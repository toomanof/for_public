import re
from icecream import ic


_global_variables = {'name': 'self.instance.sender.first_name'}


class TemplateNewsletter:
    template = None
    instance = None

    def __get__(self, instance, owner):
        ic(instance, instance.__dict__)
        return self.execute()

    def __set__(self, instance, value):
        self.instance = instance
        self.template = value
        instance.__dict__[self.name] = value

    def __set_name__(self, owner, name):
        self.name = name

    @property
    def variables(self):
        regex = r"{{(\w*)}}"
        result = re.findall(regex, self.template)
        return result

    @property
    def dict_variables(self):
        result = {}
        for key, val in _global_variables.items():
            ic(eval(val), self.instance.sender)
            result[key] = eval(val)
        return result

    def execute(self):
        _template = self.template
        for key, value in self.dict_variables.items():
            _template = re.sub('{{%s}}' % key, value, _template)
        return _template
