from icecream import ic

from newsletters.models import (
    MixinTypeNewsLetter, NewsLetterModel, TemplateModel, NewsLetterRemindReceptionModel, NewsLetterRemindProcedureModel,
    NewsLetterFeedBackModel, NewsLetterBirthdayModel, NewsLetterGenderHolidayModel)
from . import queues


class NewsLetterService:
    """
    Сервис отвечающий за создание очереди рассылок и отправление их
    """

    def __init__(self, newsletter):
        self.newsletter = newsletter
        self._creator_queue = queues.CreatorQueue(newsletter)
        self._creator_queue.register_format(MixinTypeNewsLetter.NT_RECEPTION,
                                            queues.QueueNewsLettersRemindReception)
        self._creator_queue.register_format(MixinTypeNewsLetter.NT_PROCEDURE,
                                            queues.QueueNewsLettersRemindProcedure)
        self._creator_queue.register_format(MixinTypeNewsLetter.NT_FEEDBACK,
                                            queues.QueueNewsLettersFeedBack)
        self._creator_queue.register_format(MixinTypeNewsLetter.NT_BIRTHDAY,
                                            queues.QueueNewsLettersBirthday)
        self._creator_queue.register_format(MixinTypeNewsLetter.NT_ONCE,
                                            queues.QueueNewsLettersOnce)
        self._creator_queue.register_format(MixinTypeNewsLetter.NT_REGULAR,
                                            queues.QueueNewsLettersRegular)
        self._creator_queue.register_format(MixinTypeNewsLetter.NT_HOLIDAY_GENDER,
                                            queues.QueueNewsLettersGenderHoliday)

    def create_queues_newsletter(self):
        return self._creator_queue.create()

    def update_queues_newsletter(self):
        return self._creator_queue.update()


class NewsLettersService:
    @staticmethod
    def create_default_newsletters(creator, recipient, default_newsletters_cls):
        """
        Создает отмеченные дефолтные рассылки для recipient
        """
        for default_newsletter_cls in default_newsletters_cls:
            default_newsletter_cls(creator, recipient).create()

    @staticmethod
    def update_default_newsletters(creator, recipient, newsletters_cls):
        """
        Создает отмеченные дефолтные рассылки для recipient
        """
        NewsLetterModel.objects.filter(
            creator=creator,
            default=True,
        ).delete()
        ic(creator, recipient, newsletters_cls)
        NewsLettersService.create_default_newsletters(creator, recipient, newsletters_cls)


class DefaultNewsletter:
    def __init__(self, creator, recipient):
        self.creator = creator
        self.recipient = recipient
        ic(creator, recipient)

    def get_caption(self):
        pass

    def create(self):
        ic()
        parent, _ = NewsLetterModel.objects.update_or_create(
            creator=self.creator,
            newsletter_type=self.type_newsletter,
            default=True,
            defaults={'caption': f'Дефолтная {self.get_caption()}',
                      'template': self.get_template(),
                      'has_template': True
                      }
        )
        self.create_related_instance(parent)

    def get_template(self):
        template = TemplateModel.objects.filter(
            default=True,
            newsletter_type=self.type_newsletter
        ).first()
        assert template, f'Template for class "{self.cls_related_model}" not set'
        return template


class DefaultNewsletterRemindReception(DefaultNewsletter):
    type_newsletter = MixinTypeNewsLetter.NT_RECEPTION
    cls_related_model = NewsLetterRemindReceptionModel

    def get_caption(self):
        return 'рассылка о записях'

    def create_related_instance(self, parent):
        result, _ = self.cls_related_model.objects.update_or_create(
            parent=parent,
            defaults={'remind': self.cls_related_model.BEFORE_FIVE_DAYS})
        result.procedures.set([])


class DefaultNewsletterRemindProcedure(DefaultNewsletter):
    type_newsletter = MixinTypeNewsLetter.NT_PROCEDURE
    cls_related_model = NewsLetterRemindProcedureModel

    def get_caption(self):
        return 'рассылка о процедурах'

    def create_related_instance(self, parent):
        result, _ = self.cls_related_model.objects.update_or_create(
            parent=parent,
            defaults={'remind': self.cls_related_model.BEFORE_TWENTY_FOUR_HOURS})
        result.procedures.set([])


class DefaultNewsletterFeedBack(DefaultNewsletter):
    type_newsletter = MixinTypeNewsLetter.NT_FEEDBACK
    cls_related_model = NewsLetterFeedBackModel

    def get_caption(self):
        return 'рассылка обратной связи'

    def create_related_instance(self, parent):
        result, _ = self.cls_related_model.objects.update_or_create(
            parent=parent,
            defaults={'when_send': self.cls_related_model.AFTER_SEVEN_DAYS})
        result.procedures.set([])


class DefaultNewsletterBirthday(DefaultNewsletter):
    type_newsletter = MixinTypeNewsLetter.NT_BIRTHDAY
    cls_related_model = NewsLetterBirthdayModel

    def get_caption(self):
        return 'рассылка поздравлений с днём рождения'

    def create_related_instance(self, parent):
        result, _ = self.cls_related_model.objects.update_or_create(parent=parent)
        result.clients.set([self.recipient])


class DefaultNewsletterGenderHoliday(DefaultNewsletter):
    type_newsletter = MixinTypeNewsLetter.NT_HOLIDAY_GENDER
    cls_related_model = NewsLetterGenderHolidayModel

    def get_caption(self):
        return 'рассылка поздравлений с муж/жен праздниками'

    def create_related_instance(self, parent):
        result, _ = self.cls_related_model.objects.update_or_create(parent=parent)
        holidays_today = NewsLetterGenderHolidayModel.objects.holidays_today()
        result.holidays.set(holidays_today)
