import re
import datetime

from functools import cached_property
from icecream import ic

from django.utils import timezone, dateformat

from core.constants import get_begin_end_day_with
from appointment.models import AppointmentModel
from ..models import QueueModel, NewsLetterModel
from .templates import TemplateNewsletter


ic.configureOutput(includeContext=True)


class CreatorQueue:
    def __init__(self, newsletter):
        self._creators = {}
        self.newsletter = newsletter

    def register_format(self, type_queue, cls):
        self._creators[type_queue] = cls

    def _create_queues(self):
        creator = self._creators.get(self.object.newsletter_type)
        if not creator:
            raise ValueError(self.object.newsletter_type)
        return creator(self.object).create_queues()

    def create(self):
        return self._create_queues()

    def update(self):
        self.object.queues.exclude(status=QueueModel.ST_SUCCESS).delete()
        return self._create_queues()

    @cached_property
    def object(self):
        return self.newsletter if isinstance(self.newsletter, NewsLetterModel) else self.newsletter.parent


class AbstractQueueNewsLetters:
    newsletter = None
    sender = None

    def __init__(self, newsletter):
        self.newsletter = newsletter
        self.sender = newsletter.creator
        self.template = self.newsletter.template.text if self.newsletter.has_template else self.newsletter.text

    def create_queue(self, **defaults_kwargs):
        kwargs = {'sender': self.sender,
                  'newsletter': self.newsletter,
                  'recipient': defaults_kwargs.pop('recipient'),
                  'is_send': False}
        ic(kwargs)
        QueueModel.objects.update_or_create(**kwargs, defaults=defaults_kwargs)

    def get_related_name(self):
        assert self.related_name, 'Is not set related_name!'
        return self.related_name

    @cached_property
    def related_filed(self):
        return getattr(self.newsletter, self.get_related_name())

    def render_template(self, template=None, **variables):
        _template = self.template if not template else template
        ic(_template)
        for key, value in variables.items():
            _template = re.sub('{{%s}}' % key, value, _template)
        return _template

    def format_date_send(self, date_send):
        return f'{dateformat.format(date_send, "d.m.Y в H:i")}'


class MixinQueueNewsLettersWithProcedures:
    newsletter = None

    @cached_property
    def all_sender_id_procedures(self):
        return self.sender.procedures.all().values('id')

    @property
    def id_procedures(self):
        if not self.related_filed.procedures.exists():
            return self.all_sender_id_procedures
        else:
            return self.related_filed.procedures.all().values('id')

    @cached_property
    def appointments(self):
        return AppointmentModel.objects.filter(procedure__in=self.id_procedures,
                                               done=False,
                                               reception_date__gte=timezone.now())

    def create_queues(self):
        for appointment in self.appointments:
            try:
                self.create_queue(
                    recipient=appointment.client,
                    phone=appointment.client.phone,
                    text=self.render_template(name=str(appointment.client),
                                              procedure=str(appointment.procedure),
                                              date=self.format_date_send(appointment.reception_date)
                                              ),
                    date_send=self.get_date_send(appointment.reception_date)
                )
            except Exception as e:
                ic(e)


class MixinQueueNewsLettersWithClients:

    @property
    def recipients(self):
        return self.related_filed.clients.all()

    def create_queues(self):
        ic(self.recipients)
        for recipient in self.recipients:
            ic(recipient)
            try:
                self.create_queue(
                    recipient=recipient,
                    phone=recipient.phone,
                    text=self.render_template(name=str(recipient)),
                    date_send=self.get_date_send(recipient.birthday)
                )
            except Exception as e:
                ic(e)


class QueueNewsLettersRemindReception(MixinQueueNewsLettersWithProcedures,
                                      AbstractQueueNewsLetters):
    related_name = 'remind_reception'

    def get_date_send(self, reception_date):
        return reception_date - datetime.timedelta(days=self.related_filed.remind)


class QueueNewsLettersRemindProcedure(MixinQueueNewsLettersWithProcedures,
                                      AbstractQueueNewsLetters):
    related_name = 'remind_procedure'

    def get_date_send(self, reception_date):
        return reception_date - datetime.timedelta(hours=self.related_filed.remind)


class QueueNewsLettersFeedBack(MixinQueueNewsLettersWithProcedures,
                               AbstractQueueNewsLetters):
    related_name = 'feedback'

    def get_date_send(self, reception_date):
        return reception_date + datetime.timedelta(days=self.related_filed.when_send)


class QueueNewsLettersBirthday(MixinQueueNewsLettersWithClients,
                               AbstractQueueNewsLetters):
    related_name = 'birthday'

    @property
    def recipients(self):
        ic()
        result = self.related_filed.holidays\
            if self.related_filed.holidays.exists() else self.newsletter.creator.clients.all()
        return result.filter(
            birthday__range=get_begin_end_day_with(
                year=timezone.now().year,
                month=timezone.now().month,
                day=timezone.now().day))

    def get_date_send(self, reception_date):
        ic()
        return datetime.datetime(year=timezone.now().year,
                                 month=reception_date.month,
                                 day=reception_date.day,
                                 hour=9,
                                 minute=0)


class QueueNewsLettersOnce(MixinQueueNewsLettersWithProcedures,
                           AbstractQueueNewsLetters):
    related_name = 'once'

    @property
    def id_clients(self):
        if not self.related_filed.clients.exists():
            return self.newsletter.creator.clients.all().values('id')
        else:
            return self.related_filed.clients.all().values('id')

    def get_date_send(self, reception_date):
        return self.related_filed.date_send

    @cached_property
    def appointments(self):
        result = AppointmentModel.objects.filter(procedure__in=self.id_procedures,
                                                 client__in=self.id_clients,
                                                 done=False)
        if self.related_filed.has_reception:
            result.filter(reception_date__isnull=False)

        return result


class QueueNewsLettersRegular(MixinQueueNewsLettersWithProcedures,
                              AbstractQueueNewsLetters):
    related_name = 'regular'

    def get_date_send(self, reception_date):
        return timezone.now()

    @property
    def id_clients(self):
        if not self.related_filed.clients.exists():
            return self.newsletter.creator.clients.all().values('id')
        else:
            return self.related_filed.clients.all().values('id')

    def get_date_send(self, reception_date):
        return self.related_filed.date_send

    @cached_property
    def appointments(self):
        result = AppointmentModel.objects.filter(procedure__in=self.id_procedures,
                                                 client__in=self.id_clients,
                                                 done=False,
                                                 reception_date__gte=timezone.now())
        if self.related_filed.has_reception:
            result.filter(reception_date__isnull=False)

        return result


class QueueNewsLettersGenderHoliday(MixinQueueNewsLettersWithClients,
                                    AbstractQueueNewsLetters):
    related_name = 'gender_holiday'

    @cached_property
    def holidays_today(self):
        self.related_filed.holidays.holidays_today()

    @cached_property
    def all_clients(self):
        self.newsletter.creator.clients.all()

    def man_holidays_today(self):
        from core.models import HolidayModel
        self.holidays_today.filter(gender=HolidayModel.MAN)

    def woman_holidays_today(self):
        from core.models import HolidayModel
        self.holidays_today.filter(gender=HolidayModel.WOMAN)

    def man_recipients(self):
        from core.models import ClientModel
        self.all_clients.filter(gender=ClientModel.MAN)

    def man_recipients(self):
        from core.models import ClientModel
        self.all_clients.filter(gender=ClientModel.MAN)

    def get_date_send(self, reception_date):
        ic()
        return datetime.datetime(year=timezone.now().year,
                                 month=reception_date.month,
                                 day=reception_date.day,
                                 hour=9,
                                 minute=0)

    def create_queues_holidays(self, holidays, recipients):
        for holiday in holidays:
            for recipient in recipients:
                ic(recipient)
                try:
                    self.create_queue(
                        recipient=recipient,
                        phone=recipient.phone,
                        text=self.render_template(
                            name=str(recipient), template=holiday.template),
                        date_send=self.get_date_send(recipient.birthday)
                    )
                except Exception as e:
                    ic(e)

    def create_queues(self):
        self.create_queues_holidays(self.man_holidays_today(),
                                    self.man_recipients())
