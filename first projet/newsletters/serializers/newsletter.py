from icecream import ic
from rest_framework import serializers
from django.shortcuts import get_object_or_404

from core.serializers import ProcedureSerializer
from core.models import ProcedureModel

from ..models import (NewsLetterModel, NewsLetterRemindReceptionModel,
                      NewsLetterRemindProcedureModel, NewsLetterFeedBackModel,
                      NewsLetterBirthdayModel, NewsLetterOnceModel,
                      NewsLetterRegularModel, NewsLetterGenderHolidayModel)


class NewsLetterAllSerializer(serializers.ModelSerializer):

    class Meta:
        model = NewsLetterModel
        fields = ('id', 'caption', 'creator', 'newsletter_type')


class NewsLetterRemindReceptionSerializer(serializers.ModelSerializer):
    procedures = serializers.PrimaryKeyRelatedField(many=True, queryset=ProcedureModel.objects.all())

    class Meta:
        model = NewsLetterRemindReceptionModel
        fields = ('remind', 'procedures', )


class NewsLetterRemindProcedureSerializer(serializers.ModelSerializer):

    class Meta:
        model = NewsLetterRemindProcedureModel
        fields = ('remind', 'procedures', )


class NewsLetterFeedBackSerializer(serializers.ModelSerializer):

    class Meta:
        model = NewsLetterFeedBackModel
        fields = ('when_send', 'procedures', )


class NewsLetterBirthdaySerializer(serializers.ModelSerializer):

    class Meta:
        model = NewsLetterBirthdayModel
        fields = '__all__'


class NewsLetterGenderHolidaySerializer(serializers.ModelSerializer):

    class Meta:
        model = NewsLetterGenderHolidayModel
        fields = '__all__'

class NewsLetterOnceSerializer(serializers.ModelSerializer):

    class Meta:
        model = NewsLetterOnceModel
        fields = '__all__'


class NewsLetterRegularSerializer(serializers.ModelSerializer):

    class Meta:
        model = NewsLetterRegularModel
        fields = '__all__'


class NewsLetterSerializer(serializers.ModelSerializer):
    many_to_many_field_name_in_related_data = ()

    class Meta:
        model = NewsLetterModel
        fields = '__all__'

    def create(self, validated_data):
        if hasattr(self, 'related_model_class'):
            related_data = validated_data.pop(self.related_field_name, None)
            news_letter = NewsLetterModel.objects.create(**validated_data)
            if related_data:
                related_data['parent'] = news_letter
                ic(related_data)
                related_data_result = self.pull_many_to_many_field_name_in_related_data(related_data)
                ic(related_data)
                related_obj = self.related_model_class.objects.create(**related_data)
                self.set_many_to_many_field_name_in_related_data(related_obj, related_data_result)
            return news_letter

        return super().create(validated_data)

    def is_valid(self, raise_exception=False):
        if hasattr(self, 'related_model_class') and \
                self.related_field_name in self.initial_data and\
                'parent' in self.initial_data[self.related_field_name]:
            self.initial_data[self.related_field_name].pop('parent')
        super(NewsLetterSerializer, self).is_valid(raise_exception)

    def update(self, instance, validated_data):
        ic()
        if hasattr(self, 'related_model_class'):
            related_data = validated_data.pop(self.related_field_name, None)

            if related_data:
                related_data['parent'] = instance
                related_data_result = self.pull_many_to_many_field_name_in_related_data(related_data)
                ic(related_data, related_data_result, dir(instance))

                self.related_model_class.objects\
                    .filter(id=getattr(instance, self.related_field_name).id)\
                    .update(**related_data)
                related_obj = self.related_model_class.objects.get(
                    id=getattr(instance, self.related_field_name).id)

                self.set_many_to_many_field_name_in_related_data(related_obj,
                                                                 related_data_result)

            return super().update(instance, validated_data)

        return super().create(validated_data)

    def pull_many_to_many_field_name_in_related_data(self, related_data):
        """
        Выбирает из related_data значения полей с именнами указанными
        в self.many_to_many_field_name_in_related_data
        """
        result = {}
        if hasattr(self, 'many_to_many_field_name_in_related_data'):
            for attr_name in self.many_to_many_field_name_in_related_data:
                ic(attr_name)
                result[attr_name] = related_data.pop(attr_name)
        return result

    def set_many_to_many_field_name_in_related_data(self, related_obj,
                                                    related_data_result):
        """
        Устанавливает значения переданные в словаре related_data_result
        в связанную модель related_obj в поле с именем
        соотвесвующим ключам related_data_result
        """
        for attr_name, value in related_data_result.items():
            if hasattr(self, 'many_to_many_field_name_in_related_data')\
                    and hasattr(related_obj, attr_name):
                getattr(related_obj, attr_name).set(value)


class NewsLetterWithReceptionSerializer(NewsLetterSerializer):
    related_model_class = NewsLetterRemindReceptionModel
    related_field_name = 'remind_reception'
    many_to_many_field_name_in_related_data = ('procedures', )
    remind_reception = NewsLetterRemindReceptionSerializer()


class NewsLetterWithProcedureSerializer(NewsLetterSerializer):
    related_model_class = NewsLetterRemindProcedureModel
    related_field_name = 'remind_procedure'
    many_to_many_field_name_in_related_data = ('procedures',)
    remind_procedure = NewsLetterRemindProcedureSerializer()


class NewsLetterWithFeedBackSerializer(NewsLetterSerializer):
    related_model_class = NewsLetterFeedBackModel
    related_field_name = 'feedback'
    many_to_many_field_name_in_related_data = ('procedures',)
    feedback = NewsLetterFeedBackSerializer()


class NewsLetterWithBirthdaySerializer(NewsLetterSerializer):
    related_model_class = NewsLetterBirthdayModel
    related_field_name = 'birthday'
    many_to_many_field_name_in_related_data = ('clients',)
    birthday = NewsLetterBirthdaySerializer()


class NewsLetterWithGenderHolidaySerializer(NewsLetterSerializer):
    related_model_class = NewsLetterGenderHolidayModel
    related_field_name = 'gender_holiday'
    many_to_many_field_name_in_related_data = ('holidays',)
    gender_holiday = NewsLetterGenderHolidaySerializer()


class NewsLetterWithOnceSerializer(NewsLetterSerializer):
    related_model_class = NewsLetterOnceModel
    related_field_name = 'once'
    many_to_many_field_name_in_related_data = ('clients', 'procedures',)
    once = NewsLetterOnceSerializer()


class NewsLetterWithRegularSerializer(NewsLetterSerializer):
    related_model_class = NewsLetterRegularModel
    related_field_name = 'regular'
    many_to_many_field_name_in_related_data = ('clients', 'procedures',)
    regular = NewsLetterRegularSerializer()
