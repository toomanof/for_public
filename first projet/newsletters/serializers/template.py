from rest_framework import serializers

from ..models import TemplateModel


class TemplateSerializer(serializers.ModelSerializer):

    class Meta:
        model = TemplateModel
        fields = '__all__'
        read_only_fields = ['public']
