from rest_framework import serializers
from ..models import QueueModel


class QueueSerializer(serializers.ModelSerializer):
    first_name = serializers.SerializerMethodField()
    last_name = serializers.SerializerMethodField()
    count_sms = serializers.SerializerMethodField()

    class Meta:
        model = QueueModel
        fields = ('id', 'first_name', 'last_name', 'text',
                  'phone', 'date_send', 'count_sms')

    def get_first_name(self, element):
        return element.recipient.first_name

    def get_last_name(self, element):
        return element.recipient.last_name

    def get_count_sms(self, element):
        return element.num_segments
