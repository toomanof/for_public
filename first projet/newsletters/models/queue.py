from django.contrib.auth.models import User
from django.db import models
from phonenumber_field.modelfields import PhoneNumberField


class QueueModel(models.Model):
    ST_SUCCESS, ST_ERROR = range(2)
    CHOICE_STATUSES = (
        (ST_SUCCESS, 'Успешно'),
        (ST_ERROR, 'ошибка')
    )
    sender = models.ForeignKey(User, verbose_name='Отправитель',
                               related_name='queue_newsletters', on_delete=models.CASCADE)
    recipient = models.ForeignKey('core.ClientModel', verbose_name='Клиент',
                                  related_name='queue_newsletters', on_delete=models.CASCADE)
    newsletter = models.ForeignKey('newsletters.NewsLetterModel', verbose_name='Рассылка',
                                   related_name='queues', on_delete=models.CASCADE)
    text = models.TextField('Текст сообщения')
    phone = PhoneNumberField('Телефон', blank=True, null=True)
    is_send = models.BooleanField('Отправлено', default=False)
    status = models.PositiveSmallIntegerField(
        'Статус', choices=CHOICE_STATUSES, blank=True, null=True)
    msg_error = models.TextField('Ошибка отправки сообщения', blank=True, null=True)
    date_send = models.DateTimeField('Дата отправки', blank=True, null=True)
    date_create = models.DateTimeField('Дата записи', auto_now_add=True)
    date_update = models.DateTimeField('Дата обновление записи', auto_now=True)
    num_segments = models.PositiveSmallIntegerField('Количество сегментов смс', default=0)
    cost_sms = models.FloatField('Стоимость смс', blank=True, null=True)

    class Meta:
        verbose_name = "Очередь рассылки"
        verbose_name_plural = "Очереди рассылок"
        ordering = ('-date_send', '-date_create',)
