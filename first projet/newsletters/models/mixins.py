from django.db import models


class MixinTypeNewsLetter(models.Model):
    """
    Mixin типы рассылок
    """
    (NT_RECEPTION, NT_PROCEDURE, NT_BIRTHDAY,
     NT_FEEDBACK, NT_ONCE, NT_REGULAR, NT_HOLIDAY_GENDER) = range(7)
    NEWSLETTERS_TYPE = (
        (NT_RECEPTION, 'Напоминание о записи'),
        (NT_PROCEDURE, 'Напоминание о процедуре'),
        (NT_BIRTHDAY, 'Поздравление с днём рождения'),
        (NT_FEEDBACK, 'Обратная связь'),
        (NT_ONCE, 'Разово'),
        (NT_REGULAR, 'Регулярно'),
        (NT_HOLIDAY_GENDER, 'Муж/Жен поздравления')
    )

    newsletter_type = models.PositiveSmallIntegerField(
        'Тип рассылки', choices=NEWSLETTERS_TYPE, default=NT_RECEPTION)

    class Meta:
        abstract = True

    @property
    def is_reception(self):
        return self.newsletter_type == self.NT_RECEPTION

    @property
    def is_procedure(self):
        return self.newsletter_type == self.NT_PROCEDURE

    @property
    def is_birthday(self):
        return self.newsletter_type == self.NT_BIRTHDAY

    @property
    def is_feedback(self):
        return self.newsletter_type == self.NT_FEEDBACK

    @property
    def is_once(self):
        return self.newsletter_type == self.NT_ONCE

    @property
    def is_regular(self):
        return self.newsletter_type == self.NT_REGULAR

    @property
    def is_gender(self):
        return self.newsletter_type == self.NT_HOLIDAY_GENDER


class MixinChoiceProcedureModel(models.Model):
    CP_ALL_PROCEDURES, CP_SELECT_PROCEDURES = range(2)
    CHOICE_PROCEDURE = (
        (CP_ALL_PROCEDURES, 'По всем процедурам'),
        (CP_SELECT_PROCEDURES, 'Список процедур, по которым рассылать')
    )

    type_choice_procedure = models.PositiveSmallIntegerField(
        'Выбор процедуры', choices=CP_ALL_PROCEDURES, blank=True, null=True)

    class Meta:
        abstract = True
