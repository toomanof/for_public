from django.db import models
from django.contrib.auth.models import User
from .template import TemplateModel
from .mixins import MixinChoiceProcedureModel, MixinTypeNewsLetter


class NewsLetterModel(MixinTypeNewsLetter, models.Model):
    creator = models.ForeignKey(User, on_delete=models.CASCADE,
                                related_name='news_letters',
                                verbose_name='Создатель')
    caption = models.CharField('Наименование', max_length=255)
    template = models.ForeignKey(
        TemplateModel, verbose_name='Шаблон рассылки',
        on_delete=models.CASCADE, blank=True, null=True)
    text = models.TextField('Текст рассылки', blank=True, null=True)
    has_template = models.BooleanField('Содержит шаблон', default=False)
    data_send = models.DateField('Дата отправки сообщения', blank=True, null=True)
    time_send = models.TimeField('Время отправки сообщения', blank=True, null=True)
    repetitions = models.PositiveSmallIntegerField('Количество повторений',
                                                   blank=True, null=True)
    default = models.BooleanField('Дефолтная', default=False)

    class Meta:
        verbose_name = 'Рассылка'
        verbose_name_plural = 'Рассылки'

    def __str__(self):
        return self.caption


class NewsLetterRemindReceptionModel(MixinChoiceProcedureModel,  models.Model):
    BEFORE_TEN_DAYS, BEFORE_FIVE_DAYS = 10, 5
    CHOICE_REMIND = (
        (BEFORE_TEN_DAYS, 'За 10 дней'),
        (BEFORE_FIVE_DAYS, 'За 5 дней')
    )
    parent = models.OneToOneField(
        NewsLetterModel, on_delete=models.CASCADE,
        related_name='remind_reception',
        blank=True, null=True)
    remind = models.PositiveSmallIntegerField('За сколько напоминать',
                                              choices=CHOICE_REMIND,
                                              blank=True, null=True)
    procedures = models.ManyToManyField(
        'core.ProcedureModel', verbose_name='Список процедур',
        related_name='remind_receptions', blank=True)

    class Meta:
        verbose_name = "Напоминание о записи"
        verbose_name_plural = "Напоминания о записях"

    def __str__(self):
        return self.parent.caption


class NewsLetterRemindProcedureModel(MixinChoiceProcedureModel,  models.Model):
    BEFORE_TWO_HOURS, BEFORE_TWENTY_FOUR_HOURS = 2, 24
    CHOICE_REMIND = (
        (BEFORE_TWO_HOURS, 'За 2 часа'),
        (BEFORE_TWENTY_FOUR_HOURS, 'За 24 часа')
    )
    parent = models.OneToOneField(
        NewsLetterModel, on_delete=models.CASCADE, related_name='remind_procedure',
        blank=True, null=True)
    remind = models.PositiveSmallIntegerField(
        'За сколько напоминать', choices=CHOICE_REMIND, blank=True, null=True)
    procedures = models.ManyToManyField(
        'core.ProcedureModel', verbose_name='Список процедур',
        related_name='remind_procedures', blank=True)

    class Meta:
        verbose_name = "Повторная запись"
        verbose_name_plural = "Повторные записи"

    def __str__(self):
        return self.parent.caption


class NewsLetterFeedBackModel(MixinChoiceProcedureModel,  models.Model):
    AFTER_SEVEN_DAYS, AFTER_TWENTY_FOUR_HOURS = 7, 24
    CHOICE_REMIND = (
        (AFTER_SEVEN_DAYS, 'Через 7 дней после процедуры'),
        (AFTER_TWENTY_FOUR_HOURS, 'Через 24 часа после процедуры')
    )
    parent = models.OneToOneField(
        NewsLetterModel, on_delete=models.CASCADE, related_name='feedback',
        blank=True, null=True)
    when_send = models.PositiveSmallIntegerField(
        'Когда отсылать', choices=CHOICE_REMIND, blank=True, null=True)
    procedures = models.ManyToManyField(
        'core.ProcedureModel', verbose_name='Список процедур',
        related_name='remind_feedback', blank=True)

    class Meta:
        verbose_name = "Обратная связь"
        verbose_name_plural = "Обратная связь"

    def __str__(self):
        return self.parent.caption


class NewsLetterBirthdayModel(models.Model):
    parent = models.OneToOneField(
        NewsLetterModel, on_delete=models.CASCADE, related_name='birthday',
        blank=True, null=True)
    holidays = models.ManyToManyField(
        'core.ClientModel', verbose_name='Список праздников',
        related_name='birthday_news_letters', blank=True)

    class Meta:
        verbose_name = "Поздравление"
        verbose_name_plural = "Поздравления"

    def __str__(self):
        return self.parent.caption


class NewsLetterGenderHolidayModel(models.Model):
    parent = models.OneToOneField(
        NewsLetterModel, on_delete=models.CASCADE, related_name='gender_holiday',
        blank=True, null=True)
    holidays = models.ManyToManyField(
        'core.HolidayModel', verbose_name='Список праздников',
        related_name='news_letters', blank=True)

    class Meta:
        verbose_name = "Поздравление в муж/жен праздники"
        verbose_name_plural = "Поздравления в муж/жен праздники"

    def __str__(self):
        return self.parent.caption


class NewsLetterOnceModel(models.Model):
    parent = models.OneToOneField(
        NewsLetterModel, on_delete=models.CASCADE, related_name='once',
        blank=True, null=True)
    date_send = models.DateTimeField('Дата и время отправления')
    clients = models.ManyToManyField(
        'core.ClientModel', verbose_name='Список клиентов',
        related_name='newsletter_once_clients', blank=True)
    procedures = models.ManyToManyField(
        'core.ProcedureModel', verbose_name='Список процедур',
        related_name='newsletter_once_procedures', blank=True)
    has_reception = models.BooleanField('Есть ли запись на будущую процедуру',
                                        default=False)

    class Meta:
        verbose_name = "Массовая рассылка"
        verbose_name_plural = "Массовые рассылки"

    def __str__(self):
        return self.parent.caption


class NewsLetterRegularModel(models.Model):
    parent = models.OneToOneField(
        NewsLetterModel, on_delete=models.CASCADE, related_name='regular',
        blank=True, null=True)
    period_days_send = models.PositiveIntegerField(
        'Выбор количество дней, через сколько рассылать', default=1)
    clients = models.ManyToManyField(
        'core.ClientModel', verbose_name='Список клиентов',
        related_name='newsletter_regular_clients', blank=True)
    procedures = models.ManyToManyField(
        'core.ProcedureModel', verbose_name='Список процедур',
        related_name='newsletter_regular_procedures', blank=True)
    has_reception = models.BooleanField('Есть ли запись на будущую процедуру',
                                        default=False)

    class Meta:
        verbose_name = "Своя рассылка"
        verbose_name_plural = "Свои рассылки"

    def __str__(self):
        return self.parent.caption
