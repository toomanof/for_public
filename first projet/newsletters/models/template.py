from icecream import ic
from django.contrib.auth.models import User
from django.db import models

from .mixins import MixinTypeNewsLetter


class TemplateModel(MixinTypeNewsLetter, models.Model):

    creator = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='templates',
        verbose_name='Создатель шаблона', blank=True, null=True)
    title = models.CharField('Наменование', max_length=255)
    public = models.BooleanField('Публичный шаблон', default=False)
    text = models.TextField('Текст шаблона')
    default = models.BooleanField('Дефолтный шаблон', default=False)

    def __str__(self):
        return self.title

    def save(self, **kwargs):
        if TemplateModel.objects.filter(newsletter_type=self.newsletter_type,
                                        default=True).exists():
            raise Exception('The default template of the presented type is already there!')
        if self.default:
            self.public = True
        return super().save(**kwargs)
