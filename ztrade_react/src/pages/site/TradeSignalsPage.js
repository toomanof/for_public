import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'
import {
    Breadcrumbs,
    SideWrapper,
    SideRecommend
} from 'components'


@inject('menu')
@observer
export default class TradeSignalsPage extends React.Component{
    render(){
        if (!this.props.menu.ami){
            return null
        }

        return(
            <React.Fragment>
                <div className="container middle">
                    <div className="brick">
                        <SideWrapper />

                        <div className="brick_content">
                            <div className="brick_box">
                                <Breadcrumbs />

                                <h1>{this.props.menu.ami.h1}</h1>

                                <div className="module page">
                                    <div className="page_cover" style={{backgroundImage: 'url(images/img1.jpg)'}}>
                                        <article>
                                            <h2>Стратегия торговли на прорывах волатильности</h2>
                                            <p>Статистически доказано, что периоды высокой и низкой
                                                волатильности устойчиво меняются на всех финансовых рынках.</p>
                                        </article>
                                    </div>
                                    <div className="page_entry">
                                        <p>Статистически доказано, что периоды высокой и низкой волатильности устойчиво меняются на всех финансовых рынках. А раз так, то этот факт открывает перспективы для построения прибыльной торговой системы на циклах волатильности.</p>
                                        <p>В книге «Стратегия торговли на прорывах волатильности» Вы познакомитесь со стратегией, построенной на взаимосвязи между циклами волатильности и треугольниками. Как оказалось, треугольники это одни из самых эффективных фигур технического анализа, графически отображающие переход между циклами низкой и высокой волатильности. Данная стратегия скорее является долгосрочной и позволяет определять зарождение и начало сильных трендовых движений.</p>
                                        <h3>Из книги Вы узнаете:</h3>
                                        <ul>
                                            <li>Что такое циклы волатильности</li>
                                            <li>Связь между волатильностью и треугольниками</li>
                                            <li>Виды треугольников и их особенности</li>
                                            <li>Примеры торговли на прорывах волатильности</li>
                                        </ul>
                                    </div>
                                </div>

                                <div className="module subswrap">
                                    <div className="module_title">Торговые сигналы</div>
                                    <div className="row">
                                        <div className="grid6 sm12">
                                            <div className="subsbox">
                                                <div className="subsbox_title">Валютный рынок</div>
                                                <div className="subsbox_price"><b>2 000 руб.</b> в месяц</div>
                                                <div className="subsbox_test">
                                                    <span>Тестовый период <b>14 дней</b></span>
                                                    Перед первым платежом,<br />
                                                    никаких скрытых условий
                                                </div>
                                                <ul>
                                                    <li>Наше полное предложение <small>Как для годового плана</small></li>
                                                    <li>Опимально для старта <small>Никаких контрактов, всегда можно отписаться</small></li>
                                                </ul>
                                                <Link
                                                    to="/site/signup"
                                                    className="btn btn-orange btn-big">

                                                    {'Оформить подписку'}
                                                </Link>
                                            </div>
                                        </div>
                                        <div className="grid6 sm12">
                                            <div className="subsbox">
                                                <div className="subsbox_title">Фондовые рынки</div>
                                                <div className="subsbox_price"><b>3 000 руб.</b> в месяц</div>
                                                <div className="subsbox_test">
                                                    <span>Тестовый период <b>14 дней</b></span>
                                                    Перед первым платежом,<br />
                                                    никаких скрытых условий
                                                </div>
                                                <ul>
                                                    <li>Три месяца по цене двух!</li>
                                                    <li>Наше полное предложение <small>Как для годового плана</small></li>
                                                    <li>Оплата 1 раз в 3 месяца</li>
                                                </ul>
                                                <Link
                                                    to="/site/signup"
                                                    className="btn btn-orange btn-big">

                                                    {'Оформить подписку'}
                                                </Link>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <SideRecommend />

                </div>
            </React.Fragment>
        )
    }
}


TradeSignalsPage.propTypes = {
    menu: MPropTypes.observableObject
}