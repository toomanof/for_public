import React, { Component } from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'

@inject('user')
@observer
export default class NotFoundPage extends Component {
    render() {
        return <div>{'Page not found'}</div>
    }
}

NotFoundPage.propTypes = {
    user: MPropTypes.observableObject
}
