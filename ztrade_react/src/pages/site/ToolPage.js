import React, { Fragment, useEffect } from 'react'
import { Link } from 'react-router-dom'

import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import SwiperCore, { Navigation, Pagination, Scrollbar, A11y } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import { rHTML } from 'services/helpers'
import { Loading, SideWrapper } from 'components'


SwiperCore.use([Navigation, Pagination, Scrollbar, A11y]);

@inject('menu')
@observer
export default class ToolPage extends React.Component {
    componentDidMount() {
        const { menu } = this.props
        if (!menu.pageTool) {
            menu.loadPageTool()
        }
        menu.loadToolsFirstSlider()
        menu.loadToolsSecondSlider()
    }

    render() {
        const { menu } = this.props
        if (!this.props.menu.ami){
            return null
        }

        const divLatptopSliderGreen = {
            backgroundImage: 'green'
        }
        const divLatptopSliderBlue = {
            backgroundImage: 'blue'
        }
        if (!menu.pageTool) {
            return <Loading />
        }


        return (
            <React.Fragment>
                <SideWrapper onlyMobile={true} />
                <div className="toolspage">

                    <div className="toolspage_first">
                        <div className="container">
                            <h2>Инструменты для анализа и торговли, индикатор опционных уровней для валютного рынка
                                форекс</h2>
                            <div className="txt">
                                <img src="images/tools/phone.png" alt="" srcSet="images/tools/phone-2x.png 2x"
                                     className="iphonex"/>
                                    <p>Индикатор опционных уровней, отображает отчетные данные с СМЕ Group Чикагской
                                        товарной биржи, и выводит их на график в виде уровней, тем самым упрощает задачу
                                        анализа рынка форекс, а также поиску уверенных точек входа с хорошим потенциалом
                                        риск к прибыли</p>
                            </div>
                            <Link to="/account/tools" className="btn btn-green">Начать пользоваться</Link>
                        </div>
                    </div>

                    <div className="toolspage_second">
                        <div className="container">
                            <div className="entry">
                                <h2><b>Инструменты</b> для анализа и уверенной торговли на форекс, от опционных уровней
                                </h2>
                                <div className="txt">
                                    <p>Используйте наши стратегии, профессиональные индикаторы, инструменты для анализа
                                        и успешной торговли на валютном рынке форекс, фьючерсном рынке, а также срочного
                                        рынка фортс.</p>
                                </div>
                                <Link to="/account/tools" className="btn btn-green hidden-sm">Начать пользоваться</Link>
                            </div>
                        </div>
                    </div>

                    <div className="toolspage_third">
                        <div className="container">
                            <div className="flex">
                                <div className="flexcol">
                                    <h2><b>Анализируйте рынок</b> и торгуйте уверенно</h2>
                                    <div className="txt">
                                        <p>Специально для простоты анализа рынка форекс, мы разработали индикатор
                                            опционных уровней. Он рисует на графике важные опционные уровни, от которых
                                            происходят развороты тренда. Имея данный инструмент, вы будете входить в
                                            позиции намного увереннее и эффективнее</p>
                                    </div>
                                </div>
                                <div className="flexcol img">
                                    <img src="images/tools/bg3.jpg" alt=""/>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="toolspage_fourth">
                        <div className="container">
                            <h2>Почему работают <b>опционы на форекс?</b></h2>
                            <div className="row">
                                <div className="grid3">
                                    <div className="advtool">
                                        <img src="images/tools/icon1.svg" alt=""/>
                                            40% контрактов по валютным инструментам это контракты на фьючерсы и опционы
                                            на фьючерсы.
                                    </div>
                                </div>
                                <div className="grid3">
                                    <div className="advtool advtool-blue">
                                        <img src="images/tools/icon2.svg" alt=""/>
                                            Опционы и фьючерсы – прозрачный рынок, по отчетам с которого мы можем
                                            отследить реальный проторгованный объем, открытый интерес и волатильность.
                                    </div>
                                </div>
                                <div className="grid3">
                                    <div className="advtool advtool-green">
                                        <img src="images/tools/icon3.svg" alt=""/>
                                            Это рынок, на котором нет дневных спекулянтов и скальперов. Поэтому мы можем
                                            отследить настроение и ожидания рынка.
                                    </div>
                                </div>
                                <div className="grid3">
                                    <div className="advtool advtool-red">
                                        <img src="images/tools/icon4.svg" alt=""/>
                                            Рынок СПОТ (валютный, акции) и срочный рынок (фьючерсы, опционы) ходит
                                            одинаково, а это значит что информацию из отчетов Чикагской биржи мы можем
                                            применять на рынке Форекс.
                                    </div>
                                </div>
                            </div>

                            <div className="indicators">
                                <h2><b>Используйте наши индикаторы</b> применяйте их на форекс, получайте уровни
                                    разворота цены в МТ4</h2>
                                <Link to="/account/tools" className="btn btn-green">Начать пользоваться</Link>
                            </div>
                        </div>
                    </div>

                    <div className="toolspage_fifth">
                        <div className="container">
                            <div className="flex">
                                <div className="flexcol">
                                    <h2><b>Лучшие индикаторы</b> опционного анализа</h2>
                                    <div className="txt">
                                        <p>Все наши индикаторы, строят важные ценовые уровни, которые мы извлекаем из
                                            отчетов с Чикагской товарной биржи CME Group.</p>
                                    </div>
                                    <div className="flex flex_fifth">
                                        <Link to="/account/tools" className="btn btn-green">Подробнее</Link>
                                        <span
                                            className="grey">Можно узнать, и скачать индикатор в личном кабинете</span>
                                    </div>
                                </div>
                                <div className="flexcol">
                                    <div className="latptop_slider">
                                        <div className="latptop_slider_mask"></div>
                                        {this.props.menu.toolsFirstSlider &&
                                            <Swiper
                                                spaceBetween={50}
                                                navigation
                                                pagination={{clickable: true}}
                                                scrollbar={{draggable: true}}
                                                onSlideChange={() => console.log('slide change')}
                                                onSwiper={(swiper) => console.log(swiper)}
                                            >
                                                {this.props.menu.toolsFirstSlider.map((obj, index) => {
                                                    let style_item = {backgroundImage:'url(' + obj.pic + ')'}

                                                    return (
                                                        <SwiperSlide key={obj.id}>
                                                            <h4>{obj.title}</h4>
                                                            <p>{rHTML(obj.description, '')}</p>
                                                            <div className="latptop_slider_img latptop_slider_chat"
                                                                 style={style_item} key={index}></div>
                                                        </SwiperSlide>
                                                    )
                                                })}
                                            </Swiper>
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="toolspage_sixth">
                        <div className="container">
                            <div className="flex">
                                <div className="flexcol order2">
                                    <h2>Применяйте Наши торговые решения и получайте <b>уверенные точки входа</b></h2>
                                    <div className="txt">
                                        <p>Торгуйте правильно, в одном направлении с крупными участниками и маркет
                                            мейкерерами, выбирая только прибыльные сделки, держите свои риски под
                                            контролем</p>
                                    </div>
                                    <Link to="/account/tools" className="btn btn-green btn-play"><span>Посмотреть</span></Link>
                                </div>
                                <div className="flexcol order1">
                                    {this.props.menu.toolsSecondSlider &&
                                        <OwlCarousel
                                            className='toolspage_slider owl-carousel owl-theme'
                                            id="signals_slider"
                                            items={1}
                                            margin={0}
                                            mouseDrag={true}
                                            dots={true}
                                            nav={false}
                                            autoplay={true}
                                            loop={true}
                                        >
                                            {this.props.menu.toolsSecondSlider.map((obj, index) => {
                                                let style_item = {backgroundImage: 'url(' + obj.pic + ')'}

                                                return (
                                                    <div className="item" key={obj.id}>
                                                        <img src={obj.pic} alt=""/></div>
                                                )
                                            })}
                                        </OwlCarousel>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="toolspage_seventh">
                        <div className="container">
                            <div className="flex">
                                <div className="flexcol">
                                    <h2><b>Совершенствуйте торговлю</b> используя инструменты</h2>
                                    <div className="txt">
                                        <p>С помощью наших индикаторов, Вы облегчите свой анализ, повысите уровень
                                            торговли, будете знать, где покупает или продает актив крупный участник
                                            рынка, определять более точные входы.</p>
                                    </div>
                                </div>
                                <div className="flexcol">

                                    <div className="smtools">
                                        <div className="smtool">
                                            <div className="smtool_inner">
                                                <div className="smtool_type">OU1</div>
                                                <div className="smtool_name">Уровень первого порядка</div>
                                                Индикатор показывает уровень ликвидности, на котором вероятнее всего
                                                будет происходить набор позиции крупных участников рынка
                                            </div>
                                        </div>
                                        <div className="smtool smtool-green">
                                            <div className="smtool_inner">
                                                <div className="smtool_type">OU2</div>
                                                <div className="smtool_name">Уровень второго порядка</div>
                                                Уровень от которого возможен с наибольшей долей вероятности разворот
                                                цены, индикатор строит данный уровень на графике в терминале МТ4
                                            </div>
                                        </div>
                                        <div className="smtool smtool-orange">
                                            <div className="smtool_inner">
                                                <div className="smtool_type">OU3</div>
                                                <div className="smtool_name">Уровень третьего порядка</div>
                                                Показывает максимальное скопление открытых контрактов, индикатор
                                                отображает эти данные на графике в вашем терминале МТ4
                                            </div>
                                        </div>
                                        <div className="smtool smtool-red">
                                            <div className="smtool_inner">
                                                <div className="smtool_type">Open interest</div>
                                                <div className="smtool_name">Открытый интерес</div>
                                                Показывает общий объем контрактов которые отрывали участники рынка, как
                                                на продажу так и на покупку
                                            </div>
                                        </div>
                                        <div className="smtool">
                                            <div className="smtool_inner">
                                                <div className="smtool_type">PNT</div>
                                                <div className="smtool_name">ClearPort</div>
                                                Уровни приватных и крупных участников рынка, имея данные уровни на
                                                графике, с большой долей вероятности можно войти в сделку с минимальными
                                                рисками.
                                            </div>
                                        </div>
                                        <div className="smtool smtool-green">
                                            <div className="smtool_inner">
                                                <div className="smtool_type">Risk Premium</div>
                                                <div className="smtool_name">Премии за риск</div>
                                                Зная, где расположены премии, можно избежать убытков присущих рынку
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="toolspage_eigth">
                        <div className="container">
                            <h2>Прибыль трейдеров, которые пользуются <b>сервисом Ztrade</b></h2>


                            <div className="trader_slider">
                                <div className="module trader">
                                    <div className="trader_card">
                                        <div className="trader_ava"><img src="images/tools/ava.jpg" alt=""/></div>
                                        <div className="trader_entry">
                                            <div className="trader_name">Makssis</div>
                                            <p>Недавно начал изучать объемный и опционный анализ, прошел пару курсов по
                                                этой теме. Очень порадовали индикаторы, очень сильно облегчают работу.
                                                Познакомился с ними когда еще были «сырые» и не было самого сайта с
                                                аналитикой, но теперь часть недочетов и багов исправлено, думаю скоро
                                                будут работать идеально.</p>
                                        </div>
                                    </div>
                                    <div className="trader_list">
                                        <div className="trader_box">
                                            <div className="trader_box_in">
                                                <div className="trader_box_name">Торговые инструменты</div>
                                                EURUSD, GBPUSD, USDJPY, USDCAD
                                            </div>
                                        </div>
                                        <div className="trader_box">
                                            <div className="trader_box_in">
                                                <div className="trader_box_name">Торговая стратегия</div>
                                                Торговля по месячным, недельным и дневным границам рынка
                                            </div>
                                        </div>
                                        <div className="trader_box">
                                            <div className="trader_box_in">
                                                <div className="trader_box_name">Money Managment</div>
                                                <div><b>2%</b><br/> <small>на сделку</small></div>
                                            </div>
                                        </div>
                                        <div className="trader_box">
                                            <div className="trader_box_in">
                                                <div className="trader_box_name">Средняя прибыль в месяц</div>
                                                <b>10%</b>
                                            </div>
                                        </div>
                                        <div className="trader_box">
                                            <div className="trader_box_in">
                                                <div className="trader_box_name">Макс. прибыль в месяц</div>
                                                <b>30%</b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="trader_slider_arrows">
                                    <div className="arrow_avatar arrow_avatar-prev">
                                        <div><img src="images/tools/ava.jpg" alt=""/></div>
                                        <span>Иван Жарновский</span>
                                    </div>
                                    <div className="arrow_avatar arrow_avatar-next">
                                        <div><img src="images/tools/ava.jpg" alt=""/></div>
                                        <span>Иван Жарновский</span>
                                    </div>
                                </div>
                            </div>

                            <div className="indicators">
                                <h2><b>Используйте наши индикаторы</b> применяйте их на форекс, получайте уровни
                                    разворота цены в МТ4</h2>
                                <a href="/account/tools" className="btn btn-green">Начать пользоваться</a>
                            </div>
                        </div>
                    </div>


                </div>

            </React.Fragment>
        )
    }
}

ToolPage.propTypes = {
    menu: MPropTypes.observableObject
}
