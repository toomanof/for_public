import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { SideWrapper, Breadcrumbs, SideRecommend } from 'components'
import {Link} from 'react-router-dom';


@inject('menu')
@observer
export default class PaymentSuccessTariff extends React.Component{
    render(){
        const { menu } = this.props

        return (
            <React.Fragment>
                <div className="container middle">
                    <div className="brick">
                        <SideWrapper />

                        <div className="brick_content">
                            <div className="brick_box">
                                <Breadcrumbs />

                                {/* <h1>{menu.ami.h1}</h1> */}
                                
                                <div className="module page">
                                
                                    <p>
                                        Вы успешно оформили подписку на торговые рекомендации, воспользоваться сервисом торговых сигналов, можно в личном кабинете во вкладке
                                        <Link to="/account/signals">{'«Сигналы»'}</Link>
                                    </p>
                                    <p>
                                        Чтобы не пропустить важные рекомендации, подпишитесь на нашу группу <Link to="https://t.me/ztradesignals">{'«Telegram»'}</Link>, и получай сигналы прямо на телефон.
                                    </p>

                                </div>
                            </div>
                        </div>
                    </div>

                    <SideRecommend />

                </div>
            </React.Fragment>
        )
    }
}


PaymentSuccessTariff.propTypes = {
    menu: MPropTypes.observableObject
}