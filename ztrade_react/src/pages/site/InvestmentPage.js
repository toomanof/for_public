import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'
import { rHTML, rpHTML } from 'services/helpers'
import {
    Breadcrumbs,
    SideWrapper,
} from 'components'


@inject('menu', 'routing', 't', 'user')
@observer
export default class InvestmentPage extends React.Component{
    state = {
        name: 'Инвестору',
        text: this.props.t.d.a007,
        investorClassName: 'active',
        managerClassName: '',
        leftColumnTitle: this.props.t.d.a009,
        rightColumnTitle: this.props.t.d.a010,
        leftColumnText: this.props.t.d.a011,
        rightColumnText: this.props.t.d.a012,
    }

    onClickInvestor = (e) => {
        e.preventDefault()
        this.setState({
            name: 'Инвестору',
            text: this.props.t.d.a007,
            investorClassName: 'active',
            managerClassName: '',
            leftColumnTitle: this.props.t.d.a009,
            rightColumnTitle: this.props.t.d.a010,
            leftColumnText: this.props.t.d.a011,
            rightColumnText: this.props.t.d.a012,
        })
    }

    onClickManager = (e) => {
        e.preventDefault()
        this.setState({
            name: 'Управляющему',
            text: this.props.t.d.a008,
            investorClassName: '',
            managerClassName: 'active',
            leftColumnTitle: this.props.t.d.a013,
            rightColumnTitle: this.props.t.d.a014,
            leftColumnText: this.props.t.d.a015,
            rightColumnText: this.props.t.d.a016,
        })
    }


    onClickContinue = () => {
        this.props.routing.push('/site/signup')
    }

    render(){
        if (!this.props.menu.ami){
            return null
        }

        const { t } = this.props

        return(
            <React.Fragment>
                <div className="container middle">
                    <Breadcrumbs />

                    <h1>{this.props.menu.ami.h1}</h1>

                    <ul className="switch">
                        <li className={this.state.managerClassName}>
                            <a href="" onClick={this.onClickManager}>
                                {'Управляющему'}
                            </a>
                        </li>
                        <li className={this.state.investorClassName}>
                            <a href="" onClick={this.onClickInvestor}>
                                {'Инвестору'}
                            </a>
                        </li>
                    </ul>

                    <div className="invest_text">
                        <img src="/images/icons/invest.png" alt="" srcSet="/images/icons/invest-2x.png 2x" />
                        <h2>{this.state.name}</h2>
                        {rpHTML(this.state.text)}
                    </div>

                    <div className="row invest_row">
                        <div className="grid6 sm12">
                            <div className="module receipt">
                                <h2>{this.state.leftColumnTitle}</h2>
                                {rHTML(this.state.leftColumnText)}
                            </div>
                        </div>
                        <div className="grid6 sm12">
                            <div className="module invest">
                                <h2>{this.state.rightColumnTitle}</h2>
                                {rHTML(this.state.rightColumnText)}
                            </div>
                        </div>
                    </div>
                </div>

                {!this.props.user.isAuthenticated &&
                    <div className="register_box">
                        <div className="container">
                            <h2>{'Зарегистрируйтесь'}</h2>
                            <p>
                                {'Заполните форму, чтобы познакомиться с сервисом'}
                                <br />
                                {'на демо- или реальном счете.'}
                            </p>
                            
                            <input
                                type="button"
                                onClick={this.onClickContinue}
                                className="btn btn-orange"
                                value="Продолжить" />

                            <p />

                            <small>
                                {'Уже зарегистрированы?'}<br />
                                {' Узнайте, как стать '}
                                <Link to={t.d.a006}>{'Инвестором'}</Link>
                            </small>
                        </div>
                    </div>
                }

                <SideWrapper isDesktopHidden />
            </React.Fragment>
        )
    }
}


InvestmentPage.propTypes = {
    menu: MPropTypes.observableObject,
    routing: MPropTypes.observableObject,
    t: MPropTypes.observableObject,
    user: MPropTypes.observableObject
}