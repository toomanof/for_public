import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'
import { formToObject } from 'services/helpers'
import { CountryModel } from 'models'
import { ErrorBlock, Input, LoadingPage, Select } from 'components'

@inject('general', 'menu', 'routing', 'user')
@observer
export default class SignupPage extends React.Component {
    constructor(props) {
        super(props)
        this.phoneInputRef = React.createRef()
    }

    state = {
        countryId: null,
        phone: '',
        phonePrefix: null
    }

    componentDidMount() {
        document.body.classList.add('loginpage')
        this.props.general.loadCountries()
    }

    componentWillUnmount() {
        document.body.classList.remove('loginpage')
        this.props.user.clear()
    }

    onChangeCountry = value => {
        if (value === '') {
            this.setState({
                countryId: null,
                phonePrefix: null,
                phone: ''
            })
        } else {
            value = parseInt(value, 10)

            const c = CountryModel.getById(value)
            this.setState({
                countryId: value,
                phonePrefix: c.phonePrefix,
                phone: ''
            })
        }
        this.phoneInputRef.current.focus()
    }

    onChangePhone = value => {
        this.setState({ phone: value })
    }

    onSuccess = () => {
        this.props.routing.push('/site/signup_confirm')
    }

    onSubmit = e => {
        e.preventDefault()
        const { user } = this.props
        const form = formToObject(e.target, ['country_id'])
        user.signup(form, this.onSuccess)
    }

    render() {
        const { general, menu, user } = this.props

        if (!menu.ami || !general.countries) {
            return <LoadingPage />
        }

        return (
            <div className="signbox">
                <div className="logobox">
                    <Link to="/">
                        <img src="/images/logo.svg" alt="" />
                    </Link>
                </div>

                <Link to="/site/login" className="btn btn-blue">
                    {'Вход в личный кабинет'}
                </Link>
                <div className="clear" />

                <div className="module signform">
                    <h2>{'Регистрация'}</h2>

                    <form onSubmit={this.onSubmit}>
                        <Select
                            name="country_id"
                            options={CountryModel.selectOptions}
                            errors={user.errors}
                            value={this.state.countryId}
                            onChange={this.onChangeCountry}
                        />

                        <Input
                            cref={this.phoneInputRef}
                            type="phone"
                            name="phone"
                            placeholder="Телефон"
                            errors={user.errors}
                            value={this.state.phone}
                            prefix={this.state.phonePrefix}
                            onChange={this.onChangePhone}
                        />

                        <Input
                            name="first_name"
                            placeholder="Имя"
                            errors={user.errors}
                        />

                        <Input
                            name="last_name"
                            placeholder="Фамилия"
                            errors={user.errors}
                        />

                        <Input
                            type="email"
                            name="email"
                            placeholder="E-mail"
                            errors={user.errors}
                        />

                        <Input
                            type="text"
                            name="city"
                            placeholder="Город"
                            errors={user.errors}
                        />

                        <ErrorBlock errors={user.errors} />

                        <input
                            type="submit"
                            className="btn btn-orange btn-big"
                            value="Зарегистрироваться"
                        />
                    </form>
                </div>
            </div>
        )
    }
}

SignupPage.propTypes = {
    general: MPropTypes.observableObject,
    menu: MPropTypes.observableObject,
    routing: MPropTypes.observableObject,
    user: MPropTypes.observableObject
}
