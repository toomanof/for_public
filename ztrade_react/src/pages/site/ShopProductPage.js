import React, { useState } from 'react';
import PropTypes from 'prop-types'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'
import SwiperCore, { Thumbs, Navigation, Pagination, Scrollbar, A11y, EffectCoverflow } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';

import { ActiveMenuItemModel } from 'models'
import { rHTML } from 'services/helpers'
import {
    Breadcrumbs,
    ButtonSubmit,
    Loading,
    ProductBuyModal,
    ProductPics,
    SideSocial,
    SideWrapper
} from 'components'

import 'swiper/swiper.scss';
import 'swiper/components/effect-fade/effect-fade.scss';

SwiperCore.use([Navigation, Pagination, Scrollbar, A11y, Thumbs, EffectCoverflow]);

@inject('menu', 'modal', 'payment', 'routing', 'shop', 'user')
@observer
export default class ShopProductPage extends React.Component {
    state = {
        activeType: 'info',
        thumbsSwiper: null,
        setThumbsSwiper: null

    }
    constructor(props) {
        super(props);
        const thumbsSwiper = null
        const setThumbsSwiper = null
    }
    componentDidMount() {
        const slug = this.props.match.params.slug
        this.props.shop.loadProduct(slug, this.onNotFound)
        window.	$('.gallery_big .swiper-slide a').fancybox({
            autoFocus: false,
            touch: false,
            closeBtn: false,
            buttons: [
                "zoom",
                //"share",
                "slideShow",
                //"fullScreen",
                //"download",
                "thumbs",
                "close"
            ],
            btnTpl: { close: '', smallBtn: '' }
        });
    }

    componentWillUnmount() {
        this.props.payment.clear()
    }

    onNotFound = () => {
        const ami = new ActiveMenuItemModel({
            title: 'Страница не найдена',
            h1: 'Страница не найдена'
        })
        this.props.menu.setActiveMenuItem(ami)
        this.props.routing.push('/404')
    }

    onClickPayButton = () => {
        const { modal, payment, shop, user } = this.props

        if (user.isAuthenticated) {
            const form = {}
            form.name = user.profile.firstName
            form.email = user.profile.email
            form.product_id = shop.productItem.id
            form.phone = user.profile.phone
            payment.createBillProduct(form, true)
        } else {
            modal.openProductBuyModal()
        }
    }

    render() {
        const { menu, modal, payment, shop } = this.props

        if (!menu.ami) {
            return null
        }

        if (!shop.productItem) {
            return <Loading />
        }

        const product = shop.productItem
        console.log('this.state.thumbsSwiper', this.state.thumbsSwiper)
        console.log('this.state.setThumbsSwiper', this.state.setThumbsSwiper)
        return (
            <React.Fragment>
                <div className="container middle">
                    <Breadcrumbs />
                    <h1>{product.h1}</h1>

                    <div className="row">
                        <div className="grid8 tb12">
                            <div className="module gallery_out">
                                {/*<ProductPics items={product.pics} /> */}
                                <div className="gallery">
                                    <div className="gallery_big">
                                        {product.pics &&
                                            <Swiper
                                                spaceBetween={50}
                                                navigation

                                                thumbs={{ swiper: this.state.thumbsSwiper }}
                                            >
                                                {product.pics.map((obj, index) => {
                                                    return (
                                                        <SwiperSlide key={obj.id}>
                                                            <div className="swiper-slide">
                                                                <a href={obj.pic_url} data-fancybox="gallery">
                                                                    <img src={obj.pic_url} alt=""/>
                                                                </a>
                                                            </div>
                                                        </SwiperSlide>
                                                    )
                                                })}
                                            </Swiper>
                                        }
                                    </div>
                                    <div className="gallery_thumbs">
                                        {product.pics &&
                                        <Swiper
                                            spaceBetween={10}
                                            slidesPerView={5}
                                            onSwiper={(swiper) =>{ console.log(swiper); this.setState({thumbsSwiper: swiper}) }}
                                            freeMode
                                            watchSlidesVisibility
                                            watchSlidesProgress
                                        >
                                            {product.pics.map((obj, index) => {
                                                let style_item = {backgroundImage:'url(' + obj.pic_url + ')'}
                                                return (
                                                    <SwiperSlide key={obj.id}>
                                                        <div className="swiper-slide" style={style_item}></div>
                                                    </SwiperSlide>
                                                )
                                            })}
                                        </Swiper>
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="grid4 tb12">
                            <div className="module shopMeta">
                                <div className="shopMeta_box shopMeta_price">
                                    <label>{'Цена'}</label>
                                    <div className="smp">
                                        <div className="smp_bold">
                                            {product.pricePart}
                                        </div>
                                        <div className="smp_small">
                                            <span>{'RUB '}</span>
                                            {'.'}
                                            {product.priceCentsPart}
                                        </div>
                                    </div>
                                </div>

                                <div className="shopMeta_box shopMeta_part">
                                    <label>
                                        {'Партнерам '}
                                        <b>{`${product.partnerAmount} USD`}</b>
                                    </label>
                                    <Link to="#" className="how">
                                        {'Как заработать?'}
                                    </Link>
                                </div>

                                <div className="shopMeta_box">
                                    <label>{'Оплата через'}</label>
                                    <select className="selectbox">
                                        <option>{'Яндекс Деньги'}</option>
                                    </select>

                                    <ButtonSubmit
                                        type="button"
                                        text="Купить"
                                        className="btn btn-blue btn-big"
                                        onClick={this.onClickPayButton}
                                        isButtonEnabled={
                                            !payment.isCreateBillInProgress
                                        }
                                    />
                                </div>
                            </div>
                            <div className="module shopPageSocial">
                                <SideSocial />
                            </div>
                        </div>
                    </div>

                    {rHTML(product.content, 'text')}

                    {modal.isProductBuyModalOpened && (
                        <ProductBuyModal productId={product.id} />
                    )}
                </div>
                <SideWrapper isDesktopHidden />
            </React.Fragment>
        )
    }
}

ShopProductPage.propTypes = {
    match: PropTypes.object,
    menu: MPropTypes.observableObject,
    modal: MPropTypes.observableObject,
    payment: MPropTypes.observableObject,
    routing: MPropTypes.observableObject,
    shop: MPropTypes.observableObject,
    user: MPropTypes.observableObject
}
