import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import {
    Breadcrumbs,
    SideWrapper,
    SideRecommend
} from 'components'


@inject('menu')
@observer
export default class PartnerPage extends React.Component{
    render(){
        if (!this.props.menu.ami){
            return null
        }

        return(
            <React.Fragment>
                <div className="container middle">
                    <div className="brick">
                        <SideWrapper />

                        <div className="brick_content">
                            <div className="brick_box">
                                <Breadcrumbs />

                                <h1>{this.props.menu.ami.h1}</h1>
                                
                                <div className="module page">
                                    <div className="page_cover2">
                                        <img src={this.props.menu.ami.picUrl} alt="" />
                                    </div>
                                    <div className="page_entry">
                                        <p>Представляющий брокер — это отличная возможность для агентов, предпринимателей и представителей бизнеса расширить свое дело, привлекая клиентов в Mtrading и зарабатывая на комиссии с трейдинга. Как частные лица, так и организации могут стать успешными представителями брокера.<br />
                                        Наша задача — строить взаимовыгодные отношения с целью обслуживать наших клиентов на высоком профессиональном уровне, предлагая отличные комиссионные и бонусы нашим партнерам.</p>
                                        <h2>Как это работает</h2>
                                        <ul className="ul_checks">
                                            <li>Зарегистрируйтесь как представитель брокера и откройте IB-счет.</li>
                                            <li>Продвигайте услуги компании среди потенциальных клиентов, предлагайте открыт счет и начать торговлю.</li>
                                            <li>Получайте IB-вознаграждение.</li>
                                        </ul>

                                        <div className="infograf">
                                            <div className="infograf_itm itm1"><span>Клиенты</span></div>
                                            <div className="infograf_itm itm2"><span>Введение брокера</span></div>
                                            <div className="infograf_itm itm3 active"><span>Ztrade</span></div>
                                            <div className="infograf_part">Доля выручки</div>
                                        </div>

                                        <hr />
                                        <h2>Ztrade предлагает представителям:</h2>
                                        <p><b>Конкурентную комиссионную схему</b><br />
                                        Выбор из нескольких комиссионных схем. Чем больше ваш бизнес, тем большую комиссию мы предлагаем.</p>
                                        <p><b>Прозрачные и гибкие выплаты</b><br />
                                        Мы поддерживаем различные платежные системы и эл. кошельки, которые вы можете использовать для безопасного вывода своего вознаграждения</p>
                                        <p><b>Компетентная и дружественная поддержка на нескольких языках</b><br />
                                        Наша команда состоит из специалистов из разных стран мира, которые всегда смогут вам помочь</p>
                                        <p><b>Доступ к Кабинету Партнера в режиме 24/7</b><br />
                                        Защищенный и мгновенный доступ к личной информации, результатам торговли клиентов, данным по комиссиям и прибыли.</p>
                                        <p><b>Маркетинговые материалы</b><br />
                                        Наши материалы представлены на нескольких языках: это баннеры, виджеты, буклеты, шаблоны эл. писем, оформление для соц. сетей и многое другое прямо в Кабинете Партнера.</p>
                                        <hr />
                                        <h2>Частные лица и организации, которые могут начать успешное партнерство</h2>
                                        <div className="row">
                                            <div className="grid6 sm12">
                                                <h3>Частные лица:</h3>
                                                <ul>
                                                    <li>Финансовые консультанты</li>
                                                    <li>Фондовые посредники</li>
                                                    <li>Агенты по недвижимости</li>
                                                    <li>Бухгалтера</li>
                                                    <li>Трейдеры, желающие работать на финансовом рынке</li>
                                                    <li></li>
                                                    <li>Частные лица, предоставляющие финансовые услуги</li>
                                                </ul>
                                            </div>
                                            <div className="grid6 sm12">
                                                <h3>Юридические лица:</h3>
                                                <ul>
                                                    <li>Банки</li>
                                                    <li>Страховые компании</li>
                                                    <li>Компании, занимающиеся недвижимостью</li>
                                                    <li>Бухгалтерские компании</li>
                                                    <li>Компании, работающие на рынке производных фин. инструментов</li>
                                                    <li>Компании, предоставляющие финансовые услуги</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <hr />
                                        <h2>Часто задаваемые вопросы:</h2>
                                        <div className="spoiler">
                                            <div className="spoiler_name">В КП у вас есть доступ к различным отчетам и статистике для анализа вашей работы?</div>
                                            <div className="spoiler_content">
                                                Наши материалы представлены на нескольких языках: это баннеры, виджеты, буклеты, шаблоны эл. писем, оформление для соц. сетей и многое другое прямо в Кабинете Партнера.
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="module partnerForm">
                                    <div className="partnerForm_title">Стать партнером</div>
                                    <p>Введите адрес эл. почты, чтобы продолжить.</p>
                                    <div className="register_form">
                                        <form action="#" method="get">
                                            <input type="text" name="reg" className="reginp" placeholder="Ваш E-mail" />
                                            <input type="button" className="btn btn-blue" value="Продолжить" />
                                        </form>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <SideRecommend />

                </div>
            </React.Fragment>
        )
    }
}


PartnerPage.propTypes = {
    menu: MPropTypes.observableObject
}