import React from 'react'
import PropTypes from 'prop-types'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { ActiveMenuItemModel } from 'models'
import { EXCHANGE } from 'services/constants'
import {
    Breadcrumbs,
    ExchangeTabAccountTypes,
    ExchangeTabInfo,
    ExchangeTabParams,
    ExchangeTabReviews,
    ExchangeTabs,
    ExchangeTopBox,
    Loading,
    SideWrapper,
    SideRecommend,
    ExchangeWhichAccountOpenModal
} from 'components'

@inject('exchange', 'menu', 'routing', 'modal')
@observer
export default class ExchangePage extends React.Component {
    state = {
        activeType: 'info'
    }

    componentDidMount() {
        const slug = this.props.match.params.slug
        this.props.exchange.loadExchange(slug, EXCHANGE, this.onNotFound)
    }

    onNotFound = () => {
        const ami = new ActiveMenuItemModel({
            title: 'Страница не найдена',
            h1: 'Страница не найдена'
        })
        this.props.menu.setActiveMenuItem(ami)
        this.props.routing.push('/404')
    }

    onClickTab = type => {
        this.setState({ activeType: type })
    }

    render() {
        const { exchange, menu, modal } = this.props
        const { activeType } = this.state

        if (!menu.ami || !exchange.exchangeItem) {
            return <Loading />
        }

        return (
            <React.Fragment>
                <div className="container middle exchange">
                    <div className="brick">
                        <SideWrapper />
                        <div className="brick_content">
                            <div className="brick_box">
                                <Breadcrumbs />

                                <h1>{menu.ami.h1}</h1>

                                <ExchangeTopBox />

                                <ExchangeTabs
                                    activeType={activeType}
                                    onClickTab={this.onClickTab}
                                />
                                {activeType === 'info' && <ExchangeTabInfo />}
                                {activeType === 'params' && <ExchangeTabParams />}
                                {activeType === 'types' && (
                                    <ExchangeTabAccountTypes />
                                )}
                                {activeType === 'reviews' && (
                                    <ExchangeTabReviews />
                                )}
                            </div>
                        </div>
                    </div>
                    {modal.isExchangeWhichAccountModalOpened && (
                        <ExchangeWhichAccountOpenModal />
                    )}
                    <SideRecommend />
                </div>
            </React.Fragment>
        )
    }
}

ExchangePage.propTypes = {
    exchange: MPropTypes.observableObject,
    match: PropTypes.object,
    menu: MPropTypes.observableObject,
    routing: MPropTypes.observableObject
}
