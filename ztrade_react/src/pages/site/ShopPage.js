import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'

import { rHTML } from 'services/helpers'

import {
    Breadcrumbs,
    Loading,
    ProductBox,
    Select,
    SideWrapper,
    Paginator,
} from 'components'


@inject('menu', 'paginator', 'shop')
@observer
export default class ShopPage extends React.Component{
    componentDidMount(){
        const { shop } = this.props
        shop.loadCategories()
        shop.loadMarkets()
        shop.loadProducts()
    }

    componentWillUnmount(){
        this.props.paginator.clear()
        this.props.shop.clear()   
    }

    onChangeCategory = (value) => {
        this.props.shop.setSelectedCategoryId(value)
    }

    onChangeMarket = (value) => {
        this.props.shop.setSelectedMarketId(value)
    }

    get categoryOptions(){
        const { shop } = this.props

        let arr = []
        arr.push({value: '', text: 'Все категории'})
        for (let obj of shop.categories){
            arr.push({value: obj.id, text: obj.name})
        }
        return arr
    }

    get marketOptions(){
        const { shop } = this.props

        let arr = []
        arr.push({value: '', text: 'Все рынки'})
        for (let obj of shop.markets){
            arr.push({value: obj.id, text: obj.name})
        }
        return arr
    }

    render(){
        const { menu, shop, paginator } = this.props
        const ami = menu.ami

        if (
            !shop.categories || 
            !shop.markets || 
            !ami){
            return <Loading />
        }

        return (
            <React.Fragment>
                <div className="container middle">

                    <div className="shopTop">
                        <div className="shopTop_left">
                            <Breadcrumbs />
                            <h1>{ami.h1}</h1>
                        </div>
                        
                        <div className="shopTop_right">
                            <Select
                                name="category"
                                hasWrapper={false}
                                options={this.categoryOptions}
                                onChange={this.onChangeCategory} />

                            <Select
                                name="market"
                                hasWrapper={false}
                                options={this.marketOptions}
                                onChange={this.onChangeMarket} />

                            <div className="shopTop_view">
                                <a href="/" className="viewlink viewlink_box active" />
                                <a href="/" className="viewlink viewlink_list" />
                            </div>
                        </div>
                    </div>

                    <div className="learInfo">
                        {'В нашем магазине, вы можете купить и скачать различные авторские индикаторы, прибыльные торговые стратегии, торговые роботы, советники, книги по трейдингу, обучающие курсы по трейдингу. Данные инструменты применяются для анализа и торговли на финансовых рынках.'}
                    </div>

                    {!shop.products &&
                        <Loading />
                    }

                    {paginator.paginatedItems.length > 0 &&
                        <React.Fragment>
                            <div className="row">
                                {paginator.paginatedItems.map(obj => {
                                    return <ProductBox key={obj.id} product={obj} />
                                })}
                            </div>

                            <Paginator />
                        </React.Fragment>
                    }

                    {rHTML(ami.content, 'text')}

                </div>

                <SideWrapper isDesktopHidden />
            </React.Fragment>
        )
    }
}


ShopPage.propTypes = {
    menu: MPropTypes.observableObject,
    paginator: MPropTypes.observableObject,
    shop: MPropTypes.observableObject,
}