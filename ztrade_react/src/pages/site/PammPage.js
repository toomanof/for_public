import React from 'react'
import PropTypes from 'prop-types'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { ActiveMenuItemModel } from 'models'
import { PAMM } from 'services/constants'
import { rHTML } from 'services/helpers'
import {
    Breadcrumbs,
    BrokerTopBox,
    BrokerPammBox,
    Loading,
    PammBox,
    SideWrapper,
    SideRecommend,
    SocialBox
} from 'components'



@inject('broker', 'menu', 'routing')
@observer
export default class PammPage extends React.Component{
    componentDidMount(){
        const slug = this.props.match.params.slug
        this.props.broker.loadBroker(slug, PAMM, this.onNotFound)
    }

    onNotFound = () => {
        const ami = new ActiveMenuItemModel(
            {title: 'Страница не найдена', h1: 'Страница не найдена'}
        )
        this.props.menu.setActiveMenuItem(ami)
        this.props.routing.push('/404')
    }

    render(){
        const { broker, menu } = this.props
        
        if (!menu.ami){
            return null
        }

        if (!broker.brokerItem){
            return <Loading />
        }

        return(
            <React.Fragment>
                <div className="container middle">
                    <div className="brick">
                        <SideWrapper />

                        <div className="brick_content">
                            <div className="brick_box">
                                <Breadcrumbs />

                                <h1>{menu.ami.h1}</h1>

                                <BrokerTopBox />

                                <div className="header1">{'ПАММ портфель'}</div>
                                
                                <BrokerPammBox />

                                <div className="module">
                                    {broker.brokerItem.pamms.map(obj => {
                                        return <PammBox key={obj.id} obj={obj} />
                                    })}
                                </div>

                                <div className="header1">{'Как инвестировать в ПАММ счета'}</div>

                                <div className="module page">
                                    <div className="page_entry">
                                        <p>{'Ниже вы можете найти несколько видеороликов, которые наглядно показывают, как происходит процесс вложения средств в памм-счета различных брокеров онлайн.'}</p>
                                        
                                        {broker.brokerItem.pammVideos.map(obj => {
                                            return (
                                                <React.Fragment key={obj.id}>
                                                    {obj.title && <h3>{obj.title}</h3>}
                                                    <iframe title="ПАММ" src={obj.link} />            
                                                </React.Fragment>
                                            )
                                        })}

                                        <SocialBox className="socialbox" />

                                        {broker.brokerItem.pammRecommends.length > 0 &&
                                            <div className="page_related">
                                                <div className="page_related_title">{'Также рекомендуем'}</div>
                                                <ul>
                                                    {broker.brokerItem.pammRecommends.map(obj => {
                                                        return (
                                                            <li key={obj.id}>
                                                                <a href={obj.url}>{obj.name}</a>
                                                            </li>                                                            
                                                        )
                                                    })}
                                                </ul>
                                            </div>
                                        }
                                    </div>                  
                                </div>

                                {rHTML(broker.brokerItem.contentPamm, 'text')}

                            </div>
                        </div>
                    </div>

                    <SideRecommend />

                </div>
            </React.Fragment>
        )
    }
}


PammPage.propTypes = {
    broker: MPropTypes.observableObject,
    match: PropTypes.object,
    menu: MPropTypes.observableObject,
    routing: MPropTypes.observableObject,
}