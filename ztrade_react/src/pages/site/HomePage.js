import React, { Component } from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { rHTML } from 'services/helpers'
import {
    ArticlesSlider,
    ExchangeBlock,
    HomeSlider,
    HomeSignalInfo,
    HomeSignalSlider,
    HomeBenefits,
    SideWrapper,
    SideRecommend,
    MainHomeSlider
} from 'components'

@inject('menu')
@observer
export default class HomePage extends Component {
    render() {
        if (!this.props.menu.ami) {
            return null
        }

        return (
            <React.Fragment>
                <MainHomeSlider/>
                { /*<HomeSlider /> */}
                <div className="container">
                    <div className="brick">
                        <SideWrapper />
                        <div className="brick_content">
                            <div className="brick_box">
                                <HomeSignalSlider />
                                <ExchangeBlock />
                                <HomeSignalInfo />
                                <HomeBenefits />
                                <ArticlesSlider/>

                                {rHTML(this.props.menu.ami.content, 'post')}
                            </div>
                        </div>
                    </div>

                    <SideRecommend />
                </div>
            </React.Fragment>
        )
    }
}

HomePage.propTypes = {
    menu: MPropTypes.observableObject
}
