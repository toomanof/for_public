import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { NotFoundPage } from 'pages'
import {
    BOOKS,
    CONTENT,
    CONTENT_PIC,
    CONTENT_ICO,
} from 'services/constants'
import { logGroupMessage } from 'services/helpers'

import {
    ContentItem,
    Loading,
    MenuBooks,
    MenuContent,
    MenuContentPic,
    MenuContentIco,
} from 'components'


/**
 * Component renders all generic menu items and content items.
 *
 * Step 1. Look in menu store and try to find menu item by url
 * If item found, render appropriate component by type.
 * 
 * Step 2. Try to find content item from Content API.
 * If item found - render it.
 * 
 * If nothing found - render 404
 *
 */
@inject('menu', 'routing', 't')
@observer
export default class MenuItemPage extends React.Component{
    componentDidMount(){
        const url = this.props.routing.location.pathname
        logGroupMessage('MenuItemPage стр.39', url)
        this.getItem(url)
    }

    componentDidUpdate(prevProps) {
        if (this.props.routing.location.pathname !== prevProps.location.pathname) {
            const url = this.props.routing.location.pathname
            this.getItem(url)
        }
    }

    getItem(url){
        const { menu } = this.props

        menu.setMenuItemPageReady(false)
        menu.setMenuItemPageNotFound(false)

        // Get menu item
        let item = menu.mapByUrl.get(url)

        if (item){
            // Load initially first 10 items,
            // so initial page will be loaded very fast
            menu.loadMenuContentItems(item.id, item.perPage, true)
            menu.setMenuItemPageReady(true)
            
            // Load all items in background
            menu.loadMenuContentItems(item.id, item.perPage, false)
        } else {
            // Get content item
            menu.loadContentItem(url)
        }
    }

    render(){
        const { menu } = this.props
        console.log(' menu item page', menu)
        if (menu.isMenuItemPageNotFound){
            return <NotFoundPage />
        }

        if (!menu.isMenuItemPageReady || !menu.ami){
            return <Loading />
        }

        if (menu.ami.type === CONTENT){
            return <MenuContent />
        }

        if (menu.ami.type === CONTENT_PIC){
            return <MenuContentPic />
        }

        if (menu.ami.type === CONTENT_ICO){
            return <MenuContentIco />
        }

        if (menu.ami.type === BOOKS){
            return <MenuBooks />
        }

        return (
            <ContentItem />
        )
    }
}


MenuItemPage.propTypes = {
    menu: MPropTypes.observableObject,
    routing: MPropTypes.observableObject,
    t: MPropTypes.observableObject,
}