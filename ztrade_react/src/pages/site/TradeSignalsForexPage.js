import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'

import {
    Breadcrumbs,
    SideWrapper,
} from 'components'


@inject('account', 'menu', 'user')
@observer
export default class TradeSignalsPage extends React.Component{
    onClickLinkFreePeriod = e =>{
        const { account } = this.props
        if( !account.freePeriod){
            document.cookie = "setFreePeriod=true"
            account.subscriptionFreePeriod()
        }
    }

    render(){
        if (!this.props.menu.ami){
            return null
        }
        const { user } = this.props
        const path_to_free_period = user.isAuthenticated ? '/account/signals': '/site/signup'
        return (
            <React.Fragment>
                <SideWrapper onlyMobile={true} />
                <div className="landpage">
                    <div className="container">
                        <Breadcrumbs />
                        <h1>{this.props.menu.ami.h1}</h1>
                        <div className="txt">
                            <h4>от опытных банковских трейдеров</h4>
                            <p>Получайте торговые сигналы на Форекс Крипто Фортс ММВБ от трейдеров с опытом
                                высокого уровня, в <a href="#">online</a> торговой панели <a href="#">Ztrade Group</a>
                            </p>
                        </div>
                        <div className="txt">
                            <Link
                                className="btn pull-left"
                                to={path_to_free_period}
                                onClick={this.onClickLinkFreePeriod}
                            >
                                Начать с бесплатного пробного периода
                            </Link>
                            <p className="pull-right"><a href="#">Узнай как это работает?</a></p>
                        </div>

                        <ul className="landpage_icons">
                            <li>
                                <div className="landpage_top">
                                    <span>Сигналы формируют:</span>
                                    Крупные участники<br/> Банки, хедж. фонды<br/> Проф. спекулянты
                                </div>
                                <div className="landpage_bottom">
                                    <img src="images/land/icon_1.svg" alt=""
                                         className="landpage_icon landpage_icon-top"/>
                                    <span className="landpage_dot"></span>
                                    <img src="images/land/icon_6.svg" alt=""
                                         className="landpage_icon landpage_icon-bottom"/>
                                </div>
                            </li>
                            <li>
                                <div className="landpage_top">
                                    <span>Уведомления:</span>
                                    Web<br/> Mobail<br/> Android
                                </div>
                                <div className="landpage_bottom">
                                    <img src="images/land/icon_2.svg" alt=""
                                         className="landpage_icon landpage_icon-top"/>
                                    <span className="landpage_dot"></span>
                                    <img src="images/land/icon_5.svg" alt=""
                                         className="landpage_icon landpage_icon-bottom"
                                    />
                                </div>
                            </li>
                            <li>
                                <div className="landpage_top">
                                    <span>Статистика сделок:</span>
                                    Swing Trading <b>75-85%</b><br/> Day Trading <b>80-90%</b><br/> Scalp
                                    Trading <b>85-95%</b>
                                </div>
                                <div className="landpage_bottom">
                                    <img src="images/land/icon_3.svg" alt=""
                                         className="landpage_icon landpage_icon-top"/>
                                    <span className="landpage_dot"></span>
                                    <img src="images/land/icon_7.svg" alt=""
                                         className="landpage_icon landpage_icon-bottom"/>
                                </div>
                            </li>
                            <li>
                                <div className="landpage_top">
                                    <span>Рынки:</span>
                                    Forex<br/> Crypto<br/> Forts
                                </div>
                                <div className="landpage_bottom">
                                    <img src="images/land/icon_8.svg" alt=""
                                         className="landpage_icon landpage_icon-top"/>
                                    <span className="landpage_dot"></span>
                                    <img src="images/land/icon_4.svg" alt=""
                                         className="landpage_icon landpage_icon-bottom"/>
                                </div>
                            </li>
                        </ul>

                        <div className="landpage_progress">
                            <div></div>
                            <div></div>
                        </div>
                        <div className="landpage_progress_2">
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                        </div>

                        <div className="phone">
                            <div className="phone_img"></div>
                            <div className="phone_button"></div>
                            <div className="phone_element"></div>
                            <div className="phone_element_tb"></div>
                            <div className="phone_element_mb"></div>
                            <div className="phone_btns">
                                <a href="#" className="phone_open"></a>
                                <a href="#" className="phone_close active"></a>
                            </div>
                        </div>

                    </div>
                </div>
            </React.Fragment>
        )
    }
}


TradeSignalsPage.propTypes = {
    account: MPropTypes.observableObject,
    menu: MPropTypes.observableObject,
    user: MPropTypes.observableObject
}