import React from 'react'
import PropTypes from 'prop-types'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { ActiveMenuItemModel } from 'models'
import { BROKER } from 'services/constants'
import {
    Breadcrumbs,
    BrokerTabAccountTypes,
    BrokerTabInfo,
    BrokerTabParams,
    BrokerTabReviews,
    BrokerTabs,
    BrokerTopBox,
    Loading,
    SideWrapper,
    SideRecommend,
    BrokerWhichAccountOpenModal
} from 'components'

@inject('broker', 'menu', 'routing', 'modal')
@observer
export default class BrokerPage extends React.Component {
    state = {
        activeType: 'info'
    }

    componentDidMount() {
        const slug = this.props.match.params.slug
        this.props.broker.loadBroker(slug, BROKER, this.onNotFound)

    }

    onNotFound = () => {
        const ami = new ActiveMenuItemModel({
            title: 'Страница не найдена',
            h1: 'Страница не найдена'
        })
        this.props.menu.setActiveMenuItem(ami)
        this.props.routing.push('/404')
    }

    onClickTab = type => {
        this.setState({ activeType: type })
    }

    render() {
        const { broker, menu, modal } = this.props
        const { activeType } = this.state

        if (!menu.ami || !broker.brokerItem) {
            return <Loading />
        }

        return (
            <React.Fragment>
                <div className="container middle broker">
                    <div className="brick">
                        <SideWrapper />
                        <div className="brick_content">
                            <div className="brick_box">
                                <Breadcrumbs />

                                <h1>{menu.ami.h1}</h1>

                                <BrokerTopBox />

                                <BrokerTabs
                                    activeType={activeType}
                                    onClickTab={this.onClickTab}
                                />
                                {activeType === 'info' && <BrokerTabInfo />}
                                {activeType === 'params' && <BrokerTabParams />}
                                {activeType === 'types' && (
                                    <BrokerTabAccountTypes />
                                )}
                                {activeType === 'reviews' && (
                                    <BrokerTabReviews />
                                )}
                            </div>
                        </div>
                    </div>
                    <BrokerWhichAccountOpenModal />
                    <SideRecommend />
                </div>
            </React.Fragment>
        )
    }
}

BrokerPage.propTypes = {
    broker: MPropTypes.observableObject,
    match: PropTypes.object,
    menu: MPropTypes.observableObject,
    routing: MPropTypes.observableObject
}
