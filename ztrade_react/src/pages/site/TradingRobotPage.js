import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'


@inject('menu')
@observer
export default class TradingRobotPage extends React.Component{
    render() {
        return (
            <React.Fragment>
                <link href="/css/robot/css/styles.css" rel="stylesheet" type="text/css" />
                <link href="/css/robot/css/resp.css" rel="stylesheet" type="text/css" />
                <link href="/css/robot/js/common.js" rel="stylesheet" type="text/css" />
                <div className="firstcreen">

                    <header className="header">
                        <div className="container">
                            <div className="logo">
                                <a href="#"><img src="images/logo.svg" alt=""/></a>
                            </div>
                            <div className="header_desc sm-hidden">
                                <div className="header_desc_name">12,5%</div>
                                <div className="header_desc_text">Получение прибыли на курсе валют с торговым роботом
                                    ZTI
                                </div>
                            </div>
                            <div className="header_buttons">
                                <a href="#indicators-form" className="btn js_scroll">Арендовать</a>
                                <a href="#" className="btn btn-white">Купить</a>
                            </div>
                        </div>
                        <div className="header_desc header_desc_mobile">
                            <div className="header_desc_name">12,5%</div>
                            <div className="header_desc_text">Получение прибыли на курсе валют с торговым роботом ZTI
                            </div>
                        </div>
                    </header>

                    <div className="container firstcreen-container">
                        <div className="firstcreen_left">
                            <div className="firstcreen_text">
                                <h1>ZTI</h1>
                                <h3>Биржевой торговый робот</h3>
                                <p>Прибыльный торговый алгоритм от опытных банковских трейдеров и программистов высокого
                                    уровня</p>
                            </div>

                            <div className="rangebox_container">
                                <div className="rangebox">
                                    <div className="rangebox_name">Период</div>
                                    <div className="rangebox_line">
                                        <input type="text" id="period-slider" name="my_range" value=""/>
                                    </div>
                                </div>

                                <div className="rangebox">
                                    <div className="rangebox_name">Размер инвестиций</div>
                                    <div className="rangebox_line">
                                        <input type="text" id="size-slider" name="my_range2" value=""/>
                                    </div>
                                </div>
                            </div>

                            <div className="firstcreen_left_footer">
                                <a href="#indicators-form" className="btn btn-orange btn-large js_scroll">Арендовать</a>
                                <div className="firstcreen_price">
                                    <span>Ваш доход:</span>
                                    $8 280
                                </div>
                            </div>
                        </div>

                        <div className="firstcreen_right">
                            <img src="images/robot.png" alt="" srcSet="images/robot-2x.png 2x"/>
                        </div>
                    </div>

                </div>


                <div className="youcan">
                    <div className="container">
                        <div className="youcan_title">С биржевым роботом <span>вы сможете</span></div>
                        <div className="youcan_content">
                            <div className="row">
                                <div className="grid-4 sm12">
                                    <div className="youcan_box">
                                        <div className="youcan_icon icon_profit"></div>
                                        <div>Получать прибыль на разнице курсов валюты</div>
                                    </div>
                                </div>
                                <div className="grid-4 sm12">
                                    <div className="youcan_box">
                                        <div className="youcan_icon icon_monitor"></div>
                                        <div>Анализировать рынок с помощью алгоритма</div>
                                    </div>
                                </div>
                                <div className="grid-4 sm12">
                                    <div className="youcan_box">
                                        <div className="youcan_icon icon_pie"></div>
                                        <div>Торговать на валютном рынке автоматически</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="container">
                    <div className="indicators">
                        <div className="indicators_form" id="indicators-form">
                            <div className="indicators_title">Получите робота в <span>АРЕНДУ!</span></div>
                            <form action="#" method="POST">
                                <div className="input-field">
                                    <input type="text" className="input" placeholder="Ваше имя"/>
                                </div>
                                <div className="input-field">
                                    <input type="text" className="input" placeholder="Фамилия"/>
                                </div>
                                <div className="input-field">
                                    <input type="text" className="input" placeholder="E-mail"/>
                                </div>
                                <div className="input-field">
                                    <div className="phone_field">
                                        <select className="select_code">
                                            <option className="rus">+7</option>
                                            <option className="ukr">+380</option>
                                            <option className="usa">+44</option>
                                        </select>
                                        <input type="text" className="input" placeholder="Телефон"/>
                                    </div>
                                </div>
                                <div className="input-field">
                                    <label className="check-field">
                                        <input type="checkbox" checked/>
                                            Установив этот флажок, я принимаю <a href="#">Условия и положения</a>,<a
                                            href="#">Политику конфиденциальности</a> и подтверждаю, что мне больше 18
                                            лет. Я согласен со сбором и обработкой моих личных данных.
                                    </label>
                                </div>
                                <div className="input-field">
                                    <a href="#rentbox" className="btn btn-orange js_scroll">Скачать</a>
                                </div>
                            </form>
                        </div>
                        <div className="indicators_content">
                            <div className="indicators_title">Показатели торгового<br/> робота <span>ZTI -</span></div>
                            <ul>
                                <li>Доходность в валюте: <b>от 2 до 12,5% в мес.</b></li>
                                <li>Торговый инструмент: <b>EUR/USD, USD/JPY, GBP/USD, USD/CHF, AUD/USD, USD/CAD</b>
                                </li>
                                <li>Рекомендуемый депозит: <b>$2000 или больше</b></li>
                                <li>Минимальный размер лота: <b>0.01</b></li>
                                <li>Кредитное плечо: <b>1:500 или больше</b></li>
                            </ul>
                        </div>
                    </div>

                    <div className="conditions">
                        <div className="pagetitle"><span>Лучшие условия</span> для торговли роботами</div>
                        <div className="row">
                            <div className="grid-3 sm6">
                                <div className="conditionbox">
                                    <div className="conditionbox_icon icn1"></div>
                                    <span className="conditionbox_name">Прибыль в месяц от 2 до 12%</span>
                                </div>
                            </div>

                            <div className="grid-3 sm6">
                                <div className="conditionbox">
                                    <div className="conditionbox_icon icn2"></div>
                                    <span className="conditionbox_name">Минимальный депозит от $500</span>
                                </div>
                            </div>

                            <div className="grid-3 sm6">
                                <div className="conditionbox">
                                    <div className="conditionbox_icon icn3"></div>
                                    <span className="conditionbox_name">Максимальная нагрузка 10%</span>
                                </div>
                            </div>

                            <div className="grid-3 sm6">
                                <div className="conditionbox">
                                    <div className="conditionbox_icon icn4"></div>
                                    <span className="conditionbox_name">Доступ к онлайн статистике</span>
                                </div>
                            </div>

                            <div className="grid-3 sm6">
                                <div className="conditionbox">
                                    <div className="conditionbox_icon icn5"></div>
                                    <span className="conditionbox_name">12 лет успешной торговли</span>
                                </div>
                            </div>
                            <div className="grid-3 sm6">
                                <div className="conditionbox">
                                    <div className="conditionbox_icon icn6"></div>
                                    <span className="conditionbox_name">Поддержка торговых алгоритмов</span>
                                </div>
                            </div>

                            <div className="grid-3 sm6">
                                <div className="conditionbox">
                                    <div className="conditionbox_icon icn7"></div>
                                    <span className="conditionbox_name">Счета в EUR, RUB, USD</span>
                                </div>
                            </div>

                            <div className="grid-3 sm6">
                                <div className="conditionbox">
                                    <div className="conditionbox_icon icn8"></div>
                                    <span className="conditionbox_name">Торговая платформа MT4 – MT5</span>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div className="rent" id="rentbox">
                        <img src="images/phone.png" alt="" srcSet="images/phone-2x.png 2x" className="rent_phone"/>
                            <img src="images/phone_mob.png" alt="" className="rent_phone mob"/>
                                <div className="pagetitle">
                                    Как получить робота в аренду
                                    <span>и начать зарабатывать -</span>
                                </div>
                                <div className="pagetitle2">3 простых шага<br/> на пути к стабильному доходу</div>

                                <div className="rent_row">
                                    <div className="row">
                                        <div className="grid-4 md12">
                                            <div className="rent_step">
                                                <div className="rent_step_text">
                                                    <div className="rent_step_num">1</div>
                                                    <div className="rent_step_entry">
                                                        <div className="rent_step_name">Открытие личного торгового
                                                            счета
                                                        </div>
                                                        <p>Счет необходим каждому, кто хочет подключить нашего биржевого
                                                            робота</p>
                                                    </div>
                                                </div>
                                                <a href="#modal1" className="btn js_modal1">Как открыть счет?</a>
                                            </div>
                                        </div>
                                        <div className="grid-4 md12">
                                            <div className="rent_step">
                                                <div className="rent_step_text">
                                                    <div className="rent_step_num">2</div>
                                                    <div className="rent_step_entry">
                                                        <div className="rent_step_name">Подтверждение данных брокерского
                                                            счета
                                                        </div>
                                                        <p>Вы зарегистрировали личный кабинет и открыли торговый
                                                            счет?</p>
                                                    </div>
                                                </div>
                                                <a href="#modal2" className="btn js_modal2">Да, подтвердить</a>
                                            </div>
                                        </div>
                                        <div className="grid-4 md12">
                                            <div className="rent_step">
                                                <div className="rent_step_text">
                                                    <div className="rent_step_num">3</div>
                                                    <div className="rent_step_entry">
                                                        <div className="rent_step_name">Запрос на подключение к
                                                            биржевому роботу
                                                        </div>
                                                        <p>Отправьте нам запрос на подключение и аренду биржевого
                                                            робота</p>
                                                    </div>
                                                </div>
                                                <a href="#modal3" className="btn js_modal3">Отправить запрос</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                    </div>

                    <div className="bottom_description">
                        <div className="bottom_description_logos">
                            <img src="images/logo.svg" alt=""/>
                                <img src="images/brands/vfsc.png" alt="" srcSet="images/brands/vfsc-2x.png 2x"/>
                                    <img src="images/brands/crofr.png" alt="" srcSet="images/brands/crofr-2x.png 2x"/>
                        </div>
                        <div className="bottom_description_entry">
                            <div className="short_text">
                                <div className="short_text_inner">
                                    HIGH RISK INVESTMENT WARNING: Trading foreign exchange on margin carries a high
                                    level of risk, can result in losses and may not be suitable for everybody. Persons
                                    below 18 years of age are not eligible to open Individual Accounts with Gatelinas,
                                    UAB. The high degree of leverage can work against you as well as for you. Before
                                    deciding to trade foreign exchange or Contracts for Difference (“CFD’s”) you should
                                    carefully consider your trading objectives, level of experience, and risk appetite.
                                    The possibility exists that you could sustain a loss of some or all of your initial
                                    trading capital and therefore you should not deposit money that you cannot afford to
                                    lose. You should be aware of all the risks associated with foreign exchange and CFD
                                    trading, and seek advice from an independent advisor if you have any doubts. Nothing
                                    on this site should be considered as an advertisement, offer or solicitation to use
                                    our services. The information contained in this site is intended for information
                                    purposes only. Therefore it should not be regarded as an offer or solicitation to
                                    any person in any jurisdiction in which such an offer or solicitation is not
                                    authorised or to any person to whom it would be unlawful to make such an offer or
                                    solicitation, nor regarded as recommendation to buy, sell or otherwise deal with any
                                    particular currency or precious metal trade. If you are not sure about your local
                                    currency and spot metals trading regulations then you should leave this site
                                    immediately. The information on this site is not directed at residents of the United
                                    States or any European Union member state and is not intended for distribution to,
                                    or use by, any person in any country or jurisdiction where such distribution or use
                                    would be contrary to local law or regulation.
                                </div>
                            </div>
                            <a href="#" className="showlink">Читать полностью</a>
                        </div>
                    </div>

                </div>

                <footer className="footer">
                    <div className="container">
                        <div className="bottom_description">
                            <div className="bottom_description_logos">&copy; Ztrade.ru 2020</div>
                            <div className="bottom_description_brands">
                                <img src="images/brands/paypal.svg" alt=""/>
                                    <img src="images/brands/mastercard.svg" alt=""/>
                                        <img src="images/brands/visa.svg" alt=""/>
                                            <img src="images/brands/bank.png" alt=""/>
                                                <img src="images/brands/skrill.svg" alt=""/>
                                                    <img src="images/brands/neteller.svg" alt=""/>
                                                        <img src="images/brands/webmoney.svg" alt=""/>
                                                            <img src="images/brands/yad.svg" alt=""/>
                                                                <img src="images/brands/qiwi.svg" alt=""/>
                            </div>
                        </div>
                    </div>
                </footer>
            </React.Fragment>
        )
    }

}

TradingRobotPage.propTypes = {
    menu: MPropTypes.observableObject
}