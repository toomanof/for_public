import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { rHTML } from 'services/helpers'
import {
    Breadcrumbs,
    BrokerRow,
    BrokerRowFeatured,
    Loading,
    SideWrapper,
    SideRecommend
} from 'components'
import {ButtonSubmit} from '../../components';

@inject('broker', 'menu')
@observer
export default class BrokersPage extends React.Component {
    state = {
        showContent: true
    }

    componentDidMount() {
        this.props.broker.setFilter(null)
        this.props.broker.loadCategories()
        this.props.broker.loadBrokers()
    }

    onClickShowAllBrokers = (e) => {
        e.preventDefault()
        this.props.broker.setAllBrokers()
        this.props.broker.loadBrokers()
        return false;
    }

    onClickFilter = (id, e) => {
        e.preventDefault()
        this.props.broker.setFilter(id)
        window.scrollTo(0, 0)
        this.setState({ showContent: false })
    }

    render() {
        const { broker, menu } = this.props

        if (!menu.ami) {
            return null
        }

        if (!broker.categories || !broker.brokers) {
            return <Loading />
        }
        let link_all_brokers;
        if (!broker.all_brokers){
            link_all_brokers = <a href=""
                                  onClick={this.onClickShowAllBrokers}
                                  className="btn btn-blue btn-big btn-center"
                                >
                                  {'Все брокеры'}
                               </a>
        }

        return (
            <React.Fragment>
                <div className="container middle">
                    <div className="brick">
                        <SideWrapper />

                        <div className="brick_content">
                            <div className="brick_box">
                                <Breadcrumbs />

                                <h1>{menu.ami.h1}</h1>

                                <div className="module module_brokers">
                                    <div className="brokers">
                                        <div className="brokers_head">
                                            <div className="brokers_cell brokers_img">
                                                {'Брокер'}
                                            </div>
                                            <div className="brokers_cell brokers_rate">
                                                {'Рейтинг'}
                                            </div>
                                            <div className="brokers_cell">
                                                {'Информация'}
                                            </div>
                                            <div className="brokers_cell brokers_spred">
                                                {'Спред'}
                                            </div>
                                        </div>

                                        <div className="brokers_active_outer">
                                            {broker.featuredBrokers.map(obj => {
                                                return (
                                                    <BrokerRowFeatured
                                                        key={obj.id}
                                                        item={obj}
                                                    />
                                                )
                                            })}
                                        </div>

                                        {broker.regularBrokers.map(obj => {
                                            return (
                                                <BrokerRow
                                                    key={obj.id}
                                                    item={obj}
                                                />
                                            )
                                        })}
                                    </div>
                                    {link_all_brokers}
                                </div>

                                <div className="categories">
                                    <div className="categories_title">
                                        {'Категории'}
                                    </div>

                                    {broker.categories.map(obj => {
                                        return (
                                            <a
                                                key={obj.id}
                                                onClick={this.onClickFilter.bind(
                                                    null,
                                                    obj.id
                                                )}
                                                href=""
                                                className="btn tag">
                                                {obj.name}
                                            </a>
                                        )
                                    })}

                                    <div className="clear" />
                                    <a
                                        href=""
                                        onClick={this.onClickFilter.bind(
                                            null,
                                            null
                                        )}
                                        className="btn btn-blue btn-big">
                                        {'Все категории'}
                                    </a>
                                </div>

                                {this.state.showContent &&
                                    rHTML(menu.ami.content, 'text')}
                            </div>
                        </div>
                    </div>

                    <SideRecommend />
                </div>
            </React.Fragment>
        )
    }
}

BrokersPage.propTypes = {
    broker: MPropTypes.observableObject,
    menu: MPropTypes.observableObject
}
