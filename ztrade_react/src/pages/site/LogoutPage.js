import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Redirect } from 'react-router-dom'


@inject('user')
@observer
export default class LogoutPage extends React.Component{
    componentDidMount(){
        this.props.user.logout()   
    }

    render(){
        return <Redirect to="/" />
    }
}


LogoutPage.propTypes = {
    user: MPropTypes.observableObject
}