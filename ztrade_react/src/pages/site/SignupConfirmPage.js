import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'
import { Redirect } from 'react-router-dom'
import {
    ButtonSubmit,
    Checkbox,
    ErrorBlock,
    Input,
    SignupChangeDataModal
} from 'components'

@inject('alert', 'menu', 'modal', 'routing', 'user')
@observer
export default class SignupConfirmPage extends React.Component {
    state = {
        emailCode: '',
        smsCode: '',
        isCheckboxChecked: false
    }

    componentDidMount() {
        document.body.classList.add('loginpage')
    }

    componentWillUnmount() {
        document.body.classList.remove('loginpage')
        this.props.user.clear()
    }

    onClickChangeData = e => {
        e.preventDefault()
        this.props.modal.openSignupChangeDataModal()
    }

    onClickResendEmailCode = e => {
        e.preventDefault()
        this.props.user.signupResendEmailCode()
    }

    onClickResendPhoneCode = e => {
        e.preventDefault()
        const form = { sms_guid: this.props.user.signupSmsGuid }
        this.props.user.signupResendPhoneCode(form)
    }

    onChangeEmailCode = value => {
        this.setState({ emailCode: value })
    }

    onChangeSmsCode = value => {
        this.setState({ smsCode: value })
    }

    onCheckboxChange = value => {
        this.setState({ isCheckboxChecked: value })
    }

    onSuccess = () => {
        this.props.alert.createAlert(
            'Успешная регистрация! На E-mail отправлено письмо с логином и паролем для входа.'
        )
        this.props.routing.push('/site/login')
    }

    onSubmit = e => {
        e.preventDefault()

        const form = {
            sms_guid: this.props.user.signupSmsGuid,
            email_code: this.state.emailCode,
            sms_code: this.state.smsCode
        }
        this.props.user.signupConfirm(form, this.onSuccess)
    }

    get isButtonEnabled() {
        if (this.state.emailCode.trim().length === 0) {
            return false
        }

        if (this.state.smsCode.trim().length === 0) {
            return false
        }

        if (!this.state.isCheckboxChecked) {
            return false
        }
        return true
    }

    render() {
        const { menu, user } = this.props

        if (!menu.ami) {
            return null
        }

        if (!user.profile || !user.isSignupStep1Completed) {
            return <Redirect to="/site/signup" />
        }

        const profile = user.profile

        return (
            <div className="signbox">
                <div className="logobox">
                    <Link to="/">
                        <img src="/images/logo.svg" alt="" />
                    </Link>
                </div>

                <Link to="/site/login" className="btn btn-blue">
                    {'Вход в личный кабинет'}
                </Link>
                <div className="clear" />

                <div className="module signform">
                    <h2>{'Подтверждение данных'}</h2>
                    <div className="userdata">
                        <p>{profile.fullName}</p>
                        <p>{profile.email}</p>
                        <p className="last">{profile.phone}</p>
                        <a
                            onClick={this.onClickChangeData}
                            href="/"
                            className="userdata_edit">
                            {'Изменить'}
                        </a>
                    </div>

                    <form onSubmit={this.onSubmit}>
                        <p>
                            {
                                'На указанные почту и телефон были отправлены коды подтверждения.'
                            }
                        </p>
                        <p>
                            {
                                'Если не получили коды, проверьте папку спам.'
                            }
                        </p>
                        <p>{'Пожалуйста, введите полученные коды.'}</p>

                        <div className="inpf">
                            <Input
                                name="email_code"
                                onChange={this.onChangeEmailCode}
                                placeholder="Код подтверждения e-mail"
                                value={this.state.emailCode}
                                errors={user.errors}
                            />

                            <Input
                                name="sms_code"
                                onChange={this.onChangeSmsCode}
                                placeholder="Код подтверждения телефона"
                                value={this.state.smsCode}
                                errors={user.errors}
                            />
                        </div>

                        <label className="checkf">
                            <Checkbox
                                hasLabel={false}
                                onChange={this.onCheckboxChange}
                            />

                            {'Я принимаю условия '}
                            <Link to="/client_agreement">
                                {'Клиентского соглашения'}
                            </Link>
                            {' и '}
                            <Link to="/client_agreement">
                                {'Политику конфиденциальности'}
                            </Link>
                            {', подтверждаю, что мне есть 18 лет. \n Я согласен со сбором и обработкой моих личных данных.'}


                        </label>

                        <ErrorBlock errors={user.errors} />

                        <ButtonSubmit
                            isButtonEnabled={this.isButtonEnabled}
                            text="Подтвердить"
                        />

                        <small>
                            {
                                'Не получили код? Проверьте правильность ввода данных. '
                            }
                            {'Если Телефон или E-mail указаны неверно — '}
                            <a onClick={this.onClickChangeData} href="">
                                {'Измените данные'}
                            </a>

                            {'. Если данные указаны верно — Вы можете '}
                            <a onClick={this.onClickResendPhoneCode} href="/">
                                {'Повторно запросить SMS-код'}
                            </a>

                            {' или '}

                            <a onClick={this.onClickResendEmailCode} href="/">
                                {'Повторно запросить E-mail'}
                            </a>

                            {'.'}
                        </small>
                    </form>
                </div>

                {this.props.modal.isSignupChangeDataModalOpened && (
                    <SignupChangeDataModal />
                )}
            </div>
        )
    }
}

SignupConfirmPage.propTypes = {
    alert: MPropTypes.observableObject,
    menu: MPropTypes.observableObject,
    modal: MPropTypes.observableObject,
    routing: MPropTypes.observableObject,
    user: MPropTypes.observableObject
}
