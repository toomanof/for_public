import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'
import { formToObject } from 'services/helpers'
import { ButtonSubmit, Input } from 'components'
import { EMAIL, SMS } from 'services/constants'

@inject('alert', 'menu', 'routing', 'user')
@observer
export default class ResetPasswordPage extends React.Component {
    state = {
        type: null
    }

    componentDidMount() {
        document.body.classList.add('loginpage2')
    }

    componentWillUnmount() {
        document.body.classList.remove('loginpage2')
        this.props.user.clear()
    }

    onSelectType = type => {
        console.log('======onSelectType', type)
        this.setState({ type: type })
    }

    onClickSendCode = e => {
        e.preventDefault()
        const { type } = this.state
        const { user } = this.props

        user.clear()

        if (!type) {
            return
        }

        const form = formToObject(this.formRef.current)
        if (!this.validateForm(form)) {
            return
        }

        const d = {
            type: type,
            uid_email: form.uid_email
        }
        user.resetPasswordSendCode(d, this.onSuccessSendCode)
    }

    onSuccessSendCode = () => {
        this.props.alert.createAlert('Код отправлен')
    }

    onSuccess = () => {
        this.props.alert.createAlert('Пароль успешно установлен')
        this.props.routing.push('/site/login')
    }

    onSubmit = e => {
        e.preventDefault()
        const { user } = this.props
        const form = formToObject(e.target)
        user.resetPassword(form, this.onSuccess)
    }

    get isButtonEnabled() {
        return true
    }

    validateForm(form) {
        const { user } = this.props

        if (form.uid_email.trim().length === 0) {
            user.setErrors({ uid_email: ['Это поле не может быть пустым'] })
            return false
        }

        if (form.password.trim().length === 0) {
            user.setErrors({ password: ['Это поле не может быть пустым'] })
            return false
        }

        if (form.password.trim().length < 8) {
            user.setErrors({
                password: ['Минимальная длина пароля 8 символов']
            })
            return false
        }

        if (form.password2.trim().length === 0) {
            user.setErrors({ password2: ['Это поле не может быть пустым'] })
            return false
        }

        if (form.password.trim() !== form.password2.trim()) {
            user.setErrors({ password2: ['Пароль не совпадает'] })
            return false
        }

        return true
    }

    render() {
        const { menu, user } = this.props

        if (!menu.ami) {
            return null
        }

        this.formRef = React.createRef()

        return (
            <div className="signbox">
                <div className="logobox">
                    <Link to="/">
                        <img src="/images/logo.svg" alt="" />
                    </Link>
                </div>

                <Link to="/site/login" className="btn btn-blue">
                    {'Вход в личный кабинет'}
                </Link>
                <div className="clear" />

                <div className="module signform">
                    <h2>{'Востановление пароля'}</h2>

                    <form ref={this.formRef} onSubmit={this.onSubmit}>
                        <Input
                            name="uid_email"
                            onChange={this.onChangeUidEmail}
                            value={this.state.uidEmail}
                            placeholder="ID номер личного кабинета / E-mail"
                            errors={user.errors}
                        />

                        <Input
                            wrapperClassName="inpf inpf_q"
                            name="password"
                            type="password"
                            placeholder="Новый пароль"
                            tooltip="Введите новый пароль"
                            errors={user.errors}
                        />

                        <Input
                            name="password2"
                            type="password"
                            placeholder="Подтвердите пароль"
                            errors={user.errors}
                        />

                        <div className="logText marg20">
                            <p>
                                {
                                    'Для продолжения операции необходимо получить код подтверждения'
                                }
                            </p>
                        </div>

                        <div className="approve">
                            <div className="approve_title">
                                {'Способ получения кода подтверждения:'}
                            </div>

                            <div className="approve_left">
                                <label className="checkf">
                                    <input
                                        onClick={this.onSelectType.bind(
                                            null,
                                            SMS
                                        )}
                                        type="radio"
                                        name="sms"
                                        className="radio"
                                    />
                                    {'SMS'}
                                </label>
                                <label className="checkf">
                                    <input
                                        onClick={this.onSelectType.bind(
                                            null,
                                            EMAIL
                                        )}
                                        type="radio"
                                        name="sms"
                                        className="radio"
                                    />
                                    {'E-mail'}
                                </label>
                            </div>

                            <div className="approve_rt">
                                <a
                                    onClick={this.onClickSendCode}
                                    href=""
                                    className="btn btn-blue">
                                    {'Получить код'}
                                </a>
                                <div className="quest">
                                    <div className="tooltip">
                                        {'Выберите способ получения кода'}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <Input
                            name="code"
                            placeholder="Код подтверждения"
                            errors={user.errors}
                        />

                        <ButtonSubmit
                            isButtonEnabled={this.isButtonEnabled}
                            text="Изменить пароль"
                        />
                    </form>
                </div>
            </div>
        )
    }
}

ResetPasswordPage.propTypes = {
    alert: MPropTypes.observableObject,
    menu: MPropTypes.observableObject,
    routing: MPropTypes.observableObject,
    user: MPropTypes.observableObject
}
