import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { SideWrapper, Breadcrumbs, SideRecommend } from 'components'


@inject('menu')
@observer
export default class PaymentSuccessProduct extends React.Component{
    render(){
        const { menu } = this.props

        return (
            <React.Fragment>
                <div className="container middle">
                    <div className="brick">
                        <SideWrapper />

                        <div className="brick_content">
                            <div className="brick_box">
                                <Breadcrumbs />

                                {/* <h1>{menu.ami.h1}</h1> */}
                                
                                <div className="module page">
                                
                                    <p>Благодарим за приобретение программы.
                                        На E-mail  который вы указывали при регистрации, отправлена вся информация касаемо данного заказа и ссылка для скачивания программы.
                                    </p>
                                    <p>
                                        <b>Не пришло письмо?</b> Посмотрите его в папке спам!
                                    </p>

                                </div>
                            </div>
                        </div>
                    </div>

                    <SideRecommend />

                </div>
            </React.Fragment>
        )
    }
}


PaymentSuccessProduct.propTypes = {
    menu: MPropTypes.observableObject
}