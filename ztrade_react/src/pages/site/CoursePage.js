import React from 'react'
import PropTypes from 'prop-types'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { ActiveMenuItemModel } from 'models'
import { COURSE } from 'services/constants'

import {
    Breadcrumbs,
    ContentCourse,
    Loading,
    SideWrapper,
    SideRecommend,
    SideBarCourse
} from 'components'

@inject('course', 'menu')
@observer
export default class CoursePage extends React.Component {

    constructor(props) {
        super(props)
        const { course } = this.props
        this.course = course
        this.course.loadAnalysis()
        this.course.loadMarkets()
        this.course.loadFreeCourses()
        this.course.loadPaidCourses()
    }

    render() {
        const { broker, menu } = this.props

        if (!menu.ami) {
            return <Loading />
        }

        return (
            <React.Fragment>
                <div className="container middle">
                    <div className="brick">
                        <SideWrapper />

                        <div className="brick_content">
                            <div className="brick_box">
                                <Breadcrumbs />

                                <h1>{menu.ami.h1}</h1>

                                <div className="module studybanner">
                                    <div className="studybanner_inner">
                                        <div className="studybanner_title">Система обучения и торговля</div>
                                        <div className="studybanner_name">Опционный анализ рынков, анализ объема и
                                            биржевых отчетов CME Group, торговля на валютных и фондовых рынках.
                                        </div>
                                        <div className="studybanner_group">CME Group</div>
                                    </div>
                                </div>

                                <div id="study_wrapper" className="bricksm">
                                    <SideBarCourse />
                                    <ContentCourse />
                                </div>
                            </div>
                        </div>
                    </div>

                    <SideRecommend />
                </div>
            </React.Fragment>
        )
    }
}

CoursePage.propTypes = {
    menu: MPropTypes.observableObject,
    course: MPropTypes.observableObject,
}
