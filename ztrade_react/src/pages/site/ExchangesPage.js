import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { rHTML } from 'services/helpers'
import {
    Breadcrumbs,
    ExchangeRow,
    ExchangeRowFeatured,
    Loading,
    SideWrapper,
    SideRecommend
} from 'components'
import {ButtonSubmit} from '../../components';

@inject('exchange', 'menu')
@observer
export default class ExchangesPage extends React.Component {
    state = {
        showContent: true
    }

    componentDidMount() {
        this.props.exchange.setFilter(null)
        this.props.exchange.loadCategories()
        this.props.exchange.loadExchanges()
    }

    onClickFilter = (id, e) => {
        e.preventDefault()
        this.props.exchange.setFilter(id)
        window.scrollTo(0, 0)
        this.setState({ showContent: false })
    }

    render() {
        const { exchange, menu } = this.props

        if (!menu.ami) {
            return null
        }

        if (!exchange) {
            return <Loading />
        }

        return (
            <React.Fragment>
                <div className="container middle">
                    <div className="brick">
                        <SideWrapper />

                        <div className="brick_content">
                            <div className="brick_box">
                                <Breadcrumbs />

                                <h1>{menu.ami.h1}</h1>

                                <div className="module module_brokers">
                                    <div className="brokers crypts">
                                        <div className="brokers_head">
                                            <div className="brokers_cell brokers_img">{'Биржа'}</div>
                                            <div className="brokers_cell brokers_rate">{'Объем торгов'}</div>
                                            <div className="brokers_cell brokers_crypt">{'Криптовалюты'}</div>
                                            <div className="brokers_cell brokers_int">{'Интерфейс'}</div>
                                            <div className="brokers_cell brokers_fee">{'Комиссия'}</div>
                                        </div>

                                        <div className="brokers_active_outer">
                                            {exchange.featuredExchanges.map(obj => {
                                                return (
                                                    <ExchangeRowFeatured
                                                        key={obj.id}
                                                        item={obj}
                                                    />
                                                )
                                            })}
                                        </div>
                                        {exchange.regularExchanges.map(obj => {
                                            return (
                                                <ExchangeRow
                                                    key={obj.id}
                                                    item={obj}
                                                />
                                            )
                                        })}
                                    </div>

                                </div>

                                <div className="categories">
                                    <div className="categories_title">
                                        {'Категории'}
                                    </div>

                                    {exchange.categories.map(obj => {
                                        return (
                                            <a
                                                key={obj.id}
                                                onClick={this.onClickFilter.bind(
                                                    null,
                                                    obj.id
                                                )}
                                                href=""
                                                className="btn tag">
                                                {obj.name}
                                            </a>
                                        )
                                    })}

                                    <div className="clear" />
                                    <a
                                        href=""
                                        onClick={this.onClickFilter.bind(
                                            null,
                                            null
                                        )}
                                        className="btn btn-blue btn-big">
                                        {'Все категории'}
                                    </a>
                                </div>

                                {this.state.showContent &&
                                    rHTML(menu.ami.content, 'text')}
                            </div>
                        </div>
                    </div>

                    <SideRecommend />
                </div>
            </React.Fragment>
        )
    }
}

ExchangesPage.propTypes = {
    exchange: MPropTypes.observableObject,
    menu: MPropTypes.observableObject
}
