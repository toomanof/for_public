import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'
import { SideWrapper, Breadcrumbs, SideRecommend } from 'components'


@inject('menu')
@observer
export default class PaymentSuccessSubscription extends React.Component{
    render(){
        const { menu } = this.props

        return (
            <React.Fragment>
                <div className="container middle">
                    <div className="brick">
                        <SideWrapper />

                        <div className="brick_content">
                            <div className="brick_box">
                                <Breadcrumbs />

                                {/* <h1>{menu.ami.h1}</h1> */}
                                <h1>{'Подписка'}</h1>
                                
                                <div className="module page">
                                
                                    <p>Вы успешно оформили подписку на инструменты, индикатор опционных уровней, шаблоны опционных уровней.</p>
                                    <p>Скачать индикатор опционных уровней, шаблоны опционных уровней, можно в личном кабинете во вкладке
                                        <Link to="/account/tools">{'«Инструменты»'}</Link>
                                    </p>
                                    <p>
                                        <Link to="/account/home">{'Перейти в личный кабинет'}</Link>
                                    </p>

                                </div>
                            </div>
                        </div>
                    </div>

                    <SideRecommend />

                </div>
            </React.Fragment>
        )
    }
}


PaymentSuccessSubscription.propTypes = {
    menu: MPropTypes.observableObject
}