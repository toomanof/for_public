import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'
import { formToObject } from 'services/helpers'
import { ErrorBlock, Input } from 'components'

@inject('menu', 'routing', 'user')
@observer
export default class LoginPage extends React.Component {
    componentDidMount() {
        document.body.classList.add('loginpage2')
    }

    componentWillUnmount() {
        document.body.classList.remove('loginpage2')
        this.props.user.clear()
    }

    onSuccess = () => {
        this.props.routing.push('/account/home')
    }

    onSubmit = e => {
        e.preventDefault()

        const form = formToObject(e.target)
        this.props.user.login(form, this.onSuccess)
    }

    render() {
        console.log('login page', this.props)
        const { menu, user } = this.props

/*        if (!menu.ami) {
            return null
        }
*/
        return (
            <div className="signbox signbox_mob">
                <div className="logobox">
                    <Link to="/">
                        <img
                            src="/images/logo.svg"
                            alt=""
                        />
                    </Link>
                </div>

                <Link to="/site/signup" className="btn btn-blue">
                    {'Зарегистрировать личный кабинет'}
                </Link>
                <div className="clear" />

                <div className="module signform">
                    <h2>{'Вход в Личный кабинет'}</h2>
                    <form onSubmit={this.onSubmit}>
                        <div className="inpf inpf_q">
                            <Input
                                name="uid_email"
                                placeholder="ID номер личного кабинета / E-mail"
                                errors={user.errors}
                                hasWrapper={false}
                            />

                            <div className="quest">
                                <div className="tooltip">
                                    {
                                        'Введите номер Вашего Личного кабинета или E-mail, которые указывали при регистрации'
                                    }
                                </div>
                            </div>
                        </div>

                        <Input
                            name="password"
                            type="password"
                            placeholder="Пароль"
                            errors={user.errors}
                        />

                        <ErrorBlock errors={user.errors} />

                        <input
                            type="submit"
                            className="btn btn-orange btn-big"
                            value="Войти"
                        />

                        <div className="logText_p">
                            <p>
                                <Link to="/site/reset_password">
                                    {'Восстановить пароль'}
                                </Link>
                            </p>
                            <p>
                                <Link to="/site/login">
                                    {'Войти в Личный кабинет партнера'}
                                </Link>
                            </p>
                        </div>

                        <div className="logText">
                            <div className="logText_title">
                                {'Защитите Ваши личные данные'}
                            </div>
                            <ul>
                                <li>
                                    {
                                        'Не сообщайте никому пароль от Вашего Личного кабинета, в том числе сотрудникам ZTrade, это конфиденциальная информация'
                                    }
                                </li>
                                <li>
                                    {
                                        'Сотрудники ZTrade никогда не попросят Вас сообщить пароль и код подтверждения, высланный на Ваш телефон или email. Если кто-либо пытается получить у Вас такую информацию, напишите нам.'
                                    }
                                </li>
                                <li>
                                    {
                                        'Рекомендуем менять пароли не реже чем раз в шесть месяцев. Повторное использование одинаковых паролей запрещено в целях безопасности Ваших личных данных.'
                                    }
                                </li>
                                <li>
                                    {
                                        'Не храните в своем почтовом ящике следующую информацию: данные паспорта, сканы документов, пароли от Личного кабинета'
                                    }
                                </li>
                            </ul>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}

LoginPage.propTypes = {
    menu: MPropTypes.observableObject,
    routing: MPropTypes.observableObject,
    user: MPropTypes.observableObject
}
