import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'
import { Breadcrumbs, Loading, SideWrapper } from 'components'
import { rHTML } from 'services/helpers'


@inject('menu')
@observer
export default class AnalyticPage extends React.Component{
    componentDidMount(){
        const { menu } = this.props
        if (!menu.pageAnalytic){
            menu.loadPageAnalytic()
        }
    }

    render(){
        const { menu } = this.props

        if (!menu.pageAnalytic){
            return <Loading />
        }

        return (
            <React.Fragment>
                <div className="container middle">
                    <Breadcrumbs />
                    <h1>{menu.ami.h1}</h1>

                    <div className="row booksrow">
                        {menu.pageAnalytic.map(obj => {
                            return (
                                <div key={obj.id} className="grid3 md6 sm12">
                                    <div className="module bookshort">
                                        <div className="bookshort_img">
                                            <img
                                                src={obj.icoUrl}
                                                alt=""
                                                srcSet={`${obj.icoSrcsetUrl} 2x`} />
                                        </div>
                                        
                                        {rHTML(obj.name, 'bookshort_name')}
                                        {rHTML(obj.shortDescription, 'bookshort_text2')}
                                        
                                        <Link to={obj.url} className="btn">
                                            {'Подробнее'}
                                        </Link>
                                    </div>
                                </div>
                            )
                        })}
                    </div>
                </div>

                <SideWrapper isDesktopHidden />

            </React.Fragment>
        )
    }
}


AnalyticPage.propTypes = {
    menu: MPropTypes.observableObject
}