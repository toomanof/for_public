import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { 
    EconomicCalendar,
    ForexNews,
    ForexNewsFilter,
    MostPopular,
    PublishMaterials,
    Loading,
} from 'components'

@inject('menu')
@observer
export default class AccountHomePage extends React.Component{
    componentDidMount() {
        const { menu } = this.props
        menu.loadRecommendLenta()
    }

    render(){
        const { menu } = this.props
        let first_recomend = null
        let populars = null

        if (!menu.ami){
            return <Loading />
        }

        if(menu.recommend_lenta_page){
            first_recomend = menu.recommend_lenta_page[0]
            populars = menu.recommend_lenta_page.slice(1,)
        }

        return (
            <div className="wrap">
                <h1>{menu.ami.h1}</h1>
                
                <div className="clearfix">
                    <div className="col_left">
                        <ForexNewsFilter />
                    </div>

                    <div className="col_mid">
                        <div className="row">
                            <ForexNews />

                            <div className="grid5 md12">
                                {first_recomend &&(
                                <div className="module text-center">
                                    <div className="module_content">
                                        <a
                                            rel="noopener"
                                            target="_blank"
                                            href={first_recomend.url}>
                                            <img
                                                src={first_recomend.picUrl}
                                                alt=""
                                                srcSet={`${first_recomend.picSrcsetUrl} 2x`} />
                                        </a>
                                    </div>
                                </div>)
                                }

                                {/* <EconomicCalendar /> */}
                                { (populars && populars.length > 0) &&
                                  (<MostPopular populars={populars}/>)
                                }

                                <PublishMaterials  />

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


AccountHomePage.propTypes = {
    menu: MPropTypes.observableObject
}
