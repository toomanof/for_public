import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import {
    Input,
    Select,
    LoadingPage,
    Checkbox,
    ButtonSubmit,
    ErrorBlock
} from 'components'
import { CountryModel } from 'models'
import { formToObject, validateEmail } from 'services/helpers'

@inject('user', 'general')
@observer
export default class AccountProfilePage extends React.Component {
    constructor(props) {
        super(props)
        this.changePasswordFormRef = React.createRef()
        this.changeInfoFormRef = React.createRef()
        this.phoneInputRef = React.createRef()
    }

    state = {
        phoneGroupOpened: false,
        emailGroupOpened: false,
        infoData: {},
        email: '',
        emailCode: '',
        countryId: null,
        phonePrefix: '',
        phone: '',
        smsCode: ''
    }

    componentDidMount() {
        const {
            isReceiveEmail,
            isReceiveNews,
            isReceiveNotifications,
            city,
            country
        } = this.props.user.profile
        this.props.general.loadCountries()
        const infoData = {
            is_receive_email: isReceiveEmail,
            is_receive_news: isReceiveNews,
            is_receive_notifications: isReceiveNotifications,
            country_id: country ? country.id: 0,
            city
        }
        this.setState({ infoData: infoData })
    }

    changePhoneGroupVisibility = e => {
        e.preventDefault()
        this.setState({ phoneGroupOpened: !this.state.phoneGroupOpened })
    }

    changeEmailGroupVisibility = e => {
        e.preventDefault()
        this.setState({ emailGroupOpened: !this.state.emailGroupOpened })
    }

    changePassword = e => {
        const { user } = this.props
        const data = formToObject(this.changePasswordFormRef.current)
        user.changePassword(data, () =>
            this.changePasswordFormRef.current.reset()
        )
    }

    changeInfo = e => {
        const { user } = this.props
        const { infoData } = this.state
        user.changeProfile(infoData)
    }

    setInfoDataValue = (key, value) => {
        let { infoData } = this.state
        infoData = { ...infoData }
        infoData[key] = value
        this.setState({ infoData: infoData })
    }

    onChangeEmail = email => {
        this.props.user.clear()
        this.setState({ email, emailCode: '' })
    }

    onChangeEmailCode = emailCode => {
        this.setState({ emailCode })
    }

    sendEmailCode = () => {
        const { email } = this.state
        const data = { email }
        this.props.user.sendEmailCode(data)
    }

    changeEmail = () => {
        const { email, emailCode } = this.state
        const data = { email, email_code: emailCode }
        this.props.user.changeEmail(data, () => {
            this.setState({ email: '', emailCode: '', emailGroupOpened: false })
        })
    }

    sendSMSCode = () => {
        const { phone, phonePrefix } = this.state
        const data = { phone: phonePrefix.replace('-', '') + phone }
        this.props.user.sendSMSCode(data)
    }

    onChangeCountry = value => {
        if (value === '') {
            this.setState({
                countryId: null,
                phonePrefix: null,
                phone: ''
            })
        } else {
            value = parseInt(value, 10)

            const c = CountryModel.getById(value)
            this.setState({
                countryId: value,
                phonePrefix: c.phonePrefix,
                phone: ''
            })
        }
        this.setState({ smsCode: '' })
        this.props.user.clear()
        this.phoneInputRef.current.focus()
    }

    onChangePhone = value => {
        this.setState({ phone: value, smsCode: '' })
        this.props.user.clear()
    }

    onChangeSmsCode = value => {
        this.setState({ smsCode: value })
    }

    changePhone = () => {
        const { phone, phonePrefix, smsCode } = this.state
        const data = {
            phone: phonePrefix.replace('-', '') + phone,
            sms_code: smsCode
        }
        this.props.user.changePhone(data, () => {
            this.setState({
                countryId: null,
                phone: '',
                phonePrefix: '',
                smsCode: '',
                phoneGroupOpened: false
            })
        })
    }

    renderPhoneGroup() {
        const { phone, phonePrefix, smsCode } = this.state
        const { user } = this.props
        const prefix = user.profile.country ? user.profile.country.phone_prefix.replace('-', ''): ''
        return (
            <div className="module_content module_content2">
                <div className="fieldout">
                    <div className="fieldout_name fieldout_name2">
                        {'Номер телефона'}
                    </div>
                    <div className="fieldout_input">
                        <div className="num_total">
                            <span>
                                <b>{'Код страны:'}</b> {prefix}{' '}
                                {'(' + !user.profile.country ? '': user.profile.country.name + ')'}
                            </span>
                            <span>
                                <b>{'Номер:'}</b>{' '}
                                {user.profile.phone && user.profile.phone
                                    .replace(prefix, '')
                                    .split(' ')
                                    .join('')}
                            </span>
                            <a
                                href="#"
                                className="userdata_edit"
                                onClick={this.changePhoneGroupVisibility}>
                                {'Изменить'}
                            </a>
                        </div>

                        {this.state.phoneGroupOpened && (
                            <div className="row num_total_edit">
                                <div className="grid2 md4 xs12">
                                    <Select
                                        name="country_id"
                                        options={CountryModel.selectOptions}
                                        errors={user.errors}
                                        value={this.state.countryId}
                                        onChange={this.onChangeCountry}
                                    />
                                </div>
                                <div className="grid2 md8 xs12">
                                    <Input
                                        cref={this.phoneInputRef}
                                        type="phone"
                                        name="phone"
                                        placeholder="Телефон"
                                        errors={user.errors}
                                        value={this.state.phone}
                                        prefix={this.state.phonePrefix}
                                        onChange={this.onChangePhone}
                                    />
                                </div>
                                {phone && phonePrefix && (
                                    <div className="grid4 md6 sm12">
                                        <div className="partnerForm">
                                            <div className="register_form">
                                                <Input
                                                    type="text"
                                                    name="sms_code"
                                                    className="reginp"
                                                    placeholder="Код подтверждения"
                                                    value={smsCode}
                                                    disabled={!user.changePhoneSMSGuid}
                                                    onChange={
                                                        this.onChangeSmsCode
                                                    }
                                                />
                                                <div className="partnerForm_btn">
                                                    <ButtonSubmit
                                                        type="button"
                                                        className="btn btn-sm"
                                                        isButtonEnabled
                                                        text="Получить код"
                                                        onClick={
                                                            this.sendSMSCode
                                                        }
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                )}
                                {phone &&
                                    phonePrefix &&
                                    smsCode &&
                                    user.changePhoneSMSGuid && (
                                        <div className="grid2 md3 sm6">
                                            <ButtonSubmit
                                                type="button"
                                                className="btn btn-blue btn-big"
                                                text="Сохранить"
                                                isButtonEnabled={
                                                    !user.isChangePhoneInProgress
                                                }
                                                onClick={this.changePhone}
                                            />
                                        </div>
                                    )}
                                <div className="grid2 md3 sm6">
                                    <input
                                        type="button"
                                        className="btn btn-big"
                                        value="Отмена"
                                        onClick={
                                            this.changePhoneGroupVisibility
                                        }
                                    />
                                </div>
                            </div>
                        )}
                    </div>
                </div>
            </div>
        )
    }

    renderEmailGroup() {
        const { user } = this.props
        return (
            <div className="module_content">
                <div className="fieldout">
                    <div className="fieldout_name fieldout_name2">
                        {'E-mail'}
                    </div>
                    <div className="fieldout_input">
                        <div className="num_total">
                            <span>
                                <b>{'E-mail:'}</b> {user.profile.email}
                            </span>
                            <a
                                href="#"
                                className="userdata_edit"
                                onClick={this.changeEmailGroupVisibility}>
                                {'Изменить'}
                            </a>
                        </div>

                        {this.state.emailGroupOpened && (
                            <div className="row num_total_edit">
                                <div className="grid4 md12">
                                    <Input
                                        name="email"
                                        type="text"
                                        className="styler"
                                        placeholder="E-mail"
                                        errors={user.errors}
                                        value={this.state.email}
                                        onChange={this.onChangeEmail}
                                    />
                                </div>
                                {validateEmail(this.state.email) && (
                                    <div className="grid4 md6 sm12">
                                        <div className="partnerForm">
                                            <div className="register_form">
                                                <Input
                                                    name="email_code"
                                                    errors={user.errors}
                                                    className="reginp"
                                                    placeholder="Код подтверждения"
                                                    value={this.state.emailCode}
                                                    disabled={
                                                        !user.isEmailCodeSent
                                                    }
                                                    onChange={
                                                        this.onChangeEmailCode
                                                    }
                                                />
                                                <div className="partnerForm_btn">
                                                    <ButtonSubmit
                                                        type="button"
                                                        className="btn btn-sm"
                                                        text="Получить код"
                                                        isButtonEnabled={
                                                            !user.isSendEmailCodeInProgress
                                                        }
                                                        onClick={
                                                            this.sendEmailCode
                                                        }
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                )}
                                {validateEmail(this.state.email) &&
                                    this.state.emailCode &&
                                    user.isEmailCodeSent && (
                                        <div className="grid2 md3 sm6">
                                            <ButtonSubmit
                                                type="button"
                                                className="btn btn-blue btn-big"
                                                onClick={this.changeEmail}
                                                isButtonEnabled={
                                                    !user.isChangeEmailInProgress
                                                }
                                                text="Сохранить"
                                            />
                                        </div>
                                    )}
                                <div className="grid2 md3 sm6">
                                    <input
                                        type="button"
                                        className="btn btn-big"
                                        value="Отмена"
                                        onClick={
                                            this.changeEmailGroupVisibility
                                        }
                                    />
                                </div>
                            </div>
                        )}
                    </div>
                </div>
            </div>
        )
    }

    renderInfoGroup() {
        const { user } = this.props
        const { infoData } = this.state
        return (
            <React.Fragment>
                <div className="module">
                    <div className="module_title2">{'Данные профиля'}</div>
                    <div className="module_content">
                        <form
                            className="data_form"
                            ref={this.changeInfoFormRef}>
                            <div className="fieldout">
                                <div className="fieldout_name">
                                    <label>{'Ваше имя:'}</label>
                                </div>
                                <div className="fieldout_input">
                                    <input
                                        type="text"
                                        className="styler"
                                        placeholder={user.profile.firstName}
                                        disabled
                                    />
                                </div>
                            </div>
                            <div className="fieldout">
                                <div className="fieldout_name">
                                    <label>{'Фамилия:'}</label>
                                </div>
                                <div className="fieldout_input">
                                    <input
                                        type="text"
                                        className="styler"
                                        placeholder={user.profile.lastName}
                                        disabled
                                    />
                                </div>
                            </div>
                            <div className="fieldout">
                                <div className="fieldout_name">
                                    <label>{'Страна:'}</label>
                                </div>
                                <div className="fieldout_input">
                                    <Select
                                        name="country_id"
                                        options={CountryModel.selectOptions}
                                        wrapperClassName=""
                                        errors={user.errors}
                                        value={infoData['country_id']}
                                        onChange={v =>
                                            this.setInfoDataValue(
                                                'country_id',
                                                v
                                            )
                                        }
                                    />
                                </div>
                            </div>
                            <div className="fieldout">
                                <div className="fieldout_name">
                                    <label>{'Город:'}</label>
                                </div>
                                <div className="fieldout_input">
                                    <input
                                        type="text"
                                        className="styler"
                                        onChange={e =>
                                            this.setInfoDataValue(
                                                'city',
                                                e.target.value
                                            )
                                        }
                                        value={infoData.city}
                                    />
                                </div>
                            </div>
                            <div className="fieldout">
                                <div className="fieldout_name">
                                    <label>{'ID пользователя:'}</label>
                                </div>
                                <div className="fieldout_input">
                                    <input
                                        type="text"
                                        className="styler"
                                        placeholder={user.profile.uid}
                                        disabled
                                    />
                                </div>
                            </div>
                            <div className="fieldout_checks">
                                <div>
                                    <Checkbox
                                        name="is_receive_news"
                                        label="Получать новости"
                                        onChange={v =>
                                            this.setInfoDataValue(
                                                'is_receive_news',
                                                v
                                            )
                                        }
                                        isChecked={infoData['is_receive_news']}
                                        isControllable
                                    />
                                </div>
                                <div>
                                    <Checkbox
                                        name="is_recieve_notifications"
                                        label="Получать уведомления"
                                        onChange={v =>
                                            this.setInfoDataValue(
                                                'is_receive_notifications',
                                                v
                                            )
                                        }
                                        isChecked={
                                            infoData['is_receive_notifications']
                                        }
                                        isControllable
                                    />
                                </div>
                                <div>
                                    <Checkbox
                                        name="is_receive_email"
                                        label="Получать общие почтовые сообщения"
                                        onChange={v =>
                                            this.setInfoDataValue(
                                                'is_receive_email',
                                                v
                                            )
                                        }
                                        isChecked={infoData['is_receive_email']}
                                        isControllable
                                    />
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div className="data_form_btn">
                    <div className="row">
                        <div className="grid6">
                            <ButtonSubmit
                                className="btn btn-big btn-blue"
                                text="Сохранить"
                                onClick={this.changeInfo}
                                isButtonEnabled={
                                    !user.isChangeProfileInProgress
                                }
                            />
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }

    renderChangePassword() {
        const { user } = this.props

        return (
            <React.Fragment>
                <div className="module">
                    <div className="module_title2">{'Смена пароля'}</div>
                    <div className="module_content">
                        <form
                            className="data_form"
                            ref={this.changePasswordFormRef}>
                            <div className="fieldout">
                                <div className="fieldout_name">
                                    <label>{'Старый пароль:'}</label>
                                </div>
                                <div className="fieldout_input">
                                    <Input
                                        type="password"
                                        name="old_password"
                                        className="styler"
                                        errors={user.errors}
                                    />
                                </div>
                            </div>
                            <div className="fieldout">
                                <div className="fieldout_name">
                                    <label>{'Новый пароль:'}</label>
                                </div>
                                <div className="fieldout_input">
                                    <Input
                                        type="password"
                                        name="new_password"
                                        className="styler"
                                        errors={user.errors}
                                    />
                                </div>
                            </div>
                            <div className="fieldout">
                                <div className="fieldout_name">
                                    <label>{'Пожалуйста, подтвердите:'}</label>
                                </div>
                                <div className="fieldout_input">
                                    <Input
                                        type="password"
                                        name="new_password2"
                                        className="styler"
                                        errors={user.errors}
                                    />
                                </div>
                            </div>
                            <ErrorBlock errors={user.errors} />
                        </form>
                    </div>
                </div>
                <div className="data_form_btn">
                    <div className="row">
                        <div className="grid6">
                            <ButtonSubmit
                                className="btn btn-big btn-blue"
                                text="Сохранить"
                                onClick={this.changePassword}
                                isButtonEnabled={
                                    !user.isChangePasswordInProgress
                                }
                            />
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }

    render() {
        const { general } = this.props
        if (!general.countries || !this.state.infoData) {
            return <LoadingPage />
        }
        return (
            <div className="wrap">
                <h1>{'Личные данные'}</h1>
                <div className="module module_num">
                    {this.renderPhoneGroup()}
                    {this.renderEmailGroup()}
                </div>
                {this.renderInfoGroup()}
                {this.renderChangePassword()}
            </div>
        )
    }
}

AccountProfilePage.propTypes = {
    general: MPropTypes.observableObject,
    user: MPropTypes.observableObject
}
