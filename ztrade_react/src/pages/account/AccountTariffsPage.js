import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Loading, Modal, TariffBox } from 'components'

@inject('routing', 'sgn', 'user')
@observer
export default class AccountTariffsPage extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            isModalOpened: false,
        }
    }

    componentDidMount() {
        const { sgn, match } = this.props
        const code = match.params.code

        sgn.loadMarkets()
        sgn.loadTariffs(code)
    }

    onRouteChanged() {
        const { match, sgn } = this.props
        const code = match.params.code
        sgn.loadTariffs(code)
    }

    onChangeMarket = (e, options) => {
        console.log(options, options[1].value, e.target.value, options[1].value === e.target.value)
        const active_option = options.find(item => item.value === e.target.value)
        if(active_option.active) {
            const url = e.target.value
            this.props.routing.push(url)
        } else {
            this.setState({ isModalOpened: true })
        }

    }

    onClickMarket = market => e => {
        e.preventDefault()
        if(market.active){
            this.onRouteChanged()
        } else {
            this.setState({ isModalOpened: true })
        }
    }

    onClickCloseModal = () => {
        this.setState({ isModalOpened: false })
    }

    render() {
        const { sgn, match, user } = this.props
        if (!sgn.marketMap || !sgn.tariffMap) {
            return <Loading />
        }
        const code = match.params.code
        const first_name = (!user.profile.firstName) ?'': user.profile.firstName.charAt(0).toUpperCase() + user.profile.firstName.slice(1)
        const tariffs = sgn.tariffMap.get(code)
        const market = sgn.marketMap.get(code)
        let options = []

        Array.from(sgn.marketMap.values()).map(obj => {
            options.push({value:obj.tariffsUrl, active:obj.active})
        })

        if (!tariffs || !market) {
            return <Loading />
        }

        return (
            <React.Fragment >
                <div className="wrap">
                    <h1>{'Тарифы'}</h1>

                    <ul className="bigtabs hidden-sm">
                        {Array.from(sgn.marketMap.values()).map(obj => {
                            const cn = obj.code === code ? 'active' : ''

                            return (
                                <li key={obj.id} className={cn}>
                                    <a
                                        href={obj.tariffsUrl}
                                        onClick={this.onClickMarket(obj)}
                                    >{obj.name}</a>
                                </li>
                            )
                        })}
                    </ul>

                    <div className="seltabs seltabs_bigtabs visible-sm">
                        <select
                            defaultValue={market.tariffsUrl}
                            onChange={e => this.onChangeMarket(e, options)}
                            className="selectbox">
                            {Array.from(sgn.marketMap.values()).map(obj => {
                                return (
                                    <option key={obj.id} value={obj.tariffsUrl}>
                                        {obj.name}
                                    </option>
                                )
                            })}
                        </select>
                    </div>

                    {/*<div className="learInfo">
                        <span>{'Инфо'}</span>
                        </div>*/}

                    <div className="row tarifrow">
                        {tariffs.map(obj => {
                            return <TariffBox key={obj.id} obj={obj} />
                        })}
                    </div>

                    {market.tariffFooter && (
                        <div className="border_block">
                            <div className="quote">{market.tariffFooter}</div>
                        </div>
                    )}
                </div>

                {this.state.isModalOpened && (
                    <Modal
                        className="modal simple_modal"
                        onClickClose={this.onClickCloseModal}
                        isClickOutsideCloseModal={false}
                    >
                        <React.Fragment>
                                <div className="modal_header">Уважаемый (ая), {first_name}</div>
                                <div className="modal_content">
                                    <p><b>Данный рынок не доступен!</b> Мы работаем действительно над чем-то полезным,
                                        и уже скоро можно будет воспользоваться сервисом.</p>
                                </div>
                                <a href="#" className="modal_close" data-fancybox-close=""></a>
                        </React.Fragment>
                    </Modal>
                )}
            </React.Fragment >
        )
    }
}

AccountTariffsPage.propTypes = {
    match: PropTypes.object,
    routing: MPropTypes.observableObject,
    sgn: MPropTypes.observableObject,
    user: MPropTypes.observableObject,
}
