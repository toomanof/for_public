import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'


@inject('menu')
@observer
export default class AccountCalendarPage extends React.Component{
    render(){
        return (
            <div className="wrap">
                {'Календарь'}
            </div>
        )
    }
}


AccountCalendarPage.propTypes = {
    menu: MPropTypes.observableObject
}