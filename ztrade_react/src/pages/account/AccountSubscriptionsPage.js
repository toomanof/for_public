import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Loading, SubscriptionBlock, AccountSubScripActiveModal1, AccountSubScripActiveModal2 } from 'components'


@inject('account')
@observer
export default class AccountSubscriptionsPage extends React.Component {
    componentDidMount() {
        this.props.account.loadSubscriptions()
    }

    render() {
        const { account } = this.props

        if (!account.subscriptions) {
            return <Loading />
        }
        return (
            <div className="wrap">
                {account.subscriptions.map((obj, index) => {
                    return (
                        <SubscriptionBlock
                            key={obj.id}
                            obj={obj}
                            num={index + 1}
                        />
                    )
                })}
                <AccountSubScripActiveModal1 subsciption={account.subscriptions && account.subscriptions[0]}/>
                <AccountSubScripActiveModal2 subsciption={account.subscriptions && account.subscriptions[0]}/>
            </div>
        )
    }
}

AccountSubscriptionsPage.propTypes = {
    account: MPropTypes.observableObject
}
