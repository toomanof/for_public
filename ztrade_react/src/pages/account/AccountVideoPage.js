import React from 'react'
import PropTypes from 'prop-types'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'
import qs from 'qs'
import { Loading, Player } from 'components'

/**
 * Videos from subscriptions
 */
@inject('account', 'menu', 'routing')
@observer
export default class AccountVideoPage extends React.Component {
    state = {
        featuredVideo: null
    }

    componentDidMount() {
        this.props.account.loadSubscriptions()
    }

    onClickVideo = (obj, e) => {
        e.preventDefault()
        this.setState({ featuredVideo: obj })
    }

    onClickSubscription = (obj, e) => {
        e.preventDefault()
        this.props.routing.push(obj.videosUrl)
    }

    get h1() {
        if (this.activeSubscription) {
            return `Видео обзоры - ${this.activeSubscription.name}`
        }
        return 'Видео обзоры'
    }

    /**
     * Returns id from query params or null
     */
    get subscriptionId() {
        const parsed = qs.parse(this.props.location.search.slice(1))

        const id = parsed.subscription_id
        if (id) {
            return parseInt(id, 10)
        }
        return null
    }

    /**
     * Returns subscription from query param or null
     */
    get activeSubscription() {
        const { account } = this.props
        for (let obj of account.paidSubscriptions) {
            if (obj.id === this.subscriptionId) {
                return obj
            }
        }
        return null
    }

    /**
     * Returns videos filtered by query param
     */
    get filteredVideos() {
        const { account } = this.props

        let arr = [...account.paidSubscriptions]
        let videos = []

        if (this.activeSubscription) {
            arr = arr.filter(obj => obj.id === this.activeSubscription.id)
        }

        for (let obj of arr) {
            for (let video of obj.paidVideos) {
                videos.push(video)
            }
        }
        return videos
    }

    /**
     * Returns feturedVideo from state or first video
     */
    get featuredVideo() {
        const { featuredVideo } = this.state
        if (featuredVideo) {
            return featuredVideo
        }

        if (this.filteredVideos.length > 0) {
            return this.filteredVideos[0]
        }

        return null
    }

    renderTop() {
        const { account } = this.props

        return (
            <div className="module learnvid">
                <div className="module_content">
                    {this.featuredVideo && (
                        <React.Fragment>
                            <div className="learnvid_title">
                                {this.featuredVideo.name}
                            </div>

                            <div className="ytbox_video">
                                <Player
                                    height={270}
                                    fileUrl={this.featuredVideo.file_url}
                                    link={this.featuredVideo.link}
                                />
                            </div>
                        </React.Fragment>
                    )}

                    <div className="learnvid_tags">
                        <h4>{'Подписки'}</h4>
                        <ul>
                            {account.paidSubscriptions.map(obj => {
                                return (
                                    <li key={obj.id}>
                                        <a
                                            href="#"
                                            onClick={this.onClickSubscription.bind(
                                                null,
                                                obj
                                            )}>
                                            {obj.name}
                                        </a>
                                    </li>
                                )
                            })}

                            {account.paidSubscriptions.length > 1 && (
                                <li>
                                    <Link to="/account/video">
                                        {'Выбрать все'}
                                    </Link>
                                </li>
                            )}
                        </ul>
                    </div>
                </div>
            </div>
        )
    }

    renderVideos() {
        return (
            <div className="row">
                {this.filteredVideos.map(obj => {
                    return (
                        <div key={obj.id} className="grid20 tb6 xs12">
                            <div className="ytbox ytbox-small">
                                <div className="ytbox_video">
                                    <Player
                                        fileUrl={obj.file_url}
                                        link={obj.link}
                                    />
                                </div>
                                <div className="ytbox_name">
                                    <a
                                        onClick={this.onClickVideo.bind(
                                            null,
                                            obj
                                        )}
                                        href="#">
                                        {obj.name}
                                    </a>
                                </div>
                            </div>
                        </div>
                    )
                })}
            </div>
        )
    }

    render() {
        const { account } = this.props

        if (!account.subscriptions) {
            return <Loading />
        }

        return (
            <div className="wrap">
                <h1>{this.h1}</h1>
                {this.renderTop()}
                {this.renderVideos()}
            </div>
        )
    }
}

AccountVideoPage.propTypes = {
    account: MPropTypes.observableObject,
    location: PropTypes.object,
    menu: MPropTypes.observableObject,
    routing: MPropTypes.observableObject
}
