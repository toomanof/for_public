import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { SgnMarket, SgnMarketMenu, Loading } from 'components'

@inject('menu', 'sgn')
@observer
export default class AccountSignalsPage extends React.Component {
    componentDidMount() {
        const { sgn, menu } = this.props
        sgn.loadMarketsExt()
    }

    componentWillUnmount() {
        this.props.sgn.clear()
    }

    render() {
        const { sgn } = this.props

        if (!sgn.marketMap || !sgn.activeMarket) {
            return <Loading />
        }

        return (
            <React.Fragment>
                <div className="wrap">
                    <SgnMarketMenu />
                    <SgnMarket />
                </div>
            </React.Fragment>
        )
    }
}

AccountSignalsPage.propTypes = {
    menu: MPropTypes.observableObject,
    sgn: MPropTypes.observableObject
}
