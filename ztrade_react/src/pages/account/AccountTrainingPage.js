import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'

import {
    StrategiesTab,
    TutorialBookTab,
    VideoLessonsTab,
    VideosTab,
    Tabs2
} from 'components'


/**
 * Set tabs on mount, then use them from store.
 */
@inject('menu', 'tabs')
@observer
export default class AccountTrainingPage extends React.Component{
    componentDidMount(){
        this.props.tabs.setTabs(this.tabs, 0)   
    }

    get tabs(){
        return [
            {name: 'Торговые стратегии', className: ''},
            {name: 'Обучающие видео', className: ''},
            {name: 'Видеоуроки', className: ''},
            {name: 'Учебник', className: ''},
        ]
    }

    render(){
        const { tabs } = this.props

        if (tabs.arr.length === 0){
            return null
        }

        return (
            <div className="wrap">
                <h1>{tabs.activeTab && tabs.activeTab.name}</h1>

                <Tabs2>
                    <StrategiesTab />
                    <VideosTab />
                    <VideoLessonsTab />
                    <TutorialBookTab />
                </Tabs2>
            </div>
        )
    }
}


AccountTrainingPage.propTypes = {
    menu: MPropTypes.observableObject,
    tabs: MPropTypes.observableObject
}