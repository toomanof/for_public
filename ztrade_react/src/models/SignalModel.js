import React from 'react'
import moment from 'moment'
import Model from './private/Model'

import { BUY } from 'services/constants'
import { DAILY } from 'services/constants'
import { MEDIUM } from 'services/constants'

export default class SignalModel extends Model {
    get publishAt() {
        if (this.isPaid) {
            return this.publishAtPaid
        }
        return this.publishAtFree
    }

    /**
     * Returns price using precision
     */
    get priceStr() {
        if (!this.price) {
            return ''
        }
        return parseFloat(this.price).toFixed(this.symbol.prec)
    }

    /**
     * Returns take profit using precision
     */
    get tpStr() {
        if (!this.tp) {
            return ''
        }
        return parseFloat(this.tp).toFixed(this.symbol.prec)
    }

    /**
     * Returns stop loss using precision
     */
    get slStr() {
        if (!this.sl) {
            return ''
        }
        return parseFloat(this.sl).toFixed(this.symbol.prec)
    }

    get tradeTypeClassName() {
        if (this.tradeType === BUY) {
            if (this.signalType.code === MEDIUM) {
                return 'up up3'
            }

            if (this.signalType.code === DAILY) {
                return 'up up2'
            }

            return 'up'
        }

        if (this.signalType.code === MEDIUM) {
            return 'dn dn3'
        }

        if (this.signalType.code === DAILY) {
            return 'dn dn2'
        }

        return 'dn'
    }

    /**
     * Used for filtering
     */
    get isVisible() {
        if (this.isPaid) {
            return true
        }

        // Not paid, but match publishAt
        if (moment.utc() > this.publishAt) {
            return true
        }

        return false
    }

    /**
     * row in countdown mode
     */
    get isCountdown() {
        if (this.isPaid && moment.utc() < this.publishAt) {
            return true
        }
        return false
    }

    /**
     * row in active mode (trade not closed yet)
     */
    get isActive() {
        if (this.isClosed) {
            return false
        }

        // Paid users
        if (this.isPaid) {
            return true
        }

        // Free users
        if (moment.utc() > this.publishAt) {
            return true
        }

        return false
    }

    /**
     * Result that is showed in "ID" column
     */
    get colID() {
        return this.id
    }

    /**
     * Result that is showed in "symbol" column
     */
    get colSymbol() {
        return this.symbol.name
    }

    get symbolCode() {
        return this.symbol.code
    }

    /**
     * Result that is showed in "trade type" column
     */
    get colTradeType() {
        if (this.isCountdown) {
            return ''
        }
        const tradeType =
            this.tradeType.slice(0, 1).toUpperCase() +
            this.tradeType.slice(1, this.tradeType.length + 1)
        return <span className={this.tradeTypeClassName}>{tradeType}</span>
    }

    /**
     * Result that is showed in "order type" column
     */
    get colOrderType() {
        if (this.isCountdown) {
            return ''
        }
        return (
            this.orderType.slice(0, 1).toUpperCase() +
            this.orderType.slice(1, this.orderType.length + 1)
        )
    }

    /**
     * Result that is showed in "price" column
     */
    get colPrice() {
        if (this.isCountdown) {
            return ''
        }
        return this.priceStr
    }

    /**
     * Result that is showed in "take profit" column
     */
    get colTp() {
        if (this.isCountdown) {
            return ''
        }
        return this.tpStr
    }

    /**
     * Result that is showed in "stop loss" column
     */
    get colSl() {
        if (this.isCountdown) {
            return ''
        }
        return this.slStr
    }

    /**
     * Result that is showed in "result" column
     */
    get colResult() {
        if (this.isCountdown) {
            const now = moment.utc()
            let ms = this.publishAt.diff(now)
            let d = moment.duration(ms)

            if (d.asSeconds() > 3600) {
                return moment.utc(d.asMilliseconds()).format('HH:mm:ss')
            }

            return moment.utc(d.asMilliseconds()).format('mm:ss')
        }

        if (this.isActive) {
            return ''
        }
        return this.result
    }

    /**
     * Additional class name for result column
     */
    get colResultClassName() {
        if (this.isCountdown || this.isActive) {
            return ''
        }
        if (this.result >= 0) {
            return 'signalcl-green'
        }
        return 'signalcl-red'
    }
}
