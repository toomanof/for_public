import Model from './private/Model'
import { CONTENT_PIC, CONTENT_ICO } from 'services/constants'


export default class MenuModel extends Model{
    get perPage(){
        if (this.type === CONTENT_PIC){
            return 3
        } else if (this.type === CONTENT_ICO){
            return 5
        }
        return 10
    }

}

