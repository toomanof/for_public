import Model from './private/Model'
import {
    ADVANTAGE,
    DISADVANTAGE,
    POSITIVE,
    NEUTRAL,
    NEGATIVE
} from 'services/constants'

export default class ExchangeModel extends Model{
    /**
     * Logo that is showed on Brokers List page
     * If broker featured, show white logo.
     */
    get advantages(){
        return this.descs.filter(obj => obj.type === ADVANTAGE)
    }

    get disadvantages(){
        return this.descs.filter(obj => obj.type === DISADVANTAGE)
    }

    get allReviews(){
        let arr = []
        for (let obj of this.reviews){
            arr.push(new Model(obj))
        }
        return arr
    }

    get positiveReviews(){
        return this.allReviews.filter(obj => obj.type === POSITIVE)
    }

    get neutralReviews(){
        return this.allReviews.filter(obj => obj.type === NEUTRAL)
    }

    get negativeReviews(){
        return this.allReviews.filter(obj => obj.type === NEGATIVE)
    }

    get countReviews(){
        return this.countPositiveReviews + this.countNeutralReviews + this.countNegativeReviews
    }

    get logoListUrl(){
        if (this.isFeatured){
            return this.logoWhiteUrl
        }
        return this.logoUrl
    }

    get logoListSrcsetUrl() {
        if (this.isFeatured) {
            return this.logoWhiteSrcsetUrl
        }
        return this.logoSrcsetUrl
    }
}