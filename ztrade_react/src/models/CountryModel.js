import { getGeneralStore } from 'stores'
import Model from './private/Model'


export default class CountryModel extends Model{
    static get selectOptions(){
        const gstore = getGeneralStore()

        let arr = []
        arr.push({value: '', text: '--- Выбрать страну'})
        for (let obj of gstore.countries){
            arr.push({value: obj.id, text: obj.name})
        }
        return arr
    }

    static getById(id){
        const gstore = getGeneralStore()

        for (let obj of gstore.countries){
            if (obj.id === id){
                return obj
            }
        }
        return null    
    }
}