import {
    ADVANTAGE,
    DISADVANTAGE,
    POSITIVE,
    NEUTRAL,
    NEGATIVE
} from 'services/constants'

import Model from './private/Model'

export default class BrokerModel extends Model{
    get advantages(){
        return this.descs.filter(obj => obj.type === ADVANTAGE)
    }

    get disadvantages(){
        return this.descs.filter(obj => obj.type === DISADVANTAGE)   
    }

    get allReviews(){
        let arr = []
        for (let obj of this.reviews){
            arr.push(new Model(obj))
        }
        return arr
    }

    get positiveReviews(){
        return this.allReviews.filter(obj => obj.type === POSITIVE)
    }

    get neutralReviews(){
        return this.allReviews.filter(obj => obj.type === NEUTRAL)
    }

    get negativeReviews(){
        return this.allReviews.filter(obj => obj.type === NEGATIVE)
    }

    get countReviews(){
        return this.countPositiveReviews + this.countNeutralReviews + this.countNegativeReviews
    }

    get hasPamms(){
        if (this.pamms.length > 0){
            return true
        }
        return false
    }

    /**
     * Logo that is showed on Brokers List page
     * If broker featured, show white logo.
     */
    get logoListUrl(){
        if (this.isFeatured){
            return this.logoWhiteUrl
        }
        return this.logoUrl
    }

    get logoListSrcsetUrl(){
        if (this.isFeatured){
            return this.logoWhiteSrcsetUrl
        }
        return this.logoSrcsetUrl
    }

}