import moment from 'moment'
import Model from './private/Model'

export default class SubscriptionModel extends Model{
    get updateDateMoment(){
        return moment(this.updateDate)
    }

    get dateFromMoment(){
        return moment(this.dateFrom)
    }

    get dateToMoment(){
        return moment(this.dateTo)
    }

    get updateDateStr(){
        return this.updateDateMoment.format('DD.MM.YYYY')
    }

    get dateFromStr(){
        return this.dateFromMoment.format('DD.MM.YYYY')
    }

    get dateToStr(){
        return this.dateToMoment.format('DD.MM.YYYY')
    }

    get price1Number(){
        return parseInt(this.price1, 10)
    }

    get price3Number(){
        return parseInt(this.price3, 10)
    }

    get price6Number(){
        return parseInt(this.price6, 10)
    }

    get videosUrl(){
        return `/account/video?subscription_id=${this.id}`
    }

}