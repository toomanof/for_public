import Model from './private/Model'

export default class MarketModel extends Model {
    get tariffsUrl() {
        return `/account/tariffs/${this.code}`
    }
}
