import Model from './private/Model'

export default class StrategyModel extends Model{
    get timeFrameIds(){
        if (this.timeFrames && this.timeFrames.length > 0){
            return this.timeFrames.map(obj => obj.id)
        }
    }

    get indicatorIds(){
        if (this.indicators && this.indicators.length > 0){
            return this.indicators.map(obj => obj.id)
        }
    }
}