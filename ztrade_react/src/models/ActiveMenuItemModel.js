import Model from './private/Model'


/**
 * Required fields for every ActiveMenuItem:
 * h1
 * title
 * breadcrumbs
 *
 * All other fields are related to item itself
 */
export default class ActiveMenuItemModel extends Model{
}