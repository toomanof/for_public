import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import {Link} from 'react-router-dom';

import {CODE_BANNER_FIRST_BANNER_IN_RIGHT_COL_MAIN_PAGE} from 'api'

@inject('t', 'general')
@observer
export default class SideBanner extends React.Component{
    render(){
        let banner = this.props.general.getBanner(CODE_BANNER_FIRST_BANNER_IN_RIGHT_COL_MAIN_PAGE)

        return (
            <div className="module text-center hidden-tb">
                <div className="module_content">
                    { banner &&
                        (<Link to={banner.url}>
                            <img
                                src={banner.image}
                                alt=""
                                srcSet={banner.imageBig + " 2x"}/>
                        </Link>)
                    }
                </div>
            </div>
        )
    }
}


SideBanner.propTypes = {
    t: MPropTypes.observableObject,
    general: MPropTypes.observableObject,
}