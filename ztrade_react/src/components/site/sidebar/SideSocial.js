import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import {
    FacebookShareButton,
    GooglePlusShareButton,
    OKShareButton,
    TelegramShareButton,
    VKShareButton
} from 'share'

@inject('menu')
@observer
export default class SideSocial extends React.Component {
    render() {
        const { menu } = this.props

        if (!menu.ami) {
            return null
        }

        const shareUrl = window.location.toString()
        const title = menu.ami.title

        return (
            <div className="module hidden-tb">
                <div className="module_content">
                    <TelegramShareButton
                        url={shareUrl}
                        title={title}
                        className="social_but social_te"
                    />

                    <VKShareButton
                        url={shareUrl}
                        className="social_but social_vk"
                    />

                    <OKShareButton
                        url={shareUrl}
                        className="social_but social_ok"
                    />

                    <FacebookShareButton
                        url={shareUrl}
                        quote={title}
                        className="social_but social_fb"
                    />

                    <a href="#" className="social_but social_add" />
                </div>
            </div>
        )
    }
}

SideSocial.propTypes = {
    menu: MPropTypes.observableObject
}
