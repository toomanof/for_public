import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'


/**
 * Menu item may have class name "item"
 * In top menu it is rendered by class name
 * But in mobile menu it rendered vice versa
 * If item has class name "item", then no class
 * If item do not have class name "item", then class name is "item"
 */

@inject('menu', 'modal', 't')
@observer
export default class SideMobileMenu extends React.Component{
    onClickSupport = (e) => {
        e.preventDefault()
        this.props.modal.openSupportModal()
    }

    render(){
        return (
            <div className="module module_nav">
                <div className="module_title">{'Меню'}</div>
                <ul className="module_menu">
                    {this.props.menu.menuTop.map(obj => {
                        
                        let cn = obj.className === 'item' ? '' : 'item'
                        
                        return (
                            <li key={obj.id} className={cn}>
                                {obj.url === '/site/support' ? 
                                    <a onClick={this.onClickSupport} href="">
                                        {obj.name}
                                    </a>  
                                : 
                                    <Link to={obj.url}>{obj.name}</Link>
                                }
                            </li>
                        )
                    })}
                </ul>
            </div>
        )
    }
}


SideMobileMenu.propTypes = {
    menu: MPropTypes.observableObject,
    modal: MPropTypes.observableObject,
    t: MPropTypes.observableObject
}