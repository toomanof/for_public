import React from 'react'
import PropTypes from 'prop-types'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'

/**
 * Component is rendered in 2 places.
 * 1) In sidebar
 * 2) In the bottom of the page
 *
 */
@inject('menu')
@observer
export default class SideRecommend extends React.Component {
    static get defaultProps() {
        return {
            type: 'bottom'
        }
    }

    render() {
        const { menu } = this.props

        let cn = 'module module_recomend hidden-tb'
        if (this.props.type === 'bottom') {
            cn = 'module module_recomend hidden-lg visible-tb'
        }

        return (
            <div className={cn}>
                <div className="module_title">{'Рекомендую'}</div>
                <div className="module_content">
                    {menu.recommend.map(obj => {
                        return (
                            <a
                                rel="noopener"
                                target="_blank"
                                key={obj.id}
                                href={obj.url}>
                                <img
                                    src={obj.picUrl}
                                    alt=""
                                    srcSet={`${obj.picSrcsetUrl} 2x`}
                                />
                            </a>
                        )
                    })}
                </div>
            </div>
        )
    }
}

SideRecommend.propTypes = {
    menu: MPropTypes.observableObject,
    type: PropTypes.oneOf(['bottom', 'side'])
}
