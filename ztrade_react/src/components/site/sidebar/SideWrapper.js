import React from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import classNames from 'classnames'
import { logMessage, isMobile } from 'services/helpers'

import {
    SideBanner,
    SideMenuLinks,
    SideMobileMenu,
    SideRazdel,
    SideRecommend,
    SideSearch,
    SideSocial
} from 'components'



@inject('menu', 't')
@observer
export default class SideWrapper extends React.Component{

    static get defaultProps() {
        return {
            isDesktopHidden: false,
        }
    }

    constructor(props){
        super(props)
        this.asideRef = React.createRef()
    }

    componentDidMount(){
        document.addEventListener('click', this.handleClick)

        const $ = window.$
        if ($('div').is('#foottrig')) {
            var height_body = $('#foottrig').offset().top
        }
        if ( document.documentElement.clientWidth < 1020 ) {
            $('.mobnav').height(height_body)
        }
    }

    componentWillUnmount(){
        document.removeEventListener('click', this.handleClick)
        this.props.menu.closeMobileMenu()
    }

    handleClick = (e) => {
        if (!this.props.menu.isMobileMenuOpened){
            return
        }

        const area = ReactDOM.findDOMNode(this.asideRef.current)
        logMessage('SideWrapper handle click')

        // Click on "showmenu" button should not be handled
        if (e.target.classList.contains('showmenu')){
            return
        }

        if (e.target.classList.contains('showmenu_sm')){
            return
        }

        if (area && !area.contains(e.target)) {
            this.props.menu.closeMobileMenu()    
        }
    }

    onClickCloseMenu = (e) => {
        e.preventDefault()
        this.props.menu.closeMobileMenu()
    }
   
    render(){
        const { isDesktopHidden, menu } = this.props
        let showSide =  this.props.onlyMobile ? isMobile() !== null: true
        console.log('showSide:', showSide, 'isMobile', isMobile() !== null)
        const cn = classNames(
            'aside mobnav',
            {'hidden-lg': isDesktopHidden, opened: menu.isMobileMenuOpened}
        )

        return (
            <React.Fragment>
                <aside ref={this.asideRef} className={cn}>
                    <SideMobileMenu />
                    {showSide && <SideSearch /> }
                    {showSide && <SideSocial /> }
                    {showSide && <SideBanner /> }
                    {showSide && <SideMenuLinks /> }
                    {showSide && <SideRazdel /> }
                    {showSide && <SideRecommend type="side" /> }
                    <a
                        onClick={this.onClickCloseMenu}
                        href=""
                        className="mobnav_close" />
                </aside>
                {menu.isMobileMenuOpened &&
                    <div className="mobnav_overlay" style={{display: 'block'}} />}                
            </React.Fragment>
        )
    }
}


SideWrapper.propTypes = {
    isDesktopHidden: PropTypes.bool,
    menu: MPropTypes.observableObject,
    t: MPropTypes.observableObject
}
