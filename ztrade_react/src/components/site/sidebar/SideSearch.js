import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'

@inject('t')
@observer
export default class SideSearch extends React.Component {
    render() {
        return (
            <div className="module module_search">
                <div className="module_title">{'Искать на сайте'}</div>
                <div className="module_content">
                    <div className="search">
                        <form>
                            <input
                                type="text"
                                name="s"
                                className="search_inp"
                                placeholder="Введите запрос"
                            />
                            <input type="submit" className="search_but" />
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

SideSearch.propTypes = {
    t: MPropTypes.observableObject
}
