import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'


@inject('menu', 't')
@observer
export default class SideMenuLinks extends React.Component{
    render(){
        return (
            <div className="module">
                <div className="module_title">{'Разделы'}</div>
                <ul className="module_menu">
                    {this.props.menu.menuRightLink.map(obj => {
                        return (
                            <li key={obj.id}>
                                <Link to={obj.url}>{obj.name}</Link>
                            </li>
                        )
                    })}
                </ul>
            </div>
        )
    }
}


SideMenuLinks.propTypes = {
    menu: MPropTypes.observableObject,
    t: MPropTypes.observableObject
}