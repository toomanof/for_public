import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'


/**
 * onChange that passed to props, sends isChecked state and contextObj
 * back to parent.
 *
 * Checkbox2 can be controllable (get isChecked from props and update local state)
 * or not controllable (using local state only).
 *
 * contextObj - any object with any data
 */
export default class Checkbox2 extends React.Component{
    static get defaultProps() {
        return {
            contextObj: {},
            isControllable: false,
            isChecked: false,
            onChange: () => {}
        }
    }

    static getDerivedStateFromProps(nextProps, prevState){
        const { isControllable } = nextProps
        if (isControllable && (nextProps.isChecked !== prevState.isChecked)){
            return {...prevState, isChecked: nextProps.isChecked}
        }
        return null
    }

    state = {
        isChecked: this.props.isChecked
    }

    onClick = () => {
        const newValue = !this.state.isChecked
        this.setState({isChecked: newValue})
        this.props.onChange(newValue, {...this.props.contextObj})
    }

    render(){
        const cn = classNames('jq-checkbox alertchange', {checked: this.state.isChecked})

        return (
            <div
                onClick={this.onClick}
                className={cn}
                unselectable="on"
                style={{display: 'inline-block', position: 'relative', overflow: 'hidden'}}>

                <div className="jq-checkbox__div" />
            </div>

        )
    }
}


Checkbox2.propTypes = {
    contextObj: PropTypes.object,
    isChecked: PropTypes.bool,
    isControllable: PropTypes.bool,
    onChange: PropTypes.func
}
