import React from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'
import classNames from 'classnames'

export default class Select extends React.Component {
    static get defaultProps() {
        return {
            values: []
        }
    }

    constructor(props) {
        super(props)
        this.componentRef = React.createRef()
    }

    state = { isOpened: false }

    componentDidMount() {
        document.addEventListener('click', this.handleClick)
    }

    componentWillUnmount() {
        document.removeEventListener('click', this.handleClick)
    }

    handleClick = e => {
        const area = this.componentRef.current

        if (area && !area.contains(e.target)) {
            if (this.state.isOpened) {
                this.setState({ isOpened: false })
            }
        }
    }

    changeIsOpened = () => {
        this.setState({ isOpened: !this.state.isOpened })
    }

    onChange = value => {
        const { onChange, values } = this.props
        let newValues = [...values]
        newValues.includes(value)
            ? (newValues = newValues.filter(v => v !== value))
            : newValues.push(value)
        if (this.props.onChange) {
            this.props.onChange(newValues)
        }
    }

    render() {
        const { isOpened } = this.state
        const { options, values } = this.props
        const cnSelect = classNames('mselect', { mselect__opened: isOpened })

        return (
            <div className={cnSelect} ref={this.componentRef}>
                <div className="mselect__top-row" onClick={this.changeIsOpened}>
                    <p>{'Валюты'}</p>
                    <span>{values.length}</span>
                </div>
                <ul className="mselect__items">
                    {options.map(option => {
                        const cnItem = classNames('mselect__item', {
                            'mselect__item--active': values.includes(
                                option.value
                            )
                        })
                        return (
                            <li
                                className={cnItem}
                                key={option.value}
                                onClick={() => this.onChange(option.value)}>
                                {option.text}
                            </li>
                        )
                    })}
                </ul>
            </div>
        )
    }
}

Select.propTypes = {
    onChange: PropTypes.func,
    options: PropTypes.arrayOf(
        PropTypes.shape({
            value: PropTypes.any,
            text: PropTypes.any
        })
    ).isRequired,
    values: PropTypes.array
}
