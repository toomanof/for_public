import React from 'react'
import PropTypes from 'prop-types'


/**
 * Simple select.
 * Default value can be passed via props: "value"
 * isValueFromParent controls component either from parent
 * or internally.
 */
export default class Select extends React.Component{
    static get defaultProps() {
        return {
            className: 'selectbox',
            hasWrapper: true,
            isValueFromParent: false,
            wrapperClassName: 'inpf'
        }
    }

    
    state = {
        value: this.props.value || this.props.options[0].value
    }
    

    onChange = (e) => {
        this.setState({value: e.target.value})
        if (this.props.onChange){
            this.props.onChange(e.target.value)
        }
    }

    get hasError(){
        const { errors, name } = this.props
        if (errors && errors.hasOwnProperty(name)){
            return true
        }
        return false
    }

    renderErrors(){
        const { errors, name } = this.props
        if (errors && errors.hasOwnProperty(name)){
            return (
                <React.Fragment>
                    {errors[name].map((message, index) => {
                        return (
                            <div key={index} className="error_text">
                                {message}
                            </div>
                        )
                    })}    
                </React.Fragment>
            )
        }
        return null
    }

    render(){
        const {
            hasWrapper,
            isValueFromParent,
            name,
            options,
            wrapperClassName } = this.props

        const value = isValueFromParent ? this.props.value : this.state.value

        let { className } = this.props
        if (this.hasError){
            className += ' error'
        }

        if (hasWrapper){
            return (
                <div className={wrapperClassName}>
                    <select
                        name={name}
                        value={value}
                        onChange={this.onChange}
                        className={className}>
                        
                        {options.map((obj, index) => {
                            return (
                                <option
                                    key={index}
                                    value={obj.value}>

                                    {obj.text}
                                </option>        
                            )
                        })}
                    </select>
                    {this.renderErrors()}
                </div>
            )
            
        }

        return (
            <select
                name={name}
                value={value}
                onChange={this.onChange}
                className={className}>
                
                {options.map((obj, index) => {
                    return (
                        <option
                            key={index}
                            value={obj.value}>

                            {obj.text}
                        </option>        
                    )
                })}
            </select>
        )
    }
}

Select.propTypes = {
    className: PropTypes.string,
    errors: PropTypes.object,
    hasWrapper: PropTypes.bool,
    isValueFromParent: PropTypes.bool,
    name: PropTypes.string.isRequired,
    onChange: PropTypes.func,
    options: PropTypes.arrayOf(
        PropTypes.shape({
            value: PropTypes.any,
            text: PropTypes.any
        })
    ).isRequired,
    value: PropTypes.any,
    wrapperClassName: PropTypes.string
}
