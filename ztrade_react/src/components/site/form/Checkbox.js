import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'


/**
 * onChange that passed to props, sends isChecked state and contextObj
 * back to parent.
 *
 * Checkbox can be controllable (get isChecked from props and update local state)
 * or not controllable (using local state only).
 *
 * contextObj - any object with any data
 */
export default class Checkbox extends React.Component{
    static get defaultProps() {
        return {
            contextObj: {},
            hasLabel: true,
            isControllable: false,
            isChecked: false,
            labelClassName: 'checkf',
            onChange: () => {}
        }
    }

    static getDerivedStateFromProps(nextProps, prevState){
        const { isControllable } = nextProps
        if (isControllable && (nextProps.isChecked !== prevState.isChecked)){
            return {...prevState, isChecked: nextProps.isChecked}
        }
        return null
    }

    state = {
        isChecked: this.props.isChecked
    }

    onClick = () => {
        const newValue = !this.state.isChecked
        this.setState({isChecked: newValue})
        this.props.onChange(newValue, {...this.props.contextObj})
    }

    render(){
        const cn = classNames('jq-checkbox', {checked: this.state.isChecked})

        return (
            <React.Fragment>
                {this.props.hasLabel ?
                    <label className={this.props.labelClassName}>
                        <div
                            onClick={this.onClick}
                            className={cn}>
                            <div className="jq-checkbox__div" />
                        </div>
                        {this.props.label}
                    </label>
                :
                    <div
                        onClick={this.onClick}
                        className={cn}>
                        <div className="jq-checkbox__div" />
                    </div>
                }
            </React.Fragment>

        )
    }
}


Checkbox.propTypes = {
    contextObj: PropTypes.object,
    hasLabel: PropTypes.bool,
    isChecked: PropTypes.bool,
    isControllable: PropTypes.bool,
    label: PropTypes.string,
    labelClassName: PropTypes.string,
    onChange: PropTypes.func

}