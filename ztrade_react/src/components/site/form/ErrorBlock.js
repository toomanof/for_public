import React from 'react'
import PropTypes from 'prop-types'


/**
 * We may have multiple ErrorBlocks on the page.
 * In that case we pass "name" to props
 * and set "errorBlockName" to errors.
 */
export default class ErrorBlock extends React.Component{
    render(){
        const { errors, name } = this.props

        if (errors === null || !errors.hasOwnProperty('errors') || errors.errors.length === 0)
            return null

        if (name && errors.hasOwnProperty('errorBlockName') && name !== errors.errorBlockName){
            return null
        }

        return (
            <div style={{marginTop: '10px', marginBottom: '10px'}}>
                {errors.errors.map((msg, index) => {
                    return (
                        <div key={index} className="error_text">
                            {msg}
                        </div>                        
                    )
                })}
            </div>
        )
    }
}


ErrorBlock.propTypes = {
    errors: PropTypes.object,
    name: PropTypes.string
}
