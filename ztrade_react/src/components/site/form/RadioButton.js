import React from 'react'
import PropTypes from 'prop-types'


/**
 * Selected option passed from parent
 * Pass selected option object to parent via onUpdate callback
 */
export default class RadioButton extends React.Component{
    static get defaultProps() {
        return {
            className: 'checkf_rad',
            selectedId: null
        }
    }

    onClickOption = (obj) => {
        this.props.onUpdate(obj)
    }

    render(){
        const { className, options, selectedId } = this.props

        return (
            <div className={className}>
                {options.map(obj => {
                    if (obj.id === selectedId){
                        return (
                            <label
                                key={obj.id}
                                className="checkf"
                                onClick={this.onClickOption.bind(null, obj)}>
                                
                                <div className="jq-radio checked">
                                    <div className="jq-radio__div" />
                                </div>
                                {' '}{obj.text}
                            </label>
                        )
                    }

                    return (
                        <label
                            key={obj.id}
                            className="checkf"
                            onClick={this.onClickOption.bind(null, obj)}>
                            
                            <div className="jq-radio" />
                            {' '}{obj.text}
                        </label>
                    )  
                })}
            </div>
        )
    }
}


RadioButton.propTypes = {
    className: PropTypes.string,
    onUpdate: PropTypes.func.isRequired,
    options: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.any.isRequired,
        value: PropTypes.any.isRequired,
        text: PropTypes.string.isRequired
    })),
    selectedId: PropTypes.any,
}
