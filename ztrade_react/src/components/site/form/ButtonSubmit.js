import React from 'react'
import PropTypes from 'prop-types'



/**
 * If onClick is passed - then it used as "click" button
 * otherwise it used as "submit" form button
 */
export default class ButtonSubmit extends React.Component{
    static get defaultProps() {
        return {
            className: 'btn btn-orange btn-big',
            type: 'submit'
        }
    }

    onClick = (e) => {
        if (this.props.onClick){
            e.preventDefault()
            this.props.onClick()
        }
    }

    render(){
        const style = this.props.isButtonEnabled ? {} : {opacity: '0.3'}

        return (
            <input
                type={this.props.type}
                onClick={this.onClick}
                disabled={!this.props.isButtonEnabled}
                style={style}
                className={this.props.className}
                value={this.props.text} />
        )
    }
}


ButtonSubmit.propTypes = {
    className: PropTypes.string,
    isButtonEnabled: PropTypes.bool,
    onClick: PropTypes.func,
    text: PropTypes.string,
    type: PropTypes.string,
}