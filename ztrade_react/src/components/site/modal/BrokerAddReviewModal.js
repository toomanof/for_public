import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { formToObject } from 'services/helpers'
import { NEUTRAL, POSITIVE, NEGATIVE } from 'services/constants'
import { ButtonSubmit, Input, Modal, RadioButton, Textarea } from 'components'


@inject('broker', 'menu', 'modal', 'user')
@observer
export default class BrokerAddReviewModal extends React.Component{
    state = {
        selectedOptionId: null,
        selectedType: null
    }

    componentWillUnmount(){
        this.props.broker.clear()   
    }

    onClickClose = () => {
        this.props.modal.closeBrokerAddReviewModal()
    }

    onSuccess = () => {
        this.onClickClose()
    }

    onSubmit = (e) => {
        e.preventDefault()
        const { broker, user } = this.props
        broker.clear()

        const form = formToObject(e.target)
        form.broker_id = broker.brokerItem.id
        form.type = this.state.selectedType

        if (user.isAuthenticated){
            form.name = user.profile.firstName
            form.email = user.profile.email
        }
        broker.addReview(form, user.isAuthenticated, this.onSuccess)
    }

    /**
     * Callback from RadioButton
     */ 
    onSelectOption = (obj) => {
        this.setState({selectedOptionId: obj.id, selectedType: obj.value})
    }

    /**
     * Passed to RadioButton
     */
    get options(){
        return [
            {id: 1, value: NEUTRAL, text: 'Нейтральный'},
            {id: 2, value: POSITIVE, text: 'Положительный'},
            {id: 3, value: NEGATIVE, text: 'Отрицательный'},
        ]
    }

    get isButtonEnabled(){
        const { broker } = this.props 
        
        if (broker.isAddReviewInProgress){
            return false
        }
        
        if (!this.state.selectedOptionId){
            return false
        }

        return true
    }

    render(){
        const { broker, user } = this.props

        const brokerItem = broker.brokerItem
        if (!brokerItem){
            return null
        }

        return (
            <Modal
                onClickClose={this.onClickClose}>

                <React.Fragment>
                    <div className="modal_header">
                        <span>{`Добавить отзыв о брокере ${brokerItem.name}`}</span>
                    </div>
                    
                    <form onSubmit={this.onSubmit}>
                        {!user.isAuthenticated &&
                            <Input
                                name="name"
                                errors={broker.errors}
                                placeholder="Имя"
                                hasLabel={false} />
                        }

                        {!user.isAuthenticated &&
                            <Input
                                name="email"
                                errors={broker.errors}
                                placeholder="E-mail"
                                hasLabel={false} />
                        }

                        <Textarea
                            name="text"
                            errors={broker.errors}
                            placeholder="Выскажите ваше мнение..."
                            hasLabel={false} />
                        
                        <RadioButton
                            selectedId={this.state.selectedOptionId}
                            onUpdate={this.onSelectOption}
                            options={this.options} />
                    
                        <ButtonSubmit
                            className="btn btn-blue"
                            text="Добавить отзыв"
                            isButtonEnabled={this.isButtonEnabled} />
                    </form>
                    
                </React.Fragment>
            </Modal>
        )
    }
}


BrokerAddReviewModal.propTypes = {
    broker: MPropTypes.observableObject,
    menu: MPropTypes.observableObject,
    modal: MPropTypes.observableObject,
    user: MPropTypes.observableObject,
}