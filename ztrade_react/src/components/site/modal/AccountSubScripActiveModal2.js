import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'
import { ButtonSubmit, Modal } from 'components'
import { getIndicatorFileSubcscribtion } from 'services/helpers'


@inject('account', 'alert', 'menu', 'modal', 'user')
@observer
export default class AccountSubScripActiveModal2 extends React.Component{

    componentDidMount() {
        const $ = window.$
        const { account, user }  = this.props
        $('.close_modal').click(function() {
            account.subscriptionSendActiveInfoEmail(`ID ${user.profile.uid}`, user.SubscriptionTradeId, user.SubscriptionDateFrom, user.SubscriptionDateTo);
            $.fancybox.close();
        });

    }
    render(){
        const { subsciption }  = this.props
        return (
            <React.Fragment>
                <div className="large_modal" id="modal_subscrip2">
                    <div className="large_modal_title">Данные отправлены</div>
                    <div className="large_modal_small">
                        Теперь Вы можете скачать индикатор опционных уровней и установить его<br/>
                        на свой терминал MT4, в течение 30 минут, мы активируем его работу.
                    </div>
                    <div className="formbox formbox-large text-center">
                        <div className="formbox_icon">
                            <img src="images/comp.png" alt=""/>
                        </div>
                        <div className="inpf inpf_small">
                            <input type="text" className="styler" placeholder="Индикатор опционных уровней" disabled/>
                                <div className="inpf_text">Эта информация поможет<br/> нам активировать индикатор</div>
                        </div>
                        <a href={getIndicatorFileSubcscribtion(subsciption)} className="btn btn-big btn-orange close_modal" target="_blank">Скачать</a>
                    </div>
                    <div className="text-center blue">Желаем успешного трейдинга!</div>
                    <a href="#" id="modal_close" className="modal_close" data-fancybox-close></a>
                </div>
            </React.Fragment>

        )
    }
}


AccountSubScripActiveModal2.propTypes = {
    account: MPropTypes.observableObject,
    alert: MPropTypes.observableObject,
    user: MPropTypes.observableObject,
    modal: MPropTypes.observableObject
}