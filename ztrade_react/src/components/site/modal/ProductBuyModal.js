import React from 'react'
import PropTypes from 'prop-types'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { formToObject } from 'services/helpers'
import { ButtonSubmit, ErrorBlock,  Input, Modal } from 'components'


@inject('modal', 'payment', 'user')
@observer
export default class ProductBuyModal extends React.Component{
    constructor(props){
        super(props)
        this.formRef = React.createRef()
    }
    
    componentWillUnmount(){
        this.props.payment.clear()
    }

    onClickClose = () => {
        this.props.modal.closeProductBuyModal()
    }

    onSuccess = () => {
        this.onClickClose()
    }

    onSubmit = (e) => {
        e.preventDefault()
        const { payment, productId, user } = this.props
        payment.clear()

        const form = formToObject(e.target)
        form.product_id = productId
        payment.createBillProduct(form, false, this.onSuccess)
    }

    render(){
        const { payment } = this.props

        return (
            <Modal
                onClickClose={this.onClickClose}>

                <React.Fragment>
                    <div className="modal_header">
                        <span>{'Заполните форму'}</span>
                    </div>
                    
                    <form onSubmit={this.onSubmit}>
                        <Input
                            name="name"
                            errors={payment.errors}
                            placeholder="Имя"
                            hasLabel={false} />

                        <Input
                            name="email"
                            errors={payment.errors}
                            placeholder="E-mail"
                            hasLabel={false} />
                        
                        <Input
                            name="phone"
                            errors={payment.errors}
                            placeholder="Телефон"
                            hasLabel={false} />
                        
                        <ButtonSubmit
                            className="btn btn-blue btn-big"
                            text="Купить"
                            isButtonEnabled={!payment.isCreateBillInProgress} />

                        <ErrorBlock errors={payment.errors} />
                    </form>
                    
                </React.Fragment>
            </Modal>
        )
    }
}


ProductBuyModal.propTypes = {
    modal: MPropTypes.observableObject,
    payment: MPropTypes.observableObject,
    productId: PropTypes.number,
    user: MPropTypes.observableObject,
}