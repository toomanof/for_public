import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { validateEmail } from 'services/helpers'

import { ButtonSubmit, Input, Modal } from 'components'


@inject('menu', 'modal', 'user')
@observer
export default class SignupChangeDataModal extends React.Component{
    state = {
        newEmail: this.props.user.profile.email,
        // Remove prefix from phone as it will be used in Input
        newPhone: this.props.user.profile.phone.replace(this.props.user.profile.country.phone_prefix, '')
    }

    componentWillUnmount(){
        this.props.user.clearErrors()   
    }

    onClickClose = () => {
        this.props.modal.closeSignupChangeDataModal()
    }

    onSuccess = () => {
        this.onClickClose()
    }

    onChangeEmail = (value) => {
        this.setState({newEmail: value})
    }

    onChangePhone = (value) => {
        this.setState({newPhone: value})
    }

    onSubmit = () => {
        const { newEmail, newPhone } = this.state
        const { user } = this.props

        if (!validateEmail(newEmail)){
            user.setErrors({new_email: ['Неправильный email']})
            return
        }

        const form = {
            sms_guid: user.signupSmsGuid,
            new_email: newEmail,
            new_phone: user.profile.country.phone_prefix + newPhone,
        }

        this.props.user.signupChangeData(form, this.onSuccess)
    }

    get isButtonEnabled(){
        const fields = ['newEmail', 'newPhone']
        const d = {...this.state}

        for (let field of fields){
            if (d[field].trim().length === 0){
                return false
            }
        }
        return true
    }

    render(){
        const { user } = this.props

        return (
            <Modal
                onClickClose={this.onClickClose}>

                <React.Fragment>
                    <div className="modal_header">
                        <span>{'Изменить данные'}</span>
                    </div>
                    
                    <Input
                        onChange={this.onChangeEmail}
                        name="new_email"
                        placeholder="E-mail"
                        errors={user.errors}
                        value={this.state.newEmail} />

                    <Input
                        prefix={user.profile.country.phone_prefix}
                        onChange={this.onChangePhone}
                        name="new_phone"
                        placeholder="Телефон"
                        errors={user.errors}
                        value={this.state.newPhone} />
                    
                    <ButtonSubmit
                        onClick={this.onSubmit}
                        className="btn btn-blue btn-big"
                        text="Отправить"
                        isButtonEnabled={this.isButtonEnabled} />
                    
                </React.Fragment>
            </Modal>
        )
    }
}


SignupChangeDataModal.propTypes = {
    menu: MPropTypes.observableObject,
    modal: MPropTypes.observableObject,
    user: MPropTypes.observableObject,
}