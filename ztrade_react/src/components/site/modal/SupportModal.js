import React from 'react'
import { Link } from 'react-router-dom'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import classNames from 'classnames'
import { validateEmail, formToObject } from 'services/helpers'

import { ButtonSubmit, Input, Modal } from 'components'

@inject('menu', 'modal', 'user')
@observer
export default class SupportModal extends React.Component {
    constructor(props) {
        super(props)
        this.formRef = React.createRef()
    }

    state = {
        isButtonReady: false,
        isChecked: false
    }

    componentDidMount() {
        // render button with delay for "formRef" to be available
        setTimeout(() => {
            this.setState({ isButtonReady: true })
        }, 50)
    }

    componentWillUnmount() {
        this.props.user.clear()
    }

    onClickClose = () => {
        const { modal } = this.props
        modal.closeSupportModal()
    }

    onSuccess = () => {
        const form = this.formRef.current
        form.reset()
    }

    onSubmit = () => {
        const form = formToObject(this.formRef.current)

        if (!validateEmail(form.email)) {
            this.props.user.setErrors({ email: ['Неправильный email'] })
            return
        }

        this.props.user.support(form, this.onSuccess)
    }

    // rerender
    onInputChange = () => {
        this.setState(this.state)
    }

    get isButtonEnabled() {
        const form = formToObject(this.formRef.current)
        const fields = ['first_name', 'last_name', 'email', 'question']

        for (let field of fields) {
            if (form[field].trim().length === 0) {
                return false
            }
        }

        if (!this.state.isChecked) {
            return false
        }

        return true
    }

    render() {
        const { user } = this.props

        const cn = classNames('jq-checkbox', { checked: this.state.isChecked })

        return (
            <Modal
                isClickOutsideCloseModal={false}
                onClickClose={this.onClickClose}>
                <React.Fragment>
                    <div className="modal_header">
                        <span>{'Задать вопрос'}</span>
                        <small>{'Заполните форму, и мы ответим вам.'}</small>
                    </div>

                    <form ref={this.formRef} onSubmit={this.onSubmit}>
                        <Input
                            onChange={this.onInputChange}
                            name="first_name"
                            placeholder="Имя"
                            errors={user.errors}
                        />

                        <Input
                            onChange={this.onInputChange}
                            name="last_name"
                            placeholder="Фамилия"
                            errors={user.errors}
                        />

                        <Input
                            onChange={this.onInputChange}
                            name="email"
                            placeholder="E-mail"
                            errors={user.errors}
                        />

                        <div className="inpf">
                            <textarea
                                onChange={this.onInputChange}
                                name="question"
                                className="styler"
                                placeholder="Вопрос"
                            />
                        </div>

                        <div className="inpf">
                            <label
                                className="checkf"
                                onClick={() =>
                                    this.setState({
                                        isChecked: !this.state.isChecked
                                    })
                                }>
                                <div className={cn}>
                                    <div className="jq-checkbox__div" />
                                </div>
                                {'Согласие на обработку '}
                                <Link to="/personal_data">
                                    {'персональных данных'}
                                </Link>
                            </label>
                        </div>

                        {this.state.isButtonReady && (
                            <ButtonSubmit
                                onClick={this.onSubmit}
                                className="btn btn-blue btn-big"
                                text="Отправить"
                                isButtonEnabled={this.isButtonEnabled}
                            />
                        )}
                    </form>
                </React.Fragment>
            </Modal>
        )
    }
}

SupportModal.propTypes = {
    menu: MPropTypes.observableObject,
    modal: MPropTypes.observableObject,
    user: MPropTypes.observableObject
}
