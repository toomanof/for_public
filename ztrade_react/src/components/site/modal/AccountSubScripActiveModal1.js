import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'
import { ButtonSubmit, Modal } from 'components'


@inject('menu', 'modal', 'user')
@observer
export default class AccountSubScripActiveModal1 extends React.Component{

    state ={
        tradeId: null
    }
    onChangeTradeId = e =>{
        this.setState({tradeId: e.target.value})
    }

    componentDidMount() {
        const self = this
        const $ = window.$
        const { user, subsciption } = this.props
        console.log('componentDidMount AccountSubScripActiveModal1', subsciption)
        $('.js_modal_subscrip2').click(function() {
            user.setSubscriptionActiveInfo(self.state.tradeId, subsciption.dateFrom, subsciption.dateTo)
            $.fancybox.close();

        });

        $('.js_modal_subscrip2').fancybox({
            touch : false,
            closeBtn: false,
            autoFocus: false,
            btnTpl: { close: false, smallBtn: false }
        });

    }

    render(){
        const { user } = this.props
        return (
            <React.Fragment>
                <div className="large_modal" id="modal_subscrip1">
                    <div className="large_modal_title">Активация индикатора</div>
                    <div className="large_modal_small">
                        Заполните форму ниже и подтвердите свои данные.<br/>
                        Для активации индикатора, пожалуйста, укажите достоверные данные.
                    </div>
                    <div className="formbox">
                        <div className="flex-row">
                            <div className="flex-col4 inpf">
                                <p>ID личного кабинета:</p>
                                <select className="selectbox" disabled={true}>
                                    <option>{`ID ${user.profile.uid}`}</option>
                                </select>
                                <div className="inpf_text">Эта информация поможет нам активировать индикатор</div>
                            </div>
                            <div className="flex-col4 inpf">
                                <p>Номер торгового счета:</p>
                                <div className="inpf_inner">
                                    <input id="tradeId" type="text" className="styler" placeholder="29928729" onChange={this.onChangeTradeId}/>
                                        <div className="question_box">
                                            <div className="tooltip rt">Активация индикатора, происходит непоседственно
                                                для указанного в форме номера торгового счета.
                                            </div>
                                        </div>
                                </div>
                                <div className="inpf_text">Эта информация поможет нам активировать индикатор</div>
                            </div>
                            <div className="flex-col4">
                                <p className="hidden-sm">&nbsp;</p>
                                <a href="#modal_subscrip2"
                                   className="btn btn-orange btn-big block js_modal_subscrip2">Отправить</a>
                            </div>
                        </div>
                    </div>
                    <div className="large_modal_text">
                        После того, как вы отправите данные для активации,<br/>
                        мы активируем работу индикатора в течение 30 минут.
                    </div>
                    <div className="text-center"><a href="https://t.me/ztrade" className="tme_link">Есть вопросы? Задайте их нам в
                        группу</a></div>
                    <a href="#" className="modal_close" data-fancybox-close></a>
                </div>

            </React.Fragment>

        )
    }
}


AccountSubScripActiveModal1.propTypes = {
    user: MPropTypes.observableObject,
    modal: MPropTypes.observableObject
}