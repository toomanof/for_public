import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { formToObject } from 'services/helpers'
import { NEUTRAL, POSITIVE, NEGATIVE } from 'services/constants'
import { ButtonSubmit, Input, Modal, RadioButton, Textarea } from 'components'
import {logGroupMessage} from '../../../services/helpers';


@inject('exchange', 'menu', 'modal', 'user')
@observer
export default class ExchangeAddReviewModal extends React.Component{
    state = {
        selectedOptionId: null,
        selectedType: null
    }

    componentWillUnmount(){
        this.props.exchange.clear()   
    }

    onClickClose = () => {
        this.props.modal.closeExchangeAddReviewModal()
    }

    onSuccess = () => {
        this.onClickClose()
    }

    onSubmit = (e) => {
        e.preventDefault()
        const { exchange, user } = this.props
        exchange.clear()

        const form = formToObject(e.target)
        form.exchange_id = exchange.exchangeItem.id
        form.type = this.state.selectedType

        if (user.isAuthenticated){
            form.name = user.profile.firstName
            form.email = user.profile.email
        }

        logGroupMessage('onSubmit стр 37 in ExchangeAddreviewModel,', form, user)

        exchange.addReview(form, user.isAuthenticated, this.onSuccess)
    }

    /**
     * Callback from RadioButton
     */ 
    onSelectOption = (obj) => {
        this.setState({selectedOptionId: obj.id, selectedType: obj.value})
    }

    /**
     * Passed to RadioButton
     */
    get options(){
        return [
            {id: 1, value: NEUTRAL, text: 'Нейтральный'},
            {id: 2, value: POSITIVE, text: 'Положительный'},
            {id: 3, value: NEGATIVE, text: 'Отрицательный'},
        ]
    }

    get isButtonEnabled(){
        const { exchange } = this.props 
        
        if (exchange.isAddReviewInProgress){
            return false
        }
        
        if (!this.state.selectedOptionId){
            return false
        }

        return true
    }

    render(){
        const { exchange, user } = this.props

        const exchangeItem = exchange.exchangeItem
        if (!exchangeItem){
            return null
        }

        return (
            <Modal
                onClickClose={this.onClickClose}>

                <React.Fragment>
                    <div className="modal_header">
                        <span>{`Добавить отзыв о брокере ${exchangeItem.name}`}</span>
                    </div>
                    
                    <form onSubmit={this.onSubmit}>
                        {!user.isAuthenticated &&
                            <Input
                                name="name"
                                errors={exchange.errors}
                                placeholder="Имя"
                                hasLabel={false} />
                        }

                        {!user.isAuthenticated &&
                            <Input
                                name="email"
                                errors={exchange.errors}
                                placeholder="E-mail"
                                hasLabel={false} />
                        }

                        <Textarea
                            name="text"
                            errors={exchange.errors}
                            placeholder="Выскажите ваше мнение..."
                            hasLabel={false} />
                        
                        <RadioButton
                            selectedId={this.state.selectedOptionId}
                            onUpdate={this.onSelectOption}
                            options={this.options} />
                    
                        <ButtonSubmit
                            className="btn btn-blue btn-big"
                            text="Добавить отзыв"
                            isButtonEnabled={this.isButtonEnabled} />
                    </form>
                    
                </React.Fragment>
            </Modal>
        )
    }
}


ExchangeAddReviewModal.propTypes = {
    exchange: MPropTypes.observableObject,
    menu: MPropTypes.observableObject,
    modal: MPropTypes.observableObject,
    user: MPropTypes.observableObject,
}