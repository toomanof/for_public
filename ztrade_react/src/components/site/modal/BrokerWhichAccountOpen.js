import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'



@inject('menu', 'modal', 'broker')
@observer
export default class BrokerWhichAccountOpenModal extends React.Component{
    state = {
        activeTab: 0
    }
    onClickTab = (e) => {
        this.setState({ activeTab: e.target.getAttribute('index') })
    }

    onClickClose = () => {
        this.props.modal.closeBrokerWhichAccountModal()
    }

    render(){
        const { activeTab } = this.state
        const { broker } = this.props
        const item = broker.brokerItem
        return (
            <React.Fragment>
                <div className="modal minimodal" id="open_account" style={{'display': 'none'}}>
                        <div className="minimodal_title">Какой счет вы планируете открыть?</div>
                        <div className="minimodal_tabs">
                            <a href="#"
                               className={(activeTab == 0) ? " btn show-minimodal-tab1 btn-blue" : "btn show-minimodal-tab1"}
                            index={0}
                               onClick={this.onClickTab.bind(this)}
                            >Для торговли с помощью робота</a>
                            <a href="#"
                               className={(activeTab == 1) ? " btn show-minimodal-tab2 btn-blue" : "btn show-minimodal-tab2" }
                               index={1}
                               onClick={this.onClickTab.bind(this)}
                            >Для самостоятельной торговли</a>
                        </div>
                        <div id="minimodal-tab1" style={{display: activeTab == 0 ? 'block': 'none'}}>
                            <div className="minimodal_img">
                                <img src="/images/Samostoyatelnaya_torgovlya.png" alt="" />
                            </div>
                            <div className="minimodal_head">
                                <h2>Автоматическая торговля</h2>
                                <p>Сервис, позволяющий получать прибыль,<br/> не торгуя на финансовых рынках
                                    самостоятельно.</p>
                            </div>
                            <div className="minimodal_text">
                                <p>Инвестор открывает личный торговый счет с <b>рекомендуемыми параметрами у
                                    рекомендованного брокера</b>, после чего мы подключаем данный счет к одному из наших
                                    торговых роботов, который установлен на наших серверах.</p>
                                <p>Далее после получения данных вашего счета, мы настраиваем алгоритм под ваши параметры
                                    счета. После чего мы подключаем ваш торговый счет к нашему биржевому роботу и
                                    запускаем на него торговлю.</p>
                                <p><b>Важно:</b> Мы не имеем доступ к вашим личным средствам, а только лишь подключаем
                                    ваш счет у себя на сервере.</p>
                            </div>
                            <div className="minimodal_footer">
                                <a rel="noopener" href={item.linkOpenAccountWithRobot} target="_blank">Подробная инструкция, как открыть счет для аренды и<br/> подключения к нашему
                                    биржевому роботу</a>
                            </div>
                        </div>
                        <div id="minimodal-tab2" style={{display: activeTab == 1 ? 'block': 'none'}}>
                            <div className="minimodal_img">
                                <img src="/images/Avtomaticheskaya_torgovlya.png" alt=""/>
                            </div>
                            <div className="minimodal_head">
                                <h2>Самостоятельная торговля</h2>
                                <p>Сервис, позволяющий получать прибыль,<br/> торгуя на финансовых рынках самостоятельно.</p>
                            </div>
                            <div className="minimodal_text">
                                <p>Трейдер выбирает подходящего брокера, регистрирует личный кабинет и открывает торговый
                                    счет. Необходимо также пройти верификацию платежных систем, пополнить данный счет и уже
                                    после этого можно приступать к торговле.</p>
                                <p><b>Какой нужно открыть счет для самостоятельной торговли на валютном рынке и как его
                                    открыть, вы можете узнать более подробно по ссылке ниже.</b></p>
                                <p><b>Важно:</b> Есть ряд брокеров у которых не стоит открывать свои счета. Мы же
                                    рекомендуем открывать счета только у проверенных нами брокеров. <a href="/brokers">Выбрать
                                        которого можно из нашего списка</a>.</p>
                            </div>
                            <div className="minimodal_footer">
                                <a rel="noopener" href={item.linkOpenAccount} target="_blank">Подробная инструкция, как открыть счет для самостоятельной<br/> торговли на
                                    финансовых рынках</a>
                            </div>
                        </div>
                    <a href="#" className="modal_close" data-fancybox-close></a>
                </div>
            </React.Fragment>

        )
    }
}


BrokerWhichAccountOpenModal.propTypes = {
    broker: MPropTypes.observableObject,
    modal: MPropTypes.observableObject
}