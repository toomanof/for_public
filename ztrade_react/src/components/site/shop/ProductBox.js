import React from 'react'
import PropTypes from 'prop-types'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'

/**
 * Product box on products list page
 */
@inject('menu')
@observer
export default class ProductBox extends React.Component {
    get picUrl() {
        const { product } = this.props
        if (!product.pics || product.pics.length === 0) {
            return '/pics/nopic.png'
        }
        return product.pics[0].pic_url
    }

    renderProductBoxContent() {
        const { product } = this.props
        return (
            <div className="module shopbox">
                <div className="shopbox_top">
                    <div className="shopbox_left">
                        <img src={this.picUrl} alt="" />
                    </div>

                    <div className="shopbox_txt">
                        <div className="shopbox_name">
                            {/* {product.frontendUrl && (
                                <Link to={product.frontendUrl}>
                                    {product.name}
                                </Link>
                            )}
                            {!product.frontendUrl && <div>{product.name}</div>} */}
                            {product.name}
                        </div>
                        <div className="shopbox_date">
                            {product.rusDateStr('createdAt')}
                        </div>
                    </div>
                </div>

                <div className="shopbox_grey">
                    <div className="shopbox_perc1">
                        <small>{'Можно заработать'}</small>
                        {' до '}
                        {product.potentialProfit}
                        {'%'}
                    </div>
                    <div className="shopbox_perc2">
                        <small>{'Текущая доходность'}</small>{' '}
                        {product.currentProfit}
                        {'%'}
                    </div>
                </div>
            </div>
        )
    }

    render() {
        const { product } = this.props

        return (
            <div className="grid4 tb6 sm12">
                {product.frontendUrl ? (
                    <Link to={product.frontendUrl}>
                        {this.renderProductBoxContent()}
                    </Link>
                ) : (
                    this.renderProductBoxContent()
                )}
            </div>
        )
    }
}

ProductBox.propTypes = {
    menu: MPropTypes.observableObject,
    product: PropTypes.object.isRequired
}
