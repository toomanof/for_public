import React from 'react'
import PropTypes from 'prop-types'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import classNames from 'classnames'
import Swipe from 'react-easy-swipe'
import { Loading } from 'components'


/**
 * items - array of object that contain id and pic_url
 * numItems - number of visible thumbs
 */
@inject('menu')
@observer
export default class ProductPics extends React.Component{
    static get defaultProps() {
        return {
            numItems: 4,
        }
    }

    state = {
        activeItemId: null,
        startIndex: 0,
    }

    componentDidMount(){
        this.init()
    }
   
    init(){
        const { items } = this.props
        if (items.length > 0){
            this.setState({activeItemId: items[0].id})    
        }
    }

    onClickArrowUp = (e) => {
        e.preventDefault()
        if (this.isArrowUpEnabled){
            this.setState({startIndex: this.state.startIndex - 1})    
        }
    }

    onClickArrowDown = (e) => {
        e.preventDefault()
        if (this.isArrowDownEnabled){
            this.setState({startIndex: this.state.startIndex + 1})       
        }
    }

    onClickThumb = (id, e) => {
        e.preventDefault()
        this.setState({activeItemId: id})
    }

    onSwipeLeft = () => {
        let index = this.activeIndex
        if (index >= this.maxIndex - 1){
            return
        }
        index += 1
        this.setState({activeItemId: this.props.items[index].id})
    }

    onSwipeRight = () => {
        let index = this.activeIndex
        if (index <= 0){
            return
        }
        index -= 1
        this.setState({activeItemId: this.props.items[index].id})
    }

    get endIndex(){
        return this.state.startIndex + this.props.numItems
    }

    get maxIndex(){
        return this.props.items.length
    }

    get isArrowUpEnabled(){
        const { items, numItems } = this.props
        if (items.length <= numItems){
            return false
        }

        if (this.state.startIndex === 0){
            return false
        }
        return true
    }

    get isArrowDownEnabled(){
        const { items, numItems } = this.props
        if (items.length <= numItems){
            return false
        }
        
        if (this.endIndex >= this.maxIndex){
            return false
        }
        return true
    }

    /**
     * Slice of items[startIndex:endIndex]  
     */
    get thumbs(){
        return this.props.items.slice(this.state.startIndex, this.endIndex)
    }

    get activeItem(){
        return this.props.items.find(obj => obj.id === this.state.activeItemId)
    }

    /**
     * Returns index in array of active item
     */
    get activeIndex(){
        return this.props.items.indexOf(this.activeItem)
    }

    renderThumbs(){
        const cnPrev = classNames('prev', {deactive: !this.isArrowUpEnabled})
        const cnNext = classNames('next', {deactive: !this.isArrowDownEnabled})
        
        return (
            <div className="gallery_thumbs">
                <ul>
                    {this.thumbs.map(obj => {
                        const cn = classNames({active: obj.id === this.state.activeItemId})
                        
                        return (
                            <li key={obj.id} className={cn}>
                                <a href="#" onClick={this.onClickThumb.bind(null, obj.id)}>
                                    <img src={obj.pic_url} alt="" />
                                </a>
                            </li>                    
                        )
                    })}
                </ul>
                <a href="#" onClick={this.onClickArrowUp} className={cnPrev} />
                <a href="#" onClick={this.onClickArrowDown} className={cnNext} />
            </div>
        )
    }

    renderPicBox(){
        return (
            <Swipe
                onSwipeLeft={this.onSwipeLeft}
                onSwipeRight={this.onSwipeRight}>

                <div className="gallery_big_inner">
                    <img src={this.activeItem.pic_url} alt="" />
                </div>
            </Swipe>
        )
    }

    renderDots(){
        return (
            <div className="owl-dots">
                {this.props.items.map(obj => {
                    const cn = classNames('owl-dot', {active: this.state.activeItemId === obj.id})

                    return (
                        <div
                            key={obj.id}
                            onClick={this.onClickThumb.bind(null, obj.id)}
                            className={cn}>
                        
                            <span />
                        </div>
                    )                
                })}
            </div>
        )
    }

    render(){
        const { items } = this.props
        const { activeItemId } = this.state
        
        if (items.length === 0){
            return null
        }

        if (!activeItemId){
            return <Loading />
        }
        
        return (
            <div className="gallery">
                {this.renderThumbs()}

                <div className="gallery_big">
                    {this.renderPicBox()}
                    {this.renderDots()}
                </div>
            </div>
        )
    }
}


ProductPics.propTypes = {
    items: PropTypes.array,
    menu: MPropTypes.observableObject,
    numItems: PropTypes.number,
}