import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { rHTML } from 'services/helpers'


/**
 * Broker detail page.
 * Params Tab.
 */
@inject('broker', 'menu')
@observer
export default class BrokerTabParams extends React.Component{
    render(){
        const { broker } = this.props
        const item = broker.brokerItem

        return (
            <React.Fragment>

                <div className="module brokerInfo">
                    {item.params.map(obj => {
                        return (
                            <p key={obj.id}>
                                <b>{obj.name}</b><br />
                                {obj.value}
                            </p>                            
                        )
                    })}
                </div>

                {rHTML(item.content, 'text')}

            </React.Fragment>
        )
    }
}


BrokerTabParams.propTypes = {
    broker: MPropTypes.observableObject,
    menu: MPropTypes.observableObject
}