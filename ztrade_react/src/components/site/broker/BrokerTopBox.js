import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'

/**
 * Broker detail page.
 * Top box.
 * link1 and link2 actions.
 * If link starts with http, then open Modal window.
 */
@inject('broker', 'menu', 'modal')
@observer
export default class BrokerTopBox extends React.Component {

    componentDidMount() {
        window.$('.js_open_account').fancybox({
            autoFocus: false,
            touch: false,
            closeBtn: false,
            btnTpl: { close: '', smallBtn: '' }
        });
    }

    onClickWhichAccountOpen = (e) => {
        e.preventDefault()
        this.props.modal.openBrokerWhichAccountModal()
        return false;
    }
    onClickPayButton = () => {

    }
    render() {
        const { broker, modal } = this.props
        const item = broker.brokerItem

        return (
            <div className="module pammInfo">
                <div className="pammInfo_inner">
                    <div className="pammInfo_cell pammInfo_img">
                        <img
                            src={item.logoUrl}
                            alt=""
                            srcSet={`${item.logoSrcsetUrl} 2x`}
                        />
                    </div>

                    <div className="pammInfo_cell">
                        <div className="pammInfo_btnbox">
                            <a
                                rel="noopener"
                                target="_blank"
                                href={item.siteUrl}
                                className="btn btn-blue">
                                {'Открыть счет'}
                            </a>
                            <p className="sml">
                                    <a href="#open_account" className="js_open_account">
                                        {'Как открыть счет?'}
                                    </a>

                            </p>
                        </div>

                        <div className="pammInfo_btnbox">
                            <a
                                rel="noopener"
                                target="_blank"
                                href={item.siteUrl}
                                className="btn btn-green">
                                {'Сайт брокера'}
                            </a>
                        </div>

                        {item.hasPamms && (
                            <div className="pammInfo_btnbox">
                                <Link
                                    to={item.frontendPammUrl}
                                    className="btn btn-orange">
                                    {'Инвестировать в ПАММ счет'}
                                </Link>

                                {item.link2 && (
                                    <p className="sml">
                                        {item.link2.startsWith('http') ? (
                                            <a
                                                rel="noopener"
                                                target="_blank"
                                                href={item.link2}>
                                                {'Как инвестировать?'}
                                            </a>
                                        ) : (
                                            <Link to={item.link2}>
                                                {'Как инвестировать?'}
                                            </Link>
                                        )}
                                    </p>
                                )}
                            </div>
                        )}
                    </div>

                    <div className="pammInfo_cell pammInfo_rates">
                        <div className="pammInfo_rate">
                            <span className="up">{item.rating}</span>
                            {' Рейтинг'}
                        </div>
                        <div className="pammInfo_rate">
                            <span>
                                {item.minDeposit}
                                {'$'}
                            </span>
                            {' Мин. депозит'}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

BrokerTopBox.propTypes = {
    broker: MPropTypes.observableObject,
    menu: MPropTypes.observableObject,
    modal: MPropTypes.observableObject
}
