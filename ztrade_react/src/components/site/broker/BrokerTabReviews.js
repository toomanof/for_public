import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import classNames from 'classnames'
import { rHTML } from 'services/helpers'
import { POSITIVE, NEUTRAL, NEGATIVE } from 'services/constants'
import { BrokerAddReviewModal } from 'components'

/**
 * Broker detail page.
 * Reviews Tab.
 */
@inject('broker', 'menu', 'modal')
@observer
export default class BrokerTabReviews extends React.Component {
    state = {
        filter: null
    }

    onClickFilter = (value, e) => {
        e.preventDefault()
        this.setState({ filter: value })
    }

    onClickShowAddReviewModal = e => {
        e.preventDefault()
        this.props.modal.openBrokerAddReviewModal()
    }

    get filterItems() {
        const { filter } = this.state
        return [
            {
                name: 'Все',
                type: null,
                className: filter === null ? 'active' : ''
            },
            {
                name: 'Положительные',
                type: POSITIVE,
                className: filter === POSITIVE ? 'active' : ''
            },
            {
                name: 'Нейтральные',
                type: NEUTRAL,
                className: filter === NEUTRAL ? 'active' : ''
            },
            {
                name: 'Негативные',
                type: NEGATIVE,
                className: filter === NEGATIVE ? 'active' : ''
            }
        ]
    }

    get reviews() {
        const { broker } = this.props
        const item = broker.brokerItem
        const { filter } = this.state

        if (filter === POSITIVE) {
            return item.positiveReviews
        } else if (filter === NEUTRAL) {
            return item.neutralReviews
        } else if (filter === NEGATIVE) {
            return item.negativeReviews
        }

        return item.allReviews
    }

    render() {
        const { broker, modal } = this.props
        const item = broker.brokerItem

        return (
            <React.Fragment>
                <div className="module brokerInfo">
                    <div className="brokerRevNav">
                        <span>{'Показать:'}</span>
                        {this.filterItems.map(obj => {
                            return (
                                <a
                                    onClick={this.onClickFilter.bind(
                                        null,
                                        obj.type
                                    )}
                                    key={obj.name}
                                    href="/"
                                    className={obj.className}>
                                    {obj.name}
                                </a>
                            )
                        })}
                    </div>

                    {this.reviews.map((obj, index) => {
                        const cnWrapper = classNames('opinion', {
                            last: this.reviews.length - 1 === index
                        })

                        const cn = classNames('opinion_name', {
                            pos: obj.type === POSITIVE,
                            neg: obj.type === NEGATIVE
                        })

                        return (
                            <div key={obj.id} className={cnWrapper}>
                                <div className={cn}>
                                    <span>{obj.title}</span>
                                    {' от '}
                                    {obj.name}
                                </div>

                                <div className="opinion_text">{obj.text}</div>

                                <div className="opinion_meta">
                                    <span className="opinion_date">
                                        {obj.rusDateTimeStr('createdAt')}
                                    </span>

                                    <div className="opinion_rate votebox">
                                        <a
                                            href="#"
                                            onClick={
                                                this.onClickShowAddReviewModal
                                            }
                                            className="votebox_like"
                                        />
                                        <span className="votebox_val">
                                            {obj.count_likes}
                                        </span>
                                        <a
                                            href="#"
                                            onClick={
                                                this.onClickShowAddReviewModal
                                            }
                                            className="votebox_dis"
                                        />
                                    </div>
                                </div>
                            </div>
                        )
                    })}
                </div>

                <div className="module sendReview">
                    <a
                        onClick={this.onClickShowAddReviewModal}
                        href="#"
                        className="btn btn-orange btn-big">
                        {'Написать отзыв'}
                    </a>
                </div>

                {rHTML(item.content, 'text')}

                {modal.isBrokerAddReviewModalOpened && <BrokerAddReviewModal />}
            </React.Fragment>
        )
    }
}

BrokerTabReviews.propTypes = {
    broker: MPropTypes.observableObject,
    menu: MPropTypes.observableObject,
    modal: MPropTypes.observableObject
}
