import React from 'react'
import PropTypes from 'prop-types'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'

/**
 * Table row on Broker List page
 */
@inject('menu')
@observer
export default class BrokerRow extends React.Component {
    get spreadClass() {
        const { item } = this.props

        if (item.spread === 1) {
            return ['spred_box gr', 'spred_box v2', 'spred_box v3']
        } else if (item.spread === 2) {
            return ['spred_box or', 'spred_box v2 or', 'spred_box v3']
        } else if (item.spread === 3) {
            return ['spred_box red', 'spred_box v2 red', 'spred_box v3 red']
        }
        return ['', '', '']
    }

    render() {
        const { item } = this.props

        return (
            <div className="brokers_row">
                <Link
                    to={item.frontendUrl}
                    className="brokers_cell brokers_img">
                    <img
                        src={item.logoListUrl}
                        alt=""
                        srcSet={`${item.logoListSrcsetUrl} 2x`}
                    />
                </Link>

                <div className="brokers_cell brokers_rate">
                    <small>{'Рейтинг'}</small>
                    <div className="brokers_dig line">
                        <b className="gr">{item.rating}</b>
                        {'Рейтинг'}
                    </div>

                    <div className="brokers_dig">
                        <div className="brokers_comments">
                            <span className="green">
                                {item.countPositiveReviews}
                            </span>
                            <span className="orange">
                                {item.countNeutralReviews}
                            </span>
                            <span className="red">
                                {item.countNegativeReviews}
                            </span>
                        </div>
                        {'Отзывы'}
                    </div>
                </div>

                <div className="brokers_cell">
                    <small>{'Информация'}</small>
                    <div className="brokers_dig line">
                        <b>
                            {item.minDeposit}
                            {'$'}
                        </b>
                        {' Мин. депозит'}
                    </div>
                    <div className="brokers_dig">
                        <b>
                            {'1:'}
                            {item.maxLeverage}
                        </b>
                        {' Макс. плечо'}
                    </div>
                </div>

                <div className="brokers_cell brokers_spred">
                    <div className="spred_outer">
                        <div className="spred">
                            <b className={this.spreadClass[0]} />
                            <b className={this.spreadClass[1]} />
                            <b className={this.spreadClass[2]} />
                        </div>
                        {'Спред'}
                    </div>
                </div>
            </div>
        )
    }
}

BrokerRow.propTypes = {
    item: PropTypes.object,
    menu: MPropTypes.observableObject
}
