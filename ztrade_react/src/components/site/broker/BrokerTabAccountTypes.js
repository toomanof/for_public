import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import classNames from 'classnames'
import { rHTML } from 'services/helpers'


/**
 * Broker detail page.
 * Account types Tab.
 */
@inject('broker', 'menu')
@observer
export default class BrokerTabAccountTypes extends React.Component{
    state = {
        activeType: null
    }

    componentDidMount(){
        const { broker } = this.props
        const item = broker.brokerItem

        if (item.accountTypes.length > 0){
            this.setState({activeType: item.accountTypes[0]})
        }   
    }

    onClickType = (obj, e) => {
        e.preventDefault()
        this.setState({activeType: obj})
    }

    get accountItems(){
        const { broker } = this.props
        const item = broker.brokerItem
        const { activeType } = this.state

        if (!activeType){
            return []
        }

        return item.accountItems.filter(obj => obj.account_type_id === activeType.id)
    }

    renderTypes(){
        const { broker } = this.props
        const item = broker.brokerItem

        return (
            <ul className="brokerTypesNav">
                {item.accountTypes.map(obj => {
                    const cn = classNames({active: obj.id === this.state.activeType.id})
                    return (
                        <li key={obj.id} className={cn}>
                            <a
                                onClick={this.onClickType.bind(null, obj)}
                                href="">

                                {obj.name}
                            </a>
                        </li>
                    )
                })}
            </ul>
        )
    }

    render(){
        const { broker } = this.props
        const item = broker.brokerItem
        const { activeType } = this.state

        if (!activeType){
            return null
        }

        return (
            <React.Fragment>
                <div className="module brokerInfo">
                    <div className="row">
                        <div className="grid4 sm12">
                            {this.renderTypes()}
                        </div>
                        <div className="grid8 sm12 brokerTypesText">
                            <h2>{activeType.name}</h2>
                            <div className="typam">
                                {this.accountItems.map(obj => {
                                    return (
                                        <div key={obj.id} className="typam_tr">
                                            <div className="typam_td">
                                                {obj.name}
                                            </div>
                                            <div className="typam_val">
                                                {obj.value}
                                            </div>
                                        </div>        
                                    )
                                })}
                            </div>
                        </div>
                    </div>
                </div>

                {rHTML(item.content, 'text')}
            </React.Fragment>
        )
    }
}


BrokerTabAccountTypes.propTypes = {
    broker: MPropTypes.observableObject,
    menu: MPropTypes.observableObject
}