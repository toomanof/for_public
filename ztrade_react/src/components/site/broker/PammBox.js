import React from 'react'
import PropTypes from 'prop-types'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'

@inject('broker', 'menu')
@observer
export default class PammBox extends React.Component {
    render() {
        const { broker, obj } = this.props

        return (
            <div className="pamm">
                <div className="pamm_inner">
                    <div className="pamm_img">
                        <img
                            src={broker.brokerItem.logoCircleUrl}
                            alt=""
                            srcSet={broker.brokerItem.logoCircleSrcsetUrl}
                        />
                    </div>
                    <div className="pamm_desc">
                        <div className="pamm_name">
                            <a rel="noopener" target="_blank" href={obj.link}>
                                {obj.first_name} {obj.last_name}
                            </a>
                        </div>
                        <div className="row">
                            <div className="grid6 md12">
                                <p>
                                    {'Возраст: '}
                                    <b>{`${obj.age} дн. (${obj.age_str})`}</b>
                                </p>
                                <p>
                                    {'В среднем инвестору в мес.: '}
                                    <b>{`${obj.avg_month}%`}</b>
                                </p>
                                <p>
                                    {'В среднем инвестору в год: '}
                                    <b>{`${obj.avg_year}%`}</b>
                                </p>
                            </div>
                            <div className="grid6 md12">
                                <p>
                                    {'Минимальный депозит: '}
                                    <b>{`${obj.min_deposit}$`}</b>
                                </p>
                                <p>
                                    {'Комиссия управляющего: '}
                                    <b>{`${obj.fee}%`}</b>
                                </p>
                            </div>
                        </div>
                        <a
                            rel="noopener"
                            target="_blank"
                            href={obj.link}
                            className="btn btn-blue">
                            {'Инвестировать'}
                        </a>
                    </div>
                    <div className="pamm_chart">
                        <div className="chartWrapper">
                            <img src={obj.pic_url} alt="" />
                            <div className="chartWrapper_val">{`${obj.perc} %`}</div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

PammBox.propTypes = {
    broker: MPropTypes.observableObject,
    menu: MPropTypes.observableObject,
    obj: PropTypes.object.isRequired
}
