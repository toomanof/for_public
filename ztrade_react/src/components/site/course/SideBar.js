import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'
import CoursePage from '../../../pages/site/CoursePage';


let scrollAnimateToTopCoursePage = () => {
    window.$([document.documentElement, document.body]).animate(
        {
            scrollTop: window.$('#study_wrapper').offset().top - 30
        },
        1000
    )
    window.scrollTo(0, 0)
};


@inject('menu', 'course')
@observer
export default class SideBarCourse extends React.Component{

    constructor(props) {
        super(props)
        const { course } = this.props
        this.course = course
        this.set
    }

     onClickCourse = (id, e) => {
        e.preventDefault()
        this.props.course.setActiveCourse(id);
        scrollAnimateToTopCoursePage();
    }

    onClickAnalysisFilter = (analysis, e) => {
        e.preventDefault()
        this.props.course.setAnalysisFilter(analysis);
        scrollAnimateToTopCoursePage();
    }

    onClickClearAnalysisFilter = (e) => {
        e.preventDefault()
        this.props.course.setAnalysisFilter(null);
        scrollAnimateToTopCoursePage();
    }

    onClickTab = (index, e) => {
        e.preventDefault()
        this.course.state_markets_nav[index] = !this.course.state_markets_nav[index]
        for (var [key, value] of Object.entries(this.course.state_markets_nav)) {
            if (key != index){this.course.state_markets_nav[key] = false}
        }
    }

    renderSubMenuMarket = (market) => {
        return (
                <React.Fragment>
                    <li>
                        {market.allSubMenu &&
                            <span className="studynav_title"
                                  onClick={this.onClickClearAnalysisFilter.bind(null)}
                                  style={{cursor: 'pointer'}}
                            >Методы анализа</span>
                        }
                        <ul style={{display: 'block'}}>
                            {this.course.analysis_courses.map((analysis, index) => {
                                return (
                                    <li key={analysis.id}>
                                        <a href="#"
                                           className={analysis.id == this.course.active_analysis.id ? "active_analysis" : ""}
                                           onClick={this.onClickAnalysisFilter.bind(
                                               null,
                                               analysis
                                           )}
                                        >{analysis.name}</a>
                                    </li>
                                )
                            })}
                        </ul>
                    </li>
                    {market.allSubMenu &&
                    <React.Fragment>
                        <li>
                        <span className="studynav_title">Бесплатное обучение</span>
                        <ul className="studynav_bl" style={{display: 'block'}}>
                            {this.course.free_courses.filter(free_course => free_course.market == market.id).map((free_course, index) => {
                                return (
                                    <li key={free_course.id}>
                                        <a href="#"
                                           onClick={this.onClickCourse.bind(
                                               null,
                                               free_course.id
                                           )}
                                        >{free_course.name}</a>
                                    </li>
                                )
                            })}
                        </ul>
                    </li>
                        <li>
                        <span className="studynav_title">Платное обучение</span>
                        <ul className="studynav_gr" style={{display: 'block'}}>
                    {this.course.paid_courses.filter(paid_course => paid_course.market == market.id).map((paid_course, index) => {
                        return (
                        <li key={paid_course.id}>
                        <a href="#"
                        onClick={this.onClickCourse.bind(
                        null,
                        paid_course.id
                        )}
                        >{paid_course.name}</a>
                        </li>
                        )
                    })}
                        </ul>
                        </li>
                    </React.Fragment>
                    }
                </React.Fragment>
        )
    }

    renderAnalysisMenu =() =>{
        return (
            this.course.markets.map((market, index) => {
            let styleUl = (this.course.state_markets_nav[market.id]) ? {display:'block'}: {display:'none'}
            return(
                <li key={market.id}>
                    <a
                        href="#"
                        className= {this.course.state_markets_nav[market.id] ? "studynav_show studynav_parent open": "studynav_show studynav_parent"}
                        onClick={this.onClickTab.bind(null, market.id)}
                    >{market.name}
                    </a>
                    { market.published &&
                        <ul style={styleUl}>
                            {this.renderSubMenuMarket(market)}
                        </ul>
                    }
                </li>
            )
        })
        )
    }

    render(){
        return (
            <div className="column220 shown">
                <div className="studynav">
                    <div className="module studynav_mobile">
                        <a href="#" className="studynav_mobile_inner js_studynav_mobile_inner">
                            <span className="studynav_mobile_title">Валютный рынок форекс</span>
                            <span className="studynav_mobile_choose">Метод анализа: Анализ объема</span>
                        </a>
                    </div>
                    <div className="module studynav_nav">
                        <div className="studynav_header">{this.course.active_analysis ? this.course.active_analysis.name: ""}</div>
                        <ul>
                            {this.renderAnalysisMenu()}
                        </ul>
                    </div>
                </div>

            </div>
        )
    }
}
CoursePage.propTypes = {
    menu: MPropTypes.observableObject,
    course: MPropTypes.observableObject,
}
