import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'
import { Player } from 'components'

@inject('menu', 'course', 'modal')
@observer
export default class ContentCourse extends React.Component{
    constructor(props) {
        super(props)
        const {course} = this.props
        this.course_store = course
    }
    onClickSupport = e => {
        e.preventDefault()
        this.props.modal.openSupportModal()
    }

    render(){
        let price_str = "Бесплатно"
        if(this.course_store.active_course && this.course_store.active_course.price > 0){
            price_str = this.course_store.active_course.discountPrice ? this.course_store.active_course.discountPrice : this.course_store.active_course.price + "s"
        }
        console.log('--------------------------- price_str ', price_str)
        return (
            <div className="bricksmBox">
                {this.course_store.active_course &&
                <div className="bricksmBoxIn">
                    <div className="module">
                        <div className="module_content modulekurs">
                            <div className="modulekurs_txt">
                                <h2>Описание курса</h2>
                                {this.course_store.active_course.description}
                            </div>

                            <div className="coursePrice">
                                <div className="coursePrice_col">
                                    <div className="coursePrice_title">Стоимость курса</div>
                                    <div className="coursePrice_price">
                                        {this.course_store.active_course.discountPrice > 0 &&
                                            <del>{this.course_store.active_course.price}$</del>
                                        }
                                        {price_str}
                                    </div>
                                </div>
                                <div className="coursePrice_col">
                                    <a href="#" className="btn btn-green">Оплатить курс</a>
                                    <small>* После оплаты, Вам на почту придет ссылка на скачивание
                                        данного курса</small>
                                </div>
                                {this.course_store.active_course.differenceEndDiscount &&
                                <div className="coursePrice_col">
                                    <p>Успей, предложение ограничено!</p>
                                    <div className="counter">
                                        <div className="counter_dig">{this.course_store.active_course.differenceEndDiscount.days}</div>
                                        <div className="counter_dig">{this.course_store.active_course.differenceEndDiscount.hours}</div>
                                        <div className="counter_dig">{this.course_store.active_course.differenceEndDiscount.minutes}</div>
                                    </div>
                                </div>
                                }
                            </div>

                        </div>

                        <div className="module_content modulekursDesc grey">
                            <h2>Порядок проведения</h2>

                            <ul>
                                {this.course_store.active_course.carrying.map((obj, index) => {
                                    return (<li key={obj.id}>{obj.title}</li>)
                                })}
                            </ul>
                        </div>

                        <div className="module_content modulekursDesc">
                            <h2>Темы занятий</h2>
                            <ol className="numbered">
                                {this.course_store.active_course.lessons.map((lesson, index) => {
                                    return (<li key={lesson.id}><span><b>{index}</b></span>{lesson.title}</li>)
                                })}
                            </ol>
                        </div>

                    </div>

                    <div className="module module_video">
                        {this.course_store.active_course.video &&
                            <Player
                               fileUrl={this.course_store.active_course.video.file_url}
                                link={this.course_store.active_course.video.link}/>
                        }
                    </div>

                    <div className="module">
                        <div className="module_content orderCourse">
                            <a href="#" className="btn btn-green">Записаться на онлайн
                                обучение &rarr;</a>
                        </div>
                        <div className="module_content module_consult">
                            <div className="module_consult_box">
                                <h3>Остались вопросы?</h3>
                                <p>Получи квалифицированный ответ нашего специалиста в течение 15
                                    минут</p>
                            </div>
                            <a href="/"
                               className="btn btn-quest"
                               onClick={this.onClickSupport}>
                                Задать вопрос
                            </a>
                        </div>
                    </div>
                </div>
                }
            </div>
        )
    }
}


ContentCourse.propTypes = {
    menu: MPropTypes.observableObject,
    t: MPropTypes.observableObject,
    course: MPropTypes.observableObject,
}