import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import classNames from 'classnames'
import Centrifuge from 'centrifuge'
import { WS_CENTRIFUGO_URL } from 'api'

let cent

@inject('general', 'menu', 't')
@observer
export default class ExchangeBlock extends React.Component {
    state = {
        prices: null
    }

    componentDidMount() {
        cent = new Centrifuge(WS_CENTRIFUGO_URL)
        cent.setToken(this.props.general.centrifugoToken)

        let subscription = cent.subscribe('rates', msg => {
            console.log('Centrifuge subscribe rates', msg.data)
            this.setState({ prices: msg.data })
        })

        subscription.history().then(publications => {
            if (publications.length > 0) {
                let msg = publications[0]
                this.setState({ prices: msg.data })
            }
        })

        cent.connect()
    }

    componentWillUnmount() {
        cent.disconnect()
    }

    prepareValue = obj => {
        if (obj.symbol.endsWith('JPY')) {
            return parseFloat(obj.value).toFixed(3)
        }
        return parseFloat(obj.value).toFixed(5)
    }

    onClickSymbol = e => {
        e.preventDefault()
    }

    render() {
        const { prices } = this.state
        if (!prices) {
            return null
        }

        return (
            <div className="module">
                <div className="module_title">
                    <b>{this.props.t.d.a018}</b>
                    <small>{this.props.t.d.a019}</small>
                </div>
                <div className="chartbox" />
                <table className="antable">
                    <tbody>
                        <tr>
                            <th className="antable_name">{'Инструмент'}</th>
                            <th>{'Цена'}</th>
                            <th>{'Изменение (%)'}</th>
                        </tr>

                        {prices.map(obj => {
                            const cnPrice = classNames('antable_price')
                            const cnSpanPrice = classNames({
                                red: obj.direction === 'down' && obj.isChanged,
                                green: obj.direction === 'up' && obj.isChanged
                            })
                            const cnDirection = obj.isChanged
                                ? obj.direction
                                : ''
                            return (
                                <tr key={obj.symbol}>
                                    <td className="antable_name">
                                        <a
                                            href="#"
                                            onClick={this.onClickSymbol}>
                                            {obj.symbol}
                                        </a>
                                    </td>
                                    <td className={cnPrice}>
                                        <span className={cnSpanPrice}>
                                            {this.prepareValue(obj)}
                                        </span>
                                    </td>
                                    <td className={cnDirection}>
                                        {obj.isChanged && `${obj.changePerc}%`}
                                    </td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>

                <div className="kursrow">
                    <marquee behavior="scroll" direction="left">
                        {prices.map(obj => {
                            const cn = classNames('kursbox', obj.direction)

                            return (
                                <div key={obj.symbol} className={cn}>
                                    <a href="#" onClick={this.onClickSymbol}>
                                        {obj.symbol}
                                    </a>
                                    <span>{`${obj.value} | ${obj.prevValue}`}</span>
                                </div>
                            )
                        })}
                    </marquee>
                </div>
            </div>
        )
    }
}

ExchangeBlock.propTypes = {
    general: MPropTypes.observableObject,
    menu: MPropTypes.observableObject,
    t: MPropTypes.observableObject
}
