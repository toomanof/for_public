import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'

import {
    Header,
    TopMenu,
    Footer
} from 'components'



@inject('routing')
@observer
export default class BaseLayout extends Component{
    render(){
        const { user } = this.props

        return (
        	<React.Fragment>
                <Header />
                <TopMenu />
                
                {this.props.children}
                
                <Footer />
        	</React.Fragment>
        )
    }
}


BaseLayout.propTypes = {
    children: PropTypes.node,
    routing: MPropTypes.observableObject,
}