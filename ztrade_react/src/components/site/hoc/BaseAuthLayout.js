import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import {
    AccountHead,
    AccountHeadMob,
    AccountLeftMenu,
    Loading,
    WSUpdater
} from 'components'

@inject('modal', 'routing', 'user')
@observer
export default class BaseAuthLayout extends Component {
    componentDidMount() {
        const { user } = this.props
        if (!user.profile) {
            user.loadProfile()
        }
    }

    render() {
        const { user } = this.props

        if (!user.profile) {
            return <Loading />
        }

        return (
            <div className="adminpage">
                <AccountHeadMob />
                <AccountLeftMenu />
                <AccountHead />

                {this.props.children}
                <WSUpdater />
            </div>
        )
    }
}

BaseAuthLayout.propTypes = {
    children: PropTypes.node,
    routing: MPropTypes.observableObject,
    user: MPropTypes.observableObject
}
