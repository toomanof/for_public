import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Alerts, LoadingPage, SupportModal } from 'components'
import generalStore from '../../../stores/GeneralStore'
import moment from 'moment'
import 'moment/locale/ru'


// let updateLastAccessInterval = null

@inject('account', 'general', 'menu', 'modal', 'routing', 't', 'user')
@observer
export default class Base extends Component {
    componentDidMount() {
        // updateLastAccessInterval = setInterval(() => user.updateLastAccess(), 1000000000)
        console.log('Base componentDidMount props', this.props)
        const { account, menu, t, user } = this.props

        t.load()
        menu.load()
        menu.loadMenuRight()
        menu.loadMenuRightLink()
        menu.loadMenuFooter()
        menu.loadMenuTop()
        menu.loadHomeSlider()
        menu.loadMainHomeSlider()
        menu.loadTopHeaderSlider()
        menu.loadHomeWhy()
        menu.loadHomeBenefits()
        menu.loadSignalSlider()
        menu.loadRecommend()
        menu.loadFooterLink()
        menu.loadArticles(9)
        account.loadFreePeriod()

        generalStore.getCentrifugoToken()
        generalStore.loadBanners()

        if (user.isAuthenticated && !user.profile) {
            user.loadProfile()
        }

        this.initMenu()

        window.addEventListener('click', this.handleMouseClick)

        moment.locale('ru')

    }

    componentDidUpdate(prevProps) {
        if (this.props.routing.location !== prevProps.location) {
            this.onRouteChanged()
        }
    }

    componentWillUnmount() {
        // updateLastAccessInterval = null
        window.removeEventListener('click', this.handleMouseClick)
    }

    handleMouseClick = e => {

        if(e.target.id != 'btn_lk' && this.props.menu.isMenuLKOpened){
            this.props.menu.closeMenuLK()
        }
        console.log('------------------------------ handleMouseClick------------------- ', e.target.id)
        if (
            typeof e.target.className === 'string' &&
            e.target.className.includes('zlink')
        ) {
            e.preventDefault()
            const to = e.target.getAttribute('href')
            this.props.routing.push(to)
        }
    }

    initMenu() {
        const { menu } = this.props
        if (!menu.mapByUrl) {
            setTimeout(() => {
                this.initMenu()
            }, 10)
            return
        }
        this.onRouteChanged()
    }

    onRouteChanged() {
        const { menu } = this.props
        const url = this.props.routing.location.pathname


        // Initial loading, mapByUrl not ready yet
        if (!menu.mapByUrl) {
            return
        }
        menu.setActiveMenuItemAuto(url)
    }

    render() {
        const { general, menu, modal, t, user } = this.props

        if (
            (user.isAuthenticated && !user.profile) ||
            !t.d ||
            !menu.map ||
            !menu.mapByUrl ||
            !menu.menuRight ||
            !menu.menuRightLink ||
            !menu.menuFooter ||
            !menu.menuTop ||
            !menu.homeSlider ||
            !menu.homeWhy ||
            !menu.homeBenefits ||
            !menu.signalSlider ||
            !menu.recommend ||
            !general.centrifugoToken
        ) {
            return (
                <LoadingPage />
            )
        }

        return (
            <React.Fragment>

                <Alerts />
                {this.props.children}

                {modal.isSupportModalOpened && <SupportModal />}
            </React.Fragment>
        )
    }
}

Base.propTypes = {
    children: PropTypes.node,
    account: MPropTypes.observableObject,
    general: MPropTypes.observableObject,
    location: PropTypes.object,
    menu: MPropTypes.observableObject,
    modal: MPropTypes.observableObject,
    routing: MPropTypes.observableObject,
    t: MPropTypes.observableObject,
    user: MPropTypes.observableObject
}
