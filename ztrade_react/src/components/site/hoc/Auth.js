import React, { Component } from 'react';
import PropTypes from 'prop-types'
import { Redirect } from 'react-router-dom'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Loading } from 'components'


/**
 * Require Auth
 */
@inject('user')
@observer
export default class Auth extends Component{
    componentDidMount() {
        this.props.user.checkToken()
    }

    render(){
        const { user } = this.props
        
        
        if (user.isAuthenticated === null){
            return <Loading />
        }

        if (user.isAuthenticated === false){
            return <Redirect to="/site/login" />
        }

        return {...this.props.children}
    }
}


Auth.propTypes = {
    children: PropTypes.node,
    user: MPropTypes.observableObject,
}
