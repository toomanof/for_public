import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import OwlCarousel from 'react-owl-carousel';
import SwiperCore, { Navigation, Pagination, Scrollbar, A11y, Autoplay, EffectFade} from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import { rHTML } from 'services/helpers'

SwiperCore.use([Navigation, Pagination, Scrollbar, A11y, Autoplay, EffectFade]);

@inject('menu')
@observer
export default class MainHomeSlider extends React.Component {

    render() {
        if (!this.props.menu.mainHomeSlider) {
            return null
        }

        return (
            <div className="homebig_slider" style={{paddingBottom:'30px'}}>
                <div className="swiper-container">
                    <Swiper
                        slidesPerView={"auto"}
                        spaceBetween={20}
                        navigation
                        pagination={{clickable: true}}
                        scrollbar={{draggable: true}}
                        autoplay={{delay:7000}}
                        >
                            {this.props.menu.mainHomeSlider.map((obj, index) => {
                                return(
                                    <SwiperSlide key={obj.sort}>
                                        {rHTML(obj.content, "swiper-slide " + obj.css)}
                                    </SwiperSlide>
                                )
                            })}
                    </Swiper>

                </div>
            </div>
        )
    }
}

MainHomeSlider.propTypes = {
    menu: MPropTypes.observableObject
}
