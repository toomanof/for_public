import React, {Fragment} from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import OwlCarousel from 'react-owl-carousel';


@inject('menu')
@observer
export default class ArticlesSlider extends React.Component {

    render() {
        if (!this.props.menu.articles) {
            return null
        }

        let itm = 3;
        if(window.screen.width <= 1020)
        {
            itm = 1.5;
        }
        console.log('---------------------', window.screen.width, itm)
        return (
            <Fragment>
                <div className="swiper-container" id="articles_slider">

                        <OwlCarousel
                            id={"articles_slider"}
                            className={'swiper-wrapper swiper-container-initialized swiper-container-horizontal'}
                            stageClass={'swiper-slide'}
                            dotClass={"swiper-pagination-bullet"}
                            dotsClass={"swiper-pagination swiper-pagination-clickable swiper-pagination-bullets"}
                            items={itm}
                            dots={true}
                            nav={false}
                            autoplay={true}
                            loop={true}
                            margin={30}
                            mergeFit={false}
                            lazyLoad={true}
                            >

                            {this.props.menu.articles.map((obj, index) => {
                                    let style_item = {backgroundImage:'url(' + obj.picUrl + ')'}
                                    return(
                                        <div className="module anons2" key={obj.id}>
                                            <a href={obj.url} className="anons2_img"
                                               style={style_item}>
                                                <span className="anons2_tag"></span>
                                            </a>
                                            <div className="anons2_content">
                                                <div className="anons2_date">{obj.rusDateTimeStr('createdAt')}</div>
                                                <div className="anons2_name"><a href={obj.url}>{obj.name}</a></div>
                                                <div className="anons2_meta">
                                                    <a href={obj.url} className="anons2_likes">160</a>
                                                    <div className="anons2_views">3 708</div>
                                                    <div className="anons2_comments">12</div>
                                                </div>
                                            </div>
                                        </div>
                                    )
                                })
                            }

                        </OwlCarousel>
                </div>



                <div className='text-center articles_slider_more'>
                    <a href="/forex_articles" className="btn btn-more"><b>{'Все статьи'}</b></a>
                </div>
            </Fragment>
        )
    }
}

ArticlesSlider.propTypes = {
    menu: MPropTypes.observableObject
}
