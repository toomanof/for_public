import React from 'react'
import PropTypes from 'prop-types'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'

@inject('menu')
@observer
export default class HomeBenefits extends React.Component {
    render() {
        const { menu } = this.props
        return (
            <div className="module">
                <div className="module_title2">
                    <b>{'Получите больше чем просто Forex и Crypto'}</b>
                    {' - '}
                    <small>
                        {
                            'Онлайн сервис для успешной биржевой торговли и аналииза финансовых рынков'
                        }
                    </small>
                </div>
                <div className="module_content">
                    <div className="row xmbox">
                        {menu.homeBenefits.map(obj => {
                            return (
                                <div className="grid4 md6" key={obj.id}>
                                    <img src={obj.picUrl} alt="" />
                                    <span className="xmbox_name">
                                        <span className="xmbox_num">
                                            {obj.fnumber}
                                        </span>
                                        <span className="xmbox_title">
                                            {obj.title}
                                        </span>
                                    </span>
                                </div>
                            )
                        })}
                    </div>
                    <div className="xmbox_text">
                        <Link to="/site" className="btn btn-blue btn-big">
                            {'Зарегистрироваться'}{' '}
                            <small>{'в личном кабинете'}</small>
                        </Link>
                        <p>
                            {
                                'В личном кабинете вы получите все неоходимые инструменты для анализа рынка и успешной торговли от '
                            }
                            <Link to="/">{'Ztrade Group'}</Link>
                        </p>
                    </div>
                </div>
            </div>
        )
    }
}

HomeBenefits.propTypes = {
    menu: MPropTypes.observableObject
}
