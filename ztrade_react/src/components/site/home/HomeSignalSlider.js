import React from 'react'
import PropTypes from 'prop-types'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import classNames from 'classnames'
import { Link } from 'react-router-dom'
import { rHTML } from 'services/helpers'

function Slide({ obj }) {
    const leftCn = classNames('homeslider_left', {
        [`homeslider_left-${obj.color}`]: obj.color !== 'default'
    })
    return (
        <div className="item">
            <div className={leftCn}>
                <div className="homeslider_head">{obj.title}</div>
                <div className="homeslider_img">
                    <img src={obj.picWhiteUrl} alt="" className="hide" />
                    <img src={obj.picYellowUrl} alt="" className="hover" />
                </div>
            </div>

            <div className="homeslider_right">
                {rHTML(obj.content, '')}
                <Link to={obj.link} className="btn btn-blue">
                    {obj.button}
                </Link>
                <Link to={obj.link} className="test">
                    {obj.text2}
                </Link>
            </div>
        </div>
    )
}

Slide.propTypes = {
    obj: PropTypes.object
}

@inject('menu')
@observer
export default class HomeSignalSlider extends React.Component {
    componentDidMount() {
        const $ = window.$
        $('#homeslider_slide').owlCarousel({
            items: 1,
            margin: 0,
            mouseDrag: false,
            dots: true,
            nav: true
        })

        $('#homeslider_slide').each(function(index) {
            $(this)
                .find('.owl-nav, .owl-dots')
                .wrapAll('<div class="owl-controls"></div>')
        })
    }

    componentWillUnmount() {
        const $ = window.$
        $('#homeslider_slide').owlCarousel(null)
    }

    render() {
        if (!this.props.menu.signalSlider) {
            return null
        }
        return (
            <div className="module homeslider">
                <div id="homeslider_slide" className="owl-carousel">
                    {this.props.menu.signalSlider.map(obj => {
                        return <Slide key={obj.sort} obj={obj} />
                    })}
                </div>
            </div>
        )
    }
}

HomeSignalSlider.propTypes = {
    menu: MPropTypes.observableObject
}
