import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import OwlCarousel from 'react-owl-carousel';


@inject('menu')
@observer
export default class TopHeaderSlider extends React.Component {

    render() {
        if (!this.props.menu.topHeaderSlider) {
            return null
        }
        const style_swiper_wrapper ={height:'64px'}
        const style_img = {height:'56px', width:'61px'}
        return (
            <div className="head_banner">
                <div className="swiper-container" style={style_swiper_wrapper}>
                        <OwlCarousel
                            className='swiper-wrapper'
                            id="signals_slider"
                            items={1}
                            dots={false}
                            nav={false}
                            autoplay={true}
                            loop={true}
                            margin={10}
                        >
                                    {this.props.menu.topHeaderSlider.map((obj, index) => {
                                        return(
                                            <div className="swiper-slide" key={obj.sort}>
                                                <div className="head_banner_box">
                                                    <img src={obj.picUrl} alt="" style={style_img}/>
                                                    <a href={obj.url} >
                                                        <div className="head_banner_text">{obj.text}</div>
                                                    </a>
                                                </div>
                                            </div>
                                        )
                                    })}
                        </OwlCarousel>

                </div>
            </div>
        )
    }
}

TopHeaderSlider.propTypes = {
    menu: MPropTypes.observableObject
}
