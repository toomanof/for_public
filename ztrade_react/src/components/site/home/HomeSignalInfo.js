import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'

@inject('menu')
@observer
export default class HomeSignalInfo extends React.Component {
    render() {
        const { menu } = this.props
        return (
            <div className="module module_signals">
                <div className="module_title">{'Зачем нужны сигналы?'}</div>
                <div className="module_content">
                    <div className="row">
                        {menu.homeWhy.map(obj => {
                            return (
                                <div
                                    key={obj.id}
                                    className="grid6 sm12 signals">
                                    <span className="circle_icon">
                                        <img src={obj.picUrl} alt="" />
                                    </span>
                                    <div className="signals_text">
                                        <span>{obj.title}</span>
                                        {obj.text}
                                    </div>
                                </div>
                            )
                        })}
                    </div>
                </div>
            </div>
        )
    }
}

HomeSignalInfo.propTypes = {
    menu: MPropTypes.observableObject
}
