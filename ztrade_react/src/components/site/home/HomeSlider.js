import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'

@inject('menu')
@observer
export default class HomeSlider extends React.Component {
    componentDidMount() {
        window.$('#hometopSlider').owlCarousel({
            items: 5,
            margin: 10,
            mouseDrag: false,
            dots: true,
            nav: false,
            responsive: {
                0: { items: 2 },
                800: { items: 3 },
                1020: { items: 4 },
                1100: { items: 5 }
            }
        })
    }

    componentWillUnmount() {
        window.$('#hometopSlider').owlCarousel(null)
    }

    onClickItem(index) {
        window.$('#homeslider_slide').trigger('to.owl.carousel', index)
        window.$([document.documentElement, document.body]).animate(
            {
                scrollTop: window.$('#homeslider_slide').offset().top - 30
            },
            1000
        )
    }

    render() {
        if (!this.props.menu.homeSlider) {
            return null
        }
        return (
            <div className="hometop">
                <div className="container">
                    <div className="ipad" />
                    <div className="hometop_title">
                        {'Инструменты для анализа '}
                        <small>{'и торговли на финансовых рынках'}</small>
                    </div>

                    <div className="hometopCar">
                        <div id="hometopSlider" className="owl-carousel">
                            {this.props.menu.homeSlider.map((obj, index) => {
                                return (
                                    <div
                                        className="item"
                                        key={obj.sort}
                                        onClick={e => this.onClickItem(obj.sort)}>
                                        <div className="hometopbox">
                                            <div className="hometopbox_img">
                                                <div className="hide">
                                                    <img
                                                        src={obj.picUrl}
                                                        alt=""
                                                    />
                                                </div>
                                                <div className="hover">
                                                    <img
                                                        src={obj.picHoverUrl}
                                                        alt=""
                                                    />
                                                </div>
                                            </div>
                                            <span>{obj.title}</span>
                                            {obj.text}
                                        </div>
                                    </div>
                                )
                            })}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

HomeSlider.propTypes = {
    menu: MPropTypes.observableObject
}
