import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'

import { Breadcrumbs, Loading, SideWrapper } from 'components'


@inject('menu', 't')
@observer
export default class MenuBooks extends React.Component{
    render(){
        const { menu } = this.props

        if (!menu.ami || !menu.contentItems){
            return <Loading />
        }

        return (
            <React.Fragment>
                <div className="container middle">
        
                    <Breadcrumbs />

                    <h1>{menu.ami.h1}</h1>

                    <div className="row booksrow">
                        {menu.contentItems.map(obj => {
                            return (
                                <div key={obj.id} className="grid4 tb6 sm12">
                                    <div className="module bookshort">
                                        <img
                                            src="/images/icons/book.png"
                                            alt=""
                                            srcSet="/images/icons/book-2x.png 2x" />
                                        
                                        <div className="bookshort_name">
                                            <Link to={obj.url}>{obj.name}</Link>
                                        </div>
                                        <div className="bookshort_text">
                                            {obj.shortDescription}
                                        </div>
                                        <Link
                                            to={obj.url}
                                            className="btn">
                                            {'Скачать бесплатно'}
                                        </Link>
                                    </div>
                                </div>
                            )
                        })}
                       
                    </div>
                </div>

                <SideWrapper isDesktopHidden />
            </React.Fragment>

        )
    }
}


MenuBooks.propTypes = {
    menu: MPropTypes.observableObject,
    t: MPropTypes.observableObject
}