import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'
import { rHTML } from 'services/helpers'
import {
    Breadcrumbs,
    Loading,
    Paginator,
    SideRecommend,
    SideWrapper,
} from 'components'


@inject('menu', 'paginator')
@observer
export default class MenuContentPic extends React.Component{
    render(){
        const { menu, paginator } = this.props

        if (!paginator.paginatedItems || !menu.ami){
            return <Loading />
        }

        return (
            <React.Fragment>
                <div className="container middle">
                    <div className="brick">
                        <SideWrapper />

                        <div className="brick_content">
                            <div className="brick_box">
                                <Breadcrumbs />

                                <h1>{menu.ami.h1}</h1>
    
                                {paginator.paginatedItems.map(obj => {
                                    return (
                                        <div key={obj.id} className="module articlebox articlebox_new">
                                            <div className="module_content">
                                                <div className="articlebox_date">{obj.rusDateTimeStr('createdAt')}</div>
                                                <div className="articlebox_name">
                                                    <Link to={obj.url}>{obj.name}</Link>
                                                </div>
                                                
                                                {(obj.tags && obj.tags.length > 0) &&
                                                    <div className="articlebox_meta">
                                                        <div className="articlebox_cat">
                                                            {obj.tags.map((tag, index) => {
                                                                return (
                                                                    <React.Fragment key={tag.id}>
                                                                        <Link to={tag.url}>{tag.name}</Link>
                                                                        {(index < obj.tags.length - 1) &&  ', '}
                                                                    </React.Fragment>
                                                                )           
                                                            })}
                                                        </div>
                                                    </div>
                                                }

                                                <div className="articlebox_cover">
                                                    <Link to={obj.url}>
                                                        <img src={obj.picUrl} alt="" />
                                                    </Link>
                                                </div>
                                                
                                                {rHTML(obj.shortDescription, 'articlebox_txt')}
                                                
                                                <div className="articlebox_meta">
                                                    {(obj.url && obj.url.length > 0) &&
                                                        <Link to={obj.url} className="btn btn-more">
                                                            <b>{'Читать дальше'}</b>
                                                        </Link>
                                                    }
                                                    <div className="articlebox_comments_wrap">
                                                        <span className="articlebox_comments">
                                                            <Link to="/">{'20 Комментариев'}</Link>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    )  
                                })}

                                <Paginator />

                            </div>
                        </div>
                    </div>

                    <SideRecommend />

                </div>
            </React.Fragment>
        )
    }
}


MenuContentPic.propTypes = {
    menu: MPropTypes.observableObject,
    paginator: MPropTypes.observableObject
}
