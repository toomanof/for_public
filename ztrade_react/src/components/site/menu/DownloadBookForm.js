import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { formToObject } from 'services/helpers'

import { CountryModel } from 'models'
import {
    Checkbox,
    ButtonSubmit,
    ErrorBlock,
    Input,
    Select
} from 'components'



/**
 * Not auth user fill the form.
 * Auth user just press button (fields are filled from user's profile).
 */
@inject('alert', 'general', 'menu', 'user')
@observer
export default class DownloadBookForm extends React.Component{
    constructor(props){
        super(props)
        this.formRef = React.createRef()
    }

    state = {
        isCheckboxChecked: false
    }

    componentDidMount(){
        this.props.general.loadCountries()   
    }

    componentWillUnmount(){
        this.props.user.clear()   
    }

    onSuccess = () => {
        this.formRef.current.reset()
        this.props.alert.createAlert('Информация отправлена на email')
    }

    onSubmit = (e) => {
        e.preventDefault()
        const { user } = this.props
        
        let form = {}
        let isAuth

        if (user.isAuthenticated){
            isAuth = true
            form.first_name = user.profile.firstName
            form.last_name = user.profile.lastName
            form.email = user.profile.email
            form.phone = user.profile.phone
            form.country_id = user.profile.country.id
        } else {
            isAuth = false
            form = formToObject(e.target, ['country_id'])
        }
        
        const bookId = this.props.menu.ami.id
        this.props.user.downloadBookRequest(bookId, form, isAuth, this.onSuccess)
    }

    onChangeCheckbox = (value) => {
        this.setState({isCheckboxChecked: value})
    }

    onToggleCheckbox = (e) => {
        e.preventDefault()
        this.setState({isCheckboxChecked: !this.state.isCheckboxChecked})
    }

    get isButtonEnabled(){
        return this.state.isCheckboxChecked
    }

    render(){
        const { general, user } = this.props

        if (!general.countries){
            return null
        }

        if (user.isAuthenticated){
            return (
                <div className="module download_book">
                    <div className="download_form">
                        <form ref={this.formRef} onSubmit={this.onSubmit}>
                            <ButtonSubmit
                                isButtonEnabled
                                text="Скачать бесплатно"
                                className="btn btn-blue btn-big" />
                        </form>
                    </div>
                </div>
            )
        }


        return (
            <div className="module download_book">
                <div className="module_title">{'Скачать книгу бесплатно'}</div>
                <small>{'Заполните форму, чтобы скачать книгу'}</small>
                
                <div className="download_form">
                    <form ref={this.formRef} onSubmit={this.onSubmit}>
                        <Input
                            name="first_name"
                            placeholder="Имя"
                            errors={user.errors} />

                        <Input
                            name="last_name"
                            placeholder="Фамилия"
                            errors={user.errors} />

                        <Input
                            type="email"
                            name="email"
                            placeholder="E-mail"
                            errors={user.errors} />

                        <Input
                            name="phone"
                            placeholder="Телефон"
                            errors={user.errors} />

                        <Select
                            name="country_id"
                            options={CountryModel.selectOptions}
                            errors={user.errors} />
                        
                        <Checkbox
                            label="Я принимаю Политику конфиденциальности и подтверждаю, что мне больше 18 лет"
                            onChange={this.onChangeCheckbox} />

                        <ErrorBlock errors={user.errors} />

                        <ButtonSubmit
                            isButtonEnabled={this.isButtonEnabled}
                            text="Продолжить"
                            className="btn btn-blue btn-big" />

                    </form>
                </div>
            </div>
        )
    }
}


DownloadBookForm.propTypes = {
    alert: MPropTypes.observableObject,
    general: MPropTypes.observableObject,
    menu: MPropTypes.observableObject,
    user: MPropTypes.observableObject
}