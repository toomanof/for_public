import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { rHTML } from 'services/helpers'
import { BOOK } from 'services/constants'
import {
    Breadcrumbs,
    DownloadBookForm,
    SideRecommend,
    SideWrapper,
    Loading
} from 'components'

/**
 * For content with type BOOK, show form to download book.
 * contentItem is ami item.
 */

@inject('menu')
@observer
export default class ContentItem extends React.Component {
    render() {
        const { menu } = this.props

        if (!menu.ami) {
            return <Loading />
        }

        return (
            <React.Fragment>
                <div className="container middle">
                    <div className="brick">
                        <SideWrapper />

                        <div className="brick_content">
                            <div className="brick_box">
                                <Breadcrumbs />

                                <h1>{menu.ami.h1}</h1>

                                <div className="module page">
                                    {menu.ami.picUrl && (
                                        <div className="page_cover2">
                                            <img src={menu.ami.picUrl} alt="" />
                                        </div>
                                    )}

                                    {rHTML(menu.ami.content, 'page_entry')}
                                </div>

                                {menu.ami.type === BOOK && <DownloadBookForm />}
                            </div>
                        </div>
                    </div>

                    <SideRecommend />
                </div>
            </React.Fragment>
        )
    }
}

ContentItem.propTypes = {
    menu: MPropTypes.observableObject
}
