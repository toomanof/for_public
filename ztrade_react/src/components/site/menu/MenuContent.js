import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { rHTML } from 'services/helpers'
import {
    Breadcrumbs,
    SideRecommend,
    SideWrapper,
    Loading,
} from 'components'



@inject('menu')
@observer
export default class MenuContent extends React.Component{
    render(){
        const { menu } = this.props

        if (!menu.ami){
            return <Loading />
        }

        return (
            <React.Fragment>
                <div className="container middle">
                    <div className="brick">
                        <SideWrapper />

                        <div className="brick_content">
                            <div className="brick_box">
                                <Breadcrumbs />

                                <h1>{menu.ami.h1}</h1>
                                
                                <div className="module page">
                                    {menu.ami.picUrl &&
                                        <div className="page_cover2">
                                            <img src={menu.ami.picUrl} alt="" />
                                        </div>
                                    }

                                    {rHTML(menu.ami.content, 'page_entry')}
                                   
                                </div>

                            </div>
                        </div>
                    </div>

                    <SideRecommend />

                </div>
            </React.Fragment>
        )
    }
}


MenuContent.propTypes = {
    menu: MPropTypes.observableObject
}