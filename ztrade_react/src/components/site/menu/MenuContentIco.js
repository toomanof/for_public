import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'
import { rHTML } from 'services/helpers'
import {
    Breadcrumbs,
    Loading,
    Paginator,
    SideRecommend,
    SideWrapper,
} from 'components'


@inject('menu', 'paginator')
@observer
export default class MenuContentIco extends React.Component{
    render(){
        const { menu, paginator } = this.props

        if (!paginator.paginatedItems || !menu.ami){
            return <Loading />
        }

        return (
            <React.Fragment>
                <div className="container middle">
                    <div className="brick">
                        <SideWrapper />

                        <div className="brick_content">
                            <div className="brick_box">
                                <Breadcrumbs />

                                <h1>{menu.ami.h1}</h1>

                                {paginator.paginatedItems.map(obj => {
                                    return (
                                        <div key={obj.id} className="module review review_new">
                                            <div className="review_clr">
                                                <div className="review_image">
                                                    <img src={obj.icoUrl} alt="" srcSet={`${obj.icoUrl} 2x`} />
                                                </div>
                                                
                                                <div className="review_text">
                                                    <div className="articlebox_name">
                                                        <Link to={obj.url}>{obj.name}</Link>
                                                    </div>
                                                    <div className="articlebox_date">{obj.rusDateTimeStr('createdAt')}</div>
                                                    {rHTML(obj.shortDescription)}
                                                </div>
                                            </div>
                                            
                                            <div className="review_meta">
                                                {(obj.url && obj.url.length > 0) &&
                                                    <Link to={obj.url} className="btn btn-more">
                                                        <b>{'Читать дальше'}</b>
                                                    </Link>
                                                }
                                                
                                                <div className="articlebox_comments_wrap">
                                                    <span className="articlebox_comments">
                                                        <Link to="/">{'20 Комментариев'}</Link>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    )  
                                })}

                                <Paginator />

                            </div>
                        </div>
                    </div>

                    <SideRecommend />

                </div>
            </React.Fragment>
        )
    }
}


MenuContentIco.propTypes = {
    menu: MPropTypes.observableObject,
    paginator: MPropTypes.observableObject
}