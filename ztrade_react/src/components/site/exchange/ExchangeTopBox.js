import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'

/**
 * Exchange detail page.
 * Top box.
 * link1 and link2 actions.
 * If link starts with http, then open Modal window.
 */
@inject('exchange', 'menu', 'modal')
@observer
export default class ExchangeTopBox extends React.Component {

    onClickWhichAccountOpen = (e) => {
        e.preventDefault()
        this.props.modal.openExchangeWhichAccountModal()
        return false;
    }
    onClickPayButton = () => {

    }
    render() {
        const { exchange, modal } = this.props
        const item = exchange.exchangeItem

        return (
            <div className="module pammInfo">
                <div className="pammInfo_inner">
                    <div className="pammInfo_cell pammInfo_img">
                        <img
                            src={item.logoUrl}
                            alt=""
                            srcSet={`${item.logoSrcsetUrl} 2x`}
                        />
                    </div>

                    <div className="pammInfo_cell">
                        <div className="pammInfo_btnbox">
                            <a
                                rel="noopener"
                                target="_blank"
                                href={item.siteUrl}
                                className="btn btn-blue">
                                {'Открыть счет'}
                            </a>
                        </div>

                        <div className="pammInfo_btnbox">
                            <a
                                rel="noopener"
                                target="_blank"
                                href={item.siteUrl}
                                className="btn btn-green">
                                {'Сайт биржи'}
                            </a>
                        </div>
                    </div>
                    <div className="pammInfo_cell">
                        <div className="brokers_dig">
                            {item.tradingVolumes.map((obj, index) => {
                                let clasname = index % 2 == 0 ? "green" : "orange"
                                let comma = (index < item.tradingVolumes.length -1 ) ? "," : ""
                                return (
                                    <React.Fragment key={obj.id}>
                                        {obj.value.toLocaleString()}
                                        <span className={clasname}> {obj.iso}</span>{comma}<br/>
                                    </React.Fragment>
                                )
                            })}
                            <span className="red">Объем торгов</span>
                        </div>
                        <div className="brokers_dig">
                            {item.localizations.map((obj, index) => {
                                let clasname = "orange"
                                if (index % 2 == 0)
                                    clasname = "red"
                                if (index % 3 == 0)
                                    clasname = "green"
                                let comma = (index < item.localizations.length -1 ) ? "," : ""
                                return (
                                    <React.Fragment key={obj.id}>
                                        <span className={clasname}> {obj.iso}</span>{comma}
                                    </React.Fragment>
                                )
                            })}
                            <br/>
                            Языки
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

ExchangeTopBox.propTypes = {
    exchange: MPropTypes.observableObject,
    menu: MPropTypes.observableObject,
    modal: MPropTypes.observableObject
}
