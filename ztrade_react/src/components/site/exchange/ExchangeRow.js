import React from 'react'
import PropTypes from 'prop-types'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'

/**
 * Table featured row on Exchange List page
 */
@inject('menu')
@observer
export default class ExchangeRowFeatured extends React.Component {

    get сommissionClass() {
        const { item } = this.props
        if (item.сommission === 1) {
            return ['spred_box gr', ' spred_box v2', 'spred_box v3']
        } else if (item.сommission === 2) {
            return ['spred_box or', ' spred_box v2 or', 'spred_box v3']
        } else if (item.сommission === 3) {
            return ['spred_box red', ' spred_box v2 red', 'spred_box v3 red']
        }
        return ['', '', '']
    }

    render() {
        const { item } = this.props

        return (
            <div className="brokers_row">
                    <Link to={item.frontendUrl} className="brokers_cell brokers_img">
                        <img
                            src={item.logoListUrl}
                            alt=""
                            srcSet={`${item.logoListSrcsetUrl} 2x`}
                        />
                    </Link>

                    <div className="brokers_cell brokers_rate">
                        <small>Объем торгов</small>
                        <div className="brokers_sm">
                            {item.tradingVolumes.map(obj => {
                                return (
                                    <React.Fragment key={obj.id}>
                                        {obj.value.toLocaleString()} {obj.iso} <br></br>
                                    </React.Fragment>
                                )
                            })}
                        </div>
                        <div className="brokers_dig">
                            <div className="brokers_comments">
                                <span className="green">{item.countPositiveReviews}</span>
                                <span className="orange">{item.countNeutralReviews}</span>
                                <span className="red">{item.countNegativeReviews}</span>
                            </div>
                            Отзывы
                        </div>
                    </div>
                    <div className="brokers_cell brokers_crypt">
                        <small>Криптовалюты</small>
                        <div className="brokers_sm">
                            {item.currencies.map(obj => {
                                return (
                                    <React.Fragment key={obj.id}>
                                        {obj.iso},
                                    </React.Fragment>
                                )
                            })}
                        </div>
                    </div>
                    <div className="brokers_cell brokers_int">
                        <small>Интерфейс</small>
                        <div className="brokers_dig">
                            <div className="brokers_comments">
                                {item.localizations.map( (obj, index) => {
                                    let className = "orange"
                                    if(index % 2){className = "red"}
                                    if(index % 3){className = "green"}
                                    return (
                                        <React.Fragment key={obj.id}>
                                            <span key={obj.id} className={className}>{obj.iso}</span>,
                                        </React.Fragment>
                                    )
                                })}
                            </div>
                            Языки
                        </div>
                    </div>
                    <div className="brokers_cell brokers_fee">
                        <small>Комиссия</small>
                        <div className="spred_outer">
                            <div className="spred">
                                <b className={this.сommissionClass[0]} />
                                <b className={this.сommissionClass[1]} />
                                <b className={this.сommissionClass[2]} />
                            </div>
                            %
                        </div>
                    </div>
                </div>

        )
    }
}

ExchangeRowFeatured.propTypes = {
    item: PropTypes.object,
    menu: MPropTypes.observableObject
}
