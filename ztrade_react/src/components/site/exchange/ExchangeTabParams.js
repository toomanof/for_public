import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { rHTML } from 'services/helpers'


/**
 * E detail page.
 * Params Tab.
 */
@inject('exchange', 'menu')
@observer
export default class ETabParams extends React.Component{
    render(){
        const { exchange } = this.props
        const item = exchange.exchangeItem

        return (
            <React.Fragment>

                <div className="module brokerInfo">
                    {item.params.map(obj => {
                        return (
                            <p key={obj.id}>
                                <b>{obj.name}</b><br />
                                {obj.value}
                            </p>                            
                        )
                    })}
                </div>

                {rHTML(item.content, 'text')}

            </React.Fragment>
        )
    }
}


ETabParams.propTypes = {
    exchange: MPropTypes.observableObject,
    menu: MPropTypes.observableObject
}