import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Modal, SocialBox } from 'components'
import { rHTML } from 'services/helpers'
import { ADVANTAGE, DISADVANTAGE } from 'services/constants'

/**
 * Exchange detail page.
 * Info Tab.
 */
@inject('alert', 'exchange', 'menu', 'user')
@observer
export default class ExchangeTabInfo extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            isAddAdvantageModalOpened: false,
            isAddDisadvantageModalOpened: false,
            descText: ''
        }
    }

    onClickAddAdvantage = e => {
        e.preventDefault()
        if (!this.props.user.isAuthenticated) {
            this.showAuthAlert()
            return
        }
        this.setState({ isAddAdvantageModalOpened: true, descText: '' })
    }

    onClickAddDisadvantage = e => {
        e.preventDefault()
        if (!this.props.user.isAuthenticated) {
            this.showAuthAlert()
            return
        }
        this.setState({ isAddDisadvantageModalOpened: true, descText: '' })
    }

    onClickCloseAddAdvantageModal = () => {
        this.setState({ isAddAdvantageModalOpened: false })
    }

    onClickCloseAddDisadvantageModal = () => {
        this.setState({ isAddDisadvantageModalOpened: false })
    }

    onChangeDesc = e => {
        this.setState({ descText: e.target.value })
    }

    /**
     * Submit advantage or disadvantage
     */
    onSubmitDesc = (type, e) => {
        e.preventDefault()
        const { alert, exchange } = this.props

        let text = this.state.descText
        text = text.trim()

        if (text.length === 0) {
            const msg =
                type === ADVANTAGE
                    ? 'Введите преимущество'
                    : 'Введите недостаток'
            alert.createAlert(msg, 'danger')
            return
        }

        const form = {
            type: type,
            text: text,
            exchange_id: exchange.exchangeItem.id
        }
        exchange.addDesc(form, this.onSubmitDescSuccess)
    }

    onSubmitDescSuccess = () => {
        this.onClickCloseAddAdvantageModal()
        this.onClickCloseAddDisadvantageModal()
    }

    showAuthAlert = () => {
        this.props.alert.createAlert(
            'Добавления преимуществ и недостатков брокера доступно только для зарегистрированных пользователей. Требуется аутентификация.',
            'danger',
            false
        )
    }

    renderTextItems() {
        const { exchange } = this.props
        const item = exchange.exchangeItem
        if (item.textItemsList.length === 0) {
            return null
        }

        return (
            <div className="row brokerInfo_list">
                <ul className="grid4 sm12">
                    {item.textItemsList[0].map((name, index) => {
                        return <li key={index}>{name}</li>
                    })}
                </ul>

                <ul className="grid4 sm12">
                    {item.textItemsList[1].map((name, index) => {
                        return <li key={index}>{name}</li>
                    })}
                </ul>

                <ul className="grid4 sm12">
                    {item.textItemsList[2].map((name, index) => {
                        return <li key={index}>{name}</li>
                    })}
                </ul>
            </div>
        )
    }

    incrementDesc = id => {
        const { exchange } = this.props
        exchange.incrementDesc(id)
    }

    render() {
        const { exchange } = this.props
        const item = exchange.exchangeItem

        return (
            <React.Fragment>
                <div className="module brokerInfo">
                    <div className="broker_adv">
                        <div className="row">
                            <div className="grid4 sm12">
                                <b>{'Торговые платформы'}</b>{' '}
                                {item.tradePlatform}
                            </div>
                            <div className="grid4 sm12">
                                <b>{'Регулирование'}</b> {item.regulation}
                            </div>
                            <div className="grid4 sm12">
                                <b>{'Платежные системы на вывод'}</b>{' '}
                                {item.payout}
                            </div>
                        </div>
                    </div>

                    {this.renderTextItems()}

                    <div className="advants_row">
                            <div className="advbox">
                                <div className="advbox_top">
                                    <div className="advbox_title">
                                        {'Преимущества'}
                                    </div>
                                    <ul
                                        className="list_scroll"
                                        style={{ minHeight: '300px' }}>
                                        {item.advantages.map(obj => {
                                            return (
                                                <li key={obj.id}>
                                                    <span
                                                        onClick={e =>
                                                            this.incrementDesc(
                                                                obj.id
                                                            )
                                                        }
                                                        className="advbox_like"
                                                    />
                                                    {obj.text}{' '}
                                                    <b>
                                                        {'+'}
                                                        {obj.count}
                                                    </b>
                                                </li>
                                            )
                                        })}
                                    </ul>
                                </div>
                                <div className="advbox_bottom">
                                    <a
                                        href="/"
                                        onClick={this.onClickAddAdvantage}
                                        className="btn btn-green btn-big">
                                        {'Добавить преимущество'}
                                    </a>
                                </div>
                            </div>

                            <div className="advbox red">
                                <div className="advbox_top">
                                    <div className="advbox_title">
                                        {'Недостатки'}
                                    </div>
                                    <ul
                                        className="list_scroll"
                                        style={{ minHeight: '300px' }}>
                                        {item.disadvantages.map(obj => {
                                            return (
                                                <li key={obj.id}>
                                                    <button
                                                        className="advbox_like"
                                                        onClick={e =>
                                                            this.incrementDesc(
                                                                obj.id
                                                            )
                                                        }
                                                    />
                                                    {obj.text}{' '}
                                                    <b>
                                                        {'+'}
                                                        {obj.count}
                                                    </b>
                                                </li>
                                            )
                                        })}
                                    </ul>
                                </div>
                                <div className="advbox_bottom">
                                    <a
                                        onClick={this.onClickAddDisadvantage}
                                        href="/"
                                        className="btn btn-red btn-big">
                                        {'Добавить недостаток'}
                                    </a>
                                </div>
                            </div>
                    </div>

                    <SocialBox />
                </div>

                {rHTML(item.content, 'text')}

                {this.state.isAddAdvantageModalOpened && (
                    <Modal
                        className="advmodal"
                        onClickClose={this.onClickCloseAddAdvantageModal}>
                        <React.Fragment>
                            <div className="modal_header">
                                <span>{'Добавить преимущество'}</span>
                            </div>
                            <div className="inpf">
                                <textarea
                                    onChange={this.onChangeDesc}
                                    className="styler"
                                    placeholder="Введите преимущество"
                                />
                            </div>
                            <div className="row modalButtons">
                                <div className="grid6">
                                    <input
                                        onClick={
                                            this.onClickCloseAddAdvantageModal
                                        }
                                        type="button"
                                        className="btn btn-red btn-big"
                                        value="Закрыть"
                                    />
                                </div>
                                <div className="grid6">
                                    <input
                                        onClick={this.onSubmitDesc.bind(
                                            null,
                                            ADVANTAGE
                                        )}
                                        type="submit"
                                        className="btn btn-green btn-big"
                                        value="Добавить"
                                    />
                                </div>
                            </div>
                        </React.Fragment>
                    </Modal>
                )}

                {this.state.isAddDisadvantageModalOpened && (
                    <Modal
                        className="advmodal"
                        onClickClose={this.onClickCloseAddDisadvantageModal}>
                        <React.Fragment>
                            <div className="modal_header">
                                <span>{'Добавить недостаток'}</span>
                            </div>
                            <div className="inpf">
                                <textarea
                                    onChange={this.onChangeDesc}
                                    className="styler"
                                    placeholder="Введите недостаток"
                                />
                            </div>
                            <div className="row modalButtons">
                                <div className="grid6">
                                    <input
                                        onClick={
                                            this
                                                .onClickCloseAddDisadvantageModal
                                        }
                                        type="button"
                                        className="btn btn-red btn-big"
                                        value="Закрыть"
                                    />
                                </div>
                                <div className="grid6">
                                    <input
                                        onClick={this.onSubmitDesc.bind(
                                            null,
                                            DISADVANTAGE
                                        )}
                                        type="submit"
                                        className="btn btn-green btn-big"
                                        value="Добавить"
                                    />
                                </div>
                            </div>
                        </React.Fragment>
                    </Modal>
                )}
            </React.Fragment>
        )
    }
}

ExchangeTabInfo.propTypes = {
    alert: MPropTypes.observableObject,
    exchange: MPropTypes.observableObject,
    menu: MPropTypes.observableObject,
    user: MPropTypes.observableObject
}
