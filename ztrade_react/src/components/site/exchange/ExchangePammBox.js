import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'

/**
 * Exchange's PAMM page.
 * Top box.
 */
@inject('exchange', 'menu')
@observer
export default class ExchangePammBox extends React.Component {
    render() {
        const { exchange } = this.props
        const item = exchange.exchangeItem

        return (
            <div className="module">
                <div className="pammport">
                    <div className="pammport_in">
                        <div className="pammport_chart">
                            <div
                                className="pammport_chartwrap"
                                style={{
                                    backgroundImage: `url(${item.pammPicUrl})`
                                }}>
                                <div className="pammport_chartwrap_text">
                                    {'Результаты ТОП-5 за последние 12 недель'}
                                </div>
                            </div>
                        </div>
                        <div className="pammport_rt">
                            <div className="pammport_title">
                                {'Проинвестируйте в ТОП-5 стратегий'}
                            </div>
                            <div className="pammport_text">
                                {
                                    'Инвестирование в несколько стратегий снижает риски'
                                }
                            </div>
                            <div className="pammport_perc">
                                <p>
                                    {'Средняя месячная доходность'}
                                    <br />
                                    {' ТОП-5 стратегий:'}
                                </p>
                                <b>
                                    {'+'}
                                    {item.pammPerc}
                                    {' %'}
                                </b>
                            </div>
                            <a
                                rel="noopener"
                                target="_blank"
                                href={item.pammLink}
                                className="btn btn-green">
                                {'Инвестировать'}
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

ExchangePammBox.propTypes = {
    exchange: MPropTypes.observableObject,
    menu: MPropTypes.observableObject
}
