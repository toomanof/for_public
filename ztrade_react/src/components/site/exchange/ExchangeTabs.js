import React from 'react'
import PropTypes from 'prop-types'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import classNames from 'classnames'


const INFO = 'info'
const PARAMS = 'params'
const TYPES = 'types'
const REVIEWS = 'reviews'


/**
 * Exchange detail page.
 * Tabs header.
 */
@inject('exchange', 'menu')
@observer
export default class ExchangeTabs extends React.Component{
    get tabs(){
        const { exchange } = this.props

        return [
            {name: 'Информация', type: INFO},
            {name: 'Параметры', type: PARAMS},
            {name: 'Типы счетов', type: TYPES},
            {name: `Отзывы (${exchange.exchangeItem.countReviews})`, type: REVIEWS},
        ]
    }

    onClickItem = (type, e) => {
        e.preventDefault()
        this.props.onClickTab(type)
    }

    onChangeSelect = (e) => {
        this.props.onClickTab(e.target.value)   
    }

    render(){
        return (
            <React.Fragment>
                <ul className="tabs hidden-xs">
                    {this.tabs.map((obj, index) => {
                        const cn = classNames('', {active: obj.type === this.props.activeType})
                        return (
                            <li
                                key={index}
                                className={cn}>
                                <a
                                    href=""
                                    onClick={this.onClickItem.bind(null, obj.type)}>

                                    {obj.name}
                                </a>
                            </li>                            
                        )                       
                    })}
                </ul>

                <div className="seltabs visible-xs">
                    <select onChange={this.onChangeSelect} className="selectbox">
                        {this.tabs.map((obj, index) => {
                            return (
                                <option key={index} value={obj.type}>
                                    {obj.name}
                                </option>
                            )                       
                        })}
                    </select>
                </div>
            </React.Fragment>
        )
    }
}


ExchangeTabs.propTypes = {
    activeType: PropTypes.string,
    exchange: MPropTypes.observableObject,
    menu: MPropTypes.observableObject,
    onClickTab: PropTypes.func
}