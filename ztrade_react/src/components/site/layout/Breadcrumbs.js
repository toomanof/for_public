import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'
import { logGroupMessage } from 'services/helpers'

@inject('menu')
@observer
export default class Breadcrumbs extends React.Component{
    /**
     * Links except last breadcrumb
     */
    get breadcrumbs(){
        const { menu } = this.props
        logGroupMessage(
            'breadcrumbs стр.14 in BreadCrumbs',
            menu,
            [...menu.ami.breadcrumbs.slice(0, menu.ami.breadcrumbs.length - 1)])
        return [...menu.ami.breadcrumbs.slice(0, menu.ami.breadcrumbs.length - 1)]
    }

    get lastBreadcrumb(){
        const { menu } = this.props
        return menu.ami.breadcrumbs[menu.ami.breadcrumbs.length - 1]
    }

    render(){
        const { menu } = this.props

        if (!menu.ami || !menu.ami.breadcrumbs){
            return null
        }
        logGroupMessage('BreadCrumbs стр.28', menu)
        return (
            <div className="breadcrumbs">
                {this.breadcrumbs.map((obj, index) => {
                    return (
                        <React.Fragment key={index}>
                            <Link to={obj.url}>{obj.name}</Link>
                            {' / '}
                        </React.Fragment>

                    )
                })}
                
                {this.lastBreadcrumb.name}
            </div>
        )
    }
}


Breadcrumbs.propTypes = {
    menu: MPropTypes.observableObject
}
