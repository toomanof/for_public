import React from 'react'
import PropTypes from 'prop-types'
import { DefaultPlayer as Video } from 'react-html5video'
import 'react-html5video/dist/styles.css'


/**
 * fileUrl - direct link to mp4 file
 * link - link to youtube video
 * height - iframe height for youtube video
 *
 * If link passed - show iframe with youtube link
 * otherwise show fileUrl as HTML5 video
 */
export default class Player extends React.Component{
    static get defaultProps() {
        return {
            link: null,
        }
    }

    get height(){
        if (this.props.height){
            return this.props.height.toString()
        }
        return 'auto'
    }

    render(){
        const { fileUrl, link, height } = this.props

        if (link && link.length > 0){
            return (
                <iframe
                    allowFullScreen
                    width="100%"
                    height={this.height}
                    frameBorder="0"
                    src={link} />
            )
        }

        return (
            <Video
                controls={['PlayPause', 'Seek', 'Time', 'Volume', 'Fullscreen']}
                onCanPlayThrough={() => {
                    // Do stuff
                }}>
                <source src={fileUrl} type="video/mp4" />
            </Video>
        )
    }
}


Player.propTypes = {
    fileUrl: PropTypes.string,
    height: PropTypes.number,
    link: PropTypes.string,
}