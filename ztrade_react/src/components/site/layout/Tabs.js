import React from 'react'
import PropTypes from 'prop-types'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import classNames from 'classnames'

/**
 * Component uses data from TabsStore
 * Tabs content passed via children nodes.
 * The quantity of nodes should be equal the quantity of tabs.
 */
@inject('tabs')
@observer
export default class Tabs extends React.Component {
    onClickTab = (index, e) => {
        e.preventDefault()
        this.props.tabs.setActiveIndex(index)
    }

    onChangeTab = e => {
        const index = parseInt(e.target.value, 10)
        this.props.tabs.setActiveIndex(index)
    }

    render() {
        const { children, tabs, selectClassName } = this.props

        const cnSelect = selectClassName
            ? `seltabs ${selectClassName} visible-sm`
            : 'seltabs visible-sm'

        return (
            <React.Fragment>
                <ul className="usertabs hidden-sm">
                    {tabs.arr.map((obj, index) => {
                        let active = index === tabs.activeIndex ? true : false

                        const firstClass = obj.hasOwnProperty('className')
                            ? obj.className
                            : ''
                        let cn = classNames(firstClass, { active: active })

                        return (
                            <li key={index} className={cn}>
                                <a
                                    onClick={this.onClickTab.bind(null, index)}
                                    href="">
                                    {obj.name}
                                </a>
                            </li>
                        )
                    })}
                </ul>

                <div className={cnSelect}>
                    <select
                        onChange={this.onChangeTab}
                        className="selectbox"
                        value={tabs.activeIndex}>
                        {tabs.arr.map((obj, index) => {
                            return (
                                <option value={index} key={index}>
                                    {obj.name}
                                </option>
                            )
                        })}
                    </select>
                </div>

                {React.Children.toArray(children).map((child, index) => {
                    const active = index === tabs.activeIndex ? true : false
                    const style = active ? 'block' : 'none'
                    return (
                        <div key={index} style={{ display: style }}>
                            {child}
                        </div>
                    )
                })}
            </React.Fragment>
        )
    }
}

Tabs.propTypes = {
    children: PropTypes.arrayOf(PropTypes.node),
    selectClassName: PropTypes.string,
    tabs: MPropTypes.observableObject
}
