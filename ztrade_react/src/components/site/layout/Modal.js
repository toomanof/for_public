import React from 'react'
import PropTypes from 'prop-types'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import classNames from 'classnames'
import { logMessage } from 'services/helpers'


@inject('modal')
@observer
export default class Modal extends React.Component{
    static defaultProps = {
        className: '',
        isClickOutsideCloseModal: true
    }

    componentDidMount () {
        this.props.modal.open()
        if (this.props.isClickOutsideCloseModal){
            document.addEventListener('click', this.handleClick)
        }
    }

    componentWillUnmount () {
        if (this.props.isClickOutsideCloseModal){
            document.removeEventListener('click', this.handleClick)    
        }
        this.props.modal.close()
    }

    handleClick = (e) => {
        logMessage('Modal: click outside')
        const area = this.modalRef.current
        if (area && !area.contains(e.target)){
            this.props.onClickClose()
        }    
    }

    onClickClose = (e) => {
        e.preventDefault()
        this.props.onClickClose()
    }

    render(){
        const { children, className } = this.props

        const cn = classNames('modal', className)

        this.modalRef = React.createRef()

        return (
            <div className="modal_outer" style={{display: 'block'}}>
                <div className="modal_overlay" />
                <div className="modal_flex">
                    <div className={cn} ref={this.modalRef}>

                        {children}

                        <a
                            onClick={this.onClickClose}
                            href=""
                            className="modal_close" />
                    </div>
                </div>
            </div>

            
        )
    }
}


Modal.propTypes = {
    children: PropTypes.node,
    className: PropTypes.string,
    isClickOutsideCloseModal: PropTypes.bool,
    modal: MPropTypes.observableObject,
    onClickClose: PropTypes.func.isRequired,
}
