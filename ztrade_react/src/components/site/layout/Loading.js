import React from 'react'


export default class Loading extends React.Component{
    render(){
        return (
            <div className="center-me">
                <img src="/pics/loading.gif" alt="" />
            </div>
        )
    }
}

