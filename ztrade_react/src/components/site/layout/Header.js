import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'
import {TopHeaderSlider} from 'components'


@inject('menu', 't', 'user')
@observer
export default class Header extends React.Component{
    onClickMenu = (e) => {
        e.preventDefault()
        this.props.menu.toggleMobileMenu()
    }

    render(){
        const { t, user } = this.props

        return (
            <header className="header">
                <div className="container head_container">

                    <div className="logobox_outer">
                        <div className="logobox">
                            <Link to="/">
                                <img src="/images/logo.svg" alt="" />
                            </Link>
                        </div>
                        <div className="sitedesc hidden-tb">
                            <b>{'16'}</b>{' лет опыт в торговле'}
                            <br />{' Более '}<b>{'3 000'}</b>{' пользователей'}
                        </div>
                    </div>

                    <TopHeaderSlider />

                    <div className="head_btns">
                        {!user.isAuthenticated &&
                        <Link to="/site/signup" className="btn hidden-md">{'Зарегистрироваться'}</Link>}
                        <Link to="/site/login" className="btn hidden-md">{'Вход в личный кабинет'}</Link>
                        {!user.isAuthenticated &&
                        <Link to="/site/signup" className="signlink visible-md" />}
                        <Link to="/site/login" className="loginlink visible-md" />
                        <a
                            onClick={this.onClickMenu}
                            href=""
                            className="showmenu_sm visible-sm" />
                    </div>

                </div>
            </header>

    )
    }
}


Header.propTypes = {
    menu: MPropTypes.observableObject,
    t: MPropTypes.observableObject,
    user: MPropTypes.observableObject,
}
