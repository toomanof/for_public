import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import Centrifuge from 'centrifuge'
import { WS_CENTRIFUGO_URL } from 'api'
import { logMessage } from 'services/helpers'

let cent
const NEWS_CREATED = 'NEWS_CREATED'
const SIGNAL_CREATED = 'SIGNAL_CREATED'
const SIGNAL_STOP_LOSS_UPDATED = 'SIGNAL_STOP_LOSS_UPDATED'
const SIGNAL_TAKE_PROFIT_UPDATED = 'SIGNAL_TAKE_PROFIT_UPDATED'
const SIGNAL_CLOSED = 'SIGNAL_CLOSED'
const NOTIFICATION_CREATED = 'NOTIFICATION_CREATED'

/**
 * Component connects to Centrifugo websocket server and listens
 * for events.
 */
@inject('account', 'general', 'menu', 'sgn', 'user')
@observer
export default class WSUpdater extends React.Component {
    componentDidMount() {
        const { user } = this.props

        cent = new Centrifuge(WS_CENTRIFUGO_URL)
        cent.setToken(this.props.general.centrifugoToken)
        console.log('centrifugo connect', cent)
        // Subscribe to all account notifications
        cent.subscribe('account', msg => {
            this.process(msg.data)
        })

        // Subscribe to notifications for particular user
        cent.subscribe(`notifications${user.profile.id}`, msg => {
            this.process(msg.data)
        })

        cent.connect()
    }

    componentWillUnmount() {
        cent.disconnect()
    }

    process(data) {
        console.log('centrifugo data', data)
        switch (data.type) {
            case NEWS_CREATED:
                this.updateForexNews(data)
                return

            case SIGNAL_CREATED:
                this.signalCreated(data)
                return

            case SIGNAL_STOP_LOSS_UPDATED:
                this.signalStopLossUpdated(data)
                return

            case SIGNAL_TAKE_PROFIT_UPDATED:
                this.signalTakeProfitUpdated(data)
                return

            case SIGNAL_CLOSED:
                this.signalClosed(data)
                return

            case NOTIFICATION_CREATED:
                this.notificationCreated(data)
                return

            default:
                logMessage('WebSocket: incorrect message type')
        }
    }

    updateForexNews(data) {
        logMessage(data)
        if (this.props.user.isAuthenticated) {
            this.props.account.loadForexNews(true)
        }
    }

    signalCreated(data) {
        logMessage('signalCreated', data)
        this.props.sgn.wsSignalCreated(data)
    }

    signalStopLossUpdated(data) {
        logMessage(data)
        this.props.sgn.wsSignalStopLossUpdated(data)
    }

    signalTakeProfitUpdated(data) {
        logMessage(data)
        this.props.sgn.wsSignalTakeProfitUpdated(data)
    }

    signalClosed(data) {
        logMessage(data)
        this.props.sgn.wsSignalClosed(data)
    }

    notificationCreated(data) {
        logMessage(data)
        this.props.account.updateMarketNotifications(data)
    }

    render() {
        return <div />
    }
}

WSUpdater.propTypes = {
    account: MPropTypes.observableObject,
    general: MPropTypes.observableObject,
    menu: MPropTypes.observableObject,
    sgn: MPropTypes.observableObject,
    user: MPropTypes.observableObject
}
