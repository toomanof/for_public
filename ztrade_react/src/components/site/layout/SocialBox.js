import React from 'react'
import PropTypes from 'prop-types'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import {
    FacebookShareButton,
    GooglePlusShareButton,
    OKShareButton,
    TelegramShareButton,
    TwitterShareButton,
    WhatsappShareButton,
    VKShareButton,
} from 'share'


@inject('menu')
@observer
export default class SocialBox extends React.Component{
    static get defaultProps() {
        return {
            className: 'module_content',
        }
    }

    render(){
        const { className, menu } = this.props

        if (!menu.ami){
            return null
        }

        const shareUrl = window.location.toString()
        const title = menu.ami.title

        return (
            <div className={className}>
                    
                <TelegramShareButton
                    url={shareUrl}
                    title={title}
                    className="social_but social_te" />

                <VKShareButton
                    url={shareUrl}
                    className="social_but social_vk" />

                <OKShareButton
                    url={shareUrl}
                    className="social_but social_ok" />

                <FacebookShareButton
                    url={shareUrl}
                    quote={title}
                    className="social_but social_fb" />

                <GooglePlusShareButton
                    url={shareUrl}
                    className="social_but social_gl" />

                <TwitterShareButton
                    url={shareUrl}
                    title={title}
                    className="social_but social_tw" />

                <WhatsappShareButton
                    url={shareUrl}
                    title={title}
                    separator=":: "
                    className="social_but social_phn" />

            </div>
        )
    }
}


SocialBox.propTypes = {
    className: PropTypes.string,
    menu: MPropTypes.observableObject
}
