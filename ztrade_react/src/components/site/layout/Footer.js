import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'
import { FooterMenu } from 'components'


@inject('menu', 't')
@observer
export default class Footer extends React.Component{
    render(){
        const { t } = this.props
        console.log(this.props.menu.footerLink)
        return (
            <React.Fragment>
                <div className="copyright">
                    <div className="container">
                        <div className="copyright_table">
                            <div className="copyright_cell copyright_left">
                                <p>
                                    <span>{'© '}
                                    <Link to="/">{'Ztrade.ru'}</Link>{', 2018'}</span> 
                                    <span>{'Все права защищены'}</span>
                                </p>
                                <p>
                                    {this.props.menu.footerLink.map(obj => {
                                        console.log(obj.fileUrl)
                                        return (<span key={obj.id}>
                                                    <a href={obj.fileUrl}>
                                                        {obj.name}
                                                    </a>
                                                </span>)
                                    })}
                                </p>
                            </div>
                            
                            <div className="copyright_cell">
                                <div className="copyright_warn">
                                    {'Предупреждение о рисках'}<br />
                                    {t.d.a017}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="footmails">
                    <div className="container">
                        <span>{'Инвесторам: '}<a href={`mailto:${t.d.a003}`}>{t.d.a003}</a></span>
                        <span>{'Общие вопросы: '}<a href={`mailto:${t.d.a004}`}>{t.d.a004}</a></span>
                        <span>{'Сотрудничество: '}<a href={`mailto:${t.d.a005}`}>{t.d.a005}</a></span>
                    </div>
                </div>

                <div className="footer">
                    <div className="container">
                        <div className="copyright_table">
                            <div className="copyright_cell copyright_left2">
                                <Link to="/">
                                    <img src="/images/logo.svg" alt="" />
                                </Link>
                            </div>
                            <div className="copyright_cell">
                                <FooterMenu />
                            </div>
                        </div>
                    </div>
                </div>
                <div id="foottrig" />                
            </React.Fragment>
        )
    }
}


Footer.propTypes = {
    t: MPropTypes.observableObject
}


