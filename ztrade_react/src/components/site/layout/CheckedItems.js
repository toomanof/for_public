import React from 'react'
import PropTypes from 'prop-types'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Checkbox } from 'components'


/**
 * Component receives in props items, that is array of
 * {name: ..., id:..., isChecked: ...}
 * "All" - is special checkbox and has id=null
 * On change it updates items and send items via onChange callback
 * to parent component. Parent makes some actions and sends new items.
 * So, items are not stored here, they come from props.
 *
 * Example: items = [
 *      {id: null, name: 'All',  isChecked: true},
 *      {id: 1,    name: 'Some', isChecked: true},   
 * ]
 */
@inject('menu')
@observer
export default class CheckedItems extends React.Component{
    /**
     * "context" object is changed and got newValue
     *
     * Algo.
     * 1) Main checkbox ("All") became true.
     *    Set all to true
     *
     * 2) Main checkbox ("All") became false
     *    Set all to false
     *
     * 3) Regular checkbox became true.
     *    Check if all regular checkboxes are true,
     *    set all to true (meaning set main also to true).
     *
     * 3) Regular checkbox became false.
     *    set main to false.
     *
     */
    onCheckCheckbox = (newValue, context) => {
        // Update items
        let arr = []
        for (let obj of [...this.props.items]){
            if (obj.id === context.id){
                obj.isChecked = newValue
            }
            arr.push(obj)
        }
        
        // 1 case
        if (context.id === null && newValue === true){
            arr = arr.map(obj => {
                obj.isChecked = true
                return obj
            })
        }

        // 2 case
        if (context.id === null && newValue === false){
            arr = arr.map(obj => {
                obj.isChecked = false
                return obj
            })
        }

        // 3 case
        if (context.id !== null && newValue === true){
            let res = true
            for (let obj of arr){
                if (obj.id !== null && !obj.isChecked){
                    res = false
                    break
                }
            }

            if (res){
                arr = arr.map(obj => {
                    obj.isChecked = true
                    return obj
                })    
            }
        }

        // 4 case
        if (context.id !== null && newValue === false){
            arr = arr.map(obj => {
                if (obj.id === null){
                    obj.isChecked = false    
                }
                return obj
            })    
        }
        this.props.onChange(arr)
    }

    render(){
        return (
            <React.Fragment>
                {this.props.items.map((obj, index) => {
                    return (
                        <Checkbox
                            key={index}
                            isControllable
                            contextObj={obj}
                            onChange={this.onCheckCheckbox}
                            label={obj.name}
                            isChecked={obj.isChecked} />
                    )
                })}
            </React.Fragment>
        )
    }
}


CheckedItems.propTypes = {
    items: MPropTypes.observableArray,
    menu: MPropTypes.observableObject,
    onChange: PropTypes.func.isRequired
}
