import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'


@inject('menu', 'modal', 't')
@observer
export default class FooterMenu extends React.Component{
    onClickSupport = (e) => {
        e.preventDefault()
        this.props.modal.openSupportModal()
    }

    render(){
        return (
            <div className="footnav">
                <ul>
                    {this.props.menu.menuFooter.map(obj => {
                        return (
                            <li key={obj.id}>
                                {obj.url === '/site/support' ? 
                                    <a onClick={this.onClickSupport} href="/">
                                        {obj.name}
                                    </a>  
                                : 
                                    <Link to={obj.url}>{obj.name}</Link>
                                }
                            </li>
                        )
                    })}

                </ul>
            </div>
        )
    }
}


FooterMenu.propTypes = {
    menu: MPropTypes.observableObject,
    modal: MPropTypes.observableObject,
    t: MPropTypes.observableObject
}