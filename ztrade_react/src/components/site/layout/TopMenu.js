import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'

@inject('menu', 'modal', 't')
@observer
export default class TopMenu extends React.Component {
    onClickMenu = e => {
        e.preventDefault()
        this.props.menu.toggleMobileMenu()
    }

    onClickSupport = e => {
        e.preventDefault()
        this.props.modal.openSupportModal()
    }

    render() {
        return (
            <nav className="navmenu hidden-sm">
                <div className="container">
                    <ul>
                        {this.props.menu.menuTop.map(obj => {
                            return (
                                <li key={obj.id} className={obj.className}>
                                    {obj.url === '/site/support' ? (
                                        <a
                                            onClick={this.onClickSupport}
                                            href="/">
                                            {obj.name}
                                        </a>
                                    ) : (
                                        <Link to={obj.url}>{obj.name}</Link>
                                    )}
                                </li>
                            )
                        })}
                    </ul>
                    <div onClick={this.onClickMenu} className="showmenu" />
                </div>
            </nav>
        )
    }
}

TopMenu.propTypes = {
    menu: MPropTypes.observableObject,
    modal: MPropTypes.observableObject,
    t: MPropTypes.observableObject
}
