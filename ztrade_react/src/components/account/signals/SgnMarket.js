import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import {
    SgnRating,
    SgnRules,
    SgnSamples,
    SgnSignals,
    SgnStat,
    SgnNotifications,
    Tabs
} from 'components'

/**
 * Market signals data when activeMarket selected.
 */
@inject('sgn', 'tabs')
@observer
export default class SgnMarket extends React.Component {
    componentDidMount() {
        const { tabs } = this.props
        tabs.setTabs(this.tabs, 0)
    }

    get tabs() {
        return [
            { name: 'Сигналы', className: '' },
            { name: 'Статистика', className: '' },
            { name: 'Правила', className: '' },
        /*    { name: 'Примеры', className: '' },
            { name: 'Рейтинг', className: '' },

         */
            { name: 'Уведомления', className: '' }
        ]
    }

    render() {
        return (
            <Tabs selectClassName="seltabs_usertabs">
                <SgnSignals />
                <SgnStat />
                <SgnRules />
                {/*        <SgnSamples />
                <SgnRating />
                */}
                <SgnNotifications />
            </Tabs>
        )
    }
}

SgnMarket.propTypes = {
    sgn: MPropTypes.observableObject,
    tabs: MPropTypes.observableObject
}
