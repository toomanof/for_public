import React from 'react'
import PropTypes from 'prop-types'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import classNames from 'classnames'

@inject('account', 'payment')
@observer
export default class TariffBox extends React.Component {
    onClickPayButton = (id, e) => {
        e.preventDefault()
        const { payment } = this.props

        const form = {
            tariff_id: id
        }
        payment.createBillTariff(form)
    }

    render() {
        const { obj } = this.props
        const cn = classNames('module tarif', obj.headerCssClass)
        return (
            <div className="grid3">
                <div className={cn}>
                    <div className="tarif_head">
                        {obj.name}
                        <div className="tarif_days">
                            <span>{obj.periodStr}</span>
                        </div>
                        {obj.sale > 0 && (
                            <div className="tarif_head-sale">
                                <b>{`-${obj.sale}%`}</b>
                            </div>
                        )}
                    </div>

                    <div className="tarif_price">
                        <span className="tarif_name">{obj.subtitle}</span>

                        {obj.subtitlePic && (
                            <img
                                src={`/images/tarifs/${obj.subtitlePic}`}
                                alt=""
                                className={obj.subtitleClass}
                            />
                        )}

                        <span className="tarif_rur">
                            {parseInt(obj.price, 10)}
                            {' руб. '}
                        </span>
                    </div>

                    <ul>
                        {obj.rows.map(row => {
                            return (
                                <li key={row.id} className={row.class_name}>
                                    {row.name}
                                </li>
                            )
                        })}
                    </ul>

                    <div className="tarif_bottom">
                        <button
                            onClick={this.onClickPayButton.bind(null, obj.id)}
                            href=""
                            className="btn btn-big btn-orange">
                            {'Заказать'}
                        </button>
                    </div>
                </div>
            </div>
        )
    }
}

TariffBox.propTypes = {
    account: MPropTypes.observableObject,
    obj: PropTypes.object.isRequired,
    payment: MPropTypes.observableObject
}
