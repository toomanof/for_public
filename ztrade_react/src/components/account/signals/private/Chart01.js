import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'

const colors = ['#90ee7e', '#7cb5ec']

@inject('chart')
@observer
export default class Chart01 extends React.Component {
    get categories() {
        const data = this.props.chart.chart01Data
        return data.map(d => d.name)
    }

    get profit() {
        const data = this.props.chart.chart01Data
        return data.map(d => d.profit)
    }

    get loss() {
        const data = this.props.chart.chart01Data
        return data.map(d => Math.abs(d.loss))
    }

    get profitSum() {
        return this.profit.reduce((a, b) => a + b, 0)
    }

    get lossSum() {
        return this.loss.reduce((a, b) => a + b, 0)
    }

    render() {
        const { chart } = this.props
        if (!chart.chart01Data || chart.chart01Data.length === 0) {
            return null
        }

        const options = {
            title: {
                text: 'Статистика сделок прибыль - убыток'
            },

            colors: colors,

            xAxis: {
                categories: this.categories
            },
            labels: {
                items: [
                    {
                        html: 'Profit Factor',
                        style: {
                            left: '5px',
                            top: '5px',
                            color:
                                (Highcharts.defaultOptions.title.style &&
                                    Highcharts.defaultOptions.title.style
                                        .color) ||
                                'black'
                        }
                    }
                ]
            },
            series: [
                {
                    type: 'column',
                    name: 'Profit',
                    data: this.profit
                },
                {
                    type: 'column',
                    name: 'Loss',
                    data: this.loss
                },

                {
                    type: 'pie',
                    name: 'Pips',
                    data: [
                        {
                            name: 'Profit',
                            y: this.profitSum,
                            color: colors[0]
                        },
                        {
                            name: 'Loss',
                            y: this.lossSum,
                            color: colors[1]
                        }
                    ],
                    center: [100, 80],
                    size: 100,
                    showInLegend: false,
                    dataLabels: {
                        enabled: false
                    }
                }
            ]
        }

        return <HighchartsReact highcharts={Highcharts} options={options} />
    }
}

Chart01.propTypes = {
    chart: MPropTypes.observableObject
}
