import React from 'react'
import PropTypes from 'prop-types'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'
import { SIGNALS_NOTIFICATIONS_TAB_INDEX } from 'services/constants'
import { Checkbox2, CheckedItems } from 'components'

@inject('sgn', 'user', 'account', 'tabs')
@observer
export default class SignalFilter extends React.Component {
    componentDidMount() {
        this.props.sgn.loadSignalFilters()
    }

    onClickToggleFilter = e => {
        e.preventDefault()
        this.props.sgn.toggleSignalFilter()
    }

    onChangeSound = value => {
        const { sgn } = this.props
        const form = { is_sound: value }
        sgn.updateSignalFilter(sgn.activeMarketCode, form)
    }

    /**
     * Callback for symbols checkboxes
     * Update local state (to show immediate result)
     * Update on server (after server update they also will be updated in state again)
     */
    onChangeSymbols = items => {
        const { sgn } = this.props
        sgn.setSymbolCheckedItems(items)

        const mainItem = items.find(obj => obj.id === null)
        const checkedItems = items.filter(
            obj => obj.id !== null && obj.isChecked
        )

        const form = {
            is_all_symbols: mainItem.isChecked,
            symbol_ids: checkedItems.map(obj => obj.id)
        }
        sgn.updateSignalFilter(sgn.activeMarketCode, form)
    }

    goToNotifications = (e, type) => {
        const { account, tabs } = this.props
        e.preventDefault()
        account.setNotificationsActiveTab(type)
        tabs.setActiveIndex(SIGNALS_NOTIFICATIONS_TAB_INDEX)
    }

    renderSignalsNotificationNotReadCount() {
        const { account } = this.props
        if (
            account.signalsNotificationsNotReadCount &&
            account.signalsNotificationsNotReadCount !== 0
        )
            return <b>{account.signalsNotificationsNotReadCount}</b>
        return null
    }

    renderNewsNotificationNotReadCount() {
        const { account } = this.props
        if (
            account.newsNotificationsNotReadCount &&
            account.newsNotificationsNotReadCount !== 0
        )
            return <b>{account.newsNotificationsNotReadCount}</b>
        return null
    }

    IsAlertSubscriber() {
        const {sgn, account} = this.props
        let result = true
        if(sgn.activeMarket.isPaid)
            result = false
        if(account.freePeriod && account.freePeriod.isActive)
            result = false
        return result
    }

    render() {
        const { sgn, tabs, account } = this.props

        if (!sgn.signalFilter) {
            return null
        }
        return (
            <div className="row">
                <div className="grid6 md12">
                    <div className="alfilter_outer">
                        <div className="alfilter">
                            <div className="filtersm_cl">
                                <Checkbox2
                                    isControllable
                                    isChecked={sgn.signalFilter.isSound}
                                    onChange={this.onChangeSound}
                                    hasLabel={false}
                                />
                            </div>

                            <div className="alfilter_btns">
                                <a
                                    href="#"
                                    onClick={e =>
                                        this.goToNotifications(e, 'signals')
                                    }
                                    className="alfilter-ring">
                                    {this.renderSignalsNotificationNotReadCount()}
                                </a>
                                <a
                                    href="#"
                                    onClick={e =>
                                        this.goToNotifications(e, 'news')
                                    }
                                    className="alfilter-news">
                                    {this.renderNewsNotificationNotReadCount()}
                                </a>
                            </div>
                            {this.IsAlertSubscriber() && (
                                <div className="filtersm_name">
                                    {'Сигналы поступают с задержкой '}
                                    <b>{'2 часа'}</b>
                                    <br />
                                    <Link to={sgn.activeMarket.tariffsUrl}>
                                        {'Оформите подписку'}
                                    </Link>
                                    {' и получайте сигналы в онлайн режиме'}
                                </div>
                            )}

                            {!this.IsAlertSubscriber() && (
                                <div className="filtersm_name">
                                    {'Сигналы поступают в онлайн режиме'}
                                </div>
                            )}

                            <a
                                onClick={this.onClickToggleFilter}
                                href=""
                                className="filtersel">
                                {'Фильтр'}
                            </a>
                        </div>

                        {sgn.isSignalFilterOpened && (
                            <div className="filtersm_win">
                                <CheckedItems
                                    items={sgn.symbolCheckedItems}
                                    onChange={this.onChangeSymbols}
                                />
                            </div>
                        )}
                    </div>
                </div>
                <div className="grid6 md12">
                    <div className="signals_btns">
                        <a
                            href="#"
                            onClick={e => this.goToNotifications(e, 'signals')}
                            className="signals_btns-item ring">
                            {this.renderSignalsNotificationNotReadCount()}{' '}
                            {'Уведомления'}
                        </a>
                        <a
                            href="#"
                            onClick={e => this.goToNotifications(e, 'news')}
                            className="signals_btns-item news">
                            {this.renderNewsNotificationNotReadCount()}{' '}
                            {'Новостная лента'}
                        </a>
                    </div>
                </div>
            </div>
        )
    }
}

SignalFilter.propTypes = {
    account: MPropTypes.observableObject,
    sgn: MPropTypes.observableObject,
    tabs: MPropTypes.observableObject,
    user: MPropTypes.observableObject
}
