import React from 'react'
import PropTypes from 'prop-types'

import {inject, observer, PropTypes as MPropTypes} from 'mobx-react';

@inject('sgn')

@observer
export default class TradingChart extends React.Component {
    first_render  = true

    renderTradingChat(){
        const { sgn } = this.props

        if(!this.first_render) {
            new window.TradingView.widget({
                autosize: true,
                symbol: 'OANDA:' + sgn.activeTradingChartCode.toUpperCase(),
                interval: '240',
                timezone: 'Europe/Moscow',
                theme: 'Light',
                style: '9',
                locale: 'ru',
                toolbar_bg: '#f1f3f6',
                enable_publishing: false,
                hide_side_toolbar: false,
                allow_symbol_change: true,
                save_image: false,
                container_id: 'tradingview_e0696'
            })
        }
    }

    componentDidMount() {
        this.renderTradingChat()
    }

    render() {
        const { sgn } = this.props

        this.renderTradingChat()
        this.first_render = false
        return (
            <div className="tradingview-widget-container">
                <div id="tradingview_e0696" style={{ height: '560px' }}>
                    <div className="tradingview-widget-copyright">
                        <a
                            href={"https://ru.tradingview.com/symbols/OANDA-"+sgn.activeTradingChartCode.toUpperCase()+"/"}
                            rel="noopener"
                            target="_blank">
                            <span className="blue-text">{'График '+ sgn.activeTradingChartCode.toUpperCase()}</span>
                        </a>
                    </div>
                </div>
            </div>
        )
    }
}

TradingChart.propTypes = {
    sgn: MPropTypes.observableObject
}
