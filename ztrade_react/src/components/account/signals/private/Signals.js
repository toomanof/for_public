import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { getWindowWidth } from 'services/helpers'

import { Paginator } from 'components'

import SignalRow from './SignalRow'

@inject('sgn', 'paginator')
@observer
export default class Signals extends React.Component {
    renderSignals() {
        const { paginator, sgn } = this.props
        const width = getWindowWidth()

        // On small resolution via paginator
        if (width && width <= 600) {
            return (
                <React.Fragment>
                    {paginator.paginatedItems.map((obj, index) => {
                        if(index<50){
                            return <SignalRow key={obj.id} signal={obj}/>
                        }

                    })}
                </React.Fragment>
            )
        }

        return (
            <React.Fragment>
                {sgn.signals.map((obj, index) => {
                    if(index<50) {
                        return <SignalRow key={obj.id} signal={obj}/>
                    }
                })}
            </React.Fragment>
        )
    }

    render() {
        const width = getWindowWidth()
        const styleHide = {display: 'None'}
        return (
            <React.Fragment>
                <div className="signalpagerow_relative">
                    <div className="module table100perc">
                        <div className="signal_scroll">
                            <div className="signaltb">
                                <div className="signalthead">
                                    <div className="signalth">{'Инструмент'}</div>
                                    <div className="signalth" style={styleHide}>{'ID'}</div>
                                    <div className="signalth">{'Buy / Sell'}</div>
                                    <div className="signalth">{'Ордер'}</div>
                                    <div className="signalth">{'Цена'}</div>
                                    <div className="signalth">{'T / P'}</div>
                                    <div className="signalth">{'S / L'}</div>
                                    <div className="signalth">{'Результат'}</div>
                                </div>

                                {this.renderSignals()}
                            </div>
                        </div>
                    </div>
                </div>

                <div className="pagmob">
                    {width && width <= 600 && <Paginator />}
                </div>
            </React.Fragment>
        )
    }
}

Signals.propTypes = {
    paginator: MPropTypes.observableObject,
    sgn: MPropTypes.observableObject
}
