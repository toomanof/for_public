import React from 'react'
import PropTypes from 'prop-types'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import classNames from 'classnames'

/**
 * If row in countDown mode, rerender component
 * until exit from countdown
 */
@inject('sgn')
@observer
export default class SignalRow extends React.Component {
    static getDerivedStateFromProps(nextProps, prevState) {
        if (prevState.publishAt !== nextProps.signal.publishAt) {
            return {
                ...prevState,
                colResult: nextProps.signal.colResult,
                publishAt: nextProps.signal.publishAt
            }
        }
        return null
    }

    state = {
        isShow: false, // for mobiles
        publishAt: this.props.signal.publishAt,
        colResult: this.props.signal.colResult
    }

    componentDidMount() {
        this.intv = setInterval(this.checkCountdown, 1000)
    }

    componentWillUnmount() {
        this.intv && clearInterval(this.intv)
    }

    checkCountdown = () => {
        const { signal } = this.props
        if (!signal.isCountdown) {
            this.intv && clearInterval(this.intv)
            this.intv = false
            this.setState({
                colResult: '',
                publishAt: null
            })
            return
        }
        this.setState({ colResult: signal.colResult })
    }

    onClickDelete = () => {
        const { sgn, signal } = this.props
        sgn.deleteSignal(signal)
    }

    /**
     * For mobiles
     */
    onClickShow = e => {
        e.preventDefault()
        this.setState({ isShow: !this.state.isShow })
    }

    onClickActiveSymbol = (symbolCode) => {
        const { sgn } = this.props
        sgn.activateTradingChartCode(symbolCode)
    }

    /**
     * Render text in "result" column
     */
    renderResult() {
        const { signal } = this.props

        if (signal.isCountdown) {
            return <span className="signal_time">{this.state.colResult}</span>
        }

        if (signal.isActive) {
            return (
                <React.Fragment>
                    <span className="signal_active">{'Active'}</span>
                    <span className="act" />
                </React.Fragment>
            )
        }

        return (
            <div className="signalcl_box">
                <span>{signal.colResult}</span>{' '}
                <span
                    onClick={this.onClickDelete}
                    style={{ cursor: 'pointer' }}
                    className="del"
                />
            </div>
        )
    }

    render() {
        const { isShow } = this.state
        const { signal, sgn } = this.props

        const cnSignal = classNames('signalcl', {
            'mobile-hidden-600': !isShow
        })
        const cnColResult = classNames(cnSignal, signal.colResultClassName)
        const cnArrow = classNames('opBtn', { active: isShow })

        const signalRowCn = classNames('signalrow signalrow-first', {
            'current':
                sgn.highlightedSignals.get(sgn.activeMarketCode) === signal.id
        })

        const styleHide = {display: 'None'}

        return (
            <div className={signalRowCn}>
                <div className="signalcl">
                    <div className="signalcl_hide">{'Инструмент'}</div>
                    <div className="signalcl_box">
                        <span
                            className = "sybol_name"
                            onClick={this.onClickActiveSymbol.bind(this, signal.symbolCode)}
                        >
                            {signal.colSymbol}
                        </span>
                        {signal.isCountdown && (
                            <span className="signal_warn">
                                {'!'}
                                <span className="signal_warn_popup">
                                    {
                                        'Формируется новый сигнал, нажмите на иконку '
                                    }
                                    <img src="/images/opbtn2.svg" width="15" />
                                    {' чтобы посмотреть параметры для входа'}
                                </span>
                            </span>
                        )}
                    </div>
                </div>

                <div className={cnSignal} style={styleHide}>
                    <div className="signalcl_hide">{'ID'}</div>
                    <div className="signalcl_box">{signal.colID}</div>
                </div>

                <div className={cnSignal}>
                    <div className="signalcl_hide">{'Buy / Sell'}</div>
                    <div className="signalcl_box">{signal.colTradeType}</div>
                </div>

                <div className={cnSignal}>
                    <div className="signalcl_hide">{'Ордер'}</div>
                    <div className="signalcl_box">{signal.colOrderType}</div>
                </div>

                <div className={cnSignal}>
                    <div className="signalcl_hide">{'Цена'}</div>
                    <div className="signalcl_box">{signal.colPrice}</div>
                </div>

                <div className={cnSignal}>
                    <div className="signalcl_hide">{'T / P'}</div>
                    <div className="signalcl_box">{signal.colTp}</div>
                </div>

                <div className={cnSignal}>
                    <div className="signalcl_hide">{'S / L'}</div>
                    <div className="signalcl_box">{signal.colSl}</div>
                </div>

                <div className={cnColResult}>
                    <div className="signalcl_hide">{'Результат'}</div>
                    <div className="signalcl_box">{this.renderResult()}</div>
                </div>

                <a href="#" onClick={this.onClickShow} className={cnArrow} />
            </div>
        )
    }
}

SignalRow.propTypes = {
    sgn: MPropTypes.observableObject,
    signal: PropTypes.object
}
