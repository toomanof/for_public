import React from 'react'
import PropTypes from 'prop-types'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'
import { getWindowWidth } from 'services/helpers'

// Show on mobile only (width < 600)
@inject('chart')
@observer
export default class Chart03 extends React.Component {
    state = {
        width: 0
    }

    componentDidMount() {
        this.setState({ width: getWindowWidth() })
    }

    render() {
        const { chart } = this.props
        if (
            !chart.chart03Data ||
            chart.chart03Data.length === 0 ||
            this.state.width >= 600
        ) {
            return null
        }

        const options = {
            chart: {
                zoomType: 'x'
            },

            title: {
                text: 'Линейный график доходности'
            },

            subtitle: {
                text:
                    document.ontouchstart === undefined
                        ? 'Click and drag in the plot area to zoom in'
                        : 'Pinch the chart to zoom in'
            },

            xAxis: {
                type: 'datetime'
            },

            yAxis: {
                title: {
                    text: 'line chart'
                }
            },

            legend: {
                enabled: false
            },

            plotOptions: {
                area: {
                    fillColor: {
                        linearGradient: {
                            x1: 0,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                            [0, Highcharts.getOptions().colors[0]],
                            [
                                1,
                                Highcharts.Color(
                                    Highcharts.getOptions().colors[0]
                                )
                                    .setOpacity(0)
                                    .get('rgba')
                            ]
                        ]
                    },
                    marker: {
                        radius: 2
                    },
                    lineWidth: 1,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    },
                    threshold: null
                }
            },

            series: [
                {
                    type: 'area',
                    name: 'Pips',
                    data: chart.chart03Data
                }
            ]
        }

        return <HighchartsReact highcharts={Highcharts} options={options} />
    }
}

Chart03.propTypes = {
    chart: MPropTypes.observableObject
}
