import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { rHTML } from 'services/helpers'


@inject('sgn')
@observer
export default class SgnSamples extends React.Component{
    render(){
        const { sgn } = this.props

        return (
            <React.Fragment>
                {rHTML(sgn.activeMarket.pageSamples)}
            </React.Fragment>
        )
    }
}


SgnSamples.propTypes = {
    sgn: MPropTypes.observableObject
}