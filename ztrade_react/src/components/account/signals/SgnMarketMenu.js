import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Modal } from 'components'
import {Link} from 'react-router-dom';

@inject('account', 'sgn', 'user')
@observer
export default class SgnMarketMenu extends React.Component {
    constructor(props) {
        super(props)
        this.dateFormat = new Intl.DateTimeFormat('ru');
        this.state = {
            countRequestFreePeriod: 0,
            isModalOpened: false,
            isFreePeriodModalOpened: this.getIsFreePeriodModalOpened(),
            isUsedFreePeriodModalOpened: this.getIsUsedFreePeriodModalOpened()
        }
    }

    getIsFreePeriodModalOpened() {
        const {sgn, account} = this.props
        let isFreePeriodModalOpened = false
        if(!sgn.activeMarket.isPaid)
            isFreePeriodModalOpened = account.freePeriod && account.freePeriod.isActive
        return isFreePeriodModalOpened
    }

    getIsUsedFreePeriodModalOpened() {
        const {sgn, account} = this.props
        let isFreePeriodModalOpened = false
        if(!sgn.activeMarket.isPaid)
            isFreePeriodModalOpened = account.freePeriod && !account.freePeriod.isActive
        return isFreePeriodModalOpened
    }

    componentDidMount() {
        if(this.state.countRequestFreePeriod < 3){
            this.intervalID = setInterval(this.timer, 1000);
        }
    }
    componentWillUnmount(){
        clearInterval(this.intervalID)
    }

    timer =() => {
        if(this.state.countRequestFreePeriod < 3) {
            const {account, sgn} = this.props
            if (!account.freePeriod) {
                account.loadFreePeriod()
            } else {
                sgn.loadMarkets()
                this.setState({
                    isFreePeriodModalOpened: this.getIsFreePeriodModalOpened(),
                    isUsedFreePeriodModalOpened: this.getIsUsedFreePeriodModalOpened()
                })
                clearInterval(this.intervalID)
            }
            this.setState({countRequestFreePeriod: this.state.countRequestFreePeriod + 1})
        } else{
            clearInterval(this.intervalID)
        }
    }

    onClickItem = (market , e) => {
        e.preventDefault()
        console.log('onClickItem')
        if(market.active) {
            this.props.sgn.setActiveMarketCode(market.code)
        } else {
            this.setState({ isModalOpened: true })
        }
    }

    onChangeItem = (e, options) => {
        const active_option = options.find(item => item.value === e.target.value)
        const code = e.target.value
        console.log('onChangeItem')
        if(active_option.active) {
            this.props.sgn.setActiveMarketCode(code)
        } else {
            this.setState({ isModalOpened: true })
        }
    }

    onClickCloseModal = () => {
        this.setState({ isModalOpened: false })
    }

    onClickCloseFreePeriodModal = () => {
        this.setState({ isFreePeriodModalOpened: false })
    }

    onClickCloseUsedFreePeriodModal = () => {
        this.setState({ isUsedFreePeriodModalOpened: false })
    }

    render() {
        const { account, sgn, user } = this.props
        const first_name = (!user.profile.firstName) ?'': user.profile.firstName.charAt(0).toUpperCase() + user.profile.firstName.slice(1)
        let options = []
        Array.from(sgn.marketMap.values()).map(obj => {
            options.push({value:obj.code, active:obj.active})
        })
        return (
            <React.Fragment>
                <ul className="bigtabs hidden-sm">
                    {Array.from(sgn.marketMap.values()).map(obj => {
                        const cn =
                            obj.code === sgn.activeMarketCode ? 'active' : ''

                        return (
                            <li key={obj.code} className={cn}>
                                <a
                                    onClick={this.onClickItem.bind(
                                        null,
                                        obj
                                    )}
                                    href="">
                                    {obj.name}
                                </a>
                            </li>
                        )
                    })}
                </ul>

                <div className="seltabs seltabs_bigtabs visible-sm">
                    <select onChange={e => this.onChangeItem(e, options)} className="selectbox">
                        {Array.from(sgn.marketMap.values()).map(obj => {
                            return (
                                <option key={obj.code} value={obj.code} active={String(obj.active)}>
                                    {obj.name}
                                </option>
                            )
                        })}
                    </select>
                </div>

                {this.state.isModalOpened && (
                    <Modal
                        className="modal simple_modal"
                        onClickClose={this.onClickCloseModal}
                        isClickOutsideCloseModal={false}
                    >
                        <React.Fragment>
                            <div className="modal_header">Уважаемый (ая), {first_name}</div>
                            <div className="modal_content">
                                <p><b>Данный рынок не доступен!</b> Мы работаем действительно над чем-то полезным,
                                    и уже скоро можно будет воспользоваться сервисом.</p>
                            </div>
                            <a href="#" className="modal_close" data-fancybox-close=""></a>
                        </React.Fragment>
                    </Modal>
                )}
                {this.state.isFreePeriodModalOpened && (
                    <Modal
                        className="modal simple_modal"
                        onClickClose={this.onClickCloseFreePeriodModal}
                        isClickOutsideCloseModal={false}
                    >
                        <React.Fragment>
                            <div className="modal_header">Уважаемый (ая), {first_name}</div>
                            <div className="modal_content">
                                <p>Вам предоставлен бесплатный доступ на торговые сигналы c
                                    <b> {this.dateFormat.format(account.freePeriod.createdAt)}</b> по
                                    <b> {this.dateFormat.format(account.freePeriod.dateTo)}</b>
                                </p>
                            </div>
                            <a href="#" className="modal_close" data-fancybox-close=""></a>
                        </React.Fragment>
                    </Modal>
                )}

                {this.state.isUsedFreePeriodModalOpened && (
                    <Modal
                        className="modal simple_modal"
                        onClickClose={this.onClickCloseUsedFreePeriodModal}
                        isClickOutsideCloseModal={false}
                    >
                        <React.Fragment>
                            <div className="modal_header">Уважаемый (ая), {first_name}</div>
                            <div className="modal_content">
                                <p>
                                    Ваш пробный период завешен! Для того чтобы начать<br/>
                                    получать торговые сигналы и рекомендации онлайн,<br/>пожалуйста
                                    <Link to={sgn.activeMarket.tariffsUrl}> оформите подписку</Link>
                                </p>
                            </div>
                            <a href="#" className="modal_close" data-fancybox-close=""></a>
                        </React.Fragment>
                    </Modal>
                )}
            </React.Fragment>
        )
    }
}

SgnMarketMenu.propTypes = {
    account: MPropTypes.observableObject,
    sgn: MPropTypes.observableObject,
    user: MPropTypes.observableObject
}
