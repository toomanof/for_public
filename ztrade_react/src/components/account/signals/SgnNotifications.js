import React from 'react'
import PropTypes from 'prop-types'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Loading } from 'components'
import classNames from 'classnames'

let makeAsReadNotificationsInterval = null

@inject('account', 'alert', 'general', 'sgn', 'tabs', 'menu')
@observer
export default class SgnNotifications extends React.Component {
    state = { isTimezonesOpened: false }

    componentDidMount() {
        const { account, general, menu } = this.props
        account.loadMarketNotifications()
        general.loadTimezones()
        makeAsReadNotificationsInterval = setInterval(
            this.props.account.markAllNotificationsAsRead,
            10000
        )
        menu.loadRecommendSignals()
    }

    componentWillUnmount() {
        clearInterval(makeAsReadNotificationsInterval)
        makeAsReadNotificationsInterval = null
    }

    setNewTimezone = tzId => {
        const { sgn } = this.props
        this.changeIsTimezonesOpened()
        sgn.updateSignalFilter(
            sgn.activeMarketCode,
            {
                timezone_id: tzId
            },
            this.setNewTimezoneOnSuccess
        )
    }

    setNewTimezoneOnSuccess = () => {
        const { alert } = this.props
        alert.createAlert('Часовой пояс успешно изменен')
        this.loadSignals()
    }

    changeIsTimezonesOpened = e => {
        e && e.preventDefault()
        this.setState({ isTimezonesOpened: !this.state.isTimezonesOpened })
    }

    onClickSignalId = (e, obj) => {
        const { sgn, tabs } = this.props
        e.preventDefault()
        sgn.setHighlightedSignal(obj.contract.signalId)
        tabs.setActiveIndex(0)
    }

    renderSignalsNotifications = () => {
        const { account, sgn } = this.props

        if (!account.signalsNotifications) return <Loading />

        const isPaid = sgn.activeMarket.isPaid || (account.freePeriod && account.freePeriod.isActive)

        return (
            <React.Fragment>
                {Array.from(account.signalsNotifications.values())
                    .slice(0, 10)
                    .map(obj => {
                        const ringCn = classNames('ringbtn', {
                            active: !obj.isRead
                        })
                        return (
                            <div className="notify_line" key={obj.id}>
                                <div className="notify_line_icon">
                                    <a href="#" className={ringCn} />
                                </div>
                                <div className="notify_line_date">
                                    {obj.createdAt.format('D MMMM, HH:mm')}
                                </div>
                                <div className="notify_line_name">
                                    {obj.contract &&
                                        obj.contract.signalId &&
                                        isPaid && (
                                            <a
                                                href="#"
                                                onClick={e =>
                                                    this.onClickSignalId(e, obj)
                                                }>{`ID ${obj.contract.signalId} `}</a>
                                        )}
                                    {obj.text}
                                </div>
                            </div>
                        )
                    })}
            </React.Fragment>
        )
    }

    renderNewsNotifications = () => {
        const { account } = this.props

        if (!account.newsNotifications) return <Loading />

        return (
            <React.Fragment>
                {Array.from(account.newsNotifications.values())
                    .slice(0, 10)
                    .map(obj => {
                        const dateCn = classNames('datebtn', {
                            active: !obj.isRead
                        })
                        return (
                            <div className="notify_line" key={obj.id}>
                                <div className="notify_line_icon">
                                    <a href="#" className={dateCn} />
                                </div>
                                <div className="notify_line_date">
                                    {obj.createdAt.format('D MMMM, HH:mm')}
                                </div>
                                <div className="notify_line_name">
                                    {obj.text}
                                </div>
                            </div>
                        )
                    })}
            </React.Fragment>
        )
    }

    renderSignalsNotificationNotReadCount() {
        const { account } = this.props
        if (
            account.signalsNotificationsNotReadCount &&
            account.signalsNotificationsNotReadCount !== 0
        )
            return <b>{account.signalsNotificationsNotReadCount}</b>
        return null
    }

    renderNewsNotificationNotReadCount() {
        const { account } = this.props
        if (
            account.newsNotificationsNotReadCount &&
            account.newsNotificationsNotReadCount !== 0
        )
            return <b>{account.newsNotificationsNotReadCount}</b>
        return null
    }

    renderNotificationsTimezone() {
        const { general, account, sgn } = this.props
        if (!sgn.signalFilter || !general.timezones) {
            return 'Выберите часовый пояс'
        }
        const arr = general.timezones.filter(
            obj => obj.id === sgn.signalFilter.timezoneId
        )
        return arr.length === 0 ? 'Выберите часовой пояс' : arr[0].name
    }

    renderTimezones() {
        const { general, account, sgn } = this.props
        if (!general.timezones || !sgn.signalFilter) return <Loading />
        return (
            <ul className="aside_notify_list">
                {Array.from(general.timezones.values()).map(obj => {
                    const itemCn = classNames({
                        active: obj.id === sgn.signalFilter.timezoneId
                    })
                    return (
                        <li
                            onClick={() => this.setNewTimezone(obj.id)}
                            key={obj.id}
                            className={itemCn}>
                            <span>
                                {'GMT'} {obj.offset}
                            </span>
                            {obj.name}
                        </li>
                    )
                })}
            </ul>
        )
    }

    render() {
        const { isTimezonesOpened } = this.state
        const { account, menu } = this.props
        const signalsTabCn = classNames('aside_notify_box ring', {
            active: account.notificationsActiveTab === 'signals'
        })
        const newsTabCn = classNames('aside_notify_box paper', {
            active: account.notificationsActiveTab === 'news'
        })

        let first_recomend = null
        let populars = null

        if(menu.recommend_signal_page){
            first_recomend = menu.recommend_signal_page[0]
            populars = menu.recommend_signal_page.slice(1,)
        }
        return (
            <div className="clearfix">
                <div className="col_left">
                    <div className="module aside_notify">
                        <a
                            className={signalsTabCn}
                            onClick={() =>
                                account.setNotificationsActiveTab('signals')
                            }>
                            <span>
                                {this.renderSignalsNotificationNotReadCount()}{' '}
                                {'Торговые сигналы'}
                            </span>
                            {/* <span className="aside_notify_show" /> */}
                        </a>
                        <div className="aside_notify_title">{'Update'}</div>

                        <a
                            className={newsTabCn}
                            onClick={() =>
                                account.setNotificationsActiveTab('news')
                            }>
                            <span>
                                {this.renderNewsNotificationNotReadCount()}{' '}
                                {'Новостная лента'}
                            </span>
                        </a>
                        <div className="aside_notify_title">{'News'}</div>

                        <div className="aside_notify_box time">
                            <span>{'Текущее время'}</span>
                        </div>
                        <div className="aside_notify_title">
                            {this.renderNotificationsTimezone()}
                            <a
                                href="#"
                                className="dots"
                                onClick={this.changeIsTimezonesOpened}
                            />
                        </div>
                        {isTimezonesOpened && this.renderTimezones()}
                    </div>
                </div>

                <div className="col_mid">
                    <div className="row">
                        <div className="grid8 md12">
                            <div className="notify_wrapper">
                                {account.notificationsActiveTab === 'signals' &&
                                    this.renderSignalsNotifications()}
                                {account.notificationsActiveTab === 'news' &&
                                    this.renderNewsNotifications()}
                            </div>
                        </div>

                        <div className="grid4 md12">
                            <div className="module text-center">
                                {first_recomend && (
                                    <div className="module_content">
                                        <a
                                            rel="noopener"
                                            target="_blank"
                                            href={first_recomend.url}>
                                            <img
                                                src={first_recomend.picUrl}
                                                alt=""
                                                srcSet={`${first_recomend.picSrcsetUrl} 2x`} />
                                        </a>
                                    </div>
                                )}
                            </div>

                            <div className="module popular">
                                <div className="module_title">
                                    {'Рекомендуемый брокер'}
                                </div>
                                {populars && populars.map((obj, index) => {
                                    return(
                                        <div key={index} className="module_content text-center">
                                            <a
                                            rel="noopener"
                                            target="_blank"
                                            key={obj.id}
                                            href={obj.url}>
                                                <img
                                                    src={obj.picUrl}
                                                    alt=""
                                                    srcSet={`${obj.picSrcsetUrl} 2x`}
                                                />
                                            </a>
                                        </div>
                                    )
                                })}

                            </div>

                            <div className="module popular">
                                <div className="module_title">
                                    {'Партнерская программа'}
                                </div>
                                <div className="module_content">
                                    <div className="publ">
                                        <p>
                                            {
                                                'Вы заинтересованы в дополнительном заработке? Переходите в раздел для партнеров в личном кабинете'
                                            }
                                        </p>
                                        <a href="#" className="publ_more">
                                            {'Узнать подробнее'}
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

SgnNotifications.propTypes = {
    account: MPropTypes.observableObject,
    alert: MPropTypes.observableObject,
    general: MPropTypes.observableObject,
    sgn: MPropTypes.observableObject,
    tabs: MPropTypes.observableObject,
    menu: MPropTypes.observableObject
}
