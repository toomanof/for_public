import React from 'react'
import PropTypes from 'prop-types'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'
import { Loading } from 'components'

import { isMobile } from 'services/helpers'
import Signals from './private/Signals'
import SignalFilter from './private/SignalFilter'
import TradingChart from './private/TradingChart'

/**
 * Every minute load signals from server for active market.
 * There is no real time checking for not paid users,
 * Not paid users see signals after some delay (2 or more hours).
 * Delay is set on backend.
 * Request every minute updates signals in the state.
 *
 * Signals for paid users are updated real-time via websockets.
 * Component WSUpdater.
 */
@inject('sgn', 'user', 'account')
@observer
export default class SgnSignals extends React.Component {
    componentDidMount() {
        this.intv = setInterval(this.loadSignals, 60 * 1000)
    }

    componentWillUnmount() {
        this.intv && clearInterval(this.intv)
    }

    loadSignals = () => {
        const { sgn } = this.props
        if (sgn.activeMarketCode) {
            sgn.loadSignals(sgn.activeMarketCode)
        }
    }
    IsAlertSubscriber() {
        const {sgn, account} = this.props
        let result = true
        if(sgn.activeMarket.isPaid)
            result = false
        if(account.freePeriod && account.freePeriod.isActive)
            result = false
        return result
    }
    render() {
        const { sgn, user, account } = this.props
        const profile = user.profile

        if (!sgn.signals) {
            return <Loading />
        }
        return (
            <React.Fragment>
                {isMobile() && this.IsAlertSubscriber() &&
                    <React.Fragment>
                        <div className="filtersm_name" style={{marginBottom: "20px"}}>
                            {'Сигналы поступают с задержкой '}
                            <b>{'2 часа'}</b>
                            <br />
                            <Link to={sgn.activeMarket.tariffsUrl}>
                                {'Оформите подписку'}
                            </Link>
                            {' и получайте сигналы в онлайн режиме'}
                        </div>
                    </React.Fragment>
                }
                <SignalFilter />
                {isMobile() && this.IsAlertSubscriber() &&
                <React.Fragment>
                    <div className="module alertbox">
                        <span>{`Уважаемый(ая), ${profile.firstName}`}</span>
                        {'Сигналы поступают с задержкой '}
                        <b>{'2 часа'}</b>
                        <br />
                        <Link to={sgn.activeMarket.tariffsUrl}>
                            {'Оформите подписку'}
                        </Link>
                        {' и получайте сигналы в онлайн режиме'}
                    </div>
                    <br/>
                </React.Fragment>
                }
                <div className="row signalpagerow">
                    <div className="grid6 md12">
                        <Signals />
                    </div>

                    <div className="grid6">
                        <div className="module">
                            <div className="signal_chart">
                                <TradingChart />
                            </div>
                        </div>

                        {/* <div className="module">
                            <div className="module_content">
                                {'Контентный блок'}
                            </div>
                        </div> */}
                    </div>
                </div>

                <div className="signal_social">
                    <a href="https://t.me/ztrade" className="ss_item2" />
                    <a href="mailto:support@ztrade.ru" className="ss_item3" />
                </div>

                {!isMobile() && this.IsAlertSubscriber() && (
                    <React.Fragment>
                        <div className="module alertbox">
                            <span>{`Уважаемый(ая), ${profile.firstName}`}</span>
                            {'Сигналы поступают с задержкой '}
                            <b>{'2 часа'}</b>
                            <br />
                            <Link to={sgn.activeMarket.tariffsUrl}>
                                {'Оформите подписку'}
                            </Link>
                            {' и получайте сигналы в онлайн режиме'}
                        </div>
                    </React.Fragment>
                )}
            </React.Fragment>
        )
    }
}

SgnSignals.propTypes = {
    sgn: MPropTypes.observableObject,
    user: MPropTypes.observableObject
}
