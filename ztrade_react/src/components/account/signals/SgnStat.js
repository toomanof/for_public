import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { toJS } from 'mobx'
import { Link } from 'react-router-dom'
import { MultiSelect, Loading } from 'components'
import classNames from 'classnames'

import Chart01 from './private/Chart01'
import Chart02 from './private/Chart02'
import Chart03 from './private/Chart03'

@inject('chart', 'sgn')
@observer
export default class SgnStat extends React.Component {

    componentDidMount() {
        this.props.sgn.loadSymbols()
    }

    onChangeSymbols = values => {
        this.props.chart.setActiveSymbols(values)
        this.props.chart.loadChart01()
        this.props.chart.loadChart02()
        this.props.chart.loadChart03()
    }

    onClickWeek = e => {
        e.preventDefault()
        this.props.chart.setActivePeriod('week')
        this.props.chart.loadChart01()
    }

    onClickMonth = e => {
        e.preventDefault()
        this.props.chart.setActivePeriod('month')
        this.props.chart.loadChart01()
    }

    render() {
        const { chart, sgn } = this.props

        if (!sgn.symbols) {
            return <Loading />
        }

        const mselectOptions = sgn.symbols.map(symb => {
            return { value: symb.code, text: symb.name }
        })

        const monthTabCn = classNames({'statistics_icons--active': chart.activePeriod === 'month'})
        const weekTabCn = classNames({'statistics_icons--active': chart.activePeriod === 'week'})

        return (
            <div className="module stats_charts">
                <div className="module_title2">
                    <b>{'Сигналы: '}</b>
                    {'Статистика доходности'}
                </div>
                <div className="module_content module_content_stat">
                    <div className="statistics">
                        <div className="statistics_top">
                            <div className="statistics_left">
                                <ul className="statistics_icons">
                                    <li className={weekTabCn}>
                                        <a onClick={this.onClickWeek} href="#">
                                            {'Неделя'}
                                        </a>
                                    </li>
                                    <li className={monthTabCn}>
                                        <a onClick={this.onClickMonth} href="#">
                                            {'Месяц'}
                                        </a>
                                    </li>
                                </ul>
                                <span>выбрать период</span>
                                {/*
                                <MultiSelect
                                    options={mselectOptions}
                                    values={toJS(chart.activeSymbols)}
                                    onChange={this.onChangeSymbols}
                                />
                                */}
                            </div>

                            {!sgn.activeMarket.isPaid && (
                                <Link
                                    to={sgn.activeMarket.tariffsUrl}
                                    className="btn btn-blue">
                                    {'Получить сигналы'}
                                </Link>
                            )}
                        </div>

                        <div className="statistics_chart">
                            <Chart01 />
                            <Chart02 />
                            <Chart03 />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

SgnStat.propTypes = {
    chart: MPropTypes.observableObject,
    sgn: MPropTypes.observableObject
}
