import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { default as MobileDetect } from 'mobile-detect'
import { rHTML } from 'services/helpers'
import { Loading, Modal } from 'components'

import StrategyFilter from './private/StrategyFilter'

@inject('account')
@observer
export default class StrategiesTab extends React.Component {
    constructor(props) {
        super(props)
        this.containerRef = React.createRef()
    }

    state = {
        isModalOpened: false
    }

    componentDidMount() {
        this.props.account.loadStrategies()
    }

    onClickOpenModal = (obj, e) => {
        e.preventDefault()
        this.props.account.loadStrategy(obj.id)
        this.setState({ isModalOpened: true })
    }

    onClickCloseModal = () => {
        this.setState({ isModalOpened: false })
    }

    onClickStrategy = (obj, e) => {
        e.preventDefault()
        this.props.account.setActiveStrategy(obj)

        const md = new MobileDetect(window.navigator.userAgent)
        if (md.mobile()) {
            const node = this.containerRef.current
            if (node) {
                const x = node.offsetLeft
                const y = node.offsetTop
                window.scrollTo(x, y)
            }
        }
    }

    render() {
        const { account } = this.props

        if (!account.strategies) {
            return <Loading />
        }

        const activeSt = account.activeStrategy

        return (
            <React.Fragment>
                <div className="row">
                    <div className="grid7 md12">
                        <StrategyFilter />

                        <div className="strat_table">
                            {account.filteredStrategies.map(obj => {
                                return (
                                    <div key={obj.id} className="strat_line">
                                        <a
                                            href=""
                                            onClick={this.onClickStrategy.bind(
                                                null,
                                                obj
                                            )}>
                                            <b>{obj.timeFramesName}</b>{' '}
                                            {obj.name}
                                        </a>
                                    </div>
                                )
                            })}
                        </div>
                    </div>

                    {activeSt && (
                        <div ref={this.containerRef} className="grid5 md12">
                            <div className="module ureview">
                                <div className="ureview_meta">
                                    {'Рекомендуемый таймфрейм '}
                                    {activeSt.timeFramesName}
                                </div>

                                <div className="ureview_name">
                                    <a
                                        href=""
                                        onClick={this.onClickOpenModal.bind(
                                            null,
                                            activeSt
                                        )}>
                                        {activeSt.name}
                                    </a>
                                </div>

                                {rHTML(activeSt.shortDescription, '')}

                                <p>
                                    <img src={activeSt.picUrl} alt="" />
                                </p>

                                <a
                                    onClick={this.onClickOpenModal.bind(
                                        null,
                                        activeSt
                                    )}
                                    href=""
                                    className="btn btn-blue">
                                    {'Прочитать обзор'}
                                </a>
                            </div>
                        </div>
                    )}
                </div>

                {this.state.isModalOpened && account.strategyItem && (
                    <Modal onClickClose={this.onClickCloseModal}>
                        <div>
                            <h3>{account.strategyItem.name}</h3>
                            <p>{'Здесь будет контент стратегии'}</p>
                        </div>
                    </Modal>
                )}
            </React.Fragment>
        )
    }
}

StrategiesTab.propTypes = {
    account: MPropTypes.observableObject
}
