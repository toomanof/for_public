import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Loading } from 'components'

const styleCont = {
    position: 'relative',
    overflow: 'hidden',
    paddingTop: '56.25%'
}

const styleIframe = {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    border: 0
}

@inject('account')
@observer
export default class TutorialBookTab extends React.Component {
    componentDidMount() {
        this.props.account.loadTutorialBook()
    }

    render() {
        const { account } = this.props

        if (!account.tutorialBook) {
            return <Loading />
        }

        return (
            <div className="module">
                <div className="module_content lkbookTop">
                    <a
                        rel="noopener"
                        target="_blank"
                        href={account.tutorialBook.pdfFileUrl}
                        className="btn btn-big btn-learn btn-blank">
                        <span>{'Открыть учебник в новой вкладке'}</span>
                    </a>

                    <a
                        href={account.tutorialBook.zipFileUrl}
                        className="btn btn-big btn-learn btn-download">
                        <span>{'Скачать учебник'}</span>
                    </a>
                </div>
                <div className="lkbookTop_pdf">
                    <div style={styleCont}>
                        <iframe
                            title="Книга"
                            style={styleIframe}
                            frameBorder="0"
                            src={`https://docs.google.com/viewer?url=${account.tutorialBook.pdfFileUrl}&embedded=true`}
                        />
                    </div>
                </div>
            </div>
        )
    }
}

TutorialBookTab.propTypes = {
    account: MPropTypes.observableObject
}
