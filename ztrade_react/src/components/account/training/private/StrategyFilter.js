import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import classNames from 'classnames'


@inject('account')
@observer
export default class StrategyFilter extends React.Component{
    onClickIndicator = (id, e) => {
        e.preventDefault()
        this.props.account.addIndicatorFilter(id)
    }

    onClickTimeFrame = (id, e) => {
        e.preventDefault()
        this.props.account.addTimeFrameFilter(id)
    }

    render(){
        const { account } = this.props

        if (!account.timeFrames || !account.indicators){
            return null
        }

        const cn = classNames('filtertrain', {'mobile-hidden-740': !account.strategyFilterIsShowOnMobile})

        return (
            <div className={cn}>
                <div className="filtertrain_title">{'Таймфрейм'}</div>
                
                <div className="filtertrain_links">
                    {this.props.account.timeFrames.map(obj => {
                        const cn = classNames(
                            'filtertrain_btn',
                            {active: account.timeFrameFilter.includes(obj.id)}
                        )

                        return (
                            <a
                                onClick={this.onClickTimeFrame.bind(null, obj.id)}
                                key={obj.id}
                                href="/"
                                className={cn}>

                                {obj.name}
                            </a>
                        )
                    })}
                </div>
                
                <div className="filtertrain_title">{'Indicators'}</div>
                
                <div className="filtertrain_links">
                    {this.props.account.indicators.map(obj => {
                        const cn = classNames(
                            'filtertrain_btn',
                            {active: account.indicatorFilter.includes(obj.id)}
                        )

                        return (
                            <a
                                onClick={this.onClickIndicator.bind(null, obj.id)}
                                key={obj.id}
                                href="/"
                                className={cn}>

                                {obj.name}
                            </a>
                        )
                    })}
                </div>
            </div>
        )
    }
}


StrategyFilter.propTypes = {
    account: MPropTypes.observableObject
}