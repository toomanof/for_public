import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Loading, Player } from 'components'


@inject('account')
@observer
export default class VideosTab extends React.Component{
    componentDidMount(){
        this.props.account.loadVideos()   
        this.props.account.loadUserSettings()   
    }

    onClickTag = (id, e) => {
        e.preventDefault()
        this.props.account.setVideoTagFilterId(id)
    }

    onClickVideo = (id, e) => {
        e.preventDefault()
        const form = {featured_video_id: id}
        this.props.account.updateUserSettings(form)
        window.scrollTo(0, 0)
    }

    /**
     * Returns first or featured (from settings) video.
     */
    get featuredVideo(){
        const { account } = this.props

        if (account.videos.length === 0){
            return null
        }

        for (let obj of account.videos){
            if (obj.id === account.userSettings.featuredVideoId){
                return obj
            }
        }
        return account.videos[0]
    }

    render(){
        const { account } = this.props

        if (!account.videos || !account.userSettings){
            return <Loading />
        }

        if (!this.featuredVideo){
            return null
        }

        return (
            <React.Fragment>
                <div className="module learnvid">
                    <div className="module_content">
                        <div className="learnvid_title">
                            {this.featuredVideo.name}
                        </div>
                        
                        <div className="ytbox_video">
                            <Player
                                height={270}
                                fileUrl={this.featuredVideo.fileUrl}
                                link={this.featuredVideo.link} />
                        </div>
                        
                        <div className="learnvid_tags">
                            <h4>{'Поиск по тегам'}</h4>
                            <ul>
                                {account.videoTags.map(obj => {
                                    return (
                                        <li key={obj.id}>
                                            <a
                                                href="#"
                                                onClick={this.onClickTag.bind(null, obj.id)}>

                                                {obj.name}
                                            </a>
                                        </li>        
                                    )
                                })}

                                <li>
                                    <a
                                        href="#"
                                        onClick={this.onClickTag.bind(null, null)}>
                                        {'Выбрать все'}
                                    </a>
                                </li>        
                                
                            </ul>
                        </div>
                    </div>          
                </div>

                <div className="row">
                    {account.filteredVideos.map(obj => {
                        return (
                            <div key={obj.id} className="grid20 tb6 xs12">
                                <div className="ytbox ytbox-small">
                                    <div className="ytbox_video">
                                        <Player
                                            fileUrl={obj.fileUrl}
                                            link={obj.link} />
                                    </div>
                                    <div className="ytbox_name">
                                        <a
                                            onClick={this.onClickVideo.bind(null, obj.id)}
                                            href="#">
                                            
                                            {obj.name}
                                        </a>
                                    </div>
                                    <div className="ytbox_tags">
                                        {obj.tagsName}
                                    </div>
                                </div>
                            </div>        
                        )
                    })}

                    
                </div>
            </React.Fragment>
        )
    }
}


VideosTab.propTypes = {
    account: MPropTypes.observableObject
}