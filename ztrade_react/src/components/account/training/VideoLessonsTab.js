import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Loading, Player } from 'components'


@inject('account')
@observer
export default class VideoLessonsTab extends React.Component{
    componentDidMount(){
        this.props.account.loadVideoLessons()   
    }

    render(){
        const { account } = this.props

        if (!account.videoLessons){
            return <Loading />
        }

        return (
            <div className="row">
                {account.videoLessons.map(obj => {
                    return (
                        <div key={obj.id} className="grid4 tb6 xs12">
                            <div className="ytbox">
                                <div className="ytbox_video">
                                    <Player
                                        fileUrl={obj.fileUrl}
                                        link={obj.link} />
                                </div>

                                <div className="ytbox_name">
                                    <a href="#">{obj.name}</a>
                                </div>
                            </div>
                        </div>                    
                    )
                })}
            </div>
        )
    }
}


VideoLessonsTab.propTypes = {
    account: MPropTypes.observableObject
}
