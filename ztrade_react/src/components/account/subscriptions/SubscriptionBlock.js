import React from 'react'
import PropTypes from 'prop-types'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'

import SubscriptionBlockStep1 from './private/SubscriptionBlockStep1'
import SubscriptionBlockStep2 from './private/SubscriptionBlockStep2'
import SubscriptionBlockStep3 from './private/SubscriptionBlockStep3'

/**
 * If user not paid for subscription - SubscriptionBlock
 * can be in 3 states (steps)
 */
@inject('account', 'routing')
@observer
export default class SubscriptionBlock extends React.Component {
    state = {
        step: 1
    }

    componentDidMount() {
        const $ = window.$

        $(".js_modal_subscrip1").fancybox({
            autoFocus: false,
            touch: false,
            closeBtn: false,
            btnTpl: { close: false, smallBtn: false }
        });

        window.addEventListener('popstate', function(event) {
            event.preventDefault()
            // some stuff
        })
    }

    componentWillUnmount() {
        this.onClickSetStep(1)
    }

    onClickSetStep = value => {
        const { account, obj } = this.props
        account.setSubscriptionStep(obj.id, value)
    }

    render() {
        const { subscriptionSteps } = this.props.account
        const { num, obj } = this.props

        const step = subscriptionSteps.get(obj.id) || 1

        if (step === 1) {
            return (
                <SubscriptionBlockStep1
                    obj={obj}
                    num={num}
                    onClickStep={this.onClickSetStep}
                />
            )
        }

        if (step === 2) {
            return (
                <SubscriptionBlockStep2
                    obj={obj}
                    num={num}
                    onClickStep={this.onClickSetStep}
                />
            )
        }

        return (
            <SubscriptionBlockStep3
                obj={obj}
                num={num}
                onClickStep={this.onClickSetStep}
            />
        )
    }
}

SubscriptionBlock.propTypes = {
    account: MPropTypes.observableObject,
    num: PropTypes.number,
    obj: PropTypes.object
}
