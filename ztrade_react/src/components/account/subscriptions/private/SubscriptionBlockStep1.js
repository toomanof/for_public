import React from 'react'
import PropTypes from 'prop-types'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import classNames from 'classnames'
import { rHTML, getTemplateFileSubcscribtion } from 'services/helpers'
import { Modal } from 'components'

@inject('account', 'alert', 'routing',)
@observer
export default class SubscriptionBlockStep1 extends React.Component {

    onClickCol3 = e => {
        const { obj } = this.props
        if (!obj.isPaid) {
            e.preventDefault()
            this.showWarningMessage()
        }
    }

    onClickDownload = e => {
        e.preventDefault()
        const { obj } = this.props

        if (obj.isPaid) {
            this.props.alert.createAlert(
                'На Ваш email отправлена вся информация о подписке'
            )
            this.props.account.subscriptionSendInfoEmail(obj.id)
        } else {
            this.showWarningMessage()
        }
    }

    /**
     * If subscription is paid - redirect to videos url
     */
    onClickVideoReports = e => {
        e.preventDefault()
        const { obj, routing } = this.props

        if (obj.isPaid) {
            routing.push(obj.videosUrl)
        } else {
            this.showWarningMessage()
        }
    }

    onClickLearnMore = e => {
        e.preventDefault()
        this.props.onClickStep(2)
    }

    onClickSubscribe = e => {
        e.preventDefault()
        this.props.onClickStep(3)
    }

    showWarningMessage() {
        this.props.alert.createAlert(
            'Чтобы воспользоваться данным продуктом, оформите пожалуйста подписку',
            'danger'
        )
    }

    render() {
        const { num, obj, onClickStep } = this.props

        const cnMethodContent = classNames('method_content', obj.blockCssClass)
        const cnNum = classNames('method_header_num', { odd: num % 2 === 0 })
        console.log('render SubscriptionBlockStep1', obj)
        return (
            <React.Fragment>
                <div className="module">
                    <div className="method">
                        <div className="method_col">
                            <div className="method_header">
                                <div className={cnNum}>
                                    <b>{num}</b> {obj.name}
                                </div>
                            </div>
                            <div className={cnMethodContent}>
                                <h3>{obj.blockTitle}</h3>
                                {rHTML(obj.blockContent, '')}

                                <div className="method_content_btn">
                                    <a
                                        onClick={this.onClickVideoReports}
                                        href="#"
                                        className="btn btn-more">
                                        <b>{`Видео обзор рынка от ${obj.updateDateStr}`}</b>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div className="method_col method_col2">
                            <div className="method_header">
                                <span className="method_header_span">
                                    {'Инструменты:'}
                                </span>
                                <small>{`Обновление данных от ${obj.updateDateStr}`}</small>
                            </div>
                            <div className="method_content">
                                <div className="methodIcons">
                                    <div className="methodIcons_item">
                                        <img
                                            src={obj.col1IcoUrl}
                                            alt=""
                                        />

                                        <span>{obj.col1Title}</span>
                                        {obj.isPaid ? (
                                            <div className="orange">{`Оформлена до ${obj.dateToStr}`}</div>
                                        ) : (
                                            <a
                                                onClick={this.onClickSubscribe}
                                                href="#">
                                                {'Подписаться'}
                                            </a>
                                        )}
                                    </div>

                                    <div className="methodIcons_item">
                                        <img
                                            src={obj.col2IcoUrl}
                                            alt=""
                                        />

                                        <span>{obj.col2Title}</span>
                                        <a href={obj.files && obj.files.length > 0 && getTemplateFileSubcscribtion(obj)} target="_blank">
                                            {'Скачать'}
                                        </a>
                                    </div>

                                    <div className="methodIcons_item">
                                        <img
                                            src={obj.col3IcoUrl}
                                            alt=""
                                        />

                                        <span>{obj.col3Title}</span>
                                        <a href="#modal_subscrip1"  onClick={this.onClickCol3} className={(obj.isPaid) ? "js_modal_subscrip1": ""}>
                                            {'Скачать'}
                                        </a>
                                    </div>
                                </div>

                                <div className="method_content_btn">
                                    <a
                                        onClick={this.onClickLearnMore}
                                        href="#"
                                        className="btn btn-more">
                                        <b>{'Узнать подробнее'}</b>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

    SubscriptionBlockStep1.propTypes = {
        account: MPropTypes.observableObject,
        alert: MPropTypes.observableObject,
        num: PropTypes.number,
        obj: PropTypes.object,
        onClickStep: PropTypes.func,
        routing: MPropTypes.observableObject
    }
