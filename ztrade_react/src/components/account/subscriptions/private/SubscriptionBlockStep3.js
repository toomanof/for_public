import React from 'react'
import PropTypes from 'prop-types'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import classNames from 'classnames'
import { rHTML } from 'services/helpers'
import { rpHTML } from 'services/helpers'

@inject('account', 'alert', 'payment')
@observer
export default class SubscriptionBlockStep3 extends React.Component {
    onClickPayButton = (numMonths, e) => {
        e.preventDefault()
        const { obj, payment } = this.props

        const form = {
            subscription_id: obj.id,
            num_months: numMonths
        }
        payment.createBillSubscription(form)
    }

    onClickVideoReports = e => {
        e.preventDefault()
        this.props.alert.createAlert(
            'В данный момент Вы не можете просматривать обзоры, для доступа, оформите пожалуйста подписку.',
            'danger'
        )
    }

    render() {
        const { num, obj, payment } = this.props

        const cnMethodContent = classNames('method_content', obj.blockCssClass)
        const cnNum = classNames('method_header_num', { odd: num % 2 === 0 })

        return (
            <div className="module">
                <div className="method">
                    <div className="method_col">
                        <div className="method_header">
                            <div className={cnNum}>
                                <b>{num}</b> {obj.name}
                            </div>
                        </div>
                        <div className={cnMethodContent}>
                            <h3>{obj.blockTitle}</h3>
                            {rHTML(obj.blockContent, '')}

                            <div className="method_content_btn">
                                <a
                                    href="#"
                                    className="btn btn-more"
                                    onClick={this.onClickVideoReports}>
                                    <b>{`Видео обзор рынка от ${obj.updateDateStr}`}</b>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div className="method_col method_col2">
                        <div className="method_header">
                            <span className="method_header_span">
                                {'Инструменты:'}
                            </span>
                            <small>{'Инструменты для успешной торговли'}</small>
                        </div>

                        <div className="method_content">
                            <div className="methodIcons methodIcons2">
                                <div className="methodIcons_item">
                                    <div className="mImg">
                                        <div className="mImg_img">
                                            <img
                                                src="/images/lk-tools/icon_2.svg"
                                                alt=""
                                            />

                                            <div className="mImg_price">
                                                {`${obj.price1Number} руб`}
                                            </div>
                                        </div>
                                    </div>
                                    <span className="mi_name">
                                        {'Подписка на 1 мес'}
                                    </span>
                                    {rpHTML(obj.col1Html)}
                                    <button
                                        disabled={
                                            payment.isCreateBillInProgress
                                        }
                                        onClick={this.onClickPayButton.bind(
                                            null,
                                            1
                                        )}
                                        className="btn">
                                        {'Оформить подписку'}
                                    </button>
                                </div>

                                <div className="methodIcons_item">
                                    <div className="mImg">
                                        <div className="mImg_img">
                                            <img
                                                src="/images/lk-tools/icon_2.svg"
                                                alt=""
                                            />

                                            <div className="mImg_price">
                                                {`${obj.price3Number} руб`}
                                            </div>
                                        </div>
                                    </div>
                                    <span className="mi_name">
                                        {'Подписка на 3 мес'}
                                    </span>
                                    {rpHTML(obj.col2Html)}
                                    <button
                                        disabled={
                                            payment.isCreateBillInProgress
                                        }
                                        onClick={this.onClickPayButton.bind(
                                            null,
                                            3
                                        )}
                                        className="btn">
                                        {'Оформить подписку'}
                                    </button>
                                </div>

                                <div className="methodIcons_item">
                                    <div className="mImg">
                                        <div className="mImg_img">
                                            <img
                                                src="/images/lk-tools/icon_2.svg"
                                                alt=""
                                            />

                                            <div className="mImg_price">
                                                {`${obj.price6Number} руб`}
                                            </div>
                                        </div>
                                    </div>
                                    <span className="mi_name">
                                        {'Подписка на 6 мес'}
                                    </span>
                                    {rpHTML(obj.col3Html)}
                                    <button
                                        disabled={
                                            payment.isCreateBillInProgress
                                        }
                                        onClick={this.onClickPayButton.bind(
                                            null,
                                            6
                                        )}
                                        className="btn">
                                        {'Оформить подписку'}
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

SubscriptionBlockStep3.propTypes = {
    account: MPropTypes.observableObject,
    alert: MPropTypes.observableObject,
    num: PropTypes.number,
    obj: PropTypes.object,
    onClickStep: PropTypes.func,
    payment: MPropTypes.observableObject
}
