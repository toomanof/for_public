import React from 'react'
import PropTypes from 'prop-types'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Player } from 'components'
import classNames from 'classnames'

@inject('account')
@observer
export default class SubscriptionBlockStep2 extends React.Component {
    state = {
        activeVideoId: this.props.obj.beginVideos[0].id
    }

    onClickVideo = (id, e) => {
        e.preventDefault()
        this.setState({ activeVideoId: id })
    }

    onClickSubscribe = e => {
        e.preventDefault()
        this.props.onClickStep(3)
    }

    get activeVideo() {
        const { activeVideoId } = this.state

        if (activeVideoId) {
            for (let video of this.props.obj.beginVideos) {
                if (video.id === activeVideoId) {
                    return video
                }
            }
        }
        return null
    }

    render() {
        const { obj, num, onClickStep } = this.props
        const cnNum = classNames('method_header_num', { odd: num % 2 === 0 })


        return (
            <div className="module">
                <div className="method">
                    <div className="method_col">
                        <div className="method_header">
                            <div className={cnNum}>
                                <b>{num}</b> {obj.name}
                            </div>
                        </div>

                        <div className="method_content orange2">
                            <h3>{'Часто задаваемые вопросы:'}</h3>
                            <ul className="dotted">
                                {obj.beginVideos.map(vobj => {
                                    return (
                                        <li key={vobj.id}>
                                            <a
                                                href="#"
                                                onClick={this.onClickVideo.bind(
                                                    null,
                                                    vobj.id
                                                )}>
                                                {vobj.name}
                                            </a>
                                        </li>
                                    )
                                })}
                            </ul>
                            {!obj.isPaid && (
                                <div className="method_content_btn">
                                    <a
                                        onClick={this.onClickSubscribe}
                                        href="#"
                                        className="btn btn-more-blue">
                                        <b>{'Оформить подписку'}</b>
                                    </a>
                                </div>
                            )}
                        </div>
                    </div>
                    <div className="method_col method_col2">
                        <div className="method_header">
                            <span className="method_header_span">
                                {'Ответы на вопросы:'}
                            </span>
                            <small>{'Инструменты для успешной торговли'}</small>
                        </div>

                        <div className="method_content">
                            {this.activeVideo && (
                                <Player
                                    height={400}
                                    link={this.activeVideo.link}
                                    fileUrl={this.activeVideo.file_url}
                                />
                            )}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

SubscriptionBlockStep2.propTypes = {
    account: MPropTypes.observableObject,
    num: PropTypes.number,
    obj: PropTypes.object,
    onClickStep: PropTypes.func
}
