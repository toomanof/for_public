import React from 'react'
import ReactDOM from 'react-dom'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'
import classNames from 'classnames'
import { logMessage } from 'services/helpers'


@inject('menu', 'user')
@observer
export default class AccountLeftMenu extends React.Component{
    constructor(props){
        super(props)
        this.menuRef = React.createRef()
    }    

    componentDidMount(){
        document.addEventListener('click', this.handleClick)
    }

    componentWillUnmount(){
        document.removeEventListener('click', this.handleClick)
        this.props.menu.closeMobileAccountMenu()
    }

    handleClick = (e) => {
        if (!this.props.menu.isMobileAccountMenuOpened){
            return
        }

        const area = ReactDOM.findDOMNode(this.menuRef.current)
        logMessage('AccontMenu handle click')

        // Open button should not be handled
        if (e.target.classList.contains('unav')){
            return
        }

        if (area && !area.contains(e.target)) {
            this.props.menu.closeMobileAccountMenu()    
        }
    }

    onClickClose = (e) => {
        e.preventDefault()
        this.props.menu.closeMobileAccountMenu()
    }

    render(){
        const { menu, user } = this.props

        const cn = classNames(
            'userleftmenu',
            {opened: menu.isMobileAccountMenuOpened}
        )

        const styleOverlay = menu.isMobileAccountMenuOpened ? 'block' : 'none'

        return (
            <React.Fragment>
                <div ref={this.menuRef} className={cn}>
                    <div className="userleftmenu_logo hidden-sm">
                        <Link to="/" className="logo">
                            <img
                                src="/images/logo2.png"
                                alt=""
                                srcSet="/images/logo2-2x.png 2x" />
                        </Link>
                    </div>

                    <ul>
                        {menu.accountMenu.map(obj => {
                            const isActive = (menu.ami && menu.ami.id === obj.id) ? true : false
                            const cn = classNames(obj.className, {active: isActive})

                            return (
                                <li key={obj.id} className={cn}>
                                    <Link to={obj.url}>{obj.name}</Link>
                                </li>
                            )
                        })}
                    </ul>

                    <div className="userleftmenu_id visible-sm">
                        <div className="userid">
                            {'Ваш личный '}<b>{`ID ${user.profile.uid}`}</b>
                        </div>
                    </div>

                    <a
                        onClick={this.onClickClose}
                        href=""
                        className="unav_close visible-sm" />
                </div>
                
                <div
                    className="userleftmenu_overlay"
                    style={{display: styleOverlay}} />
            
            </React.Fragment>
        )
    }
}


AccountLeftMenu.propTypes = {
    menu: MPropTypes.observableObject,
    user: MPropTypes.observableObject
}