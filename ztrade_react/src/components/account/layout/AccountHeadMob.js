import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'

@inject('menu', 'user',)
@observer
export default class AccountHeadMob extends React.Component {

    onClickShowPopup(e){
        this.props.menu.openMenuLK()
    }

    onClickHidePopup(e){
        this.props.menu.closeMenuLK()
    }

    onClickMenu = e => {
        e.preventDefault()
        this.props.menu.toggleMobileAccountMenu()
    }

    render() {
        const { user } = this.props

        return (
            <div className="uheadmob visible-sm">
                <div className="wrap">
                    <Link to="/" className="logo">
                        <img
                            src="/images/logo2.png"
                            alt=""
                            srcSet="/images/logo2-2x.png 2x"
                        />
                    </Link>

                    <div className="pull-right">
                        <div className="user_cabinet">
                            <a href="#" id='btn_lk' className="user_cabinet_btn" onClick={this.onClickShowPopup.bind(this)} />
                            <div className="usercab_popup">
                                <ul>
                                    <li className="us">
                                        <span>{user.profile.fullName}</span>
                                    </li>
                                    <li>
                                        <Link to="/account/profile" onClick={this.onClickHidePopup.bind(this)}>
                                            {'Мои данные'}
                                        </Link>
                                    </li>
                                    <li>
                                        <span>{`ID ${user.profile.uid}`}</span>
                                    </li>
                                    <li className="exit">
                                        <Link to="/site/logout">{'Выйти'}</Link>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <a
                            onClick={this.onClickMenu}
                            href=""
                            className="unav"
                        />
                    </div>
                </div>
            </div>
        )
    }
}

AccountHeadMob.propTypes = {
    menu: MPropTypes.observableObject,
    user: MPropTypes.observableObject
}
