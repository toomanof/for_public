import React from 'react'
import PropTypes from 'prop-types'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import classNames from 'classnames'


/**
 * Clone of Tabs component with special logic for training page Filter
 */
@inject('account', 'tabs')
@observer
export default class Tabs extends React.Component{
    onClickTab = (index, e) => {
        e.preventDefault()
        this.props.tabs.setActiveIndex(index)
    }

    onChangeTab = (e) => {
        const index = parseInt(e.target.value, 10)
        this.props.tabs.setActiveIndex(index)
    }

    onClickFilter = (e) => {
        e.preventDefault()
        this.props.account.toggleStrategyFilterShowOnMobile()
    }

    get cnSelect(){
        const { tabs, selectClassName } = this.props

        let s = 'seltabs visible-sm'

        if (tabs.activeIndex === 0){
            s += ' seltabs_f'
        }

        if (selectClassName && selectClassName.length > 0){
            s += ' '
            s += selectClassName
        }
        return s
    }

    render(){
        const { children, tabs, selectClassName } = this.props

        return (
            <React.Fragment>
                <ul className="usertabs hidden-sm">
                    {tabs.arr.map((obj, index) => {
                        let active = index === tabs.activeIndex ? true : false
                        
                        const firstClass = obj.hasOwnProperty('className') ? obj.className : ''
                        let cn = classNames(firstClass, {active: active})
                        
                        return (
                            <li key={index} className={cn}>
                                <a
                                    onClick={this.onClickTab.bind(null, index)}
                                    href="">{obj.name}</a>
                            </li>                            
                        )
                    })}
                </ul>

                <div className={this.cnSelect}>
                    <select onChange={this.onChangeTab} className="selectbox">
                        {tabs.arr.map((obj, index) => {
                            return (
                                <option value={index} key={index}>
                                    {obj.name}
                                </option>
                            )
                        })}
                    </select>
                    {tabs.activeIndex === 0 &&
                        <a
                            onClick={this.onClickFilter}
                            href="#" className="seltabs_filter">

                            {'Фильтр'}
                        </a>
                    }
                    

                </div>

                {React.Children.toArray(children).map((child, index) => {
                    const active = index === tabs.activeIndex ? true : false
                    const style = active ? 'block' : 'none'
                    return (
                        <div key={index} style={{display: style}}>
                            {child}
                        </div>
                    )
                })}
            </React.Fragment>
        )
    }
}


Tabs.propTypes = {
    account: MPropTypes.observableObject,
    children: PropTypes.arrayOf(PropTypes.node),
    selectClassName: PropTypes.string,
    tabs: MPropTypes.observableObject,
}
