import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'

@inject('menu', 'user')
@observer
export default class AccountHead extends React.Component {

    onClickShowPopup(e){
        const { menu } = this.props
        menu.openMenuLK()
    }

    onClickHidePopup(e){
        const { menu } = this.props
        menu.closeMenuLK()
    }

    render() {
        const { user } = this.props
        return (
            <div className="userhead hidden-sm">
                <div className="wrap">
                    <div className="row">
                        <div className="grid4 hidden-md">
                            <div className="sitedesc hidden-tb">
                                <b>{'16'}</b>
                                {' лет опыт в торговле'}
                                <br />
                                {' Более '}
                                <b>{'3 000'}</b>
                                {' подписчиков на наши сигналы'}
                            </div>
                        </div>
                        <div className="userid">
                            {'Ваш личный '}
                            <b>{`ID ${user.profile.uid}`}</b>
                        </div>

                        <div className="grid4 userlang">
                            <div className="pull-right">
                                <div className="usercab">
                                    <a href="#" id='btn_lk' className="btn btn-blue" onClick={this.onClickShowPopup.bind(this)}>
                                        {'Личный кабинет'}
                                    </a>
                                    <div className="usercab_popup">
                                        <ul>
                                            <li className="us">
                                                <span>
                                                    {user.profile.fullName}
                                                </span>
                                            </li>
                                            <li>
                                                <Link to="/account/profile" onClick={this.onClickHidePopup.bind(this)}>
                                                    {'Мои данные'}
                                                </Link>
                                            </li>
                                            <li>
                                                <span>{`ID ${user.profile.uid}`}</span>
                                            </li>
                                            <li className="exit">
                                                <Link to="/site/logout">
                                                    {'Выйти'}
                                                </Link>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

AccountHead.propTypes = {
    menu: MPropTypes.observableObject,
    user: MPropTypes.observableObject
}
