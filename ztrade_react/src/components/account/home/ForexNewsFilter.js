import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { Checkbox, CheckedItems } from 'components'
import classNames from 'classnames'

@inject('account', 'menu')
@observer
export default class ForexNewsFilter extends React.Component {
    state = {
        isFilterOpened: null
    }

    componentDidMount() {
        this.props.account.loadForexNewsFilter()
    }

    /**
     * isFilterOpened - null boolean
     * Initially - it is null
     * After first click - true
     * Then toggle true/false
     *
     * Algo to control display:
     * null - no style
     * true - display: block
     * false - display: none
     */
    onClickArrow = e => {
        e.preventDefault()
        if (this.state.isFilterOpened === null) {
            this.setState({ isFilterOpened: true })
        } else {
            this.setState({ isFilterOpened: !this.state.isFilterOpened })
        }
    }

    onChangeSound = value => {
        const { account } = this.props
        account.updateForexNewsFilter({ is_sound: value })
    }

    /**
     * Callback for category checkboxes
     * Update local state (to show immediate result)
     * Update on server (after server update they also will be updated in state again)
     */
    onChangeCategories = items => {
        const { account } = this.props
        account.setForexNewsCategoryCheckedItems(items)

        const mainItem = items.find(obj => obj.id === null)
        const checkedItems = items.filter(
            obj => obj.id !== null && obj.isChecked
        )

        const form = {
            is_all_categories: mainItem.isChecked,
            forex_category_ids: checkedItems.map(obj => obj.id)
        }
        account.updateForexNewsFilter(form)
    }

    /**
     * Callback for symbols checkboxes
     * Update local state (to show immediate result)
     * Update on server (after server update they also will be updated in state again)
     */
    onChangeSymbols = items => {
        const { account } = this.props
        account.setForexNewsSymbolCheckedItems(items)

        const mainItem = items.find(obj => obj.id === null)
        const checkedItems = items.filter(
            obj => obj.id !== null && obj.isChecked
        )

        const form = {
            is_all_symbols: mainItem.isChecked,
            forex_symbol_ids: checkedItems.map(obj => obj.id)
        }
        account.updateForexNewsFilter(form)
    }

    render() {
        const { account } = this.props

        if (!account.forexNewsFilter) {
            return null
        }

        const { isFilterOpened } = this.state
        let style = {}
        if (isFilterOpened === true) {
            style = { display: 'block' }
        } else if (isFilterOpened === false) {
            style = { display: 'none' }
        }

        const cnArrow = classNames('fshow', { open: isFilterOpened })

        return (
            <div className="module filternews">
                <div className="filternews_head">
                    {'Отбирать новости по '}
                    <a
                        onClick={this.onClickArrow}
                        href=""
                        className={cnArrow}
                    />
                </div>

                <div className="filternews_hide" style={style}>
                    <div className="filternews_name">{'Пара'}</div>
                    <div className="filternews_box">
                        <CheckedItems
                            items={account.forexNewsSymbolCheckedItems}
                            onChange={this.onChangeSymbols}
                        />
                    </div>

                    <div className="filternews_name">{'Тема'}</div>
                    <div className="filternews_box filternews_box_1">
                        <CheckedItems
                            items={account.forexNewsCategoryCheckedItems}
                            onChange={this.onChangeCategories}
                        />
                    </div>

                    <div className="filternews_sound">
                        <label className="checkf">
                            <Checkbox
                                isChecked={account.forexNewsFilter.isSound}
                                onChange={this.onChangeSound}
                                hasLabel={false}
                            />
                            <span className="sound">
                                {'Звуковые уведомления'}
                            </span>
                        </label>
                    </div>
                </div>
            </div>
        )
    }
}

ForexNewsFilter.propTypes = {
    account: MPropTypes.observableObject,
    menu: MPropTypes.observableObject
}
