import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'


@inject('menu')
@observer
export default class PublishMaterials extends React.Component{
    render(){
        return (
            <div className="module popular">
                <div className="module_title">
                    {'Публикуй свои материалы на '}<span>{'ZTrade.ru'}</span>
                </div>
                
                <div className="module_content">
                    <div className="publ">
                        <p>
                            {'Вы заинтересованы в бесплатной публикации своих '}
                            <a href="">{'новостей'}</a>{' или '}
                        <a href="">{'аналитики'}</a>{' на нашем вебсайте?'}
                    </p>
                    
                    <a href="" className="publ_more">{'Свяжитесь с нами!'}</a>
                    </div>
                </div>
            </div>
        )
    }
}


PublishMaterials.propTypes = {
    menu: MPropTypes.observableObject
}