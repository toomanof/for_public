import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'


@inject('menu')
@observer
export default class MostPopular extends React.Component{

    render(){
        return (
            <div className="module popular">
                <div className="module_title">{'Наиболее популярные'}</div>
                {this.props.populars && this.props.populars.map((obj, index) => { return(
                    <div key={index} className="module_content text-center">
                        <a
                            rel="noopener"
                            target="_blank"
                            key={obj.id}
                            href={obj.url}>
                            <img
                                src={obj.picUrl}
                                alt=""
                                srcSet={`${obj.picSrcsetUrl} 2x`}
                            />
                        </a>
                    </div>
                    )
                })}

            </div>
        )
    }
}


MostPopular.propTypes = {
    menu: MPropTypes.observableObject
}