import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'


@inject('account', 'menu')
@observer
export default class ForexNews extends React.Component{
    componentDidMount(){
        this.props.account.loadForexNews()   
    }

    onClick = (e) => {
        e.preventDefault()
    }

    render(){
        const { account } = this.props

        if (!account.forexNews){
            return null
        }

        return (
            <div className="grid7 md12">
                {account.forexNews.map(obj => {
                    return (
                        <div key={obj.id} className="newsbox">
                            <div className="newsbox_date">
                                <span>
                                    {obj.rusDayMonthStr('dt')}
                                    {','}
                                    <br />
                                    {obj.dt.format('HH:mm')}{' GMT'}
                                </span>
                            </div>
                            
                            <div className="newsbox_text">
                                <div className="newsbox_name">
                                    <a href="" onClick={this.onClick}>
                                        {obj.title}
                                    </a>
                                </div>
                                <div className="newsbox_cat">
                                    {obj.supplier}
                                </div>
                            </div>
                        </div>
                    )
                })}
            </div>
        )
    }
}


ForexNews.propTypes = {
    account: MPropTypes.observableObject,
    menu: MPropTypes.observableObject
}