import React from 'react'
import { PropTypes as MPropTypes, inject, observer } from 'mobx-react'
import { rHTML } from 'services/helpers'


@inject('menu')
@observer
export default class EconomicCalendar extends React.Component{
    constructor(props){
        super(props)
        this.ref = React.createRef()
    }

    state = {
        iframeWidth: 400,
        iframeHeight: 400,
    }

    componentDidMount(){
        // Init width of iframe depends on width of container
        this.initWidth()    
    }

    initWidth(){
        const node = this.ref.current
        const width = node.offsetWidth
        if (width){
            this.setState({
                iframeWidth: width,
                iframeHeight: parseInt(width/1.4, 10) 
            })
        }
    }

    render(){
        const s = `
            <iframe
                src="https://sslecal2.forexprostools.com?columns=exc_flags,exc_currency,exc_importance,exc_actual,exc_forecast,exc_previous&importance=3&features=datepicker,timezone,timeselector,filters&countries=25,4,17,39,72,26,10,6,37,43,56,36,5,61,22,12,35&calType=day&timeZone=18&lang=7"
                width="${this.state.iframeWidth}"
                height="${this.state.iframeHeight}"
                frameborder="0"
                allowtransparency="true"
                marginwidth="0"
                marginheight="0" />

            <div className="poweredBy" style="font-family: Arial, Helvetica, sans-serif;">
                <span style="font-size: 11px;color: #333333;text-decoration: none;">
                    Экономический онлайн-календарь от
                    <a
                        href="https://ru.investing.com/"
                        rel="nofollow"
                        target="_blank"
                        style="font-size: 11px; color: rgb(6, 82, 157);
                        font-weight: bold;
                        text-decoration: line-through !important;"
                        className="underline_link">

                        Investing.com Россия
                    </a>, ведущего финансового портала
                </span>
            </div>
        `
        return (
            <div ref={this.ref} className="module popular">
                <div className="module_title">{'Ближайшие экономические события'}</div>
                {rHTML(s, '')}
            </div>
        )
    }
}


EconomicCalendar.propTypes = {
    menu: MPropTypes.observableObject
}
