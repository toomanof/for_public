// ACCOUNT

// home
import EconomicCalendar from './account/home/EconomicCalendar'
import ForexNews from './account/home/ForexNews'
import ForexNewsFilter from './account/home/ForexNewsFilter'
import MostPopular from './account/home/MostPopular'
import PublishMaterials from './account/home/PublishMaterials'

// layout
import AccountHead from './account/layout/AccountHead'
import AccountHeadMob from './account/layout/AccountHeadMob'
import AccountLeftMenu from './account/layout/AccountLeftMenu'
import Tabs2 from './account/layout/Tabs2'

// signals
import SgnMarket from './account/signals/SgnMarket'
import SgnMarketMenu from './account/signals/SgnMarketMenu'
import SgnNotifications from './account/signals/SgnNotifications'
import SgnRating from './account/signals/SgnRating'
import SgnRules from './account/signals/SgnRules'
import SgnSamples from './account/signals/SgnSamples'
import SgnSignals from './account/signals/SgnSignals'
import SgnStat from './account/signals/SgnStat'
import TariffBox from './account/signals/TariffBox'

// subscriptions
import SubscriptionBlock from './account/subscriptions/SubscriptionBlock'

// training
import FilterTab from './account/training/FilterTab'
import StrategiesTab from './account/training/StrategiesTab'
import TutorialBookTab from './account/training/TutorialBookTab'
import VideosTab from './account/training/VideosTab'
import VideoLessonsTab from './account/training/VideoLessonsTab'

// SITE

// broker
import BrokerPammBox from './site/broker/BrokerPammBox'
import BrokerRow from './site/broker/BrokerRow'
import BrokerRowFeatured from './site/broker/BrokerRowFeatured'
import BrokerTabAccountTypes from './site/broker/BrokerTabAccountTypes'
import BrokerTabInfo from './site/broker/BrokerTabInfo'
import BrokerTabParams from './site/broker/BrokerTabParams'
import BrokerTabReviews from './site/broker/BrokerTabReviews'
import BrokerTabs from './site/broker/BrokerTabs'
import BrokerTopBox from './site/broker/BrokerTopBox'
import PammBox from './site/broker/PammBox'

//excanghe
import ExchangeRow from './site/exchange/ExchangeRow'
import ExchangeTabAccountTypes from './site/exchange/ExchangeTabAccountTypes'
import ExchangeTabInfo from './site/exchange/ExchangeTabInfo'
import ExchangeTabParams from './site/exchange/ExchangeTabParams'
import ExchangeTabReviews from './site/exchange/ExchangeTabReviews'
import ExchangeTabs from './site/exchange/ExchangeTabs'
import ExchangeTopBox from './site/exchange/ExchangeTopBox'
import ExchangeRowFeatured from './site/exchange/ExchangeRowFeatured';
import  ExchangeAddReviewModal from './site/modal/ExchangeAddReviewModal'

//course
import SideBarCourse from './site/course/SideBar'
import ContentCourse from './site/course/Content'

// form
import ButtonSubmit from './site/form/ButtonSubmit'
import Checkbox from './site/form/Checkbox'
import Checkbox2 from './site/form/Checkbox2'
import ErrorBlock from './site/form/ErrorBlock'
import Input from './site/form/Input'
import MultiSelect from './site/form/MultiSelect'
import RadioButton from './site/form/RadioButton'
import Select from './site/form/Select'
import Textarea from './site/form/Textarea'

// hoc
import Auth from './site/hoc/Auth'
import AuthAlready from './site/hoc/AuthAlready'
import AuthCheck from './site/hoc/AuthCheck'
import AuthRoute from './site/hoc/AuthRoute'
import Base from './site/hoc/Base'
import BaseLayout from './site/hoc/BaseLayout'
import BaseAuthLayout from './site/hoc/BaseAuthLayout'

// home
import ArticlesSlider from './site/home/ArticlesSlider';
import HomeBenefits from './site/home/HomeBenefits'
import MainHomeSlider from './site/home/MainHomeSlider'
import HomeSlider from './site/home/HomeSlider'
import HomeSignalInfo from './site/home/HomeSignalInfo'
import HomeSignalSlider from './site/home/HomeSignalSlider'
import TopHeaderSlider from './site/home/TopHeaderSlider'

// layout
import Alert from './site/layout/Alert'
import Alerts from './site/layout/Alerts'
import Breadcrumbs from './site/layout/Breadcrumbs'
import CheckedItems from './site/layout/CheckedItems'
import Footer from './site/layout/Footer'
import FooterMenu from './site/layout/FooterMenu'
import Header from './site/layout/Header'
import Loading from './site/layout/Loading'
import LoadingPage from './site/layout/LoadingPage'
import Modal from './site/layout/Modal'
import Paginator from './site/layout/Paginator'
import Player from './site/layout/Player'
import SocialBox from './site/layout/SocialBox'
import Tabs from './site/layout/Tabs'
import TopMenu from './site/layout/TopMenu'
import WSUpdater from './site/layout/WSUpdater'

// menu
import ContentItem from './site/menu/ContentItem'
import DownloadBookForm from './site/menu/DownloadBookForm'
import MenuBooks from './site/menu/MenuBooks'
import MenuContent from './site/menu/MenuContent'
import MenuContentIco from './site/menu/MenuContentIco'
import MenuContentPic from './site/menu/MenuContentPic'

// modals
import BrokerAddReviewModal from './site/modal/BrokerAddReviewModal'
import ProductBuyModal from './site/modal/ProductBuyModal'
import SignupChangeDataModal from './site/modal/SignupChangeDataModal'
import SupportModal from './site/modal/SupportModal'
import BrokerWhichAccountOpenModal from './site/modal/BrokerWhichAccountOpen'
import ExchangeWhichAccountOpenModal from './site/modal/ExchangeWhichAccountOpen'
import AccountSubScripActiveModal1 from './site/modal/AccountSubScripActiveModal1';
import AccountSubScripActiveModal2 from './site/modal/AccountSubScripActiveModal2';

// shop
import ProductBox from './site/shop/ProductBox'
import ProductPics from './site/shop/ProductPics'

// sidebar
import SideBanner from './site/sidebar/SideBanner'
import SideRazdel from './site/sidebar/SideRazdel'
import SideMenuLinks from './site/sidebar/SideMenuLinks'
import SideMobileMenu from './site/sidebar/SideMobileMenu'
import SideRecommend from './site/sidebar/SideRecommend'
import SideSearch from './site/sidebar/SideSearch'
import SideSocial from './site/sidebar/SideSocial'
import SideWrapper from './site/sidebar/SideWrapper'

// tools
import ExchangeBlock from './site/tools/ExchangeBlock'

export { ArticlesSlider }
export { AccountHead }
export { AccountHeadMob }
export { AccountLeftMenu }
export { Alert }
export { Alerts }
export { Auth }
export { AuthAlready }
export { AuthCheck }
export { AuthRoute }
export { Base }
export { BaseLayout }
export { BaseAuthLayout }
export { Breadcrumbs }
export { BrokerAddReviewModal }
export { BrokerPammBox }
export { BrokerRow }
export { BrokerRowFeatured }
export { BrokerTabAccountTypes }
export { BrokerTabInfo }
export { BrokerTabParams }
export { BrokerTabReviews }
export { BrokerTabs }
export { BrokerTopBox }
export { ButtonSubmit }
export { Checkbox }
export { Checkbox2 }
export { CheckedItems }
export { ContentCourse }
export { ContentItem }
export { DownloadBookForm }
export { EconomicCalendar }
export { ErrorBlock }
export { ExchangeAddReviewModal }
export { ExchangeBlock }
export { ExchangeRow }
export { ExchangeRowFeatured }
export { ExchangeTabAccountTypes }
export { ExchangeTabInfo }
export { ExchangeTabParams }
export { ExchangeTabReviews }
export { ExchangeTabs }
export { ExchangeTopBox }
export { ExchangeWhichAccountOpenModal }
export { FilterTab }
export { Footer }
export { FooterMenu }
export { ForexNews }
export { ForexNewsFilter }
export { Header }
export { HomeSlider }
export { HomeSignalInfo }
export { HomeSignalSlider }
export { HomeBenefits }
export { Input }
export { MultiSelect }
export { Loading }
export { LoadingPage }
export { MenuBooks }
export { MenuContent }
export { MenuContentPic }
export { MenuContentIco }
export { Modal }
export { MostPopular }
export { Paginator }
export { PammBox }
export { Player }
export { ProductBuyModal }
export { ProductBox }
export { ProductPics }
export { PublishMaterials }
export { RadioButton }
export { Select }
export { SgnMarket }
export { SgnMarketMenu }
export { SgnNotifications }
export { SgnRating }
export { SgnRules }
export { SgnSamples }
export { SgnSignals }
export { SgnStat }
export { SideBarCourse }
export { SideBanner }
export { SideRazdel }
export { SideMenuLinks }
export { SideMobileMenu }
export { SideRecommend }
export { SideSearch }
export { SideSocial }
export { SideWrapper }
export { SignupChangeDataModal }
export { SocialBox }
export { StrategiesTab }
export { SubscriptionBlock }
export { SupportModal }
export { Tabs }
export { Tabs2 }
export { TariffBox }
export { Textarea }
export { TopHeaderSlider }
export { TopMenu }
export { TutorialBookTab }
export { VideosTab }
export { VideoLessonsTab }
export { WSUpdater }
export { BrokerWhichAccountOpenModal }
export { AccountSubScripActiveModal1 }
export { AccountSubScripActiveModal2 }
export { MainHomeSlider }