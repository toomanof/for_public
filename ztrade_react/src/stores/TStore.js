import { observable, action } from 'mobx'


import{
    apiCall,
    TRANSLATIONS_URL,
} from 'api'


class TStore{
    @observable d = null
    
    @action
    load = async () => {
        if (this.d){
            return
        }

        let result = await apiCall(TRANSLATIONS_URL, 'GET', {}, false)
        if(!result.isError){
            this.d = result.data
        }
    }
}

const tStore = new TStore()
export default tStore