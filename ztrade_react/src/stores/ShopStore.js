import { observable, action } from 'mobx'
import { apiCall } from 'api'

import {
    getMenuStore,
    getPaginatorStore } from 'stores'

import { constructUrlWithParams } from 'services/helpers'

import { ActiveMenuItemModel, GeneralModel, ProductModel } from 'models'

import {
    SHOP_CATEGORIES_URL,
    SHOP_MARKETS_URL,
    SHOP_PRODUCTS_URL,
    SHOP_PRODUCT_URL
} from 'api'


class ShopStore{
    @observable errors = null
    @observable categories = null
    @observable markets = null
    @observable products = null
    @observable productItem = null
    @observable selectedCategoryId = ''
    @observable selectedMarketId = ''
    
    @action
    clear(){
        this.errors = null
        this.products = null
        this.selectedCategoryId = ''
        this.selectedMarketId = ''
    }

    @action
    loadCategories = async () => {
        if (this.categories){
            return
        }

        let result = await apiCall(SHOP_CATEGORIES_URL, 'GET', {}, false)
        if(!result.isError){
            let arr = []
            for (let obj of result.data){
                arr.push(new GeneralModel(obj))
            }
            this.categories = arr
        }
    }

    @action
    loadMarkets = async () => {
        if (this.markets){
            return
        }

        let result = await apiCall(SHOP_MARKETS_URL, 'GET', {}, false)
        if(!result.isError){
            let arr = []
            for (let obj of result.data){
                arr.push(new GeneralModel(obj))
            }
            this.markets = arr
        }
    }

    @action
    loadProducts = async (params=null, perPage=6) => {
        this.products = null

        let url = SHOP_PRODUCTS_URL
        if (params){
            url = constructUrlWithParams(url, params)
        }

        let result = await apiCall(url, 'GET', {}, false)
        if(!result.isError){
            let arr = []
            for (let obj of result.data){
                arr.push(new ProductModel(obj))
            }
            this.products = arr
            const store = getPaginatorStore()
            store.initPaginator(arr, 1, perPage)
        }
    }

    @action
    loadProduct = async (slug, onNotFound) => {
        this.productItem = null

        const url = SHOP_PRODUCT_URL.replace('<slug>', slug)

        let result = await apiCall(url, 'GET', {}, false)
        if(!result.isError){
            this.productItem = new ProductModel(result.data)    

            // Carefully: do not mutate otiginal menu item
            const mstore = getMenuStore()
            const menuItem = mstore.mapByUrl.get('/shop')

            const breadcrumbs = [...menuItem.breadcrumbs]
            breadcrumbs.push({name: this.productItem.name, url: null})

            const ami = new ActiveMenuItemModel(this.productItem)
            ami.breadcrumbs = breadcrumbs
            mstore.setActiveMenuItem(ami)

        } else {
            onNotFound()
        }
    }

    @action
    setSelectedCategoryId(value){
        this.selectedCategoryId = value
        this.reloadProducts()
    }

    @action
    setSelectedMarketId(value){
        this.selectedMarketId = value
        this.reloadProducts()
    }

    /**
     * Filter and reload products
     */
    @action
    reloadProducts() {
        if (this.selectedCategoryId.length === 0 && this.selectedMarketId.length === 0){
            this.loadProducts()
            return 
        }

        let params = {}
        if (this.selectedCategoryId){
            params.category_ids = this.selectedCategoryId
        }

        if (this.selectedMarketId){
            params.market_ids = this.selectedMarketId
        }

        this.loadProducts(params)
    }


    
}

const shopStore = new ShopStore();
export default shopStore