import { observable, action } from 'mobx'
import { getAlertStore } from 'stores'

import{
    apiCall,
    CREATE_BILL_PRODUCT_AUTH_URL,
    CREATE_BILL_PRODUCT_NOT_AUTH_URL,
    CREATE_BILL_SUBSCRIPTION_URL,
    CREATE_BILL_TARIFF_URL,
} from 'api'


class PaymentStore{
    @observable isCreateBillInProgress = false
    @observable errors = null

    @action
    clear(){
        this.isCreateBillInProgress = false
        this.errors = null
    }

    @action
    async createBillProduct(form, isAuth, cb){
        this.isCreateBillInProgress = true
        this.errors = null

        let url = isAuth ? CREATE_BILL_PRODUCT_AUTH_URL : CREATE_BILL_PRODUCT_NOT_AUTH_URL

        let result = await apiCall(url, 'POST', form, isAuth)
        if (result.isError){
            if (isAuth){
                const store = getAlertStore()
                store.showResultErrors(result)
            } else {
                this.errors = result.data
            }
        } else {
            let bill = result.data
            const redirectUrl = bill.yandex_payment.confirmation_url
            cb && cb()

            if (redirectUrl !== null){
                window.location.href = redirectUrl
            }
        }
        this.isCreateBillInProgress = false
    }

    @action
    async createBillSubscription(form){
        this.isCreateBillInProgress = true
        
        let result = await apiCall(CREATE_BILL_SUBSCRIPTION_URL, 'POST', form, true)
        if (result.isError){
            const store = getAlertStore()
            store.showResultErrors(result)
        } else {
            let bill = result.data
            const redirectUrl = bill.yandex_payment.confirmation_url
            if (redirectUrl !== null){
                window.location.href = redirectUrl
            }
        }
        this.isCreateBillInProgress = false
    }

    @action
    async createBillTariff(form){
        this.isCreateBillInProgress = true

        let result = await apiCall(CREATE_BILL_TARIFF_URL, 'POST', form, true)
        if (result.isError){
            const store = getAlertStore()
            store.showResultErrors(result)
        } else {
            let bill = result.data
            const redirectUrl = bill.yandex_payment.confirmation_url
            if (redirectUrl !== null){
                window.location.href = redirectUrl
            }
        }
        this.isCreateBillInProgress = false
    }
}

const paymentStore = new PaymentStore()
export default paymentStore



