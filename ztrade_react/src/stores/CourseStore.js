import { computed, observable, action } from 'mobx'
import { getAlertStore, getMenuStore } from 'stores'
import { COURSE } from 'services/constants'

import { CourseModel, GeneralModel } from 'models'

import {
    apiCall,
    COURSE_ANALYSIS_URL,
    COURSE_MARKETS_URL,
    COURSES_FREE_URL,
    COURSES_PAID_URL
} from 'api'


class CourseStore {
    @observable active_course = null
    @observable active_analysis = null
    @observable analysis_courses = []
    @observable free_courses = []
    @observable paid_courses = []
    @observable markets = []
    @observable state_markets_nav = {}
    @observable errors = null
    @observable filterAnalysisId = null


    @action
    setActiveCourse(id){
        let arr = this.free_courses.filter(obj => obj.id == id);
        if(arr.length > 0){
            this.active_course = arr[0]
        } else{
            arr = this.paid_courses.filter(obj => obj.id == id);
            if(arr.length > 0){
                this.active_course = arr[0]
            }
        }
        console.log(' ------  setActiveCourse ===== ', this.active_course)
    }

    @action
    setAnalysisFilter(analysis) {
        this.filterAnalysisId = analysis.id
        this.active_analysis = analysis
        this.loadPaidCourses()
        this.loadFreeCourses()
    }

    @action
    loadAnalysis = async () => {
        let result = await apiCall(COURSE_ANALYSIS_URL, 'GET', {}, false)
        if (!result.isError) {
            let arr = []
            for (let obj of result.data) {
                arr.push(new GeneralModel(obj))
            }
            this.analysis_courses = arr
            if(arr.length > 0) {this.active_analysis = arr[0]}
        }
    }

    @action
    loadMarkets = async () => {
        let result = await apiCall(COURSE_MARKETS_URL, 'GET', {}, false)
        if (!result.isError) {
            let arr = []
            let new_state = {}
            let once = true
            for (let obj of result.data) {
                arr.push(new GeneralModel(obj))
                new_state[obj.id] = once
                if(once){
                    once = false
                }
            }
            this.markets = arr
            this.state_markets_nav = new_state
        }
    }

    @action
    loadFreeCourses = async () => {
        let url = this.filterAnalysisId ? COURSES_FREE_URL +'&analysis='+this.filterAnalysisId: COURSES_FREE_URL
        let result = await apiCall(url, 'GET', {}, false)
        if (!result.isError) {
            let arr = []
            for (let obj of result.data) {
                arr.push(new CourseModel(obj))
            }
            console.log(' ------------ loadFreeCourses ------------',url, this.filterAnalysisId, arr)
            this.free_courses = arr
            if( arr.length > 0 ){
                this.active_course = arr[0]
            }
        }
    }

    @action
    loadPaidCourses = async () => {
        let url = this.filterAnalysisId ? COURSES_PAID_URL +'&analysis='+this.filterAnalysisId: COURSES_PAID_URL
        let result = await apiCall(url, 'GET', {}, false)
        if (!result.isError) {
            let arr = []
            for (let obj of result.data) {
                arr.push(new CourseModel(obj))
            }
            console.log(' ------------ loadPaidCourses ------------',url, this.filterAnalysisId, arr)
            this.paid_courses = arr
        }
    }
}


const courseStore = new CourseStore()
export default courseStore
