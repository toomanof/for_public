import { observable, action } from 'mobx'


class ErrorStore{
    @observable errors = null

    @action
    clear(){
        this.errors = null
    }

    @action
    setErrors(errors){
        this.errors = errors
    }
}

const errorStore = new ErrorStore();
export default errorStore