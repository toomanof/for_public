import accountStore from './AccountStore'
import alertStore from './AlertStore'
import brokerStore from './BrokerStore'
import courseStore from './CourseStore'
import chartStore from './ChartStore'
import errorStore from './ErrorStore'
import exchangeStore from './ExchangeStore'
import generalStore from './GeneralStore'
import layoutStore from './LayoutStore'
import menuStore from './MenuStore'
import modalStore from './ModalStore'
import paginatorStore from './PaginatorStore'
import paymentStore from './PaymentStore'
import sgnStore from './SgnStore'
import shopStore from './ShopStore'
import tStore from './TStore'
import tabsStore from './TabsStore'
import userStore from './UserStore'

export { accountStore }
export { alertStore }
export { brokerStore }
export { courseStore }
export { chartStore }
export { errorStore }
export { exchangeStore }
export { generalStore }
export { layoutStore }
export { menuStore }
export { modalStore }
export { paginatorStore }
export { paymentStore }
export { sgnStore }
export { shopStore }
export { tStore }
export { tabsStore }
export { userStore }

export function getAccountStore() {
    return accountStore
}

export function getAlertStore() {
    return alertStore
}

export function getBrokerStore() {
    return brokerStore
}

export function getCourseStore() {
    return courseStore
}

export function getChartStore() {
    return chartStore
}

export function getErrorStore() {
    return errorStore
}

export function getGeneralStore() {
    return generalStore
}

export function getLayoutStore() {
    return layoutStore
}

export function getModalStore() {
    return modalStore
}

export function getMenuStore() {
    return menuStore
}

export function getPaginatorStore() {
    return paginatorStore
}

export function getPaymentStore() {
    return paymentStore
}

export function getSgnStore() {
    return sgnStore
}

export function getShopStore() {
    return shopStore
}

export function getTStore() {
    return tStore
}

export function getTabsStore() {
    return tabsStore
}

export function getUserStore() {
    return userStore
}
