import { computed, observable, action } from 'mobx'
import { getAlertStore, getMenuStore } from 'stores'
import { ADVANTAGE, EXCHANGE, PAMM } from 'services/constants'
import { logGroupMessage } from 'services/helpers'

import { ActiveMenuItemModel, ExchangeModel, GeneralModel } from 'models'

import {
    apiCall,
    EXCHANGES_URL,
    EXCHANGE_URL,
    EXCHANGE_CATEGORIES_URL,
    EXCHANGE_DESCS_URL,
    EXCHANGE_DESC_INCREMENT_URL,
    EXCHANGE_CREATE_REVIEW_AUTH_URL,
    EXCHANGE_CREATE_REVIEW_NOT_AUTH_URL
} from 'api'

class ExchangeStore {
    @observable all_exchanges = false
    @observable exchanges = []
    @observable exchangeItem = null
    @observable errors = null
    @observable categories = []
    @observable filterCategoryId = null
    @observable isAddReviewInProgress = false

    @computed
    get filteredExchanges() {
        if (!this.filterCategoryId) {
            return this.exchanges
        }
        return this.exchanges.filter(obj =>
            obj.categoryIds.includes(this.filterCategoryId)
        )
    }

    @computed
    get featuredExchanges() {
        return this.filteredExchanges.filter(obj => obj.isFeatured === true)
    }

    @computed
    get regularExchanges() {
        return this.filteredExchanges.filter(obj => obj.isFeatured === false)
    }

    @action
    clear() {
        this.errors = null
        this.isAddReviewInProgress = false
    }

    @action
    loadCategories = async () => {
        let result = await apiCall(EXCHANGE_CATEGORIES_URL, 'GET', {}, false)
        if (!result.isError) {
            let arr = []
            for (let obj of result.data) {
                arr.push(new GeneralModel(obj))
            }
            this.categories = arr
        }
    }

    @action
    setFilter(id) {
        this.filterCategoryId = id
    }

    @action
    loadExchanges = async () => {
        let url_call = EXCHANGES_URL
        if(!this.all_exchanges){
            url_call += '?limit=4'
        }
        let result = await apiCall(url_call, 'GET', {}, false)
        if (!result.isError) {
            logGroupMessage('loadExchanges стр 73 in ExchangeStore', result)
            let arr = []
            for (let obj of result.data.results) {
                arr.push(new ExchangeModel(obj))
            }
            this.exchanges = arr
        } else{
            console.log('loadExchanges error!!!')
        }
    }

    /**
     * type - is type of page. From this page request is made.
     * Choices: (exchange/pamm)
     */
    @action
    loadExchange = async (slug, type, onNotFound) => {
        this.exchangeItem = null

        const url = EXCHANGE_URL.replace('<slug>', slug)

        let result = await apiCall(url, 'GET', {}, false)
        if (!result.isError) {
            this.exchangeItem = new ExchangeModel(result.data)
            logGroupMessage('loadExchange стр.89 in ExchangeStore', result.data, this.exchangeItem)
            // Carefully: do not mutate otiginal menu item
            const mstore = getMenuStore()
            const menuItem = mstore.mapByUrl.get('/exchanges')

            const breadcrumbs = [...menuItem.breadcrumbs]
            if (type === EXCHANGE) {
                breadcrumbs.push({ name: this.exchangeItem.name, url: null })
            } else if (type === PAMM) {
                breadcrumbs.push({
                    name: this.exchangeItem.name,
                    url: this.exchangeItem.frontendUrl
                })
                breadcrumbs.push({ name: 'ПАММ счета', url: null })
            }

            const ami = new ActiveMenuItemModel(this.exchangeItem)
            ami.breadcrumbs = breadcrumbs
            ami.h1 = this.exchangeItem.h1
            ami.title = this.exchangeItem.title
            ami.content = this.exchangeItem.content

            if (type === PAMM) {
                ami.title = 'Памм счета. ' + ami.title
            }
            mstore.setActiveMenuItem(ami)
        } else {
            onNotFound()
        }
    }


    /**
     * Add advantage or disadvantage
     */
    @action
    addDesc = async (form, cb) => {
        const result = await apiCall(EXCHANGE_DESCS_URL, 'POST', form, true)
        logGroupMessage('addDesc стр 135 in ExchangeStore', result)

        if (!result.isError) {
            const store = getAlertStore()

            if (form.type === ADVANTAGE) {
                store.createAlert('Преимущество добавлено')
            } else {
                store.createAlert('Недостаток добавлен')
            }
            this.exchangeItem = new ExchangeModel(result.data)
            cb && cb()
        }
    }

    /**
     * Increment advantage or disadvantage
     */
    @action
    incrementDesc = async id => {
        const url = EXCHANGE_DESC_INCREMENT_URL.replace('<id>', id)
        const result = await apiCall(url, 'POST', {}, true)
        const store = getAlertStore()
        logGroupMessage('incrementDesc стр 153 in ExchangeStore', result)

        if (!result.isError) {
            store.createAlert('Ваш голос добавлен')
            this.exchangeItem = new ExchangeModel(result.data)
        } else {
            store.createAlert(result.data.errors[0], 'danger')
        }
    }

    /**
     * Add review by auth user
     */
    @action
    addReview = async (form, isAuth, cb) => {
        this.isAddReviewInProgress = true

        let url = ''
        if (isAuth) {
            url = EXCHANGE_CREATE_REVIEW_AUTH_URL
        } else {
            url = EXCHANGE_CREATE_REVIEW_NOT_AUTH_URL
        }

        let result = await apiCall(url, 'POST', form, isAuth)
        if (result.isError) {
            this.errors = result.data
        } else {
            const store = getAlertStore()
            store.createAlert('Отзыв добавлен')
            this.exchangeItem = new ExchangeModel(result.data)
            cb && cb()
        }
        this.isAddReviewInProgress = false
    }

    @action
    setFilter(id) {
        this.filterCategoryId = id
    }

    @action
    setAllExchanges(id) {
        this.all_exchanges = true
    }

}
const exchangeStore = new ExchangeStore()
export default exchangeStore
