import { observable, action } from 'mobx'
import { CountryModel, TimezoneModel, GeneralModel } from 'models'

import {
    apiCall,
    COUNTRIES_URL,
    CENTRIFUGO_TOKEN_URL,
    TIMEZONES_URL,
    BANNERS_URL,
} from 'api'

class GeneralStore {
    @observable banners = null
    @observable countries = null
    @observable centrifugoToken = null
    @observable timezones = null

    @action
    loadCountries = async () => {
        if (this.countries) {
            return
        }

        let result = await apiCall(COUNTRIES_URL, 'GET', {}, false)
        if (!result.isError) {
            let arr = []
            for (let obj of result.data) {
                arr.push(new CountryModel(obj))
            }
            this.countries = arr
        }
    }

    @action
    loadTimezones = async () => {
        if (this.timezones) {
            return
        }

        let result = await apiCall(TIMEZONES_URL, 'GET', {}, false)
        if (!result.isError) {
            let arr = []
            for (let obj of result.data) {
                arr.push(new TimezoneModel(obj))
            }
            this.timezones = arr
        }
    }

    @action
    getCentrifugoToken = async () => {
        if (this.centrifugoToken) {
            return
        }

        let result = await apiCall(CENTRIFUGO_TOKEN_URL, 'GET', {}, false)
        if (!result.isError) {
            this.centrifugoToken = result.data.token
        }
    }

    @action
    loadBanners = async () => {
        if (this.banners) {
            return
        }

        let result = await apiCall(BANNERS_URL, 'GET', {}, false)
        if (!result.isError) {

            let arr = []
            for (let obj of result.data) {
                arr.push(new GeneralModel(obj))
            }

            this.banners = arr
        }
    }

    @action
    getBanner(code){

        if(!this.banners){
            return null
        }
        let result = this.banners.filter( obj => obj.code === code)
        console.log('=========getBanner', this.banners, code, result[0])
        if(result.length > 0){
            return result[0]
        }
    }


}

const generalStore = new GeneralStore()
export default generalStore
