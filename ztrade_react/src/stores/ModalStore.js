import { observable, action } from 'mobx'

class ModalStore{
	@observable isModalOpened = false
    @observable isSignupChangeDataModalOpened = false
    @observable isBrokerAddReviewModalOpened = false
    @observable isExchangeAddReviewModalOpened = false
    @observable isBrokerWhichAccountModalOpened = false
    @observable isExchangeWhichAccountModalOpened = false
    @observable isProductBuyModalOpened = false


    // Global modal
    @observable isSupportModalOpened = false

	@action
	open(){
		this.isModalOpened = true
	}

	@action
	close(){
		this.isModalOpened = false
	}

    @action
    openBrokerAddReviewModal(){
        this.isModalOpened = true
        this.isBrokerAddReviewModalOpened = true
    }

    @action
    closeBrokerAddReviewModal(){
        this.isModalOpened = false
        this.isBrokerAddReviewModalOpened = false
    }

    @action
    openExchangeAddReviewModal(){
        this.isModalOpened = true
        this.isExchangeAddReviewModalOpened = true
    }

    @action
    closeExchangeAddReviewModal(){
        this.isModalOpened = false
        this.isExchangeAddReviewModalOpened = false
    }

    @action
    openBrokerWhichAccountModal(){
	    console.log('openBrokerWhichAccountModal')
        this.isModalOpened = true
        this.isBrokerWhichAccountModalOpened = true
    }

    @action
    openExchangeWhichAccountModal(){
        console.log('openBrokerWhichAccountModal')
        this.isModalOpened = true
        this.isExchangeWhichAccountModalOpened = true
    }

    @action
    closeBrokerWhichAccountModal(){
        this.isModalOpened = false
        this.isBrokerWhichAccountModalOpened = false
    }

    @action
    closeExchangeWhichAccountModal(){
        this.isModalOpened = false
        this.isExchangeWhichAccountModalOpened = false
    }

    @action
    openProductBuyModal(){
        this.isModalOpened = true
        this.isProductBuyModalOpened = true
    }

    @action
    closeProductBuyModal(){
        this.isModalOpened = false
        this.isProductBuyModalOpened = false
    }

    @action
    openSupportModal(){
        this.isSupportModalOpened = true
    }

    @action
    closeSupportModal(){
        this.isSupportModalOpened = false
    }

    @action
    openSignupChangeDataModal(){
        this.isModalOpened = true
        this.isSignupChangeDataModalOpened = true
    }

    @action
    closeSignupChangeDataModal(){
        this.isModalOpened = false
        this.isSignupChangeDataModalOpened = false
    }
}

const modalStore = new ModalStore()
export default modalStore