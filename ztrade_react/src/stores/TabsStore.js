import { observable, computed, action } from 'mobx'



class TabsStore{
    @observable activeIndex = 0
    // arr: array of object {name:..., className: ...}
    // className is not required
    @observable arr = []
    
    @computed
    get activeTab(){
        if (this.arr.length === 0){
            return null
        }
        let i = 0
        for (let t of this.arr){
            if (i === this.activeIndex){
                return t
            }
            i++
        }
        return null
    }

    @action
    clear = () => {
        this.activeIndex = 0
        this.arr = []
    }

    @action
    setTabs = (arr, activeIndex) => {
        this.arr = arr
        this.activeIndex = activeIndex
    }

    @action
    setActiveIndex = (index) => {
        this.activeIndex = index
    }
}

const tabsStore = new TabsStore()
export default tabsStore