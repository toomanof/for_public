import { computed, observable, action } from 'mobx'

import { GeneralModel, StrategyModel, SubscriptionModel } from 'models'
import { getSgnStore, getAlertStore, getTabsStore } from 'stores'
import { SIGNALS_NOTIFICATIONS_TAB_INDEX } from 'services/constants'

import {
    apiCall,
    ACCOUNT_FOREX_CATEGORIES_URL,
    ACCOUNT_FOREX_SYMBOLS_URL,
    ACCOUNT_FOREX_NEWS_URL,
    ACCOUNT_FOREX_NEWS_FILTER_URL,
    ACCOUNT_NOTIFICATIONS_URL,
    ACCOUNT_NOTIFICATIONS_MARK_ALL_URL,
    ACCOUNT_NOTIFICATIONS_NOT_READ_URL,
    ACCOUNT_STRATEGIES_URL,
    ACCOUNT_STRATEGY_URL,
    ACCOUNT_SUBSCRIPTIONS_URL,
    ACCOUNT_SUBSCRIPTION_SEND_INFO_URL,
    ACCOUNT_SUBSCRIPTION_SEND_ACTIVE_INFO_URL,
    ACCOUNT_TUTORIAL_BOOK_URL,
    ACCOUNT_USER_SETTINGS_URL,
    ACCOUNT_VIDEOS_URL,
    ACCOUNT_VIDEO_LESSONS_URL
} from 'api'
import {ACCOUNT_FREE_PERIOD_URL} from '../api';

class AccountStore {
    @observable activeStrategy = null
    @observable forexCategories = null
    @observable forexSymbols = null

    // work with CheckedItems component
    @observable forexNewsCategoryCheckedItems = null
    @observable forexNewsSymbolCheckedItems = null

    @observable forexNews = null
    @observable freshestForexNewsDt = null
    @observable forexNewsFilter = null
    @observable strategies = null
    @observable strategyItem = null
    @observable strategyFilterIsShowOnMobile = false
    @observable subscriptions = null
    @observable tutorialBook = null
    @observable timeFrames = null
    @observable indicators = null
    @observable timeFrameFilter = []
    @observable indicatorFilter = []
    @observable videos = null
    @observable videoLessons = null
    @observable videoTags = null
    @observable videoTagFilterId = null
    @observable userSettings = null

    @observable signalsNotifications = null
    @observable newsNotifications = null
    @observable signalsNotificationsNotReadCount = null
    @observable newsNotificationsNotReadCount = null
    @observable notificationsActiveTab = 'signals'

    @observable subscriptionSteps = new Map()
    @observable freePeriod = null

    @action
    setSubscriptionStep(subscriptionId, value) {
        this.subscriptionSteps.set(subscriptionId, value)
    }

    /**
     * Returns strategies based on time frame and indicator filters
     */
    @computed
    get filteredStrategies() {
        if (!this.strategies) {
            return []
        }

        if (!this.timeFrameFilter && !this.indicatorFilter) {
            return this.strategies
        }

        let arr = [...this.strategies]

        if (this.timeFrameFilter.length > 0) {
            for (let obj of arr) {
                const arr1 = [...obj.timeFrameIds]
                const arr2 = [...this.timeFrameFilter]
                const arr3 = arr1.filter(id => arr2.includes(id))

                if (arr3.length === 0) {
                    arr = arr.filter(x => x.id !== obj.id)
                }
            }
        }

        if (this.indicatorFilter.length > 0) {
            for (let obj of arr) {
                const arr1 = [...obj.indicatorIds]
                const arr2 = [...this.indicatorFilter]
                const arr3 = arr1.filter(id => arr2.includes(id))

                if (arr3.length === 0) {
                    arr = arr.filter(x => x.id !== obj.id)
                }
            }
        }

        return arr
    }

    /**
     * Returns videos based on video tag filter
     */
    @computed
    get filteredVideos() {
        if (!this.videos) {
            return []
        }

        if (!this.videoTagFilterId) {
            return this.videos
        }

        let arr = [...this.videos]

        for (let obj of arr) {
            const arr1 = [...obj.tagIds]

            if (!arr1.includes(this.videoTagFilterId)) {
                arr = arr.filter(x => x.id !== obj.id)
            }
        }

        return arr
    }

    /**
     * Returns user's paid subscriptions
     */
    @computed
    get paidSubscriptions() {
        if (!this.subscriptions) {
            return []
        }
        return this.subscriptions.filter(obj => obj.isPaid === true)
    }

    /**
     * Currently not used.
     * Ready checked items are provided from API to forexNewsFilter
     */
    @action
    loadForexCategories = async () => {
        if (this.forexCategories) {
            return
        }
        let result = await apiCall(
            ACCOUNT_FOREX_CATEGORIES_URL,
            'GET',
            {},
            true
        )
        if (!result.isError) {
            const arr = []
            for (let obj of result.data) {
                arr.push(new GeneralModel(obj))
            }
            this.forexCategories = arr
        }
    }

    /**
     * Currently not used.
     * Ready checked items are provided from API to forexNewsFilter
     */
    @action
    loadForexSymbols = async () => {
        if (this.forexSymbols) {
            return
        }
        let result = await apiCall(ACCOUNT_FOREX_SYMBOLS_URL, 'GET', {}, true)
        if (!result.isError) {
            const arr = []
            for (let obj of result.data) {
                arr.push(new GeneralModel(obj))
            }
            this.forexSymbols = arr
        }
    }

    @action
    loadForexNews = async (isCompare = false) => {
        let result = await apiCall(ACCOUNT_FOREX_NEWS_URL, 'GET', {}, true)
        if (!result.isError) {
            const arr = []
            for (let obj of result.data) {
                arr.push(new GeneralModel(obj))
            }

            // Compare with all forex news and if new news available
            // and if user has "isSound" in settings - play audio alert
            if (isCompare) {
                const dt = this.freshestForexNewsDt

                if (this.forexNewsFilter && this.forexNewsFilter.isSound) {
                    if (dt && arr.length > 0) {
                        if (arr[0].dt > this.freshestForexNewsDt) {
                            this.soundAlertForexNews()
                        }
                    }
                }
            }

            this.forexNews = arr
            this.freshestForexNewsDt = arr.length > 0 ? arr[0].dt : null
        }
    }

    @action
    loadForexNewsFilter = async () => {
        let result = await apiCall(
            ACCOUNT_FOREX_NEWS_FILTER_URL,
            'GET',
            {},
            true
        )
        if (!result.isError) {
            this.processForexNewsFilter(result.data)
        }
    }

    /**
     * Filter already updated locally.
     * Update it on server and reupdate again locally
     */
    @action
    updateForexNewsFilter = async form => {
        let result = await apiCall(
            ACCOUNT_FOREX_NEWS_FILTER_URL,
            'PATCH',
            form,
            true
        )
        if (!result.isError) {
            this.processForexNewsFilter(result.data)

            // Update news
            this.loadForexNews()
        }
    }

    /**
     * Activate filter from data received from server
     */
    @action
    processForexNewsFilter(data) {
        let arr1 = data.forex_news_category_checked_items
        let arr2 = data.forex_news_symbol_checked_items

        delete data.forex_news_category_checked_items
        delete data.forex_news_symbol_checked_items

        this.setForexNewsCategoryCheckedItems(arr1)
        this.setForexNewsSymbolCheckedItems(arr2)

        this.forexNewsFilter = new GeneralModel(data)
    }

    @action
    loadStrategies = async () => {
        if (this.strategies) {
            return
        }
        let result = await apiCall(ACCOUNT_STRATEGIES_URL, 'GET', {}, true)
        if (!result.isError) {
            const arr1 = []
            for (let obj of result.data.time_frames) {
                arr1.push(new GeneralModel(obj))
            }
            this.timeFrames = arr1

            const arr2 = []
            for (let obj of result.data.indicators) {
                arr2.push(new GeneralModel(obj))
            }
            this.indicators = arr2

            const arr3 = []
            for (let obj of result.data.strategies) {
                arr3.push(new StrategyModel(obj))
            }
            this.strategies = arr3

            if (!this.activeStrategy && this.strategies.length > 0) {
                this.activeStrategy = this.strategies[0]
            }
        }
    }

    @action
    loadStrategy = async id => {
        this.strategyItem = null

        let result = await apiCall(
            ACCOUNT_STRATEGY_URL.replace('<id>', id),
            'GET',
            {},
            true
        )
        if (!result.isError) {
            this.strategyItem = new GeneralModel(result.data)
        }
    }

    @action
    loadSubscriptions = async () => {
        let result = await apiCall(ACCOUNT_SUBSCRIPTIONS_URL, 'GET', {}, true)
        if (!result.isError) {
            const arr = []
            for (let obj of result.data) {
                arr.push(new SubscriptionModel(obj))
            }
            this.subscriptions = arr
        }
    }

    @action
    subscriptionSendInfoEmail = async id => {
        const url = ACCOUNT_SUBSCRIPTION_SEND_INFO_URL.replace('<id>', id)
        await apiCall(url, 'POST', {}, true)
    }

    @action
    subscriptionSendActiveInfoEmail = async (idAccount, tradeId, dateFrom, dateTo) => {
        const url = ACCOUNT_SUBSCRIPTION_SEND_ACTIVE_INFO_URL
        console.log('setSubscriptionActiveInfo', idAccount, tradeId, dateFrom, dateTo)
        await apiCall(url, 'POST', {
            idAccount: idAccount,
            tradeId: tradeId,
            dateFrom: dateFrom,
            dateTo: dateTo

        }, true)
    }

    @action
    loadTutorialBook = async () => {
        if (this.tutorialBook) {
            return
        }
        let result = await apiCall(ACCOUNT_TUTORIAL_BOOK_URL, 'GET', {}, true)
        if (!result.isError) {
            this.tutorialBook = new GeneralModel(result.data)
        }
    }

    @action
    loadVideos = async () => {
        if (this.videos) {
            return
        }
        let result = await apiCall(ACCOUNT_VIDEOS_URL, 'GET', {}, true)
        if (!result.isError) {
            const arr1 = []
            for (let obj of result.data.video_tags) {
                arr1.push(new GeneralModel(obj))
            }
            this.videoTags = arr1

            const arr2 = []
            for (let obj of result.data.videos) {
                arr2.push(new GeneralModel(obj))
            }
            this.videos = arr2
        }
    }

    @action
    loadVideoLessons = async () => {
        if (this.videoLessons) {
            return
        }
        let result = await apiCall(ACCOUNT_VIDEO_LESSONS_URL, 'GET', {}, true)
        if (!result.isError) {
            const arr = []
            for (let obj of result.data) {
                arr.push(new GeneralModel(obj))
            }
            this.videoLessons = arr
        }
    }

    @action
    loadUserSettings = async () => {
        if (this.userSettings) {
            return
        }
        let result = await apiCall(ACCOUNT_USER_SETTINGS_URL, 'GET', {}, true)
        if (!result.isError) {
            this.userSettings = new GeneralModel(result.data)
        }
    }

    @action
    updateUserSettings = async form => {
        let result = await apiCall(
            ACCOUNT_USER_SETTINGS_URL,
            'PATCH',
            form,
            true
        )
        if (!result.isError) {
            this.userSettings = new GeneralModel(result.data)
        }
    }

    @action
    setActiveStrategy = obj => {
        this.activeStrategy = obj
    }

    /**
     * Algo.
     * Initially get from server and set to state.
     *
     * After local update, set updated items to state
     * to show them immediately, then send updated items
     * to server, and then update them again with data
     * received from server.
     */
    @action
    setForexNewsCategoryCheckedItems = items => {
        this.forexNewsCategoryCheckedItems = items
    }

    /**
     * Algo.
     * Initially get from server and set to state.
     *
     * After local update, set updated items to state
     * to show them immediately, then send updated items
     * to server, and then update them again with data
     * received from server.
     */
    @action
    setForexNewsSymbolCheckedItems = items => {
        this.forexNewsSymbolCheckedItems = items
    }

    @action
    soundAlertForexNews() {
        const audio = new Audio('/files/sounds/forex_news.mp3')
        audio.play()
    }

    @action
    addIndicatorFilter(id) {
        const arr = [...this.indicatorFilter]

        if (arr.includes(id)) {
            this.indicatorFilter = arr.filter(x => x !== id)
        } else {
            arr.push(id)
            this.indicatorFilter = arr
        }
    }

    @action
    addTimeFrameFilter(id) {
        const arr = [...this.timeFrameFilter]

        if (this.timeFrameFilter.includes(id)) {
            this.timeFrameFilter = arr.filter(x => x !== id)
        } else {
            arr.push(id)
            this.timeFrameFilter = arr
        }
    }

    @action
    setVideoTagFilterId(id) {
        if (!id) {
            this.videoTagFilterId = null
        } else {
            this.videoTagFilterId = id
        }
    }

    @action
    toggleStrategyFilterShowOnMobile() {
        this.strategyFilterIsShowOnMobile = !this.strategyFilterIsShowOnMobile
    }

    getNotificationsUrl(url, type) {
        const store = getSgnStore()
        return `${url}?type=${type}&market_id=${store.activeMarket.id}`
    }

    @action
    loadMarketNotifications() {
        this.signalsNotifications = null
        this.newsNotifications = null
        this.signalsNotificationsNotReadCount = null
        this.newsNotificationsNotReadCount = null

        this.loadSignalsNotifications()
        this.loadNewsNotifications()
    }

    @action
    loadSignalsNotifications = async () => {
        const store = getSgnStore()

        let url = this.getNotificationsUrl(ACCOUNT_NOTIFICATIONS_URL, 'signals')
        let result = await apiCall(url, 'GET', {}, true)
        if (!result.isError) {
            const arr = []
            for (let obj of result.data) {
                if (obj.contract) {
                    obj.contract = new GeneralModel(obj.contract)
                }
                arr.push(new GeneralModel(obj))
            }
            this.signalsNotifications = arr
            this.loadSignalsNotificationsNotReadCount()
        }
    }

    @action
    loadNewsNotifications = async () => {
        const store = getSgnStore()

        let url = this.getNotificationsUrl(ACCOUNT_NOTIFICATIONS_URL, 'news')
        let result = await apiCall(url, 'GET', {}, true)
        if (!result.isError) {
            const arr = []
            for (let obj of result.data) {
                arr.push(new GeneralModel(obj))
            }
            this.newsNotifications = arr
            this.loadNewsNotificationsNotReadCount()
        }
    }

    @action
    loadSignalsNotificationsNotReadCount = async () => {
        const store = getSgnStore()

        let url = this.getNotificationsUrl(
            ACCOUNT_NOTIFICATIONS_NOT_READ_URL,
            'signals'
        )
        let result = await apiCall(url, 'GET', {}, true)
        if (!result.isError) {
            this.signalsNotificationsNotReadCount = result.data.count
        }
    }

    @action
    loadNewsNotificationsNotReadCount = async () => {
        const store = getSgnStore()

        let url = this.getNotificationsUrl(
            ACCOUNT_NOTIFICATIONS_NOT_READ_URL,
            'news'
        )
        let result = await apiCall(url, 'GET', {}, true)
        if (!result.isError) {
            this.newsNotificationsNotReadCount = result.data.count
        }
    }

    @action
    markAllNotificationsAsRead = async () => {
        const tabsStore = getTabsStore()
        if (tabsStore.activeIndex !== SIGNALS_NOTIFICATIONS_TAB_INDEX) {
            return
        }

        const type = this.notificationsActiveTab
        let url = this.getNotificationsUrl(
            ACCOUNT_NOTIFICATIONS_MARK_ALL_URL,
            type
        )

        if (
            (type === 'signals' &&
                this.signalsNotificationsNotReadCount === 0) ||
            (type === 'news' && this.newsNotificationsNotReadCount === 0)
        ) {
            return
        }

        const result = await apiCall(url, 'POST', {}, true)
        if (!result.isError && type === 'signals') {
            this.signalsNotificationsNotReadCount = 0
            let signalsNotifications = Array.from(
                this.signalsNotifications.values()
            ).map(obj => {
                obj.isRead = true
                return obj
            })
            this.signalsNotifications = signalsNotifications
        }

        if (!result.isError && type === 'news') {
            this.newsNotificationsNotReadCount = 0
            let newsNotifications = Array.from(
                this.newsNotifications.values()
            ).map(obj => {
                obj.isRead = true
                return obj
            })
            this.newsNotifications = newsNotifications
        }
    }

    @action
    setNotificationsActiveTab(type) {
        this.notificationsActiveTab = type
    }

    @action
    updateMarketNotifications(data) {
        const sgnStore = getSgnStore()
        data = new GeneralModel(data)
        if (
            sgnStore.activeMarket &&
            sgnStore.activeMarket.id === data.marketId
        ) {
            if (data.notificationType === 'signals')
                this.loadSignalsNotifications()
            if (data.notificationType === 'news') this.loadNewsNotifications()
        }
    }

    @action
    loadFreePeriod = async () => {
        if (this.freePeriod) {
            return
        }
        let result = await apiCall(ACCOUNT_FREE_PERIOD_URL, 'GET', {}, true)
        if (!result.isError) {
            this.freePeriod = new GeneralModel(result.data)
        }
    }

    @action
    subscriptionFreePeriod = async () => {
        const url = ACCOUNT_FREE_PERIOD_URL
        await apiCall(url, 'POST', {}, true)
    }
}

const accountStore = new AccountStore()
export default accountStore
