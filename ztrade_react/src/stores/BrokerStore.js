import { computed, observable, action } from 'mobx'
import { getAlertStore, getMenuStore } from 'stores'
import { ADVANTAGE, BROKER, PAMM } from 'services/constants'

import { ActiveMenuItemModel, BrokerModel, GeneralModel } from 'models'

import {
    apiCall,
    BROKER_CATEGORIES_URL,
    BROKERS_URL,
    BROKER_URL,
    BROKER_DESCS_URL,
    BROKER_DESC_INCREMENT_URL,
    BROKER_CREATE_REVIEW_AUTH_URL,
    BROKER_CREATE_REVIEW_NOT_AUTH_URL
} from 'api'

class BrokerStore {
    @observable all_brokers = false
    @observable brokers = null
    @observable brokerItem = null
    @observable categories = null
    @observable errors = null
    @observable filterCategoryId = null
    @observable isAddReviewInProgress = false

    @computed
    get filteredBrokers() {
        if (!this.filterCategoryId) {
            return this.brokers
        }
        return this.brokers.filter(obj =>
            obj.categoryIds.includes(this.filterCategoryId)
        )
    }

    @computed
    get regularBrokers() {
        return this.filteredBrokers.filter(obj => obj.isFeatured === false)
    }

    @computed
    get featuredBrokers() {
        return this.filteredBrokers.filter(obj => obj.isFeatured === true)
    }

    @action
    clear() {
        this.errors = null
        this.isAddReviewInProgress = false
    }

    @action
    loadCategories = async () => {
        if (this.categories) {
            return
        }

        let result = await apiCall(BROKER_CATEGORIES_URL, 'GET', {}, false)
        if (!result.isError) {
            let arr = []
            for (let obj of result.data) {
                arr.push(new GeneralModel(obj))
            }
            this.categories = arr
        }
    }

    @action
    loadBrokers = async () => {
        let url_call = BROKERS_URL
        if(!this.all_brokers){
            url_call += '?limit=4'
        }
        let result = await apiCall(url_call, 'GET', {}, false)
        if (!result.isError) {
            let brokers = []
            if(result.data.results){
                brokers = result.data.results
            } else {
                brokers = result.data
            }
            let arr = []
            for (let obj of brokers) {
                arr.push(new BrokerModel(obj))
            }
            this.brokers = arr
            console.log('loadBrokers==================', this.brokers)
        }
    }

    /**
     * type - is type of page. From this page request is made.
     * Choices: (broker/pamm)
     */
    @action
    loadBroker = async (slug, type, onNotFound) => {
        this.brokerItem = null

        const url = BROKER_URL.replace('<slug>', slug)

        let result = await apiCall(url, 'GET', {}, false)
        if (!result.isError) {
            this.brokerItem = new BrokerModel(result.data)

            // Carefully: do not mutate otiginal menu item
            const mstore = getMenuStore()
            const menuItem = mstore.mapByUrl.get('/brokers')

            const breadcrumbs = [...menuItem.breadcrumbs]
            if (type === BROKER) {
                breadcrumbs.push({ name: this.brokerItem.name, url: null })
            } else if (type === PAMM) {
                breadcrumbs.push({
                    name: this.brokerItem.name,
                    url: this.brokerItem.frontendUrl
                })
                breadcrumbs.push({ name: 'ПАММ счета', url: null })
            }

            const ami = new ActiveMenuItemModel(this.brokerItem)
            ami.breadcrumbs = breadcrumbs
            ami.h1 = this.brokerItem.h1
            ami.title = this.brokerItem.title
            ami.content = this.brokerItem.content

            if (type === PAMM) {
                ami.title = 'Памм счета. ' + ami.title
            }
            mstore.setActiveMenuItem(ami)
        } else {
            onNotFound()
        }
    }

    /**
     * Add advantage or disadvantage
     */
    @action
    addDesc = async (form, cb) => {
        const result = await apiCall(BROKER_DESCS_URL, 'POST', form, true)
        console.log(result)
        if (!result.isError) {
            const store = getAlertStore()

            if (form.type === ADVANTAGE) {
                store.createAlert('Преимущество добавлено')
            } else {
                store.createAlert('Недостаток добавлен')
            }
            this.brokerItem = new BrokerModel(result.data)
            cb && cb()
        }
    }

    /**
     * Increment advantage or disadvantage
     */
    @action
    incrementDesc = async id => {
        const url = BROKER_DESC_INCREMENT_URL.replace('<id>', id)
        const result = await apiCall(url, 'POST', {}, true)
        const store = getAlertStore()
        if (!result.isError) {
            store.createAlert('Ваш голос добавлен')
            this.brokerItem = new BrokerModel(result.data)
        } else {
            store.createAlert(result.data.errors[0], 'danger')
        }
    }

    /**
     * Add review by auth user
     */
    @action
    addReview = async (form, isAuth, cb) => {
        this.isAddReviewInProgress = true

        let url = ''
        if (isAuth) {
            url = BROKER_CREATE_REVIEW_AUTH_URL
        } else {
            url = BROKER_CREATE_REVIEW_NOT_AUTH_URL
        }

        let result = await apiCall(url, 'POST', form, isAuth)
        if (result.isError) {
            this.errors = result.data
        } else {
            const store = getAlertStore()
            store.createAlert('Отзыв добавлен')
            this.brokerItem = new BrokerModel(result.data)
            cb && cb()
        }
        this.isAddReviewInProgress = false
    }

    @action
    setFilter(id) {
        this.filterCategoryId = id
    }

    @action
    setAllBrokers(id) {
        this.all_brokers = true
    }
}

const brokerStore = new BrokerStore()
export default brokerStore

// Carefull: original menuItem, should not be mutated
// Make deep clone

// const mi = JSON.parse(JSON.stringify(menuItem))

// const ami = new ActiveMenuItemModel(mi)

// ami.breadcrumbs.push({name: this.brokerItem.name, url: null})
// ami.h1 = this.brokerItem.h1
// ami.title = this.brokerItem.title
// ami.content = this.brokerItem.content

// mstore.setActiveMenuItem(ami)
