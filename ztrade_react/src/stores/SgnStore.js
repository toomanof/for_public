import { computed, observable, action } from 'mobx'
import { getAlertStore, getPaginatorStore, getAccountStore } from 'stores'

import { GeneralModel, MarketModel, SignalModel } from 'models'

import {
    apiCall,
    MARKETS_URL,
    SYMBOLS_URL,
    SIGNAL_TYPES_URL,
    SIGNALS_URL,
    SIGNAL_URL,
    SIGNAL_FILTERS_URL,
    SIGNAL_FILTER_URL,
    TARIFFS_URL
} from 'api'

class SgnStore {
    @observable activeTradingChartCode = 'EURUSD'
    @observable activeMarketCode = null
    @observable isSignalFilterOpened = false

    // key - marketCode
    // value - Market object
    @observable marketMap = null

    // key - marketCode
    // value - array of Tariff object
    @observable tariffMap = null

    @observable signalTypes = null

    // key - marketCode
    // value - array of Signal object
    @observable signalMap = null

    // key - marketCode
    // value - SignalFilter object
    @observable signalFilterMap = null

    // key - marketCode
    // value - checkedItems
    @observable symbolCheckedItemsMap = null

    @observable symbolsAll = null
    @observable highlightedSignals = new Map()

    @action
    clear() {
        this.activeTradingChartCode = 'EURUSD'
        this.activeMarketCode = null
        this.isSignalFilterOpened = false
    }

    @computed
    get activeMarket() {
        if (!this.activeMarketCode || !this.marketMap) {
            return null
        }
        return this.marketMap.get(this.activeMarketCode)
    }

    @action
    activateTradingChartCode(value) {
        this.activeTradingChartCode = value
    }

    /**
     * Returns array of signals for active market
     */
    @computed
    get signals() {
        if (!this.activeMarketCode || !this.signalMap) {
            return null
        }

        const arr = this.signalMap.get(this.activeMarketCode)
        if (!arr) {
            return null
        }

        return arr.filter(s => s.isVisible === true)
    }

    /**
     * Returns array of symbols for active market
     */
    @computed
    get symbols() {
        if (!this.activeMarketCode || !this.symbolsAll) {
            return null
        }

        return this.symbolsAll.filter(
            s => s.marketCode === this.activeMarketCode
        )
    }

    /**
     * Returns signalFilter for active market
     */
    @computed
    get signalFilter() {
        if (!this.activeMarketCode || !this.signalFilterMap) {
            return null
        }

        const obj = this.signalFilterMap.get(this.activeMarketCode)
        if (!obj) {
            return null
        }

        return obj
    }

    /**
     * Returns checkedItems for active market
     */
    @computed
    get symbolCheckedItems() {
        if (!this.activeMarketCode || !this.symbolCheckedItemsMap) {
            return null
        }

        const items = this.symbolCheckedItemsMap.get(this.activeMarketCode)
        if (!items) {
            return null
        }

        return items
    }

    @action
    loadMarkets = async () => {
        if (!this.marketMap) {
            this.marketMap = new Map()
        }

        let result = await apiCall(MARKETS_URL, 'GET', {}, true)
        if (!result.isError) {
            const map = new Map()
            for (let obj of result.data) {
                const mobj = new MarketModel(obj)
                map.set(mobj.code, mobj)
            }
            this.marketMap = map
        }
    }

    // Load markets with additional data
    @action
    loadMarketsExt = async () => {
        // If markets and signals already in state
        // Do not load markets, and just set activeMarketCode
        if (this.marketMap) {
            this.setInitialActiveMarketCode(this.marketMap)
            this.paginate()
            return
        }

        let result = await apiCall(MARKETS_URL, 'GET', {}, true)
        if (!result.isError) {
            const map = new Map()
            for (let obj of result.data) {
                const mobj = new MarketModel(obj)
                map.set(mobj.code, mobj)
            }
            this.setInitialActiveMarketCode(map)
            this.marketMap = map
        }
    }

    @action
    setInitialActiveMarketCode = async map => {
        // Initial loading
        // 1) Set active market
        // 2) Load signals for this market
        // 3) Load signals for all markets in background
        if (!this.activeMarketCode) {
            this.activeMarketCode = Array.from(map.values())[0].code

            // Load signals for active market
            await this.loadSignals(this.activeMarketCode)

            // Load signals for all markets
            for (let code of Array.from(this.marketMap.keys())) {
                this.loadSignals(code)
            }
        }
    }

    @action
    loadSymbols = async () => {
        if (this.symbolsAll) {
            return
        }
        let result = await apiCall(SYMBOLS_URL, 'GET', {}, true)
        if (!result.isError) {
            const arr = []
            for (let obj of result.data) {
                arr.push(new GeneralModel(obj))
            }
            this.symbolsAll = arr
        }
    }

    @action
    loadSignalTypes = async () => {
        if (this.signalTypes) {
            return
        }
        let result = await apiCall(SIGNAL_TYPES_URL, 'GET', {}, true)
        if (!result.isError) {
            const arr = []
            for (let obj of result.data) {
                arr.push(new GeneralModel(obj))
            }
            this.signalTypes = arr
        }
    }

    /**
     * Get signals for particular market
     */
    @action
    loadSignals = async marketCode => {
        if (!this.signalMap) {
            this.signalMap = new Map()
        }

        let result = await apiCall(
            SIGNALS_URL.replace('<code>', marketCode),
            'GET',
            {},
            true
        )
        if (!result.isError) {
            const arr = []
            for (let obj of result.data) {
                const signal = new SignalModel(obj)
                arr.push(signal)
            }

            this.signalMap.set(marketCode, arr)
            this.paginate()
        }
    }

    /**
     * Get signal by id
     * Used in web socket messages
     */
    @action
    getSignal = async (id, cb) => {
        let result = await apiCall(
            SIGNAL_URL.replace('<id>', id),
            'GET',
            {},
            true
        )

        if (!result.isError) {
            const signal = new SignalModel(result.data)
            if (cb) {
                cb(signal)
            }
        }
    }

    /**
     * Get user's signal filters for all market
     * put them to map, where key is marketCode and value filter object
     */
    @action
    loadSignalFilters = async onSuccess => {
        let result = await apiCall(SIGNAL_FILTERS_URL, 'GET', {}, true)
        if (!result.isError) {
            for (let obj of result.data) {
                this.processSignalFilter(obj.market_code, obj)
            }
        }
    }

    /**
     * Update user's signal filter for particular market
     */
    @action
    updateSignalFilter = async (marketCode, form, onSuccess) => {
        let result = await apiCall(
            SIGNAL_FILTER_URL.replace('<code>', marketCode),
            'PATCH',
            form,
            true
        )
        if (!result.isError) {
            this.processSignalFilter(marketCode, result.data)
            onSuccess && onSuccess()

            // Reload signals
            this.loadSignals(marketCode)
        }
    }

    /**
     * Activate filter for particular market
     * from data received from server
     */
    @action
    processSignalFilter(marketCode, data) {
        if (!this.signalFilterMap) {
            this.signalFilterMap = new Map()
        }

        let arr = data.symbol_checked_items
        delete data.symbol_checked_items

        this.setSymbolCheckedItems(marketCode, arr)
        this.signalFilterMap.set(marketCode, new GeneralModel(data))
    }

    @action
    loadTariffs = async marketCode => {
        if (!this.tariffMap) {
            this.tariffMap = new Map()
        }

        const url = TARIFFS_URL.replace('<code>', marketCode)

        let result = await apiCall(url, 'GET', {}, true)
        if (!result.isError) {
            const arr = []
            for (let obj of result.data) {
                arr.push(new GeneralModel(obj))
            }
            this.tariffMap.set(marketCode, arr)
        }
    }

    // Call from notifications page
    @action
    setHighlightedSignal = id => {
        this.highlightedSignals.set(this.activeMarketCode, id)
        setTimeout(() => {
            for (const key of Array.from(this.marketMap.keys())) {
                this.highlightedSignals.delete(key)
            }
        }, 15000)
    }

    /**
     * Hide Signal for user.
     */
    @action
    deleteSignal = async s => {
        const url = SIGNAL_URL.replace('<id>', s.id)
        let result = await apiCall(url, 'DELETE', {}, true)
        if (!result.isError) {
            this.loadSignals(s.marketCode)
        }
    }

    @action
    setActiveMarketCode = code => {
        const accountStore = getAccountStore()
        this.activeMarketCode = code
        this.loadSignals(code)
        accountStore.loadMarketNotifications()
    }

    @action
    openSignalFilter = () => {
        this.isSignalFilterOpened = true
    }

    @action
    closeSignalFilter = () => {
        this.isSignalFilterOpened = false
    }

    @action
    toggleSignalFilter = () => {
        this.isSignalFilterOpened = !this.isSignalFilterOpened
    }

    /**
     * Init paginator for active market
     */
    @action
    paginate() {
        const store = getPaginatorStore()
        store.initPaginator(this.signals, 1, 5)
    }

    /**
     * Algo.
     * Initially get from server and set to state.
     *
     * After local update, set updated items to state
     * to show them immediately, then send updated items
     * to server, and then update them again with data
     * received from server.
     */
    @action
    setSymbolCheckedItems = (marketCode, items) => {
        if (!this.symbolCheckedItemsMap) {
            this.symbolCheckedItemsMap = new Map()
        }
        this.symbolCheckedItemsMap.set(marketCode, items)
    }

    /**
     * Websocket alert
     */
    @action
    wsSignalCreated = async data => {
        this.getSignal(data.signal_id, async signal => {
            await this.loadSignals(signal.marketCode)

            if (signal.isNotify) {
                const store = getAlertStore()
                store.createAlert(data.alert_text)

/*                if (signal.isSound) { */
                const audio = new Audio('/files/sounds/signal_created.mp3')
                audio.play()
/*                } */
            }
        })
    }

    /**
     * Websocket alert
     */
    @action
    wsSignalStopLossUpdated = async data => {
        this.getSignal(data.signal_id, async signal => {
            await this.loadSignals(signal.marketCode)

            if (signal.isNotify) {
                const store = getAlertStore()
                store.createAlert(data.alert_text)

                if (signal.isSound) {
                    const audio = new Audio(
                        '/files/sounds/signal_stop_loss_updated.mp3'
                    )
                    audio.play()
                }
            }
        })
    }

    /**
     * Websocket alert
     */
    @action
    wsSignalTakeProfitUpdated = async data => {
        this.getSignal(data.signal_id, async signal => {
            await this.loadSignals(signal.marketCode)

            if (signal.isNotify) {
                const store = getAlertStore()
                store.createAlert(data.alert_text)

                if (signal.isSound) {
                    const audio = new Audio(
                        '/files/sounds/signal_take_profit_updated.mp3'
                    )
                    audio.play()
                }
            }
        })
    }

    /**
     * Websocket alert
     */
    @action
    wsSignalClosed = async data => {
        this.getSignal(data.signal_id, async signal => {
            await this.loadSignals(signal.marketCode)

            if (signal.isNotify) {
                const store = getAlertStore()
                store.createAlert(data.alert_text)

                if (signal.isSound) {
                    const audio = new Audio('/files/sounds/signal_closed.mp3')
                    audio.play()
                }
            }
        })
    }
}

const sgnStore = new SgnStore()
export default sgnStore
