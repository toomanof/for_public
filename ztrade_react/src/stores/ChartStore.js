import { observable, action, computed } from 'mobx'
import { getSgnStore } from 'stores'

import { apiCall, CHART01_URL, CHART02_URL, CHART03_URL } from 'api'

class ChartStore {
    // key: market code
    // value: array of symbol codes
    @observable sMap = new Map()

    // key: market code
    // value: chart period: month | week
    @observable pMap = new Map()

    // key: market code
    // value: chart01 data
    @observable c01Map = new Map()

    // key: market code
    // value: chart02 data
    @observable c02Map = new Map()

    // key: market code
    // value: chart03 data
    @observable c03Map = new Map()

    @computed
    get activeSymbols() {
        const store = getSgnStore()
        if (!store.activeMarket) {
            return []
        }
        return this.sMap.get(store.activeMarket.code) || []
    }

    @computed
    get activePeriod() {
        const store = getSgnStore()
        if (!store.activeMarket) {
            return 'month'
        }
        return this.pMap.get(store.activeMarket.code) || 'month'
    }

    @computed
    get chart01Data() {
        const store = getSgnStore()
        let value = this.c01Map.get(store.activeMarket.code) || null
        if (value !== null) {
            return value
        }
        this.loadChart01()
        return null
    }

    @computed
    get chart02Data() {
        const store = getSgnStore()
        let value = this.c02Map.get(store.activeMarket.code) || null
        if (value !== null) {
            return value
        }
        this.loadChart02()
        return null
    }

    @computed
    get chart03Data() {
        const store = getSgnStore()
        let value = this.c03Map.get(store.activeMarket.code) || null
        if (value !== null) {
            return value
        }
        this.loadChart03()
        return null
    }

    @action setActiveSymbols(values) {
        const store = getSgnStore()
        if (store.activeMarket) {
            this.sMap.set(store.activeMarket.code, values)
        }
    }

    @action setActivePeriod(value) {
        const store = getSgnStore()
        if (store.activeMarket) {
            this.pMap.set(store.activeMarket.code, value)
        }
    }

    @action
    loadChart01 = async () => {
        const store = getSgnStore()
        let url = CHART01_URL.replace('<code>', store.activeMarket.code)
        url += `?period=${this.activePeriod}`

        if (this.activeSymbols.length > 0) {
            url += `&symbol_codes=${this.activeSymbols.join(',')}`
        }

        let result = await apiCall(url, 'GET', {}, true)
        if (!result.isError) {
            this.c01Map.set(store.activeMarket.code, result.data.data)
        }
    }

    @action
    loadChart02 = async () => {
        const store = getSgnStore()
        let url = CHART02_URL.replace('<code>', store.activeMarket.code)

        if (this.activeSymbols.length > 0) {
            url += `?symbol_codes=${this.activeSymbols.join(',')}`
        }

        let result = await apiCall(url, 'GET', {}, true)
        if (!result.isError) {
            this.c02Map.set(store.activeMarket.code, result.data.data)
        }
    }

    @action
    loadChart03 = async () => {
        const store = getSgnStore()
        let url = CHART03_URL.replace('<code>', store.activeMarket.code)

        if (this.activeSymbols.length > 0) {
            url += `?symbol_codes=${this.activeSymbols.join(',')}`
        }

        let result = await apiCall(url, 'GET', {}, true)
        if (!result.isError) {
            this.c03Map.set(store.activeMarket.code, result.data.data)
        }
    }
}

const chartStore = new ChartStore()
export default chartStore
