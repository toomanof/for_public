import { observable, action } from 'mobx'

class LayoutStore{
	@observable isSidebarOpened = false

	@action
	toggleSidebar(){
		if(this.isSidebarOpened){
			document.body.classList.remove('mini-navbar')
        	this.isSidebarOpened = false
        	return
		}
		document.body.classList.add('mini-navbar')
        this.isSidebarOpened = true
	}
}

const layoutStore = new LayoutStore();
export default layoutStore