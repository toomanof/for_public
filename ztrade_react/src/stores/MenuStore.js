import { observable, action, computed } from 'mobx'
import md5 from 'md5'
import { getPaginatorStore, getUserStore } from 'stores'
import { logGroupMessage } from 'services/helpers'
import {
    ActiveMenuItemModel,
    ContentModel,
    MenuModel,
    GeneralModel
} from 'models'


import{
    apiCall,
    ARTICLES_URL,
    CONTENT_URL,
    HOME_SLIDER_URL,
    MAIN_HOME_SLIDER_URL,
    HOME_WHY_URL,
    HOME_BENEFITS_URL,
    MENU_URL,
    MENU_RIGHT_URL,
    MENU_RIGHT_LINK_URL,
    MENU_FOOTER_URL,
    MENU_TOP_URL,
    MENU_CONTENTS_URL,
    PAGE_ANALYTIC_URL,
    PAGE_TOOL_URL,
    RECOMMEND_MAIN_URL,
    RECOMMEND_LENTA_URL,
    RECOMMEND_SIGNAL_URL,
    SIGNAL_SLIDER_URL,
    FOOTER_LINK_URL,
    TOP_HEADER_SLIDER_URL,
    TOOLS_FIRST_SLIDER_URL,
    TOOLS_SECOND_SLIDER_URL,
} from 'api'


class MenuStore{
    @observable articles = null
    @observable ami = null
    @observable map = null
    @observable mapByUrl = null
    @observable menuRight = null
    @observable menuRightLink = null
    @observable menuFooter = null
    @observable menuTop = null
    @observable contentItems = null
    @observable homeSlider = null
    @observable topHeaderSlider = null
    @observable mainHomeSlider = null
    @observable toolsFirstSlider = null
    @observable toolsSecondSlider = null
    @observable homeWhy = null
    @observable homeBenefits = null
    @observable pageAnalytic = null
    @observable pageTool = null
    @observable recommend = null
    @observable recommend_lenta_page = null
    @observable recommend_signal_page = null
    @observable signalSlider = null
    @observable isMobileMenuOpened = false
    @observable isMobileAccountMenuOpened = false
    @observable footerLink = null

    // General MenuItemPage for all routes that not match
    // <Route path="*" component={MenuItemPage} />
    @observable isMenuItemPageReady = false
    @observable isMenuItemPageNotFound = false
    @observable isMenuLKOpened = false


    @action
    openMenuLK(){
        this.isMenuLKOpened = true
        console.log('=============== openMenuLK ===============')
        window.$('.usercab_popup').css({opacity:1, visibility:"visible"})
    }

    @action
    closeMenuLK(){
        this.isMenuLKOpened = false
        console.log('=============== closeMenuLK ===============')
        window.$('.usercab_popup').css({opacity:0, visibility:"hidden"})
    }

    /**
     * If user didn't pay for subscriptions, remove
     * video_reports item from menu
     */
    @computed
    get accountMenu(){
        let arr = Array.from(this.map.values())
        arr = arr.filter(obj => obj.isAccount === true)

        const user = getUserStore()
        if (!user.isAuthenticated || !user.profile){
            return arr
        }

        if (!user.profile.isSubscriptionPaid){
            arr = arr.filter(obj => obj.url !== '/account/video')
        }
        return arr
    }

    @action
    load = async () => {
        if (this.map){
            return
        }

        let result = await apiCall(MENU_URL, 'GET', {}, false)
        if(!result.isError){
            let map1 = new Map()
            let map2 = new Map()
            for(let obj of result.data){
                map1.set(obj.id, new MenuModel(obj))
                map2.set(obj.url, new MenuModel(obj))
            }
            this.map = map1
            this.mapByUrl = map2
        }
    }

    @action
    loadMenuRight = async () => {
        if (this.menuRight){
            return
        }

        let result = await apiCall(MENU_RIGHT_URL, 'GET', {}, false)
        if(!result.isError){
            let arr = []
            for(let obj of result.data){
                arr.push(new GeneralModel(obj))
            }
            this.menuRight = arr
        }
    }

    @action
    loadMenuRightLink = async () => {
        if (this.menuRightLink){
            return
        }

        let result = await apiCall(MENU_RIGHT_LINK_URL, 'GET', {}, false)
        if(!result.isError){
            let arr = []
            for(let obj of result.data){
                arr.push(new GeneralModel(obj))
            }
            this.menuRightLink = arr
        }
    }

    @action
    loadMenuFooter = async () => {
        if (this.menuFooter){
            return
        }

        let result = await apiCall(MENU_FOOTER_URL, 'GET', {}, false)
        if(!result.isError){
            let arr = []
            for(let obj of result.data){
                arr.push(new GeneralModel(obj))
            }
            this.menuFooter = arr
        }
    }

    @action
    loadMenuTop = async () => {
        if (this.menuTop){
            return
        }

        let result = await apiCall(MENU_TOP_URL, 'GET', {}, false)
        if(!result.isError){
            let arr = []
            for(let obj of result.data){
                arr.push(new GeneralModel(obj))
            }
            this.menuTop = arr
        }
    }

    /**
     * Component initially loads first 10 items.
     * Then load in background all items.
     */
    @action
    loadMenuContentItems = async (id, perPage, isInitial) => {
        let url = MENU_CONTENTS_URL.replace('<id>', id)

        if (isInitial){
            this.contentItems = null    
            url += '?is_initial=true'
        }

        let result = await apiCall(url, 'GET', {}, false)
        if(!result.isError){
            let arr = []
            for(let obj of result.data){
                arr.push(new ContentModel(obj))
            }
            this.contentItems = arr
            const store = getPaginatorStore()
            store.initPaginator(arr, 1, perPage)
        }
    }

    @action
    loadArticles = async (limit=null) => {
        let url = (limit) ? ARTICLES_URL + '?limit=' + limit : ARTICLES_URL

        let result = await apiCall(url, 'GET', {}, false)
        if(!result.isError){
            let arr = []
            logGroupMessage('loadArticles стр.201', url, result)
            for(let obj of result.data.results){
                arr.push(new ContentModel(obj))
            }
            this.articles = arr
        }
    }

    /**
     * Set contentItem to ami
     */
    @action
    loadContentItem = async (url) => {
        const urlMD5 = md5(url)
        url = CONTENT_URL.replace('<url_md5>', urlMD5)
        let result = await apiCall(url, 'GET', {}, false)
        if(!result.isError){
            // Set content item to ami
            const ami =  new ActiveMenuItemModel(result.data)
            
            const breadcrumbs = [...ami.menuItem.breadcrumbs]
            breadcrumbs.push({name: ami.name, url: null})
            
            ami.breadcrumbs = breadcrumbs
            ami.content = ami.body
            this.setActiveMenuItem(ami)
            this.setMenuItemPageReady(true)
        } else {
            const ami = new ActiveMenuItemModel({title: 'Страница не найдена'})
            this.setActiveMenuItem(ami)
            this.setMenuItemPageNotFound(true)
        }
    }

    @action
    loadMainHomeSlider = async () => {
        if (this.mainHomeSlider){
            return
        }

        let result = await apiCall(MAIN_HOME_SLIDER_URL, 'GET', {}, false)
        if(!result.isError){
            let arr = []
            for(let obj of result.data){
                arr.push(new GeneralModel(obj))
            }
            this.mainHomeSlider = arr
        }
    }

    @action
    loadHomeSlider = async () => {
        if (this.homeSlider){
            return
        }

        let result = await apiCall(HOME_SLIDER_URL, 'GET', {}, false)
        if(!result.isError){
            let arr = []
            for(let obj of result.data){
                arr.push(new GeneralModel(obj))
            }
            this.homeSlider = arr
        }
    }

    @action
        loadTopHeaderSlider = async () => {
        if (this.topHeaderSlider){
            return
        }

        let result = await apiCall(TOP_HEADER_SLIDER_URL, 'GET', {}, false)
        if(!result.isError){
            let arr = []
            for(let obj of result.data){
                arr.push(new GeneralModel(obj))
            }
            this.topHeaderSlider = arr
        }
    }

    @action
    loadToolsFirstSlider = async () => {
        if (this.toolsFirstSlider){
            return
        }

        let result = await apiCall(TOOLS_FIRST_SLIDER_URL, 'GET', {}, false)
        if(!result.isError){
            let arr = []
            for(let obj of result.data){
                arr.push(new GeneralModel(obj))
            }
            this.toolsFirstSlider = arr
        }
    }

    @action
    loadToolsSecondSlider = async () => {
        if (this.toolsSecondSlider){
            return
        }

        let result = await apiCall(TOOLS_SECOND_SLIDER_URL, 'GET', {}, false)
        if(!result.isError){
            let arr = []
            for(let obj of result.data){
                arr.push(new GeneralModel(obj))
            }
            this.toolsSecondSlider = arr
        }
    }

    @action
    loadHomeWhy = async () => {
        if (this.homeWhy){
            return
        }

        let result = await apiCall(HOME_WHY_URL, 'GET', {}, false)
        if(!result.isError){
            let arr = []
            for(let obj of result.data){
                arr.push(new GeneralModel(obj))
            }
            this.homeWhy = arr
        }
    }

    @action
    loadHomeBenefits = async () => {
        if (this.homeBenefits){
            return
        }

        let result = await apiCall(HOME_BENEFITS_URL, 'GET', {}, false)
        if(!result.isError){
            let arr = []
            for(let obj of result.data){
                arr.push(new GeneralModel(obj))
            }
            this.homeBenefits = arr
        }
    }

    @action
    loadPageAnalytic = async () => {
        let result = await apiCall(PAGE_ANALYTIC_URL, 'GET', {}, false)
        if(!result.isError){
            let arr = []
            for(let obj of result.data){
                arr.push(new GeneralModel(obj))
            }
            this.pageAnalytic = arr
        }
    }

    @action
    loadPageTool = async () => {
        let result = await apiCall(PAGE_TOOL_URL, 'GET', {}, false)
        if(!result.isError){
            let arr = []
            for(let obj of result.data){
                arr.push(new GeneralModel(obj))
            }
            this.pageTool = arr
        }
    }

    @action
    loadRecommend = async () => {
        logGroupMessage('loadRecommend стр.291', this.recommend)
        if (this.recommend){
            return
        }

        let result = await apiCall(RECOMMEND_MAIN_URL, 'GET', {}, false)
        if(!result.isError){
            let arr = []
            for(let obj of result.data){
                arr.push(new GeneralModel(obj))
            }
            this.recommend = arr
        }
        logGroupMessage('loadRecommend стр.304', this.recommend)
    }

    @action
    loadRecommendLenta = async () => {
        if (this.recommend_lenta_page){
            return
        }

        let result = await apiCall(RECOMMEND_LENTA_URL, 'GET', {}, false)
        if(!result.isError){
            let arr = []
            for(let obj of result.data){
                arr.push(new GeneralModel(obj))
            }
            this.recommend_lenta_page = arr
        }
    }


    @action
    loadRecommendSignals = async () => {
        if (this.recommend_signal_page){
            return
        }

        let result = await apiCall(RECOMMEND_SIGNAL_URL, 'GET', {}, false)
        if(!result.isError){
            let arr = []
            for(let obj of result.data){
                arr.push(new GeneralModel(obj))
            }
            this.recommend_signal_page = arr
        }
    }

    @action
    loadSignalSlider = async () => {
        if (this.signalSlider){
            return
        }

        let result = await apiCall(SIGNAL_SLIDER_URL, 'GET', {}, false)
        if(!result.isError){
            let arr = []
            for(let obj of result.data){
                arr.push(new GeneralModel(obj))
            }
            this.signalSlider = arr
        }
    }

    @action
    loadFooterLink = async () => {
        if (this.footerLink){
            return
        }

        let result = await apiCall(FOOTER_LINK_URL, 'GET', {}, false)
        if(!result.isError){
            let arr = []
            for(let obj of result.data){
                arr.push(new GeneralModel(obj))
            }
            this.footerLink = arr
        }
    }


    /**
     * Automatic set ActiveMenuItem.
     * Called from Base after changing routing.
     * If item is not found in menu, do nothing,
     * this case will be handled manually
     * from other each particular place.
     */
    @action
    setActiveMenuItemAuto = (url) => {
        const item = this.mapByUrl.get(url)

        if (item){
            this.closeMobileMenu()
            this.closeMobileAccountMenu()
            this.ami = new ActiveMenuItemModel(item)
            document.title = item.title
            window.scrollTo(0, 0)
        }
    }

    /**
     * Set ActiveMenuItem manually from special places.
     */
    @action
    setActiveMenuItem = (ami) => {
        this.closeMobileMenu()
        this.closeMobileAccountMenu()
        this.ami = ami
        document.title = ami.title
        window.scrollTo(0, 0)
        
    }

    @action
    setMenuItemPageReady = (value) => {
        this.isMenuItemPageReady = value
    }

    @action
    setMenuItemPageNotFound = (value) => {
        this.isMenuItemPageNotFound = value
    }

    /**
     * Site menu
     */
    @action
    closeMobileMenu = () => {
        this.isMobileMenuOpened = false
    }

    /**
     * Site menu
     */
    @action
    toggleMobileMenu = () => {
        this.isMobileMenuOpened = !this.isMobileMenuOpened
    }

    /**
     * Account menu
     */
    @action
    closeMobileAccountMenu = () => {
        this.isMobileAccountMenuOpened = false
    }

    /**
     * Account menu
     */
    @action
    toggleMobileAccountMenu = () => {
        this.isMobileAccountMenuOpened = !this.isMobileAccountMenuOpened
    }
}

const menuStore = new MenuStore()
export default menuStore