import { observable, action } from 'mobx'
import { apiCall } from 'api'
import { getAlertStore } from 'stores'
import { logMessage } from 'services/helpers'

import { UserModel } from 'models'

import {
    CHECK_TOKEN_URL,
    DOWNLOAD_BOOK_REQUEST_URL,
    LOGIN_URL,
    PROFILE_URL,
    PROFILE_CHANGE_EMAIL_URL,
    PROFILE_CHANGE_PASSWORD_URL,
    PROFILE_CHANGE_PHONE_URL,
    PROFILE_SEND_EMAIL_CODE_URL,
    PROFILE_SEND_SMS_CODE_URL,
    RESET_PASSWORD_URL,
    RESET_PASSWORD_SEND_CODE_URL,
    SIGNUP_URL,
    SIGNUP_CONFIRM_URL,
    SIGNUP_CHANGE_DATA_URL,
    SIGNUP_RESEND_EMAIL_CODE_URL,
    SIGNUP_RESEND_PHONE_CODE_URL,
    SUBSCRIBE_FORM_URL,
    SUPPORT_URL
} from 'api'

class UserStore {
    @observable errors = null
    @observable token = null
    @observable profile = null
    @observable signupSmsGuid = null
    @observable isAuthenticated = null
    @observable isDownloadBookRequestInProgress = false
    @observable isChangeEmailInProgress = false
    @observable isChangePasswordInProgress = false
    @observable isChangePhoneInProgress = false
    @observable isChangeProfileInProgress = false
    @observable isEmailCodeSent = false
    @observable isLoginInProgress = false
    @observable isResetPasswordSendCodeInProgress = false
    @observable isResetPasswordInProgress = false
    @observable isSendEmailCodeInProgress = false
    @observable isSendSMSCodeInProgress = false
    @observable isSignupInProgress = false
    @observable isSignupConfirmInProgress = false
    @observable isSignupChangeDataInProgress = false
    @observable isSignupResendEmailCodeInProgress = false
    @observable isSignupResendPhoneCodeInProgress = false
    @observable isSignupStep1Completed = false
    @observable isSubscribeFormInProgress = false
    @observable isSupportInProgress = false
    @observable changePhoneSMSGuid = null
    @observable SubscriptionTradeId = null
    @observable SubscriptionDateFrom = null
    @observable SubscriptionDateTo = null

    @action
    clear() {
        this.errors = null
        this.isDownloadBookRequestInProgress = false
        this.isChangeEmailInProgress = false
        this.isChangePasswordInProgress = false
        this.isChangePhoneInProgress = false
        this.isChangeProfileInProgress = false
        this.isEmailCodeSent = false
        this.isLoginInProgress = false
        this.isResetPasswordSendCodeInProgress = false
        this.isResetPasswordInProgress = false
        this.isSendEmailCodeInProgress = false
        this.isSendSMSCodeInProgress = false
        this.isSignupInProgress = false
        this.isSignupConfirmInProgress = false
        this.isSignupChangeDataInProgress = false
        this.isSignupResendEmailCodeInProgress = false
        this.isSignupResendPhoneCodeInProgress = false
        this.isSubscribeFormInProgress = false
        this.isSupportInProgress = false
        this.changePhoneSMSGuid = null
    }

    @action
    clearErrors() {
        this.errors = null
    }

    /**
     * Check token
     */
    @action
    checkToken = async () => {
        logMessage('Checking user token')

        let token = window.localStorage.getItem('token')
        if (token === null) {
            this.token = null
            this.isAuthenticated = false
            logMessage(this)
            return
        }

        const form = { token: token }

        let result = await apiCall(CHECK_TOKEN_URL, 'POST', form, false)
        if (result.isError) {
            this.token = null
            this.isAuthenticated = false
            return
        }

        if (!result.data.is_valid) {
            this.token = null
            this.isAuthenticated = false
            return
        }

        this.token = token
        this.isAuthenticated = true
    }

    @action
    loadProfile = async () => {
        let result = await apiCall(PROFILE_URL, 'GET', {}, true)
        if (!result.isError) {
            this.profile = new UserModel(result.data)
        }
    }

    @action
    changeProfile = async data => {
        this.errors = null
        const store = getAlertStore()
        this.isChangeProfileInProgress = true

        let result = await apiCall(PROFILE_URL, 'PUT', data, true)
        if (result.isError) {
            this.errors = result.data
            store.showResultErrors(result)
        } else {
            store.createAlert('Профиль успешно изменён')
        }
        this.isChangeProfileInProgress = false
    }

    @action
    logout() {
        window.localStorage.removeItem('token')
        this.token = null
        this.isAuthenticated = false
    }

    @action
    login = async data => {
        this.isLoginInProgress = true
        this.errors = null

        let result = await apiCall(LOGIN_URL, 'POST', data, false)
        if (result.isError) {
            this.errors = result.data
        } else {
            window.localStorage.setItem('token', result.data.token)
            this.token = result.data.token
            this.isAuthenticated = true
        }
        this.isLoginInProgress = false
    }

    @action
    signup = async (data, onSuccess) => {
        this.errors = null
        this.isSignupInProgress = true

        let result = await apiCall(SIGNUP_URL, 'POST', data, false)
        if (result.isError) {
            this.errors = result.data
        } else {
            this.isSignupStep1Completed = true
            this.profile = new UserModel(result.data.user)
            this.signupSmsGuid = result.data.sms_guid
            onSuccess && onSuccess()
        }
        this.isSignupInProgress = false
    }

    @action
    resetPasswordSendCode = async (data, onSuccess) => {
        this.errors = null
        this.isResetPasswordSendCodeInProgress = true

        let result = await apiCall(
            RESET_PASSWORD_SEND_CODE_URL,
            'POST',
            data,
            false
        )
        if (result.isError) {
            this.errors = result.data
        } else {
            onSuccess && onSuccess()
        }
        this.isResetPasswordSendCodeInProgress = false
    }

    @action
    resetPassword = async (data, onSuccess) => {
        this.errors = null
        this.isResetPasswordInProgress = true

        let result = await apiCall(RESET_PASSWORD_URL, 'POST', data, false)
        if (result.isError) {
            this.errors = result.data
        } else {
            onSuccess && onSuccess()
        }
        this.isResetPasswordInProgress = false
    }

    @action
    signupChangeData = async (data, onSuccess) => {
        let url = SIGNUP_CHANGE_DATA_URL.replace('<guid>', this.profile.guid)
        this.isSignupChangeDataInProgress = true
        this.errors = null

        let result = await apiCall(url, 'POST', data, false)
        if (result.isError) {
            this.errors = result.data
        } else {
            const d = result.data
            this.profile = new UserModel(d.user)
            this.signupSmsGuid = d.sms_guid

            const store = getAlertStore()
            if (d.is_email_changed) {
                store.createAlert('На email отправлен код')
            }

            if (d.is_phone_changed) {
                store.createAlert('На телефон отправлен код')
            }
            onSuccess && onSuccess()
        }
        this.isSignupChangeDataInProgress = false
    }

    @action
    signupResendEmailCode = async () => {
        let url = SIGNUP_RESEND_EMAIL_CODE_URL.replace(
            '<guid>',
            this.profile.guid
        )
        this.isSignupResendEmailCodeInProgress = true
        this.errors = null

        let result = await apiCall(url, 'POST', {}, false)
        if (!result.isError) {
            const store = getAlertStore()
            store.createAlert('Код отправлен на email')
        }
        this.isSignupResendEmailCodeInProgress = true
    }

    @action
    signupResendPhoneCode = async data => {
        let url = SIGNUP_RESEND_PHONE_CODE_URL.replace(
            '<guid>',
            this.profile.guid
        )
        this.isSignupResendPhoneCodeInProgress = true
        this.errors = null

        let result = await apiCall(url, 'POST', data, false)
        if (result.isError) {
            const store = getAlertStore()
            store.createAlert(result.data.errors[0])
        } else {
            this.signupSmsGuid = result.data.sms_guid

            const store = getAlertStore()
            store.createAlert('Код отправлен на телефон')
        }
        this.isSignupResendPhoneCodeInProgress = false
    }

    @action
    signupConfirm = async (data, onSuccess) => {
        let url = SIGNUP_CONFIRM_URL.replace('<guid>', this.profile.guid)
        this.isSignupConfirmInProgress = true
        this.errors = null

        let result = await apiCall(url, 'POST', data, false)
        if (result.isError) {
            this.errors = result.data
        } else {
            onSuccess && onSuccess()
        }
        this.isSignupConfirmInProgress = false
    }

    @action
    subscribeForm = async (data, onSuccess) => {
        this.isSubscribeFormInProgress = true
        this.errors = null

        const store = getAlertStore()

        let result = await apiCall(SUBSCRIBE_FORM_URL, 'POST', data, false)
        if (result.isError) {
            this.errors = result.data
            store.showResultErrors(result)
        } else {
            store.createAlert('Форма отправлена')
            onSuccess && onSuccess()
        }
        this.isSubscribeFormInProgress = false
    }

    @action
    support = async (data, onSuccess) => {
        this.isSupportInProgress = true
        this.errors = null

        const store = getAlertStore()

        let result = await apiCall(SUPPORT_URL, 'POST', data, false)
        if (result.isError) {
            this.errors = result.data
            store.showResultErrors(result)
        } else {
            store.createAlert('Сообщение отправлено')
            onSuccess && onSuccess()
        }
        this.isSupportInProgress = false
    }

    @action
    downloadBookRequest = async (bookId, data, isAuth, onSuccess) => {
        let url = DOWNLOAD_BOOK_REQUEST_URL.replace('<book_id>', bookId)
        this.isDownloadBookRequestInProgress = true
        this.errors = null

        let result = await apiCall(url, 'POST', data, isAuth)
        if (result.isError) {
            this.errors = result.data
        } else {
            onSuccess && onSuccess()
        }
        this.isDownloadBookRequestInProgress = false
    }

    @action
    changePassword = async (data, onSuccess) => {
        this.errors = null

        const store = getAlertStore()

        if (data['new_password'] !== data['new_password2']) {
            this.errors = { errors: ['Пароли не совпадают'] }
            return
        }

        this.isChangePasswordInProgress = true

        let result = await apiCall(
            PROFILE_CHANGE_PASSWORD_URL,
            'POST',
            data,
            true
        )
        if (result.isError) {
            this.errors = result.data
            store.showResultErrors(result)
        } else {
            store.createAlert('Пароль успешно изменён')
            onSuccess && onSuccess()
        }
        this.isChangePasswordInProgress = false
    }

    @action
    sendEmailCode = async data => {
        const store = getAlertStore()

        let url = PROFILE_SEND_EMAIL_CODE_URL
        this.isSendEmailCodeInProgress = true
        this.errors = null
        let result = await apiCall(url, 'POST', data, true)
        if (result.isError) {
            this.errors = result.data
            store.showResultErrors(result)
        } else {
            this.isEmailCodeSent = true
            store.createAlert('Код отправлен на e-mail')
        }
        this.isSendEmailCodeInProgress = false
    }

    @action
    sendSMSCode = async data => {
        const store = getAlertStore()

        let url = PROFILE_SEND_SMS_CODE_URL
        this.isSendSMSCodeInProgress = true
        this.errors = null
        let result = await apiCall(url, 'POST', data, true)
        if (result.isError) {
            this.errors = result.data
            store.showResultErrors(result)
        } else {
            this.changePhoneSMSGuid = result.data['sms_guid']
            store.createAlert('Код отправлен на новый номер телефона')
        }
        this.isSendSMSCodeInProgress = false
    }

    @action
    changeEmail = async (data, onSuccess) => {
        const store = getAlertStore()

        let url = PROFILE_CHANGE_EMAIL_URL
        this.isChangeEmailInProgress = true
        this.errors = null
        let result = await apiCall(url, 'POST', data, true)
        if (result.isError) {
            this.errors = result.data
            store.showResultErrors(result)
        } else {
            this.profile = new UserModel(result.data)
            onSuccess && onSuccess()
            store.createAlert('E-mail успешно изменен')
        }
        this.isChangeEmailInProgress = false
    }

    @action
    changePhone = async (data, onSuccess) => {
        const store = getAlertStore()
        data['sms_guid'] = this.changePhoneSMSGuid

        let url = PROFILE_CHANGE_PHONE_URL
        this.isChangePhoneInProgress = true
        this.errors = null
        let result = await apiCall(url, 'POST', data, true)
        if (result.isError) {
            this.errors = result.data
            store.showResultErrors(result)
        } else {
            this.profile = new UserModel(result.data)
            onSuccess && onSuccess()
            store.createAlert('Телефон успешно изменен')
        }
        this.isChangePhoneInProgress = false
    }

    @action
    updateLastAccess = async () => {
        let url = '/v1/profile/update_last_access'
        await apiCall(url, 'POST', {}, true)
    }

    @action
    setErrors(errors) {
        this.errors = errors
    }

    @action
    setSubscriptionActiveInfo(tradeId, dateFrom, dateTo) {
        console.log('setSubscriptionActiveInfo', tradeId, dateFrom, dateTo)
        this.SubscriptionTradeId = tradeId
        this.SubscriptionDateFrom = dateFrom
        this.SubscriptionDateTo = dateTo
    }


}

const userStore = new UserStore()
export default userStore
