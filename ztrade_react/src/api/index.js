import { getUserStore } from 'stores'
import { logGroupMessage, logMessage } from 'services/helpers'

export const WS_CENTRIFUGO_URL = 'wss://api.ztrade.ru/connection/websocket'

// account urls
export const ACCOUNT_FOREX_CATEGORIES_URL = '/v1/account/forex_categories'
export const ACCOUNT_FOREX_SYMBOLS_URL = '/v1/account/forex_symbols'
export const ACCOUNT_FOREX_NEWS_URL = '/v1/account/forex_news'
export const ACCOUNT_FOREX_NEWS_FILTER_URL = '/v1/account/forex_news_filter'
export const ACCOUNT_NOTIFICATIONS_URL = '/v1/account/notifications'
export const ACCOUNT_NOTIFICATIONS_NOT_READ_URL = '/v1/account/notifications/not_read_count'
export const ACCOUNT_NOTIFICATIONS_MARK_ALL_URL = '/v1/account/notifications/mark_all'
export const ACCOUNT_STRATEGIES_URL = '/v1/account/strategies'
export const ACCOUNT_STRATEGY_URL = '/v1/account/strategies/<id>'
export const ACCOUNT_SUBSCRIPTIONS_URL = '/v1/account/subscriptions'
export const ACCOUNT_SUBSCRIPTION_SEND_INFO_URL =
    '/v1/account/subscriptions/<id>/send_info_email'
export const ACCOUNT_SUBSCRIPTION_SEND_ACTIVE_INFO_URL =
    '/v1/account/subscriptions/send-active-info'
export const ACCOUNT_TIME_FRAMES_URL = '/v1/account/time_frames'
export const ACCOUNT_TUTORIAL_BOOK_URL = '/v1/account/tutorial_book'
export const ACCOUNT_USER_SETTINGS_URL = '/v1/account/user_settings'
export const ACCOUNT_VIDEOS_URL = '/v1/account/videos'
export const ACCOUNT_VIDEO_LESSONS_URL = '/v1/account/video_lessons'
export const ACCOUNT_FREE_PERIOD_URL = '/v1/account/free_period'


// broker urls
export const BROKER_CATEGORIES_URL = '/v1/broker/categories'
export const BROKERS_URL = '/v1/broker/brokers'
export const BROKER_URL = '/v1/broker/brokers/<slug>'
export const BROKER_DESCS_URL = '/v1/broker/descs'
export const BROKER_DESC_INCREMENT_URL = '/v1/broker/descs/<id>/increment'
export const BROKER_CREATE_REVIEW_AUTH_URL = '/v1/broker/reviews/create_auth'
export const BROKER_CREATE_REVIEW_NOT_AUTH_URL =
    '/v1/broker/reviews/create_not_auth'

// course urls
export const COURSE_ANALYSIS_URL = '/v1/courses/analysis'
export const COURSE_MARKETS_URL = '/v1/courses/markets'
export const COURSES_FREE_URL = '/v1/courses?paid=false'
export const COURSES_PAID_URL = '/v1/courses?paid=true'

// content urls
export const ARTICLES_URL = '/v1/content/articles'
export const CONTENT_URL = '/v1/content/items/<url_md5>'
export const DOWNLOAD_BOOK_REQUEST_URL =
    '/v1/content/download_book_request/<book_id>'
export const MAIN_HOME_SLIDER_URL = '/v1/content/main-home-slider'
export const HOME_SLIDER_URL = '/v1/content/home_slider'
export const HOME_WHY_URL = '/v1/content/home_why'
export const HOME_BENEFITS_URL = '/v1/content/home_benefits'
export const MENU_URL = '/v1/content/menu'
export const MENU_RIGHT_URL = '/v1/content/menu_right'
export const MENU_RIGHT_LINK_URL = '/v1/content/menu_right_link'
export const MENU_FOOTER_URL = '/v1/content/menu_footer'
export const FOOTER_LINK_URL = '/v1/content/footer_link'
export const MENU_TOP_URL = '/v1/content/menu_top'
export const MENU_CONTENTS_URL = '/v1/content/menu/<id>/items'
export const PAGE_ANALYTIC_URL = '/v1/content/page_analytic'
export const PAGE_TOOL_URL = '/v1/content/page_tool'
export const RECOMMEND_MAIN_URL = '/v1/content/recommend/pages/main'
export const RECOMMEND_LENTA_URL = '/v1/content/recommend/pages/lenta'
export const RECOMMEND_SIGNAL_URL = '/v1/content/recommend/pages/signals'
export const SIGNAL_SLIDER_URL = '/v1/content/signal_slider'
export const BANNERS_URL = '/v1/content/banners'
export const TOP_HEADER_SLIDER_URL = '/v1/content/top_header_slider'
export const TOOLS_FIRST_SLIDER_URL = '/v1/content/tools_first_slider'
export const TOOLS_SECOND_SLIDER_URL = '/v1/content/tools_second_slider'
export const CODE_BANNER_FIRST_BANNER_IN_RIGHT_COL_MAIN_PAGE = 'first_link_in_right_col'

// exchange urls
export const EXCHANGES_URL = '/v1/exchange/exchanges'
export const EXCHANGE_URL = '/v1/exchange/exchanges/<slug>'
export const EXCHANGE_CATEGORIES_URL = '/v1/exchange/categories'
export const EXCHANGE_DESCS_URL = '/v1/exchange/descs'
export const EXCHANGE_DESC_INCREMENT_URL = '/v1/exchange/descs/<id>/increment'
export const EXCHANGE_CREATE_REVIEW_AUTH_URL = '/v1/exchange/reviews/create_auth'
export const EXCHANGE_CREATE_REVIEW_NOT_AUTH_URL =
    '/v1/exchange/reviews/create_not_auth'


// payment urls
export const CREATE_BILL_PRODUCT_AUTH_URL =
    '/v1/payment/create_bill/product/auth'
export const CREATE_BILL_PRODUCT_NOT_AUTH_URL =
    '/v1/payment/create_bill/product/not_auth'
export const CREATE_BILL_SUBSCRIPTION_URL =
    '/v1/payment/create_bill/subscription'
export const CREATE_BILL_TARIFF_URL = '/v1/payment/create_bill/tariff'

// sgn urls
export const CHART01_URL = '/v1/sgn/markets/<code>/chart01'
export const CHART02_URL = '/v1/sgn/markets/<code>/chart02'
export const CHART03_URL = '/v1/sgn/markets/<code>/chart03'
export const MARKETS_URL = '/v1/sgn/markets'
export const SYMBOLS_URL = '/v1/sgn/symbols'
export const SIGNAL_TYPES_URL = '/v1/sgn/signal_types'
export const SIGNALS_URL = '/v1/sgn/markets/<code>/signals'
export const SIGNAL_URL = '/v1/sgn/signals/<id>'
export const SIGNAL_FILTERS_URL = '/v1/sgn/signal_filters'
export const SIGNAL_FILTER_URL = '/v1/sgn/markets/<code>/signal_filter'
export const TARIFFS_URL = '/v1/sgn/markets/<code>/tariffs'

// shop urls
export const SHOP_CATEGORIES_URL = '/v1/shop/categories'
export const SHOP_MARKETS_URL = '/v1/shop/markets'
export const SHOP_PRODUCTS_URL = '/v1/shop/products'
export const SHOP_PRODUCT_URL = '/v1/shop/products/<slug>'

// web urls
export const CHECK_TOKEN_URL = '/v1/auth/check_token'
export const SIGNUP_URL = '/v1/auth/signup'
export const SIGNUP_CONFIRM_URL = '/v1/auth/signup/confirm/<guid>'
export const SIGNUP_CHANGE_DATA_URL = '/v1/auth/signup/change_data/<guid>'
export const SIGNUP_RESEND_EMAIL_CODE_URL =
    '/v1/auth/signup/resend_email_code/<guid>'
export const SIGNUP_RESEND_PHONE_CODE_URL =
    '/v1/auth/signup/resend_phone_code/<guid>'
export const LOGIN_URL = '/v1/auth/login'
export const RESET_PASSWORD_URL = '/v1/auth/reset_password'
export const RESET_PASSWORD_SEND_CODE_URL = '/v1/auth/reset_password_send_code'
export const PROFILE_URL = '/v1/profile'
export const PROFILE_CHANGE_EMAIL_URL = '/v1/profile/change_email'
export const PROFILE_CHANGE_PASSWORD_URL = '/v1/profile/change_password'
export const PROFILE_CHANGE_PHONE_URL = '/v1/profile/change_phone'
export const PROFILE_SEND_EMAIL_CODE_URL = '/v1/profile/send_email_code'
export const PROFILE_SEND_SMS_CODE_URL = '/v1/profile/send_sms_code'
export const UPDATE_LAST_ACCESS_URL = '/v1/profile/update_last_access'
export const SMS_CHECK_CODE_URL = '/v1/sms/check_code'
export const SMS_SEND_URL = '/v1/sms/send'
export const COUNTRIES_URL = '/v1/general/countries'
export const CURRENCIES_URL = '/v1/general/currencies'
export const LANGS_URL = '/v1/general/langs'
export const SUBSCRIBE_FORM_URL = '/v1/general/subscribe_form'
export const SUPPORT_URL = '/v1/general/support'
export const TIMEZONES_URL = '/v1/general/timezones'
export const TRANSLATIONS_URL = '/v1/general/translations'
export const CENTRIFUGO_TOKEN_URL = '/v1/general/centrifugo_token'

export function logResponseMessage(
    briefUrl,
    url,
    method,
    data,
    result,
    status = null
) {
    let head = `${briefUrl} ${method}`
    if (status !== null) head += ` ${status}`
    logGroupMessage('logResponseMessage стр.136', head, url, data, result)
}

/**
 * Generic api call to all endpoints.
 * Under any circumstance function should
 * return result object {isError, data}.
 *
 * @param {string}  url
 * @param {string}  method - GET, POST, PUT, PATCH, DELETE
 * @param {Object}  data
 * @param {boolean} isAuth - Does request need token.
 *
 * @returns {Object}  obj
 * @returns {boolean} obj.isError
 * @returns {Object}  obj.data - requested data from server
 *                               or errors object
 *
 *
 * data - may contain file.
 * If we need to upload file, data may contain File object in any field.
 * And data should have key: "fileFieldName"
 * Example:
 *    data.file = File object
 *    data.fileFieldName = 'file'
 *
 *
 * data errors example: possible keys {
 *   field1: ['error1'],
 *   field2: ['error2', 'error3'],
 *   errors: ['error4']}
 *
 */
export async function apiCall(url, method, data, isAuth) {
    const brierUrl = url

    let apiUrl

    if (process.env.NODE_ENV === 'development') {
        apiUrl = 'http://localhost:8000'
    } else {
        apiUrl = 'https://api.ztrade.ru'
    }

/*    apiUrl = 'https://api.ztrade.ru' */

    if (!url.startsWith('http')) {
        url = apiUrl + url
    }

    isAuth = isAuth || false

    let response
    let result = { isError: false }

    let token = ''
    if (isAuth) {
        token = window.localStorage.getItem('token')
        if (token === null) {
            result.isError = true
            result.data = { errors: ['The token is not provided.'] }
            logGroupMessage(method, data, url, result)
            return result
        }
    }

    try {
        let fetchObj = { method: method }
        fetchObj.headers = {}

        if (data.hasOwnProperty('fileFieldName')) {
            // data contains file for upload
            let fieldName = data.fileFieldName
            let fdata = new FormData()

            fdata.append(fieldName, data[fieldName])

            delete data[fieldName]
            delete data.fileFieldName

            for (let key in data) {
                fdata.append(key, data[key])
            }

            fetchObj.body = fdata
        } else {
            if (method === 'POST' || method === 'PUT' || method === 'PATCH') {
                fetchObj.body = JSON.stringify(data)
                fetchObj.headers = {
                    Accept: 'application/json',
                    'Content-Type': 'application/json'
                }
            }
        }

        if (isAuth) {
            fetchObj.headers.Authorization = 'Token ' + token
        }

        response = await fetch(url, fetchObj)
    } catch (e) {
        result.isError = true
        result.data = { errors: ['Server error.'] }
        logMessage(method, data, url, result)
        return result
    }

    const status = response.status

    if (status === 404) {
        result.isError = true
        result.data = { errors: ['Resource not found.'] }
        logMessage(method, data, url, result)
        return result
    }

    if (status === 500) {
        result.isError = true
        result.data = { errors: ['Internal server error.'] }
        logMessage(method, data, url, result)
        return result
    }

    if (status === 204) {
        result.data = {}
        logMessage(method, data, url, result)
        return result
    }

    let obj = await response.json()

    if (status >= 200 && status <= 300) {
        result.data = obj
        logResponseMessage(brierUrl, url, method, data, result, status)
        return result
    }

    // In all other cases should be valid json object with errors.

    let erObj = {}

    if (obj.hasOwnProperty('error_code')) {
        if (obj.error_code.toLowerCase() === 'bad token') {
            const store = getUserStore()
            store.logout()
        }
    }

    if (obj.hasOwnProperty('detail')) {
        erObj.errors = [obj.detail]
        delete obj.detail
    } else if (obj.hasOwnProperty('non_field_errors')) {
        erObj.errors = [obj.non_field_errors]
        delete obj.non_field_errors
    }

    // Other fields
    for (let key in obj) {
        erObj[key] = obj[key]
    }

    result.isError = true
    result.data = erObj
    logMessage(method, data, url, result)
    return result
}

/**
 * fetch with progress via XMLHttpRequest
 * opts.method
 * opts.headers
 * opts.body
 * @returns {Promise}
 */
export function pfetch(url, opts = {}, onProgress) {
    return new Promise((res, rej) => {
        let xhr = new XMLHttpRequest()
        xhr.open(opts.method || 'get', url)
        for (let k in opts.headers || {}) {
            xhr.setRequestHeader(k, opts.headers[k])
        }

        xhr.onload = e => res(e.target)
        xhr.onerror = rej

        if (xhr.upload && onProgress) {
            xhr.upload.onprogress = onProgress
        }
        xhr.send(opts.body)
    })
}
