import React from 'react'
import { Router, Route, Switch } from 'react-router-dom'
import { Provider } from 'mobx-react'

import { createBrowserHistory } from 'history'
import { RouterStore, syncHistoryWithStore } from 'mobx-react-router'

import {
    Auth,
    AuthAlready,
    AuthCheck,
    Base,
    BaseLayout,
    BaseAuthLayout
} from 'components'

import {
    AccountCalendarPage,
    AccountHomePage,
    AccountSignalsPage,
    AccountTariffsPage,
    AccountProfilePage,
    AccountSubscriptionsPage,
    AccountTrainingPage,
    AccountVideoPage,
    AnalyticPage,
    BrokersPage,
    BrokerPage,
    CoursePage,
    ExchangesPage,
    ExchangePage,
    HomePage,
    InvestmentPage,
    LoginPage,
    LogoutPage,
    MenuItemPage,
    NotFoundPage,
    PartnersPage,
    PammPage,
    PaymentSuccessProduct,
    PaymentSuccessSubscription,
    PaymentSuccessTariff,
    ResetPasswordPage,
    ShopPage,
    ShopProductPage,
    SignupPage,
    SignupConfirmPage,
    TestPage,
    ToolPage,
    TradeSignalsPage,
    TradeSignalsForexPage,
    TradingRobotPage
} from 'pages'

import {
    accountStore,
    alertStore,
    brokerStore,
    courseStore,
    chartStore,
    errorStore,
    exchangeStore,
    generalStore,
    layoutStore,
    menuStore,
    modalStore,
    paginatorStore,
    paymentStore,
    sgnStore,
    shopStore,
    tStore,
    tabsStore,
    userStore
} from 'stores'

const browserHistory = createBrowserHistory()
const routingStore = new RouterStore()

const stores = {
    account: accountStore,
    alert: alertStore,
    broker: brokerStore,
    course: courseStore,
    chart: chartStore,
    error: errorStore,
    exchange: exchangeStore,
    general: generalStore,
    layout: layoutStore,
    modal: modalStore,
    menu: menuStore,
    paginator: paginatorStore,
    payment: paymentStore,
    routing: routingStore,
    sgn: sgnStore,
    shop: shopStore,
    t: tStore,
    tabs: tabsStore,
    user: userStore
}

const history = syncHistoryWithStore(browserHistory, routingStore)

const Routes = () => {
    return (
        <Provider {...stores}>
            <Router history={history}>
                <Switch>
                    <Route
                        path="/account/"
                        render={props => {
                            return (
                                <Auth>
                                    <Base {...props}>
                                        <BaseAuthLayout {...props}>
                                            <Switch>
                                                <Route
                                                    exact
                                                    path="/account/home"
                                                    component={AccountHomePage}
                                                />
                                                <Route
                                                    exact
                                                    path="/account/profile"
                                                    component={
                                                        AccountProfilePage
                                                    }
                                                />
                                                <Route
                                                    exact
                                                    path="/account/calendar"
                                                    component={
                                                        AccountCalendarPage
                                                    }
                                                />
                                                <Route
                                                    exact
                                                    path="/account/signals"
                                                    component={
                                                        AccountSignalsPage
                                                    }
                                                />
                                                <Route
                                                    exact
                                                    path="/account/tools"
                                                    component={
                                                        AccountSubscriptionsPage
                                                    }
                                                />
                                                <Route
                                                    exact
                                                    path="/account/training"
                                                    component={
                                                        AccountTrainingPage
                                                    }
                                                />
                                                <Route
                                                    exact
                                                    path="/account/video"
                                                    component={AccountVideoPage}
                                                />
                                                <Route
                                                    exact
                                                    path="/account/tariffs/:code"
                                                    component={
                                                        AccountTariffsPage
                                                    }
                                                />
                                            </Switch>
                                        </BaseAuthLayout>
                                    </Base>
                                </Auth>
                            )
                        }}
                    />

                    <Route exact path="/test_page" component={TestPage} />
                    <Route exact path="/404" component={NotFoundPage} />
                    <Route exact path="/site/logout" component={LogoutPage} />

                    <Route
                        path="/site"
                        render={props => {
                            return (
                                <AuthAlready>
                                    <Base {...props}>
                                        <Switch>
                                            <Route
                                                exact
                                                path="/site/login"
                                                component={LoginPage}
                                            />
                                            <Route
                                                exact
                                                path="/site/reset_password"
                                                component={ResetPasswordPage}
                                            />
                                            <Route
                                                exact
                                                path="/site/signup"
                                                component={SignupPage}
                                            />
                                            <Route
                                                exact
                                                path="/site/signup_confirm"
                                                component={SignupConfirmPage}
                                            />
                                        </Switch>
                                    </Base>
                                </AuthAlready>
                            )
                        }}
                    />
                    <Route
                        path="/trading_robot"
                        component={TradingRobotPage}
                    />
                    <Route
                        path="/"
                        render={props => {
                            return (
                                <AuthCheck>
                                    <Base {...props}>
                                        <BaseLayout {...props}>
                                            <Switch>
                                                <Route
                                                    exact
                                                    path="/"
                                                    component={HomePage}
                                                />
                                                <Route
                                                    exact
                                                    path="/analytics"
                                                    component={AnalyticPage}
                                                />
                                                <Route
                                                    exact
                                                    path="/brokers"
                                                    component={BrokersPage}
                                                />
                                                <Route
                                                    exact
                                                    path="/brokers/:slug"
                                                    component={BrokerPage}
                                                />
                                                <Route
                                                    exact
                                                    path="/brokers/:slug/pamm"
                                                    component={PammPage}
                                                />
                                                <Route
                                                    exact
                                                    path="/education"
                                                    component={CoursePage}
                                                />
                                                <Route
                                                    exact
                                                    path="/exchanges"
                                                    component={ExchangesPage}
                                                />
                                                <Route
                                                    exact
                                                    path="/exchanges/:slug"
                                                    component={ExchangePage}
                                                />
                                                <Route
                                                    exact
                                                    path="/investment"
                                                    component={InvestmentPage}
                                                />
                                                <Route
                                                    exact
                                                    path="/partners"
                                                    component={PartnersPage}
                                                />
                                                <Route
                                                    exact
                                                    path="/payment/success/product"
                                                    component={
                                                        PaymentSuccessProduct
                                                    }
                                                />
                                                <Route
                                                    exact
                                                    path="/payment/success/subscription"
                                                    component={
                                                        PaymentSuccessSubscription
                                                    }
                                                />
                                                <Route
                                                    exact
                                                    path="/payment/success/tariff"
                                                    component={
                                                        PaymentSuccessTariff
                                                    }
                                                />
                                                <Route
                                                    exact
                                                    path="/shop"
                                                    component={ShopPage}
                                                />
                                                <Route
                                                    exact
                                                    path="/shop/:slug"
                                                    component={ShopProductPage}
                                                />
                                                <Route
                                                    exact
                                                    path="/tools"
                                                    component={ToolPage}
                                                />
                                                <Route
                                                    exact
                                                    path="/trade_signals"
                                                    component={TradeSignalsPage}
                                                />
                                                <Route
                                                    exact
                                                    path="/trade_signals_forex"
                                                    component={TradeSignalsForexPage}
                                                />
                                                <Route
                                                    path="*"
                                                    component={MenuItemPage}
                                                />
                                            </Switch>
                                        </BaseLayout>
                                    </Base>
                                </AuthCheck>
                            )
                        }}
                    />
                </Switch>
            </Router>
        </Provider>
    )
}

export default Routes
