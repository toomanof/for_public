import React from 'react'

const MobileDetect = require('mobile-detect')

export function getErrors(errors, name) {
    if (errors === null) {
        return []
    }

    if (name === null) {
        name = 'errors'
    }

    let errors_list = []
    if (errors !== null) {
        for (let label in errors) {
            if (label === name) {
                for (let error of errors[label]) {
                    errors_list.push(error)
                }
            }
        }
    }
    return errors_list
}

// if error return {isSuccess: false, errorCode: error_code}
// if success return {isSuccess: true}
// errorCodes - 1: password length < 8, 2: passwords do not match
export function checkPasswords(password1, password2) {
    if (password1.length < 8) {
        return { isSuccess: false, errorCode: 1 }
    }

    if (password1 !== password2) {
        return { isSuccess: false, errorCode: 2 }
    }

    return { isSuccess: true }
}

export function getInitials(s) {
    let listWords = s.split(' ')
    if (listWords.length > 1) {
        return (
            listWords[0].slice(0, 1).toUpperCase() +
            listWords[1].slice(0, 1).toUpperCase()
        )
    }

    if (s.length > 1) {
        return s.slice(0, 2).toUpperCase()
    }

    return s.toUpperCase()
}

export function isHex(s) {
    return /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i.test(s)
}

/**
 * intFields - is array
 * The main purpose is for "select" fields,
 * where in most cases values are integers FK
 */
export function formToObject(form, intFields) {
    let res = {}
    for (let el of form.elements) {
        if (el.type === 'submit') {
            continue
        }
        let value = el.value
        if (typeof value === 'string') {
            value = value.trim()
        }

        if (el.getAttribute('valuetype') === 'bool') {
            if (value === 'false') {
                value = false
            } else {
                value = true
            }
        }

        if (el.getAttribute('type') === 'hidden' && value.length === 0) {
            value = null
        }

        if (el.getAttribute('valuetype') === 'list') {
            if (!value) {
                value = null
            } else {
                value = value.split(',')
                if (el.getAttribute('listtype') === 'int') {
                    value = value.map(obj => parseInt(obj, 10))
                }
            }
        }

        res[el.name] = value
    }

    if (intFields) {
        for (let k in res) {
            if (intFields.includes(k)) {
                if (res[k] === '') {
                    res[k] = null
                } else {
                    res[k] = parseInt(res[k], 10)
                }
            }
        }
    }

    return res
}

export function constructUrlWithParams(baseUrl, params) {
    let url = baseUrl
    let i = 0
    for (let key in params) {
        if (i === 0) {
            url += `?${key}=${params[key]}`
        } else {
            url += `&${key}=${params[key]}`
        }
        i++
    }
    return url
}

export function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1)
    }
    return (
        s4() +
        s4() +
        '-' +
        s4() +
        '-' +
        s4() +
        '-' +
        s4() +
        '-' +
        s4() +
        s4() +
        s4()
    )
}

export function toCamelCase(str) {
    return str
        .toLowerCase()
        .replace(/(_|-)([a-z])/g, str => str[1].toUpperCase())
}

export function isMobile() {
    const md = new MobileDetect(window.navigator.userAgent)
    console.log('isMobile', md.mobile())
    return md.mobile()
}

export function validateEmail(email) {
    var regex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/
    if (!regex.test(email)) {
        return false
    }
    return true
}

export function logMessage(...args) {
    if (process.env.NODE_ENV === 'development') {
        for (let message of args) {
            console.log(message)
        }
    }
}

export function logGroupMessage(groupName, ...args) {
    if (process.env.NODE_ENV === 'development') {
        console.groupCollapsed(groupName)
        for (let message of args) {
            console.log(message)
        }
        console.groupEnd()
    }
}

/**
 * div wrapper
 */
export function rHTML(html, className) {
    if (className) {
        return (
            <div
                className={className}
                dangerouslySetInnerHTML={{ __html: html }}
            />
        )
    }
    return <div dangerouslySetInnerHTML={{ __html: html }} />
}

/**
 * p wrapper
 */
export function rpHTML(html, className) {
    if (className) {
        return (
            <p
                className={className}
                dangerouslySetInnerHTML={{ __html: html }}
            />
        )
    }
    return <p dangerouslySetInnerHTML={{ __html: html }} />
}

export function getWindowWidth() {
    return (
        window.innerWidth ||
        document.documentElement.clientWidth ||
        document.body.clientWidth
    )
}

export function getWindowHeight() {
    return (
        window.innerHeight ||
        document.documentElement.clientHeight ||
        document.body.clientHeight
    )
}


export function getTemplateFileSubcscribtion(subscribtion){
    const results = subscribtion.files.filter( file =>file.type=="template" )
    if(results.length>0){
        return results[0].download_link
    }
}

export function getIndicatorFileSubcscribtion(subscribtion){
    const results = subscribtion.files.filter( file =>file.type=="indicator" )
    if(results.length>0){
        return results[0].download_link
    }
}