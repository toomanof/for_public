//Course
export const COURSE = 'course'


//Exchange
export const EXCHANGE = 'exchange'


// Broker
export const ADVANTAGE = 'advantage'
export const DISADVANTAGE = 'disadvantage'

export const POSITIVE = 'positive'
export const NEUTRAL = 'neutral'
export const NEGATIVE = 'negative'

export const BROKER = 'broker'
export const PAMM = 'pamm'


// Content
export const ARTICLE = 'article'
export const BOOK = 'book'

export const PIC = 'pic'
export const ICO = 'ico'


// Menu
export const CONTENT = 'content'
export const BOOKS = 'books'
export const CONTENT_PIC = 'content_pic'
export const CONTENT_ICO = 'content_ico'


// Signals

export const SHORT = 'short'
export const DAILY = 'daily'
export const MEDIUM = 'medium'

export const BUY = 'buy'
export const SELL = 'sell'

export const MARKET = 'market'
export const LIMIT = 'limit'


// Subscribe form
export const HOW_INVEST = 'how_invest'


// User
export const ADMIN = 'ADMIN'
export const EDITOR_ADMIN = 'EDITOR_ADMIN'
export const SIGNAL_ADMIN = 'SIGNAL_ADMIN'
export const ANALYTIC_ADMIN = 'ANALYTIC_ADMIN'
export const USER = 'USER'

export const EMAIL = 'email'
export const SMS = 'sms'


export const SIGNALS_NOTIFICATIONS_TAB_INDEX = 3
