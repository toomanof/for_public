$(document).ready(function() {
	
	// Placeholder
	$('input.styler, textarea.styler').placeholder();


	// Mobile nav
	$('.mobnav').after('<div class="mobnav_overlay"></div>');
	if ($('div').is('#foottrig')) {
		var height_body = $('#foottrig').offset().top;
	}
	if ( document.documentElement.clientWidth < 1020 ) {
		$('.mobnav').height(height_body);
	}
	$('.showmenu, .showmenu_sm').on('click touchstart',function() {
		$('.mobnav').addClass('opened');
		$('.mobnav_overlay').fadeIn(100);
		return false;
	});
	$('.mobnav_overlay').on('click touchstart',function() {
		$('.mobnav').removeClass('opened');
		$('.mobnav_overlay').fadeOut(100);
		return false;
	});
	$('.mobnav_close').click(function() {
		$('.mobnav').removeClass('opened');
		$('.mobnav_overlay').fadeOut(100);
		return false;
	});


	// Unav
	$('.userleftmenu').after('<div class="userleftmenu_overlay"></div>');
	$('.unav').on('click',function() {
		$('.userleftmenu').addClass('opened');
		$('.userleftmenu_overlay').fadeIn(100);
		return false;
	});
	$('.userleftmenu_overlay').on('click touchstart',function() {
		$('.userleftmenu').removeClass('opened');
		$('.userleftmenu_overlay').fadeOut(100);
		return false;
	});
	$('.unav_close').click(function() {
		$('.userleftmenu').removeClass('opened');
		$('.userleftmenu_overlay').fadeOut(100);
		return false;
	});


	// Slider
	$('#hometopSlider').owlCarousel({
		items: 5,
		margin: 10,
		mouseDrag: false,
		dots: true,
		nav: false,
		responsive: {
			0: { items: 2 },
			800: { items: 3 },
			1020: { items: 4 },
			1100: { items: 5 }
		}
	});
	$('#homeslider_slide').owlCarousel({
		items: 1,
		margin: 0,
		mouseDrag: false,
		dots: true,
		nav: true
	});
	$('#signals_slider').owlCarousel({
		items: 1,
		margin: 0,
		mouseDrag: true,
		dots: true,
		nav: false,
		autoplay: true,
		loop: true
	});
	$('#opt_slider').owlCarousel({
		items: 1,
		margin: 0,
		mouseDrag: true,
		dots: true,
		nav: true,
		autoplay: false,
		loop: true
	});
	// Wrap around nav & dots
	$('#homeslider_slide').each(function(index) {
		$(this).find('.owl-nav, .owl-dots').wrapAll('<div class="owl-controls"></div>');
	});


	// phonemob
	$('.phonemob_del').click(function() {
		$(this).parent().remove();
		return false;
	});
	

	// Spoiler
	$('.fshow').click(function() {
		$('.filternews_hide').slideToggle(200);
		$(this).toggleClass('open');
		return false;
	});
	$('.checkrow_show').click(function() {
		$('.checkrow_hide').toggleClass('open');
		$(this).toggleClass('open');
		return false;
	});


	// articles_slider
	var swiper = new Swiper('#articles_slider', {
		slidesPerView: 'auto',
		spaceBetween: 20,
		pagination: {
			el: '.swiper-pagination',
			clickable: true,
		},
		breakpoints: {
			1020: { slidesPerView: 2, spaceBetween: 30 },
			1170: { slidesPerView: 3, spaceBetween: 30 }
		}
	});

	// head_banner
	var head_banner = new Swiper('.head_banner .swiper-container', {
		slidesPerView: 1,
		spaceBetween: 10,
		autoplay: {
			delay: 5000
		}
	});


	// Ontop
	var ontopBtn = $('#ontop');
	$(window).scroll(function() {
		var scrolltop = $(document).scrollTop();
		if ( scrolltop > 400 ) { ontopBtn.fadeIn(200); }
		else { ontopBtn.fadeOut(200); }
	});
	ontopBtn.click(function() {
		$('html, body').animate({
			scrollTop: 0
		}, 700);
		return false;
	});


	// js_tme_modal_close
	$('.js_tme_modal_close').click(function() {
		$(this).closest('.tme_modal').addClass('hide');
		return false;
	});


	// Fancybox
	$('.js_open_account').fancybox({
		autoFocus: false,
		touch: false,
		closeBtn: false,
		btnTpl: { close: '', smallBtn: '' }
	});

	$('.show-minimodal-tab1').on('click', function() {
		$('#minimodal-tab1').fadeIn(200);
		$('#minimodal-tab2').hide();
		$('.minimodal_tabs .btn').removeClass('btn-blue');
		$(this).addClass('btn-blue');
		return false;
	});
	$('.show-minimodal-tab2').on('click', function() {
		$('#minimodal-tab2').fadeIn(200);
		$('#minimodal-tab1').hide();
		$('.minimodal_tabs .btn').removeClass('btn-blue');
		$(this).addClass('btn-blue');
		return false;
	});


	// new latptop_slider
	var latptop_slider = new Swiper('.latptop_slider .swiper-container', {
		slidesPerView: 1,
		spaceBetween: 10,
		navigation: {
			nextEl: '.latptop_slider .swiper-button-next',
			prevEl: '.latptop_slider .swiper-button-prev',
		},
		pagination: {
			el: '.latptop_slider .swiper-pagination',
			clickable: true,
		}
	});


	// jScrollPane
	if ( $('div').hasClass('.signal_scroll, .list_scroll') ) {
		if ( document.documentElement.clientWidth >= 600 ) {
			$('.signal_scroll').jScrollPane({
				autoReinitialise: true
			});
		}
		$('.list_scroll').jScrollPane({
			autoReinitialise: true
		});
	}
	
	/*
	$('.js_callbackwin, .js_paywinmodal').fancybox({
		touch : false,
		closeBtn: false
	});
	

	// Checkbox, Radio, Select
	$('input.radio, input.checkbox, .selectbox, .alertchange, .filtersel').styler();

	// kursrow
	$('.kursrow').marquee({
		duration: 10000,
		delayBeforeStart: 0,
		direction: 'left'
	});
	*/


	// Modal
	$('.js_indicator_modal2').on('click', function() {
		$.fancybox.close();
	});
	$('.js_review_modal, .js_negative_modal, .js_indicator_modal, .js_indicator_modal2').fancybox({
		autoFocus: false,
		touch: false,
		closeBtn: false,
		btnTpl: { close: '', smallBtn: '' }
	});


	// Gallery
	var galleryThumbs = new Swiper('.gallery_thumbs .swiper-container', {
		spaceBetween: 10,
		slidesPerView: 5,
		freeMode: true,
		watchSlidesVisibility: true,
		watchSlidesProgress: true,
	});
	var galleryTop = new Swiper('.gallery_big .swiper-container', {
		spaceBetween: 10,
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
		},
		pagination: {
			el: '.gallery_big .swiper-pagination'
		},
		thumbs: {
			swiper: galleryThumbs
		}
	});
	$('.gallery_big .swiper-slide a').fancybox({
		autoFocus: false,
		touch: false,
		closeBtn: false,
		buttons: [
			"zoom",
			//"share",
			"slideShow",
			//"fullScreen",
			//"download",
			"thumbs",
			"close"
		],
		btnTpl: { close: '', smallBtn: '' }
	});

	
	// studynav_parent
	$('.studynav_parent').on('click', function() {
		if ( !$(this).hasClass('open') ) {
			$('.studynav li ul').slideUp(200);
			$('.studynav_parent').removeClass('open');
			$(this).closest('li').find('ul').slideDown(200);
			$(this).addClass('open');
		} else {
			$(this).closest('li').find('ul').slideUp(200);
			$(this).removeClass('open');
		}
		// $(this).closest('li').find('ul').slideToggle(200);
		// $(this).toggleClass('open');
		return false;
	});

	$('.js_studynav_mobile_inner').on('click', function() {
		$('.studynav_nav').slideToggle(200);
		$('.studynav li ul').slideUp(200);
		$('.studynav_parent').removeClass('open');
		return false;
	});
	

});