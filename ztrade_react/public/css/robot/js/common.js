window.$(document).ready(function() {
	
	// Sliders
	window.$('#period-slider').ionRangeSlider({
		min: 1,
		max: 365,
		from: 153,
		postfix: ' дней'
	});
	window.$('#size-slider').ionRangeSlider({
		min: 250,
		max: 10000,
		from: 3700,
		prefix: 'window.$'
	});


	// select_code
	window.$('.select_code, .selectbox').styler();


	// showlink
	window.$('.showlink').on('click', function() {
		var ht = window.$(this).parent().find('.short_text_inner').outerHeight();
		if ( !window.$(this).hasClass('active') ) {
			window.$(this).parent().find('.short_text').css('height', ht);
			window.$(this).addClass('active');
			window.$(this).text('Скрыть');
		} else {
			window.$(this).parent().find('.short_text').removeAttr('style');
			window.$(this).removeClass('active');
			window.$(this).text('Показать полностью');
		}
		return false;
	});
	
	
	// Fancybox
	window.$('.js_modal2, .js_modal3').click(function() {
		window.$.fancybox.close();
	});
	window.$('.js_modal1, .js_modal2, .js_modal3').fancybox({
		touch : false,
		closeBtn: false,
		autoFocus: false,
		btnTpl: { close: false, smallBtn: false }
	});

	
	// Navigarion scroll
	window.$('.js_scroll').click(function() {
		var target = window.$(this).attr('href');
		window.$('html, body').animate({scrollTop: window.$(target).offset().top-20}, 700);
		return false;
	});
		

	
	
	// Waypoint blocks
	/*
	window.$('section').each(function() {
		var self = window.$(this);
		window.$(this).waypoint(function(direction) {
			if (direction === "down") {
				self.addClass('viewed');
			} else if (direction === "up") {
				self.removeClass('viewed');
			};
		}, {offset: 500});
	});
	*/

});