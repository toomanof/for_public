### Production
1. Включить `process_email` в `Emailer`

### Скрипт для TradingChart

Вставлен глобально.
<script src="https://s3.tradingview.com/tv.js"></script>

Если <TradingChart /> потом уберется, то и глобальный скрипт тоже убрать.

### Верхнее меню

Классы item назначаются автоматически через post_save signal MenuTop.

Основное меню должен содержать class item в последних 4-х пунктах


<nav class="navmenu hidden-sm">
<div class="container">
<ul>
<li><a href="#">Начать работу</a></li>
<li><a href="#">Брокеры</a></li>
<li><a href="#">Аналитика</a></li>
<li><a href="#">Обзор рынков</a></li>
<li><a href="#">Статистика</a></li>
<li><a href="#">Обучение</a></li>
<li class="item"><a href="#">Инструменты</a></li>
<li class="item"><a href="#">Партнерам</a></li>
<li class="item"><a href="#callbackwin" class="js_callbackwin">Задать вопрос</a></li>
<li class="item"><a href="#">Личный кабинет</a></li>
</ul>
<div class="showmenu"></div>
</div>
</nav><!-- #navmenu -->

Меню дублируемое, для мобильных наоборот должно содержать class item во всех пунктах, кроме последних 4-х

<div class="module module_nav">
<div class="module_title">Меню</div>
<ul class="module_menu">
<li class="item"><a href="#">Начать работу</a></li>
<li class="item"><a href="#">Брокеры</a></li>
<li class="item"><a href="#">Аналитика</a></li>
<li class="item"><a href="#">Обзор рынков</a></li>
<li class="item"><a href="#">Статистика</a></li>
<li class="item"><a href="#">Обучение</a></li>
<li><a href="#">Инструменты</a></li>
<li><a href="#">Личный кабинет</a></li>
<li><a href="#">Партнерам</a></li>
<li><a href="#callbackwin" class="js_callbackwin">Задать вопрос</a></li>
</ul>
</div><!-- #module -->



### Селекты

На данный момент в ней можно использовать и стилизованный select и html конструкцию.

Стилизованный select должен быть сделан так:
<select class="selectbox">
    <option>Option 1</option>
    <option>Option 2</option>
    <option>Option 3</option>
    ...
</select>

Html конструкция select выглядит вот так. Вместо select>option исползуем этот код:

<div class="jq-selectbox">
    <div class="jq-selectbox__select">
        <div class="jq-selectbox__select-text">Choosed/Defalt option</div>
        <div class="jq-selectbox__trigger"></div>
    </div>
    <div class="jq-selectbox__dropdown" style="display: none;">
        <ul>
            <li>Option 1</li>
            <li>Option 2</li>
            <li>Option 3</li>
        </ul>
    </div>
</div>

jq-selectbox - обертка селекта и его дропдауна
jq-selectbox__select - обертка селекта
jq-selectbox__select-text - текст селекта, если очень длинный, то показывается ...
jq-selectbox__trigger - галочка селекта
jq-selectbox__dropdown - обертка дропдауна
jq-selectbox__dropdown > ul > li - аналог option

Я рекомендую использовать html конструкцию, она 100% кроссбраузерная. Клавиатура никому не нужна. Ради нее не стоит ИМХО жертвовать дизайном и всей версткой.


======== МОДАЛЬНОЕ ОКНО ========
Сейчас имеет такую структуру:

<div class="modal_outer" style="display: none;">
    <div class="modal_overlay"></div>
    <div class="modal_flex">        
        <div class="modal">
            //
            // Контент окна тут...
            //
            <a href="#" class="modal_close"></a>
        </div><!-- #modal -->
    </div><!-- #modal_flex -->
</div><!-- #modal_outer -->

modal_outer - общая обертка окна, враппера с центровкой, и затемнения
modal_overlay - затемнение
modal_flex - данный враппер центрует окно по вертикали и горизонтали
modal - само окно
modal_close - крестик закрытия окна

Сейчас окно отцентровано по 2-м осям. modal_outer - имеет фиксированную позицию относительно экрана.


